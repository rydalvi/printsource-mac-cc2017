#ifndef __CSWFMEDIATORCLASS_H__
#define __CSWFMEDIATORCLASS_H__

#include "VCPlugInHeaders.h"
#include "PMString.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "LanguageModel.h"
#include "ActionID.h"
#include "UIDList.h"

class CSWFMediatorClass
{
	public:
		static int32 count1;
        static int32 count2;
		static int32 count3;
		static int32 count4;
		static int32 count5;
		
		static UIDRef	curSelItemUIDRef;
		static int32	curSelRowIndex;
		static PMString curSelRowString;
		static int32	curSelRadBtn;	//1:Append 2:Embed
		static int32	curSelLstbox;	//1:PF 2:PG 3:PR 4:Item
		static bool16	imageFlag;
		static IControlView* PFLstboxCntrlView;
		static IControlView* PGLstboxCntrlView;
		static IControlView* PRLstboxCntrlView;
		static IControlView* ItemLstboxCntrlView;
		static IControlView* ProjectLstboxCntrlView;
		// Awasthi
		static IControlView* dropdownCtrlView1;
		static IControlView* refreshBtnCtrlView1;
		static IControlView* appendRadCtrlView1;
		static IControlView* embedRadCtrlView1;
		static IControlView* tagFrameChkboxCtrlView1;
		// End Awasthi

		static double	lastSelClasId;
		static PMString lastSelClasName;
		static PMString lastSelClasHierarchy;
		static double	lastSelClasIdForRefresh;
		static PMString lastSelClasNameForRefresh;
		static PMString lastSelClasHierarchyForRefresh;

		static IPanelControlData* iPanelCntrlDataPtr;
		static IPanelControlData* iPanelCntrlDataPtrTemp;
		static int32	tableFlag;
		static bool16	loadData;		

		static CLanguageModel CurrentLanguageModel;
		static int32 LanguageModelCacheSize ;

		static ActionID CurrentLangActionID;
		static double CurrLanguageID;

		static bool16 doReloadFlag ;
		//added by vijay choudhari on 24-04-2006 . UID of the frame(Active frame which
		//has cursor)is stored in following variable to make it persistent.
		static int32	initialSelItemUIDRef;
		static int32    initialCaratPosition;
		static int32    overLapingStatus;
		static int32	addTagToTextDoubleClickVersion;
		static int32	textSpanOFaddTagToTextDoubleClickVersion;
		static int32	appendTextIntoSelectedAndOverlappedBox;
		static int32	isCaratInsideTable;
		static bool16   isInsideTable ;
		static bool16	checkForOverlapWithTextFrame; 
		static bool16	checkForOverlapWithMasterframe;
		static UIDList	UIDListofMasterPageItems;
		//added by vijay on 9-9-2006
		static bool16	IsOneSourceMode; 
		//added by vijay on 9-11-2006
		static bool16	IsTableInsideTableCell; 
		//----
		static double currTempletSelectedClassID ;

};

#endif
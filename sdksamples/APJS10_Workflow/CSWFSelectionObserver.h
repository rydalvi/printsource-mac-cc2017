#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "CSWFID.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "ISelectableDialogSwitcher.h"
#include "IAppFramework.h"
//#include "CSWFListboxData.h"
//#include "SDKListBoxHelper.h"
#include "IListBoxController.h"
#include "ITextControlData.h"
//#include "IClassificationTree.h"
//#include "CSWFMediatorClass.h"
#include "IDialogController.h"
//#include "CSWFCommonFunctions.h"
#include "ITriStateControlData.h"
#include "IEventHandler.h"

class CSWFSelectionObserver : public ActiveSelectionObserver
{
	public:
		CSWFSelectionObserver(IPMUnknown *boss);
		virtual ~CSWFSelectionObserver();
		void AutoAttach();
		void AutoDetach();
		void Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		//void populatePFPanelLstbox(void);
		//void populatePGPanelLstbox(void);
		//void populatePRPanelLstbox(void);
		//void populateItemPanelLstbox(void);
		void populateProjectPanelLstbox(void);
		void setTriState(const WidgetID&  widgetID, ITriStateControlData::TriState state);
		void loadPaletteData();	
		//added by vijay on 9-9-2006
		//void populateCatagoryPanelLstbox();
		//PMString GetSelectedLocaleName(IControlView* LocaleControlView, int32& selectedRow); //18-may

	protected:
		IPanelControlData*	QueryPanelControlData();
		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void UpdatePanel();
		virtual void HandleSelectionChanged(const ISelectionMessage* message);
		//void HandleDropDownListClick(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		IEventHandler* piEventHandler;
};

#ifndef __DDHELPER_H__
#define __DDHELPER_H__

#include "VCPluginHeaders.h"
#include "IControlView.h"

class DragDropHelper
{
private:
	double selectedID;
	PMString selectedName;
	int32 selectedLevel;
public:
	DragDropHelper();
	double getSelectionFromPFTree(PMString&, int32*, IControlView* = nil);
	double getSelectedID(void){ return selectedID; }
	void getSelectedName(PMString& name){ name=selectedName; }
};

#endif
#include "VCPlugInHeaders.h"
#include "IControlView.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IntNodeID.h"
#include "CTreeViewWidgetMgr.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SysControlIds.h"
#include "SPID.h"
#include "SPTreeModel.h"
#include "PublicationNode.h"
#include "MediatorClass.h"
#include "CAlert.h"
#include "SPTreeDataCache.h"
//#include "ISpecialChar.h"
//#include "ISpecialChar.h"
#include "CAlert.h"
#include "IAppFramework.h"
#include "IBooleanControlData.h"
#include "IUIFontSpec.h"
//#include "IMessageServer.h"
//#define FILENAME			PMString("SPTreeViewWidgetMgr.cpp")
//#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
//#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
#define CA(X) CAlert::InformationAlert(X); 



class SPTreeViewWidgetMgr: public CTreeViewWidgetMgr
{
public:
	SPTreeViewWidgetMgr(IPMUnknown* boss);
	virtual ~SPTreeViewWidgetMgr() {}
	virtual	IControlView*	CreateWidgetForNode(const NodeID& node) const;
	virtual	WidgetID		GetWidgetTypeForNode(const NodeID& node) const;
	virtual	bool16 ApplyNodeIDToWidget
		( const NodeID& node, IControlView* widget, int32 message = 0 ) const;
	virtual PMReal GetIndentForNode(const NodeID& node) const;
private:
	PMString getNodeText(const int32& uid, int32 *RowNo) const;
	void indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const;
	enum {ePFTreeIndentForNode=3};
};	

CREATE_PMINTERFACE(SPTreeViewWidgetMgr, kSPTreeViewWidgetMgrImpl)

SPTreeViewWidgetMgr::SPTreeViewWidgetMgr(IPMUnknown* boss) :
	CTreeViewWidgetMgr(boss)
{	//CA("Inside WidgetManager constructor");
}

IControlView* SPTreeViewWidgetMgr::CreateWidgetForNode(const NodeID& node) const
{	//CA("CreateWidgetForNode");
	IControlView* retval =
		(IControlView*) ::CreateObject(::GetDataBase(this),
							RsrcSpec(LocaleSetting::GetLocale(), 
							kSPPluginID, 
							kViewRsrcType, 
							kSPTreePanelNodeRsrcID),IID_ICONTROLVIEW);
	ASSERT(retval);
	return retval;
}

WidgetID SPTreeViewWidgetMgr::GetWidgetTypeForNode(const NodeID& node) const
{	//CA("GetWidgetTypeForNode");
	return kSPListParentWidgetId;
}

bool16 SPTreeViewWidgetMgr::ApplyNodeIDToWidget
(const NodeID& node, IControlView* widget, int32 message) const
{	
	CTreeViewWidgetMgr::ApplyNodeIDToWidget( node, widget );
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return kFalse;
	}
	do
	{
	
		InterfacePtr<IPanelControlData> panelControlData(widget, UseDefaultIID());
		ASSERT(panelControlData);
		if(panelControlData==nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::panelControlData is nil");		
			break;
		}

		IControlView*   expanderWidget = panelControlData->FindWidget(kSPTreeNodeExpanderWidgetID);
		ASSERT(expanderWidget);
		if(expanderWidget == nil) 
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::expanderWidget is nil");		
			break;
		}

		IControlView*   spTextWidget = panelControlData->FindWidget(kSPTextWidgetID);
		ASSERT(spTextWidget);
		if(spTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::spTextWidget is nil");
			break;
		}

		IControlView*  newWhitePictureButtonWidget = panelControlData->FindWidget(kNewWhitePictureButtonWidgetID);
		ASSERT(newWhitePictureButtonWidget);
		if(newWhitePictureButtonWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::newWhitePictureButtonWidget is nil");		
			break;
		}
        
        InterfacePtr<IBooleanControlData> newWhitePicturecontrolData(newWhitePictureButtonWidget, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
		if(newWhitePicturecontrolData==nil) {
			CA("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget:: - No IBooleanControlData found on this boss.");
			break;
		}
		InterfacePtr<IUIFontSpec> newWhitePicturefontSpec(newWhitePictureButtonWidget, IID_IUIFONTSPEC); // no kDefaultIID
		if(newWhitePicturefontSpec==nil) {
			CA("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget:: - No IUIFontSpec found on this boss.");
			break;
		}
        

		IControlView*   whiteStarButtonWidget = panelControlData->FindWidget(kWhiteStarButtonWidgetID);
		ASSERT(whiteStarButtonWidget);
		if(whiteStarButtonWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::whiteStarButtonWidget is nil");
			break;
		}
        InterfacePtr<IBooleanControlData> whiteStarButtoncontrolData(whiteStarButtonWidget, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
		if(whiteStarButtoncontrolData==nil) {
			CA("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget:: - No IBooleanControlData found on this boss.");
			break;
		}
		InterfacePtr<IUIFontSpec> whiteStarButtonfontSpec(whiteStarButtonWidget, IID_IUIFONTSPEC); // no kDefaultIID
		if(whiteStarButtonfontSpec==nil) {
			CA("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget:: - No IUIFontSpec found on this boss.");
			break;
		}


		IControlView*   spCountStaticTextWidget = panelControlData->FindWidget(kSPcountStaticTextWidgetID);
		ASSERT(spCountStaticTextWidget);
		if(spCountStaticTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::spCountStaticTextWidget is nil");
			break;
		}

		IControlView*   spSingleItemStaticTextWidget = panelControlData->FindWidget(kSPSingleItemStaticTextWidgetID);
		ASSERT(spSingleItemStaticTextWidget);
		if(spSingleItemStaticTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::spSingleItemStaticTextWidget is nil");
			break;
		}

		IControlView*   spLeadingItemStaticTextWidget = panelControlData->FindWidget(kSPLeadingItemStaticTextWidgetID);
		ASSERT(spLeadingItemStaticTextWidget);
		if(spLeadingItemStaticTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::TPLSelectionObserver::ApplyNodeIDToWidget::spLeadingItemStaticTextWidget is nil");
			break;
		}
		IControlView*   spItemStaticTextWidget = panelControlData->FindWidget(kSPItemStaticTextWidgetID);
		if(spItemStaticTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::TPLSelectionObserver::ApplyNodeIDToWidget::spItemStaticTextWidget is nil");
			break;
		}
	
		IControlView*   spItemGroupStaticTextWidget = panelControlData->FindWidget(kSPItemGroupStaticTextWidgetID);
		ASSERT(spItemGroupStaticTextWidget);
		if(spItemGroupStaticTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::spItemGroupStaticTextWidget is nil");		
			break;
		}
	
		IControlView*   spTableStaticTextWidget = panelControlData->FindWidget(kSPTableStaticTextWidgetID);
		ASSERT(spTableStaticTextWidget);
		if(spTableStaticTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::spTableStaticTextWidget is nil");		
			break;
		}

		/*IControlView*   spTableStaticTextWidget = panelControlData->FindWidget(kSPTableStaticTextWidgetID);
		ASSERT(spTableStaticTextWidget);
		if(spTableStaticTextWidget == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::spTableStaticTextWidget is nil");		
			break;
		}*/
		

		InterfacePtr<const ITreeViewHierarchyAdapter>   adapter(this, UseDefaultIID());
		ASSERT(adapter);
		if(adapter==nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::adapter is nil");		
			break;
		}

		SPTreeModel model;
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(node);
		int32 uid= uidNodeIDTemp->Get();

		PublicationNode pNode;
		SPTreeDataCache dc;

		dc.isExist(uid, pNode);

		TreeNodePtr<IntNodeID>  uidNodeID(node);
		ASSERT(uidNodeID);
		if(uidNodeID == nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::uidNodeID is nil");		
			break;
		}

		int32 *RowNo= NULL;
		int32 LocalRowNO;
		RowNo = &LocalRowNO;

		PMString stringToDisplay( this->getNodeText(uidNodeID->Get(), RowNo));
		stringToDisplay.SetTranslatable( kFalse );
		int result = -1; 

		int32 isProduct =0;

		if(pNode.getIsProduct() == 0 )
		{
			isProduct = 0;
		}
		else if(pNode.getIsProduct() == 1 )
		{
			isProduct = 1;
		}


		if(isProduct == 2)
		{
			//CA("isProduct == 2");
			spItemStaticTextWidget->HideView();
			spItemGroupStaticTextWidget->HideView();
			spTableStaticTextWidget->ShowView();
			spLeadingItemStaticTextWidget->HideView();
			spSingleItemStaticTextWidget->HideView();
		}
		else if(isProduct == 1)
		{
			//CA("isProduct == 1");
			spTableStaticTextWidget->HideView();
			spItemStaticTextWidget->HideView();
			spItemGroupStaticTextWidget->ShowView();
			spLeadingItemStaticTextWidget->HideView();
			spSingleItemStaticTextWidget->HideView();
		}
		else if(isProduct == 0)
		{	
			//CA("isProduct == 0");
			spTableStaticTextWidget->HideView();
			spItemGroupStaticTextWidget->HideView();
			spItemStaticTextWidget->ShowView();
			spLeadingItemStaticTextWidget->HideView();
			spSingleItemStaticTextWidget->HideView();
		}
		else if(isProduct == 3)	//---For Leading Item
		{
			spLeadingItemStaticTextWidget->ShowView();
			spTableStaticTextWidget->HideView();
			spItemGroupStaticTextWidget->HideView();
			spItemStaticTextWidget->HideView();
			spSingleItemStaticTextWidget->HideView();
		}
		else if(isProduct == 4)	//---For Single Item
		{
			spSingleItemStaticTextWidget->ShowView();
			spLeadingItemStaticTextWidget->HideView();
			spTableStaticTextWidget->HideView();
			spItemGroupStaticTextWidget->HideView();
			spItemStaticTextWidget->HideView();

		}
        
		int32 rsrcID = 0;
		if(pNode.getIsStarred())
        {
            whiteStarButtoncontrolData->Select();
            rsrcID =whiteStarButtonfontSpec->GetHiliteFontID();
            if(rsrcID != 0)
			{
				whiteStarButtonWidget->SetRsrcID(rsrcID);
			}
            
        }
        else{
            whiteStarButtoncontrolData->Deselect();
            rsrcID = whiteStarButtonfontSpec->GetFontID();
            if(rsrcID != 0)
			{
				whiteStarButtonWidget->SetRsrcID(rsrcID);
			}
        }
        rsrcID = 0;
        if(pNode.getNewProduct())
        {
            newWhitePicturecontrolData->Select();
            rsrcID =newWhitePicturefontSpec->GetHiliteFontID();
            if(rsrcID != 0)
			{
				newWhitePictureButtonWidget->SetRsrcID(rsrcID);
			}
            
        }
        else{
            newWhitePicturecontrolData->Deselect();
            rsrcID = newWhitePicturefontSpec->GetFontID();
            if(rsrcID != 0)
			{
				newWhitePictureButtonWidget->SetRsrcID(rsrcID);
			}
        }
			/*if(adapter->GetNumChildren(node)<=0)*/
				expanderWidget->HideView();	
			/*else
				expanderWidget->ShowView();
		}
*/
		IControlView* displayStringView = panelControlData->FindWidget( kSPTextWidgetID );
		ASSERT(displayStringView);
		if(displayStringView == nil) 
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::displayStringView is nil");
			break;
		}
		InterfacePtr<ITextControlData>  textControlData( displayStringView, UseDefaultIID() );
		ASSERT(textControlData);
		if(textControlData== nil)
		{
			ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::textControlData is nil");		
			break;		
		}
		
		/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		break;
		PMString stringToInsert("");
		stringToInsert.Append(iConverter->handleAmpersandCase(stringToDisplay));
		*/
        stringToDisplay.ParseForEmbeddedCharacters();
        
		textControlData->SetString(stringToDisplay);


		//IControlView *CountTextIconView=panelControlData->GetWidget(kSPcountStaticTextWidgetID);                       //(20);
		//if(CountTextIconView==nil)
		//	break;

		InterfacePtr<ITextControlData> itemCountControlData(spCountStaticTextWidget,IID_ITEXTCONTROLDATA);
		if(itemCountControlData == nil)
        {
            ptrIAppFramework->LogDebug("APJS9_ProductFinder::SPTreeViewWidgetMgr::ApplyNodeIDToWidget::itemCountControlData is nil");
			break;
        }

		PMString itemListCount("");
		if(pNode.getChildItemCount() > 0)
		{
			itemListCount.Append("(");
			itemListCount.AppendNumber(pNode.getChildItemCount());
			itemListCount.Append(")");
			itemListCount.SetTranslatable(kFalse);
			spCountStaticTextWidget->ShowView();
		}
		else
		{
			itemListCount.Append("(");
			itemListCount.AppendNumber(0);
			itemListCount.Append(")");
			itemListCount.SetTranslatable(kFalse);
			spCountStaticTextWidget->ShowView();
		}
        itemListCount.ParseForEmbeddedCharacters();
		itemCountControlData->SetString(itemListCount);

		//CA(itemListCount);
		this->indent( node, widget, displayStringView );
	} while(kFalse);
	return kTrue;
}

PMReal SPTreeViewWidgetMgr::GetIndentForNode(const NodeID& node) const
{	//CA("Inside GetIndentForNode");
	do
	{
		TreeNodePtr<IntNodeID>  uidNodeID(node);
		ASSERT(uidNodeID);
		if(uidNodeID == nil) 
			break;
		
		SPTreeModel model;
		int nodePathLengthFromRoot = model.GetNodePathLengthFromRoot(uidNodeID->Get());

		if( nodePathLengthFromRoot <= 0 ) 
			return 0.0;
		
		return  PMReal((nodePathLengthFromRoot * ePFTreeIndentForNode)+0.5);
	} while(kFalse);
	return 0.0;
}

PMString SPTreeViewWidgetMgr::getNodeText(const int32& uid, int32 *RowNo) const
{	//CA("getNodeText");
	SPTreeModel model;
	return model.ToString(uid, RowNo);
}

void SPTreeViewWidgetMgr::indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const
{	//CA("Inside indent");
	const PMReal indent = this->GetIndent(node);	
	PMRect widgetFrame = widget->GetFrame();
	widgetFrame.Left() = indent;
	widget->SetFrame( widgetFrame );
	staticTextWidget->WindowChanged();
	PMRect staticTextFrame = staticTextWidget->GetFrame();
	staticTextFrame.Right( widgetFrame.Right()+1500 );
	
	widgetFrame.Right(widgetFrame.Right()+1500);
	widget->SetFrame(widgetFrame);
	
	staticTextWidget->SetFrame( staticTextFrame );
}
	

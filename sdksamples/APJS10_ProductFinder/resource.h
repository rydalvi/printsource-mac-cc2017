//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SP.rc
//
#define kK2MinorVersionNumberForResource 0
#define kAnnaMinorVersionNumberForResource 0
#define kDragontailMinorVersionNumberForResource 0
#define kFiredrakeMinorVersionNumberForResource 0
#define kCobaltMinorVersionNumberForResource 0
#define kBasilMinorVersionNumberForResource 0
#define ROCKET_DOT_RELEASE_NUMBER       0
#define kRocketMinorVersionNumberForResource 0
#define kSDKDefPlugInStepVersionNumberForResource 0
#define kSDKDef_15_PersistMinorVersionNumber 0
#define WINDOWS                         1
#define kK2MajorVersionNumberForResource 1
#define kSherpaMajorVersionNumberForResource 1
#define kHotakaMajorVersionNumberForResource 1
#define kSPMenuItem1MenuItemPosition    1
#define kAnnaMajorVersionNumberForResource 2
#define kDragontailMajorVersionNumberForResource 3
#define kFiredrakeMajorVersionNumberForResource 4
#define kSherpaMinorVersionNumberForResource 5
#define FIREDRAKE_DOT_RELEASE_NUMBER    5
#define Cobalt_DOT_RELEASE_NUMBER       5
#define kCobaltMajorVersionNumberForResource 5
#define BASIL_DOT_RELEASE_NUMBER        5
#define kBasilMajorVersionNumberForResource 6
#define kRocketMajorVersionNumberForResource 7
#define kSPListViewCellHeight           21
#define kSPGridCellWidth                60
#define kSPGridCellHeight               67
#define kSDKDefIconInfoResourceID       180
#define kSDKDefIconInfoRolloverResourceID 180
#define kSDKDefGenericIconInfoResourceID 181
#define kSDKDefIconGenericRolloverResourceID 181
#define kSDKDefMenuResourceID           200
#define kBuildNumber                    355
#define kICCProfileRsrcType             512
#define kSPListElementRsrcID            1000
#define kSPRFilterOptionElementRsrcID   1001
#define kSelectCustomerDialogResourceID 1100
#define kFolderIcon                     20000
#define kBlankIcon                      20100
#define kItemIcon                       20101
#define kUnChkIconID                    21011
#define kChkIconID                      21012
#define kResetFilterIcon                21014
#define kSeparatorIconID                21015
#define kAddToSpreadIcon                21016
#define kUpdateCopyIcon                 21017
#define kUpdateArtIcon                  21018
#define kUpdateItemTableIcon            21019
#define kDeleteFromSpreadIcon           21020
#define kNewProductIcon                 21021
#define kUpdateProductIcon              21022
#define kGreenFilterIcon                21024
#define kNewAddIcon                     21025
#define kNewUpdateIcon                  21026
#define kNewDeleteIcon                  21027
#define kCopyArtIcon                    21028
#define kCopyItemIcon                   21029
#define kArtItemIcon                    21030
#define kCopyArtItemIcon                21031
#define kDesignerMilestoneIcon          21032
#define kPrvIcon                        21035
#define kLocateIcon                     21036
#define kProductIcon                    21037
#define GreenStarIcon                   21038
#define WhiteStarIcon                   21039
#define kGreenStariconIcon              21040
#define kNewRedPictureIcon              21041
#define kNewWhitePictureIcon            21042
#define kTableIcon                      21043
#define kviewWhiteboardIcon             21044
#define kSelectCustomerIcon             21046
#define kSPPNGSprayRsrcID               30001
#define kSPPNGSprayRollRsrcID           30001
#define kSPPNGSubSectionSprayRsrcID     30002
#define kSPPNGSubSectionSprayRollRsrcID 30002
#define kSPPNGImageRsrcID               30003
#define kSPPNGImageRollRsrcID           30003
#define kSPPNGTableSourceRsrcID         30004
#define kSPPNGTableSourceRollRsrcID     30004
#define kSPPNGFilterRsrcID              30005
#define kSPPNGFilterRollRsrcID          30005
#define kSPPNGRefreshRsrcID             30006
#define kSPPNGRefreshRollRsrcID         30006
#define kSPPNGSelectEventRsrcID         30007
#define kSPPNGSelectEventRollRsrcID     30007
#define kSPPNGRevertSearchRsrcID        30008
#define kSPPNGRevertSearchRollRsrcID    30008
#define kSPPNGPreviewRsrcID             30009
#define kSPPNGPreviewRollRsrcID         30009
#define kGreenFilterRsrcID              30010
#define kGreenFilterRollRsrcID          30010
#define kPNGFilterOKIconRsrcID          30011
#define kPNGFilterOKIconRollRsrcID      30011
#define kPNGFilterClearIconRsrcID       30012
#define kPNGFilterClearIconRollRsrcID   30012
#define kPNGFilterCancelIconRsrcID      30013
#define kPNGFilterCancelIconRollRsrcID  30013
#define kSPPNGsearchRsrcID              30014
#define kSPPNGsearchRollRsrcID          30014

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

#include "VCPluginHeaders.h"
#include "Refresh.h"
#include "RefreshData.h"
#include "BoxReader.h"

//#include "LayoutUtils.h"
#include "ILayoutUtils.h"  //Cs4
//#include "ISelection.h"
#include "CAlert.h"

#include "TagReader.h"
//#include "ISelection.h"
#include "ILayoutUtils.h" //Cs4
#include "UIDList.h"
#include "CAlert.h"
#include "IClientOptions.h"
#include "IPageList.h"
#include "IDocument.h"
#include "ISelectUtils.h"
#include "ITextAttrUID.h"
#include "ICommand.h"
#include "ITextAttrUtils.h"
#include "ISwatchList.h"
#include "IWorkspace.h"
//#include "ITextFrame.h"  //Commented By Sachin Sharma on 29/06/07
#include "IMultiColumnTextFrame.h"

#include "ITextModel.h"
#include "ITextFocusManager.h"
#include "ITextFocus.h"
#include "IFrameList.h"
#include "IFrameUtils.h"
#include "IAppFramework.h"
#include "ISpecialChar.h"
#include "ILayoutControlData.h"
#include "ISpread.h"
#include "ISpreadList.h"
#include "MediatorClass.h"
#include "IHierarchy.h"
#include "ITextMiscellanySuite.h"
//#include "IImportFileCmdData.h"
#include "IOpenFileDialog.h"
#include "IImportProvider.h"
#include "IGraphicFrameData.h"
#include "IPlaceGun.h"
#include "IPlaceBehavior.h"
#include "IReplaceCmdData.h"
#include "SDKUtilities.h"
#include "TransformUtils.h"
#include "TableUtility.h"
#include "ITextTarget.h"
#include "ISelectionUtils.h"
#include "TextEditorID.h"
#include "TextIterator.h"
#include "PMString.h"
#include "ITriStateControlData.h"
//#include "RfhSelectionObserver.h"
#include "PageData.h"
#include "ISelectionManager.h"
#include "ILayoutSelectionSuite.h"
#include "ITextSelectionSuite.h"
#include "IConcreteSelection.h"
//#include "ITextFrame.h"   //Commented By Sachin sharma on 2/06/07
#include "IMultiColumnTextFrame.h"//Added
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "ITextWalker.h"
#include "ITextWalkerSelectionUtils.h"
#include <string.h>
//#include "LayoutUIUtils.h " //Cs3
#include "ILayoutUIUtils.h" //Cs4
#include "IPageItemTypeUtils.h"

#include "CTUnicodeTranslator.h"

#include "IFrameContentFacade.h"
#include "IImportExportFacade.h"
#include "URI.h"
#include "WideString.h"

#define CA(z) CAlert::InformationAlert(z)

#define CAI(y,x)	{\
						PMString tempString(y);\
						tempString.Append(" : ");\
						tempString.AppendNumber(x);\
						CAlert::InformationAlert(tempString);\
					}
					
extern RefreshDataList rDataList;
//extern bool16 HiliteFlag;
extern RefreshDataList OriginalrDataList;
extern RefreshDataList DocProductrDataList;

RefreshDataList ColorDataList;

TextVectorList OriginalTextList;
TextVectorList NewTextList;
TextVectorList ChangedTextList;

int16 pgno,cursel,seltext,FlagParentID;
void Refresh::appendToGlobalList(const PageData& pData)
{
	RefreshData rData;

	for(int i=0; i<pData.objectDataList.size(); i++)
	{
		if(pData.objectDataList[i].elementList.size()==0 && pData.objectDataList[i].isTableFlag!=kTrue)
			continue;
		pgno=1;
		cursel=1;
		rData.StartIndex= 0;
		rData.EndIndex= 0;
		rData.BoxUIDRef=pData.BoxUIDRef;
		rData.boxID=pData.boxID;
		rData.elementID=-1;
		rData.isObject=kTrue;
		rData.isSelected=kFalse;
		rData.name=pData.objectDataList[i].objectName;
		rData.objectID=pData.objectDataList[i].objectID;
		rData.publicationID=pData.objectDataList[i].publicationID;
		rData.isTableFlag=pData.objectDataList[i].isTableFlag;
		//Awasthi
		rData.isImageFlag=pData.objectDataList[i].isImageFlag;
		rData.whichTab = pData.objectDataList[i].whichTab;
		rDataList.push_back(rData);
		OriginalrDataList.push_back(rData);

		for(int j=0; j<pData.objectDataList[i].elementList.size(); j++)
		{			
			rData.StartIndex= pData.objectDataList[i].elementList[j].StartIndex;
			rData.EndIndex= pData.objectDataList[i].elementList[j].EndIndex;
			rData.BoxUIDRef=pData.BoxUIDRef;
			rData.boxID=pData.boxID;
			rData.elementID=pData.objectDataList[i].elementList[j].elementID;
			rData.isTaggedFrame=pData.objectDataList[i].elementList[j].isTaggedFrame;
			rData.isObject=kFalse;
			rData.isSelected=kFalse;
			rData.name=pData.objectDataList[i].elementList[j].elementName;
			rData.objectID=pData.objectDataList[i].objectID;
			rData.publicationID=pData.objectDataList[i].publicationID;
			rData.TypeID=pData.objectDataList[i].elementList[j].typeID;
			rData.whichTab =pData.objectDataList[i].elementList[j].whichTab;
			rDataList.push_back(rData);		
			OriginalrDataList.push_back(rData);
		}
		
	}
	
}

bool16 Refresh::getAllPageItemsFromPage(int32 pageNumber)
{

/*	UID pageUID;
	TagReader tReader;
	if(!this->isValidPageNumber(pageNumber, pageUID))
		return kFalse;
		IDocument* document = ::GetFrontDocument(); 
	IDataBase* database= ::GetDataBase(document); 
	/*IDocument* document = layoutData->GetDocument();
	if (document == nil)
		return kFalse;
	IDataBase* database = ::GetDataBase(document);
	if(!database)
		return kFalse;*/
	
/*	InterfacePtr<ISpreadList> spreadList(document, IID_ISPREADLIST); 
	int32 spreadNo = -1; 

	for(int32 i=0; i<spreadList->GetSpreadCount(); ++i) 
	{ 
		UIDRef spreadRef(database, spreadList->GetNthSpreadUID(i)); 
		InterfacePtr<ISpread> spread(spreadRef, IID_ISPREAD); 
		if(spread->GetPageIndex(pageUID)!= -1) 
		{			
			spreadNo = i; 
			break; 
		}
	}

	if(spreadNo==-1)
		return kFalse;

	InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
	if (layoutData == nil)
		return kFalse;
	layoutData->SetSpread(spreadList->QueryNthSpread(spreadNo),kFalse);
	
	InterfacePtr<ILayoutControlData> layoutData1(::QueryFrontLayoutData());
	if (layoutData1 == nil)
		return kFalse;

	IGeometry* spreadItem = layoutData1->GetSpread();
	if (spreadItem == nil)
		return kFalse;
	InterfacePtr<ISpread> spread((IPMUnknown *)spreadItem, UseDefaultIID());
	if(spread == nil)
		return kFalse;
	UIDList tempList(database);

	spread->GetItemsOnPage(pageNumber, &tempList, kFalse);

	selectUIDList=tempList;

	for(int i=0; i<selectUIDList.Length(); i++)
	{	
		InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		if(!iHier)
			continue;

		UID kidUID;
		int32 numKids=iHier->GetChildCount();
		for(int j=0;j<numKids;j++)
		{
			IIDXMLElement* ptr;
			kidUID=iHier->GetChildUID(j);
			UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
			tReader.getTagsFromBox(boxRef, &ptr);
			if(!doesExist(ptr))
				selectUIDList.Append(kidUID);
		}
	}
	return kTrue;
*/
		UID pageUID;
		TagReader tReader;
		if(!this->isValidPageNumber(pageNumber, pageUID))
		return kFalse;
	
		IDocument* document = /*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
		IDataBase *db = ::GetDataBase(document); 
		ASSERT(db); 

		InterfacePtr<ISpreadList> spreadList(document, UseDefaultIID()); 
		ASSERT(spreadList); 

		int32 spreadNum = 0; 
		int32 spreadCount = spreadList->GetSpreadCount(); 
		int32 currentPageNumber = 0; 
		
		
		while(spreadNum < spreadCount) 
		{ 
			bool16 FirstWhilebreak= kFalse;
			InterfacePtr<ISpread> spread(db, spreadList->GetNthSpreadUID(spreadNum), IID_ISPREAD); 
			ASSERT(spread); 

			int32 pagesOnSpread = spread->GetNumPages(); 
			
			int32 PageNO=0;
			while(pagesOnSpread--) 
			{	
				bool16 SecondWhileBreak= kFalse;
				
				if(currentPageNumber == pageNumber) 
				{ 
					
					UIDList itemsList(db); 
					spread->GetItemsOnPage(/*pageNumber*/PageNO, &itemsList, kFalse); 
					selectUIDList = itemsList;
					
					SecondWhileBreak= kTrue;
					FirstWhilebreak= kTrue;
					break;//return itemsList; 
				} 
				PageNO++;
				currentPageNumber++; 
				if(SecondWhileBreak)
					break;
				
			} 
			++spreadNum; 
			if(FirstWhilebreak)
				break;
			
		} 

		for(int i=0; i<selectUIDList.Length(); i++)
		{	
			InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
			if(!iHier)
				continue;

			UID kidUID;
			int32 numKids=iHier->GetChildCount();

			bool16 isGroupFrame = kFalse ;
			isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(i));

			for(int j=0;j<numKids;j++)
			{
				IIDXMLElement* ptr;
				kidUID=iHier->GetChildUID(j);
				UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
				if(isGroupFrame == kTrue) 
				{
					TagList Newtaglist = tReader.getTagsFromBox(boxRef, &ptr);
					
					/*PMString s("NewList.size() : ");
					s.AppendNumber(NewList.size());
					CA(s);*/
				
					if(!doesExist(Newtaglist))//if(!doesExist(ptr))
						selectUIDList.Append(kidUID);
					//------------
					for(int32 tagIndex = 0 ; tagIndex < Newtaglist.size() ; tagIndex++)
					{
						Newtaglist[tagIndex].tagPtr->Release();
					}
				}
				
			}
		}
	return kTrue;

}

bool16 Refresh::isValidPageNumber(int32 pageNumber, UID& myPageUID)
{	
	IDocument * myDocPtr = Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
	IDataBase* db =::GetDataBase(myDocPtr);
	
	InterfacePtr <IDocument> myDoc(myDocPtr, UseDefaultIID());
	
	if(myDoc!=nil)
	{	
		InterfacePtr<IPageList> myPageList(myDoc, UseDefaultIID());
		if(myPageList==nil)
			return kFalse;
		
		int32 PageCount = myPageList->GetPageCount();
		PageCount--;
		if(pageNumber> PageCount)
		{	myPageUID= kInvalidUID;
			return kFalse;
		}
		myPageUID = myPageList->GetNthPageUID(pageNumber);
		if(myPageUID != kInvalidUID)
		return kTrue;
	}
	
	return kFalse;
}

bool16 Refresh::isValidPageNumber(int32 pageNumber)
{
	UID myPageUID;
	IDocument * myDocPtr = /*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument(); ///Cs4
	IDataBase* db =::GetDataBase(myDocPtr);
	
	InterfacePtr <IDocument> myDoc(myDocPtr, UseDefaultIID());

	if(myDoc!=nil)
	{
		InterfacePtr<IPageList> myPageList(myDoc, UseDefaultIID());
		if(myPageList==nil)
			return kFalse;
		
		myPageUID = myPageList->GetNthPageUID(pageNumber);
		if(myPageUID != kInvalidUID)
			return kTrue;
	}
	return kFalse;
}


bool16 Refresh::GetPageDataInfo(/*int whichSelection*/)
{
	BoxReader bReader;
	UID pageUID=kInvalidUID;
	int i,k=0;
	
	rDataList.clear();
	OriginalrDataList.clear();

	pgno=0;
	cursel=0;
	seltext=0;
	FlagParentID = 0;
	this->selectUIDList.Clear();
	
/*	switch(whichSelection)
	{
	case 1:
		{ //for current page option
			UID pageUID;
			TagReader tReader;
			int32 pageNumber= 0;
			if(!this->isValidPageNumber(pageNumber, pageUID))
				return kFalse;
				IDocument* document = ::GetFrontDocument(); 
			IDataBase* database= ::GetDataBase(document); 
			/*IDocument* document = layoutData->GetDocument();
			if (document == nil)
				return kFalse;
			IDataBase* database = ::GetDataBase(document);
			if(!database)
				return kFalse;*/
			
/*			InterfacePtr<ISpreadList> spreadList(document, IID_ISPREADLIST); 
			int32 spreadNo = -1; 

			for(int32 p=0; p<spreadList->GetSpreadCount(); ++p) 
			{ 
				UIDRef spreadRef(database, spreadList->GetNthSpreadUID(p)); 
				InterfacePtr<ISpread> spread(spreadRef, IID_ISPREAD); 
				if(spread->GetPageIndex(pageUID)!= -1) 
				{			
					spreadNo = p; 
					break; 
				}
			}

			if(spreadNo==-1)
				return kFalse;

			InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
			if (layoutData == nil)
				return kFalse;
			 pageUID = layoutData->GetPage();
			if(pageUID == kInvalidUID)
			return kFalse;

			//layoutData->SetSpread(spreadList->QueryNthSpread(spreadNo),kFalse);
			
		//	InterfacePtr<ILayoutControlData> layoutData1(::QueryFrontLayoutData());
		//	if (layoutData1 == nil)
		//		return kFalse;

			IGeometry* spreadItem = layoutData->GetSpread();
			if (spreadItem == nil)
				return kFalse;
			
			InterfacePtr<ISpread> iSpread((IPMUnknown *)spreadItem, UseDefaultIID());
			if (iSpread == nil)
				return kFalse;

			UIDList tempList(database);
			pageNumber=iSpread->GetPageIndex(pageUID);

			iSpread->GetItemsOnPage(pageNumber, &tempList, kFalse);

			selectUIDList=tempList;

			for( int p=0; p<selectUIDList.Length(); p++)
			{	
				InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(p), UseDefaultIID());
				if(!iHier)
					continue;

				UID kidUID;
				int32 numKids=iHier->GetChildCount();
				for(int j=0;j<numKids;j++)
				{
					IIDXMLElement* ptr;
					kidUID=iHier->GetChildUID(j);
					UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
					tReader.getTagsFromBox(boxRef, &ptr);
					if(!doesExist(ptr))
						selectUIDList.Append(kidUID);
				}
			}

			for(int p=0; p<selectUIDList.Length(); p++)
			{	
				PageData pData;
				bReader.getBoxInformation(selectUIDList.GetRef(p), pData);
				appendToGlobalList(pData);
				// Added By Awasthi
				UIDRef boxID=selectUIDList.GetRef(p);
				TagList tList;
				TagReader tReader;
				tList=tReader.getTagsFromBox(boxID);
				if(tList.size()==0)
				{	
					tList=tReader.getFrameTags(boxID);
					if(tList.size()==0)
					{
						//FlagParentID = 1;
						continue;
					}
				}
				for(int j=0; j<tList.size(); j++)
				{				
					if(tList[j].parentId==-1)
						continue;
					k=1;
				}
				if(k==0)
					FlagParentID = 1;			
			}
			
		break;

		}

	case 2://Page number
		if(!this->getAllPageItemsFromPage(Mediator::refreshPageNumber))
		{
			//CA("getAllPageItemsFromPage");
			
			return kFalse;
		}
		for(i=0; i<selectUIDList.Length(); i++)
		{	
			PageData pData;
			bReader.getBoxInformation(selectUIDList.GetRef(i), pData);
			appendToGlobalList(pData);
			// Added By Awasthi
			UIDRef boxID=selectUIDList.GetRef(i);
			TagList tList;
			TagReader tReader;
			tList=tReader.getTagsFromBox(boxID);
			if(tList.size()==0)
			{	
				tList=tReader.getFrameTags(boxID);
				if(tList.size()==0)
				{
					//FlagParentID = 1;
					continue;
				}
			}
			for(int j=0; j<tList.size(); j++)
			{				
				if(tList[j].parentId==-1)
					continue;
				k=1;
			}
			if(k==0)
				FlagParentID = 1;			
		}
		break;


	case 3://Current Selection
		
		if(!this->getAllBoxIds())		
			return kFalse;
		
		for(i=0; i<selectUIDList.Length(); i++)
		{
			PageData pData;
			
			bReader.getBoxInformation(selectUIDList.GetRef(i), pData);
			appendToGlobalList(pData);
			//CA("cursel");
			// Added By Awasthi
			UIDRef boxID=selectUIDList.GetRef(i);
			TagList tList;
			TagReader tReader;
			
			tList=tReader.getTagsFromBox(boxID);
			if(tList.size()==0)
			{
				tList=tReader.getFrameTags(boxID);
				if(tList.size()==0)
				{
					//FlagParentID = 1;
					continue;
				}
			}
			for(int j=0; j<tList.size(); j++)
			{
				
				if(tList[j].parentId==-1)
					continue;
				k=1;
			}
			if(k==0)
				FlagParentID = 1;
		}
		break;

	case 4://Selected Text
*/                // Code for Entire Document Option.
		do
		{

			///////////// Code to get Elementlist from Document/////////////////
			if(!getDocumentSelectedBoxIds())
			{
				//CA("getDocumentSelectedBoxIds False");
				
				return kFalse;
			}
			for(i=0; i<selectUIDList.Length(); i++)
			{
				PageData pData;
				bReader.getBoxInformation(selectUIDList.GetRef(i),pData);
				appendToGlobalList(pData);
				// Added By Awasthi
				UIDRef boxID=selectUIDList.GetRef(i);
				TagList tList;
				TagReader tReader;
				tList=tReader.getTagsFromBox(boxID);
				if(tList.size()==0)
				{
					tList=tReader.getFrameTags(boxID);
					if(tList.size()==0)
					{
						//FlagParentID = 1;
						continue;
					}
				}
				for(int j=0; j<tList.size(); j++)
				{					
					if(tList[j].parentId==-1)
						continue;
					k=1;
				}
				if(k==0)
					FlagParentID = 1;

				//------------
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
				
			}

/// Code for sorting the list For Objects and make a new list which contains only the Object ids from the Current Document ///////////////
		rDataList = OriginalrDataList;
		RefreshDataList theDataList=rDataList;
		rDataList.clear();
		RefreshDataList CurrentObjectDataList;
		CurrentObjectDataList.clear();
		DocProductrDataList.clear();
		for(int i=0; i<theDataList.size(); i++)
		{
			if(!theDataList[i].isObject || theDataList[i].isProcessed)
				continue;		
			
			theDataList[i].isProcessed=kTrue;
			RefreshData referenceNode=theDataList[i];
			bool16 ReturnFlag= kFalse;
			for(int k=0; k<CurrentObjectDataList.size(); k++)
			{
				if((CurrentObjectDataList[k].objectID ==theDataList[i].objectID) && (CurrentObjectDataList[k].publicationID == theDataList[i].publicationID ))
					ReturnFlag= kTrue;
			}
			if(ReturnFlag)
				continue;
			rDataList.push_back(theDataList[i]);//Added the Object
			RefreshData nodeToAdd;
			CurrentObjectDataList.push_back(referenceNode);
			
			DocProductrDataList.push_back(referenceNode);
			for(int j=0; j<theDataList.size();j++)//Now adding the elements which have these elements
			{	
				if(!theDataList[j].isObject)
				{
					nodeToAdd=theDataList[j];
				//	continue;
				
				
					if(referenceNode.objectID==theDataList[j].objectID)
					{
						//nodeToAdd.objectID=referenceNode.objectID;
						if(!theDataList[j].isProcessed)
						{
							nodeToAdd.isSelected=theDataList[j].isSelected;
							rDataList.push_back(nodeToAdd);//Added the Element
							theDataList[j].isProcessed=kTrue;
						}
					}
				}
			}
		}
	//	DocProductrDataList = CurrentObjectDataList;
		
		}while(kFalse);
		//break;
	
	return kTrue;
}


bool16 Refresh::refreshThisBox(UIDRef& boxID, PMString& imagePath)
{  
	//CA("1.1");
	TagList tList;
	TagReader tReader;
	tList=tReader.getTagsFromBox(boxID);
	//CA("1.2");
	if(tList.size()==0)
	{	
		//CA("1.2.1");
		tList=tReader.getFrameTags(boxID);
		//CA("1.2.2");
		if(tList.size()==0)
		{
			return kTrue;
		}
		//CA("1.2.3");
		
		refreshTaggedBox(boxID, imagePath);
		//CA("1.2.4");
		return kTrue;
	}
	//CA("1.3");
	for(int j=0; j<tList.size(); j++)
	{	
		if(!shouldRefresh(tList[j], boxID.GetUID()))
			continue;

		if(tList[j].parentId==-1)
			continue;

		if(tList[j].imgFlag==1)
			fillImageInBox(boxID, tList[j], tList[j].parentId, imagePath);
		else if(tList[j].imgFlag==0)
		{	//PMString QWE("tList[j] tList[j].parentId : ");
			//QWE.AppendNumber(tList[j].parentId);
		//CA(QWE);
			fillDataInBox(boxID, tList[j], tList[j].parentId);
		}
	}
	//CA("1.4");
	//------------
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
	return kTrue;
}

bool16 Refresh::refreshTaggedBox(UIDRef& boxID, PMString& imagePath)
{
	//CA("1");
	TagReader tReader;
	TagList tList;
	TableUtility tUtility;
	tList=tReader.getFrameTags(boxID);
	UIDRef tableUIDRef;
	
	if(tList.size()==0 || tList.size()>1)
		return kFalse;
		//CA("2");
	if(tList[0].parentId==-1)
		return kFalse;
		//CA("3");
 
	if(!shouldRefresh(tList[0], boxID.GetUID(), kTrue))
		return kFalse;
	//CA("4");
	PMString theData;

	if(tList[0].whichTab!=4)//For copy items
	{
			//CA("5");
		/*** Following is Ritesh's code for Table refresh ***
		getDataFromDB(theData, tList[0], tList[0].parentId); // parentid contains objectid
		if(tUtility.isTablePresent(boxID, tableUIDRef))
		{
			vector< vector<PMString> > tableText;

			int numCols=parseTheText(theData, tableText);

			tUtility.resizeTable(tableUIDRef, tableText.size(), numCols);
3
			for(int j=0; j<tableText.size(); j++)
			{
				for(int k=0; k<numCols; k++)
				{
					if(tableText[j].size()<=k)
						tUtility.setTableRowColData(tableUIDRef, PMString(" "), j, k);
					else
						tUtility.setTableRowColData(tableUIDRef, tableText[j][k], j, k);
				}
			}
		}
		else
			setTextInBox(boxID, theData, textAction::OverWrite);//Later changes here
		
		*********** My Changes for Table refresh start from here ****************/
		/* Check if table exists */
		if(tUtility.isTablePresent(boxID, tableUIDRef))
		{
				//CA("6");
			int32 tableId=0;

			// CA("Table exists");
			// resize table to original nos of columns and rows
			tUtility.fillDataInTable(tableUIDRef, tList[0].parentId, tList[0].typeId, tableId, tList[0].imgFlag);
		}
		/******************************** My Changes end here **********************************/
	}
	/*
	else
	{
		vector<int32> itemIDList;
		int numRows=getAllItemIDs(tList[0].parentId, tList[0].reserved1, itemIDList);
		if(numRows==0)
		{
			setTextInBox(boxID, PMString(""), textAction::OverWrite);
			return kTrue;
		}

		vector<PMString> rowString;
		for(int i=0; i<numRows; i++)
		{
			PMString tempStr;
			if(!getDataFromDB(tempStr, tList[0], itemIDList[i]))
				return kFalse;
			rowString.push_back(tempStr);
		}
			
		if(tUtility.isTablePresent(boxID, tableUIDRef))
		{
			CA("Else...table present");
			PMString tempStr;
			tUtility.resizeTable(tableUIDRef, 1, 1);
			for(int j=0; j<numRows; j++)
			{
				if(j!=numRows-1)
					rowString[j].Append("\r");
				tempStr.Append(rowString[j]);
			}
			tUtility.setTableRowColData(tableUIDRef, tempStr, 0, 0);
		}
		else
		{
			for(int j=0; j<numRows; j++)
			{
				if(j!=numRows-1)
					rowString[j].Append("\r");
				if(!j)
					setTextInBox(boxID, rowString[j], textAction::OverWrite);
				else
					setTextInBox(boxID, rowString[j], textAction::AtEnd);
			}
		}
	}*/

	//	CA("7");

	//------------
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
	return kTrue;
}


bool16 Refresh::setTextInBox(UIDRef& boxID, PMString& textToInsert, enum textAction action)
{
	/*InterfacePtr<IPMUnknown> unknown(boxID, IID_IUNKNOWN);
	UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
		return kFalse;

	//Commmented by Sachin sharma on 2/07/07
	/*InterfacePtr<ITextFrame> textFrame(boxID.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	if (textFrame == nil)
		return kFalse;*/
//++++++++++==========================================Added
	InterfacePtr<IMultiColumnTextFrame>multiColumnTextFrame(boxID.GetDataBase(), textFrameUID, UseDefaultIID());
	if(multiColumnTextFrame==nil)
	{
		ASSERT(multiColumnTextFrame);
		return kFalse;
	}
//++============+++++++++++++++++++++++
	
	InterfacePtr<ITextModel> textModel(multiColumnTextFrame->QueryTextModel());
	if (textModel == nil)
		return kFalse;

	TextIndex startIndex = multiColumnTextFrame->TextStart();
	TextIndex finishIndex = startIndex + textModel->GetPrimaryStoryThreadSpan()-1;


	InterfacePtr<ITextFocusManager> textFocusManager(textModel, UseDefaultIID());
	if (textFocusManager == nil)
		return kFalse;

	InterfacePtr<ITextFocus> frameTextFocus(textFocusManager->NewFocus(RangeData(startIndex, finishIndex, RangeData::kLeanForward)));
	if (frameTextFocus == nil)
		return kFalse;
	
	WideString* myText=new WideString(textToInsert);

	if(action==2)                //if(action==textAction::OverWrite)
	{
		int32 len=finishIndex - startIndex;
		//ErrorCode Err = textModel->Replace(kTrue,startIndex, finishIndex, myText);
		ErrorCode Err = textModel->Replace(startIndex,len,myText,nil,kTrue);//Added

		/*
		InterfacePtr<ICommand> pInsertTextCommand(textModel->ReplaceCmd(startIndex, finishIndex, myText, kFalse, nil));
		if (pInsertTextCommand ==nil )
			return kFalse;

		if (CmdUtils::ProcessCommand(pInsertTextCommand)!=kSuccess )
			return kFalse;
		*/
	}
	else if(action==0)					//(action==textAction::AtEnd)
	{
		//textModel->Insert(kTrue,finishIndex, myText);
		textModel->Insert(finishIndex, myText,nil);
		
/*		InterfacePtr<ICommand> pInsertTextCommand(textModel->InsertCmd(finishIndex, myText, kFalse, nil));
		if (pInsertTextCommand ==nil )
			return kFalse;

		if (CmdUtils::ProcessCommand(pInsertTextCommand)!=kSuccess )
			return kFalse;*/
	}
	else
		return kFalse;//Will implement later if required
	
	if(myText)
		delete myText;

	return kTrue;
}

////////////////////////////////[ From old refresh ]//////////////////////////////////////


bool16 fileExists(PMString& path, PMString& name)
{
	PMString theName("");
	theName.Append(path);
	theName.Append(name);

	const char *file=(theName.GrabCString()/*GetPlatformString().c_str()*/);  //cs4

	FILE *fp=NULL;

	fp=std::fopen(file, "r");
	if(fp!=NULL)
	{
		std::fclose(fp);
		return kTrue;
	}
	return kFalse;
}

bool16 Refresh::doesExist(IIDXMLElement * ptr)
{
	TagReader tReader;
	IIDXMLElement *xmlPtr;
	
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		TagList Newtaglist = tReader.getTagsFromBox(selectUIDList.GetRef(i), &xmlPtr);
		if(ptr==xmlPtr)
			return kTrue;

		//------------
		for(int32 tagIndex = 0 ; tagIndex < Newtaglist.size() ; tagIndex++)
		{
			Newtaglist[tagIndex].tagPtr->Release();
		}		
	}
	return kFalse;
}


bool16 Refresh::getAllBoxIds(void)
{
	TagReader tReader;
/*	InterfacePtr<ISelection> selection(::QuerySelection());
	if(selection == nil)
		return kFalse;

	scoped_ptr<UIDList> _selectUIDList(selection->CreateUIDList());
	if(_selectUIDList==nil)
		return kFalse;

	const int32 listLength=_selectUIDList->Length();
*/	
	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());

	if(!iSelectionManager)
	{	
		return 0;
	}
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(!txtMisSuite)
	{
		//CA("retun");
		
		return 0; 
	}

//	UIDList	selectUIDList;
	txtMisSuite->GetUidList(selectUIDList);

	const int32 listLength=selectUIDList.Length();
	PMString len;
	len.Append("Length=");
	len.AppendNumber(listLength);
	
	if(listLength==0)
		return kFalse;
	
//	selectUIDList=*(_selectUIDList);

	for(int i=0; i<selectUIDList.Length(); i++)
	{
		//InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		//if(!iHier)
		//	continue;

		//UID kidUID;

		//int32 numKids=iHier->GetChildCount();
		//for(int j=0;j<numKids;j++)
		//{
			IIDXMLElement* ptr;
			//kidUID=iHier->GetChildUID(j);
			//UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
			TagList Newtaglist = tReader.getTagsFromBox(selectUIDList.GetRef(i), &ptr);
			//if(!doesExist(ptr))
			//	selectUIDList.Append(kidUID);
		//}
			//------------
			for(int32 tagIndex = 0 ; tagIndex < Newtaglist.size() ; tagIndex++)
			{
				Newtaglist[tagIndex].tagPtr->Release();
			}
	}
	return kTrue;
}

void Refresh::showTagInfo(UIDRef boxRef)
{
	TagList tList;
	TagReader tReader;
	tList=tReader.getTagsFromBox(boxRef);
	PMString allInfo;
	for(int numTags=0; numTags<tList.size(); numTags++)
	{
		allInfo.Clear();
		allInfo.AppendNumber(tList[numTags].elementId);
		allInfo+="\n";
		allInfo.AppendNumber(tList[numTags].parentId);
		allInfo+="\n";
		allInfo.AppendNumber(tList[numTags].typeId);
		allInfo+="\n";
		allInfo.AppendNumber(tList[numTags].imgFlag);
		allInfo+="\n";
		allInfo+="Start and end index :";
		allInfo.AppendNumber(tList[numTags].startIndex);
		allInfo+=" / ";
		allInfo.AppendNumber(tList[numTags].endIndex);
		CAlert::InformationAlert(allInfo);
	}
	//------------
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
}

bool16 Refresh::shouldRefresh(const TagStruct& tInfo, const UID& boxID, bool16 isTaggedFrame)
{	
	for(int i=0; i<rDataList.size(); i++)
	{ 
		if(/*boxID==rDataList[i].boxID && rDataList[i].isSelected && */(!rDataList[i].isObject || rDataList[i].isTableFlag==kTrue))
		{	
			if(!isTaggedFrame)
			{  
				if(rDataList[i].elementID==tInfo.elementId && rDataList[i].objectID==tInfo.parentId)// && rDataList[i].publicationID==tInfo.reserved2)//Later make changes here
				{	
					return kTrue;
				}
			}
			else
				if(rDataList[i].objectID==tInfo.parentId && rDataList[i].TypeID ==tInfo.typeId )// && rDataList[i].publicationID==tInfo.reserved2)//Later make changes here
				{	
					return kTrue;
				}
				
		}
	}
	
	return kFalse;
}

void Refresh::doRefresh(int p)
{
//	CA("dorefresh 1");
	PMString imagePath;
	
	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions==nil)
	{
		//CAlert::ErrorAlert("Interface for IClientOptions not found.");
		return;
	}
	//CA("dorefresh 2");
	imagePath=ptrIClientOptions->getImageDownloadPath();
	if(imagePath!="")
	{
		const char *imageP=(imagePath.GrabCString()/*GetPlatformString().c_str()*/); //Cs4
		if(imageP[std::strlen(imageP)-1]!='\\' || imageP[std::strlen(imageP)-1]!=':')
			#ifdef MACINTOSH
				imagePath+=":";
			#else
				imagePath+="\\";
			#endif
	}
	//CA("dorefresh 3");
	selectUIDList.Clear();
	//CA("dorefresh 4");
//	ChangedTextList.clear();
//	int i;
//	switch(option)
//	{
//	case 1:
//		  {
//			//for current page option
//			UID pageUID;
//			TagReader tReader;
//			int32 pageNumber= 0;
//			if(!this->isValidPageNumber(pageNumber, pageUID))
//				break; 
//				IDocument* document = ::GetFrontDocument(); 
//			IDataBase* database= ::GetDataBase(document); 
//						
//			InterfacePtr<ISpreadList> spreadList(document, IID_ISPREADLIST); 
//			int32 spreadNo = -1; 
//
//			for(int32 p=0; p<spreadList->GetSpreadCount(); ++p) 
//			{ 
//				UIDRef spreadRef(database, spreadList->GetNthSpreadUID(p)); 
//				InterfacePtr<ISpread> spread(spreadRef, IID_ISPREAD); 
//				if(spread->GetPageIndex(pageUID)!= -1) 
//				{			
//					spreadNo = p; 
//					break; 
//				}
//			}
//
//			if(spreadNo==-1)
//				break; 
//
//			InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
//			if (layoutData == nil)
//				break; 
//			 pageUID = layoutData->GetPage();
//			if(pageUID == kInvalidUID)
//			break; 
//
//			IGeometry* spreadItem = layoutData->GetSpread();
//			if (spreadItem == nil)
//				break; 
//			
//			InterfacePtr<ISpread> iSpread((IPMUnknown *)spreadItem, UseDefaultIID());
//			if (iSpread == nil)
//				break; 
//
//			UIDList tempList(database);
//			pageNumber=iSpread->GetPageIndex(pageUID);
//			iSpread->GetItemsOnPage(pageNumber, &tempList, kFalse);
//			selectUIDList=tempList;
//
//			for( int p=0; p<selectUIDList.Length(); p++)
//			{	
//				InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(p), UseDefaultIID());
//				if(!iHier)
//					continue;
//
//				UID kidUID;
//				int32 numKids=iHier->GetChildCount();
//				for(int j=0;j<numKids;j++)
//				{
//					IIDXMLElement* ptr;
//					kidUID=iHier->GetChildUID(j);
//					UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
//					tReader.getTagsFromBox(boxRef, &ptr);
//					if(!doesExist(ptr))
//						selectUIDList.Append(kidUID);
//				}
//			}
//
//			for(i=0; i<selectUIDList.Length(); i++)
//			{	
//				UIDRef boxID=selectUIDList.GetRef(i);
//				this->refreshThisBox(boxID, imagePath);
//			}
//			break;
//		  }
//
//
//	case 2:
//        			
//		if(!getAllPageItemsFromPage(Mediator::refreshPageNumber))
//			return;
//		
//		for(i=0; i<selectUIDList.Length(); i++)
//		{	
//			UIDRef boxID=selectUIDList.GetRef(i);
//			this->refreshThisBox(boxID, imagePath);
//		}
//		break;
//	case 3:
//		if(!getAllBoxIds())
//			return;
//		for(i=0; i<selectUIDList.Length(); i++)
//		{
//			UIDRef boxID=selectUIDList.GetRef(i);
//			this->refreshThisBox(boxID, imagePath);
//		}
//		break;
//	case 4:
//	/*		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
//
//			if(!iSelectionManager)
//			{	
//			//	CA("null selection");
//				return;
//			}
//			// Get text selection 
//			InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
//			InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID()); 
//
//			InterfacePtr<ITextFocus> iFocus(pTextTarget->QueryTextFocus()); 
//
///*		ITextFocus * iFocus=Utils<ISelectUtils>()->QueryTextFocus();
//*/
///*		if(!iFocus)
//			return;
//
//		InterfacePtr<ITextModel> textModel(iFocus->QueryModel());
//		if (textModel==nil)
//			return;
//		
//		IFrameList* frameList=textModel->QueryFrameList();
//		if(!frameList)
//			return;
//		UID frameUID=frameList->GetNthFrameUID(0);
//				
//		IDocument *docPtr=::GetFrontDocument();
//		IDataBase* db =::GetDataBase(docPtr);
//		if(db==nil)
//			return;
//
//		UIDRef boxUIDRef(db, frameUID);
//
//		this->refreshThisBox(boxUIDRef, imagePath);
//*/
		/*if(!getDocumentSelectedBoxIds())
			{	CA("getDocumentSelectedBoxIds False");				
				return ;
			}*/

		/*for(i=0; i<selectUIDList.Length(); i++)
		{*/
		//	CA("dorefresh 5");
			UIDRef boxID=rDataList[p].BoxUIDRef ;
			this->refreshThisBox(boxID, imagePath);
		//	CA("dorefresh 6");
		/*}*/
		//break;
	//}
}


void Refresh::fillDataInBox(const UIDRef& curBox, TagStruct& slugInfo, int32 objectId)
{	
	//int32 count[50];
	/*InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
	UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
		return;
	//Commented By Sachin sharma on 3/07/07
	/*InterfacePtr<ITextFrame> textFrame(curBox.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	if (textFrame == nil)
		return;*/
	InterfacePtr<IMultiColumnTextFrame>iMultiColumnTextFrame(curBox.GetDataBase(), textFrameUID, UseDefaultIID());
	if(!iMultiColumnTextFrame)
		return;
	//TextIndex startIndex = textFrame->TextStart();
	TextIndex startIndex = iMultiColumnTextFrame->TextStart();//Added
	//TextIndex finishIndex = startIndex + textFrame->TextSpan()-1;
	TextIndex finishIndex = startIndex + iMultiColumnTextFrame->TextSpan()-1;//Added

	//InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	InterfacePtr<ITextModel> textModel(iMultiColumnTextFrame->QueryTextModel());//Added
	if (textModel == nil)
		return;
	InterfacePtr<ITextFocusManager> textFocusManager(textModel, UseDefaultIID());
	if (textFocusManager == nil)
		return;
	InterfacePtr<ITextFocus> frameTextFocus(textFocusManager->NewFocus(RangeData(startIndex, finishIndex, RangeData::kLeanForward)));
	if (frameTextFocus == nil)
		return;
	int32 tStart1;
	int32 tEnd1;
	TagReader tReader1;
		TagList tList1;
		tList1=tReader1.getTagsFromBox(curBox);
		if(!tList1.size())//The box contains no tags..But it can be the tagged frame!!!!!
		{
			tList1=tReader1.getFrameTags(curBox);
			if(tList1.size()<=0)//It really is not our box
				return ;			
		}
		
		for(int j=0; j<tList1.size(); j++)
		{	
			if((slugInfo.elementId == tList1[j].elementId) && (slugInfo.whichTab==tList1[j].whichTab))	//if(slugInfo.elementId == tList1[j].elementId)
			{	
				tStart1 = tList1[j].startIndex+1;
				tEnd1 = tList1[j].endIndex-1;
				break;
			}
		}
		//------------
		for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
		{
			tList1[tagIndex].tagPtr->Release();
		}
	//int32 tStart1=slugInfo.startIndex;
	//int32 tEnd1=slugInfo.endIndex-1;
	//PMString ZXC("tStart1 : ");
	//ZXC.AppendNumber(tStart1);
	//ZXC.Append("  tEnd1 : ");
	//ZXC.AppendNumber(tEnd1);
	//CA(ZXC);
	
	char *originalString="";
	char *changedString="";
	PMString entireStory("");
	bool16 result1 = kFalse;
	
	result1=this->GetTextstoryFromBox(textModel, tStart1, tEnd1, entireStory);
	changedString	= const_cast<char*>(entireStory.GrabCString()/*GetPlatformString().c_str()*/); //Cs4
	int32 tStart, tEnd;
	TagReader tReader;
	if(!tReader.GetUpdatedTag(slugInfo))
		return;
	tStart=slugInfo.startIndex+1;
	tEnd=slugInfo.endIndex-tStart;
	
	PMString textToInsert("");
	getDataFromDB(textToInsert, slugInfo, objectId);

	originalString	= const_cast<char*>(textToInsert.GrabCString()/*GetPlatformString().c_str()*/); //Cs4
	int32 flag=0;
	
	///////////////////////////rahul
	WideString* myText=new WideString(textToInsert);	
	//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);//Commented By Sachin Sharma on 3/07/07
	
	ErrorCode Err = textModel->Replace(tStart,tEnd,myText,nil,kTrue);//Added

	if(myText)
		delete myText;

	int32 result2 =0;
	result2= textToInsert.Compare(kTrue, entireStory );
	

		TagList tList2;
		tList2=tReader.getTagsFromBox(curBox);
		if(!tList2.size())//The box contains no tags..But it can be the tagged frame!!!!!
		{
			tList2=tReader.getFrameTags(curBox);
			if(tList2.size()<=0)//It really is not our box
				return ;			
		}
		for(int i=0; i<tList2.size(); i++)
		{	
			if((slugInfo.elementId == tList2[i].elementId) && (slugInfo.whichTab==tList2[i].whichTab))//slugInfo.elementId == tList[i].elementId)
			{
				tStart = tList2[i].startIndex+1;
				tEnd = tList2[i].endIndex;
				break;
			}
		}
		PMString GreenColour ("C=75 M=5 Y=100 K=0");
		char * OriginalString= NULL;
		char * NewString= NULL;
		OriginalString = const_cast<char*>(entireStory.GrabCString()/*GetPlatformString().c_str()*/); //Cs4
		NewString= const_cast<char*>(textToInsert.GrabCString()/*GetPlatformString().c_str()*/);  //Cs4
		int32 k1=0;
		PMString ABC("");
		NewTextList.clear();
		int32 Q=tStart;
		int32 Flagg=0;
		for(int32 p=tStart; p<=tEnd; p++)
		{ 			
			TextVector Textvector1;
			if(NewString[k1]== ' '|| p==tEnd)
			{ 
				if(Flagg==0)
				{
					Textvector1.Word.Clear();
					Textvector1.Word.Append(ABC);
					Textvector1.StartIndex=Q;
					Textvector1.EndIndex=Textvector1.StartIndex + k1;
					Q=Textvector1.EndIndex+1;
					NewTextList.push_back(Textvector1);
					ABC.Clear();
					Flagg=1;
				}
				else
				{
					Textvector1.Word.Clear();
					Textvector1.Word.Append(ABC);
					Textvector1.StartIndex=Q;
					Textvector1.EndIndex=tStart+k1;
					Q=Textvector1.EndIndex+1;
					NewTextList.push_back(Textvector1);
					ABC.Clear();
				}
			}
			else
			{
				ABC.Append(NewString[k1]);
			}
			k1++;
		}
		Flagg=0;
		ABC.Clear();
		k1=0;
		OriginalTextList.clear();
		Q=tStart1;
		for( int32 p=tStart1; p<=tEnd1+1; p++)
		{ 
			TextVector Textvector1;
			if(OriginalString[k1]== ' ' || p==tEnd1+1)
			{ 
				if(Flagg==0)
				{
					Textvector1.Word.Clear();
					Textvector1.Word.Append(ABC);
					Textvector1.StartIndex=Q;
					Textvector1.EndIndex=Textvector1.StartIndex + k1;
					Q=Textvector1.EndIndex+1;
					OriginalTextList.push_back(Textvector1);
					ABC.Clear();
					Flagg=1;
				}
				else
				{
					Textvector1.Word.Clear();
					Textvector1.Word.Append(ABC);
					Textvector1.StartIndex=Q;
					Textvector1.EndIndex=tStart+k1;
					Q=Textvector1.EndIndex+1;
					OriginalTextList.push_back(Textvector1);
					ABC.Clear();
				}
			}
			else
			{
				ABC.Append(OriginalString[k1]);
			}
			k1++;
		}
		
		//------------
		for(int32 tagIndex = 0 ; tagIndex < tList2.size() ; tagIndex++)
		{
			tList2[tagIndex].tagPtr->Release();
		}

	
	if(result2!=0 /*&& HiliteFlag==kTrue*/)
	{	
		RefreshData ColorData;
		ColorData.BoxUIDRef= curBox;
		TagReader tReader;
		TagList tList;
		tList=tReader.getTagsFromBox(curBox);
		if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
		{
			tList=tReader.getFrameTags(curBox);
			if(tList.size()<=0)//It really is not our box
				return ;			
		}
		for(int i=0; i<tList.size(); i++)
		{	
			if((slugInfo.elementId == tList1[i].elementId) && (slugInfo.whichTab==tList1[i].whichTab))//slugInfo.elementId == tList[i].elementId)
			{
				tStart = tList[i].startIndex+1;
				tEnd = tList[i].endIndex;
				break;
			}
		}
		
///////////////////////////////rahul
	
/*	
			
	int32 srt,en,flag2=0,k=0,a,b=0,c=0,tot1,tot2;//srt1,en1,
	char Odoubl[100][500],Cdoubl[100][500];
	srt = tStart1;
	en = tEnd1;
	b=0;
	
	for(a=0;originalString[a];a++)
	{
		if(originalString[a] == ' ' && originalString[a+1] == ' ')
			continue;
		if(originalString[a] == ' ')
		{
			Odoubl[b][c] = '\0';
			b++;
			c=0;
		}
		else
		{
			Odoubl[b][c] = originalString[a];
			c++;
		}
	}
	Odoubl[b][c] = '\0';
	tot1=b;
	b=0;
	c=0;
	for(a=0;changedString[a];a++)
	{
		if(changedString[a] == ' ' && changedString[a+1] == ' ')
			continue;
		if(changedString[a] == ' ')
		{
			Cdoubl[b][c] = '\0';
			b++;
			c=0;
		}
		else
		{
			Cdoubl[b][c] = changedString[a];
			c++;
		}
	}
	Cdoubl[b][c] = '\0';
	tot2=b;
	/////////////////////////////
	for(a=0;a<=tot2;a++)
	{
		PMString ab("");
		ab.Append(Cdoubl[a]);
		//CA(ab);
	}
	int32 m=0,n=0,ind=0;
	int32 Counter=0; 
	if(tot1==tot2)
	{
		for(a=0;a<=tot2;a++)
		{
			if(strcmp(Odoubl[a],Cdoubl[a])!=0)
				{
					PMString abc("");
					abc.Append(Odoubl[a]);
					TextVector Textvector1;
					Textvector1.Word.Clear();
					Textvector1.Word.Append(abc);
					ChangedTextList.push_back(Textvector1);
                    CA(abc);
				}
				
		}
	}
	else
	{
		for(a=0;a<=tot2;a++)
		{
			b=m;
			Counter = 0;
			for(;b<=tot1;b++)
			{
				
				if(strcmp(Odoubl[b],Cdoubl[a])==0)
					{
						m=b+1;
						break;
					}
				else
					{
						flag2 = 0;
						for(c=0;c<=tot2;c++)
						{
							if(strcmp(Odoubl[b],Cdoubl[c])==0)
							{
								flag2 = 1;
							}
						}
						if(flag2 == 0)
						{
							PMString abc("");
							abc.Append(Odoubl[b]);
							TextVector Textvector1;
							Textvector1.Word.Clear();
							Textvector1.Word.Append(abc);
							ChangedTextList.push_back(Textvector1);
							CA(abc);
						}
											
					}
					Counter++;

			}
			
			
		}
	}
	

*/
	// Awasthi Added
		
		int32 tot1=static_cast<int32>(NewTextList.size());
		int32 tot2=static_cast<int32>(OriginalTextList.size());
		
		int32 m=0, flag2=0;
		if(tot1==tot2)
		{	
			for(int32 a=0; a<tot2; a++)
			{	
				if((NewTextList[a].Word.Compare(kTrue, OriginalTextList[a].Word))!=0)
					{							
						PMString abc("");
						abc.Append(NewTextList[a].Word);
						TextVector Textvector1;
						Textvector1.Word.Clear();
						Textvector1.Word.Append(abc);
						Textvector1.BoxUIDRef= curBox;
						Textvector1.StartIndex= NewTextList[a].StartIndex;
						Textvector1.EndIndex= NewTextList[a].EndIndex;
						ChangedTextList.push_back(Textvector1);		
					}					
			}
		}
		else
		{
			for(int32 a=0;a<tot2;a++)
			{
				int32 b=m;
				
				for(;b<tot1;b++)
				{					
					if((NewTextList[b].Word.Compare(kTrue, OriginalTextList[a].Word)==0))//(strcmp(Odoubl[b],Cdoubl[a])==0)
						{
							m=b+1;
							break;
						}
					else
						{
							flag2 = 0;
							for(int32 c=0;c<tot2;c++)
							{
								if(((NewTextList[b].Word.Compare(kTrue, OriginalTextList[c].Word))==0))//(strcmp(Odoubl[b],Cdoubl[c])==0)
								{
									flag2 = 1;
								}
							}
							if(flag2 == 0)
							{									
								PMString abc("");
								abc.Append(NewTextList[b].Word);
								TextVector Textvector1;
								Textvector1.Word.Clear();
								Textvector1.Word.Append(abc);
								Textvector1.BoxUIDRef= curBox;
								Textvector1.StartIndex= NewTextList[b].StartIndex;
								Textvector1.EndIndex= NewTextList[b].EndIndex;
								ChangedTextList.push_back(Textvector1);
							}												
						}
					}				
				}
			}

			if(tot1> tot2)
			{
				for (int C = tot2; C< tot1; C++)
				{
					PMString abc("");
					abc.Append(NewTextList[C].Word);
					TextVector Textvector1;
					Textvector1.Word.Clear();
					Textvector1.Word.Append(abc);
					Textvector1.BoxUIDRef= curBox;
					Textvector1.StartIndex= NewTextList[C].StartIndex;
					Textvector1.EndIndex= NewTextList[C].EndIndex;
					ChangedTextList.push_back(Textvector1);
				}
			}
			// Commented By Rahul ... no more needed now
				//for(int32 s=0; s<ChangedTextList.size();s++)
				//{	
				//	ColorData.elementID=slugInfo.elementId;
				//	ColorData.StartIndex=ChangedTextList[s].StartIndex;
				//	ColorData.EndIndex=ChangedTextList[s].EndIndex; 
				//	//this->ChangeColorOfText(textModel, ChangedTextList[s].StartIndex, ChangedTextList[s].EndIndex, GreenColour);
				//	this->SetFocusForText(curBox,ChangedTextList[s].StartIndex, ChangedTextList[s].EndIndex); 
				//	ColorDataList.push_back(ColorData);
				//}
			
		//------------
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	}
	
}



bool16 Refresh::getDataFromDB(PMString& textToInsert, TagStruct& slugInfo, int32 objectId)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;

	char* tempText=NULL;

	do
	{
		CObjectValue oVal;
		if(slugInfo.elementId==-1 && slugInfo.typeId==-1)//NAME eg pfname/pgname/prname etc.
		{
			PMString myTemp;
			/*oVal = ptrIAppFramework->getObjectElementValues(objectId);*/
			if(oVal.getRef_id() == -1)
				break;
			myTemp=oVal.getName();
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				textToInsert=myTemp;
				break;
			}
			textToInsert=iConverter->translateString(myTemp);
			break;
		}

		if(slugInfo.elementId==-2 && slugInfo.typeId==-1)//PR Number
		{
			PMString myTemp;
			/*oVal=ptrIAppFramework->getObjectElementValues(objectId);*/
			if(oVal.getRef_id()==-1)
				break;
			myTemp=oVal.get_number();
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				textToInsert=myTemp;
				break;
			}
			textToInsert=iConverter->translateString(myTemp);
			break;
		}

		if(slugInfo.whichTab!=4)
		{
			PMString tempBuffer;

			/*tempBuffer=ptrIAppFramework->getObjectElementValue(objectId ,slugInfo.elementId);*/
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				textToInsert=tempBuffer;
				break;
			}
			textToInsert=iConverter->translateString(tempBuffer);
			break;
		}
			
		/*oVal=ptrIAppFramework->getObjectElementValues(objectId);*/
		
		if(oVal.getRef_id()==-1)// This object may have been deleted
			break;
	
		/*tempText=ptrIAppFramework->PRODMngr_getProductColData(oVal.getRef_id(), slugInfo.elementId, slugInfo.reserved2);
		textToInsert.Append(tempText);
		if(tempText)
			delete []tempText;
		rewrite above commented function in other format ....	
		*/
	}while(0);   
	return kTrue;
}

void Refresh::fillImageInBox(const UIDRef& boxUIDRef, TagStruct& slugInfo, int32 objectId, PMString imagePath)
{
	if(imagePath=="")
		return;

	PMString imagePathWithSubdir=imagePath;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	PMString fileName("");

	/*if(slugInfo.whichTab!=4)
		fileName=ptrIAppFramework->getObjectAssetName(objectId, slugInfo.parentTypeID, slugInfo.typeId);*/
	//else
	{
		CObjectValue oVal;

		/*oVal=ptrIAppFramework->getObjectElementValues(objectId);*/
		
		if(oVal.getRef_id()==-1)//This object may have been deleted
			return;

		/*fileName=ptrIAppFramework->getObjectAssetName(oVal.getRef_id(), slugInfo.parentTypeID, slugInfo.typeId);*/
	}
	
	if(fileName=="")
		return;

	/*if(ptrIAppFramework->getSubDirectoryFlag())
	{
		PMString typeCodeString;
		typeCodeString=ptrIAppFramework->TYPECACHE_getTypeCode(slugInfo.typeId);

		if(typeCodeString.NumUTF16TextChars()==0)
			return;

		imagePathWithSubdir.Append(typeCodeString);
		
		#ifdef WINDOWS
			imagePathWithSubdir.Append("\\");
		#endif

	}*/

	
	if(!fileExists(imagePathWithSubdir,fileName))
	{
		int answer=1;
		PMString message("\"");
		message.Append(fileName);
		message.Append("\" was not found in the local drive. Press OK to download the file.");
		answer=CAlert::ModalAlert(message, "OK", "Cancel", "", 1, CAlert::eQuestionIcon);
		if(answer==2)
			return;
		/*if(!ptrIAppFramework->downloadServer(fileName.GrabCString(), imagePathWithSubdir.GrabCString(), slugInfo.typeId))
			return;*/
		return ; // added on 6 feb 
	}

	if(!fileExists(imagePathWithSubdir,fileName))
		return;

	PMString total = imagePathWithSubdir+fileName;

	if(!ImportFileInFrame(boxUIDRef, total))
		CAlert::InformationAlert("Unable to place picture in the box");
	else
		fitImageInBox(boxUIDRef);
}


bool16 Refresh::ImportFileInFrame(const UIDRef& imageBox, const PMString& fromPath)
{
	bool16 fileExists = SDKUtilities::FileExistsForRead(fromPath) == kSuccess;
	if(!fileExists) 
		return kFalse;
	
	IDocument *docPtr=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
	IDataBase* db = ::GetDataBase(docPtr);
	if (db == nil)
		return kFalse;

	IDFile sysFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&fromPath));	
	
	/*InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
	if(!importCmd) 
		return kFalse;
		
	InterfacePtr<IImportFileCmdData> importFileCmdData(importCmd, IID_IIMPORTFILECMDDATA); // no DefaultIID for this
	if(!importFileCmdData)
		return kFalse;
	
	importFileCmdData->Set(db, sysFile, kMinimalUI);
	ErrorCode err = CmdUtils::ProcessCommand(importCmd);
	if(err != kSuccess) 
		return kFalse;

	InterfacePtr<IPlaceGun> placeGun(db, db->GetRootUID(), UseDefaultIID());
	if(!placeGun)
		return kFalse;
	
	//UIDRef placedItem(db, placeGun->GetItemUID());  //Commented By SAchin sharma on 3/07/07
	UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID ());//Added
	InterfacePtr<ICommand> replaceCmd(CmdUtils::CreateCommand(kReplaceCmdBoss));
	if (replaceCmd == nil)
		return kFalse;

	InterfacePtr<IReplaceCmdData>iRepData(replaceCmd, IID_IREPLACECMDDATA);
	if(!iRepData)
		return kFalse;
	
	iRepData->Set(db,imageBox.GetUID(), placedItem.GetUID(),kFalse);

	ErrorCode status = CmdUtils::ProcessCommand(replaceCmd);
	if(status==kFailure)
		return kFalse;
	*/
	
	// Wrap the import in a command sequence.
	
	//----- above code Comment and add new code for CS5-------
	
	PMString seqName("Import");
	CmdUtils::SequenceContext seq(&seqName);

	// If the place gun is already loaded abort it so we can re-load it. 
	InterfacePtr<IPlaceGun> placeGun(docPtr, UseDefaultIID());
	ASSERT(placeGun);
	if (!placeGun) 
	{
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
		return kFalse;
	}
	
	if (Utils<Facade::IImportExportFacade>()->AbortPlaceGun( placeGun, Facade::IImportExportFacade::kAllItems ) != kSuccess) 
	{
		ASSERT_FAIL("AbortPlaceGun failed");
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
		return kFalse;
	}

	// Now import the selected file and load it's UID into the place gun.
	/*IDataBase* db = docUIDRef.GetDataBase();
	ASSERT(db);
	if(!db)
	{
		ErrorUtils::PMSetGlobalErrorCode(kFailure);
		break;
	}*/

	const bool16 retainFormat = kFalse ;
	const bool16 convertQuotes = kFalse ;
	const bool16 applyCJKGrid = kFalse ;
	bool16 useClippingFrame, skipPlace ;
	
	const WideString strURI =(WideString) sysFile.GetString();
	const URI uri(strURI);
	

	ErrorCode err = Utils<Facade::IImportExportFacade>()->ImportAndLoadPlaceGun(db, uri, kMinimalUI, 
		retainFormat, convertQuotes, applyCJKGrid, useClippingFrame, skipPlace ) ;
	ASSERT(err == kSuccess);
	if(err != kSuccess) {
		return kFalse;
	}	
	// Get the contents of the place gun as our return value
	UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());
	//imageBox = placedItem;

	return kTrue;
}



int32 Refresh::getAllItemIDs(int32 parentID, int32 pubID, vector<int32>& itemIDList)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;
	/*if(!ptrIAppFramework->PUBCntrller_getChildrensByObjectId(parentID, pubID, itemIDList))
		return 0;*/
	return static_cast<int32>(itemIDList.size());
}


int32 Refresh::parseTheText(PMString& rowString, vector< vector<PMString> >& dataPtr)
{
	int maxCount=0;
	int tempCount=0;

	dataPtr.clear();

	vector<PMString> stringList;

	stringList.clear();
	tempCount=0;
	PMString hayStack=rowString;

	PMString tempString;
	for(int j=0; j<hayStack.NumUTF16TextChars(); j++)
	{
		if(hayStack[j]=='\t')
		{
			stringList.push_back(tempString);
			tempString.Clear();
			tempCount++;
		}
		else if(hayStack[j]=='\r')
		{
			if(tempString.NumUTF16TextChars())
			{
				stringList.push_back(tempString);
				tempString.Clear();
				tempCount++;
			}
			dataPtr.push_back(stringList);
			if(tempCount>maxCount)
				maxCount=tempCount;
			tempString.Clear();
			stringList.clear();
			tempCount=0;
		}
		else
			tempString.Append(hayStack[j]);
	}

	if(tempString.NumUTF16TextChars())
	{
		stringList.push_back(tempString);
		tempString.Clear();
		tempCount++;
	}

	if(tempCount>maxCount)
		maxCount=tempCount;
	dataPtr.push_back(stringList);

	if(maxCount==0)
		maxCount=1;

	return maxCount;
}


void Refresh::fitImageInBox(const UIDRef& boxUIDRef)
{
	do 
	{
		InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
		if(!iAlignCmd)
			return;

		InterfacePtr<IHierarchy> iHier(boxUIDRef, IID_IHIERARCHY);
		if(!iHier)
			return;

		if(iHier->GetChildCount()==0)//There may not be any image at all????
			return;

		UID childUID=iHier->GetChildUID(0);
		UIDRef newChildUIDRef(boxUIDRef.GetDataBase(), childUID);
		iAlignCmd->SetItemList(UIDList(newChildUIDRef));
		CmdUtils::ProcessCommand(iAlignCmd);
	} while (false);
}

bool16 Refresh::GetTextstoryFromBox(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story)
{	
	TextIterator begin(iModel, startIndex);
	TextIterator end(iModel, finishIndex);
	
	for (TextIterator iter = begin; iter <= end; iter++)
	{	
		//const textchar characterCode = (*iter).GetTextChar();
		char buf;
		//::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
		 UTF16TextChar utfChar[2];
		 int32 len=2;
		 (*iter).ToUTF16(utfChar,&len);
		::CTUnicodeTranslator::Instance()->TextCharToChar(utfChar, 1, &buf, 1); 
		story.Append(buf);
	}	
	return kTrue;
}

void Refresh::ChangeColorOfText(InterfacePtr<ITextModel> textModel1 ,int32 StartText1, int32 EndText1, PMString  altColorSwatch)
{
	do
	{
		/*UIDRef	BoxRefUID1 = rDataList[c].BoxUIDRef; 
		int32 LObjectID1 = rDataList[c].objectID;
		int32 LElementID1 = rDataList[c].elementID;
		int32 StartText1 = rDataList[c].StartIndex;
		int32 EndText1 = rDataList[c].EndIndex;
		
		InterfacePtr<IPMUnknown> unknown(BoxRefUID1, IID_IUNKNOWN);
		UID textFrameUID1 = FrameUtils::GetTextFrameUID(unknown);
		if (textFrameUID1 == kInvalidUID)
			break;

		InterfacePtr<ITextFrame> textFrame1(BoxRefUID1.GetDataBase(), textFrameUID1, ITextFrame::kDefaultIID);
		if (textFrame1 == nil)
			break;


		TextIndex startIndex2 = textFrame1->TextStart();
		
		InterfacePtr<ITextModel> textModel1(textFrame1->QueryTextModel());
		if (textModel1 == nil)
			break;
		*/
		
		InterfacePtr<IWorkspace> workSpace(Utils<ILayoutUIUtils>()->QueryActiveWorkspace());
		InterfacePtr<ISwatchList> swatchList(workSpace,UseDefaultIID());
		if (swatchList == nil)
		{	//CA("swatchList == nil");
			ASSERT_FAIL("Unable to get swatch list from workspace!");
			break;
		}
		
		InterfacePtr<ITextAttrUID>
		textAttrUID(::CreateObject2<ITextAttrUID>(kTextAttrColorBoss));
		UID colorUID;
		UID altColorUID = kInvalidUID;
		
		UIDRef altColorRef = swatchList->FindSwatch (altColorSwatch);
		if (altColorRef.GetUID() == kInvalidUID)
		{
			ASSERT_FAIL("Unable to get alternate swatch in DeleteColor!");
			break;
		}
		else
		{
			altColorUID = altColorRef.GetUID();
		}
		
		
		textAttrUID->Set(altColorUID);
		int32 charCount = EndText1 -StartText1;
		
		InterfacePtr<ICommand> applyCmd(
						Utils<ITextAttrUtils>()->
						BuildApplyTextAttrCmd(textModel1,
						StartText1,
						charCount,
						textAttrUID,
						kCharAttrStrandBoss) );
		ErrorCode err = CmdUtils::ProcessCommand(applyCmd);
		
	}while(0);
}

bool16 Refresh::doesExist(IIDXMLElement * ptr, UIDRef BoxRef)
{
	TagReader tReader;
	IIDXMLElement *xmlPtr;
	TagList tList1;
	tList1=tReader.getTagsFromBox(BoxRef, &xmlPtr);
		if(ptr==xmlPtr)
		{ 
			for(int32 j=0; j<tList1.size(); j++)
			{//CA("Inside check for Parent id");
				if(tList1[j].parentId==-1)
					return kFalse;
			}
			return kTrue;
		}
	//------------
	for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
	{
		tList1[tagIndex].tagPtr->Release();
	}
	return kFalse;
}
bool16 Refresh::doesExist(TagList &tagList)
{
	TagList tList;
	TagReader tReader;
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		tList = tReader.getTagsFromBox(selectUIDList.GetRef(i));

		if(tList.size()==0 || tagList.size()==0 || !tList[0].tagPtr || !tagList[0].tagPtr )
			continue;

		if(tagList[0].tagPtr == tList[0].tagPtr )
		{
			//CA("return kTrue");
			return kTrue;
		}
	}
	//CA("return kFalse");
	return kFalse;
}
/// Function to get elements from Whole Document
bool16 Refresh::getDocumentSelectedBoxIds()
{
   do
   {
	TagReader tReader;
	IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
	if(fntDoc==nil)
		return kFalse;
	InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());//Cs4
	if (layout == nil)
		return kFalse;
	IDataBase* database = ::GetDataBase(fntDoc);
	if(database==nil)
		return kFalse;
	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
	if (iSpreadList==nil)
		return kFalse;
	bool16 collisionFlag=kFalse;
	UIDList allPageItems(database);

	for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
	{
		UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
		if(!spread)
			return -1;
		int numPages=spread->GetNumPages();

		for(int i=0; i<numPages; i++)
		{
			UIDList tempList(database);
			spread->GetItemsOnPage(i, &tempList, kFalse);
			allPageItems.Append(tempList);
		}
	}
	selectUIDList= allPageItems;

		for(int i=0; i<selectUIDList.Length(); i++)
		{	//CA("1");
			//InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
			//if(!iHier)
			//	continue;
			//CA("2");
			//UID kidUID;
			//int32 numKids=iHier->GetChildCount();
			//for(int j=0;j<numKids;j++)
			//{	//CA("3");
				IIDXMLElement* ptr;
			//	kidUID=iHier->GetChildUID(j);
			//	UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
				TagList Newtaglist = tReader.getTagsFromBox(selectUIDList.GetRef(i), &ptr);
				//CA("4");
			//	if(!doesExist(ptr))
			//	{	//CA("111111111111");
			//		selectUIDList.Append(kidUID);
			//	}
				//CA("5");
			//}
				//------------
				for(int32 tagIndex = 0 ; tagIndex < Newtaglist.size() ; tagIndex++)
				{
					Newtaglist[tagIndex].tagPtr->Release();
				}
		}

    }while(0);
	//if(selectUIDList.Length()==0)
	//	{
	//		return kFalse;
	//	}
	return kTrue;
  }


void Refresh::SetFocusForText(const UIDRef &boxUIDRef ,int32 StartText1, int32 EndText1)
{

//	RangeData rData( StartText1, EndText1);	
//	
//	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection());
//	InterfacePtr<ILayoutSelectionSuite>layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//			if (!layoutSelectionSuite) 
//			{	CA("!layoutSelectionSuite");
//				return;
//			}
//
//	IDataBase * idatabase = boxUIDRef.GetDataBase();
//		UID uid = boxUIDRef.GetUID();
//		UIDList itemList(idatabase,uid);	
//
//		
//	layoutSelectionSuite->Select(itemList,Selection::kReplace,Selection::kDontScrollSelection);
//		
//	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//	if(!txtMisSuite)
//	{
//		//CA("retun");
//		
//		return; 
//	}
//
//	UIDList	newUIDList;
//	txtMisSuite->GetUidList(newUIDList);
//
//	InterfacePtr<ITextSelectionSuite>txtSelSuite(Utils<ISelectionUtils>()->QueryTextSelectionSuite(iSelectionManager),UseDefaultIID());
//	if(!txtSelSuite)
//	{ 
//		//CA("Text Selectin suite is null"); 
//		return;
//	}
//
//	InterfacePtr<IConcreteSelection>pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss));
//	// deprecated but universal (CS/2.0.2)
//
//	InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
//
//InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
//		UID textFrameUID = FrameUtils::GetTextFrameUID(unknown);
//		if (textFrameUID == kInvalidUID)
//			return;
//
////	InterfacePtr<ITextFrame>textFrame(/*boxUIDRef*/newUIDList.GetRef(0),UseDefaultIID());
//		InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
//	if(!textFrame)
//	{	CA("!textFrame");
//		return;
//	}
//		InterfacePtr<ITextModel>textModel(textFrame->QueryTextModel());
//		if(!textModel)
//		{   CA("!textModel");
//			return;
//		}	
//	
//	txtSelSuite->SetTextSelection(::GetUIDRef(textModel),rData,Selection::kScrollIntoView,nil);
//
//	pTextTarget->SetTextFocus(pTextTarget->GetTextModel(),rData);
//	CA("1");

	/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
		if (textFrameUID == kInvalidUID)
			return;


		//Commented By Sachin Sharma on 3/07/07
	//InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	//if(!textFrame)
	//{	//CA("!textFrame");
	//	return;
	//}
		//Added
		InterfacePtr<IMultiColumnTextFrame> iMultiColumnTextFrame(boxUIDRef.GetDataBase(), textFrameUID, UseDefaultIID());
		if(!iMultiColumnTextFrame)
		{	//CA("!textFrame");
			ASSERT(iMultiColumnTextFrame);
			return;
		}
		//InterfacePtr<ITextModel>textModel(textFrame->QueryTextModel());
		InterfacePtr<ITextModel>textModel(iMultiColumnTextFrame->QueryTextModel());//Added
		if(!textModel)
		{   //CA("!textModel");
			ASSERT(textModel);
			return;
		}	
//	
//	txtSelSuite->SetTextSelection(::GetUIDRef(textModel),rData,Selection::kScrollIntoView,nil);
//
//	pTextTarget->SetTextFocus(pTextTarget->GetTextModel(),rData);

InterfacePtr<IK2ServiceRegistry> pServiceRegistry(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
if(!pServiceRegistry)
{
	//CA("IK2ServiceRegistry is nil");
	return;
}
InterfacePtr<IK2ServiceProvider> pServiceProvider(pServiceRegistry->QueryServiceProviderByClassID(kTextWalkerService, kTextWalkerServiceProviderBoss));
if(!pServiceProvider)
{
	//CA("IK2ServiceProvider is nil");
	return;
}
		
InterfacePtr<ITextWalker> pWalker(pServiceProvider, UseDefaultIID());
if(!pWalker)
{
	//CA("ITextWalker is nil");
	return;
}
InterfacePtr<ITextWalkerSelectionUtils>pUtils(pWalker,UseDefaultIID());
		
		const TextWalkerSelections_CriticalSection criticalSection(pUtils);

		
if(!pUtils)
{
	//CA("ITextWalkerSelectionUtils is nil");
	return;
}

InterfacePtr<ITextWalkerSelectionUtils> pTextWalkerSelectUtils(pWalker, UseDefaultIID());

if(!pTextWalkerSelectUtils)
{
	//CA("ITextWalkerSelectionUtils is nil");
	return;
}




pTextWalkerSelectUtils->SelectText(::GetUIDRef(textModel), StartText1, EndText1 - StartText1);


}

#include "VCPlugInHeaders.h"
#include "IListControlDataOf.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IListBoxAttributes.h"
#include "IControlView.h"
#include "IApplication.h"
#include "PersistUtils.h"
//#include "PalettePanelUtils.h"   //Commented By Sachin Sharma on 29/06/07
#include "IPalettePanelUtils.h" 
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "CAlert.h"
#include "RsrcSpec.h"
#include "SPID.h"
#include "IListBoxController.h"
#include "SDKListBoxHelper.h"
#include "PublicationNode.h"
#include "MediatorClass.h"
//#include "ISpecialChar.h"
#include "IUIFontSpec.h"
#include "IBooleanControlData.h"
//#include "GlobalData.h"
//#include "ILoggerFile.h"


//extern PublicationNode pNodeDataList;

#define CA(x)	CAlert::InformationAlert(x)

extern PublicationNodeList pNodeDataList; 

SDKListBoxHelper::SDKListBoxHelper(IPMUnknown * owner, int32 pluginId) : fOwner(owner), fOwnerPluginID(pluginId)
{
}

SDKListBoxHelper::~SDKListBoxHelper()
{
	fOwner=nil;
	fOwnerPluginID=0;
}
/* 
Here passing IPanelControlData of desired listbox got in ListboxObserver
*/
IControlView* SDKListBoxHelper ::FindCurrentListBox
(IPanelControlData* iPanelControlData, int i)//Give me the iControlView of the listbox. huh big deal!!
{
	//////CA("inside findcurrentlistbox.");
	
	if(!verifyState())
		return nil;

	IControlView * listBoxControlView2 = nil;

	do {
		if(iPanelControlData ==nil)
		{
			break;
		}
		if(i==1)
		{
			//listBoxControlView2 = iPanelControlData->FindWidget(kSPListBoxWidgetID);
		}
		
		else if(i==2)
		{
			listBoxControlView2 = 
				iPanelControlData->FindWidget(kSDFilterOptionListWidgetID);
		}
		
		
		if(listBoxControlView2 == nil) 
		{
			break;
		}

	} while(0);
	return listBoxControlView2;
}

/* 
Here passing IControlView* of desired listbox returned from FindCurrentListBox()
*/
void SDKListBoxHelper::AddElement
(IControlView* lstboxControlView, PMString & displayName, WidgetID updateWidgetId, int atIndex, int x)
{	
	//CA("Inside addelement");
	if(!verifyState())
		return;
	do	
	{	
/*		IControlView * lstboxControlView = this->FindCurrentListBox(panel, x);
		if(lstboxControlView == nil) 
		{
			CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}
*/
		if(!lstboxControlView)
		{
			//CA("List box control view is nil");
			break;
		}
		InterfacePtr<IListBoxAttributes> listAttr(lstboxControlView, UseDefaultIID());
		if(listAttr == nil){ //CA("listAttr == nil"); 
			break;	}

		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID == 0){ //CA("widgetRsrcID == 0");
		break;	}

		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwnerPluginID, kViewRsrcType, widgetRsrcID);
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(lstboxControlView), elementResSpec, IID_ICONTROLVIEW));
		ASSERT_MSG(newElView != nil, "SDKListBoxHelper::AddElement() Cannot create element");
		if(newElView == nil) {// CA("newElView == nil");
		break;}
		
		this->addListElementWidget(lstboxControlView, newElView, displayName, updateWidgetId, atIndex, x);
	}
	while (false);
}

/*
void SDKListBoxHelper::RemoveElementAt(int indexRemove, int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) 
		{
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveElementAt() Found listbox but not control data?");
		if(listControlData==nil) 
		{
			break;
		}
		if(indexRemove < 0 || indexRemove >= listControlData->Length()) 
		{
			break;
		}
		listControlData->Remove(indexRemove, x);
		removeCellWidget(listBox, indexRemove);
	}
	while (false);
}

void SDKListBoxHelper::RemoveLastElement(int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) 
		{
			break;
		}
		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveLastElement() Found listbox but not control data?");
		if(listControlData==nil) 
		{
			break;
		}
		int lastIndex = listControlData->Length()-1;
		if(lastIndex > 0) 
		{		
			listControlData->Remove(lastIndex, x);
			removeCellWidget(listBox, lastIndex);
		}
	}
	while (false);
}

int SDKListBoxHelper::GetElementCount(int x) 
{
	int retval=0;
	do {
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) {
			break;
		}

		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::GetElementCount() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		retval = listControlData->Length();
	} while(0);
	
	return retval;
}

void SDKListBoxHelper::removeCellWidget(IControlView * listBox, int removeIndex) {
	
	do {
		if(listBox==nil) break;

		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) 
		{
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) 
		{
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"SDKListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) 
		{
			break;
		}

		if(removeIndex < 0 || removeIndex >= cellPanelData->Length()) 
		{
			break;
		}
		cellPanelData->RemoveWidget(removeIndex);
	} while(0);
}
*/
/* 
Here passing IControlView* of desired listbox returned from FindCurrentListBox()
*/
void SDKListBoxHelper::addListElementWidget
(IControlView* lstboxControlView, InterfacePtr<IControlView> & elView,PMString & displayName, WidgetID updateWidgetId, int atIndex, int x)
{

	//CA("Inside addlistelementwidget*********");
//	IControlView * listbox = this->FindCurrentListBox(panel, x);
	if(elView == nil || lstboxControlView == nil ) 
		return;

	do {
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if (newElPanelData == nil) 
			break;
		//IControlView* nameTextView = newElPanelData->FindWidget(updateWidgetId); //og
		InterfacePtr<IControlView> nameTextView(newElPanelData->FindWidget(updateWidgetId), UseDefaultIID());
		if ( (nameTextView == nil)  ) 
			break;

		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == nil) 
			break;
		
		// Awasthi
		/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		break;
		PMString stringToInsert("");
		stringToInsert.Append(iConverter->handleAmpersandCase(displayName));*/
		// End Awasthi

		PMString stringToInsert("");
		stringToInsert.Append(displayName);

		//CA("SSSSSSSSSSSSSSSSSSSSS");
		stringToInsert.SetTranslatable(kFalse);
        stringToInsert.ParseForEmbeddedCharacters();
		newEltext->SetString(stringToInsert, kTrue, kTrue);
				
		InterfacePtr<IPanelControlData> panelData(lstboxControlView,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::addListElementWidget() Cannot get panelData");
		if(panelData == nil) 
			break;

		//IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID); //og
		InterfacePtr<IControlView> cellControlView(panelData->FindWidget(kCellPanelWidgetID), UseDefaultIID());
		
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::addListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) 
			break;

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "SDKListBoxHelper::addListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) 
			break;
		cellPanelData->AddWidget(elView);				

		InterfacePtr< IListControlDataOf<IControlView*> > listData(lstboxControlView, UseDefaultIID());		
		if(listData == nil)  
			break;
		listData->Add(elView);	
	} while(0);

	
}

void SDKListBoxHelper::CheckUncheckRow
(IControlView *listboxCntrlView, int32 index, bool checked)
{
	do
	{
//	IControlView * listBox = this->FindCurrentListBox(x);
		if(listboxCntrlView == nil)
		{
			//CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}
		InterfacePtr<IListBoxController> listCntl(listboxCntrlView,IID_ILISTBOXCONTROLLER);		
		if(listCntl == nil) 
			break;
		
		InterfacePtr<IPanelControlData> panelData(listboxCntrlView, UseDefaultIID());
		if (panelData == nil) 
			break;
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
		if(cellControlView == nil)
			break;

		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
		if(cellPanelData == nil)
			break;
		
		IControlView* nameTextView = cellPanelData->GetWidget(index);
		if ( nameTextView == nil ) 
			break;

		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
		if(cellPanelDataChild == nil)
			break;

		int toHidden=0;
		int toShow=0;
		
		if(checked)
		{
			toHidden=0;
			toShow=1;
		}
		else
		{
			toHidden=1;
			toShow=0;
		}
		
		IControlView *childHideView=cellPanelDataChild->GetWidget(toHidden);
		if(childHideView==nil)
			break;

		IControlView *childShowView=cellPanelDataChild->GetWidget(toShow);
		if(childShowView==nil)
			break;

		childHideView->HideView();
		childShowView->ShowView();	
	}while(kFalse);
}

void SDKListBoxHelper::EmptyCurrentListBox
(IPanelControlData* panel, int x)
{
	
	do {
		IControlView* listBoxControlView = this->FindCurrentListBox(panel, x);
		if(listBoxControlView == nil) 
			break;
		InterfacePtr<IListControlData> listData (listBoxControlView, UseDefaultIID());
		if(listData == nil) 
			break;
		InterfacePtr<IPanelControlData> iPanelControlData(listBoxControlView, UseDefaultIID());
		if(iPanelControlData == nil) 
			break;
		IControlView* panelControlView = iPanelControlData->FindWidget(kCellPanelWidgetID);
		if(panelControlView == nil) 
			break;
		InterfacePtr<IPanelControlData> panelData(panelControlView, UseDefaultIID());
		if(panelData == nil) 
			break;
		listData->Clear(kFalse, kFalse);
		panelData->ReleaseAll();
		listBoxControlView->Invalidate();
		if(x==1)
		{
			pNodeDataList.clear(); 
		}
		Mediator md;
		md.setCurrentObjectID(-1);

	} while(0);

}

void SDKListBoxHelper::AddIcons(IControlView *listboxCntrlView, int32 index, int32 Check)
{
	do
	{
		//CA("SDKListBoxHelper::AddIcons");
		if(listboxCntrlView == nil)
		{
			//CAlert::InformationAlert("AddIcons nil");
			break;
		}


		InterfacePtr<IListBoxController> listCntl1(listboxCntrlView,IID_ILISTBOXCONTROLLER);		
		if(listCntl1 == nil){  
			break;
		}
		
		InterfacePtr<IPanelControlData> panelData1(listboxCntrlView, UseDefaultIID());
		if (panelData1 == nil) {  
			break;
		}

		IControlView* cellControlView1 = panelData1->FindWidget(kCellPanelWidgetID);		
		if(cellControlView1 == nil){ 
			break;
		}

		
		InterfacePtr<IPanelControlData> cellPanelData1(cellControlView1, UseDefaultIID());		
		if(cellPanelData1 == nil){ 
			break;
		}

		IControlView* nameTextView1 = cellPanelData1->GetWidget(index);
		if ( nameTextView1== nil ) { 
			break;
		}
	
		InterfacePtr<IPanelControlData> cellPanelDataChild1(nameTextView1, UseDefaultIID());		
		if(cellPanelDataChild1 == nil){  
			break;
		}
		
		IControlView *childTextView=cellPanelDataChild1->GetWidget(0);
		if(childTextView==nil){ 
			break;
		}	
		IControlView *childImageView=cellPanelDataChild1->GetWidget(1);
		if(childImageView==nil){  
			break;
		}
		IControlView *childTableView=cellPanelDataChild1->GetWidget(2);
		if(childTableView==nil){ 
			break;
		}
		if(Check == 0)				//Text
		{	
			childImageView->HideView();
			childTableView->HideView();
			childTextView->ShowView();
		}
		else if(Check == 1)			//Image
		{	
			childTextView->HideView();
			childTableView->HideView();
			childImageView->ShowView();
		}
		else if(Check == 2)			//Table
		{ 
			childTextView->HideView();
			childImageView->HideView();	
			childTableView->ShowView();
		}
			
	}while(kFalse);


}


// from here commented by prabhat  (Use: Use when count total item present)

//void SDKListBoxHelper::SetDesignerAction(IControlView *listboxCntrlView, int32 index, int32 isProduct, int32 DActionNo, int32 isStarred,int32 NewProduct,PMString itemListCount) //***
//{
//	//CA("SetDesignerAction");
//	
//	do
//	{
//		if(listboxCntrlView == nil)
//		{
//			//CAlert::InformationAlert("SetDesignerAction nil");
//			break;
//		}
//
//		
//		InterfacePtr<IListBoxController> listCntl(listboxCntrlView,IID_ILISTBOXCONTROLLER);		
//		if(listCntl == nil) 
//			break;
//		
//		InterfacePtr<IPanelControlData> panelData(listboxCntrlView, UseDefaultIID());
//		if (panelData == nil) 
//			break;
//		
//		//IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID); // og
//		InterfacePtr<IControlView> cellControlView(panelData->FindWidget(kCellPanelWidgetID), UseDefaultIID());
//		if(cellControlView == nil)
//			break;
//		
//		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
//		if(cellPanelData == nil)
//			break;
//		
//		//IControlView* nameTextView = cellPanelData->GetWidget(index); //og
//		InterfacePtr<IControlView> nameTextView(cellPanelData->GetWidget(index), UseDefaultIID());
//		if ( nameTextView == nil ) 
//			break;
//		
//		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
//		if(cellPanelDataChild == nil)
//			break;
//		
//		
//		/*IControlView *TasskUnCompliteView=cellPanelDataChild->GetWidget(0);
//		if(TasskUnCompliteView==nil)
//			break;*/
//
//		/*IControlView *TasskCompliteView=cellPanelDataChild->GetWidget(1);
//		if(TasskCompliteView==nil)
//			break;*/
//		
//
//		//IControlView *BlankIconView=cellPanelDataChild->GetWidget(0); //og
//		InterfacePtr<IControlView> BlankIconView(cellPanelDataChild->GetWidget(0), UseDefaultIID());
//		if(BlankIconView==nil)
//			break;
//		 BlankIconView->HideView();	// added by avinash
//
//
//		//IControlView *UpdateProductIconView=cellPanelDataChild->GetWidget(1); //og
//		InterfacePtr<IControlView> UpdateProductIconView(cellPanelDataChild->GetWidget(1), UseDefaultIID()); //added by avinash
//		if(UpdateProductIconView==nil)
//			break;
//		UpdateProductIconView->HideView(); // added by avinash
//					
//
//		//IControlView *NewProductIconView=cellPanelDataChild->GetWidget(2); //og
//		InterfacePtr<IControlView> NewProductIconView(cellPanelDataChild->GetWidget(2), UseDefaultIID()); //added by avinash
//		if(NewProductIconView==nil)
//			break;
//		NewProductIconView->HideView(); // added by avinash
//
//
//		//IControlView *AddToSpreadIconView=cellPanelDataChild->GetWidget(3); //og
//		InterfacePtr<IControlView> AddToSpreadIconView(cellPanelDataChild->GetWidget(3), UseDefaultIID()); //added by avinash
//		if(AddToSpreadIconView==nil)
//			break;
//		AddToSpreadIconView->HideView(); // added by avinash
//
//		//IControlView *UpdateCopyIcon=cellPanelDataChild->GetWidget(4); //og
//		InterfacePtr<IControlView> UpdateCopyIcon(cellPanelDataChild->GetWidget(4), UseDefaultIID()); //added by avinash
//		if(UpdateCopyIcon==nil)
//			break;
//		UpdateCopyIcon->HideView(); // added by avinash
//
//		//IControlView *UpdateArtIcon=cellPanelDataChild->GetWidget(5); //og 
//		InterfacePtr<IControlView> UpdateArtIcon(cellPanelDataChild->GetWidget(5), UseDefaultIID()); //added by avinash
//		if(UpdateArtIcon==nil)
//			break;
//		UpdateArtIcon->HideView(); // added by avinash
//
//		//IControlView *UpdateItemTableIconView=cellPanelDataChild->GetWidget(6); //og
//		InterfacePtr<IControlView> UpdateItemTableIconView(cellPanelDataChild->GetWidget(6), UseDefaultIID()); //added by avinash
//		if(UpdateItemTableIconView==nil)
//			break;
//		UpdateItemTableIconView->HideView(); // added by avinash
//
//		//IControlView *DeleteFromSpreadIconView=cellPanelDataChild->GetWidget(7); //og
//		InterfacePtr<IControlView> DeleteFromSpreadIconView(cellPanelDataChild->GetWidget(7), UseDefaultIID()); //added by avinash
//		if(DeleteFromSpreadIconView==nil)
//			break;
//		DeleteFromSpreadIconView->HideView(); // added by avinash
//
//
//
//		//added on 11.2.5 NewIcons combinations
//		//IControlView *NewAddIconView=cellPanelDataChild->GetWidget(8);                           //8 //og
//		InterfacePtr<IControlView> NewAddIconView(cellPanelDataChild->GetWidget(8), UseDefaultIID()); //added by avinash
//		if(NewAddIconView==nil)
//			break;
//		NewAddIconView->HideView(); // added by avinash
//
//		//IControlView *NewUpdateIconView=cellPanelDataChild->GetWidget(9);                        //9 // og
//		InterfacePtr<IControlView> NewUpdateIconView(cellPanelDataChild->GetWidget(9), UseDefaultIID()); //added by avinash
//		if(NewUpdateIconView==nil)
//			break;
//		NewUpdateIconView->HideView(); // added by avinash
//
//		//IControlView *NewDeleteIconView=cellPanelDataChild->GetWidget(10);                        //10 // og
//		InterfacePtr<IControlView> NewDeleteIconView(cellPanelDataChild->GetWidget(10), UseDefaultIID()); //added by avinash
//		if(NewDeleteIconView==nil)
//			break;
//		NewDeleteIconView->HideView(); // added by avinash
//
//		//IControlView *CopyArtIconView=cellPanelDataChild->GetWidget(11);                          //11 // og
//		InterfacePtr<IControlView> CopyArtIconView(cellPanelDataChild->GetWidget(11), UseDefaultIID()); //added by avinash
//		if(CopyArtIconView==nil)
//			break;
//		CopyArtIconView->HideView(); // added by avinash
//
//		//IControlView *CopyItemIconView=cellPanelDataChild->GetWidget(12);                         //12 //og
//		InterfacePtr<IControlView> CopyItemIconView(cellPanelDataChild->GetWidget(12), UseDefaultIID()); //added by avinash
//		if(CopyItemIconView==nil)
//			break;
//		CopyItemIconView->HideView(); // added by avinash
//
//		//IControlView *ArtItemIconView=cellPanelDataChild->GetWidget(13);                          //13 //og
//		InterfacePtr<IControlView> ArtItemIconView(cellPanelDataChild->GetWidget(13), UseDefaultIID()); //added by avinash
//		if(ArtItemIconView==nil)
//			break;
//		ArtItemIconView->HideView(); // added by avinash
//
//		//IControlView *CopyArtItemIconView=cellPanelDataChild->GetWidget(14);                      //14 //og
//		InterfacePtr<IControlView> CopyArtItemIconView(cellPanelDataChild->GetWidget(14), UseDefaultIID()); //added by avinash
//		if(CopyArtItemIconView==nil)
//			break;
//		CopyArtItemIconView->HideView(); // added by avinash
////******************************************************************
////CA("16");
//		//IControlView *TableTextIconView=cellPanelDataChild->GetWidget(15);                        //15 //og
//		InterfacePtr<IControlView> TableTextIconView(cellPanelDataChild->GetWidget(15), UseDefaultIID()); //added by avinash
//		if(TableTextIconView==nil)
//			break;
//		TableTextIconView->HideView(); // added by avinash
////CA("17");
//		//IControlView *ProductTextIconView=cellPanelDataChild->GetWidget(16);                     //(16); //og
//		InterfacePtr<IControlView> ProductTextIconView(cellPanelDataChild->GetWidget(16), UseDefaultIID()); //added by avinash
//		if(ProductTextIconView==nil)
//			break;
//		ProductTextIconView->HideView(); // added by avinash
////CA("18");
//		//IControlView *ItemTextIconView=cellPanelDataChild->GetWidget(17);                       //(17); //og
//		InterfacePtr<IControlView> ItemTextIconView(cellPanelDataChild->GetWidget(17), UseDefaultIID());
//		if(ItemTextIconView==nil)
//			break;
//		ItemTextIconView->HideView(); // added by avinash
//
//		//IControlView *LeadingItemTextIconView=cellPanelDataChild->GetWidget(18);                       //(20); //og
//		InterfacePtr<IControlView> LeadingItemTextIconView(cellPanelDataChild->GetWidget(18), UseDefaultIID()); // added by avinash
//		if(LeadingItemTextIconView==nil)
//			break;
//		LeadingItemTextIconView->HideView(); // added by avinash
//
//		//IControlView *SingleItemTextIconView=cellPanelDataChild->GetWidget(19);                       //(20); //og
//		InterfacePtr<IControlView> SingleItemTextIconView(cellPanelDataChild->GetWidget(19), UseDefaultIID()); //added by avinash
//		if(SingleItemTextIconView==nil)
//			break;
//		SingleItemTextIconView->HideView(); // added by avinash
//
//		//IControlView *CountTextIconView=cellPanelDataChild->GetWidget(20);                       //(20); // og
//		InterfacePtr<IControlView> CountTextIconView(cellPanelDataChild->GetWidget(20), UseDefaultIID()); //added by avinash
//		if(CountTextIconView==nil)
//			break;
//		CountTextIconView->HideView(); // added by avinash
//
//		InterfacePtr<ITextControlData> itemCountControlData(CountTextIconView,IID_ITEXTCONTROLDATA);
//		if(itemCountControlData == nil)					
//			break;
//		itemCountControlData->SetString(itemListCount);
//		//NewProductIconView->HideView(); // added by avinash
//		
////CA("19");	
////*************************commented by mane**********************
//		//IControlView *TableIconView=cellPanelDataChild->GetWidget(16);
//		//if(TableIconView==nil)
//		//	break;
//
//		//IControlView *ProductIconView=cellPanelDataChild->GetWidget(17);//(16);
//		//if(ProductIconView==nil)
//		//	break;
//
//		//IControlView *ItemIconView=cellPanelDataChild->GetWidget(18);//(17);
//		//if(ItemIconView==nil)
//		//	break;
////***************************************************	
//
//
//		//IControlView *StarPicturWidgetView=cellPanelDataChild->GetWidget(21);                      //(18); //og
//		InterfacePtr<IControlView> StarPicturWidgetView(cellPanelDataChild->GetWidget(21), UseDefaultIID()); //added by avinash
//		if(StarPicturWidgetView==nil)
//			break;
//		StarPicturWidgetView->HideView(); // added by avinash
//		/////////////	Added by Amit
//		//IControlView *NewProductPicturWidgetView=cellPanelDataChild->GetWidget(22);               //(19); ..og
//		InterfacePtr<IControlView> NewProductPicturWidgetView(cellPanelDataChild->GetWidget(22), UseDefaultIID()); //added by avinash
//		if(NewProductPicturWidgetView==nil)
//		{
//			CA("NewProductPicturWidgetView==nil");
//			break;
//		}
//		NewProductPicturWidgetView->HideView(); // added by avinash
//		/////////////	End
//		InterfacePtr<IBooleanControlData> controlData(StarPicturWidgetView, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
//		if(controlData==nil) {
//			CA("SDKListBoxHelper::SetDesignerAction - No IBooleanControlData found on this boss.");
//			break;
//		}
//	
//
//		InterfacePtr<IUIFontSpec> fontSpec(StarPicturWidgetView, IID_IUIFONTSPEC); // no kDefaultIID
//		if(fontSpec==nil) {
//			//CA("PicIcoPictureWidgetEH::LButtonDn - No IUIFontSpec found on this boss.");
//			break;
//		}
//	
//
//		int32 rsrcID = 0;
//		bool16 isPressedIn = controlData->IsSelected();
//		if(isStarred == 0) // ONEsourceCase where Star will get hide		
//		{
//			//CA("isStarred == 0");
//			StarPicturWidgetView->HideView();
//		}
//		else if(isStarred==2)  // Selectecd Star
//		{//// Showing Green Star Icon
//			StarPicturWidgetView->ShowView();
//			controlData->Select();
//			rsrcID =fontSpec->GetHiliteFontID(); 
//		}else if(isStarred == 1)  // Unselected Star	
//		{// Showing White Star Icon
//			StarPicturWidgetView->ShowView();
//			controlData->Deselect();	
//			rsrcID = fontSpec->GetFontID();
//		}
//		if(rsrcID != 0) {
//			StarPicturWidgetView->SetRsrcID(rsrcID);		
//		}
////////////////		Added by Amit
//		InterfacePtr<IBooleanControlData> controlData1(NewProductPicturWidgetView, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
//		if(controlData1==nil) {
//			//CA("SPPictureWidgetEH::LButtonDn - No controlData1 found on this boss.");
//			break;
//		}
//
//		InterfacePtr<IUIFontSpec> fontSpec1(NewProductPicturWidgetView, IID_IUIFONTSPEC); // no kDefaultIID
//		if(fontSpec1==nil) {
//			CA("PicIcoPictureWidgetEH::LButtonDn - No IUIFontSpec1 found on this boss.");
//			break;
//		}
//
//		int32 rsrcID1 = 0;
//		bool16 isPressedIn1 = controlData1->IsSelected();
//		if(NewProduct == 0) 		
//		{
//			NewProductPicturWidgetView->HideView();
//		}
//		else if(NewProduct==2)  
//		{
//			NewProductPicturWidgetView->ShowView();
//			controlData1->Select();
//			rsrcID1 =fontSpec1->GetHiliteFontID(); 
//		}else if(NewProduct == 1)  // Unselected Star	
//		{
//			NewProductPicturWidgetView->ShowView();
//			controlData1->Deselect();	
//			rsrcID1 = fontSpec1->GetFontID();
//		}
//		if(rsrcID1 != 0) {
//			NewProductPicturWidgetView->SetRsrcID(rsrcID1);		
//		}
//		
////////////////		End
//		//ended on 11.2.5
//		if(isProduct == 2)
//		{
//			//CA("isProduct == 2");
//			//ItemTextIconView->HideView(); //og
//			//ProductTextIconView->HideView(); //og
//			TableTextIconView->ShowView(); //og
//			//LeadingItemTextIconView->HideView(); //og 
//			//SingleItemTextIconView->HideView(); //og
//		}
//
//		if(isProduct == 1)
//		{
//			//CA("isProduct == 1");
//			//TableTextIconView->HideView(); //og
//			//ItemTextIconView->HideView(); //og
//			ProductTextIconView->ShowView(); //og
//			//LeadingItemTextIconView->HideView(); //og
//			//SingleItemTextIconView->HideView(); //og
//		}
//		if(isProduct == 0)
//		{	
//			//CA("isProduct == 0");
//			//TableTextIconView->HideView(); //og
//			//ProductTextIconView->HideView(); //og
//			ItemTextIconView->ShowView(); //og
//			//LeadingItemTextIconView->HideView(); //og
//			//SingleItemTextIconView->HideView(); //og
//		}
//		if(isProduct == 3)	//---For Leading Item
//		{
//			LeadingItemTextIconView->ShowView(); //og
//			//TableTextIconView->HideView(); //og
//			//ProductTextIconView->HideView(); //og 
//			//ItemTextIconView->HideView(); //og
//			//SingleItemTextIconView->HideView(); //og
//		}
//
//		if(isProduct == 4)	//---For Single Item
//		{
//			SingleItemTextIconView->ShowView();
//			//LeadingItemTextIconView->HideView(); //og
//			//TableTextIconView->HideView(); // og
//			//ProductTextIconView->HideView(); // og
//			//ItemTextIconView->HideView();// og
//
//		}
////PMString asd("DActionNo = ");
////asd.AppendNumber(DActionNo);
////CA(asd);
//		switch(DActionNo)
//		{
//
//			case 0://for newproduct checked
//				
//					BlankIconView->ShowView();
//					//UpdateProductIconView->HideView();
//					NewProductIconView->ShowView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//		
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//
//					break;
//
//			case 1:  // For AddProductToSpread checked
//
//					BlankIconView->ShowView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					AddToSpreadIconView->ShowView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//	/*				NewAddIconView->HideView();
//					NewUpdateIconView->HideView();
//					NewDeleteIconView->HideView();
//					CopyArtIconView->HideView();
//					CopyItemIconView->HideView();
//					ArtItemIconView->HideView();
//					CopyArtItemIconView->HideView();*/
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//
//					break;
//			case 2:  // For UpdateSpread and UpdateCopy checked
//
//					//BlankIconView->HideView();
//					UpdateProductIconView->ShowView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					UpdateCopyIcon->ShowView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//
//					break;
//
//			case 3:  // For UpdateSpread and UpdateArtIcon Action
//
//					//BlankIconView->HideView();
//					UpdateProductIconView->ShowView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					UpdateArtIcon->ShowView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//
//					break;
//
//			case 4:  // For UpdateSpread and UpdateItemTableIconView Action
//
//					//BlankIconView->HideView();
//					UpdateProductIconView->ShowView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					UpdateItemTableIconView->ShowView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//
//					break;
//
//			case 5:  // For DeleteFromSpreadIconView Action
//
//					BlankIconView->ShowView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					DeleteFromSpreadIconView->ShowView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//
//					break;
//
//			/*case 6:  // For Refresh Product Action
//
//					PRIconView->HideView();
//					NewProductIconView->HideView();
//					AddToSpreadIconView->HideView();
//					UpdateCopyIcon->HideView();
//					UpdateArtIcon->HideView();
//					UpdateItemTableIconView->HideView();
//					DeleteFromSpreadIconView->ShowView();
//					break;*/
//		//added by Yogesh on 11.2.5	New combinations of Icons.
//			case 7:
//
//					//BlankIconView->HideView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					NewAddIconView->ShowView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//			case 8:
//
//					//BlankIconView->HideView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					UpdateCopyIcon->ShowView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					NewUpdateIconView->ShowView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//			case 9:
//
//					//BlankIconView->HideView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					UpdateArtIcon->ShowView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					NewUpdateIconView->ShowView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//
//			case 10:
//					
//					//BlankIconView->HideView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					UpdateItemTableIconView->ShowView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					NewUpdateIconView->ShowView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//			case 11:
//
//					//BlankIconView->HideView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					NewUpdateIconView->ShowView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					CopyArtItemIconView->ShowView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//
//			case 12:
//
//					//BlankIconView->HideView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					NewUpdateIconView->ShowView();
//					//NewDeleteIconView->HideView();
//					CopyArtIconView->ShowView();
//					//CopyItemIconView->HideView();
//					ArtItemIconView->ShowView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//
//			case 13:
//
//					//BlankIconView->HideView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					NewUpdateIconView->ShowView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					ArtItemIconView->ShowView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//
//				case 14:
//
//					//BlankIconView->HideView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					NewUpdateIconView->ShowView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					CopyItemIconView->ShowView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//
//
//				case 15:
//
//					//BlankIconView->HideView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					NewDeleteIconView->ShowView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//
//				case 16:
//
//					//BlankIconView->HideView();
//					UpdateProductIconView->ShowView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					CopyArtItemIconView->ShowView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//
//				case 17:
////CA("Case 17");
//					//BlankIconView->HideView();
//					UpdateProductIconView->ShowView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					CopyArtIconView->ShowView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//
//			case 18:
//
//					//BlankIconView->HideView();
//					UpdateProductIconView->ShowView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					ArtItemIconView->ShowView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//
//			case 19:
//
//					//BlankIconView->HideView();
//					UpdateProductIconView->ShowView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					CopyItemIconView->ShowView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//
//			case 20:
//
//					//BlankIconView->HideView();
//					UpdateProductIconView->ShowView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					break;
//
//			case 21:
//					//CA("Inside case 21");
//					//BlankIconView->HideView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//					//added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					NewUpdateIconView->ShowView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//				break;
//
//		//ended by Yogesh on 11.2.5 New combinations of Icosn
//			default:
//
//					// og
//					//BlankIconView->HideView();
//					//UpdateProductIconView->HideView();
//					//NewProductIconView->HideView();
//					//AddToSpreadIconView->HideView();
//					//UpdateCopyIcon->HideView();
//					//UpdateArtIcon->HideView();
//					//UpdateItemTableIconView->HideView();
//					//DeleteFromSpreadIconView->HideView();
//
//
//					////added by Yogesh on 11.2.5 New combinations of Icons
//					//NewAddIconView->HideView();
//					//NewUpdateIconView->HideView();
//					//NewDeleteIconView->HideView();
//					//CopyArtIconView->HideView();
//					//CopyItemIconView->HideView();
//					//ArtItemIconView->HideView();
//					//CopyArtItemIconView->HideView();
//					//ended by Yogesh on 11.2.5 New combinations of Icons
//
//					//till here
//					break;
//					
//		}
//	}while(0);
//}

// till here commmented by prabhat





// added by prabhat

// Added by prabhat for count total item into content sprayer
void SDKListBoxHelper::SetDesignerAction(IControlView *listboxCntrlView, int32 index, int32 isProduct, int32 DActionNo, int32 isStarred,int32 NewProduct,PMString itemListCount) //***
{
	//CA("SetDesignerAction");
	
	do
	{
		if(listboxCntrlView == nil)
		{
			//CAlert::InformationAlert("SetDesignerAction nil");
			break;
		}

		
		InterfacePtr<IListBoxController> listCntl(listboxCntrlView,IID_ILISTBOXCONTROLLER);		
		if(listCntl == nil) 
			break;
		
		InterfacePtr<IPanelControlData> panelData(listboxCntrlView, UseDefaultIID());
		if (panelData == nil) 
			break;
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
		if(cellControlView == nil)
			break;
		
		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
		if(cellPanelData == nil)
			break;
		
		IControlView* nameTextView = cellPanelData->GetWidget(index);
		if ( nameTextView == nil ) 
			break;
		
		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
		if(cellPanelDataChild == nil)
			break;
		
		
		/*IControlView *TasskUnCompliteView=cellPanelDataChild->GetWidget(0);
		if(TasskUnCompliteView==nil)
			break;*/

		/*IControlView *TasskCompliteView=cellPanelDataChild->GetWidget(1);
		if(TasskCompliteView==nil)
			break;*/
		

		IControlView *BlankIconView=cellPanelDataChild->GetWidget(0);
		if(BlankIconView==nil)
			break;

		IControlView *UpdateProductIconView=cellPanelDataChild->GetWidget(1);
		if(UpdateProductIconView==nil)
			break;

		IControlView *NewProductIconView=cellPanelDataChild->GetWidget(2);
		if(NewProductIconView==nil)
			break;

		IControlView *AddToSpreadIconView=cellPanelDataChild->GetWidget(3);
		if(AddToSpreadIconView==nil)
			break;

		IControlView *UpdateCopyIcon=cellPanelDataChild->GetWidget(4);
		if(UpdateCopyIcon==nil)
			break;

		IControlView *UpdateArtIcon=cellPanelDataChild->GetWidget(5);
		if(UpdateArtIcon==nil)
			break;

		IControlView *UpdateItemTableIconView=cellPanelDataChild->GetWidget(6);
		if(UpdateItemTableIconView==nil)
			break;

		IControlView *DeleteFromSpreadIconView=cellPanelDataChild->GetWidget(7);
		if(DeleteFromSpreadIconView==nil)
			break;



		//added on 11.2.5 NewIcons combinations
		IControlView *NewAddIconView=cellPanelDataChild->GetWidget(8);                            //8
		
		if(NewAddIconView==nil)
			break;

		IControlView *NewUpdateIconView=cellPanelDataChild->GetWidget(9);                        //9
		if(NewUpdateIconView==nil)
			break;

		IControlView *NewDeleteIconView=cellPanelDataChild->GetWidget(10);                        //10
		if(NewDeleteIconView==nil)
			break;

		IControlView *CopyArtIconView=cellPanelDataChild->GetWidget(11);                          //11
		if(CopyArtIconView==nil)
			break;

		IControlView *CopyItemIconView=cellPanelDataChild->GetWidget(12);                         //12
		if(CopyItemIconView==nil)
			break;

		IControlView *ArtItemIconView=cellPanelDataChild->GetWidget(13);                          //13
		if(ArtItemIconView==nil)
			break;

		IControlView *CopyArtItemIconView=cellPanelDataChild->GetWidget(14);                      //14
		if(CopyArtItemIconView==nil)
			break;
//******************************************************************
//CA("16");
		IControlView *TableTextIconView=cellPanelDataChild->GetWidget(15);                        //15
		if(TableTextIconView==nil)
			break;
//CA("17");
		IControlView *ProductTextIconView=cellPanelDataChild->GetWidget(16);                     //(16);
		if(ProductTextIconView==nil)
			break;
//CA("18");
		IControlView *ItemTextIconView=cellPanelDataChild->GetWidget(17);                       //(17);
		if(ItemTextIconView==nil)
			break;

		IControlView *LeadingItemTextIconView=cellPanelDataChild->GetWidget(18);                       //(20);
		if(LeadingItemTextIconView==nil)
			break;

		IControlView *SingleItemTextIconView=cellPanelDataChild->GetWidget(19);                       //(20);
		if(SingleItemTextIconView==nil)
			break;

		IControlView *CountTextIconView=cellPanelDataChild->GetWidget(20);                       //(20);
		if(CountTextIconView==nil)
			break;

		InterfacePtr<ITextControlData> itemCountControlData(CountTextIconView,IID_ITEXTCONTROLDATA);
		if(itemCountControlData == nil)					
			break;
        itemListCount.ParseForEmbeddedCharacters();
		itemCountControlData->SetString(itemListCount);
		
//CA("19");	
//*************************commented by mane**********************
		//IControlView *TableIconView=cellPanelDataChild->GetWidget(16);
		//if(TableIconView==nil)
		//	break;

		//IControlView *ProductIconView=cellPanelDataChild->GetWidget(17);//(16);
		//if(ProductIconView==nil)
		//	break;

		//IControlView *ItemIconView=cellPanelDataChild->GetWidget(18);//(17);
		//if(ItemIconView==nil)
		//	break;
//***************************************************		
		IControlView *StarPicturWidgetView=cellPanelDataChild->GetWidget(21);                      //(18);
		if(StarPicturWidgetView==nil)
			break;
		/////////////	Added by Amit
		IControlView *NewProductPicturWidgetView=cellPanelDataChild->GetWidget(22);               //(19);
		if(NewProductPicturWidgetView==nil)
		{
			CA("NewProductPicturWidgetView==nil");
			break;
		}
		/////////////	End
		InterfacePtr<IBooleanControlData> controlData(StarPicturWidgetView, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
		if(controlData==nil) {
			CA("SDKListBoxHelper::SetDesignerAction - No IBooleanControlData found on this boss.");
			break;
		}

		InterfacePtr<IUIFontSpec> fontSpec(StarPicturWidgetView, IID_IUIFONTSPEC); // no kDefaultIID
		if(fontSpec==nil) {
			//CA("PicIcoPictureWidgetEH::LButtonDn - No IUIFontSpec found on this boss.");
			break;
		}

		int32 rsrcID = 0;
		bool16 isPressedIn = controlData->IsSelected();
		if(isStarred == 0) // ONEsourceCase where Star will get hide		
		{
			StarPicturWidgetView->HideView();
		}
		else if(isStarred==2)  // Selectecd Star
		{//// Showing Green Star Icon	
			StarPicturWidgetView->ShowView();
			controlData->Select();
			rsrcID =fontSpec->GetHiliteFontID(); 
		}else if(isStarred == 1)  // Unselected Star	
		{// Showing White Star Icon
			StarPicturWidgetView->ShowView();
			controlData->Deselect();	
			rsrcID = fontSpec->GetFontID();
		}
		if(rsrcID != 0) {
			StarPicturWidgetView->SetRsrcID(rsrcID);		
		}
//////////////		Added by Amit
		InterfacePtr<IBooleanControlData> controlData1(NewProductPicturWidgetView, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
		if(controlData1==nil) {
			//CA("SPPictureWidgetEH::LButtonDn - No controlData1 found on this boss.");
			break;
		}

		InterfacePtr<IUIFontSpec> fontSpec1(NewProductPicturWidgetView, IID_IUIFONTSPEC); // no kDefaultIID
		if(fontSpec1==nil) {
			CA("PicIcoPictureWidgetEH::LButtonDn - No IUIFontSpec1 found on this boss.");
			break;
		}

		int32 rsrcID1 = 0;
		//bool16 isPressedIn1 = controlData1->IsSelected();
		if(NewProduct == 0) 		
		{
			NewProductPicturWidgetView->HideView();
		}
		else if(NewProduct==2)  
		{
			NewProductPicturWidgetView->ShowView();
			controlData1->Select();
			rsrcID1 =fontSpec1->GetHiliteFontID(); 
		}else if(NewProduct == 1)  // Unselected Star	
		{
			NewProductPicturWidgetView->ShowView();
			controlData1->Deselect();	
			rsrcID1 = fontSpec1->GetFontID();
		}
		if(rsrcID1 != 0) {
			NewProductPicturWidgetView->SetRsrcID(rsrcID1);		
		}
		
//////////////		End
		//ended on 11.2.5
		if(isProduct == 2)
		{
			//CA("isProduct == 2");
			ItemTextIconView->HideView();
			ProductTextIconView->HideView();
			TableTextIconView->ShowView();
			LeadingItemTextIconView->HideView();
			SingleItemTextIconView->HideView();
		}

		if(isProduct == 1)
		{
			//CA("isProduct == 1");
			TableTextIconView->HideView();
			ItemTextIconView->HideView();
			ProductTextIconView->ShowView();
			LeadingItemTextIconView->HideView();
			SingleItemTextIconView->HideView();
		}
		if(isProduct == 0)
		{	
			//CA("isProduct == 0");
			TableTextIconView->HideView();
			ProductTextIconView->HideView();
			ItemTextIconView->ShowView();
			LeadingItemTextIconView->HideView();
			SingleItemTextIconView->HideView();
		}
		if(isProduct == 3)	//---For Leading Item
		{
			LeadingItemTextIconView->ShowView();
			TableTextIconView->HideView();
			ProductTextIconView->HideView();
			ItemTextIconView->HideView();
			SingleItemTextIconView->HideView();
		}

		if(isProduct == 4)	//---For Single Item
		{
			SingleItemTextIconView->ShowView();
			LeadingItemTextIconView->HideView();
			TableTextIconView->HideView();
			ProductTextIconView->HideView();
			ItemTextIconView->HideView();

		}
//PMString asd("DActionNo = ");
//asd.AppendNumber(DActionNo);
//CA(asd);
		switch(DActionNo)
		{

			case 0://for newproduct checked
				
					BlankIconView->ShowView();
					UpdateProductIconView->HideView();
					NewProductIconView->ShowView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();
		
					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;

			case 1:  // For AddProductToSpread checked

					BlankIconView->ShowView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->ShowView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();


					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;
			case 2:  // For UpdateSpread and UpdateCopy checked

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->ShowView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();


					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;

			case 3:  // For UpdateSpread and UpdateArtIcon Action

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->ShowView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;

			case 4:  // For UpdateSpread and UpdateItemTableIconView Action

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->ShowView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;

			case 5:  // For DeleteFromSpreadIconView Action

					BlankIconView->ShowView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->ShowView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;

			/*case 6:  // For Refresh Product Action

					PRIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->ShowView();
					break;*/
		//added by Yogesh on 11.2.5	New combinations of Icons.
			case 7:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->ShowView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;
			case 8:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->ShowView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;
			case 9:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->ShowView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 10:
					
					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->ShowView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;
			case 11:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->ShowView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 12:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->ShowView();
					CopyItemIconView->HideView();
					ArtItemIconView->ShowView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 13:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->ShowView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

				case 14:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->ShowView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;


				case 15:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->ShowView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

				case 16:

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->ShowView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

				case 17:
//CA("Case 17");
					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->ShowView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 18:

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->ShowView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 19:

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->ShowView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 20:

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 21:
					//CA("Inside case 21");
					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
				break;

		//ended by Yogesh on 11.2.5 New combinations of Icosn
			default:
					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();


					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;
					
		}
	}while(0);
}

//// till here
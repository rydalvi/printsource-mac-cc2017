#ifndef __PRODUCTSPRAY_H__
#define __PRODUCTSPRAY_H__

#include "VCPluginHeaders.h"
#include "IDataSprayer.h"


using namespace std;

class ProductSpray
{
public:
	void startSpraying(void);
	int32 checkIsSprayItemPerFrameTag(const UIDList &selectUIDList , bool16 &isItemHorizontalFlow);
	ErrorCode CreatePages(const IDocument* iDocument, const UIDRef spreadToAddTo,const int16 numPagesToInsert, const int16 pageToInsertAt, const bool16 allowShuffle);
	bool16 checkIsHorizontalFlowFlagPresent(const UIDList &selectUIDList);

private:
	bool16 getAllBoxIds(UIDList&);
	bool16 doesExist(IIDXMLElement * ptr, UIDRef BoxUidref,const UIDList &selectUIDList);
	bool16 doesExist(TagList &tagList,const UIDList &selectUIDList);
	
};

#endif	
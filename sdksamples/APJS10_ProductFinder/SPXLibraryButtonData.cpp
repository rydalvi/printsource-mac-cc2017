
#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISPXLibraryButtonData.h"

// General includes:
#include "CPMUnknown.h"

// Project includes:
#include "SPID.h"


class SPXLibraryButtonData : public CPMUnknown <ISPXLibraryButtonData>
{
public:

	SPXLibraryButtonData(IPMUnknown* boss) : CPMUnknown<ISPXLibraryButtonData>(boss) {}
	
	void			SetName(PMString& theName);
	PMString&		GetName();
};

CREATE_PMINTERFACE(SPXLibraryButtonData, kSPXLibraryButtonDataImpl)

void	SPXLibraryButtonData::SetName(PMString& theName)
{
    theName.ParseForEmbeddedCharacters();
	fName.SetString( theName );
}

PMString&	 SPXLibraryButtonData::GetName()
{
	return fName;
}



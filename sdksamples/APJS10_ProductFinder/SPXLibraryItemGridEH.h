#ifndef __SPXLIBRARYITEMGRID_H__
#define __SPXLIBRARYITEMGRID_H__
/////////////
#include "IApplication.h"
#include "IBooleanControlData.h"
#include "IControlView.h"
#include "IDialogController.h"
#include "IDialogMgr.h"
#include "IDragDropController.h"
#include "IDragDropSource.h"
#include "IEvent.h"
#include "ISPXLibraryButtonData.h"
//#include "SPSelectionObserver.h"
//#include "ILibraryAsset.h"
//#include "ILibraryAssetProxy.h"
//#include "ILibraryItemButtonData.h"
#include "ISPXLibraryViewController.h"
#include "IListBoxController.h"
#include "IPanelControlData.h"
#include "ISession.h"
#include "ISubject.h"
#include "IWidgetParent.h"
#include "IWindow.h"
#include "IWindowPort.h"
#include "IWidgetParent.h"
#include "ITextControlData.h"
#include "IWidgetUtils.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "Utils.h"
#include "IGeometry.h"
#include "TransformUtils.h"
#include "IGraphicFrameData.h"
#include "ILayoutUIUtils.h"
#include "IDataBase.h"
#include "IDocument.h"
#include "SDKUtilities.h"
#include "ICommand.h"
//#include "IImportFileCmdData.h"
#include "CmdUtils.h"
#include "IPlaceGun.h"
#include "OpenPlaceID.h"
#include "IReplaceCmdData.h"


//---From XPanelEventHandler
//#include "XPanelEventHandler.h"
#include "IPanelControlData.h"
#include "IControlView.h"
//#include "IEvent.h"
#include "ViewPortAccess.h"
#include "AcquireViewPort.h"
#include "IWindowPort.h"
#include "IWidgetParent.h"
#include "IWindow.h"
#include "ISuppressedUI.h"
#include "CAlert.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"


// ----- Includes -----

#include "AcquireViewPort.h"
#include "InterfacePtr.h"
//#include "LibraryPanelDefs.h"
//#include "XPanelEventHandler.h"
#include "UIDList.h"
#include "ViewPortAccess.h"

#include "K2Vector.h"
#include "IUIFontSpec.h"
// ----- Utility files -----
#include "IEvent.h"
#include "IEventHandler.h"
#include "CEventHandler.h"
#include "EventUtilities.h"
//#include "LibraryAssetUtils.h"

// ----- ID.h files -----
#include "SPXLibraryItemGridEH.h"
#include "DragDropID.h"
#include "CAlert.h"
#include "SPID.h"
///////////////

#include "ISelectionManager.h"
#include "SelectionObserver.h"
//#include "SPID.h"
//#include "IPanelControlData.h"
#include "IAppFramework.h"
#include "PublicationNode.h"

class SPXLibraryItemGridEH : public CEventHandler//XPanelEventHandler
{
public:
	SPXLibraryItemGridEH(IPMUnknown *boss);
	virtual ~SPXLibraryItemGridEH();
	
	virtual bool16 LButtonDn(IEvent* e); 
	virtual bool16 ButtonDblClk(IEvent* e);

	//XPanelEventHandler(IPMUnknown *boss);
	//virtual ~XPanelEventHandler();
	
	virtual bool16 MouseMove(IEvent* e); 
	virtual bool16 MouseDrag(IEvent* e) ; 
	virtual bool16 LButtonUp(IEvent* e) ; 
	virtual bool16 ControlCmd(IEvent* e) ; 
	virtual bool16 KeyDown(IEvent* e) ; 
	virtual bool16 KeyCmd(IEvent* e) ; 
	virtual bool16 KeyUp(IEvent* e) ; 
	virtual bool16 Update(IEvent* e) ; 
	virtual bool16 ButtonTrplClk(IEvent* e);
	virtual bool16 RButtonDn(IEvent* e) ; 
	virtual bool16 RButtonUp(IEvent* e) ;
	virtual bool16 PlatformEvent(IEvent* e); 
/// Chetan --
	void SetDesignerActionThumbnail(IPanelControlData *iPanelCtrldata, PMString name, int32 isProduct, int32 DActionNo, int32 isStarred,int32 NewProduct);
/// Chetan --
protected:
	IControlView*	GetWidgetView(IEvent* e);
	void			UpdateSelectionObservers();
	virtual void		ComputeDragExitRect(int32 itemClicked, SysRect& exitGRect);

	//From XPanelEventHandler
	virtual IEventHandler	*QueryWidgetEH(IEvent* e); 	// Caller of this method must release the returned IEventHandler
	virtual void			ClearCachedChildEH();
	virtual IEventHandler	*QueryCachedWidgetEH();	// Caller must release

private:
	int32	fPrevButtonClick;

	//From XPanelEventHandler
	UIDRef				fLastWidgetUIDRef;
	IEventHandler*			fLastLButtonDnEventHandler;
};

#endif
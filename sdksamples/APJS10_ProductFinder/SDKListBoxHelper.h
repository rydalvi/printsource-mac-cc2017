#ifndef __SDKListBoxHelper_h__
#define __SDKListBoxHelper_h__

class IPMUnknown;
class IControlView;
class IPanelControlData;

class SDKListBoxHelper
{
public:
	SDKListBoxHelper(IPMUnknown * fOwner, int32 pluginId);
	virtual ~SDKListBoxHelper();
	
	void		AddElement(IControlView* lstboxControlView, PMString & displayName,   WidgetID updateWidgetId, int atIndex = -2 /* kEnd */, int x=1);
	void		RemoveElementAt(int indexRemove, int x);
	void		RemoveLastElement( int x);
	IControlView* FindCurrentListBox(IPanelControlData* panel, int x);
	void		EmptyCurrentListBox(IPanelControlData* panel, int x);
	int			GetElementCount(int x);
	void		CheckUncheckRow(IControlView* listboxCntrlView, int32, bool);
	void		AddIcons(IControlView *listboxCntrlView, int32 index, int32 Check);
	void		SetDesignerAction(IControlView *listboxCntrlView, int32 , int32 , int32 ,int32 isStarred,int32 NewProduct,PMString itemListCount); //isStarred = 0 ONEsourceMode Hide Star, 1= Pub Mode Unselected WhiteStar, 2= Pum Mode Selected Greenstar

private:
	bool16		verifyState() { return (fOwner!=nil) ? kTrue : kFalse; }
	void		addListElementWidget(IControlView* lstboxControlView, InterfacePtr<IControlView> & elView, PMString & displayName, WidgetID updateWidgetId, int atIndex, int x);
	void		removeCellWidget(IControlView * listBox, int removeIndex);
	IPMUnknown* fOwner;
	int32		fOwnerPluginID;
};

#endif 




#include "VCPlugInHeaders.h"
#include "ICategoryBrowser.h"
#include "CPMUnknown.h"
#include "CTBActionComponent.h"
#include "CTBID.h"


class CCategoryBrowser:public CPMUnknown<ICategoryBrowser>
{	
public:
	CCategoryBrowser(IPMUnknown* );
	~CCategoryBrowser();	
	void OpenCategoryBrowser(int32 Top, int32 Left, int32 Right, int32 Bottom);
	int32 GetSelectedClassID(PMString &ClassName);
	void CloseCategoryBrowser();

};

CREATE_PMINTERFACE(CCategoryBrowser,kCTBCategoryBrowserImpl)

CCategoryBrowser::CCategoryBrowser(IPMUnknown* boss):CPMUnknown<ICategoryBrowser>(boss)
{}

CCategoryBrowser::~CCategoryBrowser()
{}

void CCategoryBrowser::OpenCategoryBrowser(int32 Top, int32 Left, int32 Bottom, int32 Right )
{
	CTBActionComponent actionComponetObj(this);
	actionComponetObj.DoPallete(Top, Left, Bottom, Right); 
}


int32 CCategoryBrowser::GetSelectedClassID(PMString &ClassName)
{
	return 0;
}

void CCategoryBrowser::CloseCategoryBrowser()
{
	CTBActionComponent actionComponetObj(this);
	actionComponetObj.ClosePallete(); 
}
#include "VCPluginHeaders.h"
#include "BoxReader.h"
#include "PageData.h"
#include "TagReader.h"
#include "IAppFrameWork.h"
//#include "ISpecialChar.h"
#include "CAlert.h"

/*

  This is how the hierarchy goes
								
								   BOXID
									/ \
								   /   \
							ObjectID   ObjectID
								/\		 \
							   /  \		Elem1
							 Elem1 Elem2
				   
For each box in the page, there will be a corresponding object of PageData.
Each box will contain the ObjectIDs of the sprayed data. (We have to consider the 
different publication IDs too. For example object id 11001 with pubID 100 is not
the same as object id 11001 for pubID 102
*/

#define CA(x)	CAlert::InformationAlert(x)

bool16 BoxReader::getBoxInformation(const UIDRef& boxUIDRef, PageData& pData)
{
	TagReader tReader;
	TagList tList;

	bool16 taggedFrameFlag=kFalse;
//	pData.boxID=boxUIDRef.GetUID();
	pData.BoxUIDRef=boxUIDRef;
	
	tList=tReader.getTagsFromBox(boxUIDRef);

	if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
	{
		tList=tReader.getFrameTags(boxUIDRef);
		if(tList.size()<=0)//It really is not our box
			return kFalse;
		taggedFrameFlag=kTrue;
	}
	int32 index;

	for(int i=0; i<tList.size(); i++)
	{
		TagStruct tInfo=tList[i];
		if(taggedFrameFlag)
		{	pData.boxID=boxUIDRef.GetUID();
			this->AppendObjectInfo(tInfo, pData, kTrue);
			continue;
		}
		if(this->objectIDExists(pData, tInfo, &index))//Search for this Object ID in the pData. If it exists add as an Element or add as a new Object
		{	pData.boxID=boxUIDRef.GetUID();
			this->AppendElementInfo(tInfo, pData, index);
		}
		else
		{	pData.boxID=boxUIDRef.GetUID();
			this->AppendObjectInfo(tInfo, pData);
		}
	}
	//-------lalit-----
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
	return kTrue;
}

bool16 BoxReader::elementIDExists(const ElementInfoList& eList, double elementID, PMString& elementName)
{
	for(int i=0; i<eList.size(); i++)
	{
		if(eList[i].elementID==elementID)		
		{
			if(elementName.NumUTF16TextChars())
				elementName=eList[i].elementName;
			return kTrue;
		}
	}
	return kFalse;
}

bool16 BoxReader::AppendObjectInfo(const TagStruct& tInfo, PageData& pData, bool16 isTaggedFrame)
{
	//InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	//if(ptrIAppFramework == nil)
	//	return kFalse;

	//CObjectValue oVal;
	//oVal=ptrIAppFramework->FAMMngr_getObjectElementValues(tInfo.parentId);

	//PMString objName=oVal.getName();
	//
	//if(objName.NumUTF16TextChars()==0)
	//	return kFalse;

	//ObjectData oData;
	//oData.objectID=tInfo.parentId;

	//if(isTaggedFrame && tInfo.whichTab==4)
	//	oData.objectName="Item Data";
	//else
	//	oData.objectName=oVal.getName();

	//oData.objectTypeID=oVal.getObject_type_id();
	//oData.publicationID=tInfo.reserved2;
	//
	//oData.whichTab = tInfo.whichTab;

	//if(tInfo.isTablePresent) // Added for table
	//	oData.isTableFlag=kTrue;
	//else
	//	oData.isTableFlag=kFalse;

	//if(tInfo.reserved1) // Added for Image
	//	oData.isImageFlag=kTrue;
	//else
	//	oData.isImageFlag=kFalse;
	//	
	//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//if(iConverter)
	//{
	//	PMString temp=iConverter->translateString(oData.objectName);
	//	oData.objectName=temp;
	//}

	//pData.objectDataList.push_back(oData);

	//this->AppendElementInfo(tInfo, pData, pData.objectDataList.size()-1, isTaggedFrame);
	return kTrue;
}

bool16 getAttributeName(double elementID, PMString& name)
{
	/*InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;

	VectorAttributeInfoPtr objPtr;

	objPtr=ptrIAppFramework->ATTRIBMngr_getCoreAttributes();

	if(!objPtr)
		return kFalse;

	VectorAttributeInfoValue::iterator it;

	for(it=objPtr->begin(); it!=objPtr->end(); it++)
	{
		if(it->getAttribute_id()==elementID)
		{
			name=it->getDisplay_name();
			if(name.NumUTF16TextChars()>0)
			{
				delete objPtr;
				return kTrue;
			}
			else
			{
				delete objPtr;
				return kFalse;
			}
		}
	}

	delete objPtr;

	objPtr=ptrIAppFramework->ATTRIBMngr_getParametricAttributes();

	if(!objPtr)
		return kFalse;

	for(it=objPtr->begin(); it!=objPtr->end(); it++)
	{
		if(it->getAttribute_id()==elementID)
		{
			name=it->getDisplay_name();
			if(name.NumUTF16TextChars())
			{
				delete objPtr;
				return kTrue;
			}
			else
			{
				delete objPtr;
				return kFalse;
			}
		}
	}
	delete objPtr;*/
	return kFalse;
}


bool16 BoxReader::AppendElementInfo(const TagStruct& tStruct, PageData& pData, int32 index, bool16 isTaggedFrame)
{
	TagInfo tInfo;
	tInfo.elementID=tStruct.elementId;
	tInfo.typeID=tStruct.typeId;
	tInfo.isTaggedFrame=isTaggedFrame;
	tInfo.tableFlag = tStruct.isTablePresent;
	tInfo.imageFlag = tStruct.imgFlag;
	tInfo.whichTab = tStruct.whichTab;
	ObjectData oData=pData.objectDataList[index];
	ElementInfoList eList=oData.elementList;

	PMString eName;

	if(this->elementIDExists(eList, tInfo.elementID, eName))//An element can be sprayed multiple times
		return kFalse;
	tInfo.StartIndex =tStruct.startIndex;
	tInfo.EndIndex= tStruct.endIndex;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;

	if(tInfo.typeID<=0 && tInfo.elementID<=0)//Hardcoded Name
	{
		switch(tStruct.whichTab)
		{
		case 1: tInfo.elementName="PF Name"; break;
		case 2: tInfo.elementName="PG Name"; break;
		case 3: if(tInfo.elementID == -2)
					tInfo.elementName="PR Number";
				else
					tInfo.elementName="PR Name"; 
				break;
		case 4: tInfo.elementName="Item Name"; break;
		default:return kFalse;
		}
		pData.objectDataList[index].elementList.push_back(tInfo);
		return kTrue;
	}

		
	if(tInfo.typeID<=0)//Attribute Name
	{
		PMString tname;
		tname.AppendNumber(PMReal(tInfo.elementID));
		if(!getAttributeName(tInfo.elementID, eName))
			return kFalse;
		tInfo.elementName=eName;
		pData.objectDataList[index].elementList.push_back(tInfo);
		return kTrue;
	}

	else if(tInfo.typeID>0 && tInfo.elementID>0)//Element Name
	{
		/*VectorElementInfoPtr eleValObj = ptrIAppFramework->EleMngr_getElementsByTypeId(tStruct.typeId);
		if(eleValObj==nil)
			return kFalse;
		VectorElementInfoValue::iterator it;
		
		for(it=eleValObj->begin();it!=eleValObj->end();it++)
		{
			if(it->getElement_id()==tStruct.elementId)
			{
				eName=it->getName();
				break;
			}
		}
		delete eleValObj;
		if(eName.NumUTF16TextChars()==0)
			return kFalse;

		tInfo.elementName=eName;
		pData.objectDataList[index].elementList.push_back(tInfo);
		return kTrue;*/
	}
	else if(tInfo.typeID>0 && tInfo.elementID<=0)//Images
	{
		
		/*VectorTypeInfoPtr vinfoPtr=nil;*/
		switch(tStruct.whichTab)
		{
		case 1:
			//vinfoPtr=ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PF_ASSETS");
			
			break;
		case 2:
			//vinfoPtr=ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PG_ASSETS");
			
			break;
		case 3:
			if(tInfo.tableFlag == 1)
			{
			//vinfoPtr=ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PR_TABLES");
			
			break;
			}
			//vinfoPtr=ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PR_ASSETS");
			
			
			break;
		case 4:
			//vinfoPtr=ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PRD_IMAGES");
			
			break;
		}

		//VectorTypeInfoValue::iterator it;
		//for(it=vinfoPtr->begin(); it!=vinfoPtr->end(); it++)
		//{
		//	if(it->getType_id()==tInfo.typeID)
		//	{
		//		// Awasthi
		//		eName=it->getName();
		//		break;
		//	}
		//}

		/*if(vinfoPtr)
			delete vinfoPtr;

		if(eName.NumUTF16TextChars())
		{
			
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}*/
	}
	return kFalse;
}


bool16 BoxReader::objectIDExists(const PageData& pData, const TagStruct& tInfo, int32* index)
{
	for(int i=0; i<pData.objectDataList.size(); i++)
	{
		ObjectData oData=pData.objectDataList[i];
		if(oData.objectID==tInfo.parentId && oData.publicationID==tInfo.sectionID)
		{
			*index=i;
			return kTrue;
		}
	}
	return kFalse;
}


bool16 BoxReader::getBoxInformation(const UIDRef& boxUIDRef, PageData& pData, int32 start, int32 end)
{
	pData.boxID=boxUIDRef.GetUID();
	pData.BoxUIDRef =boxUIDRef;
	TagReader tReader;
	TagList tList;
	tList=tReader.getTagsFromBox(boxUIDRef);

	if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
	{
		tList=tReader.getFrameTags(boxUIDRef);
		if(tList.size()<=0)//It really is not our box
			return kFalse;
	}

	int32 index;

	for(int i=0; i<tList.size(); i++)
	{
		TagStruct tInfo=tList[i];
		if(tInfo.endIndex<=end && tInfo.startIndex>=start)
		{
			if(this->objectIDExists(pData, tInfo, &index))
				this->AppendElementInfo(tInfo, pData, index);
			else
				this->AppendObjectInfo(tInfo, pData);
		}
	}
	//------------
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
	return kTrue;
}

//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================
//REGISTER_PMINTERFACE(SPDialogController, kSPDialogControllerImpl)
//REGISTER_PMINTERFACE(SPDialogObserver, kSPDialogObserverImpl)
REGISTER_PMINTERFACE(SPActionComponent, kSPActionComponentImpl)
REGISTER_PMINTERFACE(SPSelectionObserver, kSPSelectionObserverImpl)
//REGISTER_PMINTERFACE(SPListBoxObserver, kSPListBoxObserverImpl)
//REGISTER_PMINTERFACE(SprayerInterface,	kSPSprayerInterfaceImpl)
REGISTER_PMINTERFACE(SPLoginEventsHandler,	kSPLoginEventsHandlerImpl)
REGISTER_PMINTERFACE(TextMiscellanySuiteASB, kTextMiscellanySuiteASBImpl)
REGISTER_PMINTERFACE(TextMiscellanySuiteLayoutCSB,kTextMiscellanySuiteLayoutCSBImpl)
REGISTER_PMINTERFACE(TblBscSuiteASB,			kTblBscSuiteASBImpl)
REGISTER_PMINTERFACE(TblBscSuiteTextCSB,		kTblBscSuiteTextCSBImpl)
REGISTER_PMINTERFACE(SPActionFilter, kSPActionFilterImpl)
//REGISTER_PMINTERFACE(MyRunControlView, kPFSnipRunControlViewImpl )
//REGISTER_PMINTERFACE(DoubleClickHandlerPF , DoubleClickPFEventHandlerImpl)
REGISTER_PMINTERFACE(ContentSprayer , kContentSprayerImpl)//Added on 20/09 to refresh productfinder plugin

//REGISTER_PMINTERFACE( SPPictureWidgetEH, kSPCustomPictureWidgetEHImpl)
////////////////// For ThumbNail View  ////////////////////////
//REGISTER_PMINTERFACE(SPXLibraryItemViewPanel, kSPXLibraryItemViewPanelImpl)
//REGISTER_PMINTERFACE(SPXLibraryItemGridPanel, kSPXLibraryItemGridPanelImpl)
//REGISTER_PMINTERFACE(SPXLibraryItemGridEH, kSPXLibraryItemGridEHImpl)
//REGISTER_PMINTERFACE(SPXLibraryItemListBoxController, kSPXLibraryItemListBoxControllerImpl)
//REGISTER_PMINTERFACE(SPXLibraryItemGridController, kSPXLibraryItemGridControllerImpl)
//REGISTER_PMINTERFACE(SPXLibraryItemGridPanorama, kSPXLibraryItemGridPanoramaImpl)
////REGISTER_PMINTERFACE(SPXLibrarySelectionObserver, kSPXLibrarySelectionObserverImpl)
//REGISTER_PMINTERFACE(SPXLibraryItemButton, kSPXLibraryItemButtonImpl)
REGISTER_PMINTERFACE(SPXLibraryButtonData, kSPXLibraryButtonDataImpl)
//REGISTER_PMINTERFACE(SPXLibraryItemGridDDSource, kSPXLibraryItemGridDDSourceImpl)
//REGISTER_PMINTERFACE(SPPRImageHelper , kSPProductImageIFaceImpl)
//REGISTER_PMINTERFACE(SPMyGridTip,kSPMyGridTipImpl)
//REGISTER_PMINTERFACE(SPMyTip,kSPMyTipImpl)
////////////////// ThumbNail View Ends ////////////////////////
REGISTER_PMINTERFACE(SelectCustomerDialogController,kSelectCustomerDialogControllerImpl)
REGISTER_PMINTERFACE(SelectCustomerDialogObserver,kSelectCustomerDialogObserverImpl)

REGISTER_PMINTERFACE(SPTreeViewHierarchyAdapter, kSPTreeViewHierarchyAdapterImpl)
REGISTER_PMINTERFACE(SPTreeViewWidgetMgr,		kSPTreeViewWidgetMgrImpl)
REGISTER_PMINTERFACE( SPTreeNodeEH,				kSPTreeNodeEHImpl)

#include "VCPlugInHeaders.h"


#include "ITextMiscellanySuite.h"

#include "CmdUtils.h"
#include "UIDList.h"
#include "CAlert.h"
#include "SelectionASBTemplates.tpp"

class TextMiscellanySuiteASB : public CPMUnknown<ITextMiscellanySuite>
{
	public:
		TextMiscellanySuiteASB(IPMUnknown *iBoss);
	
	/** Destructor. */
	virtual ~TextMiscellanySuiteASB(void);

	//virtual bool16 GetCurrentSpecifier(ISpecifier *&);   //Commented By Sachin Sharma on 29/06/07
	virtual bool16 GetUidList(UIDList &);
	virtual bool16 GetFrameUIDRef(UIDRef &frameUIDRef);
	virtual bool16 GetCaretPosition(TextIndex &pos);

	virtual bool16 setFrameUser(void);
};
CREATE_PMINTERFACE(TextMiscellanySuiteASB, kTextMiscellanySuiteASBImpl)

TextMiscellanySuiteASB::TextMiscellanySuiteASB(IPMUnknown* iBoss) :
CPMUnknown<ITextMiscellanySuite>(iBoss)
{
}
TextMiscellanySuiteASB::~TextMiscellanySuiteASB(void)
{
}

//#pragma mark-   //Commented By Sachin Sharma on 29/06/07
//bool16 TextMiscellanySuiteASB::GetCurrentSpecifier(ISpecifier * & Spec)
//{
//	return (AnyCSBSupports(make_functor(&ITextMiscellanySuite::GetCurrentSpecifier,Spec), this));
//}
bool16 TextMiscellanySuiteASB::GetUidList(UIDList & TempUIDList)
{
	return (AnyCSBSupports(make_functor(&ITextMiscellanySuite::GetUidList,TempUIDList), this));
}
bool16 TextMiscellanySuiteASB::GetFrameUIDRef(UIDRef &frameUIDRef)
{
	return (AnyCSBSupports(make_functor(&ITextMiscellanySuite::GetFrameUIDRef,frameUIDRef), this));
}
bool16 TextMiscellanySuiteASB:: GetCaretPosition(TextIndex &pos)
{
	return (AnyCSBSupports(make_functor(&ITextMiscellanySuite::GetCaretPosition,pos), this));
}

//*******
bool16 TextMiscellanySuiteASB::setFrameUser(void)
{	
	return (AnyCSBSupports(make_functor(&ITextMiscellanySuite::setFrameUser), this));
}

#include "VCPluginHeaders.h"

#include "IPMUnknown.h"
#include "UIDRef.h"
//#include "ITextFrame.h"         //Commented By Sachin sharma
#include "IMultiColumnTextFrame.h"    //on 28/06/07
#include "IXMLUtils.h"
#include "IXMLTagList.h"
//#include "ISelection.h"
#include "IDocument.h"
#include "IStoryList.h"
#include "IFrameList.h"
#include "ITextModel.h"
#include "IIDXMLElement.h"
#include "IXMLReferenceData.h"
#include "IGraphicFrameData.h"
#include "UIDList.h"
#include "XMLReference.h"
#include "IFrameUtils.h"
//#include "LayoutUtils.h"  //Cs3
#include "ILayoutUtils.h"  //Cs4
#include "k2smartptr.h"
#include "CAlert.h"
#include "IXMLUtils.h"
#define CA(x) CAlert::InformationAlert(x)


#include "TagReader.h"

void TagReader::getTextFrameTags(void)
{
	do{	
		//Commented By Sachin Sharma on 29/06/07
		/*InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == nil)
				break;*/
//++++++++++++++================Adde By Sachin sharma
		InterfacePtr<IMultiColumnTextFrame>multiColumnTextFrame(boxUIDRef.GetDataBase(), textFrameUID, UseDefaultIID());
		if(multiColumnTextFrame==nil)
		{
			ASSERT(multiColumnTextFrame);
			break;
		}


//===================+++++++++++++++

		InterfacePtr<ITextModel> objTxtMdl (multiColumnTextFrame->QueryTextModel());
		if (objTxtMdl == nil)
			break;

		IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
		if (objXMLRefDat==nil)
			break;

		XMLReference xmlRef=objXMLRefDat->GetReference();

		UIDRef refUID=xmlRef.GetUIDRef();		

		//IIDXMLElement *xmlElement=xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement==nil)
			break;

		xmlPtr=xmlElement;

		int elementCount=xmlElement->GetChildCount();
		
		for(int i=0; i<elementCount; i++)
		{

			XMLReference elementXMLref=xmlElement->GetNthChild(i);
			//IIDXMLElement * childElement=elementXMLref.Instantiate();
			InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
			if(childElement==nil)
				continue;
			PMString tagName=childElement->GetTagString();
			
			TagStruct tInfo;
			
			int32 attribCount=childElement->GetAttributeCount();
			TextIndex sIndex=0, eIndex=0;
			Utils<IXMLUtils>()->GetElementIndices(childElement, &sIndex, &eIndex);
			
			tInfo.startIndex=sIndex;
			tInfo.endIndex=eIndex;
			tInfo.tagPtr=childElement;
			
			
			for(int j=0; j<attribCount; j++)
			{
				PMString attribName=childElement->GetAttributeNameAt(j);
				PMString attribVal=childElement->GetAttributeValue(WideString(attribName)); //Cs4
				getCorrespondingTagAttributes(attribName, attribVal, tInfo);
			}
			tList.push_back(tInfo);
		}
		objXMLRefDat->Release();

	}while(0);
}

void TagReader::getGraphicFrameTags(void)
{
	do{		
		InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);	
		InterfacePtr<IXMLReferenceData> objXMLRefDat(Utils<IXMLUtils>()->QueryXMLReferenceData(unknown));
		if (objXMLRefDat == nil)
			break;

		XMLReference xmlRef = objXMLRefDat->GetReference();
		UIDRef refUID = xmlRef.GetUIDRef();
		//IIDXMLElement *xmlElement = xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement == nil)
			break;

		xmlPtr=xmlElement;

		PMString tagName = xmlElement->GetTagString();
		
		TagStruct tInfo;
		tInfo.tagPtr=xmlElement;

		int32 attribCount = xmlElement->GetAttributeCount();

		for(int j=0; j<attribCount; j++)
		{
			PMString attribName = xmlElement->GetAttributeNameAt(j);
			PMString attribVal  = xmlElement->GetAttributeValue(WideString(attribName)); //Cs4
			getCorrespondingTagAttributes(attribName, attribVal, tInfo);
		}
		tInfo.endIndex=-1;
		tInfo.startIndex=-1;
		tList.push_back(tInfo);
	}while(0);
}

TagList TagReader::getTagsFromBox(UIDRef boxId, IIDXMLElement** xmlPtr)
{
	tList.clear();
	/*InterfacePtr<IPMUnknown> unknown(boxId, IID_IUNKNOWN);	
	UID frameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxId, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	boxUIDRef=boxId;
//	textFrameUID=frameUID;

	if(textFrameUID == kInvalidUID)
		getGraphicFrameTags();			
	else 
		getTextFrameTags();
	
	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;
	
	if(xmlPtr)
		*xmlPtr=this->xmlPtr;

	return tList;
}

bool16 TagReader::getCorrespondingTagAttributes(const PMString& attribName, const PMString& attribVal, TagStruct& tStruct)
{
	if(attribName.IsEqual("ID", kFalse))
	{
		tStruct.elementId=attribVal.GetAsDouble();
		return kTrue;
	}
	
	if(attribName.IsEqual("imgFlag", kFalse))
	{
		tStruct.imgFlag=attribVal.GetAsNumber();
		return kTrue;
	}

	if(attribName.IsEqual("index", kFalse))
	{
		tStruct.whichTab=attribVal.GetAsNumber();
		return kTrue;
	}

	if(attribName.IsEqual("typeId", kFalse))
	{
		tStruct.typeId=attribVal.GetAsDouble();
		return kTrue;
	}

	if(attribName.IsEqual("parentId", kFalse))
	{
		tStruct.parentId=attribVal.GetAsDouble();
		return kTrue;
	}

	if(attribName.IsEqual("sectionID", kFalse))
	{
		tStruct.sectionID=attribVal.GetAsDouble();
		return kTrue;
	}

	if(attribName.IsEqual("parentTypeID", kFalse))
	{
		tStruct.parentTypeID=attribVal.GetAsDouble();
		return kTrue;
	}
	if(attribName.IsEqual("tableFlag", kFalse))
	{
		/*tStruct.tableFlag=attribVal.GetAsNumber();
		return kTrue;*/
		int32 isTable = attribVal.GetAsNumber();
		if(isTable == 1)
		{
		//	CA("has table");
			tStruct.isTablePresent = kTrue;
		}
		else
		{
			//CA("NO table");
			tStruct.isTablePresent = kFalse;
		}
	}
	return kFalse;
}


bool16 TagReader::GetUpdatedTag(TagStruct& tInfo)
{
	if(!tInfo.tagPtr)
		return kFalse;
	
	TextIndex sIndex=0, eIndex=0;
	Utils<IXMLUtils>()->GetElementIndices(tInfo.tagPtr, &sIndex, &eIndex);
	
	tInfo.startIndex=sIndex;
	tInfo.endIndex=eIndex;
	
	return kTrue;
}

TagList TagReader::getFrameTags(UIDRef frameUIDRef)
{
	tList.clear();
	boxUIDRef=frameUIDRef;
	// Get the graphic frame tags as in case of graphic frames tag is attached with the frame itself
	getGraphicFrameTags();			

	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;

	return tList;
}
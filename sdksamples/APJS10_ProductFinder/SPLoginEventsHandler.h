#ifndef __SPLoginEventsHandler_h__
#define __SPLoginEventsHandler_h__

#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "SPID.h"
#include "ILoginEvent.h"

class SPLoginEventsHandler : public CPMUnknown<ILoginEvent>
{
public:
	SPLoginEventsHandler(IPMUnknown* );
	~SPLoginEventsHandler();
	bool16 userLoggedIn();
	bool16 userLoggedOut();
	bool16 resetPlugIn();
};

#endif
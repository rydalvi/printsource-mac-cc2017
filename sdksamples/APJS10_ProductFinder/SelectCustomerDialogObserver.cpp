//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/basicdialog/SpraySettingsDialogObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"

// General includes:
#include "CDialogObserver.h"
#include "CAlert.h"
// Project includes:
#include "SPID.h"
#include "MediatorClass.h"
#include <ITriStateControlData.h>
#include <IWidgetParent.h>
#include <ITextControlData.h>
#include <SDKFileHelper.h>
#include <SDKUtilities.h>
#include <IDialogController.h>
#include "IDropDownListController.h" 
#include "IStringListControlData.h"
#include "IAppFramework.h"

#define CA(x) CAlert::InformationAlert(x)

/**	Implements IObserver based on the partial implementation CDialogObserver; 
	allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	
	 @ingroup basicdialog
	
*/
class SelectCustomerDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		SelectCustomerDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~SelectCustomerDialogObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. 
				Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(SelectCustomerDialogObserver, kSelectCustomerDialogObserverImpl)

/* AutoAttach
*/
void SelectCustomerDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("SpraySettingsDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		// Now attach to BasicDialog's info button widget.
		//AttachToWidget(kSpraySettingsIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kOKSleCustButtonWidgetID, IID_IBOOLEANCONTROLDATA,panelControlData);		
		

	} while (false);
}

/* AutoDetach
*/
void SelectCustomerDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("SpraySettingsDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}
		
		// Now we detach from BasicDialog's info button widget.
		//DetachFromWidget(kSpraySettingsIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kOKSleCustButtonWidgetID, IID_IBOOLEANCONTROLDATA,panelControlData);	
		
	} while (false);
}

/* Update
*/
void SelectCustomerDialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("AP7_ProductFinder::SelectCustomerDialogObserver::Update::controlView invalid");
			break;
		}
		InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
		if(dialogController == nil)
		{
			//CA("dialogController == nil");
			break;
		}
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		if(theSelectedWidget==kOKSleCustButtonWidgetID && theChange==kTrueStateMessage)
		{
			//CA("kOKSleCustButtonWidgetID");
			
			InterfacePtr<IStringListControlData> DropListData(
			dialogController->QueryListControlDataInterface(kCustomerSelectionDropDownWidgetID));
			if(DropListData == nil)
			{
				//CA("DropListData nil");
				break;
			}
			InterfacePtr<IDropDownListController> dropDownListController(DropListData,UseDefaultIID());
			if (dropDownListController == nil)
			{
				//CA("dropDownListController nil");
				break;
			}

			int32 dropDownIndexSelected =  dropDownListController->GetSelected(); 
			Mediator::setDropDownListSelectedIndex(dropDownIndexSelected,1);
			double selectedCustomerPVID = -1;
			if(dropDownIndexSelected != 0)
			{
				//CA("dropDownIndexSelected != 0");
				selectedCustomerPVID = Mediator::CustomerVector.at(dropDownIndexSelected-1).getValueId();
				/*PMString s("selectedCustomerPVID : ");
				s.AppendNumber(selectedCustomerPVID);
				CA(s);*/
			}			
			ptrIAppFramework->setSelectedCustPVID(selectedCustomerPVID,1);

			InterfacePtr<IStringListControlData> divDropListData(
			dialogController->QueryListControlDataInterface(kDivisionSelectionDropDownWidgetID));
			if(divDropListData == nil)
			{
				//CA("divDropListData nil");
				break;
			}
			InterfacePtr<IDropDownListController> divDropDownListController(divDropListData,UseDefaultIID());
			if (divDropDownListController == nil)
			{
				//CA("divDropDownListController nil");
				break;
			}

			dropDownIndexSelected =  divDropDownListController->GetSelected(); 
			Mediator::setDropDownListSelectedIndex(dropDownIndexSelected,0);
			double selectedDivisionPVID = -1;
			if(dropDownIndexSelected != 0)
			{
				//CA("dropDownIndexSelected != 0");
				selectedDivisionPVID = Mediator::DivisionVector.at(dropDownIndexSelected-1).getValueId();
				/*PMString s("selectedCustomerPVID : ");
				s.AppendNumber(selectedCustomerPVID);
				CA(s);*/
			}			
			ptrIAppFramework->setSelectedCustPVID(selectedDivisionPVID,0);


			CDialogObserver::CloseDialog();
		}		

		//		

	} while (false);
}

// End, SelectCustomerDialogObserver.cpp.





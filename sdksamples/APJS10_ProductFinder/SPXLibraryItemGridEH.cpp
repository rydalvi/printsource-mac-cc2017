
#include "VCPlugInHeaders.h"

// ----- Interfaces -----

#include "IApplication.h"
#include "IBooleanControlData.h"
#include "IControlView.h"
#include "IDialogController.h"
#include "IDialogMgr.h"
#include "IDragDropController.h"
#include "IDragDropSource.h"
#include "IEvent.h"
#include "ISPXLibraryButtonData.h"
#include "SPSelectionObserver.h"
//#include "ILibraryAsset.h"
//#include "ILibraryAssetProxy.h"
//#include "ILibraryItemButtonData.h"
#include "ISPXLibraryViewController.h"
#include "IListBoxController.h"
#include "IPanelControlData.h"
#include "ISession.h"
#include "ISubject.h"
#include "IWidgetParent.h"
#include "IWindow.h"
#include "IWindowPort.h"
#include "IWidgetParent.h"
#include "ITextControlData.h"
#include "IWidgetUtils.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "Utils.h"
#include "IGeometry.h"
#include "TransformUtils.h"
#include "IGraphicFrameData.h"
#include "ILayoutUIUtils.h"
#include "IDataBase.h"
#include "IDocument.h"
#include "SDKUtilities.h"
#include "ICommand.h"
//#include "IImportFileCmdData.h"
#include "CmdUtils.h"
#include "IPlaceGun.h"
#include "OpenPlaceID.h"
#include "IReplaceCmdData.h"


//---From XPanelEventHandler
//#include "XPanelEventHandler.h"
#include "IPanelControlData.h"
#include "IControlView.h"
//#include "IEvent.h"
#include "ViewPortAccess.h"
#include "AcquireViewPort.h"
#include "IWindowPort.h"
#include "IWidgetParent.h"
#include "IWindow.h"
#include "ISuppressedUI.h"
#include "CAlert.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"


// ----- Includes -----

#include "AcquireViewPort.h"
#include "InterfacePtr.h"
//#include "LibraryPanelDefs.h"
//#include "XPanelEventHandler.h"
#include "UIDList.h"
#include "ViewPortAccess.h"

#include "K2Vector.h"
#include "IUIFontSpec.h"
// ----- Utility files -----
#include "IEvent.h"
#include "IEventHandler.h"
#include "CEventHandler.h"
#include "EventUtilities.h"
//#include "LibraryAssetUtils.h"

// ----- ID.h files -----
//#include "ProductSpray.h"
#include "AcquireModalCursor.h"
#include "SPXLibraryItemGridEH.h"
#include "MediatorClass.h"
//#include "PRImageHelper.h"
//#include "IDataSprayer.h"
#include "PublicationNode.h"
#include "IAppFramework.h"
#include "DragDropID.h"
#include "CAlert.h"
#include "SPID.h"
#include "MilestoneModel.h"

#define CA(X) CAlert::InformationAlert(X)
#define CAI(X,Y) {PMString ASD(X);\
	ASD.Append(" : ");\
	ASD.AppendNumber(Y);\
	CAlert::InformationAlert(ASD);}

K2Vector<int32> ImageSelectionList;
int32	curWidgetIndex;
K2Vector<int32> multipleSelectionForThumb;

extern K2Vector<PMString> imageType2 ;
extern K2Vector<PMString> imageVector2 ;
extern K2Vector<int32> libSelectionList;
extern K2Vector<int32> curSelection ;
extern VectorPubObjectValuePtr ptrForThumbnail;
extern int32 CurrentSelectedProductRow;
extern PublicationNodeList pNodeDataList;
extern int32 CurrentSelectedSection;
extern int32 CurrentSelectedPublicationID;
extern int32 CurrentSelectedSubSection;
extern int32 CurrentSelectedProductRow;
extern VectorPubObjectValue vectorForThumbNail;
extern int32 global_lang_id ;
extern bool16 searchResult;
extern bool16 LeadingItemFlag ;
//class SPXLibraryItemGridEH : public CEventHandler//XPanelEventHandler
//{
//public:
//	SPXLibraryItemGridEH(IPMUnknown *boss);
//	virtual ~SPXLibraryItemGridEH();
//	
//	virtual bool16 LButtonDn(IEvent* e); 
//	virtual bool16 ButtonDblClk(IEvent* e);
//
//	//XPanelEventHandler(IPMUnknown *boss);
//	//virtual ~XPanelEventHandler();
//	
//	virtual bool16 MouseMove(IEvent* e); 
//	virtual bool16 MouseDrag(IEvent* e) ; 
//	virtual bool16 LButtonUp(IEvent* e) ; 
//	virtual bool16 ControlCmd(IEvent* e) ; 
//	virtual bool16 KeyDown(IEvent* e) ; 
//	virtual bool16 KeyCmd(IEvent* e) ; 
//	virtual bool16 KeyUp(IEvent* e) ; 
//	virtual bool16 Update(IEvent* e) ; 
//	virtual bool16 ButtonTrplClk(IEvent* e);
//	virtual bool16 RButtonDn(IEvent* e) ; 
//	virtual bool16 RButtonUp(IEvent* e) ;
//	virtual bool16 PlatformEvent(IEvent* e); 
///// Chetan --
//	void SetDesignerActionThumbnail(IPanelControlData *iPanelCtrldata, PMString name, bool isProduct, int32 DActionNo, int32 isStarred,int32 NewProduct);
///// Chetan --
//protected:
//	IControlView*	GetWidgetView(IEvent* e);
//	void			UpdateSelectionObservers();
//	virtual void		ComputeDragExitRect(int32 itemClicked, SysRect& exitGRect);
//
//	//From XPanelEventHandler
//	virtual IEventHandler	*QueryWidgetEH(IEvent* e); 	// Caller of this method must release the returned IEventHandler
//	virtual void			ClearCachedChildEH();
//	virtual IEventHandler	*QueryCachedWidgetEH();	// Caller must release
//
//private:
//	int32	fPrevButtonClick;
//
//	//From XPanelEventHandler
//	UIDRef				fLastWidgetUIDRef;
//	IEventHandler*			fLastLButtonDnEventHandler;
//};
//

CREATE_PMINTERFACE(SPXLibraryItemGridEH, kSPXLibraryItemGridEHImpl);


//---------------------------------------------------------------
// SPXLibraryItemGridEH constructor / destructor
//---------------------------------------------------------------

SPXLibraryItemGridEH::SPXLibraryItemGridEH(IPMUnknown *boss) :
	CEventHandler(boss),
	fLastWidgetUIDRef(UIDRef::gNull),
	fLastLButtonDnEventHandler(nil),
	fPrevButtonClick( -1 )
{
}

SPXLibraryItemGridEH::~SPXLibraryItemGridEH()
{
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::LButtonDn
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::LButtonDn(IEvent* e) 
{//CA(__FUNCTION__);
	bool16 handled = kTrue;
	
	bool16 cmdKeyDown = e->CmdKeyDown();
	bool16 shiftKeyDown = e->ShiftKeyDown();
	InterfacePtr<ISPXLibraryViewController> viewController( this, IID_ISPXLIBRARYVIEWCONTROLLER );
	InterfacePtr<IPanelControlData> panelData( this, IID_IPANELCONTROLDATA );

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}

	int32 i;
	bool16 isNewSelection = kFalse;
	IControlView*	widgetView = GetWidgetView(e);
	if ( widgetView ) 
	{
		ImageSelectionList.clear();
		curWidgetIndex = panelData->GetIndex(widgetView);
		//PMString ASD("");
		//ASD.Append("curWidgetIndex : ");
		//ASD.AppendNumber(curWidgetIndex);
		//CA(ASD);
		//CAI("curWidgetIndex", curWidgetIndex);
//###########//////////////////////////////
		CurrentSelectedProductRow = curWidgetIndex;
		Mediator  md;
		if(md.getSprayButtonView()== nil)
		{
			//CA("ListBoxObserver::md.getSprayButtonView()==nil");
		}
		else
		md.getSprayButtonView()->Enable(kTrue);
		
		if(md.getIconView() == nil){
			//CA("ListBoxObserver::md.getIconView() == nil");
		}
		else
		md.getIconView()->Enable(kTrue);
		if(md.getIconViewNew() == nil){
			//CA("ListBoxObserver::md.getIconView() == nil");
		}
		else
		md.getIconViewNew()->Enable(kTrue);

		if(md.getPreviewWidgetView()==nil)
		{
			//CA("ListBoxObserver::md.getPreviewWidgetView()==nil");
		}
		else
            md.getPreviewWidgetView()->Enable(kTrue);


		if(md.getDesignerActionWidgetView()== nil)	
		{
			//CA("ListBoxObserver::md.getDesignerActionWidgetView()== nil");
		}
		else
		{	///////
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				return kFalse;
			}
			CMilestoneModel  * milestoneval = NULL;

			//milestoneval = ptrIAppFramework->MilestoneCache_getDesignerMilestone();

			if(milestoneval == NULL)
			{	
				md.getDesignerActionWidgetView()->Disable();							
			}
			else
			{
				// Rolr ID = 1 not requireed here ... Taken from Userprofile in Application framework
				bool16 success = kFalse;
				//bool16 success = ptrIAppFramework->MILESTONECACHE_getRoleMilestoneWriteAccess(1 , milestoneval->getId());
				if(success)
				{							
					md.getDesignerActionWidgetView()->Enable();
					md.getDesignerActionWidgetView()->ShowView();
				}
				if(!success)
				{							
					md.getDesignerActionWidgetView()->Disable();
					md.getDesignerActionWidgetView()->HideView();
				}													
			}
			///////						
		}

		if(md.getRefreshButtonView() == nil)
		{
			//CA("md.getRefreshButtonView()==nil");
		}
		else
			md.getRefreshButtonView()->Enable();
			
		//Apsiva 9 Comment
		/*InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
		if(!DataSprayerPtr)
		{
			ptrIAppFramework->LogDebug("AP7_ProductFinder::SPListBoxObserver::Update::No DataSprayerPtr");
			return kFalse;
		}

		md.setCurrentObjectID(pNodeDataList[CurrentSelectedProductRow].getPubId());
		md.setPBObjectID(pNodeDataList[CurrentSelectedProductRow].getPBObjectID());

		if(searchResult)
			DataSprayerPtr->FillPnodeStruct(pNodeDataList[CurrentSelectedProductRow],pNodeDataList[CurrentSelectedProductRow].getSectionID(), pNodeDataList[CurrentSelectedProductRow].getPublicationID(), pNodeDataList[CurrentSelectedProductRow].getSubSectionID());
		else
		DataSprayerPtr->FillPnodeStruct(pNodeDataList[CurrentSelectedProductRow],CurrentSelectedSection, CurrentSelectedPublicationID, CurrentSelectedSubSection);*/

		
		//DataSprayerPtr->startSpraying();
	
		int32 PId = pNodeDataList[CurrentSelectedProductRow].getParentId();
		int32 typeID = pNodeDataList[CurrentSelectedProductRow].getTypeId();
		
		Mediator::parentID = PId;
		Mediator::parentTypeID = typeID;
//###########//////////////////////////////
		SPSelectionObserver selectionObj(this);
		VectorPubObjectValue::iterator it2;

		///////////Added By Dattatray on 1/08/2007 for DefinedBreakMultiLineTextWidget
		InterfacePtr<IBooleanControlData>	buttonData( widgetView, IID_IBOOLEANCONTROLDATA );
		if ( buttonData && !buttonData->IsSelected())
		{
			isNewSelection = kTrue;
			InterfacePtr<ISPXLibraryButtonData> btnData(widgetView, IID_ISPXLIBRARYBUTTONDATA);
			if(btnData== nil)		{
				//CA("No btnData");
				return 0;
			}
			PMString str=btnData->GetName();
			//CA("GetName : " + str);
			InterfacePtr<IWidgetParent> btnparent(widgetView, IID_IWIDGETPARENT);
			IPMUnknown* unknownbtn=btnparent->GetParent();
			if(unknownbtn == nil){
				//CA("no unknownbtn");
				return 0;
			}
			InterfacePtr<IWidgetParent> gridparent(unknownbtn, IID_IWIDGETPARENT);
			IPMUnknown* unknowngrid=gridparent->GetParent();
			if(unknowngrid == nil){
				//CA("no unknowngrid");
				return 0;
			}
			InterfacePtr<IControlView> ctr(unknowngrid,UseDefaultIID());
			if(ctr == nil){
				//CA("no ctr");
				return 0;
			}
			InterfacePtr<IWidgetParent> panelparent(ctr, IID_IWIDGETPARENT);
			IPMUnknown* unknownpanel=panelparent->GetParent();
			if(unknownpanel == nil){
				//CA("no unknownpanel");
				return 0;
			}
			InterfacePtr<IControlView> ctr1(unknownpanel,UseDefaultIID());
			if(ctr1 == nil){
				//CA("no ctr1");
				return 0;
			}
			InterfacePtr<IPanelControlData> iData(ctr1,IID_IPANELCONTROLDATA);
			if(iData == nil)
			{
				//CA("no iData");
				return 0;
			}
//////////////////////////////////////////////////////////////		Chetan --
//int32 size = vectorForThumbNail.size();
//PMString sizze("Size of pointer : ");
//sizze.AppendNumber(size);
//CA(sizze);

			int  j,icon_count=0;
//			bool16 isONEsource = ptrIAppFramework->get_isONEsourceMode();
//CA("4");

			//if(!isONEsource)
			//{
			for(it2 = /*ptrForThumbnail->begin()*/vectorForThumbNail.begin(),j = 0; it2 != /*ptrForThumbnail->end()*/vectorForThumbNail.end(); it2++,j++)
			{
				if(curWidgetIndex == j)
				{
					icon_count = selectionObj.GetIconCountForUnfilteredProductList(it2);
					break;
				}
			}

			int32 isProduct = 0;
			if(it2->getisProduct() == 2)
			{
				isProduct = 2;
			}
			if(it2->getisProduct() == 1)
			{
				isProduct = 1;
			}
			if(it2->getisProduct() == 0)
				isProduct = 0;

			int32 NewProductFlag =0;
			if(ptrIAppFramework->get_isONEsourceMode()){
				NewProductFlag = 0;
			}
			else if(it2->getNew_product()  == kTrue)
			{
				NewProductFlag =2;
			}
			else
			{
				NewProductFlag =1;
			}

			int32 StarFlag =0;
			if(ptrIAppFramework->get_isONEsourceMode()){
				StarFlag = 0;
			}
			else if(it2->getStarredFlag1()== kTrue) // if Selected Green Star
				StarFlag =2;
			else
				StarFlag = 1;

			if(ptrIAppFramework->get_isONEsourceMode()){
				icon_count = 111;
			}

			//Apsiva 9 comment
			/*ItemMapPtr pItemMap = NULL;
			pItemMap =  ptrIAppFramework->GetProjectProducts_getAllLeadingItemIds(md.getSelectedPubId());
			if(isProduct == 0 && pItemMap != NULL && pItemMap->size() > 0)
			{
				ItemMap::iterator itr;
				int32 searchItemId = it2->getItemModel().getItemID();
				itr = pItemMap->find(searchItemId);
			
				if(itr != pItemMap->end())
				{
					if(itr->second.itemType == 0)
						isProduct = 3;
					else if(itr->second.itemType == 1)
						isProduct = 4;
				}
			}
			

			this->SetDesignerActionThumbnail (iData, str, isProduct, icon_count, StarFlag, NewProductFlag);
			
			if(pItemMap)
				delete pItemMap;*/
	////////////////////////////////////////////////////////////       Chetan --
			//}
		}

		// If new selection and no cmd key is pressed, remove all old selection
		if ( isNewSelection && !cmdKeyDown && !shiftKeyDown  ) {
			for ( i=0; i<panelData->Length(); i++) {
				IControlView*	view = panelData->GetWidget(i);
				InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
				if ( buttonData->IsSelected() )
					buttonData->Deselect();
				
			}
		}
		else  if( shiftKeyDown ) {
			
			// double-check the previous selection
			// fPrevButtonClick would not be updated if a range of selection was deleted.
			InterfacePtr<IListBoxController> libListData( this, IID_ILISTBOXCONTROLLER );
			K2Vector<int32> libSelectionList;
			libListData->GetSelected( libSelectionList );
			if ( libSelectionList.Length() == 0 )
				fPrevButtonClick = -1;
//int32 size = libSelectionList.Length();
//PMString sizze("Size of pointer : ");
//sizze.AppendNumber(size);
//CA(sizze);
			/*multipleSelectionForThumb.Clear();           // Chetan -- 
			multipleSelectionForThumb = libSelectionList;*/

			if ( fPrevButtonClick > -1 ) {
				
				int32	firstIndex = fPrevButtonClick;
				int32	secondIndex = curWidgetIndex;
				if ( firstIndex > secondIndex )
				{//CA("if ( firstIndex > secondIndex )");
					firstIndex = curWidgetIndex;
					secondIndex = fPrevButtonClick;
				}
				if ( secondIndex > panelData->Length()-1 )
					secondIndex = panelData->Length() - 1;
				
				// Select everything in between the first and second index
				for ( i=firstIndex; i < secondIndex; i++ ) {
						
					IControlView*	view = panelData->GetWidget(i);
					InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
					buttonData->Select(kTrue, kFalse);
					InterfacePtr<ISubject> buttonSubject(buttonData, IID_ISUBJECT);
					if( buttonSubject )
						buttonSubject->Change( kTrueStateMessage, IID_IBOOLEANCONTROLDATA, (IEvent*)e);
				}
			}
		}

		// Select/Deselect 'the' button
		if ( !isNewSelection && cmdKeyDown )
		{	//CA("if - !isNewSelection && cmdKeyDown");
			buttonData->Deselect();
		}
		else
		{	//CA("else - !isNewSelection && cmdKeyDown");
			buttonData->Select(kTrue, kFalse);
			InterfacePtr<ISubject> buttonSubject(buttonData, IID_ISUBJECT);
			if( buttonSubject )
				buttonSubject->Change( kTrueStateMessage, IID_IBOOLEANCONTROLDATA, (IEvent*)e);
		}	
		fPrevButtonClick = curWidgetIndex;
		
		// Check if there are still items selected
		InterfacePtr<IListBoxController> libListData( this, IID_ILISTBOXCONTROLLER );
		K2Vector<int32> libSelectionList;
		libListData->GetSelected( libSelectionList );

		multipleSelectionForThumb.clear();           // Chetan -- 
		multipleSelectionForThumb = libSelectionList;

		if ( libSelectionList.Length() > 0 ) {

			ImageSelectionList = libSelectionList;  /// ImageSelectionList is global variable to store the current selected Indexes	
			// Check if we can drag
			InterfacePtr<IDragDropSource> dragSource(this, IID_IDRAGDROPSOURCE);
			if (dragSource && dragSource->WillDrag(e)) {
				
				// Check to see if user is trying to start a drag
				bool16 isPatientUser;
				SysRect dragExitGRect;
				ComputeDragExitRect(curWidgetIndex, dragExitGRect);
				if (::IsUserStartingDrag(e/*->GetSysEvent()*/, isPatientUser, dragExitGRect))
				{
					InterfacePtr<IDragDropController> ddController(/*gSession*/GetExecutionContextSession(), IID_IDRAGDROPCONTROLLER); //CS4
					if (ddController)
					{
						ddController->StartDrag(dragSource, e);	
						e->SetSystemHandledState(IEvent::kDontCall);
						
						// Notify any selection observers that the selection changed
						UpdateSelectionObservers();
						
						return kTrue; // Make up for the fact that StartDrag will swallow the mouse up
					}
				}
			}
		}
	}
	else
	{
		// Clicked on empty part of the grid panel, deselect all.
		fPrevButtonClick = -1;
		for (int32 i=0; i<panelData->Length(); i++)
		{
			IControlView*	view = panelData->GetWidget(i);
			InterfacePtr<IBooleanControlData>	buttonData( view, IID_IBOOLEANCONTROLDATA );
			if ( buttonData->IsSelected() )
				buttonData->Deselect();
		}
	}
	
	// Notify any selection observers that the selection changed
	UpdateSelectionObservers();
///////////////////
	//APSIVA 9 Comment
			//InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(/*kProductImageIFaceBoss*/kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
			//if(ptrImageHelper != nil)
			//{
			//	AcquireWaitCursor awc ;
			//	awc.Animate() ; 
			//	if(ptrImageHelper->isImagePanelOpen())
			//	{
			//		int32 objectId = pNodeDataList[CurrentSelectedProductRow].getPubId();

			//		if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 1)
			//			ptrImageHelper->showProductImages(objectId , global_lang_id , Mediator::parentID , Mediator::parentTypeID , Mediator::sectionID ) ;
			//		if(pNodeDataList[CurrentSelectedProductRow].getIsProduct() == 0)
			//			ptrImageHelper->showItemImages(objectId , global_lang_id , Mediator::parentID ,pNodeDataList[CurrentSelectedProductRow].getTypeId() , Mediator::sectionID, kFalse);
			//	}
			//}
/////////////////////
	return handled;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::ButtonDblClk
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::ButtonDblClk(IEvent* e) 
{	
		InterfacePtr<IPanelControlData> panelData( this, IID_IPANELCONTROLDATA );
		if(!panelData)
		{
			return kFalse;
		}
		IControlView*	widgetView = GetWidgetView(e);
		if (!widgetView ) 
		{
			return kFalse;
		}
		ImageSelectionList.clear();
		curWidgetIndex = panelData->GetIndex(widgetView);
		//PMString ASD("");
		//ASD.Append("curWidgetIndex : ");
		//ASD.AppendNumber(curWidgetIndex);
		//CA(ASD);
		//CAI("curWidgetIndex", curWidgetIndex);
//###########//////////////////////////////
		CurrentSelectedProductRow = curWidgetIndex;

		AcquireWaitCursor awc ;
		awc.Animate() ; 

// Apsiva 9 Comment
//		ProductSpray PrSpryObj ;
//		PrSpryObj.startSpraying() ;	
	


	return kTrue;	
}

//__________________________________________________________________
//	ComputeDragExitRect: Compute the bounds rect that triggers the start of a drag operation.
//		In this case we use the bounds of the grid item clicked on.
//__________________________________________________________________
void SPXLibraryItemGridEH::ComputeDragExitRect(int32 itemClicked, SysRect& exitGRect)
{
	// Get the list of currently selected library items
	ViewPortAccess<IWindowPort> windowPort(this, IID_IWINDOWPORT);
	InterfacePtr<const IListBoxController> libListData( this, IID_ILISTBOXCONTROLLER );
	InterfacePtr<const IControlView> gridView(this, IID_ICONTROLVIEW);
	InterfacePtr<const IPanelControlData> gridData( gridView, IID_IPANELCONTROLDATA );
		
	AcquireViewPort							aqViewPort(windowPort);

	// Get the bounding box of the grid button
	IControlView* button = gridData->GetWidget( itemClicked );
	SysRect buttonRect( button->GetBBox() );

	exitGRect = windowPort->LocalToGlobal(buttonRect);
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::GetWidgetView
//---------------------------------------------------------------

IControlView*	SPXLibraryItemGridEH::GetWidgetView(IEvent* e)
{
	ViewPortAccess<IWindowPort> windowPort(this, IID_IWINDOWPORT);
	AcquireViewPort aq(windowPort);
	
	InterfacePtr<IPanelControlData>	myData(this, IID_IPANELCONTROLDATA);

	SysPoint localWhere;
	IControlView	*widgetView;
	
	for(int32 i = myData->Length()-1; i  >= 0 ; --i)
	{
		widgetView = myData->GetWidget(i);
		SysPoint pt = e->GlobalWhere();
		localWhere = windowPort->GlobalToLocal(pt);
		if( widgetView->IsVisible() && widgetView->HitTest(localWhere) )
		{
			return widgetView;
		}
	}
	return nil;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::UpdateSelectionObservers
//---------------------------------------------------------------

void	SPXLibraryItemGridEH::UpdateSelectionObservers()
{
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::MouseMove
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::MouseMove(IEvent* e) 
{
	bool16 handled = kFalse;
	IEventHandler *eh = QueryWidgetEH(e);
	
	if(eh) 
	{
		handled = eh->MouseMove(e);
		eh->Release();
	}

	return handled;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::MouseDrag
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::MouseDrag(IEvent* e) 
{
	bool16 handled = kFalse;
	IEventHandler *eh = QueryWidgetEH(e);
	
	if(eh) 
	{
		handled = eh->MouseDrag(e);
		eh->Release();
	}

	return handled;
}


//---------------------------------------------------------------
// SPXLibraryItemGridEH::RButtonDn
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::RButtonDn(IEvent* e) 
{
	bool16 handled = kFalse;

	// Save away UID ref of event handler/widget so we can check its existence in double click.
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	fLastWidgetUIDRef = ::GetUIDRef(eh);
	fLastLButtonDnEventHandler = eh;	

	if(eh) 
	{
		handled = eh->RButtonDn(e);
	}
	
	return handled;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::LButtonUp
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::LButtonUp(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh)
	{
		handled = eh->LButtonUp(e);
	}
	
	return handled;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::RButtonUp
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::RButtonUp(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh)
		handled = eh->RButtonUp(e);
	
	return handled;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::ControlCmd
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::ControlCmd(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh) 
		handled = eh->ControlCmd(e);
	
	return handled;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::KeyDown
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::KeyDown(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh) 
		handled = eh->KeyDown(e);
	
	return handled;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::KeyCmd
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::KeyCmd(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh) 
		handled = eh->KeyCmd(e);
	
	return handled;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::KeyUp
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::KeyUp(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh) 
		handled = eh->KeyUp(e);
	
	return handled;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::Update
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::Update(IEvent* e) 
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));
	
	if(eh) 
		handled = eh->Update(e);
	
	return handled;
}

bool16 SPXLibraryItemGridEH::PlatformEvent(IEvent* e)
{
	bool16 handled = kFalse;
	InterfacePtr<IEventHandler> eh(QueryWidgetEH(e));

	if(eh) 
	{
		handled = eh->PlatformEvent(e);
	}
	return handled;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::ButtonTrplClk
//---------------------------------------------------------------

bool16 SPXLibraryItemGridEH::ButtonTrplClk(IEvent* e)
{
	InterfacePtr<IEventHandler> lastEH(QueryCachedWidgetEH());
	if (lastEH)
		return (lastEH->ButtonTrplClk(e));

	return kFalse;
}
 
//---------------------------------------------------------------
// SPXLibraryItemGridEH::QueryWidgetEH
//---------------------------------------------------------------

//NOTE: We assume drawing will walk the paneldata in increasing order. Hence, it is more efficient to walk
// the hittest in decreasing order, so that we retain the proper z-order.

IEventHandler	*SPXLibraryItemGridEH::QueryWidgetEH(IEvent* e)
{
	InterfacePtr<ISuppressedUI>	suppressedUI(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
	InterfacePtr<IControlView> panelView(this,UseDefaultIID());
	if(!panelView->GetEnableState() || (suppressedUI && suppressedUI->IsWidgetDisabled(panelView) == kTrue))
		return nil;

	ViewPortAccess<IWindowPort> windowPort(this, IID_IWINDOWPORT);
	AcquireViewPort aqViewPort(windowPort);

	InterfacePtr<IPanelControlData>	myData(this, IID_IPANELCONTROLDATA);
	SysPoint localWhere;
	IControlView	*widgetView;
	IEventHandler	*widgetEH;
	
	for(int32 i = myData->Length()-1; i  >= 0 ; --i)
	{
		SysPoint pt = e->GlobalWhere();
		localWhere = windowPort->GlobalToLocal(pt);
		widgetView = myData->GetWidget(i);
		if(widgetView->GetVisibleState() == kTrue && (!suppressedUI || suppressedUI->IsWidgetHidden(widgetView) == kFalse)) 
		{
			if(widgetView->GetEnableState() && widgetView->HitTest(localWhere) && (!suppressedUI || suppressedUI->IsWidgetDisabled(widgetView) == kFalse) ) 
			{
				SysWindow controlWindow = widgetView->GetSysWindow();
				/*bool16 testWidget = controlWindow == nil || controlWindow == e->GetSysWindow();
				if (testWidget) 
				{*/
					widgetEH = (IEventHandler *)widgetView->QueryInterface(IID_IEVENTHANDLER); // Released by caller
					return widgetEH;
				/*}*/
			}
		}
	}
	return nil;
}

//---------------------------------------------------------------
// SPXLibraryItemGridEH::ClearCachedChildEH
//---------------------------------------------------------------

void SPXLibraryItemGridEH::ClearCachedChildEH() 
{
	fLastWidgetUIDRef = UIDRef::gNull;
	fLastLButtonDnEventHandler = nil;
}

IEventHandler* SPXLibraryItemGridEH::QueryCachedWidgetEH()
{
	// Verify that we can still instantiate the control view from the cached reference.
	// It may have been deleted in some circumstances.
	IDataBase       *db = fLastWidgetUIDRef.GetDataBase();
	if (db && db->IsValidUID(fLastWidgetUIDRef.GetUID()))
	{
		InterfacePtr<IEventHandler> lastEH(fLastWidgetUIDRef, UseDefaultIID());
		if (lastEH)
		{
			InterfacePtr<IControlView> lastView(lastEH, UseDefaultIID());
			InterfacePtr<IPanelControlData> panelData(this, UseDefaultIID());
			if (panelData && lastView)
			{
				int32 index = panelData->GetIndex(lastView);
				if (index != -1)
				{
					lastEH->AddRef();
					return lastEH;
				}
			}
		}
	}

	// if we get here, it may be that the event handler we had cached was not persistent. 
	//	in that case, hand back the cached IEventHandler*, and hope that it hasn't gone
	//	out of scope or been deleted.
	if ((fLastWidgetUIDRef == UIDRef::gNull) && (fLastLButtonDnEventHandler != nil))
	{
		fLastLButtonDnEventHandler->AddRef();
		return fLastLButtonDnEventHandler;
	}

	return nil;
}

//---------------------------------------------------------------
// static ::GetWindow()
//---------------------------------------------------------------
IWindow *GetWindow(SPXLibraryItemGridEH *panel);
IWindow *GetWindow(SPXLibraryItemGridEH *panel)
{
	IWindow *w = (IWindow*)panel->QueryInterface(IID_IWINDOW);
	if (!w)
	{
		InterfacePtr<IWidgetParent> parent(panel, IID_IWIDGETPARENT);
		w = (IWindow*)parent->QueryParentFor(IID_IWINDOW);
	}
	return w;
}
////////////////////////// For Design Action ////////////////////
void SPXLibraryItemGridEH::SetDesignerActionThumbnail(IPanelControlData *iPanelCtrlData, PMString name, int32 isProduct, int32 DActionNo, int32 isStarred,int32 NewProduct)
{
	//CA(__FUNCTION__);
	do
	{
		IControlView *BlankIconView=iPanelCtrlData->FindWidget(kBlankIconWidgetID);
		if(BlankIconView==nil)
			break;

		IControlView *UpdateProductIconView=iPanelCtrlData->FindWidget(kUpdateProductIconWidgetID);
		if(UpdateProductIconView==nil)
			break;

		IControlView *NewProductIconView=iPanelCtrlData->FindWidget(kNewProductIconWidgetID);
		if(NewProductIconView==nil)
			break;

		IControlView *AddToSpreadIconView=iPanelCtrlData->FindWidget(kAddToSpreadIconWidgetID);
		if(AddToSpreadIconView==nil)
			break;

		IControlView *UpdateCopyIcon=iPanelCtrlData->FindWidget(kUpdateCopyIconWidgetID);
		if(UpdateCopyIcon==nil)
			break;

		IControlView *UpdateArtIcon=iPanelCtrlData->FindWidget(kUpdateArtIconWidgetID);
		if(UpdateArtIcon==nil)
			break;



		IControlView *UpdateItemTableIconView=iPanelCtrlData->FindWidget(kUpdateItemTableIconWidgetID);
		if(UpdateItemTableIconView==nil)
			break;

		IControlView *DeleteFromSpreadIconView=iPanelCtrlData->FindWidget(kDeleteFromSpreadIconWidgetID);
		if(DeleteFromSpreadIconView==nil)
			break;

		CharCounter Counter = -1;
		//CA("name = " + name);
		Counter = name.IndexOfString(":");
		PMString NumberString = name;
		PMString NameString("");

		if(Counter != -1)
		{
			PMString *TempString = NULL;	
			int32 ToatalLength = name.NumUTF16TextChars();
			PMString ZeroLengthString("");	
			PMString BlankString("");
			PMString NewBlankString("");

			if(Counter > 0)
				TempString = name./*SubWString*/Substring(0, Counter);	 //Cs4
			if(Counter == 0)
				TempString = &BlankString;

			NumberString.Clear();
			NumberString.Append(*TempString);

			//CA("NumberString = " + NumberString);

			PMString *RemaningString = NULL;
			if(Counter + 2 < ToatalLength)
			{
				RemaningString = name./*SubWString*/Substring(Counter+2);//Cs4
			//	CA("RemaningString : " + *RemaningString);
			}
			if(RemaningString != NULL)
			{
				NameString.Append((*RemaningString));
				//CA("After Appending RemaningString : " + (*TempString));
			}
			//CA("NameString = " + NameString );

		}

		IControlView* iView =iPanelCtrlData->FindWidget(kSPText2WidgetID);
		if(iView == nil)
		{
			//CA("no iView");
			break;
		}
		InterfacePtr<ITextControlData> txtData(iView,IID_ITEXTCONTROLDATA);
		if(txtData == nil)
		{
			//CA("no txtData");
			break;
		}
		
		txtData->SetString(NumberString);

		IControlView* iView3 =iPanelCtrlData->FindWidget(kSPText3WidgetID);
		if(iView == nil)
		{
			//CA("no iView");
			break;
		}
		InterfacePtr<ITextControlData> txtData3(iView3,IID_ITEXTCONTROLDATA);
		if(txtData3 == nil)
		{
			//CA("no txtData");
			break;
		}
		
		txtData3->SetString(NameString);


		//added on 11.2.5 NewIcons combinations
		IControlView *NewAddIconView=iPanelCtrlData->FindWidget(kNewAddIconWidgetID);
		if(NewAddIconView==nil)
			break;

		IControlView *NewUpdateIconView=iPanelCtrlData->FindWidget(kNewUpdateIconWidgetID);
		if(NewUpdateIconView==nil)
			break;

		IControlView *NewDeleteIconView=iPanelCtrlData->FindWidget(kNewDeleteIconWidgetID);
		if(NewDeleteIconView==nil)
			break;

		IControlView *CopyArtIconView=iPanelCtrlData->FindWidget(kCopyArtIconWidgetID);
		if(CopyArtIconView==nil)
			break;

		IControlView *CopyItemIconView=iPanelCtrlData->FindWidget(kCopyItemIconWidgetID);
		if(CopyItemIconView==nil)
			break;

		IControlView *ArtItemIconView=iPanelCtrlData->FindWidget(kArtItemIconWidgetID);
		if(ArtItemIconView==nil)
			break;

		IControlView *CopyArtItemIconView=iPanelCtrlData->FindWidget(kCopyArtItemIconWidgetID);
		if(CopyArtItemIconView==nil)
			break;

		IControlView *TableTextThumbIconView1=iPanelCtrlData->FindWidget(kSPTableStaticTextThumbWidgetID);               //15
		if(TableTextThumbIconView1==nil)
			break;

		IControlView *ProductTextThumbIconView1=iPanelCtrlData->FindWidget(kSPItemGroupStaticTextThumbWidgetID);         //(16);
		if(ProductTextThumbIconView1==nil)
			break;

		IControlView *ItemTextThumbIconView1=iPanelCtrlData->FindWidget(kSPItemStaticTextThumbWidgetID);                  //(17);
		if(ItemTextThumbIconView1==nil)
			break;

		IControlView *LeadingItemTextThumbIconView=iPanelCtrlData->FindWidget(kSPLeadingItemStaticTextThumbWidgetID);                  //(17);
		if(LeadingItemTextThumbIconView==nil)
			break;

		IControlView *SingleItemTextThumbIconView=iPanelCtrlData->FindWidget(kSPSingleItemStaticTextThumbWidgetID);                  //(17);
		if(SingleItemTextThumbIconView==nil)
			break;

		

//*******commented by mane

		/*IControlView *TableIconView=iPanelCtrlData->FindWidget(kTableIconWidgetID);
		if(TableIconView==nil)
			break;

		IControlView *ProductIconView=iPanelCtrlData->FindWidget(kProductIconWidgetID);
		if(ProductIconView==nil)
			break;

		IControlView *ItemIconView=iPanelCtrlData->FindWidget(kItemIconWidgetID);
		if(ItemIconView==nil)
			break;*/
		
		IControlView *StarPicturWidgetView=iPanelCtrlData->FindWidget(kWhiteStarButtonThumbWidgetID);
		if(StarPicturWidgetView==nil)
			break;

		IControlView *NewProductPicturWidgetView=iPanelCtrlData->FindWidget(kNewWhitePictureButtonThumbWidgetID);
		if(NewProductPicturWidgetView==nil)
		{
			CA("NewProductPicturWidgetView==nil");
			break;
		}

		InterfacePtr<IBooleanControlData> controlData(StarPicturWidgetView, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
		if(controlData==nil) {
			CA("SPPictureWidgetEH::LButtonDn - No IBooleanControlData found on this boss.");
			break;
		}

		InterfacePtr<IUIFontSpec> fontSpec(StarPicturWidgetView, IID_IUIFONTSPEC); // no kDefaultIID
		if(fontSpec==nil) {
			CA("PicIcoPictureWidgetEH::LButtonDn - No IUIFontSpec found on this boss.");
			break;
		}

		int32 rsrcID = 0;
		bool16 isPressedIn = controlData->IsSelected();
		if(isStarred == 0) // ONEsourceCase where Star will get hide		
		{
			//CA("isStarred == 0");
			StarPicturWidgetView->HideView();
		}
		else if(isStarred==2)  // Selectecd Star
		{//// Showing Green Star Icon	
			StarPicturWidgetView->ShowView();
			controlData->Select();
			rsrcID =fontSpec->GetHiliteFontID(); 
		}else if(isStarred == 1)  // Unselected Star	
		{// Showing White Star Icon
			StarPicturWidgetView->ShowView();
			controlData->Deselect();	
			rsrcID = fontSpec->GetFontID();
		}
		if(rsrcID != 0) {
			StarPicturWidgetView->SetRsrcID(rsrcID);		
		}

		InterfacePtr<IBooleanControlData> controlData1(NewProductPicturWidgetView, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
		if(controlData1==nil) {
			CA("SPPictureWidgetEH::LButtonDn - No controlData1 found on this boss.");
			break;
		}

		InterfacePtr<IUIFontSpec> fontSpec1(NewProductPicturWidgetView, IID_IUIFONTSPEC); // no kDefaultIID
		if(fontSpec1==nil) {
			CA("PicIcoPictureWidgetEH::LButtonDn - No IUIFontSpec1 found on this boss.");
			break;
		}

		int32 rsrcID1 = 0;
//		bool16 isPressedIn1 = controlData1->IsSelected();
		if(NewProduct == 0) 		
		{
			NewProductPicturWidgetView->HideView();
		}
		else if(NewProduct==2)  
		{
			NewProductPicturWidgetView->ShowView();
			controlData1->Select();
			rsrcID1 =fontSpec1->GetHiliteFontID(); 
		}else if(NewProduct == 1)  // Unselected Star	
		{
			NewProductPicturWidgetView->ShowView();
			controlData1->Deselect();	
			rsrcID1 = fontSpec1->GetFontID();
		}
		if(rsrcID1 != 0) {
			NewProductPicturWidgetView->SetRsrcID(rsrcID1);		
		}

		//ended on 11.2.5
//***************************commented by mane**********************
		/*if(isProduct == 2)
		{
			ItemIconView->HideView();
			ProductIconView->HideView();
			TableIconView->ShowView();
		}
		if(isProduct == 1)
		{
			TableIconView->HideView();
			ItemIconView->HideView();
			ProductIconView->ShowView();
		}
		if(isProduct == 0)
		{
			TableIconView->HideView();
			ProductIconView->HideView();
			ItemIconView->ShowView();
		}*/
//*********************************Added by mane***********
		if(isProduct == 2)
		{	
			//CA("pro==2");
			ItemTextThumbIconView1->HideView();
			ProductTextThumbIconView1->HideView();
			LeadingItemTextThumbIconView->HideView();
			SingleItemTextThumbIconView->HideView();
			TableTextThumbIconView1->ShowView();
		}
		if(isProduct == 1)
		{	
			//CA("pro==1");
			TableTextThumbIconView1->HideView();
			ItemTextThumbIconView1->HideView();
			LeadingItemTextThumbIconView->HideView();
			SingleItemTextThumbIconView->HideView();
			ProductTextThumbIconView1->ShowView();
		}
		if(isProduct == 0)
		{	
			//CA("pro==0");
			TableTextThumbIconView1->HideView();
			ProductTextThumbIconView1->HideView();
			LeadingItemTextThumbIconView->HideView();
			SingleItemTextThumbIconView->HideView();
			ItemTextThumbIconView1->ShowView();
		}
		if(isProduct == 3)
		{	
			TableTextThumbIconView1->HideView();
			ProductTextThumbIconView1->HideView();
			ItemTextThumbIconView1->HideView();
			SingleItemTextThumbIconView->HideView();
			LeadingItemTextThumbIconView->ShowView();
		}
		if(isProduct == 4)
		{	
			TableTextThumbIconView1->HideView();
			ProductTextThumbIconView1->HideView();
			ItemTextThumbIconView1->HideView();
			LeadingItemTextThumbIconView->HideView();
			SingleItemTextThumbIconView->ShowView();
		}
//********************************************

		switch(DActionNo)
		{
			case 0://for newproduct checked
					BlankIconView->ShowView();
					UpdateProductIconView->HideView();
					NewProductIconView->ShowView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();
		
					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 1:  // For AddProductToSpread checked
			
					BlankIconView->ShowView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->ShowView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();


					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;
			case 2:  // For UpdateSpread and UpdateCopy checked

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->ShowView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();


					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;

			case 3:  // For UpdateSpread and UpdateArtIcon Action

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->ShowView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;

			case 4:  // For UpdateSpread and UpdateItemTableIconView Action

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->ShowView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;

			case 5:  // For DeleteFromSpreadIconView Action

					BlankIconView->ShowView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->ShowView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;

			/*case 6:  // For Refresh Product Action

					PRIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->ShowView();
					break;*/
		//added by Yogesh on 11.2.5	New combinations of Icons.
			case 7:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->ShowView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;
			case 8:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->ShowView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;
			case 9:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->ShowView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 10:
					
					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->ShowView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;
			case 11:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->ShowView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 12:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->ShowView();
					CopyItemIconView->HideView();
					ArtItemIconView->ShowView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 13:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->ShowView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

				case 14:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->ShowView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;


				case 15:

					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->ShowView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

				case 16:

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->ShowView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

				case 17:

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->ShowView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 18:

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->ShowView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 19:

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->ShowView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 20:

					BlankIconView->HideView();
					UpdateProductIconView->ShowView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();

					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons

					break;

			case 21:
					//CA("Inside case 21");
					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView(); //og
					UpdateArtIcon->HideView(); // og
					UpdateItemTableIconView->HideView(); //og

						DeleteFromSpreadIconView->HideView();



					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->ShowView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
				break;

		//ended by Yogesh on 11.2.5 New combinations of Icosn
			default:
					BlankIconView->HideView();
					UpdateProductIconView->HideView();
					NewProductIconView->HideView();
					AddToSpreadIconView->HideView();
					UpdateCopyIcon->HideView();
					UpdateArtIcon->HideView();
					UpdateItemTableIconView->HideView();
					DeleteFromSpreadIconView->HideView();


					//added by Yogesh on 11.2.5 New combinations of Icons
					NewAddIconView->HideView();
					NewUpdateIconView->HideView();
					NewDeleteIconView->HideView();
					CopyArtIconView->HideView();
					CopyItemIconView->HideView();
					ArtItemIconView->HideView();
					CopyArtItemIconView->HideView();
					//ended by Yogesh on 11.2.5 New combinations of Icons


					break;
					
		}
	}while(0);
}

////////////////////////// End Design Action ////////////////////
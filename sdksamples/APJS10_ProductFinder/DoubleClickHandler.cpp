#include "VCPluginHeaders.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextControlData.h"
#include "IListBoxController.h"
#include "IWidgetParent.h"
#include "IDialogController.h"
#include "CEventHandler.h"
#include "EventUtilities.h"
#include "CAlert.h"
#include "SDKListBoxHelper.h"
#include "SPID.h"
#include "IListBoxController.h"
#include "MediatorClass.h"
#include "ProductSpray.h"
#include "AcquireModalCursor.h"
#include "IDataSprayer.h"
#include "SubSectionSprayer.h"
#include "ISubSectionSprayer.h"

#define CA(X) CAlert::InformationAlert(X)

#define CA_NUM(x) {PMString str;\
			str.AppendNumber(x);\
			CAlert::InformationAlert(str);}\

extern PublicationNodeList pNodeDataList;
extern PublicationNodeList orignalpNodeDataList;
extern int32 firstpNodeListCount;
extern bool16 singleSelectionSpray;
extern bool16 isCancelButtonClick;

class DoubleClickHandlerPF : public CEventHandler
{
public:
	DoubleClickHandlerPF(IPMUnknown *boss);	
	virtual ~DoubleClickHandlerPF(){}
	virtual bool16 ButtonDblClk(IEvent* e);
	virtual bool16 LButtonUp(IEvent *e); 
};

CREATE_PMINTERFACE(DoubleClickHandlerPF , DoubleClickPFEventHandlerImpl)

DoubleClickHandlerPF::DoubleClickHandlerPF(IPMUnknown *boss) :
	CEventHandler(boss)
{

}

bool16 DoubleClickHandlerPF::ButtonDblClk(IEvent* e)
{	//CA("Inside ButtonDblClk ");
	do
	{	
		Mediator md;
		SDKListBoxHelper listboxHelper(this , kSPPluginID);
		IControlView *controlView = listboxHelper.FindCurrentListBox(md.getMainPanelCtrlData() , 1);
		InterfacePtr<IListBoxController> list_box_controller(controlView , IID_ILISTBOXCONTROLLER);
		int32 num = list_box_controller->GetSelected();
		//--------------------------------------------------------------------------------------------//
		/*AcquireWaitCursor awc ;
		awc.Animate() ; 
		ProductSpray PrSpryObj ;
		PrSpryObj.startSpraying() ;	*/

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}

		PublicationNodeList temppNodeDataList = pNodeDataList;
		
		if(firstpNodeListCount == 0)
			orignalpNodeDataList = pNodeDataList;
		else
			temppNodeDataList = orignalpNodeDataList;


		singleSelectionSpray = kTrue;
		Mediator::setIsMultipleSelection(kTrue);

		//CA("kSelectionLength > 1");		
		pNodeDataList.clear();
		//for(int32 count = 0;count < kSelectionLength ; count++)
		{
			
			PublicationNode pNode = temppNodeDataList[num];
			pNodeDataList.push_back(pNode);
		}
		
		SubSectionSprayer SSsp;			
		SSsp.selectedSubSection.Append("Selected Product/Item");	
		InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
			if(iSSSprayer==nil)
			{
				ptrIAppFramework->LogInfo("AP46_ProductFinder::SPSelectionObserver::Update::kSPSprayButtonWidgetID::iSSSprayer==nil");
				return kFalse;
			}
		iSSSprayer->setSprayCustomProductOrItemListFlag(kTrue);
		SSsp.startSprayingSubSection();	
		
		firstpNodeListCount++;
		
		if(isCancelButtonClick == kFalse)
		{
			pNodeDataList.clear();
			//CurrentSelectedProductRow = num;//-100 ; //**********
			pNodeDataList = temppNodeDataList;
			iSSSprayer->setSprayCustomProductOrItemListFlag(kFalse);

			list_box_controller->DeselectAll();
			isCancelButtonClick = kFalse;
			break;
		}
		else 
		{
			isCancelButtonClick = kTrue;
			break;
		}

		InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
		if(!DataSprayerPtr)
		{
			//CA("DataSprayer Not Found");
			//ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::populateProductListforSection::No DataSprayerPtr");
			break;
		}
		DataSprayerPtr->resetLetterOrNumberkeySeqCount(1);
				
		//--------------------------------------------------------------------------------------------//
	}
	while(0) ;

	return CEventHandler::ButtonDblClk(e) ;
}

bool16 DoubleClickHandlerPF::LButtonUp(IEvent *e)
{
	//CA("Inside  LButtonUp ");
	do
	{
		Mediator md;
		SDKListBoxHelper listboxHelper(this , kSPPluginID);
		IControlView *controlView = listboxHelper.FindCurrentListBox(md.getMainPanelCtrlData() , 1);
		InterfacePtr<IListBoxController> list_box_controller(controlView , IID_ILISTBOXCONTROLLER);
		int32 num = list_box_controller->GetSelected();

	/*	PMString ASD("Selceted Row ; ");
		ASD.AppendNumber(num);
		CA(ASD);*/

	}while(0);
	return CEventHandler::LButtonUp(e);

}
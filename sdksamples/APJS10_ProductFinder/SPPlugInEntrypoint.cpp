#include "VCPlugInHeaders.h"
#include "SPPlugInEntrypoint.h"
#include "CAlert.h"
#include "SPID.h"
#include "SPLoginEventsHandler.h"
#include "LNGID.h"
#include "ILoginEvent.h"

#define CA(z) CAlert::InformationAlert(z)

#ifdef WINDOWS
	ITypeLib* SPPlugInEntrypoint::fSPTypeLib = nil;
#endif

bool16 SPPlugInEntrypoint::Load(ISession* theSession)
{
	//CA("SPPlugInEntrypoint::Load");
	bool16 retVal=kFalse;
	do
	{
		InterfacePtr<IRegisterLoginEvent> regEvt
		((IRegisterLoginEvent*) ::CreateObject(kLNGLoginEventsHandler,IID_IREGISTERLOGINEVENT));
		if(!regEvt)
		{
			//CA("Invalid regEvt");
			
			break;
		}
		regEvt->registerLoginEvent(kSPLoginEventsHandler); 
		//CA("Successful register in PSP");
	}while(kFalse);
	return kTrue;
}

/* Global
*/
static SPPlugInEntrypoint gPlugIn;

/* GetPlugIn
	The application calls this function when the plug-in is installed 
	or loaded. This function is called by name, so it must be called 
	GetPlugIn, and defined as C linkage. See GetPlugIn.h.
*/
IPlugIn* GetPlugIn()
{
	return &gPlugIn;
}

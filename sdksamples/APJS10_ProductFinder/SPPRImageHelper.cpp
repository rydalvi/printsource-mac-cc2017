#include "VCPlugInHeaders.h"
#include "IAppFramework.h"
#include "ICoreFilename.h"
#include "SDKUtilities.h"
#include "ICoreFilenameUtils.h"
#include "SPID.h"
#include "CAlert.h"
#include "SPPRImageHelper.h"
#include "IPanelControlData.h"
#include "IClientOptions.h"
#include "SDKListBoxHelper.h"
#include "K2Vector.h" 
#include "FileUtils.h"
#include "IApplication.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
#include "IWindow.h"
#include "IDataBase.h"
#include "IApplication.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
#include "IListBoxController.h"
#include "ILibrary.h"
#include "LibraryProviderID.h"
#include "IDFile.h"
#include "LocaleSetting.h"
#include "ISPXLibraryViewController.h"
#include "IWidgetParent.h"
#include "ITextControlData.h"
#include "PMString.h"
#include "ISysFileData.h"
#include "SDKFileHelper.h"
#include "IImageFormatManager.h"
#include "StreamUtil.h"
#include "IImageReadFormat.h"
#include "IImageAttributes.h"
#include "IImportProvider.h"

//extern IAppFramework* ptrIAppFramework;
//extern IClientOptions* ptrIClientOptions;

using namespace std ;

#define CA(x) CAlert::InformationAlert(x)

#define CAI(x) {PMString str;\
			str.AppendNumber(x);\
			CAlert::InformationAlert(str);}\
 

extern IPanelControlData *global_panel_control_data ;
extern ITextControlData* textControlDataPtr;
/*extern*/ bool16 BrowseFolderOption1 ;
/*extern*/ bool16 isBrowsePrintSource1;
K2Vector<PMString> imageType2 ;
K2Vector<PMString> vectorImageTypeTagName;
K2Vector<int32> imageTypeID2 ;
K2Vector<int32> imageMPVID ;
K2Vector<PMString> imageVector2 ;
K2Vector<bool16> imageBoolFlagVector;
PMString  imageType_ ;
//////int32 global_lang_id ;
//////int32 global_section_id ;
//////int32 global_parent_id ;
//////int32 global_parent_type_id ;
int32 globle_item_parenttype_id;
int32 global_item_start_position ;
K2Vector<int32> itemIDS ;
int32 global_product_id ;
int32 global_product_type_id ;

PMString imagePath("");
//PMString useName("");
K2Vector<PMString> imagefileName;
K2Vector<PMString> typeName;
extern bool16 isThumb;
int32 index =0; 
//Used to call showProducImages and ShowItem Images in DCNActionComponent
int32 objId=0;
int32 langaugeId=0;
int32 parId=0;
int32 parTypeId=0;
int32 sectId=0;
int32 isProd=0;
ISPPRImageHelper* prImage = NULL;

bool16 isProductImageAdded = kFalse;

namespace
{
	bool16 fileExists(PMString& path, PMString& name)
	{
		PMString theName("");
		theName.Append(path);
		theName.Append(name);

		const char *file= /*const_cast<char*>*/(theName.GrabCString()/*GetPlatformString().c_str()*/); //Cs4

		FILE *fp=NULL;

		fp=std::fopen(file, "r");
		if(fp!=NULL)
		{
			std::fclose(fp);
			return kTrue;
		}
		return kFalse;
	}
}

class SPPRImageHelper : public CPMUnknown<ISPPRImageHelper>
{
public :
	SPPRImageHelper();
	SPPRImageHelper(IPMUnknown*  boss);
	~SPPRImageHelper();
	virtual void showImagePanel() ;
	virtual void showProductImages(int32 objectID , int32 langID , int32 parentID , int32 parentTypeID , int32 sectionID , PMString name) ;
	virtual void positionImagePanel(int32 left , int32 righ , int32 top , int32 bottom);
	virtual void closeImagePanel();
	virtual void showItemImages(int32 objectID , int32 langID , int32 parentID , int32 parentTypeID , int32 sectionID, int32 isProduct , PMString name);
	virtual bool16 isImagePanelOpen();
	IPMUnknown* boss_ ;
	virtual bool16 EmptyImageGrid();
////////////////////////////added by vijay on 19/8/2006////////////////////////////////////	
	virtual void WrapperForAddThumbnailImage(PMString wrpperPath, PMString wrpperDisplayName);
private:
	 void AddThumbnailImage(PMString path, PMString DisplayName);
	 bool16 EmptyThumbnailImageGrid();


};

CREATE_PMINTERFACE(SPPRImageHelper , kSPProductImageIFaceImpl)

SPPRImageHelper::SPPRImageHelper(IPMUnknown* boss):CPMUnknown<ISPPRImageHelper>(boss){
	boss_ = boss ;
	}

SPPRImageHelper::~SPPRImageHelper(){}

void SPPRImageHelper :: showImagePanel()
{

}


void SPPRImageHelper :: showProductImages(int32 objectID , int32 langID , int32 parentID , int32 parentTypeID , int32 sectionID , PMString name )
{
	//CA("showProductImages");
	BrowseFolderOption1 = kFalse;
	isBrowsePrintSource1 = kTrue;

	PMString emptyImagePath("\\ImageNotFound.gif");
	PMString emptyImageName(name);
	bool16 continued = kFalse; 

	isProd = 1;
	objId=objectID;
	langaugeId=langID;
	parId=parentID;
	parId=parentTypeID;
	sectId=sectionID;
	isProductImageAdded = kFalse;
	InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
	if(app == NULL)
		return;
	/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
	if(paletteMgr == NULL)
		return;*/
	InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
	if(panelMgr == NULL)
		return;
	IControlView* myPanel = panelMgr->GetVisiblePanel(kSPPanelWidgetID);
	if(myPanel == NULL)
		return;

	InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
	if(panelControlData == NULL)
		return;
	////////// Chetan -- Commented coz following Control Views are not required in this plugin
	//////////IControlView* lstgridWidget = panelControlData->FindWidget( kDCNlstPnlWidgetID);
	//////////if(lstgridWidget == NULL)
	//////////	return;
	//////////IControlView* textView = panelControlData->FindWidget( kDCNMultilineTextWidgetID);
	//////////if(textView == NULL)
	//////////	return;
	//////////IControlView* listImageView =panelControlData->FindWidget( kDCNListViewImageWidgetID);
	//////////if(listImageView == NULL)
	//////////{
	//////////	//CA("No listImageView");
	//////////	return;
	//////////}
	//////////InterfacePtr<ISysFileData> iImageSysFile(listImageView, IID_ISYSFILEDATA);
	//////////if(!iImageSysFile)
	//////////{
	//////////	return;
	//////////}
	//////////InterfacePtr<ITextControlData> textPtr( textView ,IID_ITEXTCONTROLDATA);
	//////////if(textPtr == NULL)
	//////////	return;
	//////////PMString blank("");
	//////////textPtr->SetString(blank);
	//////////SDKListBoxHelper listHelper(lstgridWidget,kSPPluginID,kDCNlstviewListboxWidgetID, kDCNlstviewCustomPanelViewWidgetID);
	//////////listHelper.EmptyCurrentListBox();
	////////////...............................
	//////////IControlView* listBoxControlView = listHelper.FindCurrentListBox();
	//////////InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
	//////////	ASSERT_MSG(listCntl != nil, "listCntl nil");
	//////////	if(listCntl == nil) {  //CA("listCntl is nil");
	//////////		return;
	//////////}
	//////////  
	////////////...............................
	//this->EmptyImageGrid();
	imageType2.clear();
	vectorImageTypeTagName.clear();
	imageTypeID2.clear();
	imageMPVID.clear();
    //imageVector2.clear();
	itemIDS.clear();
	imageBoolFlagVector.clear();


	index=0;

	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions == nil){
		CAlert::ErrorAlert("Interface for IClientOptions not found.");
		return ;
	}
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return ;
	ptrIAppFramework->clearAllStaticObjects();
//	VectorTypeInfoPtr typeValObj1 = ptrIAppFramework->ElementCache_getImageAttributesByIndex(3);
//	if(typeValObj1 == nil)
//		return ;
	
//	int32 size = typeValObj1->size() ;
    int32 count = 0 ;
		
	VectorTypeInfoValue :: iterator it2 ;
	bool16 Flag1stSelected = kFalse; 

//	for(it2 = typeValObj1->begin() ; it2 != typeValObj1->end() ; it2++)
	{	

		//int32 Imagetypeid = it2->getType_id() ;				
		int32 objectId =  objectID ;			
		CObjectValue oVal ;
		oVal = ptrIAppFramework->GETProduct_getObjectElementValue(objectId);  // new added in appFramework
		int32 parentTypeID = oVal.getObject_type_id();
//CA("Before do");
		do{	
			
			//VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId,parentTypeID,Imagetypeid);
			//VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId,parentTypeID);
			VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_getThumbnailAssetByParentId(objectId);
			if(AssetValuePtrObj == NULL){
				continued = kTrue;
				//CA("Show Product images : AssetValuePtrObj == NULL");
				continue ;
			}
	
			if(AssetValuePtrObj->size() ==0){
				//CA("AssetValuePtrObj->size() ==0");
				continue ;			
			}
			//global_product_type_id = Imagetypeid ;
			
//			PMString typIDString("");
//			typIDString.AppendNumber(Imagetypeid);			
//			imageTypeID2.push_back(Imagetypeid);

			PMString ImageTypeName(name) ;
		/*	ImageTypeName.Append(ptrIAppFramework->GETProduct_getObjectAndObjectValuesByObjectId(objectID)) ;
			if(ImageTypeName=="")           Not required for Product Finder
				return;*/			

//			ImageTypeName.Append(" : ") ;
//			ImageTypeName.Append(it2->getName());
//			PMString ImageTypeName_ = it2->getName();
			
			int imageTextIndex = 0;			
			PMString fileName("");
			int32 assetID ;	

			PMString imagePath = ptrIClientOptions->getImageDownloadPath();
			if(imagePath != ""){
				//CA(imagePath);
				const char *imageP=(imagePath.GrabCString()/*GetPlatformString().c_str()*/); //CS4
				if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
					#ifdef MACINTOSH
						imagePath+="/";
					#else
						imagePath+="\\";
					#endif
			}

			if(imagePath == "")
				return ;			
			
			
			//CA("imagePathWithSubdir"+imagePathWithSubdir);
			VectorAssetValue::iterator it; // iterator of Asset value

			CAssetValue objCAssetvalue;
			for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
			{
				objCAssetvalue = *it;
				fileName = objCAssetvalue.getFile_name();
				//CA("fileName"+fileName);
				assetID = objCAssetvalue.getAsset_id();					

				if(fileName=="")
				continue ;

				int32 ImageTypeId = objCAssetvalue.getType_id();
				imageTypeID2.push_back(ImageTypeId);

				int32 image_mpv = objCAssetvalue.getMpv_value_id();
				imageMPVID.push_back(image_mpv);
			
				//PMString TypeName = ptrIAppFramework->TYPECACHE_getTypeNameById(ImageTypeId);
				//CA("TypeName : "+TypeName);
				PMString NewImageNameString("");
				NewImageNameString.Append(ImageTypeName);
				//NewImageNameString.Append(" : ") ;
				//NewImageNameString.Append(TypeName);
				//PMString ImageTypeName_ = TypeName;

				//CA("fileName : "+ fileName);
				//imageType_ = fileName;
				do{
					SDKUtilities::Replace(fileName,"%20"," ");
				}while(fileName.IndexOfString("%20") != -1);

				PMString imagePathWithSubdirNew(""); 
				imagePathWithSubdirNew.Append(imagePath);
				PMString typeCodeString;

				typeCodeString = ptrIAppFramework->ClientActionGetAssets_getProductAssetFolder(assetID);
				//CA("typeCodeString"+typeCodeString );				
				//Following three lines are commented and instead of that, code following that is added.
				/*if(typeCodeString.NumUTF16TextChars()!=0)
					imagePathWithSubdir.Append(typeCodeString);	

				FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));*/

				if(typeCodeString.NumUTF16TextChars()!=0)
				{ 
				
					PMString TempString = typeCodeString;		
					CharCounter ABC = 0;
					bool16 Flag = kTrue;
					bool16 isFirst = kTrue;
					do
					{
						
						if(TempString.Contains("/", ABC))
						{
			 				ABC = TempString.IndexOfString("/"); 				
						
		 					PMString * FirstString = TempString.Substring(0, ABC);		 		
		 					PMString newsubString = *FirstString;		 	
						 				 	
		 					if(!isFirst)
		 					imagePathWithSubdirNew.Append("\\");
						 	
		 					imagePathWithSubdirNew.Append(newsubString);		 	
		 					FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdirNew), kTrue);
						 	
		 					isFirst = kFalse;
						 	
		 					PMString * SecondString = TempString.Substring(ABC+1);
		 					PMString SSSTring =  *SecondString;		 		
		 					TempString = SSSTring;		 		
						}
						else
						{				
							if(!isFirst)
		 					imagePathWithSubdirNew.Append("\\");
						 	
		 					imagePathWithSubdirNew.Append(TempString);		 		
		 					FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdirNew), kTrue);		 		
		 					isFirst = kFalse;				
							Flag= kFalse;
							
							//CA(" 2 : "+ imagePathWithSubdir);
						}				
						
					}while(Flag);
				}

				SDKUtilities ::AppendPathSeparator(imagePathWithSubdirNew);
				//CA("imagePathWithSubdir"+imagePathWithSubdir + " fileName : "+ fileName);
			//	if(!fileExists(imagePathWithSubdirNew,fileName)){	
				if(ptrIAppFramework->getAssetServerOption() == 0)
				{
					if(!ptrIAppFramework->GETAsset_downLoadProductAsset(assetID,imagePathWithSubdirNew)){	
					{
						continued = kTrue;
							//CA("No fileExists");
							continue ;
						}
					}
				}
			//	}
				//CA(imagePathWithSubdir);
				if(!fileExists(imagePathWithSubdirNew,fileName)){
					continued = kTrue;
					//CA("No FileExixts");
					continue ; 
				}

				PMString ImagePath = imagePathWithSubdirNew + fileName;
				//CA("ImagePath" +ImagePath );
				imagefileName.push_back(fileName);//This Vector is used in DCNActionComponent to show filename.
				
				//PMString useName("FileName: "+ fileName + "  ");
				PMString useName(fileName + "  ");
				//useName.Append("ImageType: " + ImageTypeName);
				useName.Append(NewImageNameString);
				typeName.push_back(useName);//Added By Dattatray
				//IControlView* lstboxControlView = panelControlData->FindWidget(kImageListboxWidgetIDM);
				//CA("ImagePath::"+ImagePath);
				imageVector2.push_back(ImagePath);
				imageType2.push_back(NewImageNameString);
				itemIDS.push_back(-1); // ADDDING -1 BECAUSE THIS IS PRODUCT , IT CAN NOT HAVE ITEM ID 
				//vectorImageTypeTagName.push_back(ImageTypeName_);
				//listHelper.AddElementImage(lstboxControlView ,ImageTypeName ,ImagePath, kPRLCustomPanelViewWidgetIDM , 0,0,1);
				//CA("ImagePath for Product : "+ImagePath+"\n & Image Name : " + NewImageNameString );

				if(isThumb == kTrue){
					this->AddThumbnailImage(ImagePath, NewImageNameString);	
					break;
				}
			}
//12-april
			if(AssetValuePtrObj)
				delete AssetValuePtrObj;
//12-april
	}while(0);

	if(continued )
	{
		imageVector2.push_back(emptyImagePath);
		this->AddThumbnailImage (emptyImagePath, emptyImageName );
	}
//CA("After do-while()");
	} 
	
	

		global_item_start_position = imageVector2.size() - 1 ;
//CA("Before showItemImages");
//		this->showItemImages(objectID ,langID , parentID , parentTypeID , sectionID, kTrue) ;    
}




void SPPRImageHelper :: positionImagePanel(int32 TopBound, int32 LeftBound, int32 BottomBound, int32 RightBound)
{

}

void SPPRImageHelper :: closeImagePanel()
{

}


void SPPRImageHelper::showItemImages(int32 objectID , int32 langID , int32 parentID , int32 parentTypeID , int32 sectionID, int32 isProduct , PMString name)
{
	//CA("showItemImages");
	//int32 index=0;
	PMString emptyImagePath("\\ImageNotFound.gif");
	PMString emptyImageName(name);
	bool16 continued = kFalse; 
	isProd=isProduct;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return ;
//CA("1.1");
	ptrIAppFramework->clearAllStaticObjects();
	/////////////For Item file name in the listBox////////////////////////////////////////
	InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
	if(app == NULL)
		return;
	/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
	if(paletteMgr == NULL)
		return;*/
	InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
	if(panelMgr == NULL)
		return;
	IControlView* myPanel = panelMgr->GetVisiblePanel(kSPPanelWidgetID);
	if(myPanel == NULL)
		return;
//CA("1.2");
	InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
	if(panelControlData == NULL)
		return;

	VectorLongIntPtr itemids = NULL;
	VectorLongIntValue singleItemvect;
	singleItemvect.clear();

	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions == nil)
	{
		CAlert::ErrorAlert("Interface for IClientOptions not found.");
		return ;
	}
//CA("1.3");
	if(isProduct == 1)
	{		
		itemids = ptrIAppFramework->GETProjectProduct_getAllItemIDsFromTables(objectID, sectionID);
		if(itemids == NULL){ //CA("itemids == NULL");
			return;
		}
		if(itemids->size() == 0){// CA("itemids->size() == 0");
			return;
		}
	}
	if(isProduct == 0)
	{
		BrowseFolderOption1 = kFalse;
		isBrowsePrintSource1 = kTrue;

//CA("1.4");
		isProductImageAdded = kFalse;
	
		imageType2.clear();
		vectorImageTypeTagName.clear();
		imageTypeID2.clear();
		imageMPVID.clear();

		itemIDS.clear();
		imageBoolFlagVector.clear();
	
		global_item_start_position =-1;

		objId=objectID;
		langaugeId=langID;
		parId=parentID;
		parId=parentTypeID;
		sectId=sectionID;
	
		singleItemvect.push_back(objectID);
		itemids =  &singleItemvect;
		index =0; 

		imagePath = ptrIClientOptions->getImageDownloadPath();
		//CA(imagePath);
//CA("1.5");
	}

	VectorLongIntValue::iterator itn ;
	
//	int32 size = typeValObj_iamge->size() ;
	int32 count = 0 ;

//	VectorTypeInfoValue::iterator it2;

	PMString Typecode = ptrIAppFramework->GetStaticFieldStringFromInterface("com/apsiva/commonframework/Types", "PARENT_PRODUCT_TYPE");
	int32 parentTypeID1 = ptrIAppFramework->TYPECACHE_getTypeByCode("PARENT_PRODUCT_TYPE");
	globle_item_parenttype_id = parentTypeID1;

	bool16 Flag1stSelected = kFalse;
	
//CA("1.6");
	for(itn = itemids->begin() ; itn != itemids->end() ; itn++)
	{//CA("For Loop");
	//	for(it2 = typeValObj_iamge->begin() ; it2 != typeValObj_iamge->end() ; it2++)
		{	
				//int32 Imagetypeid = it2->getType_id() ;
				do{	
					//VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(*itn , parentTypeID1 , Imagetypeid);
					VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_getThumbnailAssetByParentId(*itn );
					if(AssetValuePtrObj == NULL){
						continued = kTrue;
						//CA("Show item images : AssetValuePtrObj == NULL ");
						continue ;
					}
//CA("1");
					if(AssetValuePtrObj->size() ==0){
						//CA(" AssetValuePtrObj->size() == 0 ");
						continue ;
					}

					//CA("AssetValuePtrObj->size() : ");
					//CAI(AssetValuePtrObj->size());

					PMString imagePath = ptrIClientOptions->getImageDownloadPath();
					if(imagePath != ""){
						const char *imageP=(imagePath.GrabCString()/*GetPlatformString().c_str()*/);//cs4
						if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
							#ifdef MACINTOSH
								imagePath+="/";
							#else
								imagePath+="\\";
							#endif
						
					}
//CA("2");
					if(imagePath == ""){
						return ;					
					}
					PMString imagePathWithSubdir = imagePath;


				//	PMString typIDString("");
				//	typIDString.AppendNumber(Imagetypeid);
					
					//imageTypeID2.push_back(Imagetypeid); //-- commented for changing place ..

					//CItemModel itemModel = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(*itn , langID);
				
					PMString ImageTypeName = name;  					// = itemModel.getItemNo() ;
				/*	ImageTypeName.Append(" : ");
					ImageTypeName.Append(it2->getName());
					PMString ImageTypeName_ = it2->getName();*/					

					/*PMString imageCode = it2->getCode();*/
					int imageTextIndex = 0;
					
					PMString fileName("");
					int32 assetID ;
					CObjectValue oVal ;
					int32 objectId =  objectID ;
				
					VectorAssetValue::iterator it; // iterator of Asset value

					CAssetValue objCAssetvalue;
					for(it = AssetValuePtrObj->begin() ; it != AssetValuePtrObj->end() ;it++)
					{
						objCAssetvalue = *it;
						fileName = objCAssetvalue.getFile_name();
						//CA(" file name is " + fileName);
						assetID = objCAssetvalue.getAsset_id();					

						if(fileName == ""){
							continue ;
						}
//CA("3");
						imageType_ = fileName;

						int32 Imagetypeid = objCAssetvalue.getType_id();
						imageTypeID2.push_back(Imagetypeid);
						int32 image_mpv = objCAssetvalue.getMpv_value_id();
						imageMPVID.push_back(image_mpv);

						//PMString TypeName = ptrIAppFramework->TYPECACHE_getTypeNameById(Imagetypeid);
						//CA("TypeName : "+TypeName);
						PMString NewImageNameString("");
						NewImageNameString.Append(ImageTypeName);
					/*	NewImageNameString.Append(" : ") ;
						NewImageNameString.Append(TypeName);
						PMString ImageTypeName_ = TypeName;*/


						do{
							SDKUtilities::Replace(fileName,"%20"," ");
						}while(fileName.IndexOfString("%20") != -1);

						PMString typeCodeString;
						PMString NewImagePathWithSubDirectory = imagePathWithSubdir;

						typeCodeString = ptrIAppFramework->ClientActionGetAssets_getItemAssetFolder(assetID);

						//CA(typeCodeString);
						//Following three lines are commented and instead of that, code following that is added.
						//if(typeCodeString.NumUTF16TextChars()!=0)
						//	imagePathWithSubdir.Append(typeCodeString);	
//CA("4");
						//FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&imagePathWithSubdir));
						if(typeCodeString.NumUTF16TextChars()!=0)
							{ 
							
								PMString TempString = typeCodeString;		
								CharCounter ABC = 0;
								bool16 Flag = kTrue;
								bool16 isFirst = kTrue;
								do
								{
									
									if(TempString.Contains("/", ABC))
									{
			 							ABC = TempString.IndexOfString("/"); 				
								 	
		 								PMString * FirstString = TempString.Substring(0, ABC);		 		
		 								PMString newsubString = *FirstString;		 	
								 				 		
		 								if(!isFirst)
		 								NewImagePathWithSubDirectory.Append("\\");
								 		
		 								NewImagePathWithSubDirectory.Append(newsubString);		 	
		 								FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory), kTrue);
								 		
		 								isFirst = kFalse;
								 		
		 								PMString * SecondString = TempString.Substring(ABC+1);
		 								PMString SSSTring =  *SecondString;		 		
		 								TempString = SSSTring;		 		
									}
									else
									{				
										if(!isFirst)
		 								NewImagePathWithSubDirectory.Append("\\");
								 		
		 								NewImagePathWithSubDirectory.Append(TempString);		 		
		 								FileUtils::CreateFolderIfNeeded(SDKUtilities::PMStringToSysFile(&NewImagePathWithSubDirectory), kTrue);		 		
		 								isFirst = kFalse;				
										Flag= kFalse;										
										//CA(" 2 : "+ imagePathWithSubdir);
									}										
								}while(Flag);
							}
//CA("5");
							SDKUtilities ::AppendPathSeparator(NewImagePathWithSubDirectory);
							//CA("Item : "+ NewImagePathWithSubDirectory +"   File Name :"+ fileName);
						//	if(!fileExists(NewImagePathWithSubDirectory,fileName))
						//	{
							if(ptrIAppFramework->getAssetServerOption() == 0)
							{
								if(!ptrIAppFramework->GETAsset_downLoadItemAsset(assetID,NewImagePathWithSubDirectory))
								{	
									//CA("!GETAsset_downLoadItemAsset");
									continued = kTrue;
									//CAI(__LINE__);
									continue ;
								}
							}
						//	}
//CA("6");
							if(!fileExists(NewImagePathWithSubDirectory,fileName))	
							{
								continued = kTrue;
								//CAI(__LINE__);
								continue ; 
							}					
//CA("7");
							PMString ImagePath = NewImagePathWithSubDirectory + fileName;

							imageVector2.push_back(ImagePath);
							/*int32 size=imageVector2.size();
							PMString sz;
							sz.AppendNumber(size);
							CA(sz);*/


							imageType2.push_back(/*ImageTypeName_*/NewImageNameString);
							//vectorImageTypeTagName.push_back(ImageTypeName_);							
							itemIDS.push_back(*itn);
														
							imagefileName.push_back(fileName);//This Vector is used in DCNActionComponent to show filename.

							//PMString useName("FileName: "+ fileName + "  ");
							PMString useName(fileName + "  ");
							//useName.Append("ImageType: " + ImageTypeName);
							useName.Append(NewImageNameString);
							typeName.push_back(useName);//Added By Dattatray

							if(isThumb == kTrue){
								this->AddThumbnailImage(ImagePath,NewImageNameString );
								break;
							}
					}

//12-april
					if(AssetValuePtrObj)
						delete AssetValuePtrObj;
//12-april
				}
				while(0);
			if(continued )
			{
				imageVector2.push_back(emptyImagePath);
				this->AddThumbnailImage (emptyImagePath, emptyImageName );
			}				

		} // ---- for inner 
							
	}


//CA("1.7");


	//////////// Chetan -- Commented coz following condition will never occur here
	////////////if(isThumb == kFalse && Flag1stSelected == kTrue  )//To Show AutoSelect FileName in DCNLstViewMultilineTextWidget
	////////////{
	////////////	K2Vector<PMString> :: iterator listItr;
	////////////	listItr =typeName.begin();
	////////////	PMString listText = *listItr;
	////////////	int ins =  listText.IndexOfString("  ");
	////////////	PMString* NewString = listText.Substring(ins +2 );
	////////////	//CA(*NewString);
	////////////	textPtr->SetString(*NewString);
	////////////	K2Vector<PMString> ::iterator itr;
	////////////	itr = imageVector2.begin();
	////////////	PMString dataFile = *itr;
	////////////	SDKFileHelper fileHelper(dataFile);
	////////////	IDFile xFile = fileHelper.GetIDFile();
	////////////	iImageSysFile->Set(xFile);
	////////////	listImageView->Invalidate();			

	////////////}
	////////////else if(isThumb == kFalse && Flag1stSelected == kFalse && isProductImageAdded == kFalse)
	////////////{
	////////////	//CA("Empty");
	////////////	textPtr->SetString("");
	////////////	SDKFileHelper fileHelper("C:\\asd.jpg");
	////////////	IDFile xFile = fileHelper.GetIDFile();
	////////////	iImageSysFile->Set(xFile);
	////////////	listImageView->Invalidate();	
	////////////}
}


bool16 SPPRImageHelper :: isImagePanelOpen()
{
	//CA(__FUNCTION__);
	bool16 result = kFalse ;

	return result ;
}

void SPPRImageHelper::AddThumbnailImage(PMString path, PMString DisplayName)
{//CA(__FUNCTION__);
	do{
		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if ( app == NULL ) break;
		
		/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
		if ( paletteMgr == NULL ) break;
		*/
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if ( panelMgr == NULL ) break;
	
		IControlView* myPanel = panelMgr->GetVisiblePanel(kSPPanelWidgetID);
		if ( !myPanel ){
			//CA("toms  \001\004Panel not visible\001\004\n");
			break;
		}
		
		InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
		if (!panelControlData) 
		{
			//CA("toms \001\004NO IPanelControlData\001\004\n");
			break;
		}

		IControlView* gridWidget = panelControlData->FindWidget( kSPXLibraryItemGridWidgetId );
		if ( gridWidget == NULL )
		{
			//CA("toms \001\004FAILED to find widgets\001\004\n");
			break;
		}
		
		InterfacePtr<ISPXLibraryViewController> gridController( gridWidget,IID_ISPXLIBRARYVIEWCONTROLLER );
		if ( gridController == NULL )
		{
			TRACEFLOW("toms"," \001\004NO IXLibraryViewController\001\004\n");
			break;
		}

			// make up a name for the item
			//sprintf(buf,"Item %d",itemCount);						// Support code commented
			//PMString UseName(buf,-1,PMString::kNoTranslate);
			//CA(path);
			//PMString FileName("");
			//FileUtils::GetFileName(path, FileName);
			//imagefileName.push_back(FileName);//This Vector is used in DCNActionComponent to show filename.

			//PMString useName("");
			//PMString userName(DisplayName); 
			//if(	(BrowseFolderOption1 == kTrue) && (isBrowsePrintSource1 == kFalse))
			//{
			//	useName.Append(FileName);
			//}
			//else if((BrowseFolderOption1 == kFalse) && (isBrowsePrintSource1 == kTrue))
			//{
			//	
			//	useName.Append(FileName + "  ");
			//	useName.Append(DisplayName);
			//}
			PMString useName("");
			useName.Append(DisplayName);

			do
			{
				SDKFileHelper fileHelper(path);//("C:\\ais6.jpg");
				IDFile xFile = fileHelper.GetIDFile();

				InterfacePtr<IPMStream>
				iFileStream(StreamUtil::CreateFileStreamRead(xFile));
				if(iFileStream == nil)
				{	//CA("File Stream nil");
					break;
				}

				// check if there is an image format that recognizes this image file
				InterfacePtr<IImageFormatManager>
					iImageFormatManager((IImageFormatManager*)::CreateObject(
					kImageFormatManager, IID_IIMAGEFORMATMANAGER));
				//ASSERT(iImageFormatManager);
				if (iImageFormatManager == nil)
				{	//CA("image format manager nil");
					break;
				}

				InterfacePtr<IImageReadFormat> iImageReadFormat(
					iImageFormatManager->QueryImageReadFormat(iFileStream));
				// Don't assert here: if we can't get this interface, it just
				// means it's a format that we can't preview. Fair enough, fail quietly
				if (iImageReadFormat == nil)
				{	//CA("Cant read image Due to low Quality");
					imageBoolFlagVector.push_back(kTrue);					
					break;
				}

				InterfacePtr<IImageAttributes>
				iSrcAttr((IImageAttributes*)::CreateObject(kImageObject,IID_IIMAGEATTRIBUTES));
				//ASSERT(iSrcAttr);
				if(iSrcAttr == nil)
				{	//CA("Attrbute nil");
					break;
				}

				if (iImageReadFormat->CanRead(iFileStream) == kFalse)
				{	//CA("image format nil");
					break;
				}
				iImageReadFormat->Open(iFileStream);
				iImageReadFormat->GetImageAttributes(iSrcAttr);

				uint32 nWidthSrc, nHeightSrc/*, nWidthDst, nHeightDst*/;

				iSrcAttr->GetTag(IImageAttributes::kPMTagImageWidth, &nWidthSrc);
				iSrcAttr->GetTag(IImageAttributes::kPMTagImageHeight, &nHeightSrc);
				if ((nWidthSrc <= 0) || (nHeightSrc <= 0))
				{	//CA("image hieght not supported");
					break;
				}

				//PMString ASD("nWidthSrc : " );
				//ASD.AppendNumber(nWidthSrc);
				//ASD.Append("  nHeightSrc : ");
				//ASD.AppendNumber(nHeightSrc);
				//CA(ASD);

				if(nWidthSrc> 150)
					imageBoolFlagVector.push_back(kTrue);	
				else
					imageBoolFlagVector.push_back(kFalse);	

			}while(0);
//CA("UseName = "+useName);	
			IControlView* newGridItem = gridController->CreateViewItemWidget( useName );
			if ( newGridItem != NULL )
			{
				//CA(" newGridItem != NULL ");

				gridController->InsertViewItemWidgetAndUpdate( newGridItem, kTrue, kTrue , path);
				
				gridController->UpdateView();
			
				newGridItem->Release();
			
				//CA("toms created and added item: %s\n " );
			}
			else
			{
				//CA("toms \001\004FAILED to create new item\001\004\n");
			}
	}
	while ( false );
}

bool16 SPPRImageHelper::EmptyThumbnailImageGrid()
{
//	CA(__FUNCTION__);
	imagefileName.clear(); //This Vector is used in DCNActionComponent i.e. it is global so when product selection chages in productFinder plugin,it is cleared and filled with new filename.
	typeName.clear();
	bool16 Flag = kFalse;
	do{

		if(imageVector2.size() <= 0)
		{
			//CA("imageVector2.size() <= 0");
			Flag = kTrue;
			break;
		}
	
		InterfacePtr<IApplication> app(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if ( app == NULL ) break;
		
		/*InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
		if ( paletteMgr == NULL ) break;*/
		
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		if ( panelMgr == NULL ) break;

		IControlView* myPanel = panelMgr->GetVisiblePanel(kSPPanelWidgetID);
		if ( !myPanel ) 
		{
			//CA("toms \001\004Panel not visible\001\004\n");
			break;
		}
	
		InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
		if (!panelControlData) 
		{
			//CA("toms \001\004NO IPanelControlData\001\004\n");
			break;
		}
		
		IControlView* gridWidget = panelControlData->FindWidget( kSPXLibraryItemGridWidgetId );
		if ( gridWidget == NULL )
		{
			//CA("toms \001\004FAILED to find widgets\001\004\n");
			break;
		}

		InterfacePtr<ISPXLibraryViewController> gridController( gridWidget,IID_ISPXLIBRARYVIEWCONTROLLER );
		if ( gridController == NULL )
		{
			//CA("toms \001\004NO IXLibraryViewController\001\004\n");
			break;
		}

		for(int i=imageVector2.size()-1; i>=0; i--)
		{
			gridController->DeleteViewItemWidgetAndUpdate(i);
		}

		Flag = kTrue;		

	}while(0);

	return Flag;
}

bool16 SPPRImageHelper::EmptyImageGrid(){
//CA(__FUNCTION__);
	this->EmptyThumbnailImageGrid();
	//////// Chetan -- Commented coz nothing is required written in the do-while block below
	////////do
	////////{
	////////	InterfacePtr<IApplication> app(gSession->QueryApplication());
	////////	if(app == NULL) 
	////////	{ 
	////////	//CA("No Application");
	////////	break;
	////////	}
	////////		
	////////	InterfacePtr<IPaletteMgr> paletteMgr(app->QueryPaletteManager());
	////////	if(paletteMgr == NULL) 
	////////	{ //CA("No IPaletteMgr");	
	////////		break;
	////////	}
	////////	
	////////	InterfacePtr<IPanelMgr> panelMgr(paletteMgr, UseDefaultIID());
	////////	if(panelMgr == NULL) 
	////////	{	//	CA("No IPanelMgr");	
	////////		break;
	////////	}
	////////	
	////////	IControlView* myPanel = panelMgr->GetVisiblePanel(kSPPanelWidgetID);
	////////	if(!myPanel) 
	////////	{	//CA("No PnlControlView");
	////////		break;
	////////	}
	////////	
	////////	InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
	////////	if(!panelControlData) 
	////////	{
	////////		//CA("No PanelControlData");
	////////		break;
	////////	}
	////////		
	////////	IControlView* iView =panelControlData->FindWidget(kDCNMultilineTextWidgetID);
	////////	if(iView == nil)
	////////	{
	////////		//CA("no iView");
	////////		break;
	////////	}

	////////	InterfacePtr<ITextControlData> txtData(iView,IID_ITEXTCONTROLDATA);
	////////	if(txtData == nil)
	////////	{
	////////		//CA("no txtData");
	////////		break;
	////////	}	
	////////	txtData->SetString(" ");
	////////}while(0);
	return (kTrue);
}

//////////////////////////////added by vijay on 19/8/2006////////////////////////////////////////
//This method is wrapper method for AddThumbnailImage
 void SPPRImageHelper::WrapperForAddThumbnailImage(PMString wrpperPath, PMString wrpperDisplayName)
 {	
	this->AddThumbnailImage(wrpperPath , wrpperDisplayName);
 
 }

/////////////////////////////////////////////////////////////////////////////////////////////////
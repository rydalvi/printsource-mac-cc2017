//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__


// Japanese string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_jaJP)
{
        k_jaJP,	// Locale Id
        0,		// Character encoding converter

        {
        	kSPCompanyKey,					kSPCompanyValue,
            kSPAboutMenuKey,					kSPPluginName "[US]...",
            kSPPluginsMenuKey,				"Content Sprayer",
			//kSPMenuItem1MenuItemKey,	"MenuItem1",
			kSPDialogMenuItemKey,			"Show dialog",
			kPSPPluginsMenuPath,				"&PRINTsource7",
			kSPHybridTableOnlyAboutMenuKey,		"Tables Only",
			kSPProductOnlyAboutMenuKey,			"Item Groups Only",
			kSPItemOnlyAboutMenuKey,			"Items Only",
			kSPALLMenuKey,						"All",
			kSPLeadingItemMenuKey,				"Leading Items Only",
			kSPThumbViewMenuKey,			"Thumbnail View",
			kSPListViewMenuKey,		"List View",
			kSPTemplateInfoMenuKey,			"Template Info",
	
            kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_enUS,

            // ----- Command strings

            // ----- Window strings

            // ----- Panel/dialog strings
            kSPPanelTitleKey,			"Content Sprayer",	//kSPPluginName "[US]",
            kSPStaticTextKey,			"HI",	        //kSPPluginName "[US]",
			kSPDialogTitleKey,           "Filter Product",                //kSPPluginName "[US]",

	// ----- Misc strings
            kSPAboutBoxStringKey,			kSPPluginName " [US], version " kSPVersion " by " kSPAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_enUS,
			//kSPMenuItem1StringKey,	"ProductFinderPalette::MenuItem1[US]",
			//kPSPPluginsMenuPath,				"Main:&PRINTsource7",
			//kSPHybridTableOnlyAboutMenuKey,		"Tables Only",
			//kSPProductOnlyAboutMenuKey,			"Item Groups Only",
			//kSPItemOnlyAboutMenuKey,			"Items Only",
			//kSPALLMenuKey,						"All",
			kSPBlankStringKey, "",
			kSectionStringKey, "Section: ",
			kSubSectionStringKey, "Sub-Section: ",
			kSelectCategoy, "Select Category",
			kSearchResultStringkey, "Search Result",
			kSPSprayItemPerFrameStringKey, "SprayItemPerFrame", 
			kSPHorizontalFlowStringKey, "HorizontalFlow",
			kLibraryItemViewStringKey, "Library Item View",
			kSPTableStringKey, "Table",
			kItemGroupStringKey, "Item Group",
			kItemStringkey, "Item",
			kLeadingItemStringKey, "Leading Item",
			kSingleItemStringKey, "Single Item",
			kTwentyStringKey, "(20)",
			kSelectProductFilterCriteriaStringKey, "Select the Product Filter Criteria: ",
			kSPSelectStringKey, "--Select--",								
			kSpraySectionStringKey, "Spray Section",
			kSprayStringKey, "Spray",
			kListTablesStringKey, "Lists & Tables",
			kSPPrintsourcetStringKey,				"&Catsy",
        }

};

#endif // __ODFRC__

#ifndef __REFRESHDATA_H__
#define __REFRESHDATA_H__

#include "VCPluginHeaders.h"
#include <vector>

using namespace std;

class RefreshData
{
public:
	UIDRef BoxUIDRef;
	int32 StartIndex;
	int32 EndIndex;
	UID boxID;
	double objectID;
	double publicationID;
	double elementID;
	PMString name;
	bool16 isSelected;
	bool16 isObject;
	bool16 isTaggedFrame;
	bool16 isProcessed;
	bool16 isTableFlag;
	bool16 isImageFlag;
	double TypeID;
	int32 whichTab;
	int32 DesignerAction;
	double PBObjectID;


	RefreshData()
	{
		boxID=kInvalidUID;
		objectID=-1;
		publicationID=-1;
		elementID=-1;
		isSelected=kFalse;
		isObject=kFalse;
		isTaggedFrame=kFalse;
		isProcessed=kFalse;
		isTableFlag=kFalse;
		StartIndex=0;
		EndIndex=0;
		TypeID=-1;
		whichTab=-1;
		isImageFlag=kFalse;
		DesignerAction=-1;
		PBObjectID=-1;

	}
};

typedef vector<RefreshData> RefreshDataList;

#endif

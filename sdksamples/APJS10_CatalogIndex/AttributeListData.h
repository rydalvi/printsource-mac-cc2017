#ifndef __AttributeListData_H__
#define __AttributeListData_H__

#include "VCPlugInHeaders.h"
#include "vector"

using namespace std;

class AttributeListData
{
	public:	
		int16 index;
		double id;
		double typeId;
		PMString tableCol;
		PMString name;			
		PMString code;
		bool16 isImageFlag; // kFalse:Text box kTrue:Image box
		int32	tableFlag;	// for Tables // 0: No table 1:Table
		int16 catLevel; //for Event,Catagory level attributes

		AttributeListData()
		{
			index=0;
			id=0;
			typeId=0;			
			isImageFlag=kFalse;
			tableFlag=0;
			catLevel = -1; 
		}
};

typedef vector<AttributeListData> vectorAttributeListData;

#endif
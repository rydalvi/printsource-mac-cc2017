//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__


// Japanese string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_jaJP)
{
        k_jaJP,	// Locale Id
        0,		// Character encoding converter

        {
        	// ----- Menu strings
                kCTICompanyKey,					kCTICompanyValue,
                kCTIAboutMenuKey,					kCTIPluginName "[JP]...",
                kCTIPluginsMenuKey,				kCTIPluginName "[JP]",
				kCTIDialogMenuItemKey,			"Show dialog[JP]",

                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_jaJP,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings
					kCTIDialogTitleKey,     kCTIPluginName "[JP]",

              // ----- Error strings

                // ----- Misc strings
                kCTIAboutBoxStringKey,			kCTIPluginName " [JP], version " kCTIVersion " by " kCTIAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_jaJP,

        }

};

#endif // __ODFRC__

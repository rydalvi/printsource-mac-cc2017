#ifndef _CREATEINDEX_H_
#define _CREATEINDEX_H_

// PCH include
#include "VCPluginHeaders.h"

// Framework includes
//#include "SnpRunnable.h"
//#include "SnipRunLog.h"

// Interface includes
#include "IWorkspace.h"
#include "IDocument.h"
#include "IIndexTopicList.h"
#include "IIndexTopicListList.h"
#include "IndexSectionHeader.h"
#include "IEditTopicEntryCmdData.h"
#include "ICreateTopicEntryCmdData.h"
#include "IndexTopicEntry.h"
#include "TagStruct.h"

// General includes
//#include "LayoutUtils.h" //Cs3 Depricated
#include "ILayoutUtils.h" //Cs4
#include "WideString.h"
#include "IIDXMLElement.h"
#include "vector"

#include "AttributeListData.h"

using namespace std;

// Customised ASSERT
// To be used in the "do ... while(false)" loops to break out on error condition
#define ASSERT_BREAK(cond, mesg)	\
	if(!(cond))						\
	{								\
		ASSERT(mesg);				\
		break;						\
	}


class MarkerInfo
{
public:
	UIDRef PageUIDRef;
	UIDRef BoxUIDRef;
	int32 StartIndex;
	int32 EndIndex;
	double ParentId;
	int32 topicNodeId;
	UID SectionUid;
	WideString FirstTopicString;
	TagStruct MarkerTagStruct;
};
typedef vector<MarkerInfo*> VectorMarkerInfo, *VectorMarkerInfoPtr;
// The snippet class
class SnpIndexTopics /*: public SnpRunnable*/
{
public:
	// Framework hooks
	SnpIndexTopics();
	virtual ~SnpIndexTopics();
	/*bool16 CanRun(ISnpRunnableContext *runnableContext);
	ErrorCode Run(ISnpRunnableContext *runnableContext);*/

	// Print the topic list
	ErrorCode PrintTopicList(IIndexTopicList *pTopicList);

	// Recursive function to traverse the topic entries level by level
	void TraverseTopicTree(IIndexTopicList *pTopicList, double sectionId, double parentNodeId);

	// Create a new topic entry
	ErrorCode CreateTopicEntry(IIndexTopicList *pTopicList);

	// Edit a topic List
	ErrorCode EditTopicList(IIndexTopicList *pTopicList);
	
	// Edit a topic Entry
	ErrorCode EditTopicEntry(IIndexTopicList *pTopicList, IndexTopicEntry&	oldTopicEntry);
	VectorMarkerInfoPtr getDocumentSelectedBoxIds(double SelectedAttrID, int32 Index);
	ErrorCode CreatePageEntry(MarkerInfo* markerInfo);
	//ErrorCode CreatePageEntry(UIDRef BoxUIDRef, int32 StartIndex, int32 EndIndex);
	bool16 doesExist(IIDXMLElement * ptr,const UIDList& selectUIDList);
	ErrorCode GenerateIndex(void);

	ErrorCode CreateIndexPageEntry(UIDRef boxUIDRef, int32 startIndex, int32 endIndex, double TopicNodeId, UID SectionUID, WideString FirstString);
	//ErrorCode CreatTopicEntry(int32 index);
	ErrorCode CreateIndexPageEntryUpToFourLevels(UIDRef boxUIDRef, int32 startIndex, int32 endIndex, int32 Level , WideString FirstString, WideString SecondString, WideString ThirdString, WideString FourthString);

/////////
	ErrorCode insertTopicStringInList(MarkerInfo* markerInfo);
	ErrorCode insertTopicStringInListNew(MarkerInfo* markerInfo,double AttributeId);
	bool16 doesExist(TagList & tagList,const UIDList& selectUIDList1);

	// added by prabhat 
	bool16 IsTextFrameOverset(const InterfacePtr<ITextFrameColumn> textFrameColumn);
	void forceRecompose(InterfacePtr<ITextFrameColumn> textFrame);
	// till here

/////////

	VectorMarkerInfoPtr getDocumentSelectedBoxIds(AttributeListData attributeListDataObj);
	void getDocumentSelectedBoxIds(UIDList& selectUIDList);
	VectorMarkerInfoPtr getDocumentSelectedBoxIdsFor2Level(AttributeListData attributeListDataObj, UIDList& selectUIDList);
	
private:

	// To track the level of the topic in the tree
	int currentLevel;
	int runNumber;
};


#endif
// SnpIndexTopics.cpp
// List the contents of the index topics


// PCH include
#include "VCPluginHeaders.h"


// Interface includes
#include "IWorkspace.h"
#include "IDocument.h"
#include "IIndexTopicList.h"
#include "IIndexTopicListList.h"
#include "IndexSectionHeader.h"
#include "IEditTopicEntryCmdData.h"
#include "ICreateTopicEntryCmdData.h"
#include "IndexTopicEntry.h"
#include "ICreateIndexPageEntryCmdData.h"
// General includes
#include "ILayoutUtils.h"  //Cs4
#include "WideString.h"
#include "ILayoutControlData.h"
#include "ISpreadList.h"
#include "ISpread.h"
#include "ILayoutUIUtils.h"
#include "vector"
#include "K2Vector.h" 
#include "IIndexPageEntryData.h"
#include "IInsertIndexMarkCmdData.h"
#include "ISelectionManager.h"
#include "IIndexingDataSuite.h"
#include "ISelectionUtils.h"
#include "IStyleNameTable.h"
#include "ITextSelectionSuite.h"
#include "IFrameUtils.h"


#include "ITextModel.h"
#include "CreateIndex.h"
#include "IConcreteSelection.h"
#include "ITextTarget.h"
#include "TextIterator.h"
#include "IActiveTopicListContext.h"
#include "ICreateTopicListCmdData.h"
#include "IIndexPageEntryCmdData.h"
#include "CAlert.h"
#include "ITextFocus.h"
#include "IRangeData.h"
#include "ITagReader.h"
#include "IHierarchy.h"
#include "IActionManager.h"
#include "IApplication.h"
#include "IXMLUtils.h"
// ID files:
#include "IndexingID.h"
#include "IndexPanelID.h"
#include "ISelectionManager.h"
#include "ITextMiscellanySuite.h"
#include "ITextFrameColumn.h"
#include "IAppFramework.h"
#include "IGraphicFrameData.h"
#include "IPageItemTypeUtils.h"
#include "IDeleteIndexPageEntryCmdData.h"


#include "IFrameListComposer.h"


using namespace std;
#define CA(X)		CAlert::InformationAlert(X)
#define CAI(x) {PMString str;\
			str.AppendNumber(x);\
			CAlert::InformationAlert(str);}\

extern double SelectedAttrID;



UID newsectionUID;
//int32 newtopicNodeId;

// Ctor & Dtor
SnpIndexTopics::SnpIndexTopics() /*: SnpRunnable("IndexTopicsList")*/
{
	currentLevel = 0;
	runNumber = 0;
}

SnpIndexTopics::~SnpIndexTopics()
{
}


void SnpIndexTopics::TraverseTopicTree(IIndexTopicList *pTopicList, double sectionId, double parentNodeId)
{	
	CA(__FUNCTION__);
	ASSERT(pTopicList != nil);

	WideString wsTopicName;
	//PMString *topicName; 
	currentLevel++;

	// Get the number of child nodes
	// Please note the when -1 is specified for parentNodeId while calling GetNumChildrenOfNode 
	// it returns the number of the nodes at the first level
	int32 numChild = pTopicList->GetNumChildrenOfNode(sectionId, parentNodeId);

	for(int j=0; j<numChild; j++)
	{
		// Please note the when -1 is specified for parentNodeId while calling GetNthChildNodeId 
		// it returns the nth node at the first level
		int32 nodeId = pTopicList->GetNthChildNodeId(sectionId, parentNodeId, j);

		pTopicList->GetDisplayString(sectionId, nodeId, wsTopicName);

		// WideString does not properly print if directly passed to SNIPLOG
		// So first convert to PMString and then use GrabCString() on it to convert to const char 

		// Descend to the next level
		this->TraverseTopicTree(pTopicList, sectionId, nodeId);
	}

	currentLevel--;
}


ErrorCode SnpIndexTopics::PrintTopicList(IIndexTopicList *pTopicList)
{	//CA(__FUNCTION__);
	ErrorCode error = kFailure;

	do
	{
		// The name of topic list
		//SNIPLOG("Name of Topic List: %s", pTopicList->GetName().GrabCString());

		// The index is a tree of Sections and its child nodes and each node could be considered 
		// as a tree of its child nodes. It could be described with the following diagram:
		//
		// Index
		//	|
		//	|--Section_A
		//	|	|
		//	|	|-Node1
		//	|		|
		//	|		|-Node2
		//	|
		//	|--Section_B
		//	|	|
		//	|	|-Node3
		//	|	|	|
		//	|	|	|-Node4
		//	|	|	|-Node5
		//	|	|		|
		//	|	|		|-Node6
		//	|	|			|
		//	|	|			|-Node7
		//	|	|-Node8
		//	|
		//	|--next section
		// ...
		// Now the point here is that every node in the section tree belongs to that particular section ID
		// So to traverse the topic list first we need to get the num of sections and loop for the number 
		// of sections. In every iteration, first get the sections ID, then get the number of nodes at the 
		// first level, get the child nodes for every such node and do it recursively. Pls see the function 
		// TraverseTopicTree

		// Get the number of topic sections
		int32 numTopicSecs =  pTopicList->GetNumTopicSections();
		//SNIPLOG("Number of Topic Sections: %d", numTopicSecs);

		// The number of topics
		//SNIPLOG("Number of Topics: %d", pTopicList->GetNumTopics());

		// Iterate over the Topics List and print out the topics
		for(int i=0; i<numTopicSecs; i++)
		{
			//SNIPLOG("\nDisplaying the topics for section: %s", pTopicList->GetNthTopicSectionHeader(i).fUIHeader.GrabCString());

			// Get the UID and SecID for the current section
			UID sectionUid  = pTopicList->GetNthTopicSectionUID(i);
			double sectionId = pTopicList->GetTopicSectionId(sectionUid);

			// Traverse the topic tree printing out the entries
			// Please note the when -1 is specified for parentNodeId while calling GetNumChildrenOfNode 
			// it returns the number of the nodes at the first level
			this->TraverseTopicTree(pTopicList, sectionId, -1);
		}

		error = kSuccess;
	} while(false);

	return error;
}


// Create an Index Topic Entry
ErrorCode SnpIndexTopics::CreateTopicEntry(IIndexTopicList *pTopicList)
{	//CA(__FUNCTION__);
	ErrorCode error = kFailure;

	do
	{
////		VectorMarkerInfo DocMarkerList = getDocumentSelectedBoxIds();
////		if(DocMarkerList.size()==0)
////			return error;
////		
////		CAI(DocMarkerList.size());
////		UIDRef BoxUIDRef = DocMarkerList[0].BoxUIDRef;
////		int32 textIndex =1;
////
////		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
////		if(!iSelectionManager)
////		{	CA("!iSelectionManager");
////			return 0;
////		}
////		
////		InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite(),UseDefaultIID());
////		if(!txtSelSuite)
////		{
////		CA("Text Selection Suite is null");
////		return kFalse;
////		}
////
////		InterfacePtr<IPMUnknown> unknown(BoxUIDRef, IID_IUNKNOWN);
////		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
////
////		if (textFrameUID == kInvalidUID)
////			return kFalse;
////
////		InterfacePtr<ITextFrame> textFrame(BoxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
////		if (textFrame == nil)
////			return kFalse;
////			
////		
////		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
////		if (textModel == nil)
////			return kFalse;
////
////CA("17");
////		const UIDRef TextModelUIDRef = ::GetUIDRef(textModel);
////	
////		RangeData rData(1,5);
////
////		txtSelSuite->SetTextSelection(TextModelUIDRef,rData,Selection::kDontScrollSelection ,nil);
////
////		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
////		InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID()); 
////
////		/*InterfacePtr<ITextFocus> iFocus(pTextTarget->QueryTextFocus()); 
////
////		if(!iFocus)
////			return kFalse;*/
////		RangeData rData1 =pTextTarget->GetRange();
////
////		TextIterator start(textModel,rData1.Start(nil));
////		TextIterator end(textModel, rData1.End());
////
////
////		PMString textFrameStory;
////
////
////		for (TextIterator iter = start; iter <= end; iter++)
////		{
////			const UTF32TextChar characterCode = (*iter).GetValue();
////			textFrameStory.AppendW(characterCode);
////		}
////		
//	/*	PageEntryInfo newpageEntry(BoxUIDRef, textIndex);
//
//		PageEntryInfoArray CurrPageArray;
//		CurrPageArray.push_back(newpageEntry);*/
////
////
////		InterfacePtr<ICommand> createMarkerEntryCmd(CmdUtils::CreateCommand(kInsertIndexMarkCmdBoss));
////		//ASSERT_BREAK(createTopicEntryCmd != nil, "SnpIndexTopics::CreateTopicEntry() - createTopicEntryCmd is NIL");
////CA("18");
////		// Create the command data to create a new Topic Entry
////		InterfacePtr<IInsertIndexMarkCmdData> createPageMarkerCmdData(createMarkerEntryCmd, UseDefaultIID());
////		//ASSERT_BREAK(createTopicCmdData != nil, "SnpIndexTopics::CreateTopicEntry() - createTopicCmdData is NIL");
////CA("19");
////
////		///createPageMarkerCmdData->SetPageEntryUIDRef(/*DocMarkerList[0].PageUIDRef*/BoxUIDRef);
////		//createPageMarkerCmdData->SetPageEntriesInfo(CurrPageArray);
////		//createPageMarkerCmdData->SetTargetItem(DocMarkerList[0].BoxUIDRef);
////CA("19.1");
////		error = CmdUtils::ProcessCommand(createMarkerEntryCmd);
//
//
//		// Create a command to create a new Topic Entry
//		InterfacePtr<ICommand> createTopicEntryCmd(CmdUtils::CreateCommand(kCreateTopicEntryCmdBoss));
//		ASSERT_BREAK(createTopicEntryCmd != nil, "SnpIndexTopics::CreateTopicEntry() - createTopicEntryCmd is NIL");
//CA("12");
//		// Create the command data to create a new Topic Entry
//		InterfacePtr<ICreateTopicEntryCmdData> createTopicCmdData(createTopicEntryCmd, UseDefaultIID());
//		ASSERT_BREAK(createTopicCmdData != nil, "SnpIndexTopics::CreateTopicEntry() - createTopicCmdData is NIL");
//CA("13");
//		// Setup the command data
//		UIDRef topicListRef = ::GetUIDRef(pTopicList);
//		IndexTopicEntry	entry;
//		entry.GetNumNodes() = 1;
//		entry.GetTopicEntryNode(0).GetLanguage() = kLanguageUserDefault;
//CA("14");
//		// just do it at the first level
//		entry.GetTopicEntryNode(0).GetLevel() = 1;
//		entry.GetTopicEntryNode(0).GetDisplayString().SetCString(textFrameStory.GrabCString());
//CA("15");
//
//		TopicEntryInfoPair newTopicEntry(entry, textIndex);
//
//		TopicEntryInfoArray CurrTopicArray;
//		CurrTopicArray.push_back(newTopicEntry);
//
//		/*entry.GetTopicEntryNode(1).GetLevel() = 1;
//		entry.GetTopicEntryNode(1).GetDisplayString().SetCString("11");*/
//		// fill the cmd data
//		createTopicCmdData->SetTopicListUIDRef(topicListRef);
//		createTopicCmdData->SetTopicEntry(entry);
//CA("16");
//		// *** NOTE: To keep things simple, we are just creating an Index Topic.
//		// We are not creating the Page Reference for this topic.
//		// If desired, the page reference can be created using the command kCreateIndexPageEntryCmdBoss.
//		// Execute the command
//		error = CmdUtils::ProcessCommand(createTopicEntryCmd);
//		
//		
//		InterfacePtr<ICommand> createPageReferanceEntryCmd(CmdUtils::CreateCommand(kCreateIndexPageEntryCmdBoss));
//		ASSERT_BREAK(createTopicEntryCmd != nil, "SnpIndexTopics::CreateTopicEntry() - createTopicEntryCmd is NIL");
//CA("19.2");
//		// Create the command data to create a new Topic Entry
//		InterfacePtr<ICreateIndexPageEntryCmdData> createPageRefCmdData(createPageReferanceEntryCmd, UseDefaultIID());
//		ASSERT_BREAK(createTopicCmdData != nil, "SnpIndexTopics::CreateTopicEntry() - createTopicCmdData is NIL");
//CA("19.3");
//		
//		createPageRefCmdData->SetTopicListUIDRef(topicListRef);
//		createPageRefCmdData->SetTopicEntry(entry);
//		createPageRefCmdData->SetSortPRefs(kTrue);
//
//CA("20");		
//		error = CmdUtils::ProcessCommand(createPageReferanceEntryCmd);
//
//
////		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
////		if(!iSelectionManager)
////		{	CA("!iSelectionManager");
////			return 0;
////		}
////
////	
////		InterfacePtr<IIndexingDataSuite> IndexDataSuite(static_cast<IIndexingDataSuite* >
////		( Utils<ISelectionUtils>()->QuerySuite(IIndexingDataSuite::kDefaultIID,iSelectionManager))); 
////		if(!IndexDataSuite)
////		{ CA("!IndexDataSuite");
////		return 0; 
////		}
////CA("21");
////		UID INdexMarkUID;
////		IndexDataSuite->GetIndexMarkOfSelection(INdexMarkUID);
////CA("22");
////
////		UIDRef SelectedStoryUIDRef(nil, kInvalidUID);
////		IndexDataSuite->GetSelectedIndexStory(SelectedStoryUIDRef);
////CA("23");
////
////
////		InterfacePtr<IWorkspace> pWorkspace(Utils<ILayoutUIUtils>()->QueryActiveWorkspace());
////		if(pWorkspace == nil)
////		{	
////			CA("pWorkspace == nil");
////			return 0;
////		}
////CA("24");
////		// Paragraph style nametable
////			InterfacePtr<IStyleNameTable> iParaStyleNameTable(pWorkspace,IID_IPARASTYLENAMETABLE);
////			ASSERT(iParaStyleNameTable);
////			if (iParaStyleNameTable == nil) {
////				return 0;
////			}
////CA("25");
////			UID rootStyleUID = iParaStyleNameTable->GetRootStyleUID();
////			ASSERT(rootStyleUID != kInvalidUID);
////			if(rootStyleUID == kInvalidUID) {
////				return 0;
////			}
////
////CA("26");
////
////
////		error = IndexDataSuite->CreatePageReferences1(CurrTopicArray, 
////			IIndexPageEntryData::kCurrentPage ,
////			rootStyleUID,
////			1,
////			rootStyleUID,
////			SelectedStoryUIDRef
////			);
////		if(!error)
////			CA("!error");
////
////CA("24");
		
	} while(false);

	return error;
}

/* CreatePageEntry
*/
//ErrorCode SnpIndexTopics::CreatePageEntry(UIDRef BoxUIDRef, int32 StartIndex, int32 EndIndex)
ErrorCode SnpIndexTopics::CreatePageEntry(MarkerInfo *markInfo)
{	//CA(__FUNCTION__);
	ErrorCode err = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		return err;
	}
	
	do
	{
		// Check whether there exists a topic list for the document
		//  If not, create one
		//InterfacePtr<IDocument> piDocument(runnableContext->GetActiveContext()->GetContextDocument(), UseDefaultIID());
// Changes By Rahul.
		IDocument* piDocument =Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		if(piDocument == nil)
		{ 
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreatePageEntry::piDocument == nil ");
			return err;
		}
////////////////////
	
		InterfacePtr<IActiveTopicListContext> piActiveTopicListContext(GetExecutionContextSession(), UseDefaultIID());//Cs4
		UIDRef documentTopicListRef = piActiveTopicListContext->GetDefaultTopicList(::GetUIDRef(piDocument));
		if (documentTopicListRef.GetUID() == kInvalidUID)
		{
			InterfacePtr<ICommand> piCreateTopicListCmd(CmdUtils::CreateCommand(kCreateTopicListCmdBoss));
			InterfacePtr<ICreateTopicListCmdData> piCreateTopicListCmdData(piCreateTopicListCmd, UseDefaultIID());
			piCreateTopicListCmdData->SetTargetItem(piDocument->GetDocWorkSpace());
			piCreateTopicListCmdData->SetDoNotifyFlag(kTrue); // Always kTrue because the observer on topic list has to be attached

			if (CmdUtils::ProcessCommand(piCreateTopicListCmd) != kSuccess)
				return err;
			documentTopicListRef = piCreateTopicListCmd->GetItemListReference().GetRef(0);
		}

	
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreatePageEntry::!iSelectionManager");
			return err;
		}

		InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite()/*,UseDefaultIID()*/);//Amit
		if(!txtSelSuite)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreatePageEntry::Text Selection Suite is null");
			return err;
		}

		UIDRef BoxUIDRef = markInfo->BoxUIDRef;
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreatePageEntry::textFrameUID is kInvalidUID");		
			return err;
		}


		InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ASSERT(graphicFrameHierarchy);
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ASSERT(multiColumnItemHierarchy);
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ASSERT(multiColumnItemTextFrame);
			break;
		}

		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			break;
		}	
		
		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreatePageEntry::textModel is nil");		
			return err;
		}

		const UIDRef TextModelUIDRef = ::GetUIDRef(textModel);

		TagStruct tagStructInfo;
		tagStructInfo = markInfo->MarkerTagStruct;
		TextIndex sIndex=0, eIndex=0;
		Utils<IXMLUtils>()->GetElementIndices(tagStructInfo.tagPtr, &sIndex, &eIndex);
		RangeData rData(sIndex,eIndex);

		txtSelSuite->SetTextSelection(TextModelUIDRef,rData,Selection::kDontScrollSelection ,nil);
		InterfacePtr<IConcreteSelection>pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if (pTextSel == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreatePageEntry::pTextSel is nil");		
			return err;
		}

		InterfacePtr<ITextTarget>piTextTarget(pTextSel, UseDefaultIID()); 
		if (piTextTarget == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreatePageEntry::piTextTarget is nil");		
			return err;
		}

		// Get the string from the text story to add to the topic list
		//InterfacePtr<ITextTarget> piTextTarget(runnableContext, UseDefaultIID());
		InterfacePtr<ITextFocus> piTextFocus(piTextTarget ? piTextTarget->QueryTextFocus() : nil);
		if (piTextFocus == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreatePageEntry::piTextFocus is nil");		
			return err;
		}

		InterfacePtr<ITextModel> piTextModel(piTextFocus->QueryModel());
		if (piTextModel == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreatePageEntry::piTextModel is nil");		
			return err;
		}

		UIDRef textModelUIDRef = ::GetUIDRef(piTextModel);

		RangeData range = piTextFocus->GetCurrentRange();
		TextIndex start = range.Start(nil);
		TextIndex end = range.End();
		TextIterator firstChar(piTextModel, start);
		TextIterator lastChar(piTextModel, end);
		WideString topicString;
		for (TextIterator iter = firstChar; iter != lastChar; ++iter)
		{
			const UTF32TextChar characterCode = *iter;
			if (characterCode != kTextChar_ZeroSpaceNoBreak)
				topicString.Append(characterCode);
		}

		// Create a Topic Entry with the obtained string
	

		IndexTopicEntry	entry;
		entry.GetNumNodes() = 1;
		entry.GetTopicEntryNode(0).GetLanguage() = kLanguageUserDefault;
		entry.GetTopicEntryNode(0).GetLevel() = 1;
		entry.GetTopicEntryNode(0).GetDisplayString() = topicString;
		UID sectionUID;
		int32 topicNodeId;

		// Check whether the entry already exists; if not create one
		InterfacePtr<IIndexTopicList> piIndexTopicList(documentTopicListRef, UseDefaultIID());
		if (! piIndexTopicList->FindTopicEntry(entry, &sectionUID, &topicNodeId))
		{
			// Create a Topic Entry with the obtained string
			InterfacePtr<ICommand> piCreateTopicEntryCmd(CmdUtils::CreateCommand(kCreateTopicEntryCmdBoss));
			InterfacePtr<ICreateTopicEntryCmdData> piCreateTopicCmdData(piCreateTopicEntryCmd, UseDefaultIID());

			piCreateTopicCmdData->SetTopicListUIDRef(documentTopicListRef);
			piCreateTopicCmdData->SetTopicEntry(entry);
			if ((err = CmdUtils::ProcessCommand(piCreateTopicEntryCmd)) != kSuccess)
				break;

			sectionUID = piCreateTopicCmdData->GetTopicSectionUID();
			topicNodeId = piCreateTopicCmdData->GetTopicNodeId();
		}


	

		InterfacePtr<ICommand> piCreatePageEntryCmd(CmdUtils::CreateCommand(kCreateIndexPageEntryCmdBoss));
		InterfacePtr<ICreateIndexPageEntryCmdData> piCreatePageEntryCmdData(piCreatePageEntryCmd, UseDefaultIID());
		InterfacePtr<IIndexPageEntryCmdData> piIndexPageEntryCmdData(piCreatePageEntryCmd, UseDefaultIID());
		piCreatePageEntryCmdData->SetTopicListUIDRef(documentTopicListRef);
		piCreatePageEntryCmdData->SetTopicSectionUID(sectionUID);
		piCreatePageEntryCmdData->SetTopicNodeId(topicNodeId);
		piIndexPageEntryCmdData->SetPageRangeType(IIndexPageEntryData::kCurrentPage, kInvalidUID, 0);
		piIndexPageEntryCmdData->SetStyleUID(kInvalidUID);
		if ((err = CmdUtils::ProcessCommand(piCreatePageEntryCmd)) != kSuccess)
			break;



		UIDRef pageEntryUIDRef(piCreatePageEntryCmd->GetItemList()->GetRef(0));
		if (pageEntryUIDRef.GetUID() == kInvalidUID)
			break;

		// Insert a mark in the text and in the owned item strand
		InterfacePtr<ICommand> piInsertIndexMarkCmd(CmdUtils::CreateCommand(kInsertIndexMarkCmdBoss));
		InterfacePtr<IRangeData> piRangeData(piInsertIndexMarkCmd, IID_IRANGEDATA);
		InterfacePtr<IInsertIndexMarkCmdData> piInsertIndexMarkCmdData(piInsertIndexMarkCmd, UseDefaultIID());
		piInsertIndexMarkCmdData->SetTargetItem(textModelUIDRef);
		piInsertIndexMarkCmdData->SetPageEntryUIDRef(pageEntryUIDRef);

		piRangeData->Set(start, end);
		if ((err = CmdUtils::ProcessCommand(piInsertIndexMarkCmd)) != kSuccess)
			break;

		// Update the index entry page numbers
		InterfacePtr<ICommand> piUpdatePageEntryCmd(CmdUtils::CreateCommand(kUpdatePageEntryCmdBoss));
		piUpdatePageEntryCmd->SetItemList(pageEntryUIDRef);
		if ((err = CmdUtils::ProcessCommand(piUpdatePageEntryCmd)) != kSuccess)
			break;
	}while(0);	
	return err;
}



// Edit an Index Topic Entry
ErrorCode SnpIndexTopics::EditTopicList(IIndexTopicList *pTopicList)
{	//CA(__FUNCTION__);
	ErrorCode error = kFailure;
	IndexTopicEntry	oldTopicEntry;
	// fill up the Topic entry data to immitate the last topic entry created
	// this is just to show how to use editTopicEntryCmd
	oldTopicEntry.GetNumNodes() = 1;
	oldTopicEntry.GetTopicEntryNode(0).GetLanguage() = kLanguageUserDefault;
	oldTopicEntry.GetTopicEntryNode(0).GetLevel() = 1;
	oldTopicEntry.GetTopicEntryNode(0).GetDisplayString().SetCString("Vanilla");
	return EditTopicEntry(pTopicList, oldTopicEntry);
}

// funtion to edit an existing topic entry
// it adds the " with Choco sauce" string with a sequence number in the list entry string
// and changes the sort string ("Choco") to attach it with section "C"
// Please note that the Sort string, if specified,is used to sort the topic entry within 
// the topic level instead of the topic string

ErrorCode SnpIndexTopics::EditTopicEntry(IIndexTopicList *pTopicList, IndexTopicEntry&	oldTopicEntry){
	IndexTopicEntry	newTopicEntry = oldTopicEntry;
	// First form the new topic string
	PMString DisplayString(oldTopicEntry.GetTopicEntryNode(0).GetDisplayString());
    std::string tempStr = DisplayString.GetPlatformString();
	char *str = new char[100];
	::sprintf(	str, 
				"%s with Choco sauce #%d", 
				tempStr.c_str(),/*oldTopicEntry.GetTopicEntryNode(0).GetDisplayString().GetAsSystemString()->GrabCString(),*/
				runNumber/2);
	newTopicEntry.GetTopicEntryNode(0).GetDisplayString().SetCString(str);

	//change the sort string so that it appears in the "C" section
	newTopicEntry.GetTopicEntryNode(0).GetSortStringReference().SetCString("Choco");

	delete str;

	do
	{
		// As we are going to change the ID database (Doc workspace), we should use command to do so
		// so that the ID should take care of the atomicity of the operation and the user gets the the 
		// facility to undo/redo 

		// Create a command to edit a Topic Entry
		InterfacePtr<ICommand> editTopicEntryCmd(CmdUtils::CreateCommand(kEditTopicEntryCmdBoss));
		ASSERT_BREAK(editTopicEntryCmd != nil, "SnpIndexTopics::EditTopicEntry() - editTopicEntryCmd command is NIL");

		// create the command data
		InterfacePtr<IEditTopicEntryCmdData> cmdData(editTopicEntryCmd, UseDefaultIID());
		ASSERT_BREAK(cmdData != nil, "SnpIndexTopics::EditTopicEntry() - IEditTopicEntryCmdData interface is NIL");
		
		// Setup the command data
		UIDRef topicListRef = ::GetUIDRef(pTopicList);
		cmdData->SetTopicListUIDRef(topicListRef);
		cmdData->SetFromTopicEntry(oldTopicEntry);
		cmdData->SetToTopicEntry(newTopicEntry);
		
		// Execute the command
		return CmdUtils::ProcessCommand(editTopicEntryCmd);
	}while(false);

	return kFailure;
}


VectorMarkerInfoPtr SnpIndexTopics::getDocumentSelectedBoxIds(double SelectedAttrID, int32 Index)
{
	//CA(__FUNCTION__);
	VectorMarkerInfoPtr vectorMarkerInfoPtr = new VectorMarkerInfo;
	//TagReader tReader;
	IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();  
	if(fntDoc==nil)
		return vectorMarkerInfoPtr;

	InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layout == nil)
		return vectorMarkerInfoPtr;

	IDataBase* database = ::GetDataBase(fntDoc);
	if(database==nil)
		return vectorMarkerInfoPtr;

	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
	if (iSpreadList==nil)
		return vectorMarkerInfoPtr;

	bool16 collisionFlag=kFalse;
	UIDList allPageItems(database);

	InterfacePtr<ITagReader> itagReader
	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
		return vectorMarkerInfoPtr;

 do
   {
	for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
	{
		
		UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
		if(!spread)
			break;

		int numPages=spread->GetNumPages();

		for(int i=0; i<numPages; i++)
		{			
			UIDList tempList(database);
			UID PageUID = spread->GetNthPageUID(i);
			UIDRef PageUIDRef(database, PageUID);
			spread->GetItemsOnPage(i, &tempList, kFalse);
			allPageItems.Append(tempList);

		}
	}
	UIDList selectUIDList= allPageItems;
	
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		bool16 isGroupFrame = kFalse ;
		isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(i));

		if(isGroupFrame == kTrue) 
		{
			
			InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
			if(!iHier)
				continue;

			UID kidUID;
			int32 numKids=iHier->GetChildCount();
			for(int j=0 ;j < numKids ;j++)
			{
				
				IIDXMLElement* ptr;
				kidUID=iHier->GetChildUID(j);
				UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
				TagList tagList;
				tagList=itagReader->getTagsFromBox(boxRef, &ptr);
				if(!doesExist(tagList,selectUIDList))//if(!doesExist(ptr))
					selectUIDList.Append(kidUID);
				
				if(tagList.size() > 0)
				{
					for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
					{
						if(tagList[tagIndex].tagPtr)
							tagList[tagIndex].tagPtr->Release();						
					}
				}
			}
			
		}
	}

	for(int32 j=0; j<selectUIDList.Length(); j++)
	{
		TagList CurrBoxTagList = itagReader->getTextFrameTagsForIndex(selectUIDList.GetRef(j));
		if(CurrBoxTagList.size()==0)
		{
			TagList tList;
			tList=itagReader->getTagsFromBox(selectUIDList.GetRef(j));
			if(tList.size() > 0)
			{	
				TagStruct tstruct = tList[0];
				if(tstruct.isTablePresent || tstruct.elementId  == -101 )
				{
					PMString tagString = tstruct.tagPtr->GetTagString(); 
					int32 childCount = tstruct.tagPtr->GetChildCount(); 
					
					for(int32 childIndex = 0 ; childIndex < childCount ; childIndex++)
					{
						XMLReference elementXMLref = tstruct.tagPtr->GetNthChild(childIndex);
						InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
						if(childElement==nil)
							continue;
						PMString ChildtagName=childElement->GetTagString();
						int elementCount=childElement->GetChildCount();
						for(int elementIndex=0; elementIndex<elementCount; elementIndex++)
						{
							XMLReference elementXMLref1 = childElement->GetNthChild(elementIndex);
							InterfacePtr<IIDXMLElement>childTagXMLElementPtr(elementXMLref1.Instantiate());
							if(childTagXMLElementPtr == nil)
								continue;
							
							PMString ChildtagName1 = childTagXMLElementPtr->GetTagString();
							PMString displayTagData;
							PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID")); //Cs4
							double ID = strID.GetAsNumber();									
							
							PMString strTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("typeId")); //Cs4
							double typeID = strTypeID.GetAsNumber();													
							
							PMString strIndex = childTagXMLElementPtr->GetAttributeValue(WideString("index")); //Cs4
							int32 index = strIndex.GetAsNumber();				

							PMString strImgFlag = childTagXMLElementPtr->GetAttributeValue(WideString("imgFlag")); //Cs4
							int32 imgFlag = strImgFlag.GetAsNumber();								

							PMString strParentID = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
							double parentID = strParentID.GetAsNumber();				
							
							PMString strParentTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));//Cs4
							double parentTypeID = strParentTypeID.GetAsNumber();				
							
							PMString strSectionID = childTagXMLElementPtr->GetAttributeValue(WideString("sectionID"));//Cs4
							double sectionID = strSectionID.GetAsNumber();
							
							PMString strTableFlag = childTagXMLElementPtr->GetAttributeValue(WideString("tableFlag"));//CS4
							int32 tableFlag = strTableFlag.GetAsNumber();

							PMString strLanguageID = childTagXMLElementPtr->GetAttributeValue(WideString("LanguageID"));//Cs4
							double languageID = strLanguageID.GetAsNumber();

							PMString strIsAutoResize = childTagXMLElementPtr->GetAttributeValue(WideString("isAutoResize"));//Cs4
							int32 isAutoResize = strIsAutoResize.GetAsNumber();

							PMString strchildTag = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));//Cs4
							int32 childTag = strchildTag.GetAsNumber();

							PMString strchildId = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
							double childId = strchildId.GetAsNumber();

							PMString strcatLevel = childTagXMLElementPtr->GetAttributeValue(WideString("catLevel"));//Cs4
							int32 catLevel = strcatLevel.GetAsNumber();

							TagStruct tagInfo;
							tagInfo.elementId = ID;
							childTagXMLElementPtr->AddRef();//Added on 17-09-08
							tagInfo.tagPtr = childTagXMLElementPtr;
							tagInfo.typeId = typeID;
							tagInfo.languageID =  languageID;
							tagInfo.parentId = parentID;
							tagInfo.parentTypeID = parentTypeID;
							tagInfo.sectionID = sectionID;
							tagInfo.isAutoResize = isAutoResize;
							tagInfo.whichTab = index;
							tagInfo.imgFlag = imgFlag;
                            tagInfo.isTablePresent = kFalse;
							tagInfo.childTag = childTag;
							tagInfo.childId = childId;
							tagInfo.catLevel = catLevel;
															
							TextIndex sIndex=0, eIndex=0;
							Utils<IXMLUtils>()->GetElementIndices(childTagXMLElementPtr, &sIndex, &eIndex);
							
							tagInfo.startIndex=sIndex;
							tagInfo.endIndex=eIndex;						

							if((ID == SelectedAttrID) && (index == 3 || index == 4 || index == 5)  && (parentID != -1) )
							{

								MarkerInfo* NewMark = new MarkerInfo;
								NewMark->BoxUIDRef = selectUIDList.GetRef(j);
								NewMark->StartIndex = tagInfo.startIndex;
								NewMark->EndIndex = tagInfo.endIndex;
								NewMark->ParentId = tagInfo.parentId;
								NewMark->MarkerTagStruct = tagInfo;
								insertTopicStringInListNew(NewMark,SelectedAttrID);
								vectorMarkerInfoPtr->push_back(NewMark);
							}
						}
					}
				}
			}
			if(tList.size() > 0)
			{
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
			}
			continue;
		}


		for(int32 p=0; p<CurrBoxTagList.size(); p++ )
		{			
			if(/*CurrBoxTagList[p].elementId == SelectedAttrID && */(CurrBoxTagList[p].whichTab == 3 || CurrBoxTagList[p].whichTab == 4 || CurrBoxTagList[p].whichTab == 5)  && CurrBoxTagList[p].isTablePresent == kFalse && CurrBoxTagList[p].imgFlag ==0 && CurrBoxTagList[p].parentId != -1) // commented by prabhat
			{	
				//CA("123");
				MarkerInfo* NewMark = new MarkerInfo;
				NewMark->BoxUIDRef = selectUIDList.GetRef(j);
				NewMark->StartIndex = CurrBoxTagList[p].startIndex;
				NewMark->EndIndex = CurrBoxTagList[p].endIndex;
				NewMark->ParentId = CurrBoxTagList[p].parentId;
				NewMark->MarkerTagStruct = CurrBoxTagList[p];
				CurrBoxTagList[p].tagPtr->AddRef();
				NewMark->MarkerTagStruct.tagPtr  = CurrBoxTagList[p].tagPtr;
				insertTopicStringInListNew(NewMark,SelectedAttrID);
				vectorMarkerInfoPtr->push_back(NewMark);
			}
			if(CurrBoxTagList[p].typeId == -5 || CurrBoxTagList[p].elementId == -102)
			{	
				
				int32 childTagCount = CurrBoxTagList[p].tagPtr->GetChildCount();
			    for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
				{
					//CA("934 condiion true");
					XMLReference childTagRef = CurrBoxTagList[p].tagPtr->GetNthChild(childTagIndex);
					InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
					if(childTagXMLElementPtr == nil)
						continue;						
					TagStruct tagInfo;
	
					if(CurrBoxTagList[p].typeId == -5)
					{
						PMString displayTagData;
						PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));//CS4
						double ID = strID.GetAsNumber();									
						
						PMString strTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("typeId"));//Cs4
						double typeID = strTypeID.GetAsNumber();													
						
						PMString strIndex = childTagXMLElementPtr->GetAttributeValue(WideString("index"));//CS4
						int32 index = strIndex.GetAsNumber();				

						PMString strImgFlag = childTagXMLElementPtr->GetAttributeValue(WideString("imgFlag"));//Cs4
						int32 imgFlag = strImgFlag.GetAsNumber();								

						PMString strParentID = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
						double parentID = strParentID.GetAsNumber();				
						
						PMString strParentTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));//Cs4
						double parentTypeID = strParentTypeID.GetAsNumber();				
						
						PMString strSectionID = childTagXMLElementPtr->GetAttributeValue(WideString("sectionID"));//Cs4
						double sectionID = strSectionID.GetAsNumber();
						
						PMString strTableFlag = childTagXMLElementPtr->GetAttributeValue(WideString("tableFlag"));//Cs4
						int32 tableFlag = strTableFlag.GetAsNumber();

						PMString strLanguageID = childTagXMLElementPtr->GetAttributeValue(WideString("LanguageID"));//Cs4
						double languageID = strLanguageID.GetAsNumber();

						PMString strIsAutoResize = childTagXMLElementPtr->GetAttributeValue(WideString("isAutoResize"));//Cs4
						int32 isAutoResize = strIsAutoResize.GetAsNumber();

						PMString strchildTag = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));//Cs4
						int32 childTag = strchildTag.GetAsNumber();

						PMString strchildId = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
						double childId = strchildId.GetAsNumber();

						PMString strcatLevel = childTagXMLElementPtr->GetAttributeValue(WideString("catLevel"));//Cs4
						int32 catLevel = strcatLevel.GetAsNumber();

						
						tagInfo.elementId = ID;
						childTagXMLElementPtr->AddRef();//Added on 17-09-08
						tagInfo.tagPtr = childTagXMLElementPtr;
						tagInfo.typeId = typeID;
						tagInfo.languageID = languageID;
						tagInfo.parentId = parentID;
						tagInfo.parentTypeID = parentTypeID;
						tagInfo.sectionID = sectionID;
						tagInfo.isAutoResize = isAutoResize;
						tagInfo.whichTab = index;
						tagInfo.imgFlag = imgFlag;
						tagInfo.isTablePresent = kFalse;
						tagInfo.childTag = childTag;
						tagInfo.childId = childId;
						tagInfo.catLevel = catLevel;
						
						TextIndex sIndex=0, eIndex=0;
						Utils<IXMLUtils>()->GetElementIndices(childTagXMLElementPtr, &sIndex, &eIndex);
					
						tagInfo.startIndex=sIndex;
						tagInfo.endIndex=eIndex;

						if(ID == SelectedAttrID && (index == 3 || index == 4 || index == 5)  && parentID != -1)
						{	

							MarkerInfo* NewMark = new MarkerInfo;
							NewMark->BoxUIDRef = selectUIDList.GetRef(j);
							NewMark->StartIndex = tagInfo.startIndex;
							NewMark->EndIndex = tagInfo.endIndex;
							NewMark->ParentId = parentID;
							NewMark->MarkerTagStruct = tagInfo;
							insertTopicStringInList(NewMark);
							vectorMarkerInfoPtr->push_back(NewMark);
						}
					}
					else if(CurrBoxTagList[p].elementId == -102)
					{
						int32 childTagCount1 = childTagXMLElementPtr->GetChildCount();
						for(int32 childTagIndex = 0;childTagIndex < childTagCount1;childTagIndex++)
						{
							//CA("inside for loop");
							XMLReference childTagRef = childTagXMLElementPtr->GetNthChild(childTagIndex);
							InterfacePtr<IIDXMLElement>childTagXMLElementPtr1(childTagRef.Instantiate());
							if(childTagXMLElementPtr1 == nil)
								continue;						

							PMString strID = childTagXMLElementPtr1->GetAttributeValue(WideString("ID"));//CS4
							double ID = strID.GetAsNumber();									
							
							PMString strTypeID = childTagXMLElementPtr1->GetAttributeValue(WideString("typeId"));//Cs4
							double typeID = strTypeID.GetAsNumber();													
							
							PMString strIndex = childTagXMLElementPtr1->GetAttributeValue(WideString("index"));//CS4
							int32 index = strIndex.GetAsNumber();				

							PMString strImgFlag = childTagXMLElementPtr1->GetAttributeValue(WideString("imgFlag"));//Cs4
							int32 imgFlag = strImgFlag.GetAsNumber();								

							PMString strParentID = childTagXMLElementPtr1->GetAttributeValue(WideString("parentID"));//Cs4
							double parentID = strParentID.GetAsNumber();				
							
							PMString strParentTypeID = childTagXMLElementPtr1->GetAttributeValue(WideString("parentTypeID"));//Cs4
							double parentTypeID = strParentTypeID.GetAsNumber();				
							
							PMString strSectionID = childTagXMLElementPtr1->GetAttributeValue(WideString("sectionID"));//Cs4
							double sectionID = strSectionID.GetAsNumber();
							
							PMString strTableFlag = childTagXMLElementPtr1->GetAttributeValue(WideString("tableFlag"));//Cs4
							int32 tableFlag = strTableFlag.GetAsNumber();

							PMString strLanguageID = childTagXMLElementPtr1->GetAttributeValue(WideString("LanguageID"));//Cs4
							double languageID = strLanguageID.GetAsNumber();

							PMString strIsAutoResize = childTagXMLElementPtr1->GetAttributeValue(WideString("isAutoResize"));//Cs4
							int32 isAutoResize = strIsAutoResize.GetAsNumber();

							PMString strchildTag = childTagXMLElementPtr1->GetAttributeValue(WideString("childTag"));//Cs4
							int32 childTag = strchildTag.GetAsNumber();

							PMString strchildId = childTagXMLElementPtr1->GetAttributeValue(WideString("childId"));//Cs4
							double childId = strchildId.GetAsNumber();

							PMString strcatLevel = childTagXMLElementPtr1->GetAttributeValue(WideString("catLevel"));//Cs4
							int32 catLevel = strcatLevel.GetAsNumber();

							
							tagInfo.elementId = ID;
							childTagXMLElementPtr1->AddRef();//Added on 17-09-08
							tagInfo.tagPtr = childTagXMLElementPtr1;
							tagInfo.typeId = typeID;
							tagInfo.languageID = languageID;
							tagInfo.parentId = parentID;
							tagInfo.parentTypeID = parentTypeID;
							tagInfo.sectionID = sectionID;
							tagInfo.isAutoResize = isAutoResize;
							tagInfo.whichTab = index;
							tagInfo.imgFlag = imgFlag;
							tagInfo.isTablePresent = kFalse;
							tagInfo.childTag = childTag;
							tagInfo.childId = childId;
							tagInfo.catLevel = catLevel;
							
							TextIndex sIndex=0, eIndex=0;
							Utils<IXMLUtils>()->GetElementIndices(childTagXMLElementPtr1, &sIndex, &eIndex);
						
							tagInfo.startIndex=sIndex;
							tagInfo.endIndex=eIndex;


							if(ID == SelectedAttrID && (index == 3 || index == 4 || index == 5)  && parentID != -1)
							{	

								MarkerInfo* NewMark = new MarkerInfo;
								NewMark->BoxUIDRef = selectUIDList.GetRef(j);
								NewMark->StartIndex = tagInfo.startIndex;
								NewMark->EndIndex = tagInfo.endIndex;
								NewMark->ParentId = parentID;
								NewMark->MarkerTagStruct = tagInfo;
								insertTopicStringInList(NewMark);
								vectorMarkerInfoPtr->push_back(NewMark);
							}
						}
					}
				}
			}
		}
		
		if(CurrBoxTagList.size() > 0)
		{
			for(int32 tagIndex = 0 ; tagIndex < CurrBoxTagList.size() ; tagIndex++)
			{
				CurrBoxTagList[tagIndex].tagPtr->Release();
			}
		}
	
	}
 }while(0);
 return vectorMarkerInfoPtr;
}


bool16 SnpIndexTopics::doesExist(IIDXMLElement * ptr,const UIDList& selectUIDList)
{	
	InterfacePtr<ITagReader> itagReader
	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
		return kFalse;
	IIDXMLElement *xmlPtr;
	
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		itagReader->getTagsFromBox(selectUIDList.GetRef(i), &xmlPtr);
		if(ptr==xmlPtr)
			return kTrue;
	}	
	return kFalse;
}

ErrorCode SnpIndexTopics::GenerateIndex(void)
{	//CA(__FUNCTION__);
	ErrorCode err = kFailure;
	do
	{
		// Invoke the action to show the "Generate Index" dialog
		//  The dialog reads the settings from the IIndexOptions interface on kDocWorkspaceBoss
		//  So if you wish to modify any of these settings, query for IIndexOptions on the
		//  document workspace and set them.
		InterfacePtr<IApplication> piApp(GetExecutionContextSession()->QueryApplication());//Cs4
		InterfacePtr<IActionManager> piActionMgr(piApp->QueryActionManager());	
		piActionMgr->PerformAction(GetExecutionContextSession()->GetActiveContext(), kGenerateIndexActionID);	//Cs4
		err = kSuccess;
	} while (false);
	
	return err;
}


// An instance of the snippet
SnpIndexTopics instanceSnpIndexTopics;


ErrorCode SnpIndexTopics::CreateIndexPageEntry(UIDRef boxUIDRef, int32 startIndex, int32 endIndex, double TopicNodeId, UID SectionUID, WideString FirstString)
{	
	ErrorCode err = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	return err;
	
	do
	{
		UIDRef BoxUIDRef = boxUIDRef;
		int32 StartIndex = startIndex;	
		int32 EndIndex = endIndex;

		PMString startNewIndex = "Start Index: ";
		startNewIndex.AppendNumber(StartIndex);
		startNewIndex.Append("EndIndex: ");
		startNewIndex.AppendNumber(EndIndex);
		//CA(startNewIndex);


		IDocument* piDocument =Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		if(piDocument == nil)
		{ 
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::piDocument == nil ");
			return err;
		}
	
		InterfacePtr<IActiveTopicListContext> piActiveTopicListContext(GetExecutionContextSession(), UseDefaultIID());//Cs4
		UIDRef documentTopicListRef = piActiveTopicListContext->GetDefaultTopicList(::GetUIDRef(piDocument));
		if (documentTopicListRef.GetUID() == kInvalidUID)
		{
				
			InterfacePtr<ICommand> piCreateTopicListCmd(CmdUtils::CreateCommand(kCreateTopicListCmdBoss));
			InterfacePtr<ICreateTopicListCmdData> piCreateTopicListCmdData(piCreateTopicListCmd, UseDefaultIID());
			piCreateTopicListCmdData->SetTargetItem(piDocument->GetDocWorkSpace());
			piCreateTopicListCmdData->SetDoNotifyFlag(kTrue); // Always kTrue because the observer on topic list has to be attached

			if (CmdUtils::ProcessCommand(piCreateTopicListCmd) != kSuccess){
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::CreateIndexPageEntry 1");
				return err;
			}
			documentTopicListRef = piCreateTopicListCmd->GetItemListReference().GetRef(0);
		}

		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	ptrIAppFramework->LogDebug("AP46_CataLogIndex::CreateIndex::CreateIndexPageEntry::!iSelectionManager");
			return err;
		}

		InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite()/*,UseDefaultIID()*/);//Amit
		if(!txtSelSuite)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::Text Selection Suite is null");
			return err;
		}

		
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::textFrameUID == kInvalidUID");
			return err;

		}
		

		InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ASSERT(graphicFrameHierarchy);
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ASSERT(multiColumnItemHierarchy);
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ASSERT(multiColumnItemTextFrame);
			break;
		}

		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			break;
		}
		
		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == nil){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::textModel == nil");
			return err;
		}
		
		
		const UIDRef TextModelUIDRef = ::GetUIDRef(textModel);		
		RangeData r1Data(startIndex+1,endIndex);		
		txtSelSuite->SetTextSelection(TextModelUIDRef,r1Data,Selection::kDontScrollSelection ,nil);
		
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		InterfacePtr<ITextTarget> piTextTarget(pTextSel, UseDefaultIID());

		InterfacePtr<ITextFocus> piTextFocus(piTextTarget ? piTextTarget->QueryTextFocus() : nil);
		InterfacePtr<ITextModel> piTextModel(piTextFocus->QueryModel());
		UIDRef textModelUIDRef = ::GetUIDRef(piTextModel);


		TextIndex newStart =  piTextFocus->GetStart(0);
		TextIndex newEnd = piTextFocus->GetEnd();
			
		TextIterator firstChar(piTextModel, newStart);
		TextIterator lastChar(piTextModel, newEnd);
		WideString topicString;
		for (TextIterator iter = firstChar; iter != lastChar; ++iter)
		{
			const UTF32TextChar characterCode = *iter;
			if (characterCode != kTextChar_ZeroSpaceNoBreak)
				topicString.Append(characterCode);
		}


		IndexTopicEntry	entry;
		entry.GetNumNodes() = 2;
		entry.GetTopicEntryNode(0).GetLanguage() = kLanguageUserDefault;
		entry.GetTopicEntryNode(0).GetLevel() = 1;
		entry.GetTopicEntryNode(0).GetDisplayString() = FirstString;

		entry.GetTopicEntryNode(1).GetLanguage() = kLanguageUserDefault;
		entry.GetTopicEntryNode(1).GetLevel() = 2;
		entry.GetTopicEntryNode(1).GetDisplayString() = topicString;
		UID sectionUID;
		int32 topicNodeId;

		// Check whether the entry already exists; if not create one
		InterfacePtr<IIndexTopicList> piIndexTopicList(documentTopicListRef, UseDefaultIID());
		if (! piIndexTopicList->FindTopicEntry(entry, &sectionUID, &topicNodeId))
		{
			// Create a Topic Entry with the obtained string
			InterfacePtr<ICommand> piCreateTopicEntryCmd(CmdUtils::CreateCommand(kCreateTopicEntryCmdBoss));
			InterfacePtr<ICreateTopicEntryCmdData> piCreateTopicCmdData(piCreateTopicEntryCmd, UseDefaultIID());

			piCreateTopicCmdData->SetTopicListUIDRef(documentTopicListRef);
			piCreateTopicCmdData->SetTopicEntry(entry);
			if ((err = CmdUtils::ProcessCommand(piCreateTopicEntryCmd)) != kSuccess)
				break;

			sectionUID = piCreateTopicCmdData->GetTopicSectionUID();
			topicNodeId = piCreateTopicCmdData->GetTopicNodeId();
		}
		
		InterfacePtr<ICommand> piCreatePageEntryCmd(CmdUtils::CreateCommand(kCreateIndexPageEntryCmdBoss));
		InterfacePtr<ICreateIndexPageEntryCmdData> piCreatePageEntryCmdData(piCreatePageEntryCmd, UseDefaultIID());
		InterfacePtr<IIndexPageEntryCmdData> piIndexPageEntryCmdData(piCreatePageEntryCmd, UseDefaultIID());
		piCreatePageEntryCmdData->SetTopicListUIDRef(documentTopicListRef);
		piCreatePageEntryCmdData->SetTopicSectionUID(sectionUID);
		piCreatePageEntryCmdData->SetTopicNodeId(topicNodeId);
		piIndexPageEntryCmdData->SetPageRangeType(IIndexPageEntryData::kCurrentPage, kInvalidUID, 0);
		piIndexPageEntryCmdData->SetStyleUID(kInvalidUID);
	
		if ((err = CmdUtils::ProcessCommand(piCreatePageEntryCmd)) != kSuccess){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::!kSuccess");
			break;
		}
	


		UIDRef pageEntryUIDRef(piCreatePageEntryCmd->GetItemList()->GetRef(0));
		if (pageEntryUIDRef.GetUID() == kInvalidUID){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::kInvalidUID ");
			break;
		}		
		// Insert a mark in the text and in the owned item strand
		InterfacePtr<ICommand> piInsertIndexMarkCmd(CmdUtils::CreateCommand(kInsertIndexMarkCmdBoss));
		InterfacePtr<IRangeData> piRangeData(piInsertIndexMarkCmd, IID_IRANGEDATA);
		InterfacePtr<IInsertIndexMarkCmdData> piInsertIndexMarkCmdData(piInsertIndexMarkCmd, UseDefaultIID());
		piInsertIndexMarkCmdData->SetTargetItem(textModelUIDRef);
		piInsertIndexMarkCmdData->SetPageEntryUIDRef(pageEntryUIDRef);
		piRangeData->Set(newStart, newEnd);
		if ((err = CmdUtils::ProcessCommand(piInsertIndexMarkCmd)) != kSuccess){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::err");
			break;
		}
		
		// Update the index entry page numbers
		InterfacePtr<ICommand> piUpdatePageEntryCmd(CmdUtils::CreateCommand(kUpdatePageEntryCmdBoss));
		piUpdatePageEntryCmd->SetItemList(pageEntryUIDRef);
		if ((err = CmdUtils::ProcessCommand(piUpdatePageEntryCmd)) != kSuccess){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::!kSuccess ");
			break;
		}	
	}while (false);

	return err;
}

ErrorCode SnpIndexTopics::CreateIndexPageEntryUpToFourLevels(UIDRef boxUIDRef, int32 startIndex, int32 endIndex, int32 Level , WideString FirstString, WideString SecondString, WideString ThirdString, WideString FourthString)
{	//CA(__FUNCTION__);

	ErrorCode err = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	return err;
	
	do
	{
		UIDRef BoxUIDRef = boxUIDRef;
		int32 StartIndex = startIndex;	
		int32 EndIndex = endIndex;

		PMString startNewIndex = "Start Index: ";
		startNewIndex.AppendNumber(StartIndex);
		startNewIndex.Append("\n");
		startNewIndex.Append("EndIndex: ");
		startNewIndex.AppendNumber(EndIndex);
		//CA(startNewIndex);

	
		IDocument* piDocument =Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		if(piDocument == nil)
		{ 
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::piDocument == nil ");
			return err;
		}
	
		InterfacePtr<IActiveTopicListContext> piActiveTopicListContext(GetExecutionContextSession(), UseDefaultIID());//cs4
		UIDRef documentTopicListRef = piActiveTopicListContext->GetDefaultTopicList(::GetUIDRef(piDocument));
		if (documentTopicListRef.GetUID() == kInvalidUID)
		{
				
			InterfacePtr<ICommand> piCreateTopicListCmd(CmdUtils::CreateCommand(kCreateTopicListCmdBoss));
			InterfacePtr<ICreateTopicListCmdData> piCreateTopicListCmdData(piCreateTopicListCmd, UseDefaultIID());
			piCreateTopicListCmdData->SetTargetItem(piDocument->GetDocWorkSpace());
			piCreateTopicListCmdData->SetDoNotifyFlag(kTrue); // Always kTrue because the observer on topic list has to be attached

			if (CmdUtils::ProcessCommand(piCreateTopicListCmd) != kSuccess){
				ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::CreateIndexPageEntry 1");
				return err;
			}
			documentTopicListRef = piCreateTopicListCmd->GetItemListReference().GetRef(0);
		}

		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	ptrIAppFramework->LogDebug("AP46_CataLogIndex::CreateIndex::CreateIndexPageEntry::!iSelectionManager");
			return err;
		}

		InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite()/*,UseDefaultIID()*/);//Amit
		if(!txtSelSuite)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::Text Selection Suite is null");
			return err;
		}

		InterfacePtr<IPMUnknown> unknown(BoxUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		if (textFrameUID == kInvalidUID){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::textFrameUID == kInvalidUID");
			return err;

		}
		

		InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ASSERT(graphicFrameHierarchy);
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ASSERT(multiColumnItemHierarchy);
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ASSERT(multiColumnItemTextFrame);
			break;
		}

		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			break;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			break;
		}

		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == nil){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::textModel == nil");
			return err;
		}
		
		
		const UIDRef TextModelUIDRef = ::GetUIDRef(textModel);		
		RangeData r1Data(startIndex,endIndex);		
		txtSelSuite->SetTextSelection(TextModelUIDRef,r1Data,Selection::kDontScrollSelection ,nil);
		
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		InterfacePtr<ITextTarget> piTextTarget(pTextSel, UseDefaultIID());

		InterfacePtr<ITextFocus> piTextFocus(piTextTarget ? piTextTarget->QueryTextFocus() : nil);
		InterfacePtr<ITextModel> piTextModel(piTextFocus->QueryModel());
		UIDRef textModelUIDRef = ::GetUIDRef(piTextModel);


		TextIndex newStart =  piTextFocus->GetStart(0);
		TextIndex newEnd = piTextFocus->GetEnd();
			
		TextIterator firstChar(piTextModel, newStart);
		TextIterator lastChar(piTextModel, newEnd);
		WideString topicString;
		for (TextIterator iter = firstChar; iter != lastChar; ++iter)
		{
			const UTF32TextChar characterCode = *iter;
			if (characterCode != kTextChar_ZeroSpaceNoBreak)
				topicString.Append(characterCode);
		}


		//---------------------------------------------------

		bool16 result = this->IsTextFrameOverset(textFrame);
		if(result)
		{
			CA("Adding New Page");
			
			this->forceRecompose(textFrame);
			
			//::AddNewPage();		//Cs3
			//Utils<ILayoutUIUtils>()->AddNewPage();	//Cs4
			//fromGraphicFrameUIDRef = CreateAndThreadTextFrame(fromGraphicFrameUIDRef);
			
		}
		else
			CA("Not executed");



		//---------------------------------------------------







		IndexTopicEntry	entry;
		entry.GetNumNodes() = Level;

		if(Level >=1)
		{
			entry.GetTopicEntryNode(0).GetLanguage() = kLanguageUserDefault;
			entry.GetTopicEntryNode(0).GetLevel() = 1;
			entry.GetTopicEntryNode(0).GetDisplayString() = FirstString;
		}
		if(Level >=2)
		{
			entry.GetTopicEntryNode(1).GetLanguage() = kLanguageUserDefault;
			entry.GetTopicEntryNode(1).GetLevel() = 2;
			entry.GetTopicEntryNode(1).GetDisplayString() = SecondString;
		}
		if(Level >=3)
		{
			entry.GetTopicEntryNode(2).GetLanguage() = kLanguageUserDefault;
			entry.GetTopicEntryNode(2).GetLevel() = 3;
			entry.GetTopicEntryNode(2).GetDisplayString() = ThirdString;
		}
		if(Level >=4)
		{
			entry.GetTopicEntryNode(3).GetLanguage() = kLanguageUserDefault;
			entry.GetTopicEntryNode(3).GetLevel() = 4;
			entry.GetTopicEntryNode(3).GetDisplayString() = FourthString;
		}
		
		UID sectionUID;
		int32 topicNodeId;

		// Check whether the entry already exists; if not create one
		InterfacePtr<IIndexTopicList> piIndexTopicList(documentTopicListRef, UseDefaultIID());
		if (! piIndexTopicList->FindTopicEntry(entry, &sectionUID, &topicNodeId))
		{
			//CA("......Topic entry not exists");
			// Create a Topic Entry with the obtained string
			InterfacePtr<ICommand> piCreateTopicEntryCmd(CmdUtils::CreateCommand(kCreateTopicEntryCmdBoss));
			InterfacePtr<ICreateTopicEntryCmdData> piCreateTopicCmdData(piCreateTopicEntryCmd, UseDefaultIID());

			piCreateTopicCmdData->SetTopicListUIDRef(documentTopicListRef);
			piCreateTopicCmdData->SetTopicEntry(entry);
			if ((err = CmdUtils::ProcessCommand(piCreateTopicEntryCmd)) != kSuccess)
				break;

			sectionUID = piCreateTopicCmdData->GetTopicSectionUID();
			topicNodeId = piCreateTopicCmdData->GetTopicNodeId();
		}
		
		InterfacePtr<ICommand> piCreatePageEntryCmd(CmdUtils::CreateCommand(kCreateIndexPageEntryCmdBoss));
		InterfacePtr<ICreateIndexPageEntryCmdData> piCreatePageEntryCmdData(piCreatePageEntryCmd, UseDefaultIID());
		InterfacePtr<IIndexPageEntryCmdData> piIndexPageEntryCmdData(piCreatePageEntryCmd, UseDefaultIID());
		piCreatePageEntryCmdData->SetTopicListUIDRef(documentTopicListRef);
		piCreatePageEntryCmdData->SetTopicSectionUID(sectionUID);
		piCreatePageEntryCmdData->SetTopicNodeId(topicNodeId);
		piIndexPageEntryCmdData->SetPageRangeType(IIndexPageEntryData::kCurrentPage, kInvalidUID, 0);
		piIndexPageEntryCmdData->SetStyleUID(kInvalidUID);
	
		if ((err = CmdUtils::ProcessCommand(piCreatePageEntryCmd)) != kSuccess){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::!kSuccess");
			break;
		}

		UIDRef pageEntryUIDRef(piCreatePageEntryCmd->GetItemList()->GetRef(0));
		if (pageEntryUIDRef.GetUID() == kInvalidUID){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::kInvalidUID ");
			break;
		}		
		// Insert a mark in the text and in the owned item strand
		InterfacePtr<ICommand> piInsertIndexMarkCmd(CmdUtils::CreateCommand(kInsertIndexMarkCmdBoss));
		InterfacePtr<IRangeData> piRangeData(piInsertIndexMarkCmd, IID_IRANGEDATA);
		InterfacePtr<IInsertIndexMarkCmdData> piInsertIndexMarkCmdData(piInsertIndexMarkCmd, UseDefaultIID());
		
		piInsertIndexMarkCmdData->SetTargetItem(textModelUIDRef);
		piInsertIndexMarkCmdData->SetPageEntryUIDRef(pageEntryUIDRef);
		piRangeData->Set(newStart, newEnd);

		if((err = CmdUtils::ProcessCommand(piInsertIndexMarkCmd)) != kSuccess){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::err");
			break;
		}

		// Update the index entry page numbers
		InterfacePtr<ICommand> piUpdatePageEntryCmd(CmdUtils::CreateCommand(kUpdatePageEntryCmdBoss));
		piUpdatePageEntryCmd->SetItemList(pageEntryUIDRef);
		if ((err = CmdUtils::ProcessCommand(piUpdatePageEntryCmd)) != kSuccess){
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateIndexPageEntry::!kSuccess ");
			break;
		}	
	}while (false);
	return err;
}

ErrorCode SnpIndexTopics::insertTopicStringInList(MarkerInfo* markerInfo)  //UIDRef BoxUIDRef, int32 StartIndex, int32 EndIndex
{
	ErrorCode err = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return err;

	{
		UIDRef BoxUIDRef = markerInfo->BoxUIDRef;

		TextIndex sIndex=0, eIndex=0;
		Utils<IXMLUtils>()->GetElementIndices(markerInfo->MarkerTagStruct.tagPtr, &sIndex, &eIndex);

		int32 StartIndex = sIndex;	
		int32 EndIndex = eIndex;

		IDocument* piDocument =Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		if(piDocument == nil)
		{ 
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateTopicEntry::piDocument == nil ");
			return err;
		}
	
		InterfacePtr<IActiveTopicListContext> piActiveTopicListContext(GetExecutionContextSession(), UseDefaultIID());//Cs4
		UIDRef documentTopicListRef = piActiveTopicListContext->GetDefaultTopicList(::GetUIDRef(piDocument));
		if (documentTopicListRef.GetUID() == kInvalidUID)
		{
			InterfacePtr<ICommand> piCreateTopicListCmd(CmdUtils::CreateCommand(kCreateTopicListCmdBoss));
			InterfacePtr<ICreateTopicListCmdData> piCreateTopicListCmdData(piCreateTopicListCmd, UseDefaultIID());
			piCreateTopicListCmdData->SetTargetItem(piDocument->GetDocWorkSpace());
			piCreateTopicListCmdData->SetDoNotifyFlag(kTrue); // Always kTrue because the observer on topic list has to be attached

			if (CmdUtils::ProcessCommand(piCreateTopicListCmd) != kSuccess)
				return err;
			documentTopicListRef = piCreateTopicListCmd->GetItemListReference().GetRef(0);
		}

		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateTopicEntry::!iSelectionManager");
			return err;
		}

		InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite()/*,UseDefaultIID()*/);//Amit
		if(!txtSelSuite)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateTopicEntry::Text Selection Suite is null");
			return err;
		}

		InterfacePtr<IPMUnknown> unknown(BoxUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);

		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateTopicEntry::textFrameUID is kInvalidUID");		
			return err;
		}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ASSERT(graphicFrameHierarchy);
			return err;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ASSERT(multiColumnItemHierarchy);
			return err;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ASSERT(multiColumnItemTextFrame);
			return err;
		}

		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			return err;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			return err;
		}

		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateTopicEntry::textModel is nil");		
			return err;
		}

		const UIDRef TextModelUIDRef = ::GetUIDRef(textModel);	
		RangeData rData(StartIndex,EndIndex);		
		txtSelSuite->SetTextSelection(TextModelUIDRef,rData,Selection::kDontScrollSelection ,nil);

		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		InterfacePtr<ITextTarget> piTextTarget(pTextSel, UseDefaultIID()); 

		// Get the string from the text story to add to the topic list
		//InterfacePtr<ITextTarget> piTextTarget(runnableContext, UseDefaultIID());
		InterfacePtr<ITextFocus> piTextFocus(piTextTarget ? piTextTarget->QueryTextFocus() : nil);
		InterfacePtr<ITextModel> piTextModel(piTextFocus->QueryModel());
		UIDRef textModelUIDRef = ::GetUIDRef(piTextModel);

		RangeData range = piTextFocus->GetCurrentRange();
		TextIndex start = range.Start(nil);
		TextIndex end = range.End();
		TextIterator firstChar(piTextModel, start);
		TextIterator lastChar(piTextModel, end);
		WideString topicString;
		for (TextIterator iter = firstChar; iter != lastChar; ++iter)
		{
			const UTF32TextChar characterCode = *iter;
			if (characterCode != kTextChar_ZeroSpaceNoBreak)
				topicString.Append(characterCode);
		}
		if(markerInfo->MarkerTagStruct.whichTab==5)
		{
			PMString tempBuffer = ptrIAppFramework->PUBModel_getAttributeValueByLanguageID(markerInfo->MarkerTagStruct.parentId,markerInfo->MarkerTagStruct.elementId,markerInfo->MarkerTagStruct.languageID,kTrue);
			PMString r("tempBuffer : ");
			r.Append(tempBuffer);
		///	CA(r);
		}
		//CA(__FUNCTION__);
		WideString r("topicString : ");
		r.Append(topicString);
		//CA(r);
		markerInfo->FirstTopicString = topicString;

		err = kSuccess;
	}
	return err;
}

ErrorCode SnpIndexTopics::insertTopicStringInListNew(MarkerInfo* markerInfo,double attributeId)  //UIDRef BoxUIDRef, int32 StartIndex, int32 EndIndex
{
	ErrorCode err = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return err;

	{
		UIDRef BoxUIDRef = markerInfo->BoxUIDRef;

		TextIndex sIndex=0, eIndex=0;
		Utils<IXMLUtils>()->GetElementIndices(markerInfo->MarkerTagStruct.tagPtr, &sIndex, &eIndex);

		int32 StartIndex = sIndex;	
		int32 EndIndex = eIndex;

		IDocument* piDocument =Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		if(piDocument == nil)
		{ 
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateTopicEntry::piDocument == nil ");
			return err;
		}
	
		InterfacePtr<IActiveTopicListContext> piActiveTopicListContext(GetExecutionContextSession(), UseDefaultIID());//Cs4
		UIDRef documentTopicListRef = piActiveTopicListContext->GetDefaultTopicList(::GetUIDRef(piDocument));
		if (documentTopicListRef.GetUID() == kInvalidUID)
		{
			InterfacePtr<ICommand> piCreateTopicListCmd(CmdUtils::CreateCommand(kCreateTopicListCmdBoss));
			InterfacePtr<ICreateTopicListCmdData> piCreateTopicListCmdData(piCreateTopicListCmd, UseDefaultIID());
			piCreateTopicListCmdData->SetTargetItem(piDocument->GetDocWorkSpace());
			piCreateTopicListCmdData->SetDoNotifyFlag(kTrue); // Always kTrue because the observer on topic list has to be attached

			if (CmdUtils::ProcessCommand(piCreateTopicListCmd) != kSuccess)
				return err;
			documentTopicListRef = piCreateTopicListCmd->GetItemListReference().GetRef(0);
		}

		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateTopicEntry::!iSelectionManager");
			return err;
		}

		InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite()/*,UseDefaultIID()*/);//Amit
		if(!txtSelSuite)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateTopicEntry::Text Selection Suite is null");
			return err;
		}

		InterfacePtr<IPMUnknown> unknown(BoxUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);

		if (textFrameUID == kInvalidUID)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateTopicEntry::textFrameUID is kInvalidUID");		
			return err;
		}

		InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ASSERT(graphicFrameHierarchy);
			return err;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ASSERT(multiColumnItemHierarchy);
			return err;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ASSERT(multiColumnItemTextFrame);
			//CA("Its Not MultiColumn");
			return err;
		}

		InterfacePtr<IHierarchy>
		frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			return err;
		}

		InterfacePtr<ITextFrameColumn>
		textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			//CA("!!!ITextFrameColumn");
			return err;
		}

		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == nil)
		{
			ptrIAppFramework->LogDebug("AP46_CatalogIndex::CreateIndex::CreateTopicEntry::textModel is nil");		
			return err;
		}

		const UIDRef TextModelUIDRef = ::GetUIDRef(textModel);	
		RangeData rData(StartIndex,EndIndex);		
		txtSelSuite->SetTextSelection(TextModelUIDRef,rData,Selection::kDontScrollSelection ,nil);

		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		InterfacePtr<ITextTarget> piTextTarget(pTextSel, UseDefaultIID()); 

		// Get the string from the text story to add to the topic list
		//InterfacePtr<ITextTarget> piTextTarget(runnableContext, UseDefaultIID());
		InterfacePtr<ITextFocus> piTextFocus(piTextTarget ? piTextTarget->QueryTextFocus() : nil);
		InterfacePtr<ITextModel> piTextModel(piTextFocus->QueryModel());
		UIDRef textModelUIDRef = ::GetUIDRef(piTextModel);

		RangeData range = piTextFocus->GetCurrentRange();
		TextIndex start = range.Start(nil);
		TextIndex end = range.End();
		TextIterator firstChar(piTextModel, start);
		TextIterator lastChar(piTextModel, end);
		WideString topicString;
		for (TextIterator iter = firstChar; iter != lastChar; ++iter)
		{
			const UTF32TextChar characterCode = *iter;
			if (characterCode != kTextChar_ZeroSpaceNoBreak)
				topicString.Append(characterCode);
		}
		//markerInfo->MarkerTagStruct
		PMString str;

		if(markerInfo->MarkerTagStruct.whichTab==4)
		{
			//CA("1");
			str=ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(markerInfo->MarkerTagStruct.parentId,attributeId,markerInfo->MarkerTagStruct.languageID, markerInfo->MarkerTagStruct.sectionID, kTrue);
			topicString=static_cast<WideString>(str);
			//PMString asd("Str1 : ");
			//asd.Append(str);
			//CA(asd);
		}
		if(markerInfo->MarkerTagStruct.whichTab==3)
		{
			//CA("2");
			//PMString asd("Str1 : ");
			str=ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(markerInfo->MarkerTagStruct.parentId,attributeId,markerInfo->MarkerTagStruct.languageID, markerInfo->MarkerTagStruct.sectionID, kTrue);
			topicString=static_cast<WideString>(str);
			//PMString asd("Str2 : ");
			//asd.Append(str);
			//CA(asd);
		}
		//PMString q("InputID : ");
		//q.AppendNumber(markerInfo->MarkerTagStruct.parentId);
		//q.Append("\n");
		//q.Append("AttibuteID : ");
		//q.AppendNumber(attributeId);
		//CA(q);
		if(markerInfo->MarkerTagStruct.whichTab==5)
		{
			//CA("3");
			str = ptrIAppFramework->PUBModel_getAttributeValueByLanguageID(markerInfo->MarkerTagStruct.parentId,attributeId,markerInfo->MarkerTagStruct.languageID,kTrue);
			topicString=static_cast<WideString>(str);
			//PMString asd("Str3 : ");
			//asd.Append(str);
			//CA(asd);
		}
		
		/*topicString=static_cast<WideString>(str);*/
		markerInfo->FirstTopicString = topicString;
		//CA(__FUNCTION__);
		/*if(markerInfo->MarkerTagStruct.parentId==22677)
		PMString q("parent ID: ");
			q.AppendNumber(markerInfo->MarkerTagStruct.parentId);
			q.Append("\n attribute ID: ");
			q.AppendNumber(attributeId);
			q.Append("\n");
			q.Append("str : ");
			q.Append(str);
			CA(q);*/
		//WideString asd("topicString : ");
		//asd.Append(topicString);
		//CA(asd);
		err = kSuccess;
	}
	return err;
}

bool16 SnpIndexTopics::doesExist(TagList & tagList,const UIDList& selectUIDList1)
{
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		return kFalse;
	}
	TagList tList;
	for(int i=0; i<selectUIDList1.Length(); i++)
	{
		tagList=itagReader->getTagsFromBox(selectUIDList1.GetRef(i));
		
		if(tList.size()==0||tagList.size()==0 || !tList[0].tagPtr || !tagList[0].tagPtr )
			continue;

		if(tagList[0].tagPtr == tList[0].tagPtr )
		{
			return kTrue;
		}
	}
	return kFalse;
}


/// Function to get elements from Whole Document
VectorMarkerInfoPtr SnpIndexTopics::getDocumentSelectedBoxIds(AttributeListData attributeListDataObj)
{
	double SelectedAttrID = attributeListDataObj.id;
	VectorMarkerInfoPtr vectorMarkerInfoPtr = new VectorMarkerInfo;

	IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();  
	if(fntDoc==nil)
		return vectorMarkerInfoPtr;

	InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layout == nil)
		return vectorMarkerInfoPtr;

	IDataBase* database = ::GetDataBase(fntDoc);
	if(database==nil)
		return vectorMarkerInfoPtr;

	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
	if (iSpreadList==nil)
		return vectorMarkerInfoPtr;

	bool16 collisionFlag=kFalse;
	UIDList allPageItems(database);

	InterfacePtr<ITagReader> itagReader
	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
		return vectorMarkerInfoPtr;
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return vectorMarkerInfoPtr;
	ptrIAppFramework->LogDebug("AP7_CatalogIndex:: Before getting all boxes in entire document.");
 do
   {
	for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
	{
		
		UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
		if(!spread)
			break;

		int numPages=spread->GetNumPages();

		for(int i=0; i<numPages; i++)
		{			
			UIDList tempList(database);
			UID PageUID = spread->GetNthPageUID(i);
			UIDRef PageUIDRef(database, PageUID);
			spread->GetItemsOnPage(i, &tempList, kFalse);
			allPageItems.Append(tempList);

		}
	}
	UIDList selectUIDList= allPageItems;

	PMString str("");
	str.AppendNumber(static_cast<double>(selectUIDList.Length()));

	
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		bool16 isGroupFrame = kFalse ;
		isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(i));

		if(isGroupFrame == kTrue) 
		{
			
			InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
			if(!iHier)
				continue;

			UID kidUID;
			int32 numKids=iHier->GetChildCount();
			for(int j=0 ;j < numKids ;j++)
			{
				
				IIDXMLElement* ptr;
				kidUID=iHier->GetChildUID(j);
				UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
				TagList tagList;
				tagList=itagReader->getTagsFromBox(boxRef, &ptr);
				if(!doesExist(tagList,selectUIDList))//if(!doesExist(ptr))
					selectUIDList.Append(kidUID);
				
				if(tagList.size() > 0)
				{
					for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
					{
						if(tagList[tagIndex].tagPtr)
							tagList[tagIndex].tagPtr->Release();						
					}
				}
			}
			
		}
	}

	PMString str1("");
	str1.AppendNumber(static_cast<double>(selectUIDList.Length()));
	ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after iterating all boxes to find out group frames. number of boxes found = " + str1);

	ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before iterating boxes to find out the selected tag");
	for(int32 j=0; j<selectUIDList.Length(); j++)
	{
		TagList CurrBoxTagList = itagReader->getTextFrameTagsForIndex(selectUIDList.GetRef(j));
		if(CurrBoxTagList.size()==0)
		{
			TagList tList;
			tList=itagReader->getTagsFromBox(selectUIDList.GetRef(j));
			if(tList.size() > 0)
			{	
				TagStruct tstruct = tList[0];
				if(tstruct.isTablePresent || tstruct.elementId  == -101 )
				{
					PMString tagString = tstruct.tagPtr->GetTagString(); 
					int32 childCount = tstruct.tagPtr->GetChildCount(); 
					
					for(int32 childIndex = 0 ; childIndex < childCount ; childIndex++)
					{
						XMLReference elementXMLref = tstruct.tagPtr->GetNthChild(childIndex);
						InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
						if(childElement==nil)
							continue;
						PMString ChildtagName=childElement->GetTagString();
						int elementCount=childElement->GetChildCount();
						for(int elementIndex=0; elementIndex<elementCount; elementIndex++)
						{
							XMLReference elementXMLref1 = childElement->GetNthChild(elementIndex);
							InterfacePtr<IIDXMLElement>childTagXMLElementPtr(elementXMLref1.Instantiate());
							if(childTagXMLElementPtr == nil)
								continue;
							
							PMString ChildtagName1 = childTagXMLElementPtr->GetTagString();
							PMString displayTagData;
							PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID")); //Cs4
							double ID = strID.GetAsNumber();									
							
							PMString strTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("typeId")); //Cs4
							double typeID = strTypeID.GetAsNumber();													
							
							PMString strIndex = childTagXMLElementPtr->GetAttributeValue(WideString("index")); //Cs4
							int32 index = strIndex.GetAsNumber();				

							PMString strImgFlag = childTagXMLElementPtr->GetAttributeValue(WideString("imgFlag")); //Cs4
							int32 imgFlag = strImgFlag.GetAsNumber();								

							PMString strParentID = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
							double parentID = strParentID.GetAsNumber();				
							
							PMString strParentTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));//Cs4
							double parentTypeID = strParentTypeID.GetAsNumber();				
							
							PMString strSectionID = childTagXMLElementPtr->GetAttributeValue(WideString("sectionID"));//Cs4
							double sectionID = strSectionID.GetAsNumber();
							
							PMString strTableFlag = childTagXMLElementPtr->GetAttributeValue(WideString("tableFlag"));//CS4
							int32 tableFlag = strTableFlag.GetAsNumber();

							PMString strLanguageID = childTagXMLElementPtr->GetAttributeValue(WideString("LanguageID"));//Cs4
							double languageID = strLanguageID.GetAsNumber();

							PMString strIsAutoResize = childTagXMLElementPtr->GetAttributeValue(WideString("isAutoResize"));//Cs4
							int32 isAutoResize = strIsAutoResize.GetAsNumber();

							PMString strchildTag = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));//Cs4
							int32 childTag = strchildTag.GetAsNumber();

							PMString strchildId = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
							double childId = strchildId.GetAsNumber();

							PMString strcatLevel = childTagXMLElementPtr->GetAttributeValue(WideString("catLevel"));//Cs4
							int32 catLevel = strcatLevel.GetAsNumber();

							TagStruct tagInfo;
							tagInfo.elementId = ID;
							childTagXMLElementPtr->AddRef();//Added on 17-09-08
							tagInfo.tagPtr = childTagXMLElementPtr;
							tagInfo.typeId = typeID;
							tagInfo.languageID =  languageID;
							tagInfo.parentId = parentID;
							tagInfo.parentTypeID = parentTypeID;
							tagInfo.sectionID = sectionID;
							tagInfo.isAutoResize = isAutoResize;
							tagInfo.whichTab = index;
							tagInfo.imgFlag = imgFlag;
                            tagInfo.isTablePresent = kFalse;
							tagInfo.childTag = childTag;
							tagInfo.childId = childId;
							tagInfo.catLevel = catLevel;
															
							TextIndex sIndex=0, eIndex=0;
							Utils<IXMLUtils>()->GetElementIndices(childTagXMLElementPtr, &sIndex, &eIndex);
							
							tagInfo.startIndex=sIndex;
							tagInfo.endIndex=eIndex;						

							if((ID == SelectedAttrID) && (index == 3 || index == 4)  && (parentID != -1) )
							{

								MarkerInfo* NewMark = new MarkerInfo;
								NewMark->BoxUIDRef = selectUIDList.GetRef(j);
								NewMark->StartIndex = tagInfo.startIndex;
								NewMark->EndIndex = tagInfo.endIndex;
								NewMark->ParentId = tagInfo.parentId;
								NewMark->MarkerTagStruct = tagInfo;
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
								insertTopicStringInList(NewMark);
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
								vectorMarkerInfoPtr->push_back(NewMark);
							}
							else if((ID == SelectedAttrID) && (index == 5)  && (parentID != -1) && catLevel == attributeListDataObj.catLevel)
							{

								MarkerInfo* NewMark = new MarkerInfo;
								NewMark->BoxUIDRef = selectUIDList.GetRef(j);
								NewMark->StartIndex = tagInfo.startIndex;
								NewMark->EndIndex = tagInfo.endIndex;
								NewMark->ParentId = tagInfo.parentId;
								NewMark->MarkerTagStruct = tagInfo;
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
								insertTopicStringInList(NewMark);
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
								vectorMarkerInfoPtr->push_back(NewMark);
							}
						}
					}
				}
			}

			if(tList.size() > 0)
			{
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
			}
			continue;
		}


		for(int32 p=0; p<CurrBoxTagList.size(); p++ )
		{			
			if(CurrBoxTagList[p].elementId == SelectedAttrID && (CurrBoxTagList[p].whichTab == 3 || CurrBoxTagList[p].whichTab == 4 || CurrBoxTagList[p].whichTab == 5)  && CurrBoxTagList[p].isTablePresent == kFalse && CurrBoxTagList[p].imgFlag ==0 && CurrBoxTagList[p].parentId != -1)
			{	
				if(CurrBoxTagList[p].whichTab == 5)
				{
					if(CurrBoxTagList[p].catLevel == attributeListDataObj.catLevel)
					{
						MarkerInfo* NewMark = new MarkerInfo;
						NewMark->BoxUIDRef = selectUIDList.GetRef(j);
						NewMark->StartIndex = CurrBoxTagList[p].startIndex;
						NewMark->EndIndex = CurrBoxTagList[p].endIndex;
						NewMark->ParentId = CurrBoxTagList[p].parentId;
						NewMark->MarkerTagStruct = CurrBoxTagList[p];
						CurrBoxTagList[p].tagPtr->AddRef();
						NewMark->MarkerTagStruct.tagPtr  = CurrBoxTagList[p].tagPtr;
				ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
						insertTopicStringInList(NewMark);
				ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
						vectorMarkerInfoPtr->push_back(NewMark);
					}					
				}
				else
				{
					MarkerInfo* NewMark = new MarkerInfo;
					NewMark->BoxUIDRef = selectUIDList.GetRef(j);
					NewMark->StartIndex = CurrBoxTagList[p].startIndex;
					NewMark->EndIndex = CurrBoxTagList[p].endIndex;
					NewMark->ParentId = CurrBoxTagList[p].parentId;
					NewMark->MarkerTagStruct = CurrBoxTagList[p];
					CurrBoxTagList[p].tagPtr->AddRef();
					NewMark->MarkerTagStruct.tagPtr  = CurrBoxTagList[p].tagPtr;
			ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
					insertTopicStringInList(NewMark);
			ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
					vectorMarkerInfoPtr->push_back(NewMark);
				}
			}
			if(CurrBoxTagList[p].typeId == -5 || CurrBoxTagList[p].elementId == -102)
			{	
				
				int32 childTagCount = CurrBoxTagList[p].tagPtr->GetChildCount();
			    for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
				{
					XMLReference childTagRef = CurrBoxTagList[p].tagPtr->GetNthChild(childTagIndex);
					InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
					if(childTagXMLElementPtr == nil)
						continue;						
					TagStruct tagInfo;
	
					if(CurrBoxTagList[p].typeId == -5){
						PMString displayTagData;
						PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));//CS4
						double ID = strID.GetAsNumber();									
						
						PMString strTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("typeId"));//Cs4
						double typeID = strTypeID.GetAsNumber();													
						
						PMString strIndex = childTagXMLElementPtr->GetAttributeValue(WideString("index"));//CS4
						int32 index = strIndex.GetAsNumber();				

						PMString strImgFlag = childTagXMLElementPtr->GetAttributeValue(WideString("imgFlag"));//Cs4
						int32 imgFlag = strImgFlag.GetAsNumber();								

						PMString strParentID = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
						double parentID = strParentID.GetAsNumber();				
						
						PMString strParentTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));//Cs4
						double parentTypeID = strParentTypeID.GetAsNumber();				
						
						PMString strSectionID = childTagXMLElementPtr->GetAttributeValue(WideString("sectionID"));//Cs4
						double sectionID = strSectionID.GetAsNumber();
						
						PMString strTableFlag = childTagXMLElementPtr->GetAttributeValue(WideString("tableFlag"));//Cs4
						int32 tableFlag = strTableFlag.GetAsNumber();

						PMString strLanguageID = childTagXMLElementPtr->GetAttributeValue(WideString("LanguageID"));//Cs4
						double languageID = strLanguageID.GetAsNumber();

						PMString strIsAutoResize = childTagXMLElementPtr->GetAttributeValue(WideString("isAutoResize"));//Cs4
						int32 isAutoResize = strIsAutoResize.GetAsNumber();

						PMString strchildTag = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));//Cs4
						int32 childTag = strchildTag.GetAsNumber();

						PMString strchildId = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
						double childId = strchildId.GetAsNumber();

						PMString strcatLevel = childTagXMLElementPtr->GetAttributeValue(WideString("catLevel"));//Cs4
						int32 catLevel = strcatLevel.GetAsNumber();

						
						tagInfo.elementId = ID;
						childTagXMLElementPtr->AddRef();//Added on 17-09-08
						tagInfo.tagPtr = childTagXMLElementPtr;
						tagInfo.typeId = typeID;
						tagInfo.languageID = languageID;
						tagInfo.parentId = parentID;
						tagInfo.parentTypeID = parentTypeID;
						tagInfo.sectionID = sectionID;
						tagInfo.isAutoResize = isAutoResize;
						tagInfo.whichTab = index;
						tagInfo.imgFlag = imgFlag;
						tagInfo.isTablePresent = kFalse;
						tagInfo.childTag = childTag;
						tagInfo.childId = childId;
						tagInfo.catLevel = catLevel;
						
						TextIndex sIndex=0, eIndex=0;
						Utils<IXMLUtils>()->GetElementIndices(childTagXMLElementPtr, &sIndex, &eIndex);
					
						tagInfo.startIndex=sIndex;
						tagInfo.endIndex=eIndex;

						if(ID == SelectedAttrID && (index == 3 || index == 4 || index == 5)  && parentID != -1)
						{	
							MarkerInfo* NewMark = new MarkerInfo;
							NewMark->BoxUIDRef = selectUIDList.GetRef(j);
							NewMark->StartIndex = tagInfo.startIndex;
							NewMark->EndIndex = tagInfo.endIndex;
							NewMark->ParentId = parentID;
							NewMark->MarkerTagStruct = tagInfo;
					ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
							insertTopicStringInList(NewMark);
					ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
							vectorMarkerInfoPtr->push_back(NewMark);
						}
					}
					else if(CurrBoxTagList[p].elementId == -102){

						int32 childTagCount1 = childTagXMLElementPtr->GetChildCount();
						for(int32 childTagIndex = 0;childTagIndex < childTagCount1;childTagIndex++)
						{
							XMLReference childTagRef = childTagXMLElementPtr->GetNthChild(childTagIndex);
							InterfacePtr<IIDXMLElement>childTagXMLElementPtr1(childTagRef.Instantiate());
							if(childTagXMLElementPtr1 == nil)
								continue;						

							PMString strID = childTagXMLElementPtr1->GetAttributeValue(WideString("ID"));//CS4
							double ID = strID.GetAsNumber();									
							
							PMString strTypeID = childTagXMLElementPtr1->GetAttributeValue(WideString("typeId"));//Cs4
							double typeID = strTypeID.GetAsNumber();													
							
							PMString strIndex = childTagXMLElementPtr1->GetAttributeValue(WideString("index"));//CS4
							int32 index = strIndex.GetAsNumber();				

							PMString strImgFlag = childTagXMLElementPtr1->GetAttributeValue(WideString("imgFlag"));//Cs4
							int32 imgFlag = strImgFlag.GetAsNumber();								

							PMString strParentID = childTagXMLElementPtr1->GetAttributeValue(WideString("parentID"));//Cs4
							double parentID = strParentID.GetAsNumber();				
							
							PMString strParentTypeID = childTagXMLElementPtr1->GetAttributeValue(WideString("parentTypeID"));//Cs4
							double parentTypeID = strParentTypeID.GetAsNumber();				
							
							PMString strSectionID = childTagXMLElementPtr1->GetAttributeValue(WideString("sectionID"));//Cs4
							double sectionID = strSectionID.GetAsNumber();
							
							PMString strTableFlag = childTagXMLElementPtr1->GetAttributeValue(WideString("tableFlag"));//Cs4
							int32 tableFlag = strTableFlag.GetAsNumber();

							PMString strLanguageID = childTagXMLElementPtr1->GetAttributeValue(WideString("LanguageID"));//Cs4
							double languageID = strLanguageID.GetAsNumber();

							PMString strIsAutoResize = childTagXMLElementPtr1->GetAttributeValue(WideString("isAutoResize"));//Cs4
							int32 isAutoResize = strIsAutoResize.GetAsNumber();

							PMString strchildTag = childTagXMLElementPtr1->GetAttributeValue(WideString("childTag"));//Cs4
							int32 childTag = strchildTag.GetAsNumber();

							PMString strchildId = childTagXMLElementPtr1->GetAttributeValue(WideString("childId"));//Cs4
							double childId = strchildId.GetAsNumber();

							PMString strcatLevel = childTagXMLElementPtr1->GetAttributeValue(WideString("catLevel"));//Cs4
							int32 catLevel = strcatLevel.GetAsNumber();

							
							tagInfo.elementId = ID;
							childTagXMLElementPtr1->AddRef();//Added on 17-09-08
							tagInfo.tagPtr = childTagXMLElementPtr1;
							tagInfo.typeId = typeID;
							tagInfo.languageID = languageID;
							tagInfo.parentId = parentID;
							tagInfo.parentTypeID = parentTypeID;
							tagInfo.sectionID = sectionID;
							tagInfo.isAutoResize = isAutoResize;
							tagInfo.whichTab = index;
							tagInfo.imgFlag = imgFlag;
							tagInfo.isTablePresent = kFalse;
							tagInfo.childTag = childTag;
							tagInfo.childId = childId;
							tagInfo.catLevel = catLevel;
							
							TextIndex sIndex=0, eIndex=0;
							Utils<IXMLUtils>()->GetElementIndices(childTagXMLElementPtr1, &sIndex, &eIndex);
						
							tagInfo.startIndex=sIndex;
							tagInfo.endIndex=eIndex;


							if(ID == SelectedAttrID && (index == 3 || index == 4 || index == 5)  && parentID != -1)
							{	

								MarkerInfo* NewMark = new MarkerInfo;
								NewMark->BoxUIDRef = selectUIDList.GetRef(j);
								NewMark->StartIndex = tagInfo.startIndex;
								NewMark->EndIndex = tagInfo.endIndex;
								NewMark->ParentId = parentID;
								NewMark->MarkerTagStruct = tagInfo;
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
								insertTopicStringInList(NewMark);
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
								vectorMarkerInfoPtr->push_back(NewMark);
							}
						}
					}				
					
				}

			}
		}
		
		if(CurrBoxTagList.size() > 0)
		{
			for(int32 tagIndex = 0 ; tagIndex < CurrBoxTagList.size() ; tagIndex++)
			{
				CurrBoxTagList[tagIndex].tagPtr->Release();
			}
		}
	
	}

	ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after iterating boxes to find out the selected tag");
 }while(0);
 return vectorMarkerInfoPtr;
}



void SnpIndexTopics::getDocumentSelectedBoxIds(UIDList& selectUIDList)
{
	
	IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();  
	if(fntDoc==nil)
		return;

	InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layout == nil)
		return ;

	IDataBase* database = ::GetDataBase(fntDoc);
	if(database==nil)
		return ;

	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
	if (iSpreadList==nil)
		return ;

	bool16 collisionFlag=kFalse;
	UIDList allPageItems(database);

	InterfacePtr<ITagReader> itagReader
	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
		return ;
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return ;
	ptrIAppFramework->LogDebug("AP7_CatalogIndex:: Before getting all boxes in entire document.");
 do
   {
	for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
	{
		
		UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
		if(!spread)
			break;

		int numPages=spread->GetNumPages();

		for(int i=0; i<numPages; i++)
		{			
			UIDList tempList(database);
			UID PageUID = spread->GetNthPageUID(i);
			UIDRef PageUIDRef(database, PageUID);
			spread->GetItemsOnPage(i, &tempList, kFalse);
			allPageItems.Append(tempList);

		}
	}
	selectUIDList= allPageItems;

	PMString str("");
	str.AppendNumber(static_cast<double>(selectUIDList.Length()));

	

	ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after getting all boxes in entire document. number of boxes found = " + str);

	ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before iterating all boxes to find out group frames. ");
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		bool16 isGroupFrame = kFalse ;
		isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(i));

		if(isGroupFrame == kTrue) 
		{
			
			InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
			if(!iHier)
				continue;

			UID kidUID;
			int32 numKids=iHier->GetChildCount();
			for(int j=0 ;j < numKids ;j++)
			{
				
				IIDXMLElement* ptr;
				kidUID=iHier->GetChildUID(j);
				UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
				TagList tagList;
				tagList=itagReader->getTagsFromBox(boxRef, &ptr);
				selectUIDList.Append(kidUID);
				
				if(tagList.size() > 0)
				{
					for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
					{
						if(tagList[tagIndex].tagPtr)
							tagList[tagIndex].tagPtr->Release();						
					}
				}
			}
			
		}
	}

	PMString str1("");
	str1.AppendNumber(static_cast<double>(selectUIDList.Length()));
	ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after iterating all boxes to find out group frames. number of boxes found = " + str1);

 }while(0);
 return ;
}

VectorMarkerInfoPtr SnpIndexTopics::getDocumentSelectedBoxIdsFor2Level(AttributeListData attributeListDataObj, UIDList& selectUIDList)
{

	//CA(__FUNCTION__);
	double SelectedAttrID = attributeListDataObj.id;
	
	VectorMarkerInfoPtr vectorMarkerInfoPtr = new VectorMarkerInfo;

	IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();  
	if(fntDoc==nil)
		return vectorMarkerInfoPtr;

	InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layout == nil)
		return vectorMarkerInfoPtr;

	IDataBase* database = ::GetDataBase(fntDoc);
	if(database==nil)
		return vectorMarkerInfoPtr;

	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
	if (iSpreadList==nil)
		return vectorMarkerInfoPtr;

	bool16 collisionFlag=kFalse;
	UIDList allPageItems(database);

	InterfacePtr<ITagReader> itagReader
	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
		return vectorMarkerInfoPtr;
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return vectorMarkerInfoPtr;
	ptrIAppFramework->LogDebug("AP7_CatalogIndex:: Before getting all boxes in entire document.");

   do
   {

	ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before iterating boxes to find out the selected tag");
	for(int32 j=0; j<selectUIDList.Length(); j++)
	{
		
		TagList CurrBoxTagList = itagReader->getTextFrameTagsForIndex(selectUIDList.GetRef(j));
		if(CurrBoxTagList.size()==0)
		{
			TagList tList;
			tList=itagReader->getTagsFromBox(selectUIDList.GetRef(j));
			if(tList.size() > 0)
			{	
				TagStruct tstruct = tList[0];
				if(tstruct.isTablePresent || tstruct.elementId  == -101 )
				{
					PMString tagString = tstruct.tagPtr->GetTagString(); 
					int32 childCount = tstruct.tagPtr->GetChildCount(); 
					
					for(int32 childIndex = 0 ; childIndex < childCount ; childIndex++)
					{
						XMLReference elementXMLref = tstruct.tagPtr->GetNthChild(childIndex);
						InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
						if(childElement==nil)
							continue;
						PMString ChildtagName=childElement->GetTagString();
						int elementCount=childElement->GetChildCount();
						for(int elementIndex=0; elementIndex<elementCount; elementIndex++)
						{
							XMLReference elementXMLref1 = childElement->GetNthChild(elementIndex);
							InterfacePtr<IIDXMLElement>childTagXMLElementPtr(elementXMLref1.Instantiate());
							if(childTagXMLElementPtr == nil)
								continue;
							
							PMString ChildtagName1 = childTagXMLElementPtr->GetTagString();
							PMString displayTagData;
							PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID")); //Cs4
							double ID = strID.GetAsNumber();									
							
							PMString strTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("typeId")); //Cs4
							double typeID = strTypeID.GetAsNumber();													
							
							PMString strIndex = childTagXMLElementPtr->GetAttributeValue(WideString("index")); //Cs4
							int32 index = strIndex.GetAsNumber();				

							PMString strImgFlag = childTagXMLElementPtr->GetAttributeValue(WideString("imgFlag")); //Cs4
							int32 imgFlag = strImgFlag.GetAsNumber();								

							PMString strParentID = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
							double parentID = strParentID.GetAsNumber();				
							
							PMString strParentTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));//Cs4
							double parentTypeID = strParentTypeID.GetAsNumber();				
							
							PMString strSectionID = childTagXMLElementPtr->GetAttributeValue(WideString("sectionID"));//Cs4
							double sectionID = strSectionID.GetAsNumber();
							
							PMString strTableFlag = childTagXMLElementPtr->GetAttributeValue(WideString("tableFlag"));//CS4
							int32 tableFlag = strTableFlag.GetAsNumber();

							PMString strLanguageID = childTagXMLElementPtr->GetAttributeValue(WideString("LanguageID"));//Cs4
							double languageID = strLanguageID.GetAsNumber();

							PMString strIsAutoResize = childTagXMLElementPtr->GetAttributeValue(WideString("isAutoResize"));//Cs4
							int32 isAutoResize = strIsAutoResize.GetAsNumber();

							PMString strchildTag = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));//Cs4
							int32 childTag = strchildTag.GetAsNumber();

							PMString strchildId = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
							double childId = strchildId.GetAsNumber();

							PMString strcatLevel = childTagXMLElementPtr->GetAttributeValue(WideString("catLevel"));//Cs4
							int32 catLevel = strcatLevel.GetAsNumber();

							TagStruct tagInfo;
							tagInfo.elementId = ID;
							childTagXMLElementPtr->AddRef();//Added on 17-09-08
							tagInfo.tagPtr = childTagXMLElementPtr;
							tagInfo.typeId = typeID;
							tagInfo.languageID =  languageID;
							tagInfo.parentId = parentID;
							tagInfo.parentTypeID = parentTypeID;
							tagInfo.sectionID = sectionID;
							tagInfo.isAutoResize = isAutoResize;
							tagInfo.whichTab = index;
							tagInfo.imgFlag = imgFlag;
                            tagInfo.isTablePresent = kFalse;
							tagInfo.childTag = childTag;
							tagInfo.childId = childId;
							tagInfo.catLevel = catLevel;
															
							TextIndex sIndex=0, eIndex=0;
							Utils<IXMLUtils>()->GetElementIndices(childTagXMLElementPtr, &sIndex, &eIndex);
							
							tagInfo.startIndex=sIndex;
							tagInfo.endIndex=eIndex;						

							if((ID == SelectedAttrID) && (index == 3 || index == 4)  && (parentID != -1) )
							{
								MarkerInfo* NewMark = new MarkerInfo;
								NewMark->BoxUIDRef = selectUIDList.GetRef(j);
								NewMark->StartIndex = tagInfo.startIndex;
								NewMark->EndIndex = tagInfo.endIndex;
								NewMark->ParentId = tagInfo.parentId;
								NewMark->MarkerTagStruct = tagInfo;
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
								insertTopicStringInList(NewMark);
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
								vectorMarkerInfoPtr->push_back(NewMark);
							}
							else if((ID == SelectedAttrID) && (index == 5)  && (parentID != -1) && catLevel == attributeListDataObj.catLevel)
							{

								MarkerInfo* NewMark = new MarkerInfo;
								NewMark->BoxUIDRef = selectUIDList.GetRef(j);
								NewMark->StartIndex = tagInfo.startIndex;
								NewMark->EndIndex = tagInfo.endIndex;
								NewMark->ParentId = tagInfo.parentId;
								NewMark->MarkerTagStruct = tagInfo;
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
								insertTopicStringInList(NewMark);
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
								vectorMarkerInfoPtr->push_back(NewMark);
							}
						}
					}
				}
			}
			if(tList.size() > 0)
			{
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
			}
			continue;
		}


		for(int32 p=0; p<CurrBoxTagList.size(); p++ )
		{			
			
			if(CurrBoxTagList[p].elementId == SelectedAttrID && (CurrBoxTagList[p].whichTab == 3 || CurrBoxTagList[p].whichTab == 4 || CurrBoxTagList[p].whichTab == 5)  && CurrBoxTagList[p].isTablePresent == kFalse && CurrBoxTagList[p].imgFlag ==0 && CurrBoxTagList[p].parentId != -1)
			{	
				//CA("456");
				if(CurrBoxTagList[p].whichTab == 5)
				{
					/*if(CurrBoxTagList[p].elementId == SelectedAttrID)
					{*/
						//if(CurrBoxTagList[p].catLevel == attributeListDataObj.catLevel)
						//{
							MarkerInfo* NewMark = new MarkerInfo;
							NewMark->BoxUIDRef = selectUIDList.GetRef(j);
							NewMark->StartIndex = CurrBoxTagList[p].startIndex;
							NewMark->EndIndex = CurrBoxTagList[p].endIndex;
							NewMark->ParentId = CurrBoxTagList[p].parentId;
							NewMark->MarkerTagStruct = CurrBoxTagList[p];
							CurrBoxTagList[p].tagPtr->AddRef();
							NewMark->MarkerTagStruct.tagPtr  = CurrBoxTagList[p].tagPtr;
							ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
							//insertTopicStringInList(NewMark);
					
							insertTopicStringInListNew(NewMark,SelectedAttrID);
							ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
							vectorMarkerInfoPtr->push_back(NewMark);
						//}	
					//}
				}
				else
				{
					MarkerInfo* NewMark = new MarkerInfo;
					NewMark->BoxUIDRef = selectUIDList.GetRef(j);
					NewMark->StartIndex = CurrBoxTagList[p].startIndex;
					NewMark->EndIndex = CurrBoxTagList[p].endIndex;
					NewMark->ParentId = CurrBoxTagList[p].parentId;
					NewMark->MarkerTagStruct = CurrBoxTagList[p];
					CurrBoxTagList[p].tagPtr->AddRef();
					NewMark->MarkerTagStruct.tagPtr  = CurrBoxTagList[p].tagPtr;
			ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
			//insertTopicStringInList(NewMark);
					insertTopicStringInListNew(NewMark,SelectedAttrID);
			ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
					vectorMarkerInfoPtr->push_back(NewMark);
				}
			}
			if(CurrBoxTagList[p].typeId == -5 || CurrBoxTagList[p].elementId == -102)
			{	
				int32 childTagCount = CurrBoxTagList[p].tagPtr->GetChildCount();
			    for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
				{
					XMLReference childTagRef = CurrBoxTagList[p].tagPtr->GetNthChild(childTagIndex);
					InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
					if(childTagXMLElementPtr == nil)
						continue;						
					TagStruct tagInfo;
	
					if(CurrBoxTagList[p].typeId == -5){
						PMString displayTagData;
						PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));//CS4
						double ID = strID.GetAsNumber();									
						
						PMString strTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("typeId"));//Cs4
						double typeID = strTypeID.GetAsNumber();													
						
						PMString strIndex = childTagXMLElementPtr->GetAttributeValue(WideString("index"));//CS4
						int32 index = strIndex.GetAsNumber();				

						PMString strImgFlag = childTagXMLElementPtr->GetAttributeValue(WideString("imgFlag"));//Cs4
						int32 imgFlag = strImgFlag.GetAsNumber();								

						PMString strParentID = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));//Cs4
						double parentID = strParentID.GetAsNumber();				
						
						PMString strParentTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));//Cs4
						double parentTypeID = strParentTypeID.GetAsNumber();				
						
						PMString strSectionID = childTagXMLElementPtr->GetAttributeValue(WideString("sectionID"));//Cs4
						double sectionID = strSectionID.GetAsNumber();
						
						PMString strTableFlag = childTagXMLElementPtr->GetAttributeValue(WideString("tableFlag"));//Cs4
						int32 tableFlag = strTableFlag.GetAsNumber();

						PMString strLanguageID = childTagXMLElementPtr->GetAttributeValue(WideString("LanguageID"));//Cs4
						double languageID = strLanguageID.GetAsNumber();

						PMString strIsAutoResize = childTagXMLElementPtr->GetAttributeValue(WideString("isAutoResize"));//Cs4
						int32 isAutoResize = strIsAutoResize.GetAsNumber();

						PMString strchildTag = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));//Cs4
						int32 childTag = strchildTag.GetAsNumber();

						PMString strchildId = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));//Cs4
						double childId = strchildId.GetAsNumber();

						PMString strcatLevel = childTagXMLElementPtr->GetAttributeValue(WideString("catLevel"));//Cs4
						int32 catLevel = strcatLevel.GetAsNumber();

						
						tagInfo.elementId = ID;
						childTagXMLElementPtr->AddRef();//Added on 17-09-08
						tagInfo.tagPtr = childTagXMLElementPtr;
						tagInfo.typeId = typeID;
						tagInfo.languageID = languageID;
						tagInfo.parentId = parentID;
						tagInfo.parentTypeID = parentTypeID;
						tagInfo.sectionID = sectionID;
						tagInfo.isAutoResize = isAutoResize;
						tagInfo.whichTab = index;
						tagInfo.imgFlag = imgFlag;
						tagInfo.isTablePresent = kFalse;
						tagInfo.childTag = childTag;
						tagInfo.childId = childId;
						tagInfo.catLevel = catLevel;
						
						TextIndex sIndex=0, eIndex=0;
						Utils<IXMLUtils>()->GetElementIndices(childTagXMLElementPtr, &sIndex, &eIndex);
					
						tagInfo.startIndex=sIndex;
						tagInfo.endIndex=eIndex;

						if(ID == SelectedAttrID && (index == 3 || index == 4 || index == 5)  && parentID != -1)
						{	

							MarkerInfo* NewMark = new MarkerInfo;
							NewMark->BoxUIDRef = selectUIDList.GetRef(j);
							NewMark->StartIndex = tagInfo.startIndex;
							NewMark->EndIndex = tagInfo.endIndex;
							NewMark->ParentId = parentID;
							NewMark->MarkerTagStruct = tagInfo;
					ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
							insertTopicStringInList(NewMark);
					ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
							vectorMarkerInfoPtr->push_back(NewMark);
						}
					}
					else if(CurrBoxTagList[p].elementId == -102){

						int32 childTagCount1 = childTagXMLElementPtr->GetChildCount();
						for(int32 childTagIndex = 0;childTagIndex < childTagCount1;childTagIndex++)
						{
							XMLReference childTagRef = childTagXMLElementPtr->GetNthChild(childTagIndex);
							InterfacePtr<IIDXMLElement>childTagXMLElementPtr1(childTagRef.Instantiate());
							if(childTagXMLElementPtr1 == nil)
								continue;						

							PMString strID = childTagXMLElementPtr1->GetAttributeValue(WideString("ID"));//CS4
							double ID = strID.GetAsNumber();									
							
							PMString strTypeID = childTagXMLElementPtr1->GetAttributeValue(WideString("typeId"));//Cs4
							double typeID = strTypeID.GetAsNumber();													
							
							PMString strIndex = childTagXMLElementPtr1->GetAttributeValue(WideString("index"));//CS4
							int32 index = strIndex.GetAsNumber();				

							PMString strImgFlag = childTagXMLElementPtr1->GetAttributeValue(WideString("imgFlag"));//Cs4
							int32 imgFlag = strImgFlag.GetAsNumber();								

							PMString strParentID = childTagXMLElementPtr1->GetAttributeValue(WideString("parentID"));//Cs4
							double parentID = strParentID.GetAsNumber();				
							
							PMString strParentTypeID = childTagXMLElementPtr1->GetAttributeValue(WideString("parentTypeID"));//Cs4
							double parentTypeID = strParentTypeID.GetAsNumber();				
							
							PMString strSectionID = childTagXMLElementPtr1->GetAttributeValue(WideString("sectionID"));//Cs4
							double sectionID = strSectionID.GetAsNumber();
							
							PMString strTableFlag = childTagXMLElementPtr1->GetAttributeValue(WideString("tableFlag"));//Cs4
							int32 tableFlag = strTableFlag.GetAsNumber();

							PMString strLanguageID = childTagXMLElementPtr1->GetAttributeValue(WideString("LanguageID"));//Cs4
							double languageID = strLanguageID.GetAsNumber();

							PMString strIsAutoResize = childTagXMLElementPtr1->GetAttributeValue(WideString("isAutoResize"));//Cs4
							int32 isAutoResize = strIsAutoResize.GetAsNumber();

							PMString strchildTag = childTagXMLElementPtr1->GetAttributeValue(WideString("childTag"));//Cs4
							int32 childTag = strchildTag.GetAsNumber();

							PMString strchildId = childTagXMLElementPtr1->GetAttributeValue(WideString("childId"));//Cs4
							double childId = strchildId.GetAsNumber();

							PMString strcatLevel = childTagXMLElementPtr1->GetAttributeValue(WideString("catLevel"));//Cs4
							int32 catLevel = strcatLevel.GetAsNumber();

							
							tagInfo.elementId = ID;
							childTagXMLElementPtr1->AddRef();//Added on 17-09-08
							tagInfo.tagPtr = childTagXMLElementPtr1;
							tagInfo.typeId = typeID;
							tagInfo.languageID = languageID;
							tagInfo.parentId = parentID;
							tagInfo.parentTypeID = parentTypeID;
							tagInfo.sectionID = sectionID;
							tagInfo.isAutoResize = isAutoResize;
							tagInfo.whichTab = index;
							tagInfo.imgFlag = imgFlag;
							tagInfo.isTablePresent = kFalse;
							tagInfo.childTag = childTag;
							tagInfo.childId = childId;
							tagInfo.catLevel = catLevel;
							
							TextIndex sIndex=0, eIndex=0;
							Utils<IXMLUtils>()->GetElementIndices(childTagXMLElementPtr1, &sIndex, &eIndex);
						
							tagInfo.startIndex=sIndex;
							tagInfo.endIndex=eIndex;


							if(ID == SelectedAttrID && (index == 3 || index == 4 || index == 5)  && parentID != -1)
							{	

								MarkerInfo* NewMark = new MarkerInfo;
								NewMark->BoxUIDRef = selectUIDList.GetRef(j);
								NewMark->StartIndex = tagInfo.startIndex;
								NewMark->EndIndex = tagInfo.endIndex;
								NewMark->ParentId = parentID;
								NewMark->MarkerTagStruct = tagInfo;
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: before inserting topic string");
								insertTopicStringInList(NewMark);
						ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after inserting topic string");
								vectorMarkerInfoPtr->push_back(NewMark);
							}
						}
					}				
					
				}

			}
		}
		
		if(CurrBoxTagList.size() > 0)
		{
			for(int32 tagIndex = 0 ; tagIndex < CurrBoxTagList.size() ; tagIndex++)
			{
				CurrBoxTagList[tagIndex].tagPtr->Release();
			}
		}
	
	}

	ptrIAppFramework->LogDebug("AP7_CatalogIndex:: after iterating boxes to find out the selected tag");
 }while(0);
 return vectorMarkerInfoPtr;
}





// added by prabhat for testing
bool16 SnpIndexTopics :: IsTextFrameOverset(const InterfacePtr<ITextFrameColumn> textFrameColumn)
{
	bool16 overset = kFalse;
	do {		
		// Check for damage & recompose if necessary
		InterfacePtr<IFrameList> frameList(textFrameColumn->QueryFrameList());
		if (frameList == nil) {
			ASSERT_FAIL("invalid frameList");
			break;
		}		
		int32 firstDamagedFrameIndex = frameList->GetFirstDamagedFrameIndex();
		UID frameUID = ::GetUID(textFrameColumn);
		int32 frameIndex = frameList->GetFrameIndex(frameUID);
		if (frameIndex < 0) {
			ASSERT_FAIL("invalid frameIndex");
			break;
		}
		if (firstDamagedFrameIndex <= frameIndex) 
		{
			// Frame not fully composed, force recomposition.
			InterfacePtr<IFrameListComposer> frameListComposer(frameList, UseDefaultIID());
			if (frameListComposer == nil) {
				ASSERT_FAIL("frameListComposer invalid");
				break;
			}
			frameListComposer->RecomposeThru/*RecomposeThruNthFrame*/(frameIndex);	//----CS5--
		}

		// Find the TextIndex of the final character in the story.
		InterfacePtr<ITextModel> textModel(textFrameColumn->QueryTextModel());
		if (textModel == nil) {
			ASSERT_FAIL("invalid textModel");
			break;
		}
		TextIndex storyFinalTextIndex = textModel->GetPrimaryStoryThreadSpan() - 1;
		
		// Find the TextIndex of the final character in the frame.
		TextIndex frameFinalTextIndex = 
			textFrameColumn->TextStart() + textFrameColumn->TextSpan() - 1;

		if (frameFinalTextIndex == storyFinalTextIndex) {
			// Frame displays the final character in the story.
			break;
		}

		if (frameFinalTextIndex == storyFinalTextIndex - 1) {
			// Frame does not display the terminating kTextChar_CR.
			// But don't consider this condition as overset.
			break;
		}

		// If we drop through to this point in the code
		// the frame is overset.
		overset = kTrue;



	} while (false); // only do once
	return overset;
}




void SnpIndexTopics::forceRecompose(InterfacePtr<ITextFrameColumn> textFrame)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return;
	}
	InterfacePtr<IFrameList> frameList(textFrame->QueryFrameList());
	if (frameList == NULL) 
	{
		ptrIAppFramework->LogError("AP7_DataSprayerModel::CDataSprayer::forceRecompose::!frameList");	
		return;
	}
	
	int32 firstDamagedFrameIndex = frameList->GetFirstDamagedFrameIndex();
	if (firstDamagedFrameIndex >= 0)
	{
		UID frameUID = ::GetUID(textFrame);
		int32 frameIndex = frameList->GetFrameIndex(frameUID);
		if (frameIndex < 0)
			return;
		
		InterfacePtr<IFrameListComposer> frameListComposer(frameList, UseDefaultIID());
		if (frameListComposer == NULL) 
		{
			ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::forceRecompose::frameListComposer == NULL");				
			return;
		}
		CA("Shrink The box");
		//frameListComposer->RecomposeThru/*RecomposeThruNthFrame*/(frameIndex);
		frameListComposer->RecomposeFully();
		CA("The box");
	}
}


// till here

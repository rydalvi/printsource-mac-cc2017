#include "VCPluginHeaders.h"
#include "PMString.h"
#include "SectionData.h"
#include "vector"

using namespace std;

vector<PubData> SectionData::sectionInfoVector;
vector<PubData> SectionData::subSectionInfoVector;

void SectionData::setSectionVectorInfo(double id,PMString name,int32 lvl, double rootID, double typeID){
	PubData publdata;
	publdata.setPubId(id);
	publdata.setPubName(name);
	publdata.setPubLvlNo(lvl);
	publdata.setroot_ID(rootID);
	publdata.settype_ID(typeID);
	sectionInfoVector.push_back(publdata);
}

PubData SectionData::getSectionVectorInfo(int32 index){
	return sectionInfoVector[index];
}

void SectionData::clearSectionVector(void){
	sectionInfoVector.erase(sectionInfoVector.begin(),sectionInfoVector.end());
	//sectionInfoVector.clear();
}

void SectionData::setSubsectionVectorInfo(double id,PMString name,int32 lvl, double rootID, double typeID){
	PubData publdata;
	publdata.setPubId(id);
	publdata.setPubName(name);
	publdata.setPubLvlNo(lvl);
	publdata.setroot_ID(rootID);
	publdata.settype_ID(typeID);
	subSectionInfoVector.push_back(publdata);
}

PubData SectionData::getSubsectionVectorInfo(int32 index){
	return subSectionInfoVector[index];
}

void SectionData::clearSubsectionVector(void){
	subSectionInfoVector.erase(subSectionInfoVector.begin(),subSectionInfoVector.end());
	//subSectionInfoVector.clear();
}

#ifndef __CMMPUBLICATIONNODE_H__
#define __CMMPUBLICATIONNODE_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"
#include "PublicationNode.h"

using namespace std;

/*-
class PublicationNode
{
private:
	PMString strPublicationName;//Product Name/ Item no (Unique key attribute for Item)
	int32 pubId;				//will contain either ProductID/Item ID
	int32 parentId;				
	int level;
	int32 seqNumber;
	int32 childCount;
	int hitCount;
	int32 referenceId;
	int32 typeId;				//will contain either Product Type id or Item Type ID
	int32 DesignerAction;
	int32 NewProduct;
	int32 PBObjectID;
	int32 isProduct;  // isProduct = 1 for Product , = 0 for Item, =2 for Hybrid Table
	bool16 isONEsource; // isONEsource = kTrue for ONEsource (Class)navigation. isONEsource = kFalse for Publication navogation.

	bool16 isStarred; // Used to show 'stared' staus of Product/Item in publication.

	int32 publicationID;
	int32 sectionID;
	int32 subSectionID;



public:

	PublicationNode():strPublicationName(""), pubId(0), level(0), childCount(0), hitCount(0)
		, seqNumber(0){}

	void setAll(PMString& pubName, int32 pubId,int32 pbobjectId, int32 parentId, int level,
		 int32 seqNumber, int32 childCount, int hitCount, int32 TypeID , int32 ReferenceId, int32 IsProduct, bool16 IsOneSource = kFalse)
	{
		this->strPublicationName=pubName;
		this->pubId=pubId;
		
		this->PBObjectID = pbobjectId;

		this->parentId=parentId;
		this->level=level;
		this->seqNumber=seqNumber;
		this->childCount=childCount;
		this->hitCount=hitCount;
		this->typeId =TypeID; 
		this->referenceId = ReferenceId;
		this->isProduct = IsProduct;
		this->isONEsource = IsOneSource;
		
	}

	int32 getReferenceId(void) { return this->referenceId; }
	void setReferenceId(int32 id) { this->referenceId=id; }
	int32 getTypeId(void) { return this->typeId; }
	void setTypeId(int32 id) { this->typeId=id; }
	

	int32 getSequence(void) { return this->seqNumber; }

	PMString getName(void) { return this->strPublicationName; }

	int32 getPubId(void) { return this->pubId; }
	
	int32 getParentId(void) { return this->parentId; }

	int getLevel(void) { return this->level; }

	int32 getChildCount(void) { return this->childCount; }

	int getHitCount(void) { return this->hitCount; }

	void setPubId(int32 pubId) { this->pubId=pubId; }

	void setParentId(int32 parentId) { this->parentId=parentId; }

	void setLevel(int level) { this->level=level; }

	void setChildCount(int32 childCount) { this->childCount=childCount; }

	void setPublicationName(PMString& pubName) { this->strPublicationName=pubName; }

	void setHitCount(int hitCount) { this->hitCount=hitCount; }

	void setSequence(int32 seqNumber){ this->seqNumber=seqNumber; }

	void setDesignerAction(int32 DesignerAction){ this->DesignerAction=DesignerAction; }

	int32 getDesignerAction(void) { return this->DesignerAction; }

	void setNewProduct(int32 NewProduct){this->NewProduct= NewProduct;}

	int32 getNewProduct(void) {return this->NewProduct; }	

	void setPBObjectID(int32 PBObjectID){this->PBObjectID= PBObjectID;}

	int32 getPBObjectID(void) {return this->PBObjectID; }

	void setIsProduct(int32 Flag){ this->isProduct = Flag;}

	int32 getIsProduct(void){ return this->isProduct;}

	void setIsONEsource(bool16 Flag){ this->isONEsource = Flag;}

	bool16 getIsONEsource(void){ return this->isONEsource;}

	void setIsStarred(bool16 Flag){ this->isStarred = Flag;}

	bool16 getIsStarred(void){ return this->isStarred;}

	void setPublicationID(int32 publication_id){ this->publicationID = publication_id;}

	int32 getPublicationID(void){ return this->publicationID;}

	void setSectionID(int32 section_id){ this->sectionID = section_id;}

	int32 getSectionID(void){ return this->sectionID;}

	void setSubSectionID(int32 subSection_id){ this->subSectionID = subSection_id;}

	int32 getSubSectionID(void){ return this->subSectionID;}
};
-*/

class CreateMediaPublicationNode
{
public:
	PMString strPublicationName;
	double pubId;
	double parentId;
	int level;
	int32 seqNumber;
	int32 childCount;
	int hitCount;
	double referenceId;
	double typeId;
	int32 DesignerAction;
	int32 NewProduct;
	double PBObjectID;
	int32 isProduct;

	
	

	int32 pageno;
	double subSectionID;
	double sectionID;
	double publicationID;
	PMString subSectionName;

	/*
		Constructor
		Initialise the members to proper values
	*/
	CreateMediaPublicationNode():strPublicationName(""), pubId(0), level(0), childCount(0), hitCount(0)
		, seqNumber(0),pageno(-1)
	{
		subSectionID=-1;
		sectionID = -1;
		publicationID = -1;
		subSectionName = "";
	}
	/*
		One time access to all the private members
	*/
	void setAll(PMString& pubName, double pubId, double parentId, int level,
		 int32 seqNumber, int32 childCount, int hitCount, double TypeID , double ReferenceId,int32 IsProduct)
	{
		this->strPublicationName=pubName;
		this->pubId=pubId;
		this->parentId=parentId;
		this->level=level;
		this->seqNumber=seqNumber;
		this->childCount=childCount;
		this->hitCount=hitCount;
		this->typeId =TypeID; 
		this->referenceId = ReferenceId;
		this->isProduct = IsProduct;
		
	}

	double getReferenceId(void) { return this->referenceId; }
	void setReferenceId(double id) { this->referenceId=id; }
	double getTypeId(void) { return this->typeId; }
	void setTypeId(double id) { this->typeId=id; }
	
	/*
		@returns the sequence of the child
	*/
	int32 getSequence(void) { return this->seqNumber; }
	/*
		@returns name of the publication
	*/
	PMString getName(void) { return this->strPublicationName; }
	/*
		@returns the id of the publication
	*/
	double getPubId(void) { return this->pubId; }
	/*
		@returns the parent id for that node
	*/
	double getParentId(void) { return this->parentId; }
	/*
		@returns the level (distance from the root) of the publication
	*/
	int getLevel(void) { return this->level; }
	/*
		@returns the number of child for that parent
	*/
	int32 getChildCount(void) { return this->childCount; }
	/*
		@returns the number of count the node was accessed
	*/
	int getHitCount(void) { return this->hitCount; }
	/*
		Sets the publication id for the publication
		@returns none
	*/
	void setPubId(double pubId) { this->pubId=pubId; }
	/*
		Sets the parent id for the publication
		@returns none
	*/
	void setParentId(double parentId) { this->parentId=parentId; }
	/*
		Sets the level for the publication
		@returns none
	*/
	void setLevel(int level) { this->level=level; }
	/*
		Sets the child count for the publication
		@returns none
	*/
	void setChildCount(int32 childCount) { this->childCount=childCount; }
	/*
		Sets the publication name for the publication
		@returns none
	*/
	void setPublicationName(PMString& pubName) { this->strPublicationName=pubName; }
	/*
		Sets the number of hits for the publication
		@returns none
	*/
	void setHitCount(int hitCount) { this->hitCount=hitCount; }
	/*
		Sets the sequence number for the child
		@returns none
	*/
	void setSequence(int32 seqNumber){ this->seqNumber=seqNumber; }

	void setDesignerAction(int32 DesignerAction){ this->DesignerAction=DesignerAction; }

	int32 getDesignerAction(void) { return this->DesignerAction; }

	void setNewProduct(int32 NewProduct){this->NewProduct= NewProduct;}

	int32 getNewProduct(void) {return this->NewProduct; }	

	void setPBObjectID(double PBObjectID){this->PBObjectID= PBObjectID;}

	double getPBObjectID(void) {return this->PBObjectID; }

	void setIsProduct(int32 isProductFlag){ this->isProduct = isProductFlag;}

	int32 getIsProduct(void){ return this->isProduct;}

	PublicationNode convertToPublicationNode()
	{
		 PublicationNode pNode;
		 pNode.setPublicationName(strPublicationName);
		 pNode.setPubId(pubId);
		 pNode.setParentId(parentId);
		 pNode.setLevel(level);
		 pNode.setSequence(seqNumber);
		 pNode.setChildCount(childCount);
		 pNode.setHitCount(hitCount);
		 pNode.setReferenceId(referenceId);
		 pNode.setTypeId(typeId);
		 pNode.setDesignerAction(DesignerAction);
		 pNode.setNewProduct(NewProduct);
		 pNode.setPBObjectID(PBObjectID);
		 pNode.setIsProduct(isProduct);
		 pNode.setIsONEsource(kFalse);
		 return pNode;

	}

	
};

typedef vector<PublicationNode>PublicationNodeList, *PublicationNodeListPtr;
typedef vector<CreateMediaPublicationNode>CreateMediaPublicationNodeList ;


#endif
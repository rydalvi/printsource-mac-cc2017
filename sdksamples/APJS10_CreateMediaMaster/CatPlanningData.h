#ifndef __CATPLANNINGDATA_H__
#define __CATPLANNINGDATA_H__

#include "VCPlugInHeaders.h"
#include "PMString.h"
#include "PublicationNode.h"
//-#include "AP_pub_comment.h"

class CatPlanningData
{
public:
//-	APpubComment apPubCommentObj;
	CreateMediaPublicationNode createMediaPublicationNodeObj;
	
};

typedef vector<CatPlanningData> CatPlanningDataList;


class ObjectUIDListData
{
public:
	PMString stencilname;
	UIDList uidListObj;
	
};

typedef vector<ObjectUIDListData> ObjUIDListDataList;
#endif 
#include "VCPlugInHeaders.h"
#include "CMMPlugInEntrypoint.h"
#include "CMMID.h"


#include "CAlert.h"
void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line);

#define FILENAME			PMString("CMMPluginEntryPoint.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

#ifdef WINDOWS
	ITypeLib* CMMPlugInEntrypoint::fSPTypeLib = nil;
#endif

bool16 CMMPlugInEntrypoint :: Unload()
{
	//CA("unloading");
	return kTrue;
}

/* Global
*/
static CMMPlugInEntrypoint gPlugIn;

/* GetPlugIn
	The application calls this function when the plug-in is installed 
	or loaded. This function is called by name, so it must be called 
	GetPlugIn, and defined as C linkage. See GetPlugIn.h.
*/
IPlugIn* GetPlugIn()
{
	return &gPlugIn;
}

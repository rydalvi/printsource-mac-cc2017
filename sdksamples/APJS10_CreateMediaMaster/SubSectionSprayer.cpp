#include "VCPluginHeaders.h"

//#ifdef __donotincludethisfile__

//#error you should not include this file

#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "SPID.h"

//for keeping the spread together
#include "ISpreadList.h"
//end for


#include "ISpread.h"
#include "IMargins.h"
#include "ISelectUtils.h"
#include "IClipboardController.h"
#include "IScrapSuite.h"
#include "ISelectionUtils.h"
#include "ILayoutAction.h"
#include "ProgressBar.h"
#include "SSSID.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "ISubSectionSprayer.h"
#include "MediatorClass.h"
#include "ILayoutSelectionSuite.h"
#include "DSFID.h"
#include "IDataSprayer.h"
#include "ITagReader.h"
#include "ISelectionManager.h"
#include "ITextMiscellanySuite.h"
#include "vector"
#include "IDocument.h"
#include "ILayoutControlData.h"
//#include "LayoutUIUtils.h"//Cs3 Deprictaed
#include "ILayoutUIUtils.h" //Cs4
#include "SectionData.h"
#include "TableStyleUtils.h"
#include "IScrapItem.h"
#include "TagStruct.h"
#include "SubSectionSprayer.h"
#include "IFrameContentUtils.h"
#include "IGeometry.h"
#include "TransformTypes.h"
#include "ITransformFacade.h"
#include "CatPlanningData.h"  //7-may
//-#include "AP_pub_comment.h"
#include "PMRect.h"
//delete page
#include "IPageList.h"
#include "TransformUtils.h"
#include "PMPoint.h"
#include "AcquireModalCursor.h"
#include "IPageItemTypeUtils.h"
#include "IBoolData.h"
#include "IXMLAttributeCommands.h"
#include "IColumns.h"
#include "IMasterPage.h"
#include "IMasterSpreadList.h"
#include "IPageSetupPrefs.h"
#include "IPageCmdData.h"
#include "PreferenceUtils.h"


//void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line); 
#define FILENAME			PMString("SubSectionSprayer.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA(X) CAlert::InformationAlert(X)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x, y)	{PMString str; str.Append(x ); str.Append(" : ");str.AppendNumber(y);CA(str);}

//9May extern PublicationNodeList pNodeDataList;
extern CreateMediaPublicationNodeList pNodeDataList;
extern double CurrentSelectedSection;
extern double CurrentSelectedPublicationID;
extern double CurrentSelectedSubSection;
extern int32 global_project_level ;
extern double global_lang_id ;
extern int32 currentSelectedSpreadCount; //chetan
/// Global Pointers
extern ITagReader* itagReader;
extern IAppFramework* ptrIAppFramework;
//-extern ISpecialChar* iConverter;
extern IClientOptions* ptrIClientOptions;
extern IDataSprayer* DataSprayerPtr;
///////////

//define in CMMDialogObserver.cpp
extern void fillpNodeDataList(double subSectionID,double sectionID,double publicationID,PMString subSectionName);//defined in CMMDialogObserver.cpp
extern void fillpNodeDataListForWhite();
extern 	int no_of_lstboxElements;
extern CatPlanningDataList CPDataList ; //7-may

//9May PublicationNodeList CurrentSectionpNodeDataList;
CreateMediaPublicationNodeList CurrentSectionpNodeDataList;
CreateMediaPublicationNodeList CurrentSectionpNodeDataListForCatalogPlanning;  //2-may
//-vector<APpubComment> apPubCommentsBySpreadList;  //8-may
typedef vector<UID> PageUIDList;
PageUIDList pageUidList;	
int32 PageCount;

//11May
int32 pageNoFromDb = -1;
vectorBoxBounds OriginalBoxBoundVector;
//bool16 deleteStartPageOfTheDocument(UIDRef & startPageUIDRef);//A 1/4 /08

UIDList productSelUIDList;
UIDList itemSelUIDList;
UIDList hybridTableSelUIDList;
UIDList sectionSelUIDList;

bool16 isProdStencil = kFalse;
bool16 isItemStencil = kFalse;
bool16 isHybridTableStencil = kFalse;
bool16 isAllStencil = kFalse;
bool16 isSectionStencil = kFalse;

vectorBoxBounds ProdStencilBoxBoundVector;
vectorBoxBounds ItemStencilBoxBoundVector;
vectorBoxBounds HybridTableStencilBoxBoundVector;
vectorBoxBounds SectionStencilBoxBoundVector;

PMRect ProdStencilMaxBounds;
PMRect ItemStencilMaxBounds;
PMRect HybridTableStencilMaxBounds;
PMRect SectionStencilMaxBounds;

bool16 StncilOverlengthFlag = kFalse;
int32 OverlengthHorizCount = 0;
PMRect OverLengthoffsetWidth ;

bool16 SingleItemSprayReturnFlag = kFalse;
char AlphabetArray[] = "abcdefghijklmnopqrstuvwxyz";

vectorCSprayStencilInfo CSprayStencilInfoVector;


//////////////////////////		Amit
bool16 allProductSprayed = kFalse;
int32 horizontalCount = 0;
int32 verticalCount = 0;
FrameBoundsList ProdBlockBoundList;

extern UIDList sectionStencilUIDListToDelete;	//This is used to delete the section stencil When it is spray two times unnecessary
//////////////////////////		End
bool16 doesExist(TagList &tagList,const UIDList &selectUIDList);

bool16 isSpreadBasedLetterKeys = kFalse;

PMRect origMaxBoxBounds1 = kZeroRect;

int32 isSprayItemPerFrameFlag1 = 0;
bool16 isItemHorizontalFlow1 = kFalse;
int32 setoverflowflag = 0;
int32 idxVerticalCount = 0;
int32 idxHorizontalCount = 0;
PMReal RightOfPreviousColumn = 0.0;

bool16 isColumnChangeForSpraying = kFalse;
bool16 isAutoResized_FrameDeleted = kFalse;
bool16 newPageAdded = kFalse;
PMRect firstItemFramesStencilBoxBounds = kZeroRect;
int32 numProductsSprayed = 0;

bool16 isAddPageForLeadingItem_FrameDeletedAfterAutoResizeCase = kFalse;
bool16 isAddPageForLeadingItem1_NoSpaceForFrameAtBottomOfPageBeforespray = kFalse;
bool16 ishorizontalSpaceAvailable = kFalse;
bool16 isFrameDeleted = kFalse;

bool16 addPageSplCase = kFalse;
//int32 addPageSplCase_pageIndex = 0;
int32 addPageSplCase_SpreadIndex = 0;
int32 addPageSplCase_pageIndexPerSpread = 0;
UID addPageSplCase_MasterSpreadUIDOfTemplate;

PMRect addPageSplCase_marginBBox;
PMRealList addPageSplCase_columns;
int32 CCC = 0;
int32 FrameDeletedCount = 0;
PMReal RightMarkAfterSprayForLeadingItem = 0.0;
int32 toAvoidOverlappingOfNextFrame =0;

UIDList AllFramesOfCurrentLeadingItemIncludingItsChildItemFrames;
vectorBoxBounds AllFramesOfCurrentLeadingItemIncludingItsChildItemFramesBoxBounds;
bool16 fFaltuCase = kFalse;
// added by avinash on 7th sep
int32 counter = 0;
bool16 fSetTopForThisCase = kFalse;
//upto here


void SubSectionSprayer::startSpraying(void)
{
    //CA("SubSectionSprayer::startSpraying");
    
    // Added by Sunil FROM HERE
    PMReal left_latest = origMaxBoxBounds1.Left();
    PMReal top_latest = origMaxBoxBounds1.Bottom();
    bool16 newPageToBeAdded = kFalse;
    
    PMReal Temp_Right2 = 0.0;
    // Added by Sunil TILL HERE
    
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == NULL)
    {
        //CA("ptrIAppFramework == NULL");
        return ;
    }
    
    vector<double> tempIdList;
    tempIdList.clear();
    SingleItemSprayReturnFlag = kFalse;
    
    //PFTreeDataCache treeCache;
    int32 numProducts;
    
    
    bool16 result = this->getAllIdForLevel(numProducts, tempIdList);
    if(result == kFalse)
    {
        ptrIAppFramework->LogError("ProductFinder::SubSectionSprayer::startSpraying:getAllIdForLevel's result == kFalse");
        return;
    }
    this->allPFIdList = tempIdList;
    
    double selectedID = allPFIdList[sprayedProductIndex];
    
    
    /*PMString ASD(" selectedID : ");
     ASD.AppendNumber(selectedID);
     CA(ASD);*/
    InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
    if(!DataSprayerPtr)
    {
        ptrIAppFramework->LogDebug("ProductFinder::SubSectionSprayer::startSpraying:Pointer to DataSprayerPtr not found");
        return;
    }
    
    InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
    if(iSSSprayer==nil)
    {
        CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
        return;
    }
    
    PMReal VerticalBoxSpacing = iSSSprayer->getVerticalBoxSpacing();
    PMReal HorizBoxSpacing = iSSSprayer->getHorizontalBoxSpacing();
    
    //ProductSpray PrSpryObj;
    isSprayItemPerFrameFlag1 = checkIsSprayItemPerFrameTag(selectUIDList , isItemHorizontalFlow1);
LABEL:
    if(setoverflowflag == 1)
    {
        //CA("setoverflowflag == 1");
        isSprayItemPerFrameFlag1 = 1;
        setoverflowflag = 0;
        DataSprayerPtr->setItemSprayOverflowFlag(2);
        //isItemHorizontalFlow1 = kTrue;
    }
    //isItemHorizontalFlow1 = kTrue;
    if(isSprayItemPerFrameFlag1 > 0 )  // 1 or 2
    {
        DataSprayerPtr->setItemSprayOverflowFlag(3);  //it is neccesory in image spraying in ItemIDInfo loop by sagar
        
        //CA("isSprayItemPerFrameFlag1");
        if(isItemHorizontalFlow1)
        {
            //CA("isItemHorizontalFlow1");
            DataSprayerPtr->setFlow(kTrue);
        }
        else
        {
            //CA("isItemHorizontalFlow1 == kFalse");
            DataSprayerPtr->setFlow(kFalse);
        }
    }
    else if(isSprayItemPerFrameFlag1 == 0)
    {
        if(iSSSprayer->getHorizontalFlowForAllImageSprayFlag())
            DataSprayerPtr->setFlow(kTrue);
        else
            DataSprayerPtr->setFlow(kFalse);
    }
    //	DataSprayerPtr->setFlow(iSSSprayer->getHorizontalFlowForAllImageSprayFlag());
    //	DataSprayerPtr->setFlow(isItemHorizontalFlow);
    
    DataSprayerPtr->getAllIds(selectedID);//For these PF, PR, PG ITEM ID's we have to spray the data
    
    //TagReader tReader;
    TagList tagList;
    
    InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
    if(ptrIClientOptions==nil)
    {
        ptrIAppFramework->LogDebug("ProductFinder::SubSectionSprayer::startSpraying:Interface for IClientOptions not found.");
        return;
    }
    
    InterfacePtr<ITagReader> itagReader
    ((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
    if(!itagReader)
    {
        ptrIAppFramework->LogDebug("ProductFinder::SubSectionSprayer::startSpraying:iTagReader == nil");
        return ;
    }
    
    /*	imagePath=ptrIClientOptions->getImageDownloadPath();
     if(imagePath!="")
     {
     char *imageP=imagePath.GetPlatformString().c_str();
     if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
     #ifdef MACINTOSH
     imagePath+=":";
     #else
     imagePath+="\\";
     #endif
     }
     */
    int32 listLength = selectUIDList.Length();
    /*PMString temp = "";
     temp.AppendNumber(listLength);
     CA("listLength = " +temp);*/
    //Initialisation done. Spray Data
    
    //// New functionality added for Indaba for spraying Products items in their Individual Stencils.
    //CA("Navin Functionality");
    
    
    //if(isSprayItemPerFrameFlag)
    bool16 isOriginalFrameSprayedInSprayItemPerFrame = kFalse;
    if(isSprayItemPerFrameFlag1 > 0)
    {
        //CA("Inside isSprayItemPerFrameFlag case");
        int32 field1_val = -1;
        
        vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
        PMRect CopiedItemMaxBoxBoundsBforeSpray;
        UIDList ItemFrameUIDList(selectUIDList.GetDataBase());
        //CA("1");
        result = kFalse;
        result = this->getMaxLimitsOfBoxes(selectUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);
        TagStruct outputAsSwatchImageTag;
        
        for(int i=0; i<listLength; i++)
        {
            tagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));
            if(tagList.size()<=0)//This can be a Tagged Frame
            {
                //CA(" tagList.size()<=0 ");
                if(DataSprayerPtr->isFrameTagged(selectUIDList.GetRef(i)))
                {
                    tagList.clear();
                    tagList=itagReader->getFrameTags(selectUIDList.GetRef(i));
                    if(tagList.size()==0)//Ordinary box
                    {
                        continue ;
                    }
                    
                    for(int32 j=0; j <tagList.size(); j++)
                    {
                        if((tagList[j].whichTab == 4 && tagList[j].isSprayItemPerFrame != -1 )
                           || (isSprayItemPerFrameFlag1 == 2 && tagList[j].imgFlag == 1 && tagList[j].elementId != -1) /* To identify OutputAsSwatch Case. */
                           )
                        {
                            //CA("Item Tag Found");
                            ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
                            
                            if(tagList[j].isSprayItemPerFrame == 3)
                            {
                                outputAsSwatchImageTag = tagList[j];
                            }
                            
                            if(tagList[j].imgFlag != 1)
                                field1_val = tagList[j].field1;
                            break; // break out from for loop
                        }
                    }
                    
                    
                }
                else
                {
                    // This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
                    //CA(" else DataSprayerPtr->isFrameTagged");
                    InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
                    if(!iHier)
                    {
                        //CA(" !iHier >> Continue ");
                        continue;
                    }
                    UID kidUID;
                    int32 numKids=iHier->GetChildCount();
                    IIDXMLElement* ptr = NULL;
                    
                    for(int j=0;j<numKids;j++)
                    {
                        //CA("Inside For Loop");
                        bool16 Flag12 =  kFalse;
                        kidUID=iHier->GetChildUID(j);
                        UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
                        TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
                        if(NewList.size()<=0)//This can be a Tagged Frame
                        {
                            NewList.clear();
                            NewList=itagReader->getFrameTags(selectUIDList.GetRef(i));
                            if(NewList.size()==0)//Ordinary box
                            {
                                continue ;
                            }
                            
                            for(int32 j=0; j <NewList.size(); j++)
                            {
                                if((NewList[j].whichTab == 4 && tagList[j].isSprayItemPerFrame != -1)
                                   || (isSprayItemPerFrameFlag1 == 2 && tagList[j].imgFlag == 1 && tagList[j].elementId != -1))
                                {
                                    //CA("Item Tag Found");
                                    ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
                                    
                                    if(tagList[j].isSprayItemPerFrame == 3)
                                    {
                                        outputAsSwatchImageTag = tagList[j];
                                    }
                                    
                                    Flag12 = kTrue;
                                    if(tagList[j].imgFlag != 1)
                                        field1_val = tagList[j].field1;
                                    break; // break out from for loop
                                }
                            }
                        }
                        else
                        {
                            for(int32 j=0; j <NewList.size(); j++)
                            {
                                if((NewList[j].whichTab == 4 && tagList[j].isSprayItemPerFrame != -1)
                                   || (isSprayItemPerFrameFlag1 == 2 && tagList[j].imgFlag == 1 && tagList[j].elementId != -1))
                                {
                                    //CA("Item Tag Found");
                                    ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
                                    if(tagList[j].isSprayItemPerFrame == 3)
                                    {
                                        outputAsSwatchImageTag = tagList[j];
                                    }
                                    Flag12 = kTrue;
                                    
                                    if(tagList[j].imgFlag != 1)
                                        field1_val = tagList[j].field1;
                                    break; // break out from for loop
                                }
                            }
                        }
                        //-------lalit-----
                        for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
                        {
                            NewList[tagIndex].tagPtr->Release();
                        }
                        
                        if(Flag12)
                            break;
                    }
                }
            }
            else
            {
                for(int32 j=0; j <tagList.size(); j++)
                {
                    if((tagList[j].whichTab == 4 && (tagList[j].isSprayItemPerFrame == 1 || tagList[j].isSprayItemPerFrame == 2))
                       //|| (tagList[j].whichTab == 4 && isSprayItemPerFrameFlag1 == 1)
                       || (isSprayItemPerFrameFlag1 == 2 && tagList[j].imgFlag == 1 && tagList[j].elementId != -1)
                       )
                    {
                        //CA("Item Tag Found");
                        ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
                        if(tagList[j].isSprayItemPerFrame == 3)
                        {
                            outputAsSwatchImageTag = tagList[j];
                        }
                        
                        if(tagList[j].imgFlag != 1)
                            field1_val = tagList[j].field1;
                        break; // break out from for loop
                    }
                }
            }
            //Apsiva9
            for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
            {
                tagList[tagIndex].tagPtr->Release();
            }
        }
        
        int32 ItemFrameUIDListSize = ItemFrameUIDList.Length();
        if(ItemFrameUIDListSize == 0)
        {
            //CA("ItemFrameUIDListSize == 0");
            SingleItemSprayReturnFlag = kTrue;
        }
        /*PMString ASD("Length of ItemFrameUIDList : ");
         ASD.AppendNumber(ItemFrameUIDListSize);
         CA(ASD);*/
        
        DataSprayerPtr->resetLetterOrNumberkeySeqCount(1);
        int AlphabetArrayCount =0;
        if((pNodeDataList[sprayedProductIndex].getIsProduct() == 1)  || (pNodeDataList[sprayedProductIndex].getIsProduct() == 0))
        {
            //CA("Only Product Spray");
            VectorLongIntPtr ItemIDInfo = NULL;
            vector<double> FinalItemIds;
            bool16 DOWhileLoopBreakFlag = kFalse;
            do
            {
                DOWhileLoopBreakFlag = kFalse;
                //if(pNodeDataList[sprayedProductIndex].getIsONEsource())
                //{
                    //If ONEsource mode  is selected and Table stencils for item is selected.Then to get all information of table.
                    //ItemIDInfo=ptrIAppFramework->GETProduct_getAllItemIDsFromTables(pNodeDataList[sprayedProductIndex].getPubId());
                //}
                //else
                {
                    //If publication mode is selected.
                    /*PMString ASD("ObjectID : ");
                     ASD.AppendNumber(pNodeDataList[CurrentSelectedProductRow].getPubId());
                     ASD.Append("  CurrentSectionID :  " );
                     ASD.AppendNumber(CurrentSelectedSubSection);
                     CA(ASD);*/
                    //ItemIDInfo=ptrIAppFramework->GETProjectProduct_getAllItemIDsFromTables(pNodeDataList[sprayedProductIndex].getPubId(), CurrentSelectedSubSection);
                    
                    if(pNodeDataList[sprayedProductIndex].getIsProduct() == 1 && isSprayItemPerFrameFlag1 == 1)
                    {
                        if(field1_val == -1)
                        {
                            ItemIDInfo=ptrIAppFramework->GETProjectProduct_getAllItemIDsFromTables(pNodeDataList[sprayedProductIndex].getPubId(), CurrentSelectedSubSection);
                        }
                        else
                        {
                            VectorScreenTableInfoPtr tableInfo = NULL;
                            tableInfo =ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(CurrentSelectedSubSection, pNodeDataList[sprayedProductIndex].getPubId(), kTrue);
                            if(!tableInfo)
                            {
                                ptrIAppFramework->LogDebug("AP7_DataSprayerModel::CDataSprayer::arrangeForSprayingProductItemWithOtherCopyAttributes::tableInfo is NULL");
                                //ItemAbsentinProductFlag = kTrue;
                                break;
                            }
                            
                            vector<double>::iterator itrID;
                            
                            vector<double> vec_items;
                            CItemTableValue oTableSourceValue;
                            VectorScreenTableInfoValue::iterator itr;
                            for(itr = tableInfo->begin(); itr!=tableInfo->end(); itr++)
                            {
                                oTableSourceValue = *itr;
                                
                                double table_Type_ID = oTableSourceValue.getTableTypeID();
                                if(table_Type_ID != field1_val)
                                    continue;
                                
                                vec_items = oTableSourceValue.getItemIds();
                                if(FinalItemIds.size() == 0)
                                {
                                    FinalItemIds = vec_items;
                                    /*for(int32 index = 0 ; index < FinalItemIds.size() ; index++)
                                     {
                                     vecTableID.push_back(table_ID);
                                     vecTableTypeID.push_back(table_Type_ID);
                                     }*/
                                }
                                else
                                {
                                    for(int32 i=0; i<vec_items.size(); i++)
                                    {
                                        bool16 Flag = kFalse;
                                        for(int32 j=0; j<FinalItemIds.size(); j++)
                                        {
                                            if(vec_items[i] == FinalItemIds[j])
                                            {
                                                Flag = kTrue;
                                                break;
                                            }
                                        }
                                        if(!Flag )
                                        {
                                            FinalItemIds.push_back(vec_items[i]);
                                        }
                                    }
                                }
                            }
                            
                            if(tableInfo)
                                delete tableInfo;
                            
                            ItemIDInfo = &FinalItemIds;
                        }
                    }
                    else if(pNodeDataList[sprayedProductIndex].getIsProduct() == 0 && isSprayItemPerFrameFlag1 == 1)
                    {
                        do
                        {
                            VectorScreenTableInfoPtr tableInfo=
                            ptrIAppFramework->GETProjectProduct_getItemTablesByPubObjectId(pNodeDataList[sprayedProductIndex].getPubId(), CurrentSelectedSection, global_lang_id );
                            if(!tableInfo)
                            {
                                ptrIAppFramework->LogDebug("SubSectionSprayer::startSpraying::GETProjectProduct_getItemTablesByPubObjectId's !tableInfo");
                                break;
                            }
                            if(tableInfo->size()==0)
                            {
                                ptrIAppFramework->LogInfo("SubSectionSprayer::startSpraying: table size = 0");
                                break;
                            }
                            CItemTableValue oTableValue;
                            VectorScreenTableInfoValue::iterator it;
                            
                            bool16 typeidFound=kFalse;
                            vector<double> vec_items;
                            
                            
                            for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
                            {//for tabelInfo start
                                oTableValue = *it;
                                vec_items = oTableValue.getItemIds();
                                
                                if(field1_val == -1)
                                {
                                    if(FinalItemIds.size() == 0)
                                    {
                                        FinalItemIds = vec_items;
                                    }
                                    else
                                    {
                                        for(int32 i=0; i<vec_items.size(); i++)
                                        {	bool16 Flag = kFalse;
                                            for(int32 j=0; j<FinalItemIds.size(); j++)
                                            {
                                                if(vec_items[i] == FinalItemIds[j])
                                                {
                                                    Flag = kTrue;
                                                    break;
                                                }
                                            }
                                            if(!Flag)
                                                FinalItemIds.push_back(vec_items[i]);
                                        }
                                    }
                                }
                                else
                                {
                                    if(field1_val != oTableValue.getTableTypeID())
                                        continue;
                                    
                                    if(FinalItemIds.size() == 0)
                                    {
                                        FinalItemIds = vec_items;
                                    }
                                    else
                                    {
                                        for(int32 i=0; i<vec_items.size(); i++)
                                        {	bool16 Flag = kFalse;
                                            for(int32 j=0; j<FinalItemIds.size(); j++)
                                            {
                                                if(vec_items[i] == FinalItemIds[j])
                                                {
                                                    Flag = kTrue;
                                                    break;
                                                }
                                            }
                                            if(!Flag)
                                                FinalItemIds.push_back(vec_items[i]);
                                        }
                                    }
                                }
                            }//for tabelInfo end
                            if(tableInfo)
                                delete tableInfo;
                            
                        }while(kFalse);
                        ItemIDInfo = &FinalItemIds;
                    }
                    else if( isSprayItemPerFrameFlag1 == 2 ) // for outputAsSwatch fill itemIdInfo with mpv_value_id.
                    {
                        VectorAssetValuePtr AssetValuePtrObj = NULL;
                        AssetValuePtrObj = ptrIAppFramework->GETAssets_GetPVMPVAssetByParentIdAndAttributeId(pNodeDataList[sprayedProductIndex].getPubId(), outputAsSwatchImageTag.elementId, CurrentSelectedSection, global_lang_id, pNodeDataList[sprayedProductIndex].getIsProduct(), outputAsSwatchImageTag.typeId, outputAsSwatchImageTag.imageIndex);
                        if(AssetValuePtrObj == NULL){
                            ptrIAppFramework->LogDebug("SubSectionSprayer::startSpraying::GETAssets_GetPVMPVAssetByParentIdAndAttributeId AssetValuePtrObj == NULL");
                            break;
                        }
                        
                        VectorAssetValue::iterator it1; // iterator of Asset value
                        CAssetValue objCAssetvalue;
                        
                        for(it1 = AssetValuePtrObj->begin();it1 != AssetValuePtrObj->end();it1++)
                        {
                            //CA("For Loop");
                            objCAssetvalue = *it1;
                            PMString fileName("");
                            double mpv_value_id = -1;
                            
                            fileName = objCAssetvalue.geturl();
                            mpv_value_id = objCAssetvalue.getMpv_value_id();
                            
                            //pickListValue = objCAssetvalue.getPickListValue();
                            //description = objCAssetvalue.getDescription();
                            if(fileName=="")
                                continue;
                            
                            FinalItemIds.push_back(mpv_value_id);
                            
                        }
                        ItemIDInfo = &FinalItemIds;
                        
                        if(AssetValuePtrObj)
                        {
                            AssetValuePtrObj->clear();
                            delete AssetValuePtrObj;
                        }
                        
                    }
                    
                    
                }
                
                
                if(!ItemIDInfo)
                {
                    SingleItemSprayReturnFlag = kTrue;
                    //CA("AP7_DataSprayerModel::CDataSprayer::sprayForThisBox::case 1:ItemIDInfo is NULL");
                    ptrIAppFramework->LogDebug("SubSectionSprayer::startSpraying::sprayForThisBox::case 1:ItemIDInfo is NULL");
                    break;
                }
                
                if(ItemIDInfo->size()==0)
                {
                    SingleItemSprayReturnFlag = kTrue;
                    //CA("ItemIDInfo->size()==0");
                    ptrIAppFramework->LogInfo("SubSectionSprayer::startSpraying::sprayForThisBox::case 1:ItemIDInfo->size()==0");
                    break;
                }
                
                UIDRef originalPageUIDRef, originalSpreadUIDRef;
                result = this->getCurrentPage(originalPageUIDRef, originalSpreadUIDRef);
                if(result == kFalse){
                    SingleItemSprayReturnFlag = kTrue;
                    //CA("AP7_ProductFinder::SubSectionSprayer::startSprayingSubSection::!getCurrentPage");
                    ptrIAppFramework->LogDebug("SubSectionSprayer::startSpraying::startSprayingSubSection::!getCurrentPage");
                    break;
                }
                
                
                
                PMRect PagemarginBoxBounds;
                result = getMarginBounds(originalPageUIDRef, PagemarginBoxBounds);
                if(result == kFalse)
                {
                    result = getPageBounds(originalPageUIDRef, PagemarginBoxBounds);
                    if(result == kFalse)
                    {
                        SingleItemSprayReturnFlag = kTrue;
                        ///CA("result == kFalse");
                        break;
                    }
                }
                
                PMRect ItemFramesStencilMaxBounds = kZeroRect;
                
                PMReal Max_Template_Width = ItemStencilMaxBounds.Right() - ItemStencilMaxBounds.Left();
                
                vectorBoxBounds ItemFramesBoxBoundVector;
                
                result = getMaxLimitsOfBoxes(ItemFrameUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                if(result == kFalse){
                    SingleItemSprayReturnFlag = kTrue;
                    //CA("getMaxLimitsOfBoxes result == kFalse");
                    break;
                }
                
                PMReal VerticalBoxSpacing = iSSSprayer->getVerticalBoxSpacing();
                PMReal HorizBoxSpacing = iSSSprayer->getHorizontalBoxSpacing();
                
                
                
                PMReal LeftMark = (ItemFramesStencilMaxBounds.Left());
                PMReal BottomMark = (ItemFramesStencilMaxBounds.Bottom());
                PMReal TopMarkOfFrame = (ItemFramesStencilMaxBounds.Top());
                PMReal TopMark = (PagemarginBoxBounds.Top());
                PMReal RightMark = (ItemFramesStencilMaxBounds.Right());
                
                if(LeftMark > PagemarginBoxBounds.Right())
                {
                    //CA("LeftMark > PagemarginBoxBounds.Right()........................");
                    InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
                    if (layoutData == nil)
                        break;
                    
                    IDocument* document = layoutData->GetDocument();
                    if (document == NULL)
                        break;
                    
                    IDataBase* database = ::GetDataBase(document);
                    if(!database)
                        break;
                    //CA("Got wrong Page UID");
                    /*IGeometry* spreadItem = layoutData->GetSpread();
                     if(spreadItem == nil)
                     break;*/
                    UIDRef ref = layoutData->GetSpreadRef();
                    
                    InterfacePtr<ISpread> iSpread(ref, UseDefaultIID());
                    if (iSpread == nil)
                        break;
                    
                    int numPages=iSpread->GetNumPages();
                    
                    UID pageUID= iSpread->GetNthPageUID(numPages-1);
                    
                    UIDRef pageRef(database, pageUID);
                    getPageBounds (pageRef, PagemarginBoxBounds);
                }
                
                InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
                InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
                if (!layoutSelectionSuite) {
                    SingleItemSprayReturnFlag = kTrue;
                    //CA("AP7_ProductFinder::SubSectionSprayer::sprayPageWithResizableFrameNew::!layoutSelectionSuite");
                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::sprayPageWithResizableFrameNew::!layoutSelectionSuite");
                    break;
                }
                selectionManager->DeselectAll(nil); // deselect every active CSB
                
                int32 ItemFrameUIDListSize = ItemFrameUIDList.Length();
                /*PMString ASD("Length of ItemFrameUIDList : ");
                 ASD.AppendNumber(ItemFrameUIDListSize);
                 CA(ASD);*/
                
                //CA(" Before Copy Main");
                //layoutSelectionSuite->Select(ItemFrameUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	  //Commented By Sachin sharma on 2/07/07
                layoutSelectionSuite->SelectPageItems(ItemFrameUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
                
                //copy the selected items
                CopySelectedItems();
                
                //CA(" After Copy Main");
                //now get the copied item list
                UIDList FirstcopiedBoxUIDList;
                result = getSelectedBoxIds(FirstcopiedBoxUIDList);
                if(result == kFalse)
                    break;
                
                //CA("before Main");
                PBPMPoint moveToPoints1(PagemarginBoxBounds.Left(), PagemarginBoxBounds.Top());
                moveBoxes(FirstcopiedBoxUIDList, moveToPoints1);
                
                //CA("AfterMove Main");
                
                // For First Item of Product
                VectorLongIntValue::iterator it1;
                it1 = ItemIDInfo->begin();
                double FirstItemId = *it1;
                it1++;
                AlphabetArrayCount =0;
                
                for(int i=0; i<ItemFrameUIDListSize; i++)
                {
                    tagList=itagReader->getTagsFromBox(ItemFrameUIDList.GetRef(i));
                    if(tagList.size()<=0)//This can be a Tagged Frame
                    {
                        //CA(" tagList.size()<=0 ");
                        if(DataSprayerPtr->isFrameTagged(ItemFrameUIDList.GetRef(i)))
                        {
                            tagList.clear();
                            tagList=itagReader->getFrameTags(ItemFrameUIDList.GetRef(i));
                            if(tagList.size()==0)//Ordinary box
                            {
                                continue ;
                            }
                            
                            for(int32 j=0; j <tagList.size(); j++)
                            {
                                if((tagList[j].whichTab == 4) || (isSprayItemPerFrameFlag1 == 2) )
                                {
                                    ////CA("Item Tag Found");
                                    //ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
                                    //break; // break out from for loop
                                    XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
                                    if(tagList[j].imgFlag == 1)
                                    {
                                        PMString attributeValue;
                                        attributeValue.AppendNumber(PMReal(FirstItemId));
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
                                        
                                        attributeValue.clear();
                                        if(isSprayItemPerFrameFlag1 != 2)
                                        {
                                            attributeValue.AppendNumber(PMReal(pNodeDataList[sprayedProductIndex].getPubId()));
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue));
                                        }
                                    }
                                    else if(isSprayItemPerFrameFlag1 == 2)
                                    {
                                        PMString attributeValue;
                                        attributeValue.AppendNumber(PMReal(FirstItemId));
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue));
                                        
                                    }
                                    else
                                    {
                                        PMString attributeValue;
                                        attributeValue.AppendNumber(PMReal(FirstItemId));
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childId"),WideString(attributeValue));
                                        
                                        PMString childTag("1");
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childTag"),WideString(childTag));
                                        
                                        if(tagList[j].elementId == -803)
                                        {
                                            attributeValue.Clear();
                                            if(AlphabetArrayCount < 26)
                                                attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
                                            else
                                                attributeValue.Append("a");
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                        }
                                        else if(tagList[j].elementId == -827)
                                        {
                                            attributeValue.Clear();
                                            int32 numberKey = AlphabetArrayCount + 1;
                                            
                                            attributeValue.AppendNumber(numberKey);
                                            
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                        }
                                    }
                                    PMString attributeValue;
                                    attributeValue.AppendNumber(-777);
                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
                                }
                            }
                            
                        }
                        else
                        {
                            // This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
                            //CA(" else DataSprayerPtr->isFrameTagged");
                            InterfacePtr<IHierarchy> iHier(ItemFrameUIDList.GetRef(i), UseDefaultIID());
                            if(!iHier)
                            {
                                //CA(" !iHier >> Continue ");
                                continue;
                            }
                            UID kidUID;
                            int32 numKids=iHier->GetChildCount();
                            IIDXMLElement* ptr = NULL;
                            
                            for(int j=0;j<numKids;j++)
                            {
                                //CA("Inside For Loop");
                                bool16 Flag12 =  kFalse;
                                kidUID=iHier->GetChildUID(j);
                                UIDRef boxRef(ItemFrameUIDList.GetDataBase(), kidUID);
                                TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
                                if(NewList.size()<=0)//This can be a Tagged Frame
                                {
                                    NewList.clear();
                                    NewList=itagReader->getFrameTags(ItemFrameUIDList.GetRef(i));
                                    if(NewList.size()==0)//Ordinary box
                                    {
                                        continue ;
                                    }
                                    
                                    for(int32 j=0; j <NewList.size(); j++)
                                    {
                                        if((NewList[j].whichTab == 4) || (isSprayItemPerFrameFlag1 == 2) )
                                        {
                                            XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
                                            if(tagList[j].imgFlag == 1)
                                            {
                                                PMString attributeValue;
                                                attributeValue.AppendNumber(PMReal(FirstItemId));
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
                                                
                                                attributeValue.clear();
                                                if(isSprayItemPerFrameFlag1 != 2)
                                                {
                                                    attributeValue.AppendNumber(PMReal(pNodeDataList[sprayedProductIndex].getPubId()));
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue));
                                                }
                                            }
                                            else if(isSprayItemPerFrameFlag1 == 2)
                                            {
                                                PMString attributeValue;
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue));
                                                
                                            }
                                            else
                                            {
                                                PMString attributeValue;
                                                attributeValue.AppendNumber(PMReal(FirstItemId));
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childId"),WideString(attributeValue));
                                                
                                                PMString childTag("1");
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childTag"),WideString(childTag));
                                                
                                                if(tagList[j].elementId == -803)
                                                {
                                                    attributeValue.Clear();
                                                    if(AlphabetArrayCount < 26)
                                                        attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
                                                    else
                                                        attributeValue.Append("a");
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                                }
                                                else if(tagList[j].elementId == -827)
                                                {
                                                    attributeValue.Clear();
                                                    int32 numberKey = AlphabetArrayCount + 1;
                                                    
                                                    attributeValue.AppendNumber(numberKey);
                                                    
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                                }
                                            }
                                            PMString attributeValue;
                                            attributeValue.AppendNumber(-777);
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
                                            
                                        }
                                    }
                                }
                                else
                                {
                                    for(int32 j=0; j <NewList.size(); j++)
                                    {
                                        XMLReference NewListXMLRef = NewList[j].tagPtr->GetXMLReference();
                                        if( (NewList[j].whichTab == 4) || (isSprayItemPerFrameFlag1 == 2))
                                        {
                                            if(NewList[j].imgFlag == 1)
                                            {
                                                PMString attributeValue;
                                                attributeValue.AppendNumber(PMReal(FirstItemId));
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
                                                
                                                attributeValue.clear();
                                                if(isSprayItemPerFrameFlag1 != 2)
                                                {
                                                    attributeValue.AppendNumber(PMReal(pNodeDataList[sprayedProductIndex].getPubId()));
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("isAutoResize"),WideString(attributeValue));
                                                }
                                                
                                            }
                                            else if(isSprayItemPerFrameFlag1 == 2)
                                            {
                                                PMString attributeValue;
                                                attributeValue.AppendNumber(PMReal(FirstItemId));
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("parentTypeID"),WideString(attributeValue));
                                                
                                            }
                                            else
                                            {
                                                PMString attributeValue;
                                                attributeValue.AppendNumber(PMReal(FirstItemId));
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childId"),WideString(attributeValue));
                                                
                                                PMString childTag("1");
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childTag"),WideString(childTag));
                                                if(NewList[j].elementId == -803)
                                                {
                                                    attributeValue.Clear();
                                                    if(AlphabetArrayCount < 26)
                                                        attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
                                                    else
                                                        attributeValue.Append("a");
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                                }
                                                else if(tagList[j].elementId == -827)
                                                {
                                                    attributeValue.Clear();
                                                    int32 numberKey = AlphabetArrayCount + 1;
                                                    
                                                    attributeValue.AppendNumber(numberKey);
                                                    
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                                }
                                            }
                                            PMString attributeValue;
                                            attributeValue.AppendNumber(-777);
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
                                            
                                        }
                                    }
                                }
                                //------------
                                for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
                                {
                                    NewList[tagIndex].tagPtr->Release();
                                }
                                
                                if(Flag12)
                                    break;
                            }
                        }
                    }
                    else
                    {
                        for(int32 j=0; j <tagList.size(); j++)
                        {
                            XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
                            if(tagList[j].whichTab == 4 || (isSprayItemPerFrameFlag1 == 2))
                            {
                                if(tagList[j].imgFlag == 1)
                                {
                                    PMString attributeValue;
                                    attributeValue.AppendNumber(PMReal(FirstItemId));
                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
                                    
                                    attributeValue.clear();
                                    if(isSprayItemPerFrameFlag1 != 2)
                                    {
                                        attributeValue.AppendNumber(PMReal(pNodeDataList[sprayedProductIndex].getPubId()));
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue));
                                    }
                                    
                                }
                                else if(isSprayItemPerFrameFlag1 == 2)
                                {
                                    PMString attributeValue;
                                    attributeValue.AppendNumber(PMReal(FirstItemId));
                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue));
                                    
                                }
                                else
                                {
                                    PMString attributeValue;
                                    attributeValue.AppendNumber(PMReal(FirstItemId));
                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childId"),WideString(attributeValue));
                                    
                                    PMString childTag("1");
                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childTag"),WideString(childTag));
                                    
                                    if(tagList[j].elementId == -803)
                                    {
                                        attributeValue.Clear();
                                        if(AlphabetArrayCount < 26)
                                            attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
                                        else
                                            attributeValue.Append("a");
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue));//Cs4
                                    }
                                    else if(tagList[j].elementId == -827)
                                    {
                                        attributeValue.Clear();
                                        int32 numberKey = AlphabetArrayCount + 1;
                                        
                                        attributeValue.AppendNumber(numberKey);
                                        
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                    }
                                }
                                PMString attributeValue;
                                attributeValue.AppendNumber(-777);
                                Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
                                
                            }
                        }
                    }
                    //Apsiva 9
                    for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
                    {
                        tagList[tagIndex].tagPtr->Release();
                    }
                }
                
                
                if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/)
                    DataSprayerPtr->setFlow(kTrue);
                else
                    DataSprayerPtr->setFlow(kFalse);
                //		DataSprayerPtr->setFlow(iSSSprayer->getHorizontalFlowForAllImageSprayFlag());
                //		DataSprayerPtr->setFlow(isItemHorizontalFlow);
                
                DataSprayerPtr->ClearNewImageFrameList();
                
                PMReal BottomMarkBeforeSprayForLeadingItem = 0.0;
                
                
                PMReal BottomMarkBeforeSprayForNonSprayItemPerFrame = 0.0;
                PMReal BottomMarkBeforeSprayForSprayItemPerFrame = 0.0;
                
                
                UIDList newLeadingItemSprayedUIDList(selectUIDList);
                UIDList newItemSprayedUIDList(selectUIDList);
                
                newLeadingItemSprayedUIDList.Clear();
                newItemSprayedUIDList.Clear();
                
                for(int32 j=0; j<listLength; j++)
                {
                    //CA("inside for");
                    TagList newAllSprayedTempTagList = itagReader->getTagsFromBox(selectUIDList.GetRef(j));
                    if(newAllSprayedTempTagList.size() == 0)
                    {
                        //CA("newAllSprayedTempTagList.size() == 0");
                        continue;
                    }
                    
                    for(int32 k=0; k <newAllSprayedTempTagList.size(); k++)
                    {
                        //CA("inside inner for");
                        UIDRef test1= selectUIDList.GetRef(j);
                        if(newAllSprayedTempTagList[k].isSprayItemPerFrame == 2)
                        {
                            newLeadingItemSprayedUIDList.Append(test1.GetUID());
                            break;
                        }
                        else if(newAllSprayedTempTagList[k].isSprayItemPerFrame == 1)
                        {
                            newLeadingItemSprayedUIDList.Append(test1.GetUID());
                            break;
                        }
                        else if(newAllSprayedTempTagList[k].isSprayItemPerFrame == 3)
                        {
                            //-----Horizontal Flow
                            newLeadingItemSprayedUIDList.Append(test1.GetUID());
                            break;
                            
                        }
                        else if(newAllSprayedTempTagList[k].isSprayItemPerFrame == -1)
                        {
                            newItemSprayedUIDList.Append(test1.GetUID());
                            break;
                        }
                    }
                    //Apsiva 9
                    for(int32 tagIndex = 0 ; tagIndex < newAllSprayedTempTagList.size() ; tagIndex++)
                    {
                        newAllSprayedTempTagList[tagIndex].tagPtr->Release();
                    }
                    
                    
                }
                
                Parameter requiredParameters;
                vectorBoxBounds newLeadingSprayedTempBoxBoundVector;
                PMRect newLeadingSprayedTempFramesForMaxBounds = kZeroRect;
                bool16 testResult = getMaxLimitsOfBoxes(newLeadingItemSprayedUIDList, newLeadingSprayedTempFramesForMaxBounds, newLeadingSprayedTempBoxBoundVector);
                if(testResult == kFalse)
                    break;
                
                vectorBoxBounds newItemSprayedTempBoxBoundVector;
                PMRect newItemSprayedTempFramesForMaxBounds = kZeroRect;
                
                if(newItemSprayedUIDList.Length() > 0)
                {
                    testResult = getMaxLimitsOfBoxes(newItemSprayedUIDList, newItemSprayedTempFramesForMaxBounds, newItemSprayedTempBoxBoundVector);
                    if(testResult == kFalse)
                        break;
                    
                    BottomMarkBeforeSprayForNonSprayItemPerFrame = newItemSprayedTempFramesForMaxBounds.Bottom();
                    requiredParameters.fNonSprayItemPerFramePresent = kTrue;
                }
                else
                {
                    requiredParameters.fNonSprayItemPerFramePresent = kFalse;
                }
                
                requiredParameters.WidthOfSprayItemPerFrames = newLeadingSprayedTempFramesForMaxBounds.Right() - newLeadingSprayedTempFramesForMaxBounds.Left();
                
                //BottomMarkBeforeSprayForNonSprayItemPerFrame = newItemSprayedTempFramesForMaxBounds.Bottom();
                BottomMarkBeforeSprayForSprayItemPerFrame = newLeadingSprayedTempFramesForMaxBounds.Bottom();
                
                PMReal RightMarkBeforeSprayForLeadingItem = CopiedItemMaxBoxBoundsBforeSpray.Right();
                
                
                requiredParameters.idxHorizontalCount = idxHorizontalCount;
                requiredParameters.idxVerticalCount = idxVerticalCount;
                
                
                vector<DynFrameStruct>::iterator itrDSDynFrameStruct;
                itrDSDynFrameStruct = requiredParameters.ProdBlockBoundList.begin();
                requiredParameters.ProdBlockBoundList.insert (itrDSDynFrameStruct,ProdBlockBoundList.begin(),ProdBlockBoundList.end());
                
                requiredParameters.RightMarkAfterSprayForLeadingItem = RightMarkBeforeSprayForLeadingItem;
                requiredParameters.BottomMarkAfterSprayForLeadingItem = BottomMarkBeforeSprayForNonSprayItemPerFrame;
                requiredParameters.PagemarginBoxBounds = PagemarginBoxBounds;
                requiredParameters.ItemStencilMaxBounds = ItemStencilMaxBounds;
                
                VecParameter vecParameters;
                vecParameters.push_back(requiredParameters);
                
                /*PMString str("before calling setDifferentParameters = ");
                 str.AppendNumber(static_cast<int32>(vecParameters.size()));
                 CA(str);*/
                
                DataSprayerPtr->setDifferentParameters(vecParameters);
                
                
                //CA("Before Sprayting Original Frame");
                ////// Spraying original frames first:
                ICommandSequence *seq=CmdUtils::BeginCommandSequence();
                for(int i=0; i<listLength; i++)
                {
                    PMString allInfo;
                    //CA("before calling itagReader->getTagsFromBox()");
                    tagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));
                    //CA("after calling itagReader->getTagsFromBox()");
                    if(tagList.size()<=0)//This can be a Tagged Frame
                    {
                        //CA(" tagList.size()<=0 ");
                        if(DataSprayerPtr->isFrameTagged(selectUIDList.GetRef(i)))
                        {
                            //CA("isFrameTagged");
                            bool16 flaG = kFalse;
                            
                            tagList.clear();
                            tagList=itagReader->getFrameTags(selectUIDList.GetRef(i));
                            
                            if(tagList.size()==0)//Ordinary box
                            {
                                continue ;
                            }
                            
                            //CA("Frame Tags Found");
                            InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
                            InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
                            if (!layoutSelectionSuite) {
                                break;
                            }
                            
                            selectionManager->DeselectAll(nil); // deselect every active CSB
                            //layoutSelectionSuite->Select(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
                            layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Added
                            
                            //CA("Before sprayForTaggedBox");
                            DataSprayerPtr->sprayForTaggedBox(selectUIDList.GetRef(i));
                        }
                        else
                        {
                            // This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
                            //CA(" else DataSprayerPtr->isFrameTagged");
                            InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
                            if(!iHier)
                            {
                                //CA(" !iHier >> Continue ");
                                continue;
                            }
                            UID kidUID;
                            int32 numKids=iHier->GetChildCount();
                            IIDXMLElement* ptr = NULL;
                            
                            for(int j=0;j<numKids;j++)
                            {
                                //CA("Inside For Loop");
                                kidUID=iHier->GetChildUID(j);
                                UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
                                TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
                                if(NewList.size()<=0)//This can be a Tagged Frame
                                {
                                    if(DataSprayerPtr->isFrameTagged(boxRef))
                                    {
                                        //CA("isFrameTagged(selectUIDList.GetRef(i))");
                                        DataSprayerPtr->sprayForTaggedBox(boxRef);
                                    }
                                    continue;
                                }
                                DataSprayerPtr->sprayForThisBox(boxRef, NewList);
                                //------------
                                for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
                                {
                                    NewList[tagIndex].tagPtr->Release();
                                }
                            }
                        }
                        //CA("Before Continue");
                        continue;
                    }
                    
                    bool16 flaG = kFalse;
                    //CA("Before sprayForThisBox");
                    DataSprayerPtr->sprayForThisBox(selectUIDList.GetRef(i), tagList);
                    
                    if(flaG)
                    {
                        InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
                        InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
                        if (!layoutSelectionSuite) {
                            break;
                        }
                        selectionManager->DeselectAll(nil); // deselect every active CSB
                        //layoutSelectionSuite->Select(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
                        layoutSelectionSuite->SelectPageItems(selectUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//added
                        
                        TagList NewTagList;
                        NewTagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));
                        
                        if(NewTagList.size()==0)//Ordinary box
                        {
                            return ;
                        }
                        //------------
                        for(int32 tagIndex = 0 ; tagIndex < NewTagList.size() ; tagIndex++)
                        {
                            NewTagList[tagIndex].tagPtr->Release();
                        }
                    }
                    
                    //Apsiva 9
                    for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
                    {
                        tagList[tagIndex].tagPtr->Release();
                    }
                }
                
                //CA("12");
                moveAutoResizeBoxAfterSpray(selectUIDList, vectorCopiedBoxBoundsBforeSpray);
                //CA("13");
                CmdUtils::EndCommandSequence(seq);
                isOriginalFrameSprayedInSprayItemPerFrame = kTrue;
                
                UIDList newTempUIDList(/*ItemFrameUIDList*/selectUIDList);
                VectorNewImageFrameUIDList newAddedFrameUIDListAfterSpray = DataSprayerPtr->getNewImageFrameList();
                
                if(newAddedFrameUIDListAfterSpray.size() > 0)
                {
                    //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
                    for(int q=0; q < newAddedFrameUIDListAfterSpray.size(); q++)
                    {
                        newTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
                        selectUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
                    }
                }
                
                
                result = getMaxLimitsOfBoxes(ItemFrameUIDList /*newTempUIDList*/, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                if(result == kFalse)
                    break;
                
                UIDList newAllSprayedTempUIDList(newTempUIDList);
                firstItemFramesStencilBoxBounds = ItemFramesStencilMaxBounds;//--Hori--
                
                
                // Right now only for Vertical Flow
                PMReal LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
                PMReal BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                PMReal TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                PMReal RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                
                PMReal MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
                PMReal MaxRightMarkSprayHZ = RightMarkAfterSpray;
                
                // Addedd By Sunil FROM HERE
                if(isItemHorizontalFlow1)
                {
                    if(BottomMarkAfterSpray > PagemarginBoxBounds.Bottom())
                    {
                        //CA("sprayed LI is going out of bottom margin");
                        layoutSelectionSuite->SelectPageItems(newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
                        deleteThisBoxUIDList(newTempUIDList);
                        --it1;
                        --AlphabetArrayCount;
                        --sprayedProductIndex;
                        --numProductsSprayed;
                        
                        //CA("Deleting the copied frame...LI is resized_Deleted");
                        deleteThisBoxUIDList(FirstcopiedBoxUIDList);
                        //CA("The copied frame is deleted...LI is resized_Deleted");
                        isAutoResized_FrameDeleted = kTrue;
                        ++FrameDeletedCount;
                        break;
                    }
                }
                // TILL HERE
                
                //--------
                PMReal box_Height =  BottomMark - TopMark;//ItemFramesStencilMaxBounds.Bottom() - ItemFramesStencilMaxBounds.Top();
                PMReal box_Width  = RightMark - LeftMark; //ItemFramesStencilMaxBounds.Right() - ItemFramesStencilMaxBounds.Left();
                bool16 verticalSprayBottomFrameFlag = kFalse;
                bool16 horizontalSprayBottomFrameFlag = kFalse;
                int32 countVal = 0;
                //PMString r("ItemIDInfo :  ");
                //r.AppendNumber(static_cast<int32>(ItemIDInfo->size()));
                //CA(r);
                DynFrameStruct CurrentFameStruct;
                CurrentFameStruct.HorzCnt = idxHorizontalCount;
                CurrentFameStruct.VertCnt = idxVerticalCount;
                CurrentFameStruct.BoxBounds = ItemFramesStencilMaxBounds;
                CurrentFameStruct.isLastHorzFrame = kFalse;
                
                if(isItemHorizontalFlow1)
                    ProdBlockBoundList.push_back(CurrentFameStruct);
                
                PMReal BottomMarkAfterSprayForLeadingItem = BottomMarkAfterSpray;
                RightMarkAfterSprayForLeadingItem = RightMarkAfterSpray;
                
                PMReal BottomMarkAfterSprayForNonSprayItemPerFrame;
                PMReal BottomMarkAfterSprayForSprayItemPerFrame;
                PMReal LeftMarkAfterSprayForSprayItemPerFrame;
                
                PMReal RightMarkAfterSprayForSprayItemPerFrame   = ItemFramesStencilMaxBounds.Right();
                
                bool16 fNonSprayItemPerFramePresentLocal = kFalse;
                bool16 nonSIPFBottomGreaterThanSIPFBottom = kFalse;
                
                bool16 SIPFTopGreaterThanNonSIPFBottom = kFalse;
                
                bool16 firstTimeCheckForLeadingItem = kTrue;
                bool16 leadingItemFrameSizeBigSprayFirstFrameHorizontalFlow = kFalse;
                
                vectorBoxBounds newAllSprayedTempBoxBoundVector;
                PMRect newAllSprayedTempFramesForMaxBounds = kZeroRect;
                
                PMReal LeftMarkAfterSprayForSprayItemPerFrameForResizeFrame = 0.0;
                bool16 isSpaceAvailableForSecondSprayItemPerFrame = kFalse;
                bool16 leadingItemFrameSizeBigSprayFirstFrame = kFalse;
                
                PMReal SprayItemPerFrameTop = 0.0;
                PMReal SprayItemPerFrameRight = 0.0;
                if(firstTimeCheckForLeadingItem == kTrue)
                {
                    //CA("firstTimeCheckForLeadingItem == kTrue");
                    firstTimeCheckForLeadingItem = kFalse;
                    bool16 testResult = kFalse;
                    
                    testResult = getMaxLimitsOfBoxes(newAllSprayedTempUIDList, newAllSprayedTempFramesForMaxBounds, newAllSprayedTempBoxBoundVector);
                    if(testResult == kFalse)
                        break;
                    
                    UIDList newLeadingItemSprayedUIDList(selectUIDList);
                    UIDList newItemSprayedUIDList(selectUIDList);
                    
                    newLeadingItemSprayedUIDList.Clear();
                    newItemSprayedUIDList.Clear();
                    for(int32 j=0; j<newAllSprayedTempUIDList.Length(); j++)
                    {
                        TagList newAllSprayedTempTagList = itagReader->getTagsFromBox(newAllSprayedTempUIDList.GetRef(j));
                        if(newAllSprayedTempTagList.size() == 0)
                        {
                            //CA("newAllSprayedTempTagList.size() == 0");
                            continue;
                        }
                        
                        for(int32 k=0; k <newAllSprayedTempTagList.size(); k++)
                        {
                            UIDRef test1= newAllSprayedTempUIDList.GetRef(j);
                            
                            /*if(newAllSprayedTempTagList[k].isSprayItemPerFrame == -1)
                             {
                             newItemSprayedUIDList.Append(test1.GetUID());
                             break;
                             }else*/
                            if(newAllSprayedTempTagList[k].isSprayItemPerFrame == 2)
                            {
                                newLeadingItemSprayedUIDList.Append(test1.GetUID());
                                break;
                            }
                            else if(newAllSprayedTempTagList[k].isSprayItemPerFrame == 1)
                            {
                                //-----Horizontal Flow
                                newLeadingItemSprayedUIDList.Append(test1.GetUID());
                                break;
                            }
                            else if(newAllSprayedTempTagList[k].isSprayItemPerFrame == 3)
                            {
                                newLeadingItemSprayedUIDList.Append(test1.GetUID());
                                break;
                            }
                            else if(newAllSprayedTempTagList[k].isSprayItemPerFrame == -1)
                            {
                                newItemSprayedUIDList.Append(test1.GetUID());
                                break;
                            }
                        }
                        
                        //Apsiva 9
                        for(int32 tagIndex = 0 ; tagIndex < newAllSprayedTempTagList.size() ; tagIndex++)
                        {
                            newAllSprayedTempTagList[tagIndex].tagPtr->Release();
                        }
                    }
                    
                    //vectorBoxBounds newLeadingSprayedTempBoxBoundVector;
                    //PMRect newLeadingSprayedTempFramesForMaxBounds = kZeroRect;
                    testResult = getMaxLimitsOfBoxes(newLeadingItemSprayedUIDList, newLeadingSprayedTempFramesForMaxBounds, newLeadingSprayedTempBoxBoundVector);
                    if(testResult == kFalse)
                        break;
                    
                    vectorBoxBounds newItemSprayedTempBoxBoundVector;
                    PMRect newItemSprayedTempFramesForMaxBounds = kZeroRect;
                    
                    //---if nonSprayItemPerFrame List Size Zero----
                    BottomMarkAfterSprayForNonSprayItemPerFrame = newLeadingSprayedTempFramesForMaxBounds.Bottom();
                    
                    if(newItemSprayedUIDList.Length() > 0 )
                    {
                        fNonSprayItemPerFramePresentLocal = kTrue;
                        testResult = getMaxLimitsOfBoxes(newItemSprayedUIDList, newItemSprayedTempFramesForMaxBounds, newItemSprayedTempBoxBoundVector);
                        if(testResult == kFalse)
                            break;
                        
                        BottomMarkAfterSprayForNonSprayItemPerFrame = newItemSprayedTempFramesForMaxBounds.Bottom();
                        BottomMarkAfterSprayForSprayItemPerFrame = newLeadingSprayedTempFramesForMaxBounds.Bottom();
                        LeftMarkAfterSprayForSprayItemPerFrame   = ItemFramesStencilMaxBounds.Left();//newLeadingSprayedTempFramesForMaxBounds.Left();
                        
                        SprayItemPerFrameTop = newLeadingSprayedTempFramesForMaxBounds.Top();
                        SprayItemPerFrameRight = newLeadingSprayedTempFramesForMaxBounds.Right();
                        
                        // Added by Sunil FROM HERE
                        if(isItemHorizontalFlow1)
                        {
                            if(BottomMarkAfterSprayForNonSprayItemPerFrame > BottomMarkAfterSprayForSprayItemPerFrame)
                            {
                                //CA("nonSIPFBottomGreaterThanSIPFBottom = kTrue: ---");
                                top_latest = BottomMarkAfterSprayForSprayItemPerFrame + VerticalBoxSpacing;
                                left_latest = RightOfPreviousColumn;
                            }
                            else
                            {
                                //CA("Else.....");
                                top_latest = SprayItemPerFrameTop;
                                left_latest = SprayItemPerFrameRight + HorizBoxSpacing;
                                if(top_latest < BottomMarkAfterSprayForNonSprayItemPerFrame)
                                {
                                    //CA("top_latest < BottomMarkAfterSprayForNonSprayItemPerFrame");
                                    left_latest = RightOfPreviousColumn;
                                    top_latest = BottomMarkAfterSprayForSprayItemPerFrame + VerticalBoxSpacing;
                                }
                            }
                            
                            if((left_latest + newLeadingSprayedTempFramesForMaxBounds.Width()) > (RightOfPreviousColumn + origMaxBoxBounds1.Width()))
                            {
                                //CA("frame will go out of its Leading width");
                                left_latest = RightOfPreviousColumn;
                                top_latest = BottomMarkAfterSprayForSprayItemPerFrame + VerticalBoxSpacing;
                            }
                            
                            if(top_latest + newLeadingSprayedTempFramesForMaxBounds.Height() > PagemarginBoxBounds.Bottom())
                            {
                                //CA("frame will go below the page Margin");
                                isColumnChangeForSpraying = kTrue;
                            }
                            
                            if(isColumnChangeForSpraying)
                            {
                                //CA("setting second child inside isColumnChangeForSpraying");
                                //isColumnChangeForSpraying = kFalse;
                                
                                newPageAdded = kFalse;
                                CCC++;
                                
                                Temp_Right2 = 0.0;
                                for(int p = 0; p < ProdBlockBoundList.size(); p++)
                                {
                                    Temp_Right2 = ProdBlockBoundList[p].BoxBounds.Right();
                                    if(Temp_Right2 > RightOfPreviousColumn)
                                    {
                                        //CA("Temp_Right > RightOfPreviousColumn");
                                        RightOfPreviousColumn = Temp_Right2;
                                    }
                                }
                                
                                RightOfPreviousColumn = RightOfPreviousColumn + HorizBoxSpacing;
                                if(RightOfPreviousColumn + newItemSprayedTempFramesForMaxBounds.Width() < PagemarginBoxBounds.Right())
                                {
                                    //CA("ders space for leading to be sprayed on right side");
                                    left_latest = RightOfPreviousColumn;
                                    if(PagemarginBoxBounds.RectIn(origMaxBoxBounds1))
                                    {
                                        //CA("Template is inside the current page");
                                        top_latest = origMaxBoxBounds1.Top();
                                    }
                                    else
                                    {
                                        //CA("Template is not inside the current page");
                                        top_latest = PagemarginBoxBounds.Top();
                                    }
                                }
                                
                                else
                                {
                                    //CA("ders no space for leading to be sprayed on right side");
                                    
                                    this->addNewPageHere(PagemarginBoxBounds);
                                    left_latest = RightOfPreviousColumn = PagemarginBoxBounds.Left();
                                    top_latest = PagemarginBoxBounds.Top();
                                }
                            }
                        }
                        
                        // TILL HERE
                        
                        // Added LATELY FROM HERE
                        else
                        {
                            if(BottomMarkAfterSprayForNonSprayItemPerFrame > BottomMarkAfterSprayForSprayItemPerFrame)
                            {
                                leadingItemFrameSizeBigSprayFirstFrame = kTrue;
                            }
                            
                            LeftMarkAfterSprayForSprayItemPerFrameForResizeFrame = newLeadingSprayedTempFramesForMaxBounds.Right() ;
                        }
                        // Added LATELY TILL HERE
                    }
                    
                    // Added by Sunil FROM HERE
                    else
                    {
                        if(isItemHorizontalFlow1)
                        {
                            //CA("setting for second frame of SIPF..");
                            top_latest = newLeadingSprayedTempFramesForMaxBounds.Bottom() + VerticalBoxSpacing;
                            left_latest = RightOfPreviousColumn;
                            
                            if(top_latest + origMaxBoxBounds1.Height() > PagemarginBoxBounds.Bottom())
                            {
                                //CA("SIPF going out of bottom margin");
                                RightOfPreviousColumn = newLeadingSprayedTempFramesForMaxBounds.Right() + HorizBoxSpacing;
                                if(RightOfPreviousColumn + origMaxBoxBounds1.Width() < PagemarginBoxBounds.Right())
                                {
                                    //CA("thers space on right side for the frame...");
                                    newPageAdded = kFalse;
                                    left_latest = RightOfPreviousColumn;
                                    if(PagemarginBoxBounds.RectIn(origMaxBoxBounds1))
                                    {
                                        //CA("Template is inside the page..");
                                        top_latest = origMaxBoxBounds1.Top();
                                    }
                                    else
                                    {
                                        //CA("Template isnot inside the page...");
                                        top_latest = PagemarginBoxBounds.Top();
                                    }
                                }
                                else
                                {
                                    //CA("Thers no space on the right side... so add page");
                                    this->addNewPageHere(PagemarginBoxBounds);
                                    left_latest = RightOfPreviousColumn = PagemarginBoxBounds.Left();
                                    top_latest = PagemarginBoxBounds.Top();
                                }
                            }
                        }
                    }
                    // TILL HERE
                }
                
                
                FrameBoundsList tmpFrameBoundsListOuter;
                VecReturnParameter returnParameterVecOuter;
                bool16 isColumnChangeOuter = kFalse;
                
                int32 tempHorizontalCntOuter = 0;
                int32 tempVerticalCountOuter = 0;
                //CA("befote do while");
                if(DataSprayerPtr->getIsColumnChange())
                    isColumnChangeOuter = kTrue;
                if(isColumnChangeOuter)
                {
                    returnParameterVecOuter = DataSprayerPtr->getReturnParameter();
                    
                    if(returnParameterVecOuter.size() > 0)
                    {
                        for(int32 returnParaIndex = 0; returnParaIndex < returnParameterVecOuter.size(); returnParaIndex++)
                        {
                            ReturnParameter rp = returnParameterVecOuter[returnParaIndex];
                            tempHorizontalCntOuter = rp.idxHorizontalCount;
                            tempVerticalCountOuter = rp.idxVerticalCount;
                            tmpFrameBoundsListOuter.push_back(rp.CurrentDynFrameStruct);
                        }
                        
                        DataSprayerPtr->ClearReturnParameter();
                    }
                    DataSprayerPtr->resetIsColumnChange();
                    
                    UIDList tempUIDList(newTempUIDList);
                    tempUIDList.Clear();
                    
                    if(returnParameterVecOuter[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size() > 0)
                    {  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
                        for(int q=0; q < returnParameterVecOuter[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size(); q++)
                        {
                            tempUIDList.Append(returnParameterVecOuter[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn[q]);
                        }
                    }
                    
                    /*ItemFramesStencilMaxBounds = kZeroRect;
                     result = getMaxLimitsOfBoxes(tempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                     if(result == kFalse)
                     break;*/
                }
                
                
                bool16 isInsideTempFrameForHorizantalFlow = kTrue;
                do
                {
                    //Sprayting Original Frame
                    if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/ == kFalse)
                    {
                        //CA("isItemHorizontalFlow == kFalse");
                        if (PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom())
                        {
                            // For Vertical Flow.........
                            //CA("Going out of Bottom Margin ");
                            LeftMarkAfterSpray = /*ItemFramesStencilMaxBounds.Right()*/MaxRightMarkSprayHZ + HorizBoxSpacing;
                            TopMarkAfterSpray = TopMark;
                            BottomMarkAfterSpray = TopMark - VerticalBoxSpacing;
                            
                            /*PMString ASD("LeftMarkAfterSpray : " );
                             ASD.AppendNumber(LeftMarkAfterSpray);
                             ASD.Append("   TopMarkAfterSpray : ");
                             ASD.AppendNumber(TopMarkAfterSpray);
                             CA(ASD);*/
                            
                            //if((ItemFramesStencilMaxBounds.Right()+ box_Width) > PagemarginBoxBounds.Right())
                            //{
                            //	//CA("ItemFramesStencilMaxBounds.Right()+ box_Width) > PagemarginBoxBounds.Right()");
                            //	verticalSprayBottomFrameFlag = kTrue;
                            //	CurrentFameStruct.isLastHorzFrame = kTrue;
                            //	countVal++;
                            //	ProdBlockBoundList.push_back(CurrentFameStruct);
                            //	continue;
                            //}
                            //else
                            {
                                //CA(" Else ItemFramesStencilMaxBounds.Right()+ box_Width) > PagemarginBoxBounds.Right()");
                                selectionManager->DeselectAll(nil); // deselect every active CSB
                                //layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented BY sachin sharma on 2/07/07
                                layoutSelectionSuite->SelectPageItems(/*SecondcopiedBoxUIDList*/newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
                                if(ItemFramesStencilMaxBounds.Top() != TopMark)
                                {
                                    //CA("ItemFramesStencilMaxBounds.Top() != TopMark");
                                    deleteThisBoxUIDList(newTempUIDList);
                                    
                                    if(isColumnChangeOuter)
                                    {
                                        UIDList tempUIDList(selectUIDList);
                                        tempUIDList.Clear();
                                        
                                        if(returnParameterVecOuter[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size() > 0)
                                        {  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
                                            for(int q=0; q < returnParameterVecOuter[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size(); q++)
                                            {
                                                tempUIDList.Append(returnParameterVecOuter[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn[q]);
                                            }
                                        }
                                        deleteThisBoxUIDList(tempUIDList);
                                        tmpFrameBoundsListOuter.clear();
                                        
                                        
                                        if(returnParameterVecOuter[0].fPageAdded)
                                        {
                                            ProdBlockBoundList.clear();
                                            idxHorizontalCount = 0;
                                            idxVerticalCount = 0;
                                            
                                            InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
                                            if (layoutData == nil)
                                            {
                                                continue;
                                            }
                                            
                                            IDocument* document = layoutData->GetDocument();
                                            if (document == nil)
                                            {
                                                continue;
                                            }
                                            
                                            IDataBase* database = ::GetDataBase(document);
                                            if(database==nil)
                                            {
                                                //CA("AP46_ProductFinder::SPSelectionObserver::AddOrDeleteSpreads::No database");
                                                continue;
                                            }
                                            
                                            UIDList toBeDeletedPagesUIDList(database);
                                            
                                            int32 noOfPagesToBeDeleted = static_cast<int32>(returnParameterVecOuter[0].PageUIDList.size());
                                            for(int32 pageIndex =0;pageIndex < noOfPagesToBeDeleted;pageIndex++)
                                            {
                                                toBeDeletedPagesUIDList.Append(returnParameterVecOuter[0].PageUIDList[pageIndex]);
                                            }
                                            InterfacePtr<ICommand> iDeletePageCmd(CmdUtils::CreateCommand(kDeletePageCmdBoss));
                                            if (iDeletePageCmd == nil)
                                            {
                                                continue;
                                            }
                                            
                                            InterfacePtr<IBoolData> iBoolData(iDeletePageCmd,UseDefaultIID());
                                            if (iBoolData == nil){
                                                
                                                continue;
                                            }
                                            iBoolData->Set(kFalse);
                                            
                                            iDeletePageCmd->SetItemList(toBeDeletedPagesUIDList);
                                            // process the command
                                            ErrorCode status1 = CmdUtils::ProcessCommand(iDeletePageCmd);
                                        }
                                        
                                        returnParameterVecOuter.clear();
                                        isColumnChangeOuter = kFalse;
                                    }
                                    
                                    
                                    --AlphabetArrayCount;
                                    idxVerticalCount = 0;
                                    idxHorizontalCount++;
                                    --sprayedProductIndex;
                                    --numProductsSprayed;
                                    DOWhileLoopBreakFlag = kTrue;
                                    /*deleteThisBoxUIDList(FirstcopiedBoxUIDList);*/
                                    //CA("DANGER - deleteThisBoxUIDList");
                                    
                                    PMRect StencilMaxBounds = kZeroRect;
                                    vectorBoxBounds BoxBoundVector;
                                    int32 size = FirstcopiedBoxUIDList.size();
                                    /*PMString str("");
                                     str.Append("Before calling getMaxLimitsOfBoxes FirstcopiedBoxUIDList size = ");
                                     str.AppendNumber(size);
                                     CA(str);*/
                                    
                                    
                                    result = getMaxLimitsOfBoxes(FirstcopiedBoxUIDList, StencilMaxBounds, BoxBoundVector);
                                    if(result == kFalse){
                                        
                                        //CA("result == kFalse");
                                        continue;
                                    }
                                    
                                    
                                    PMReal HorizBoxSpacing = iSSSprayer->getHorizontalBoxSpacing();
                                    PMReal OrgBoxMaxWidth = abs(StencilMaxBounds.Right() - StencilMaxBounds.Left());
                                    
                                    PMReal requiredWidth = OrgBoxMaxWidth + HorizBoxSpacing;
                                    
                                    PMReal maxRightOfLastColumn = -1;
                                    for(int p=0; p<ProdBlockBoundList.size(); p++)
                                    {
                                        if(ProdBlockBoundList[p].HorzCnt == idxHorizontalCount - 1)
                                        {
                                            if(ProdBlockBoundList[p].BoxBounds.Right() > maxRightOfLastColumn)
                                            {
                                                maxRightOfLastColumn = ProdBlockBoundList[p].BoxBounds.Right();
                                            }
                                            else if(ProdBlockBoundList[p].BoxBounds.Right() < -1 && maxRightOfLastColumn == -1)
                                            {
                                                maxRightOfLastColumn = ProdBlockBoundList[p].BoxBounds.Right();
                                            }
                                        }
                                    }
                                    
                                    if(maxRightOfLastColumn != -1)
                                    {
                                        PMReal BoxRightIfPlacedHere = maxRightOfLastColumn + requiredWidth;
                                        if(BoxRightIfPlacedHere > PagemarginBoxBounds.Right())
                                        {
                                            //CA("DAnger - isAddPageForLeadingItem_FrameDeletedAfterAutoResizeCase == kTrue");
                                            isAddPageForLeadingItem_FrameDeletedAfterAutoResizeCase = kTrue;
                                            deleteThisBoxUIDList(FirstcopiedBoxUIDList);
                                            break;
                                        }
                                        else
                                        {
                                            //CA("DAnger - isAddPageForLeadingItem_FrameDeletedAfterAutoResizeCase == kFalse");
                                            isAddPageForLeadingItem_FrameDeletedAfterAutoResizeCase = kFalse;
                                            deleteThisBoxUIDList(FirstcopiedBoxUIDList);
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        //CA("maxRightOfLastColumn == -1");
                                        
                                        //CA_NUM("counter value before inc : ",counter);
                                        ++counter;
                                        //CA_NUM("counter value after inc : ",counter);
                                        
                                        PMReal HorizBoxSpacing = iSSSprayer->getHorizontalBoxSpacing();
                                        for(int32 i = 0; i < counter; i++)
                                        {
                                            PMReal left11 = CopiedItemMaxBoxBoundsBforeSpray.Right() + (i * OrgBoxMaxWidth + HorizBoxSpacing);
                                            if((left11 + OrgBoxMaxWidth) > PagemarginBoxBounds.Right())
                                            {
                                                counter = 0;
                                                //CA("55555555555");
                                                ishorizontalSpaceAvailable = kFalse;
                                                isAddPageForLeadingItem_FrameDeletedAfterAutoResizeCase = kTrue;
                                                deleteThisBoxUIDList(FirstcopiedBoxUIDList);
                                                break;
                                            }
                                            else
                                            {
                                                //CA("Breaking inside Else");
                                                counter--;
                                                ishorizontalSpaceAvailable = kTrue;
                                                isAddPageForLeadingItem_FrameDeletedAfterAutoResizeCase = kFalse;
                                                isFrameDeleted = kTrue;
                                                deleteThisBoxUIDList(FirstcopiedBoxUIDList);
                                                break;
                                            }
                                        }
                                        
                                        if(isAddPageForLeadingItem_FrameDeletedAfterAutoResizeCase)
                                            break;
                                        else
                                            isAddPageForLeadingItem_FrameDeletedAfterAutoResizeCase = kFalse;
                                    }
                                    
                                    //if(ishorizontalSpaceAvailable == kTrue)
                                    //{
                                    //	//CA("DAnger 1");
                                    //	isAddPageForLeadingItem_FrameDeletedAfterAutoResizeCase = kFalse;
                                    //	ishorizontalSpaceAvailable = kFalse;
                                    //	deleteThisBoxUIDList(FirstcopiedBoxUIDList);
                                    //}
                                    //else
                                    //{
                                    //	//CA("DAnger - ishorizontalSpaceAvailable == kFalse");
                                    //	isAddPageForLeadingItem_FrameDeletedAfterAutoResizeCase = kTrue;
                                    //	deleteThisBoxUIDList(FirstcopiedBoxUIDList);
                                    //}
                                    
                                    continue;
                                }
                                
                                //PBPMPoint NewmoveToPoints(LeftMarkAfterSpray, TopMarkAfterSpray);
                                //moveBoxes(SecondcopiedBoxUIDList, NewmoveToPoints);
                                //CA("After Moving Boxes ");
                                //ItemFramesStencilMaxBounds = kZeroRect;
                                //result = getMaxLimitsOfBoxes(newTempUIDList/*SecondcopiedBoxUIDList*/, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                                //if(result == kFalse)
                                //	break;
                                
                                //LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
                                //BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                                //TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                                //RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                                
                                //selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);
                            }
                            
                        }
                        else
                        {
                            // Right now only for Vertical Flow
                            //for Vertical Flow
                            if((PagemarginBoxBounds.Bottom()- ItemFramesStencilMaxBounds.Bottom()) < (BottomMark - /*TopMark*/TopMarkOfFrame) && (ItemFramesStencilMaxBounds.Right()+ box_Width >  PagemarginBoxBounds.Right()) &&  verticalSprayBottomFrameFlag == kTrue )//added by sagar
                            {  // if there is no space for next frame below
                                //CA(" No NEXT frame on Bottom side");
                                LeftMarkAfterSpray = (MaxRightMarkSprayHZ) + HorizBoxSpacing;
                                BottomMarkAfterSpray = TopMark - VerticalBoxSpacing;
                                TopMarkAfterSpray = TopMark;
                                RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                                
                                if((ItemFramesStencilMaxBounds.Right()+ box_Width) > PagemarginBoxBounds.Right())
                                {
                                    //CA("ItemFramesStencilMaxBounds.Right()+ box_Width) > PagemarginBoxBounds.Right() ");
                                    
                                    verticalSprayBottomFrameFlag = kTrue;
                                    CurrentFameStruct.isLastHorzFrame = kTrue;
                                    countVal++;
                                    ProdBlockBoundList.push_back(CurrentFameStruct);
                                    
                                    if(isColumnChangeOuter)
                                    {
                                        if(returnParameterVecOuter[0].fPageAdded)
                                        {
                                            ProdBlockBoundList.clear();
                                        }
                                        if(tmpFrameBoundsListOuter.size() > 0)
                                        {
                                            for(int32 index = 0; index  < tmpFrameBoundsListOuter.size() ; index++)
                                            {
                                                ProdBlockBoundList.push_back(tmpFrameBoundsListOuter[index]);
                                            }
                                        }
                                        
                                        idxHorizontalCount = tempHorizontalCntOuter;
                                        idxVerticalCount = tempVerticalCountOuter;
                                        
                                        tmpFrameBoundsListOuter.clear();
                                        //returnParameterVecOuter.clear();
                                    }
                                    else if((newLeadingSprayedTempFramesForMaxBounds.Right()+ box_Width) < PagemarginBoxBounds.Right() && fNonSprayItemPerFramePresentLocal)
                                    {
                                        LeftMarkAfterSpray = newLeadingSprayedTempFramesForMaxBounds.Right() + HorizBoxSpacing;
                                        BottomMarkAfterSpray = BottomMarkAfterSprayForNonSprayItemPerFrame;
                                    }
                                    AllFramesOfCurrentLeadingItemIncludingItsChildItemFrames = newTempUIDList;
                                    
                                    continue;
                                }
                                else
                                {
                                    //CA("countVal >= 1");
                                    verticalSprayBottomFrameFlag = kFalse;
                                    if(countVal >= 1)
                                    {
                                        BottomMarkAfterSpray = PagemarginBoxBounds.Top() - VerticalBoxSpacing;
                                        TopMarkAfterSpray = PagemarginBoxBounds.Top();
                                        LeftMarkAfterSpray = ItemFramesStencilMaxBounds.Right() + HorizBoxSpacing;
                                        RightMarkAfterSpray = box_Width + ItemFramesStencilMaxBounds.Right();
                                        
                                    }
                                }
                                
                            }
                            else
                            {
                                //CA("Verticale Flow");
                                verticalSprayBottomFrameFlag = kFalse;
                                
                                if(newItemSprayedUIDList.Length() > 0)
                                {
                                    LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
                                    BottomMarkAfterSpray = BottomMarkAfterSprayForSprayItemPerFrame/*(ItemFramesStencilMaxBounds.Bottom())*/;
                                    TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                                    RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                                }
                                else
                                {
                                    LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
                                    BottomMarkAfterSpray = /*BottomMarkAfterSprayForSprayItemPerFrame*/(ItemFramesStencilMaxBounds.Bottom());
                                    TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                                    RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                                }
                                /*PMString ASD(" LeftMarkAfterSpray : ");
                                 ASD.AppendNumber(LeftMarkAfterSpray);
                                 ASD.Append("  BottomMarkAfterSpray: ");
                                 ASD.AppendNumber(BottomMarkAfterSpray);
                                 ASD.Append("TopMarkAfterSpray : ");
                                 ASD.AppendNumber(TopMarkAfterSpray);
                                 ASD.Append("RightMarkAfterSpray : ");
                                 ASD.AppendNumber(RightMarkAfterSpray);
                                 CA(ASD);*/
                            }
                            
                            //CA("1234");
                            if(MaxRightMarkSprayHZ < RightMarkAfterSpray)
                                MaxRightMarkSprayHZ = RightMarkAfterSpray;
                            
                            selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);
                        }
                    }
                    else
                    {
                        //CA("isItemHorizontalFlow == kTrue");
                        if(isColumnChangeForSpraying)
                            isColumnChangeForSpraying = kFalse;
                        else
                            CCC = 0;
                        /* -- CBS(was der b4)
                         if (PagemarginBoxBounds.Right() < ItemFramesStencilMaxBounds.Right())
                         {
                         CA("Going out of Right Margin overflow ");
                         LeftMarkAfterSpray = LeftMark;
                         TopMarkAfterSpray = MaxBottomMarkSprayHZ + VerticalBoxSpacing;
                         RightMarkAfterSpray = LeftMark - HorizBoxSpacing;
                         -- CBS(was der b4) */
                        
                        /*	PMString ASD("LeftMarkAfterSpray : " );
                         ASD.AppendNumber(LeftMarkAfterSpray);
                         ASD.Append("   TopMarkAfterSpray : ");
                         ASD.AppendNumber(TopMarkAfterSpray);
                         CA(ASD);*/
                        
                        /* -- CBS(was der b4)
                         if((ItemFramesStencilMaxBounds.Bottom() + box_Height) > PagemarginBoxBounds.Bottom())
                         {
                         CA("MMMMMM---ItemFramesStencilMaxBounds.Bottom() + box_Height) > PagemarginBoxBounds.Bottom()");
                         horizontalSprayBottomFrameFlag = kTrue;
                         ProdBlockBoundList.push_back(CurrentFameStruct);
                         countVal++;
                         continue;
                         }
                         else
                         {
                         selectionManager->DeselectAll(nil); // deselect every active CSB
                         //layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
                         layoutSelectionSuite->SelectPageItems(newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
                         if(ItemFramesStencilMaxBounds.Left() != LeftMark)
                         {
                         //CA("ItemFramesStencilMaxBounds.Left() != LeftMark ... before deleteThisBoxUIDList");
                         deleteThisBoxUIDList(newTempUIDList);
                         --it1;
                         --AlphabetArrayCount;
                         continue;
                         }
                         
                         ///PBPMPoint NewmoveToPoints(LeftMarkAfterSpray, TopMarkAfterSpray);
                         //moveBoxes(SecondcopiedBoxUIDList, NewmoveToPoints);
                         //CA("After Moving Boxes ");
                         ItemFramesStencilMaxBounds = kZeroRect;
                         result = getMaxLimitsOfBoxes(newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                         if(result == kFalse)
                         break;
                         
                         LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
                         BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                         TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                         RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                         
                         if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
                         MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
                         
                         selectUIDList.Append(newTempUIDList);
                         }
                         
                         }
                         -- CBS(was der b4) */
                        
                        /* -- CBS(was der b4)
                         else if(PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom())
                         {
                         CA("bottom greater than page margin");
                         LeftMarkAfterSpray = ItemFramesStencilMaxBounds.Right();
                         TopMarkAfterSpray = TopMark;
                         BottomMarkAfterSpray = TopMark - VerticalBoxSpacing;
                         RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                         
                         selectionManager->DeselectAll(nil); // deselect every active CSB
                         layoutSelectionSuite->SelectPageItems(newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
                         
                         deleteThisBoxUIDList(newTempUIDList);
                         --it1;
                         --AlphabetArrayCount;
                         //idxHorizontalCount++;
                         idxVerticalCount = 0;
                         
                         --sprayedProductIndex;
                         --numProductsSprayed;
                         DOWhileLoopBreakFlag = kTrue;
                         deleteThisBoxUIDList(FirstcopiedBoxUIDList);
                         
                         //isDeleteFrameForBottom = kTrue;
                         
                         -- CBS(was der b4) */
                        
                        /*if((ItemFramesStencilMaxBounds.Right() + ItemFramesStencilMaxBounds.Width()) > PagemarginBoxBounds.Right())
                         {
                         CA("isPageADD = kTrue;");
                         isPageADD = kTrue;
                         }
                         else
                         {
                         CA("isPageADD = kFalse;");
                         isPageADD = kFalse;
                         }*/
                        /* -- CBS(was der b4)
                         continue;
                         
                         }
                         -- CBS(was der b4) */
                        
                        /* -- CBS(was der b4)
                         else
                         {
                         if((PagemarginBoxBounds.Right()- ItemFramesStencilMaxBounds.Right()) < (RightMark - LeftMark))
                         {  // if there is no space for next frame on right side
                         CA("No Next Frame on Right Side ");
                         LeftMarkAfterSpray = LeftMark;
                         BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                         if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
                         MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
                         
                         TopMarkAfterSpray = MaxBottomMarkSprayHZ + VerticalBoxSpacing;
                         RightMarkAfterSpray = LeftMark - HorizBoxSpacing;
                         
                         //---------
                         if(nonSIPFBottomGreaterThanSIPFBottom || leadingItemFrameSizeBigSprayFirstFrameHorizontalFlow == kTrue)
                         box_Height = newLeadingSprayedTempFramesForMaxBounds.Height();
                         
                         if((ItemFramesStencilMaxBounds.Bottom() + box_Height) > PagemarginBoxBounds.Bottom())
                         {
                         CA("page ADDDDDD");
                         horizontalSprayBottomFrameFlag = kTrue;
                         countVal++;
                         ProdBlockBoundList.push_back(CurrentFameStruct);
                         continue;
                         }
                         else
                         {
                         horizontalSprayBottomFrameFlag = kFalse;
                         if(PagemarginBoxBounds.RectIn(origMaxBoxBounds1))
                         {
                         CA("101");
                         BottomMarkAfterSpray = ItemFramesStencilMaxBounds.Bottom() + VerticalBoxSpacing;
                         TopMarkAfterSpray = ItemFramesStencilMaxBounds.Bottom() + VerticalBoxSpacing;
                         LeftMarkAfterSpray = origMaxBoxBounds1.Left();					//ItemFramesStencilMaxBounds.Left();
                         RightMarkAfterSpray = origMaxBoxBounds1.Left() - HorizBoxSpacing;	//ItemFramesStencilMaxBounds.Right();
                         }
                         else
                         {
                         CA("102");
                         BottomMarkAfterSpray = box_Height + ItemFramesStencilMaxBounds.Bottom();
                         TopMarkAfterSpray = ItemFramesStencilMaxBounds.Bottom() + VerticalBoxSpacing;
                         LeftMarkAfterSpray = PagemarginBoxBounds.Left();					//ItemFramesStencilMaxBounds.Left();
                         RightMarkAfterSpray = PagemarginBoxBounds.Left() - HorizBoxSpacing;	//ItemFramesStencilMaxBounds.Right();
                         }
                         
                         }
                         
                         }else
                         {
                         CA("8888");
                         horizontalSprayBottomFrameFlag = kFalse;
                         
                         if(PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom())
                         {
                         CA("bottom greater than page margin");
                         LeftMarkAfterSpray = ItemFramesStencilMaxBounds.Right();
                         TopMarkAfterSpray = TopMark;
                         BottomMarkAfterSpray = TopMark - VerticalBoxSpacing;
                         RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                         
                         selectionManager->DeselectAll(nil); // deselect every active CSB
                         layoutSelectionSuite->SelectPageItems(newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
                         
                         deleteThisBoxUIDList(newTempUIDList);
                         --it1;
                         --AlphabetArrayCount;
                         idxHorizontalCount++;
                         idxVerticalCount = 0;
                         
                         --sprayedProductIndex;
                         --numProductsSprayed;
                         DOWhileLoopBreakFlag = kTrue;
                         deleteThisBoxUIDList(FirstcopiedBoxUIDList);
                         continue;
                         
                         }
                         else
                         {
                         LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left()) ;
                         BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                         TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                         RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                         
                         // Added by Sunil FROM HERE
                         if((ItemFramesStencilMaxBounds.Bottom() + ItemFramesStencilMaxBounds.Height()) > PagemarginBoxBounds.Bottom())
                         {
                         if(PagemarginBoxBounds.RectIn(origMaxBoxBounds1))
                         {
                         CA("Inside my IF");
                         TopMarkAfterSpray = origMaxBoxBounds1.Top();
                         BottomMarkAfterSpray = origMaxBoxBounds1.Top();
                         isInsideTempFrameForHorizantalFlow = kFalse;
                         
                         PMString c("----TopMarkAfterSpray  :  ");
                         c.AppendNumber(TopMarkAfterSpray);
                         c.Append("\nRightMarkAfterSpray  :  ");
                         c.AppendNumber(RightMarkAfterSpray);
                         CA(c);
                         //------
                         //if(ItemFramesStencilMaxBounds.Right() + firstItemFramesStencilBoxBounds.Width() > PagemarginBoxBounds.Right() )
                         //{
                         //	CA("page ADD for Right");
                         //	//horizontalSprayBottomFrameFlag = kTrue;
                         //}
                         
                         }
                         else
                         {
                         CA("Inside my ELSE");
                         TopMarkAfterSpray = PagemarginBoxBounds.Top();
                         BottomMarkAfterSpray = PagemarginBoxBounds.Top();
                         }
                         }
                         // Added by Sunil TILL HERE
                         
                         if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
                         MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
                         }
                         
                         }
                         
                         selectUIDList.Append(newTempUIDList);
                         }
                         -- CBS(was der b4) */
                    }
                    
                    
                    AllFramesOfCurrentLeadingItemIncludingItsChildItemFrames = newTempUIDList;
                    if(AllFramesOfCurrentLeadingItemIncludingItsChildItemFrames.size() > 0)
                    {
                        //CA(" AllFramesOfCurrentLeadingItemIncludingItsChildItemFrames.Length() > 0 ");
                    }
                    else
                    {
                        //CA(" AllFramesOfCurrentLeadingItemIncludingItsChildItemFrames.Length() <=  0 ");
                    }
                    
                    //CA("befote push");
                    if(isItemHorizontalFlow1 == kFalse)
                        ProdBlockBoundList.push_back(CurrentFameStruct);
                    // TILL HERE
                    
                    if(isColumnChangeOuter)
                    {
                        if(tmpFrameBoundsListOuter.size() > 0)
                        {
                            for(int32 index = 0; index  < tmpFrameBoundsListOuter.size() ; index++)
                            {
                                ProdBlockBoundList.push_back(tmpFrameBoundsListOuter[index]);
                            }
                        }
                        
                        idxHorizontalCount = tempHorizontalCntOuter;
                        idxVerticalCount = tempVerticalCountOuter;
                        
                        tmpFrameBoundsListOuter.clear();
                        
                    }
                    
                }
                while(0);
                if(DOWhileLoopBreakFlag)
                    continue;
                
                if(isItemHorizontalFlow1 == kFalse)
                    idxVerticalCount++;
                
                
                if(isColumnChangeOuter)
                {
                    UIDList tempUIDList(newTempUIDList);
                    tempUIDList.Clear();
                    
                    UIDList tempUIDListForDSFrames(tempUIDList);//---
                    tempUIDListForDSFrames.Clear();
                    
                    if(returnParameterVecOuter[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size() > 0)
                    {  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
                        for(int q=0; q < returnParameterVecOuter[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size(); q++)
                        {
                            tempUIDList.Append(returnParameterVecOuter[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn[q]);
                            if(q == 0)
                            {
                                tempUIDListForDSFrames.Append(returnParameterVecOuter[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn[q]);
                            }
                        }
                    }
                    
                    ItemFramesStencilMaxBounds = kZeroRect;
                    result = getMaxLimitsOfBoxes(tempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                    if(result == kFalse)
                        break;
                    
                    
                    if(returnParameterVecOuter[0].fPageAdded)
                    {
                        UID newpageUID = returnParameterVecOuter[0].PageUIDList[0];
                        
                        IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
                        if(fntDoc==nil)
                        {
                            ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::fntDoc==nil");
                            return ;
                        }
                        IDataBase* database = ::GetDataBase(fntDoc);
                        if(database==nil)
                        {
                            ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::database==nil");
                            return ;
                        }
                        UIDRef temp_pageRef(database, newpageUID);
                        
                        result = this->getMarginBounds(temp_pageRef, PagemarginBoxBounds);
                        if(result == kFalse)
                        {
                            result = this->getPageBounds(temp_pageRef, PagemarginBoxBounds);
                            if(result == kFalse)
                                break;
                            
                        }
                        
                        pageUidList.push_back(newpageUID);
                        PageCount++;
                    }
                    else
                    {
                        BottomMarkAfterSprayForNonSprayItemPerFrame = BottomMarkAfterSprayForNonSprayItemPerFrame + VerticalBoxSpacing;
                        
                        ItemFramesStencilMaxBounds = kZeroRect;
                        result = getMaxLimitsOfBoxes(tempUIDListForDSFrames, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                        if(result == kFalse)
                            break;
                        
                        if(RightMarkAfterSprayForSprayItemPerFrame > ItemFramesStencilMaxBounds.Right())
                        {
                            
                            PBPMPoint moveToPoints(LeftMarkAfterSprayForSprayItemPerFrameForResizeFrame, BottomMarkAfterSprayForNonSprayItemPerFrame);
                            moveBoxes(tempUIDListForDSFrames, moveToPoints);
                            
                            ItemFramesStencilMaxBounds = kZeroRect;
                            result = getMaxLimitsOfBoxes(tempUIDListForDSFrames, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                            if(result == kFalse)
                                break;
                            
                            if(PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom())
                            {
                                //CA("Page Add");
                                
                                UID pageUID;
                                UIDRef pageRef = UIDRef::gNull;
                                UIDRef spreadUIDRef = UIDRef::gNull;
                                
                                //if(addPageSplCase)
                                //{
                                //	//InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
                                //	//if (layoutData == nil)
                                //	//{
                                //	//	ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData");
                                //	//	break;
                                //	//}
                                
                                //	IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
                                //	if (document == nil)
                                //	{
                                //		ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No document");
                                //		break;
                                //	}
                                //	IDataBase* database = ::GetDataBase(document);
                                //	if(!database)
                                //	{
                                //		ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No database");
                                //		break;
                                //	}
                                
                                
                                //	bool16 result = this->getCurrentPage(pageRef, spreadUIDRef);
                                //	if(result == kFalse)
                                //		break;
                                
                                //	InterfacePtr<IPageList> iPageList(document,UseDefaultIID());
                                //	if(iPageList == nil)
                                //	{
                                //		//CA("iPageList == nil");
                                //		break;
                                //	}
                                
                                //	int32 bringSpreadToFrontIndex = 0;
                                
                                //	int32 pageToInsertAt = 0;
                                //	PageType pageType = iPageList->GetPageType(pageRef.GetUID()) ;
                                //	if(pageType == kLeftPage)
                                //	{
                                //		//CA("pageType == kLeftPage");
                                //		pageToInsertAt = 1;
                                //	}
                                //	else if(pageType == kRightPage)
                                //	{
                                //		/*UID pageUID = iPageList->GetNthPageUID(addPageSplCase_pageIndex);
                                //		if(pageUID == kInvalidUID)
                                //		{
                                //			ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");
                                //			break;
                                //		}
                                //
                                //		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
                                //		if (iSpreadList==nil)
                                //		{
                                //			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");
                                //			break;
                                //		}
                                
                                //		int32 pageCount = 0;
                                //		for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
                                //		{
                                //			UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
                                //			spreadUIDRef = temp_spreadUIDRef;
                                
                                //			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
                                //			if(!spread)
                                //			{
                                //				ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");
                                //				break;
                                //			}
                                //			int numPages=spread->GetNumPages();
                                //			pageCount +=  numPages;
                                //			if(pageCount > addPageSplCase_pageIndex)
                                //			{
                                //				++numSp;
                                //				UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
                                //				spreadUIDRef = temp_spreadUIDRef;
                                
                                //				bringSpreadToFrontIndex = numSp;
                                //				break;
                                //			}
                                //		}*/
                                
                                //		//CA("pageType == kRightPage");
                                //		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
                                //		if (iSpreadList==nil)
                                //		{
                                //			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");
                                //			break;
                                //		}
                                
                                //		UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(++addPageSplCase_SpreadIndex));
                                //		spreadUIDRef = temp_spreadUIDRef;
                                
                                //		bringSpreadToFrontIndex = addPageSplCase_SpreadIndex;
                                //		pageToInsertAt = 0;
                                //	}
                                //	else if(pageType == kUnisexPage)
                                //	{
                                //		//CA("pageType == kUnisexPage");
                                //		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
                                //		if (iSpreadList==nil)
                                //		{
                                //			ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");
                                //			break;
                                //		}
                                
                                //		UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(++addPageSplCase_SpreadIndex));
                                //		spreadUIDRef = temp_spreadUIDRef;
                                
                                //		bringSpreadToFrontIndex = addPageSplCase_SpreadIndex;
                                //		pageToInsertAt = 0;
                                //	}
                                //
                                //	ProductSpray ps;
                                //
                                //	/*PMString temp("addPageSplCase_SpreadIndex = ");
                                //	temp.AppendNumber(addPageSplCase_SpreadIndex);
                                //	temp.Append(", pageToInsertAt");
                                //	temp.AppendNumber(pageToInsertAt);
                                //	CA(temp);*/
                                
                                //	ErrorCode err = ps.CreatePages(document,spreadUIDRef,1,pageToInsertAt,kTrue);
                                //	if(err == kFailure)
                                //	{
                                //		//CA("err == kFailure");
                                //		return;
                                //	}
                                
                                //	CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
                                //	//CA("added succesfully");
                                //	/*++addPageSplCase_pageIndex;*/
                                //	if(pageType == kLeftPage)
                                //	{
                                //		++addPageSplCase_pageIndexPerSpread;
                                //	}
                                //	else if(pageType == kRightPage)
                                //	{
                                //		addPageSplCase_pageIndexPerSpread = 0;
                                //	}
                                //	else if(pageType == kUnisexPage)
                                //	{
                                //		addPageSplCase_pageIndexPerSpread = 0;
                                //	}
                                //
                                //	pageRef = UIDRef::gNull;
                                //	spreadUIDRef = UIDRef::gNull;
                                
                                //	bool16 result2 = this->getCurrentPage(pageRef, spreadUIDRef);
                                //	if(result2 == kFalse)
                                //		break;
                                
                                //
                                //	InterfacePtr<IMasterPage> ptrIMasterPage(pageRef, UseDefaultIID());
                                //	if(ptrIMasterPage != NULL)
                                //	{
                                //		ptrIMasterPage->SetMasterPageUID(addPageSplCase_MasterSpreadUIDOfTemplate);
                                //	}
                                //
                                
                                //	InterfacePtr<ITransform> transform(pageRef, UseDefaultIID());
                                //	if (!transform) {//CA("!transform ");
                                //		break;
                                //	}
                                //
                                //	InterfacePtr<IMargins> margins(transform, IID_IMARGINS);
                                //	// Note it's OK if the page does not have margins.
                                //	if (margins) {
                                //		//CA(" before margins->SetMargins");
                                //		margins->SetMargins(addPageSplCase_marginBBox.Left(), addPageSplCase_marginBBox.Top(), addPageSplCase_marginBBox.Right(), addPageSplCase_marginBBox.Bottom());
                                //			//CA(" After margins->SetMargins");
                                //	}
                                
                                //	InterfacePtr<IColumns> ptrIColumns(transform, IID_ICOLUMNS);
                                //	// Note it's OK if the page does not have margins.
                                //	if (ptrIColumns) {//CA("before SetColumns");
                                //		ptrIColumns->SetColumns(addPageSplCase_columns);
                                //		//CA("After SetColumns");
                                //	}
                                //
                                
                                
                                //
                                //
                                
                                //	IControlView* fntView = Utils<ILayoutUIUtils>()->QueryFrontView();
                                //	if (fntView == nil)
                                //	{
                                //		//CA("The front view is nil.");
                                //		break;
                                //	}
                                
                                //	InterfacePtr<IGeometry> spreadGeo(spreadUIDRef, UseDefaultIID());
                                //	if (!spreadGeo) {
                                //		//CA("!spreadGeo");
                                //		break;
                                //	}
                                //	InterfacePtr<ICommand> showSprdCmd(Utils<ILayoutUIUtils>()->MakeScrollToSpreadCmd(fntView, spreadGeo, kTrue));
                                //	CmdUtils::ProcessCommand(showSprdCmd);
                                
                                //	if (CmdUtils::ProcessCommand(showSprdCmd) != kSuccess) {
                                //		//CA("MakeScrollToSpreadCmd failed");
                                //		break;
                                //	}
                                
                                //	//InterfacePtr<ILayoutControlData> layoutData1(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
                                //	//if (layoutData1 == nil)
                                //	//{
                                //	//	ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData1");
                                //	//	break;
                                //	//}
                                
                                //	//IDocument* document1 = layoutData1->GetDocument();
                                //	//if (document1 == nil)
                                //	//{
                                //	//	ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No document1");
                                //	//	break;
                                //	//}
                                
                                //	//InterfacePtr<IPageList> iPageList1(document1,UseDefaultIID());
                                //	//if(iPageList1 == nil)
                                //	//{
                                //	//	CA("iPageList1 == nil");
                                //	//	break;
                                //	//}
                                
                                //	///*UID pageUID = iPageList1->GetNthPageUID(addPageSplCase_pageIndex);
                                //	//if(pageUID == kInvalidUID)
                                //	//{
                                //	//	ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");
                                //	//	break;
                                //	//}	*/
                                
                                //	//int32 InDESC = iPageList1->GetPageIndex(pageRef.GetUID());
                                //	//PMString ASD("InDESC : ");
                                //	//ASD.AppendNumber(InDESC);
                                //	//CA(ASD);
                                
                                //
                                //	/*InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
                                //	if(!spread)
                                //	{
                                //		ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");
                                //		return;
                                //	}
                                //
                                //	UID pageUID = spread->GetNthPageUID(addPageSplCase_pageIndexPerSpread);*/
                                //	pageUidList.push_back(/*pageUID*/pageRef.GetUID());
                                //}
                                //else
                                {
                                    Utils<ILayoutUIUtils>()->AddNewPage();
                                    
                                    
                                    IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
                                    if(fntDoc==nil)
                                    {
                                        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::fntDoc==nil");
                                        return ;
                                    }
                                    IDataBase* database = ::GetDataBase(fntDoc);
                                    if(database==nil)
                                    {
                                        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::database==nil");
                                        return ;
                                    }
                                    InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
                                    if (iSpreadList==nil)
                                    {
                                        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::iSpreadList==nil");
                                        return ;
                                    }
                                    
                                    
                                    for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
                                    {
                                        if((iSpreadList->GetSpreadCount()-1) > numSp )
                                        {
                                            continue;
                                        }
                                        UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
                                        spreadUIDRef = temp_spreadUIDRef;
                                        
                                        InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
                                        if(!spread)
                                        {
                                            ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::!spread");
                                            return ;
                                        }
                                        int numPages=spread->GetNumPages();
                                        pageUID = spread->GetNthPageUID(numPages-1);
                                        UIDRef temp_pageRef(database, pageUID);
                                        pageRef = temp_pageRef;
                                        
                                    }
                                    pageUidList.push_back(pageUID);
                                    PageCount++;
                                }
                                
                                bool16 result = getMarginBounds(pageRef, PagemarginBoxBounds);
                                if(result == kFalse)
                                {
                                    result = getPageBounds(pageRef, PagemarginBoxBounds);
                                    if(result == kFalse)
                                        return;
                                    
                                }
                                
                                RightMarkAfterSpray = PagemarginBoxBounds.Right();
                                LeftMarkAfterSpray = PagemarginBoxBounds.Left();
                                BottomMarkAfterSpray = PagemarginBoxBounds.Top();
                                
                                idxVerticalCount = 1;
                                idxHorizontalCount = 0;
                                ProdBlockBoundList.clear();
                                
                                selectionManager->DeselectAll(nil);
                                layoutSelectionSuite->SelectPageItems(tempUIDListForDSFrames, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
                                //CA("CopySelectedItems 2");
                                CopySelectedItems();
                                //CA("CopySelectedItems 2");
                                
                                UIDList secondCopiedBoxUIDList;
                                result = getSelectedBoxIds(secondCopiedBoxUIDList);
                                if(result == kFalse){
                                    break;
                                }
                                
                                int32 SecondcopiedBoxUIDListSize = secondCopiedBoxUIDList.Length();
                                if(SecondcopiedBoxUIDListSize == 0){
                                    continue;
                                }
                                
                                PBPMPoint moveToPoints(PagemarginBoxBounds.Left(), PagemarginBoxBounds.Top());
                                moveBoxes(secondCopiedBoxUIDList, moveToPoints);
                                
                                ItemFramesStencilMaxBounds = kZeroRect;
                                result = getMaxLimitsOfBoxes(secondCopiedBoxUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                                if(result == kFalse)
                                    break;
                                
                                RightMarkAfterSpray = ItemFramesStencilMaxBounds.Right();
                                TopMarkAfterSpray = ItemFramesStencilMaxBounds.Top();
                                LeftMarkAfterSpray = ItemFramesStencilMaxBounds.Left();
                                BottomMarkAfterSpray = ItemFramesStencilMaxBounds.Bottom();
                                
                                DynFrameStruct CurrentFameStruct;
                                CurrentFameStruct.HorzCnt = idxHorizontalCount;
                                CurrentFameStruct.VertCnt = idxVerticalCount;
                                CurrentFameStruct.BoxBounds = ItemFramesStencilMaxBounds;
                                CurrentFameStruct.isLastHorzFrame = kFalse;
                                
                                ProdBlockBoundList.push_back(CurrentFameStruct);
                                
                                deleteThisBoxUIDList(tempUIDListForDSFrames);
                                
                            }
                        }
                    }
                    
                }
                else if(nonSIPFBottomGreaterThanSIPFBottom == kTrue)
                {
                    ItemFramesStencilMaxBounds = newLeadingSprayedTempFramesForMaxBounds;
                    ItemFramesBoxBoundVector = newLeadingSprayedTempBoxBoundVector;
                }
                
                verticalSprayBottomFrameFlag = kFalse;
                bool16 isColumnChange = kFalse;
                //CA("before for loop : ");
                //bool16 isInsideTempFrameForHorizantalFlow = kTrue;
                int32 lastIndx=1;
                
                bool16 isSprayItemPerFrameFlagNew =checkIsHorizontalFlowFlagPresent(selectUIDList);
                for(; it1 != ItemIDInfo->end(); it1++,lastIndx++)
                {
                    //CA("Inside Loop...... ");
                    PMReal NewLeft = 0.0;
                    PMReal NewTop = 0.0;
                    PMReal NewRight= 0.0;
                    
                    UIDRef gpageRef = originalPageUIDRef;
                    UIDRef gspreadUIDRef = originalSpreadUIDRef;
                    
                    CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
                    
                    //if(isItemHorizontalFlow1 == kTrue)
                    //	idxHorizontalCount++;
                    
                    
                    
                    
                    //if(isItemHorizontalFlow == kFalse)
                    
                    
                    
                    
                    
                    
                    
                    
                    if(isSprayItemPerFrameFlagNew == kTrue)
                    {
                        //CA("	if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/ == kFalse)");
                        //NewRight = LeftMarkAfterSpray;
                        //	NewTop =  BottomMarkAfterSpray + VerticalBoxSpacing ; // Vertical Spacing 5
                        
                        NewLeft = RightMarkAfterSpray + HorizBoxSpacing;
                        NewTop =  TopMarkAfterSpray; // Vertical Spacing 5
                        
                        //if(nonSIPFBottomGreaterThanSIPFBottom == kTrue && !isColumnChangeOuter)
                        //{
                        //	NewTop = BottomMarkAfterSprayForSprayItemPerFrame + VerticalBoxSpacing;
                        //	NewLeft = LeftMarkAfterSprayForSprayItemPerFrame ;
                        //	nonSIPFBottomGreaterThanSIPFBottom = kFalse;
                        //}
                        //else if(isColumnChangeOuter)
                        //{
                        //	NewTop = /*BottomMarkAfterSprayForSprayItemPerFrame*/ItemFramesStencilMaxBounds.Bottom() /*+ VerticalBoxSpacing*/;
                        //	NewLeft = ItemFramesStencilMaxBounds.Left() ;
                        //	nonSIPFBottomGreaterThanSIPFBottom = kFalse;
                        //	isColumnChangeOuter = kFalse;
                        //}
                        //else if(isColumnChange)
                        //{
                        //	NewTop = ItemFramesStencilMaxBounds.Bottom() /*+ VerticalBoxSpacing*/;
                        //	NewLeft = ItemFramesStencilMaxBounds.Left() ;
                        //	nonSIPFBottomGreaterThanSIPFBottom = kFalse;
                        //	isColumnChange = kFalse;
                        //}
                        
                        //PMString r3("NewLeft  :  ");
                        //r3.AppendNumber(NewLeft);
                        //r3.Append("\nNewTop :  ");
                        //r3.AppendNumber(NewTop);
                        //CA(r3);
                        //ptrIAppFramework->LogDebug(r3);
                        
                        //check for if space is there to put next template frames below curenly sprayed frame
                        if((PagemarginBoxBounds.Bottom()- ItemFramesStencilMaxBounds.Bottom()) < (BottomMark - /*TopMark*/TopMarkOfFrame) && (ItemFramesStencilMaxBounds.Right()+ box_Width >  PagemarginBoxBounds.Right()))
                        {
                            //CA("CONDITION  :::::::");
                            // should not go beyond the template width
                            //if((ItemFramesStencilMaxBounds.Right()+ box_Width) > RightMarkAfterSprayForLeadingItem/*PagemarginBoxBounds.Right()*/)
                            //{
                            //	CA("TRUE");
                            
                            //	if(RightMarkAfterSprayForLeadingItem + HorizBoxSpacing + Max_Template_Width < PagemarginBoxBounds.Right())
                            //	{
                            //		//if There is space for
                            //		idxVerticalCount = 0;
                            //		idxHorizontalCount++;
                            //		verticalSprayBottomFrameFlag = kFalse;
                            
                            //		if(PagemarginBoxBounds.RectIn(ItemStencilMaxBounds)) //**** if Stencil is with in Page
                            //		{
                            //			CA("Inside RectIn");
                            //			BottomMarkAfterSpray = ItemStencilMaxBounds.Top();
                            //
                            //		}
                            //		else
                            //			BottomMarkAfterSpray = PagemarginBoxBounds.Top()/* - VerticalBoxSpacing*/;
                            //
                            //
                            //		TopMarkAfterSpray = PagemarginBoxBounds.Top();
                            //		LeftMarkAfterSpray = RightMarkAfterSprayForLeadingItem + HorizBoxSpacing;
                            //		//RightMarkAfterSpray = box_Width + ItemFramesStencilMaxBounds.Right();
                            
                            //		NewLeft = LeftMarkAfterSpray;
                            //		NewTop = BottomMarkAfterSpray;
                            
                            //		RightMarkAfterSprayForLeadingItem = RightMarkAfterSprayForLeadingItem + HorizBoxSpacing + box_Width;
                            //	}
                            //	else
                            {
                                //CA("verticalSprayBottomFrameFlag = kTrue;");
                                idxVerticalCount = 0;
                                verticalSprayBottomFrameFlag = kTrue;
                            }
                            //}
                            //else
                            //{
                            //	//CA("FALSE");
                            //	idxVerticalCount = 0;
                            //	idxHorizontalCount++;
                            
                            //	fFaltuCase = kTrue;
                            //	if(PagemarginBoxBounds.RectIn(ItemStencilMaxBounds)) //**** if Stencil is with in Page
                            //	{
                            //		//CA("Inside RectIn");
                            //		//BottomMarkAfterSpray = ItemStencilMaxBounds.Top();
                            //		BottomMarkAfterSpray = BottomMarkAfterSprayForNonSprayItemPerFrame  + VerticalBoxSpacing;
                            //	}
                            //	else if((ItemFramesStencilMaxBounds.Right() + HorizBoxSpacing + box_Width) < RightMarkAfterSprayForLeadingItem)
                            //	{
                            //		//CA("still under leading frame");
                            //		BottomMarkAfterSpray = BottomMarkAfterSprayForNonSprayItemPerFrame  + VerticalBoxSpacing;
                            //	}
                            //	else
                            //		BottomMarkAfterSpray = PagemarginBoxBounds.Top()/* - VerticalBoxSpacing*/;
                            //
                            //
                            //	TopMarkAfterSpray = PagemarginBoxBounds.Top();
                            //	LeftMarkAfterSpray = ItemFramesStencilMaxBounds.Right() + HorizBoxSpacing;
                            //	RightMarkAfterSpray = box_Width + ItemFramesStencilMaxBounds.Right();
                            
                            //	NewLeft = LeftMarkAfterSpray;
                            //	NewTop = BottomMarkAfterSpray;
                            
                            //}
                            
                        }
                        else
                        {
                            //CA("verticalSprayBottomFrameFlag = kFalse;");
                            verticalSprayBottomFrameFlag = kFalse;
                        }
                        
                        if( ((ItemFramesStencilMaxBounds.Right() /*+ HorizBoxSpacing */+ box_Width) >  PagemarginBoxBounds.Right()) && verticalSprayBottomFrameFlag == kFalse)
                        {
                            //CA("if( ItemFramesStencilMaxBounds.Right()+ HorizBoxSpacing >  PagemarginBoxBounds.Right())");
                            NewLeft = PagemarginBoxBounds.Left();
                            NewTop =  BottomMarkAfterSpray + VerticalBoxSpacing; // Vertical Spacing 5
                        }
                        
                        if( (ItemFramesStencilMaxBounds.Right()+ box_Width >  PagemarginBoxBounds.Right()) && verticalSprayBottomFrameFlag == kTrue )
                        {
                            //CA("if(PagemarginBoxBounds.Right() < ItemFramesStencilMaxBounds.Right() && verticalSprayBottomFrameFlag )	");
                            
                            UID pageUID;
                            UIDRef pageRef = UIDRef::gNull;
                            UIDRef spreadUIDRef = UIDRef::gNull;
                            
                            if(addPageSplCase)
                            {
                                //InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
                                //if (layoutData == nil)
                                //{
                                //	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData");
                                //	break;
                                //}
                                
                                IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
                                if (document == nil)
                                {
                                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No document");
                                    break;
                                }
                                IDataBase* database = ::GetDataBase(document);
                                if(!database)
                                {
                                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No database");
                                    break;
                                }
                                
                                
                                bool16 result = this->getCurrentPage(pageRef, spreadUIDRef);
                                if(result == kFalse)
                                    break;
                                
                                InterfacePtr<IPageList> iPageList(document,UseDefaultIID());
                                if(iPageList == nil)
                                {
                                    //CA("iPageList == nil");
                                    break;
                                }
                                
                                int32 bringSpreadToFrontIndex = 0;
                                
                                int32 pageToInsertAt = 0;
                                PageType pageType = iPageList->GetPageType(pageRef.GetUID()) ;
                                if(pageType == kLeftPage)
                                {
                                    //CA("pageType == kLeftPage");
                                    pageToInsertAt = 1;
                                }
                                else if(pageType == kRightPage)
                                {
                                    /*UID pageUID = iPageList->GetNthPageUID(addPageSplCase_pageIndex);
                                     if(pageUID == kInvalidUID)
                                     {
                                     ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");
                                     break;
                                     }
                                     
                                     InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
                                     if (iSpreadList==nil)
                                     {
                                     ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");
                                     break;
                                     }
                                     
                                     int32 pageCount = 0;
                                     for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
                                     {
                                     UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
                                     spreadUIDRef = temp_spreadUIDRef;
                                     
                                     
                                     InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
                                     if(!spread)
                                     {
                                     ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");
                                     break;
                                     }
                                     int numPages=spread->GetNumPages();
                                     pageCount +=  numPages;
                                     if(pageCount > addPageSplCase_pageIndex)
                                     {
                                     ++numSp;
                                     UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
                                     spreadUIDRef = temp_spreadUIDRef;
                                     
                                     bringSpreadToFrontIndex = numSp;
                                     break;
                                     }
                                     }*/
                                    
                                    //CA("pageType == kRightPage");
                                    InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
                                    if (iSpreadList==nil)
                                    {
                                        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");
                                        break;
                                    }
                                    
                                    UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(++addPageSplCase_SpreadIndex));
                                    spreadUIDRef = temp_spreadUIDRef;
                                    
                                    bringSpreadToFrontIndex = addPageSplCase_SpreadIndex;
                                    pageToInsertAt = 0;
                                }
                                else if(pageType == kUnisexPage)
                                {
                                    //CA("pageType == kUnisexPage");
                                    InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
                                    if (iSpreadList==nil)
                                    {
                                        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");
                                        break;
                                    }
                                    
                                    UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(++addPageSplCase_SpreadIndex));
                                    spreadUIDRef = temp_spreadUIDRef;
                                    
                                    bringSpreadToFrontIndex = addPageSplCase_SpreadIndex;
                                    pageToInsertAt = 0;
                                }
                                
                                //ProductSpray ps;
                                
                                /*PMString temp("addPageSplCase_SpreadIndex = ");
                                 temp.AppendNumber(addPageSplCase_SpreadIndex);
                                 temp.Append(", pageToInsertAt");
                                 temp.AppendNumber(pageToInsertAt);
                                 CA(temp);*/
                                
                                ErrorCode err = CreatePages(document,spreadUIDRef,1,pageToInsertAt,kTrue);
                                if(err == kFailure)
                                {
                                    //CA("err == kFailure");
                                    return;
                                }
                                
                                PageCount++;
                                
                                CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
                                //CA("added succesfully");
                                /*++addPageSplCase_pageIndex;*/
                                if(pageType == kLeftPage)
                                {
                                    ++addPageSplCase_pageIndexPerSpread;
                                }
                                else if(pageType == kRightPage)
                                {
                                    addPageSplCase_pageIndexPerSpread = 0;
                                }
                                else if(pageType == kUnisexPage)
                                {
                                    addPageSplCase_pageIndexPerSpread = 0;
                                }
                                
                                pageRef = UIDRef::gNull;
                                spreadUIDRef = UIDRef::gNull;
                                
                                bool16 result2 = this->getCurrentPage(pageRef, spreadUIDRef);
                                if(result2 == kFalse)
                                    break;
                                
                                
                                
                                InterfacePtr<IMasterPage> ptrIMasterPage(pageRef, UseDefaultIID());
                                if(ptrIMasterPage != NULL)
                                {
                                    ptrIMasterPage->SetMasterPageData/*SetMasterPageUID*/(addPageSplCase_MasterSpreadUIDOfTemplate);	//----CS5--
                                }
                                
                                
                                InterfacePtr<ITransform> transform(pageRef, UseDefaultIID());
                                if (!transform) {//CA("!transform ");
                                    break;
                                }
                                
                                InterfacePtr<IMargins> margins(transform, IID_IMARGINS);
                                // Note it's OK if the page does not have margins.
                                if (margins) {
                                    //CA(" before margins->SetMargins");
                                    margins->SetMargins(addPageSplCase_marginBBox.Left(), addPageSplCase_marginBBox.Top(), addPageSplCase_marginBBox.Right(), addPageSplCase_marginBBox.Bottom());
                                    //CA(" After margins->SetMargins");
                                }
                                
                                InterfacePtr<IColumns> ptrIColumns(transform, IID_ICOLUMNS);
                                // Note it's OK if the page does not have margins.
                                if (ptrIColumns) {//CA("before SetColumns");
                                    ptrIColumns->SetColumns(addPageSplCase_columns);
                                    //CA("After SetColumns");
                                }
                                
                                //IControlView* fntView = Utils<ILayoutUIUtils>()->QueryFrontView(); //og
                                InterfacePtr<IControlView> fntView(Utils<ILayoutUIUtils>()->QueryFrontView()); // changed by avinash
                                if (fntView == nil)
                                {
                                    //CA("The front view is nil.");
                                    break;
                                }
                                
                                InterfacePtr<IGeometry> spreadGeo(spreadUIDRef, UseDefaultIID());
                                if (!spreadGeo) {
                                    //CA("!spreadGeo");
                                    break;
                                }
                                InterfacePtr<ICommand> showSprdCmd(Utils<ILayoutUIUtils>()->MakeScrollToSpreadCmd(fntView, spreadGeo, kTrue));
                                CmdUtils::ProcessCommand(showSprdCmd);
                                
                                if (CmdUtils::ProcessCommand(showSprdCmd) != kSuccess) {
                                    //CA("MakeScrollToSpreadCmd failed");
                                    break;
                                }
                                
                                //InterfacePtr<ILayoutControlData> layoutData1(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
                                //if (layoutData1 == nil)
                                //{
                                //	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData1");
                                //	break;
                                //}
                                
                                //IDocument* document1 = layoutData1->GetDocument();
                                //if (document1 == nil)
                                //{
                                //	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No document1");
                                //	break;
                                //}
                                
                                //InterfacePtr<IPageList> iPageList1(document1,UseDefaultIID());
                                //if(iPageList1 == nil)
                                //{
                                //	CA("iPageList1 == nil");
                                //	break;
                                //}
                                
                                ///*UID pageUID = iPageList1->GetNthPageUID(addPageSplCase_pageIndex);
                                //if(pageUID == kInvalidUID)
                                //{
                                //	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");
                                //	break;
                                //}	*/
                                
                                //int32 InDESC = iPageList1->GetPageIndex(pageRef.GetUID());
                                //PMString ASD("InDESC : ");
                                //ASD.AppendNumber(InDESC);
                                //CA(ASD);
                                
                                /*InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
                                 if(!spread)
                                 {
                                 ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");
                                 return;
                                 }
                                 
                                 UID pageUID = spread->GetNthPageUID(addPageSplCase_pageIndexPerSpread);*/
                                pageUidList.push_back(/*pageUID*/pageRef.GetUID());
                            }
                            else
                            {
                                //CA("Adding new page");
                                Utils<ILayoutUIUtils>()->AddNewPage();
                                PageCount= PageCount+1;
                                //CA_NUM("New page added and page count is : ", PageCount);
                                
                                if(fFaltuCase)
                                    fFaltuCase = kFalse;
                                
                                IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
                                if(fntDoc==nil)
                                {
                                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::fntDoc==nil");
                                    return ;
                                }
                                IDataBase* database = ::GetDataBase(fntDoc);
                                if(database==nil)
                                {
                                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::database==nil");
                                    return ;
                                }
                                InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
                                if (iSpreadList==nil)
                                {
                                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::iSpreadList==nil");
                                    return ;
                                }
                                
                                /*UID pageUID;
                                 UIDRef pageRef = UIDRef::gNull;
                                 UIDRef spreadUIDRef = UIDRef::gNull;*/
                                
                                for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
                                {
                                    if( (iSpreadList->GetSpreadCount()-1) > numSp )
                                    {
                                        continue;
                                    }
                                    UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
                                    spreadUIDRef = temp_spreadUIDRef;
                                    
                                    InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
                                    if(!spread)
                                    {
                                        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::!spread");
                                        return ;
                                    }
                                    int numPages=spread->GetNumPages();
                                    pageUID = spread->GetNthPageUID(numPages-1);
                                    UIDRef temp_pageRef(database, pageUID);
                                    pageRef = temp_pageRef;
                                    
                                }
                                pageUidList.push_back(pageUID);
                            }
                            
                            
                            /*gspreadUIDRef = spreadUIDRef;
                             gpageRef = pageRef;*/
                            
                            result = this->getMarginBounds(pageRef, PagemarginBoxBounds);
                            if(result == kFalse)
                            {
                                result = this->getPageBounds(pageRef, PagemarginBoxBounds);
                                if(result == kFalse)
                                    break;
                                
                            }
                            
                            NewLeft = PagemarginBoxBounds.Left()/* + HorizBoxSpacing*/;
                            NewTop = PagemarginBoxBounds.Top();
                            //CA("ProdBlockBoundList.clear()");
                            
                            idxVerticalCount = 0;
                            idxHorizontalCount = 0;
                            ProdBlockBoundList.clear();
                            
                            RightMarkAfterSprayForLeadingItem = NewLeft + box_Width;
                            
                        }
                        
                    }
                    else
                    {
                        
                        
                        NewLeft = LeftMarkAfterSpray;
                        NewTop =  BottomMarkAfterSpray + VerticalBoxSpacing ; // Vertical Spacing 5
                        
                        if(nonSIPFBottomGreaterThanSIPFBottom == kTrue && !isColumnChangeOuter)
                        {
                            NewTop = BottomMarkAfterSprayForSprayItemPerFrame + VerticalBoxSpacing;
                            NewLeft = LeftMarkAfterSprayForSprayItemPerFrame ;
                            nonSIPFBottomGreaterThanSIPFBottom = kFalse;
                        }
                        else if(isColumnChangeOuter)
                        {
                            NewTop = /*BottomMarkAfterSprayForSprayItemPerFrame*/ItemFramesStencilMaxBounds.Bottom() /*+ VerticalBoxSpacing*/;
                            NewLeft = ItemFramesStencilMaxBounds.Left() ;
                            nonSIPFBottomGreaterThanSIPFBottom = kFalse;
                            isColumnChangeOuter = kFalse;
                        }
                        else if(isColumnChange)
                        {
                            NewTop = ItemFramesStencilMaxBounds.Bottom() /*+ VerticalBoxSpacing*/;
                            NewLeft = ItemFramesStencilMaxBounds.Left() ;
                            nonSIPFBottomGreaterThanSIPFBottom = kFalse;
                            isColumnChange = kFalse;
                        }
                        
                        //PMString r3("NewLeft  :  ");
                        //r3.AppendNumber(NewLeft);
                        //r3.Append("\nNewTop :  ");
                        //r3.AppendNumber(NewTop);
                        //CA(r3);
                        //ptrIAppFramework->LogDebug(r3);
                        
                        //check for if space is there to put next template frames below curenly sprayed frame
                        if((PagemarginBoxBounds.Bottom()- ItemFramesStencilMaxBounds.Bottom()) < (BottomMark - /*TopMark*/TopMarkOfFrame))
                        {
                            //CA("CONDITION  :::::::");
                            // should not go beyond the template width
                            if((ItemFramesStencilMaxBounds.Right()+ box_Width) > RightMarkAfterSprayForLeadingItem/*PagemarginBoxBounds.Right()*/)
                            {
                                //CA("TRUE");
                                
                                if(RightMarkAfterSprayForLeadingItem + HorizBoxSpacing + Max_Template_Width < PagemarginBoxBounds.Right())
                                {
                                    //if There is space for
                                    idxVerticalCount = 0;
                                    idxHorizontalCount++;
                                    verticalSprayBottomFrameFlag = kFalse;
                                    
                                    if(PagemarginBoxBounds.RectIn(ItemStencilMaxBounds)) //**** if Stencil is with in Page
                                    {
                                        //CA("Inside RectIn");
                                        BottomMarkAfterSpray = ItemStencilMaxBounds.Top();
                                        
                                    }
                                    else
                                        BottomMarkAfterSpray = PagemarginBoxBounds.Top()/* - VerticalBoxSpacing*/;
                                    
                                    
                                    TopMarkAfterSpray = PagemarginBoxBounds.Top();
                                    LeftMarkAfterSpray = RightMarkAfterSprayForLeadingItem + HorizBoxSpacing;
                                    //RightMarkAfterSpray = box_Width + ItemFramesStencilMaxBounds.Right();
                                    
                                    NewLeft = LeftMarkAfterSpray;
                                    NewTop = BottomMarkAfterSpray;
                                    
                                    RightMarkAfterSprayForLeadingItem = RightMarkAfterSprayForLeadingItem + HorizBoxSpacing + box_Width;
                                }
                                else
                                {
                                    idxVerticalCount = 0;
                                    verticalSprayBottomFrameFlag = kTrue;
                                }
                            }
                            else
                            {
                                //CA("FALSE");
                                idxVerticalCount = 0;
                                idxHorizontalCount++;
                                
                                fFaltuCase = kTrue;
                                if(PagemarginBoxBounds.RectIn(ItemStencilMaxBounds)) //**** if Stencil is with in Page
                                {
                                    //CA("Inside RectIn");
                                    //BottomMarkAfterSpray = ItemStencilMaxBounds.Top();
                                    BottomMarkAfterSpray = BottomMarkAfterSprayForNonSprayItemPerFrame  + VerticalBoxSpacing;
                                }
                                else if((ItemFramesStencilMaxBounds.Right() + HorizBoxSpacing + box_Width) < RightMarkAfterSprayForLeadingItem)
                                {
                                    //CA("still under leading frame");
                                    BottomMarkAfterSpray = BottomMarkAfterSprayForNonSprayItemPerFrame  + VerticalBoxSpacing;
                                }
                                else
                                    BottomMarkAfterSpray = PagemarginBoxBounds.Top()/* - VerticalBoxSpacing*/;
                                
                                
                                TopMarkAfterSpray = PagemarginBoxBounds.Top();
                                LeftMarkAfterSpray = ItemFramesStencilMaxBounds.Right() + HorizBoxSpacing;
                                RightMarkAfterSpray = box_Width + ItemFramesStencilMaxBounds.Right();
                                
                                NewLeft = LeftMarkAfterSpray;
                                NewTop = BottomMarkAfterSpray;
                                
                            }
                            
                        }
                        else
                        {
                            verticalSprayBottomFrameFlag = kFalse;
                        }
                        
                        if(PagemarginBoxBounds.Right() < ItemFramesStencilMaxBounds.Right() || verticalSprayBottomFrameFlag )
                        {
                            //CA("PagemarginBoxBounds.Right() < ItemFramesStencilMaxBounds.Right()");
                            
                            UID pageUID;
                            UIDRef pageRef = UIDRef::gNull;
                            UIDRef spreadUIDRef = UIDRef::gNull;
                            
                            if(addPageSplCase)
                            {
                                //InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
                                //if (layoutData == nil)
                                //{
                                //	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData");
                                //	break;
                                //}
                                
                                IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
                                if (document == nil)
                                {
                                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No document");
                                    break;
                                }
                                IDataBase* database = ::GetDataBase(document);
                                if(!database)
                                {
                                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No database");
                                    break;
                                }
                                
                                
                                bool16 result = this->getCurrentPage(pageRef, spreadUIDRef);
                                if(result == kFalse)
                                    break;
                                
                                InterfacePtr<IPageList> iPageList(document,UseDefaultIID());
                                if(iPageList == nil)
                                {
                                    //CA("iPageList == nil");
                                    break;
                                }
                                
                                int32 bringSpreadToFrontIndex = 0;
                                
                                int32 pageToInsertAt = 0;
                                PageType pageType = iPageList->GetPageType(pageRef.GetUID()) ;
                                if(pageType == kLeftPage)
                                {
                                    //CA("pageType == kLeftPage");
                                    pageToInsertAt = 1;
                                }
                                else if(pageType == kRightPage)
                                {
                                    /*UID pageUID = iPageList->GetNthPageUID(addPageSplCase_pageIndex);
                                     if(pageUID == kInvalidUID)
                                     {
                                     ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");
                                     break;
                                     }
                                     
                                     InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
                                     if (iSpreadList==nil)
                                     {
                                     ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");
                                     break;
                                     }
                                     
                                     int32 pageCount = 0;
                                     for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
                                     {
                                     UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
                                     spreadUIDRef = temp_spreadUIDRef;
                                     
                                     InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
                                     if(!spread)
                                     {
                                     ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");
                                     break;
                                     }
                                     int numPages=spread->GetNumPages();
                                     pageCount +=  numPages;
                                     if(pageCount > addPageSplCase_pageIndex)
                                     {
                                     ++numSp;
                                     UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
                                     spreadUIDRef = temp_spreadUIDRef;
                                     
                                     bringSpreadToFrontIndex = numSp;
                                     break;
                                     }
                                     }*/
                                    
                                    //CA("pageType == kRightPage");
                                    InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
                                    if (iSpreadList==nil)
                                    {
                                        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");
                                        break;
                                    }
                                    
                                    UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(++addPageSplCase_SpreadIndex));
                                    spreadUIDRef = temp_spreadUIDRef;
                                    
                                    bringSpreadToFrontIndex = addPageSplCase_SpreadIndex;
                                    pageToInsertAt = 0;
                                }
                                else if(pageType == kUnisexPage)
                                {
                                    //CA("pageType == kUnisexPage");
                                    InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)document,UseDefaultIID());
                                    if (iSpreadList==nil)
                                    {
                                        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::iSpreadList==nil");
                                        break;
                                    }
                                    
                                    UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(++addPageSplCase_SpreadIndex));
                                    spreadUIDRef = temp_spreadUIDRef;
                                    
                                    bringSpreadToFrontIndex = addPageSplCase_SpreadIndex;
                                    pageToInsertAt = 0;
                                }
                                
                                //ProductSpray ps;
                                
                                /*PMString temp("addPageSplCase_SpreadIndex = ");
                                 temp.AppendNumber(addPageSplCase_SpreadIndex);
                                 temp.Append(", pageToInsertAt");
                                 temp.AppendNumber(pageToInsertAt);
                                 CA(temp);*/
                                
                                ErrorCode err = CreatePages(document,spreadUIDRef,1,pageToInsertAt,kTrue);
                                if(err == kFailure)
                                {
                                    //CA("err == kFailure");
                                    return;
                                }
                                
                                CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
                                //CA("added succesfully");
                                /*++addPageSplCase_pageIndex;*/
                                if(pageType == kLeftPage)
                                {
                                    ++addPageSplCase_pageIndexPerSpread;
                                }
                                else if(pageType == kRightPage)
                                {
                                    addPageSplCase_pageIndexPerSpread = 0;
                                }
                                else if(pageType == kUnisexPage)
                                {
                                    addPageSplCase_pageIndexPerSpread = 0;
                                }
                                
                                pageRef = UIDRef::gNull;
                                spreadUIDRef = UIDRef::gNull;
                                
                                bool16 result2 = this->getCurrentPage(pageRef, spreadUIDRef);
                                if(result2 == kFalse)
                                    break;
                                
                                
                                InterfacePtr<IMasterPage> ptrIMasterPage(pageRef, UseDefaultIID());
                                if(ptrIMasterPage != NULL)
                                {
                                    ptrIMasterPage->SetMasterPageData/*SetMasterPageUID*/(addPageSplCase_MasterSpreadUIDOfTemplate);	//----CS5--
                                }
                                
                                
                                InterfacePtr<ITransform> transform(pageRef, UseDefaultIID());
                                if (!transform) {//CA("!transform ");
                                    break;
                                }
                                
                                InterfacePtr<IMargins> margins(transform, IID_IMARGINS);
                                // Note it's OK if the page does not have margins.
                                if (margins) {
                                    //CA(" before margins->SetMargins");
                                    margins->SetMargins(addPageSplCase_marginBBox.Left(), addPageSplCase_marginBBox.Top(), addPageSplCase_marginBBox.Right(), addPageSplCase_marginBBox.Bottom());
                                    //CA(" After margins->SetMargins");
                                }
                                
                                InterfacePtr<IColumns> ptrIColumns(transform, IID_ICOLUMNS);
                                // Note it's OK if the page does not have margins.
                                if (ptrIColumns) {//CA("before SetColumns");
                                    ptrIColumns->SetColumns(addPageSplCase_columns);
                                    //CA("After SetColumns");
                                }
                                
                                //IControlView* fntView = Utils<ILayoutUIUtils>()->QueryFrontView(); //og
                                InterfacePtr<IControlView> fntView(Utils<ILayoutUIUtils>()->QueryFrontView()); // changed by avinash
                                if (fntView == nil)
                                {
                                    //CA("The front view is nil.");
                                    break;
                                }
                                
                                //Apsiva Test
                                InterfacePtr<IGeometry> spreadGeo(spreadUIDRef, UseDefaultIID());
                                if (!spreadGeo) {
                                    //CA("!spreadGeo");
                                    break;
                                }
                                InterfacePtr<ICommand> showSprdCmd(Utils<ILayoutUIUtils>()->MakeScrollToSpreadCmd(fntView, spreadGeo, kTrue));
                                CmdUtils::ProcessCommand(showSprdCmd);
                                
                                if (CmdUtils::ProcessCommand(showSprdCmd) != kSuccess) {
                                    //CA("MakeScrollToSpreadCmd failed");
                                    break;
                                }
                                
                                //InterfacePtr<ILayoutControlData> layoutData1(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
                                //if (layoutData1 == nil)
                                //{
                                //	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData1");
                                //	break;
                                //}
                                
                                //IDocument* document1 = layoutData1->GetDocument();
                                //if (document1 == nil)
                                //{
                                //	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No document1");
                                //	break;
                                //}
                                
                                //InterfacePtr<IPageList> iPageList1(document1,UseDefaultIID());
                                //if(iPageList1 == nil)
                                //{
                                //	CA("iPageList1 == nil");
                                //	break;
                                //}
                                
                                ///*UID pageUID = iPageList1->GetNthPageUID(addPageSplCase_pageIndex);
                                //if(pageUID == kInvalidUID)
                                //{
                                //	ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::getCurrentPage::No pageUID");		
                                //	break;
                                //}	*/
                                
                                //int32 InDESC = iPageList1->GetPageIndex(pageRef.GetUID());
                                //PMString ASD("InDESC : ");
                                //ASD.AppendNumber(InDESC);
                                //CA(ASD);
                                
                                
                                /*InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
                                 if(!spread)
                                 {
                                 ptrIAppFramework->LogDebug("AP7_ProductFinder::ProductSpray::startSpraying::!spread");						
                                 return;
                                 }
                                 
                                 UID pageUID = spread->GetNthPageUID(addPageSplCase_pageIndexPerSpread);*/
                                pageUidList.push_back(/*pageUID*/pageRef.GetUID());
                            }
                            else
                            {
                                //CA("Adding new page");
                                Utils<ILayoutUIUtils>()->AddNewPage();
                                PageCount= PageCount+1;
                                //CA_NUM("New page added and page count is : ", PageCount);
                                
                                if(fFaltuCase)
                                    fFaltuCase = kFalse;
                                
                                IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
                                if(fntDoc==nil)
                                {
                                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::fntDoc==nil");	
                                    return ;
                                }
                                IDataBase* database = ::GetDataBase(fntDoc);
                                if(database==nil)
                                {
                                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::database==nil");			
                                    return ;
                                }
                                InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
                                if (iSpreadList==nil)
                                {
                                    ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::iSpreadList==nil");				
                                    return ;
                                }
                                
                                /*UID pageUID;
                                 UIDRef pageRef = UIDRef::gNull;
                                 UIDRef spreadUIDRef = UIDRef::gNull;*/
                                
                                for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
                                {
                                    if( (iSpreadList->GetSpreadCount()-1) > numSp )
                                    {
                                        continue;
                                    }
                                    UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
                                    spreadUIDRef = temp_spreadUIDRef;
                                    
                                    InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
                                    if(!spread)
                                    {
                                        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::!spread");						
                                        return ;
                                    }
                                    int numPages=spread->GetNumPages();
                                    pageUID = spread->GetNthPageUID(numPages-1);
                                    UIDRef temp_pageRef(database, pageUID);
                                    pageRef = temp_pageRef;
                                    
                                }
                                pageUidList.push_back(pageUID);
                            }
                            
                            
                            
                            /*gspreadUIDRef = spreadUIDRef;
                             gpageRef = pageRef;*/
                            
                            result = this->getMarginBounds(pageRef, PagemarginBoxBounds);
                            if(result == kFalse)
                            {
                                result = this->getPageBounds(pageRef, PagemarginBoxBounds);
                                if(result == kFalse)
                                    break;
                                
                            }
                            
                            NewLeft = PagemarginBoxBounds.Left()/* + HorizBoxSpacing*/;
                            NewTop = PagemarginBoxBounds.Top();
                            
                            idxVerticalCount = 0;
                            idxHorizontalCount = 0;
                            ProdBlockBoundList.clear();
                            
                            RightMarkAfterSprayForLeadingItem = NewLeft + box_Width;
                            
                        }
                        
                        
                    }
                    
                    
                    AlphabetArrayCount++;
                    
                    selectionManager->DeselectAll(nil);// deselect every active CSB
                    
                    //CA("Before Copy");
                    //layoutSelectionSuite->Select(FirstcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
                    layoutSelectionSuite->SelectPageItems(FirstcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//added	
                    
                    //copy the selected items
                    //CA("CopySelectedItems 3");
                    CopySelectedItems();
                    //CA("CopySelectedItems 3");
                    
                    UIDList SecondcopiedBoxUIDList;
                    result = getSelectedBoxIds(SecondcopiedBoxUIDList);
                    if(result == kFalse){
                        //CA("getSelectedBoxIds result == kFalse ");
                        break;
                    }
                    
                    int32 SecondcopiedBoxUIDListSize = SecondcopiedBoxUIDList.Length();
                    if(SecondcopiedBoxUIDListSize == 0){
                        //CA("SecondcopiedBoxUIDListSize == 0");					
                        continue;
                    }			
                    
                    
                    //PMString r3(" Before move NewLeft  :  ");
                    //r3.AppendNumber(NewLeft);
                    //r3.Append("\nNewTop :  ");
                    //r3.AppendNumber(NewTop);
                    //r3.Append("\ncurr idxHorizontalCount  :  ");
                    //r3.AppendNumber(idxHorizontalCount);
                    //r3.Append("\nidxVerticalCount  :  ");
                    //r3.AppendNumber(idxVerticalCount);
                    //CA(r3);
                    
                    //ptrIAppFramework->LogDebug(r3);
                    
                    PBPMPoint moveToPoints(NewLeft, NewTop);	
                    moveBoxes(SecondcopiedBoxUIDList, moveToPoints);
                    
                    //CA("after moveBoxes");
                    
                    vectorCopiedBoxBoundsBforeSpray.clear();
                    CopiedItemMaxBoxBoundsBforeSpray = kZeroRect;
                    result = kFalse;
                    result = this->getMaxLimitsOfBoxes(SecondcopiedBoxUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);
                    
                    
                    
                    double SecondItemId = *it1;
                    
                    for(int i=0; i<SecondcopiedBoxUIDListSize; i++)
                    {
                        tagList=itagReader->getTagsFromBox(SecondcopiedBoxUIDList.GetRef(i));
                        if(tagList.size()<=0)//This can be a Tagged Frame
                        {	
                            //CA(" tagList.size()<=0 ");
                            if(DataSprayerPtr->isFrameTagged(SecondcopiedBoxUIDList.GetRef(i)))
                            {	
                                tagList.clear();
                                tagList=itagReader->getFrameTags(SecondcopiedBoxUIDList.GetRef(i));
                                if(tagList.size()==0)//Ordinary box
                                {					
                                    continue ;
                                }	
                                
                                for(int32 j=0; j <tagList.size(); j++)
                                {
                                    XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
                                    if(tagList[j].whichTab == 4 || (isSprayItemPerFrameFlag1 == 2))
                                    {
                                        ////CA("Item Tag Found");
                                        //ItemFrameUIDList.Append(selectUIDList.GetRef(i).GetUID());
                                        //break; // break out from for loop
                                        if(tagList[j].imgFlag == 1)
                                        {
                                            PMString attributeValue;
                                            attributeValue.AppendNumber(PMReal(SecondItemId));
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
                                            
                                            attributeValue.clear();
                                            if(isSprayItemPerFrameFlag1 != 2)
                                            {
                                                attributeValue.AppendNumber(PMReal(pNodeDataList[sprayedProductIndex].getPubId()));
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue));
                                            }
                                            
                                        }
                                        else if(isSprayItemPerFrameFlag1 == 2)
                                        {
                                            PMString attributeValue;
                                            attributeValue.AppendNumber(PMReal(FirstItemId));
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue));
                                            
                                        }
                                        else
                                        {
                                            PMString attributeValue;
                                            attributeValue.AppendNumber(PMReal(SecondItemId));
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childId"),WideString(attributeValue));
                                            
                                            PMString childTag("1");
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childTag"),WideString(childTag));
                                            
                                            if(tagList[j].elementId == -803)
                                            {
                                                attributeValue.Clear();
                                                if(AlphabetArrayCount < 26)
                                                    attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
                                                else
                                                    attributeValue.Append("a");
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                            }
                                            else if(tagList[j].elementId == -827)
                                            {
                                                attributeValue.Clear();
                                                int32 numberKey = AlphabetArrayCount + 1;
                                                
                                                attributeValue.AppendNumber(numberKey);
                                                
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                            }
                                        }
                                        PMString attributeValue;								
                                        attributeValue.AppendNumber(-777);
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
                                        
                                    }
                                }
                            }
                            else
                            {
                                // This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
                                //CA(" else DataSprayerPtr->isFrameTagged");
                                InterfacePtr<IHierarchy> iHier(SecondcopiedBoxUIDList.GetRef(i), UseDefaultIID());
                                if(!iHier)
                                {
                                    //CA(" !iHier >> Continue ");
                                    continue;
                                }
                                UID kidUID;				
                                int32 numKids=iHier->GetChildCount();				
                                IIDXMLElement* ptr = NULL;
                                
                                for(int j=0;j<numKids;j++)
                                {
                                    //CA("Inside For Loop");
                                    bool16 Flag12 =  kFalse;
                                    kidUID=iHier->GetChildUID(j);
                                    UIDRef boxRef(SecondcopiedBoxUIDList.GetDataBase(), kidUID);			
                                    TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
                                    if(NewList.size()<=0)//This can be a Tagged Frame
                                    {
                                        
                                        
                                        NewList.clear();
                                        NewList=itagReader->getFrameTags(SecondcopiedBoxUIDList.GetRef(i));
                                        XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
                                        XMLReference NewListXMLRef = NewList[j].tagPtr->GetXMLReference();
                                        if(NewList.size()==0)//Ordinary box
                                        {					
                                            continue ;
                                        }	
                                        
                                        for(int32 j=0; j <NewList.size(); j++)
                                        {
                                            if(NewList[j].whichTab == 4 || (isSprayItemPerFrameFlag1 == 2))
                                            {
                                                if(NewList[j].imgFlag == 1)
                                                {
                                                    PMString attributeValue;
                                                    attributeValue.AppendNumber(PMReal(SecondItemId));
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
                                                    
                                                    attributeValue.clear();
                                                    if(isSprayItemPerFrameFlag1 != 2)
                                                    {
                                                        attributeValue.AppendNumber(PMReal(pNodeDataList[sprayedProductIndex].getPubId()));
                                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("isAutoResize"),WideString(attributeValue));
                                                    }
                                                    
                                                }
                                                else if(isSprayItemPerFrameFlag1 == 2)
                                                {
                                                    PMString attributeValue;
                                                    attributeValue.AppendNumber(PMReal(FirstItemId));
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("parentTypeID"),WideString(attributeValue));
                                                    
                                                }
                                                else
                                                {
                                                    PMString attributeValue;
                                                    attributeValue.AppendNumber(PMReal(SecondItemId));
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childId"),WideString(attributeValue));
                                                    
                                                    PMString childTag("1");
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childTag"),WideString(childTag));
                                                    if(NewList[j].elementId == -803)
                                                    {
                                                        attributeValue.Clear();
                                                        if(AlphabetArrayCount < 26)
                                                            attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
                                                        else
                                                            attributeValue.Append("a");
                                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                                    }
                                                    else if(tagList[j].elementId == -827)
                                                    {
                                                        attributeValue.Clear();
                                                        int32 numberKey = AlphabetArrayCount + 1;
                                                        
                                                        attributeValue.AppendNumber(numberKey);
                                                        
                                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                                    }
                                                }
                                                PMString attributeValue;								
                                                attributeValue.AppendNumber(-777);
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
                                                
                                            }
                                        }
                                    }
                                    else
                                    {
                                        for(int32 j=0; j <NewList.size(); j++)
                                        {											
                                            XMLReference NewListXMLRef = NewList[j].tagPtr->GetXMLReference();
                                            if(NewList[j].whichTab == 4 || (isSprayItemPerFrameFlag1 == 2))
                                            {
                                                if(NewList[j].imgFlag == 1)
                                                {
                                                    PMString attributeValue;
                                                    attributeValue.AppendNumber(PMReal(SecondItemId));
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
                                                    
                                                    attributeValue.clear();
                                                    if(isSprayItemPerFrameFlag1 != 2)
                                                    {
                                                        attributeValue.AppendNumber(PMReal(pNodeDataList[sprayedProductIndex].getPubId()));
                                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("isAutoResize"),WideString(attributeValue));
                                                    }
                                                    
                                                    
                                                }
                                                else if(isSprayItemPerFrameFlag1 == 2)
                                                {
                                                    PMString attributeValue;
                                                    attributeValue.AppendNumber(PMReal(FirstItemId));
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("parentTypeID"),WideString(attributeValue));
                                                    
                                                }
                                                else
                                                {
                                                    PMString attributeValue;
                                                    attributeValue.AppendNumber(PMReal(SecondItemId));
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childId"),WideString(attributeValue));
                                                    
                                                    PMString childTag("1");
                                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("childTag"),WideString(childTag));
                                                    
                                                    if(NewList[j].elementId == -803)
                                                    {
                                                        attributeValue.Clear();
                                                        if(AlphabetArrayCount < 26)
                                                            attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
                                                        else
                                                            attributeValue.Append("a");
                                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("rowno"),WideString(attributeValue));//Cs4
                                                    }
                                                    else if(tagList[j].elementId == -827)
                                                    {
                                                        attributeValue.Clear();
                                                        int32 numberKey = AlphabetArrayCount + 1;
                                                        
                                                        attributeValue.AppendNumber(numberKey);
                                                        
                                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                                    }
                                                    
                                                }
                                                PMString attributeValue;								
                                                attributeValue.AppendNumber(-777);
                                                Utils<IXMLAttributeCommands>()->SetAttributeValue(NewListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
                                                
                                            }
                                        }
                                    }
                                    //------------
                                    for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
                                    {
                                        NewList[tagIndex].tagPtr->Release();
                                    }
                                    
                                    if(Flag12)
                                        break;
                                }
                            }
                        }
                        else
                        {
                            for(int32 j=0; j <tagList.size(); j++)
                            {
                                XMLReference tagListXMLRef = tagList[j].tagPtr->GetXMLReference();
                                if(tagList[j].whichTab == 4 || (isSprayItemPerFrameFlag1 == 2))
                                {
                                    if(tagList[j].imgFlag == 1)
                                    {
                                        PMString attributeValue;
                                        attributeValue.AppendNumber(PMReal(SecondItemId));
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue)); //Cs4
                                        
                                        attributeValue.clear();
                                        if(isSprayItemPerFrameFlag1 != 2)
                                        {
                                            attributeValue.AppendNumber(PMReal(pNodeDataList[sprayedProductIndex].getPubId()));
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("isAutoResize"),WideString(attributeValue));
                                        }
                                        
                                    }
                                    else if(isSprayItemPerFrameFlag1 == 2)
                                    {
                                        PMString attributeValue;
                                        attributeValue.AppendNumber(PMReal(FirstItemId));
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("parentTypeID"),WideString(attributeValue));
                                        
                                    }
                                    else
                                    {
                                        PMString attributeValue;
                                        attributeValue.AppendNumber(PMReal(SecondItemId));
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("typeId"),WideString(attributeValue)); //Cs4
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childId"),WideString(attributeValue));
                                        
                                        PMString childTag("1");
                                        Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("childTag"),WideString(childTag));
                                        
                                        if(tagList[j].elementId == -803)
                                        {
                                            attributeValue.Clear();
                                            if(AlphabetArrayCount < 26)
                                                attributeValue.Append(AlphabetArray[AlphabetArrayCount]);
                                            else
                                                attributeValue.Append("a");
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4										}
                                        }
                                        else if(tagList[j].elementId == -827)
                                        {
                                            attributeValue.Clear();
                                            int32 numberKey = AlphabetArrayCount + 1;
                                            
                                            attributeValue.AppendNumber(numberKey);
                                            
                                            Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("rowno"),WideString(attributeValue)); //Cs4
                                        }
                                    }
                                    PMString attributeValue;								
                                    attributeValue.AppendNumber(-777);
                                    Utils<IXMLAttributeCommands>()->SetAttributeValue(tagListXMLRef, WideString("colno"),WideString(attributeValue)); //Cs4
                                    
                                }
                            }
                        }
                        
                        for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
                        {
                            tagList[tagIndex].tagPtr->Release();
                        }
                    }
                    
                    //CA("Before Spraying Second frame onwards");
                    ////// Spraying Second Frame onwords.
                    
                    if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/)
                    {
                        //			CA("1");					
                        DataSprayerPtr->setFlow(kTrue);
                    }
                    else
                    {
                        //			CA("2");
                        DataSprayerPtr->setFlow(kFalse);
                    }
                    //	DataSprayerPtr->setFlow(isItemHorizontalFlow);
                    //	DataSprayerPtr->setFlow(iSSSprayer->getHorizontalFlowForAllImageSprayFlag());
                    
                    Parameter requiredParameters;
                    
                    requiredParameters.idxHorizontalCount = idxHorizontalCount; 
                    requiredParameters.idxVerticalCount = idxVerticalCount;
                    
                    
                    
                    vector<DynFrameStruct>::iterator itrDSDynFrameStruct;
                    itrDSDynFrameStruct = requiredParameters.ProdBlockBoundList.begin();
                    requiredParameters.ProdBlockBoundList.insert (itrDSDynFrameStruct,ProdBlockBoundList.begin(),ProdBlockBoundList.end());
                    
                    requiredParameters.RightMarkAfterSprayForLeadingItem = RightMarkAfterSprayForLeadingItem;
                    requiredParameters.BottomMarkAfterSprayForLeadingItem = BottomMarkAfterSprayForNonSprayItemPerFrame;
                    requiredParameters.PagemarginBoxBounds = PagemarginBoxBounds;
                    requiredParameters.ItemStencilMaxBounds = ItemStencilMaxBounds;
                    
                    requiredParameters.WidthOfSprayItemPerFrames = newLeadingSprayedTempFramesForMaxBounds.Right() - newLeadingSprayedTempFramesForMaxBounds.Left();
                    
                    VecParameter vecParameters;
                    vecParameters.push_back(requiredParameters);
                    
                    DataSprayerPtr->setDifferentParameters(vecParameters);
                    
                    
                    
                    
                    DataSprayerPtr->ClearNewImageFrameList();
                    
                    ICommandSequence *seq=CmdUtils::BeginCommandSequence();
                    
                    for(int i=0; i<SecondcopiedBoxUIDListSize; i++)
                    { 
                        PMString allInfo;	
                        //CA("before calling itagReader->getTagsFromBox()");
                        tagList=itagReader->getTagsFromBox(SecondcopiedBoxUIDList.GetRef(i));
                        //CA("after calling itagReader->getTagsFromBox()");
                        if(tagList.size()<=0)//This can be a Tagged Frame
                        {	
                            //CA(" tagList.size()<=0 ");
                            if(DataSprayerPtr->isFrameTagged(SecondcopiedBoxUIDList.GetRef(i)))
                            {	
                                //CA("isFrameTagged");
                                bool16 flaG = kFalse;		
                                
                                tagList.clear();
                                tagList=itagReader->getFrameTags(SecondcopiedBoxUIDList.GetRef(i));
                                
                                if(tagList.size()==0)//Ordinary box
                                {					
                                    continue ;
                                }
                                
                                //CA("Frame Tags Found");
                                InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
                                InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
                                if (!layoutSelectionSuite) {
                                    break;
                                }
                                
                                selectionManager->DeselectAll(nil); // deselect every active CSB
                                //layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
                                layoutSelectionSuite->SelectPageItems(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Added
                                
                                //	CA("Before sprayForTaggedBox");
                                DataSprayerPtr->sprayForTaggedBox(SecondcopiedBoxUIDList.GetRef(i));						
                            }
                            else
                            {
                                // This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
                                //CA(" else DataSprayerPtr->isFrameTagged");
                                InterfacePtr<IHierarchy> iHier(SecondcopiedBoxUIDList.GetRef(i), UseDefaultIID());
                                if(!iHier)
                                {
                                    //CA(" !iHier >> Continue ");
                                    continue;
                                }
                                UID kidUID;				
                                int32 numKids=iHier->GetChildCount();				
                                IIDXMLElement* ptr = NULL;
                                
                                for(int j=0;j<numKids;j++)
                                {
                                    //CA("Inside For Loop");
                                    kidUID=iHier->GetChildUID(j);
                                    UIDRef boxRef(SecondcopiedBoxUIDList.GetDataBase(), kidUID);			
                                    TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
                                    if(NewList.size()<=0)//This can be a Tagged Frame
                                    {
                                        if(DataSprayerPtr->isFrameTagged(boxRef))
                                        {	
                                            //CA("isFrameTagged(selectUIDList.GetRef(i))");
                                            DataSprayerPtr->sprayForTaggedBox(boxRef);				
                                        }
                                        continue;
                                    }
                                    DataSprayerPtr->sprayForThisBox(boxRef, NewList);
                                    //------------
                                    for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
                                    {
                                        NewList[tagIndex].tagPtr->Release();
                                    }
                                }
                            }
                            //CA("Before Continue");
                            continue;
                        }
                        
                        bool16 flaG = kFalse;			
                        //CA("Before sprayForThisBox");
                        DataSprayerPtr->sprayForThisBox(SecondcopiedBoxUIDList.GetRef(i), tagList);
                        //CA("After sprayForThisBox");
                        if(flaG)
                        {			
                            InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
                            InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
                            if (!layoutSelectionSuite) {
                                break;
                            }
                            selectionManager->DeselectAll(nil); // deselect every active CSB
                            //layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
                            layoutSelectionSuite->SelectPageItems(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Added
                            
                            TagList NewTagList;
                            NewTagList=itagReader->getTagsFromBox(SecondcopiedBoxUIDList.GetRef(i));
                            
                            if(NewTagList.size()==0)//Ordinary box
                            {
                                return ;
                            }
                            //------------
                            for(int32 tagIndex = 0 ; tagIndex < NewTagList.size() ; tagIndex++)
                            {
                                NewTagList[tagIndex].tagPtr->Release();
                            }
                        }
                        //Apsiva9
                        for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
                        {
                            tagList[tagIndex].tagPtr->Release();
                        }
                    }
                    //CA("14");					
                    moveAutoResizeBoxAfterSpray(SecondcopiedBoxUIDList, vectorCopiedBoxBoundsBforeSpray);
                    //CA("15");
                    CmdUtils::EndCommandSequence(seq);
                    
                    UIDList newTempUIDList(SecondcopiedBoxUIDList);
                    VectorNewImageFrameUIDList newAddedFrameUIDListAfterSpray = DataSprayerPtr->getNewImageFrameList();
                    
                    if(newAddedFrameUIDListAfterSpray.size() > 0)
                    {  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
                        for(int q=0; q < newAddedFrameUIDListAfterSpray.size(); q++)
                        {					
                            newTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
                        }						
                    }
                    
                    ItemFramesStencilMaxBounds = kZeroRect;
                    result = getMaxLimitsOfBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                    if(result == kFalse)
                        break;
                    
                    PMReal LONS = ItemFramesStencilMaxBounds.Left();
                    PMReal RONS = ItemFramesStencilMaxBounds.Right();
                    PMReal TONS = ItemFramesStencilMaxBounds.Top();
                    PMReal BONS = ItemFramesStencilMaxBounds.Bottom();
                    
                    
                    if(idxHorizontalCount < 0)
                        idxHorizontalCount = 0;
                    if(idxVerticalCount < 0)
                        idxVerticalCount = 0;
                    
                    DynFrameStruct CurrentFameStruct;
                    CurrentFameStruct.HorzCnt = idxHorizontalCount;
                    CurrentFameStruct.VertCnt = idxVerticalCount;
                    CurrentFameStruct.BoxBounds = ItemFramesStencilMaxBounds;
                    CurrentFameStruct.isLastHorzFrame = kFalse;
                    
                    //idxVerticalCount++;
                    if(isItemHorizontalFlow1 == kFalse)
                        idxVerticalCount++;
                    
                    CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
                    
                    FrameBoundsList tmpFrameBoundsList;
                    VecReturnParameter returnParameterVec;
                    
                    
                    int32 tempHorizontalCnt = 0;
                    int32 tempVerticalCount = 0;
                    
                    
                    if(DataSprayerPtr->getIsColumnChange())
                        isColumnChange = kTrue;
                    
                    if(isColumnChange)
                    {						
                        returnParameterVec = DataSprayerPtr->getReturnParameter();
                        
                        if(returnParameterVec.size() > 0)
                        {
                            for(int32 returnParaIndex = 0; returnParaIndex < returnParameterVec.size(); returnParaIndex++)
                            {
                                ReturnParameter rp = returnParameterVec[returnParaIndex];
                                tempHorizontalCnt = rp.idxHorizontalCount;
                                tempVerticalCount = rp.idxVerticalCount;
                                tmpFrameBoundsList.push_back(rp.CurrentDynFrameStruct);
                            }
                            
                            DataSprayerPtr->ClearReturnParameter();
                        }
                        DataSprayerPtr->resetIsColumnChange();
                        
                        UIDList tempUIDList(SecondcopiedBoxUIDList);
                        tempUIDList.Clear();
                        
                        if(returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size() > 0)
                        {  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
                            for(int q=0; q < returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size(); q++)
                            {					
                                tempUIDList.Append(returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn[q]);
                            }						
                        }
                        
                        /*ItemFramesStencilMaxBounds = kZeroRect;
                         result = getMaxLimitsOfBoxes(tempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                         if(result == kFalse)
                         break;*/
                    }
                    
                    //		if(isItemHorizontalFlow == kFalse)
                    
                    if(isItemHorizontalFlow1/*iSSSprayer->getHorizontalFlowForAllImageSprayFlag()*/ == kFalse)
                    {
                        //CA("isItemHorizontalFlow == kFalse");
                        if (PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom())
                        {
                            //CA(" For Vertical Flow.......PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom() ");
                            //CA("Going out of Bottom Margin ");
                            
                            LeftMarkAfterSpray = /*ItemFramesStencilMaxBounds.Right()*/MaxRightMarkSprayHZ + HorizBoxSpacing;
                            TopMarkAfterSpray = TopMark;
                            BottomMarkAfterSpray = TopMark - VerticalBoxSpacing;
                            
                            /*PMString ASD("LeftMarkAfterSpray : " );
                             ASD.AppendNumber(LeftMarkAfterSpray);
                             ASD.Append("   TopMarkAfterSpray : ");
                             ASD.AppendNumber(TopMarkAfterSpray);
                             CA(ASD);*/
                            
                            /*if((ItemFramesStencilMaxBounds.Right()+ box_Width) > PagemarginBoxBounds.Right())
                             {
                             verticalSprayBottomFrameFlag = kTrue;
                             CurrentFameStruct.isLastHorzFrame = kTrue;
                             countVal++;
                             ProdBlockBoundList.push_back(CurrentFameStruct);
                             continue;
                             }
                             else*/
                            {
                                
                                selectionManager->DeselectAll(nil); // deselect every active CSB
                                //layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented BY sachin sharma on 2/07/07
                                layoutSelectionSuite->SelectPageItems(/*SecondcopiedBoxUIDList*/newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
                                if(ItemFramesStencilMaxBounds.Top() != TopMark)
                                {
                                    //CA(" Going For delete");
                                    deleteThisBoxUIDList(newTempUIDList);
                                    
                                    if(isColumnChange)
                                    {
                                        UIDList tempUIDList(SecondcopiedBoxUIDList);
                                        tempUIDList.Clear();
                                        
                                        if(returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size() > 0)
                                        {  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
                                            for(int q=0; q < returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size(); q++)
                                            {					
                                                tempUIDList.Append(returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn[q]);
                                            }						
                                        }
                                        deleteThisBoxUIDList(tempUIDList);
                                        tmpFrameBoundsList.clear();
                                        
                                        if(returnParameterVec[0].fPageAdded)
                                        {											
                                            ProdBlockBoundList.clear();
                                            idxHorizontalCount = 0;
                                            idxVerticalCount = 0;
                                            
                                            InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
                                            if (layoutData == nil)
                                            {
                                                continue;
                                            }											
                                            
                                            IDocument* document = layoutData->GetDocument();
                                            if (document == nil)
                                            {											
                                                continue;
                                            }
                                            
                                            IDataBase* database = ::GetDataBase(document);
                                            if(database==nil)
                                            {
                                                //CA("AP46_ProductFinder::SPSelectionObserver::AddOrDeleteSpreads::No database");
                                                continue;
                                            }
                                            
                                            UIDList toBeDeletedPagesUIDList(database);
                                            
                                            int32 noOfPagesToBeDeleted = static_cast<int32>(returnParameterVec[0].PageUIDList.size());
                                            for(int32 pageIndex =0;pageIndex < noOfPagesToBeDeleted;pageIndex++)
                                            {
                                                toBeDeletedPagesUIDList.Append(returnParameterVec[0].PageUIDList[pageIndex]);
                                            }
                                            InterfacePtr<ICommand> iDeletePageCmd(CmdUtils::CreateCommand(kDeletePageCmdBoss));
                                            if (iDeletePageCmd == nil)
                                            {
                                                continue;
                                            }
                                            
                                            InterfacePtr<IBoolData> iBoolData(iDeletePageCmd,UseDefaultIID());
                                            if (iBoolData == nil){
                                                
                                                continue;
                                            }
                                            iBoolData->Set(kFalse);
                                            
                                            iDeletePageCmd->SetItemList(toBeDeletedPagesUIDList);
                                            // process the command
                                            ErrorCode status1 = CmdUtils::ProcessCommand(iDeletePageCmd);
                                        }								
                                        
                                        
                                        returnParameterVec.clear();
                                        isColumnChange = kFalse;
                                    }
                                    --it1;
                                    --AlphabetArrayCount;
                                    //idxHorizontalCount++;
                                    idxVerticalCount = 0;
                                    continue;
                                }
                                
                                //PBPMPoint NewmoveToPoints(LeftMarkAfterSpray, TopMarkAfterSpray);	
                                //moveBoxes(SecondcopiedBoxUIDList, NewmoveToPoints);
                                //CA("After Moving Boxes ");
                                ItemFramesStencilMaxBounds = kZeroRect;
                                result = getMaxLimitsOfBoxes(newTempUIDList/*SecondcopiedBoxUIDList*/, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                                if(result == kFalse)
                                    break;
                                
                                LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
                                BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                                TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                                RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                                
                                selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);
                            }
                            
                        }
                        else
                        {
                            // Right now only for Vertical Flow
                            //for Vertical Flow
                            //CA(" Else For Vertical Flow.......PagemarginBoxBounds.Bottom() < ItemFramesStencilMaxBounds.Bottom() ");
                            if((PagemarginBoxBounds.Bottom()- ItemFramesStencilMaxBounds.Bottom()) < (BottomMark - /*TopMark*/TopMarkOfFrame) && (ItemFramesStencilMaxBounds.Right()+ box_Width >  PagemarginBoxBounds.Right()) && verticalSprayBottomFrameFlag == kTrue )
                            {  // if there is no space for next frame below 
                                //CA(" No NEXT frame on Bottom side");
                                LeftMarkAfterSpray = (MaxRightMarkSprayHZ) + HorizBoxSpacing;
                                BottomMarkAfterSpray = TopMark - VerticalBoxSpacing;
                                TopMarkAfterSpray = TopMark;
                                RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                                
                                if((ItemFramesStencilMaxBounds.Right()+ box_Width) > PagemarginBoxBounds.Right())
                                {
                                    //CA("Checking Right");
                                    verticalSprayBottomFrameFlag = kTrue;
                                    CurrentFameStruct.isLastHorzFrame = kTrue;
                                    countVal++;
                                    ProdBlockBoundList.push_back(CurrentFameStruct);
                                    
                                    if(isColumnChange)
                                    {
                                        
                                        if(returnParameterVec[0].fPageAdded)
                                            ProdBlockBoundList.clear();
                                        
                                        if(tmpFrameBoundsList.size() > 0)
                                        {
                                            for(int32 index = 0; index  < tmpFrameBoundsList.size() ; index++)
                                            {
                                                ProdBlockBoundList.push_back(tmpFrameBoundsList[index]);
                                            }
                                        }
                                        
                                        idxHorizontalCount = tempHorizontalCnt;
                                        idxVerticalCount = tempVerticalCount;
                                        
                                        tmpFrameBoundsList.clear();
                                        
                                        if(returnParameterVec[0].fPageAdded)
                                        {
                                            UID newpageUID = returnParameterVec[0].PageUIDList[0];
                                            
                                            IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
                                            if(fntDoc==nil)
                                            {
                                                ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::fntDoc==nil");	
                                                return ;
                                            }
                                            IDataBase* database = ::GetDataBase(fntDoc);
                                            if(database==nil)
                                            {
                                                ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::database==nil");			
                                                return ;
                                            }
                                            UIDRef temp_pageRef(database, newpageUID);
                                            
                                            result = this->getMarginBounds(temp_pageRef, PagemarginBoxBounds);
                                            if(result == kFalse)
                                            {
                                                result = this->getPageBounds(temp_pageRef, PagemarginBoxBounds);
                                                if(result == kFalse)
                                                    break;
                                                
                                            }
                                            
                                            pageUidList.push_back(newpageUID);
                                            PageCount++;
                                            
                                            idxVerticalCount = 1;
                                            idxHorizontalCount = 0;
                                            
                                            
                                        }									
                                        
                                        UIDList tempUIDList(SecondcopiedBoxUIDList);
                                        tempUIDList.Clear();
                                        
                                        if(returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size() > 0)
                                        {  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
                                            for(int q=0; q < returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size(); q++)
                                            {					
                                                tempUIDList.Append(returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn[q]);
                                            }						
                                        }
                                        
                                        ItemFramesStencilMaxBounds = kZeroRect;
                                        result = getMaxLimitsOfBoxes(tempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                                        if(result == kFalse)
                                            break;										
                                        
                                        returnParameterVec.clear();
                                    }
                                    
                                    if(newTempUIDList.size() > 0)
                                    {  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
                                        for(int q=0; q < newTempUIDList.size(); q++)
                                        {					
                                            AllFramesOfCurrentLeadingItemIncludingItsChildItemFrames.Append(newTempUIDList[q]);
                                        }										
                                    }
                                    
                                    
                                    continue;
                                }
                                else
                                {
                                    //CA("Else of Checking RIght");
                                    verticalSprayBottomFrameFlag = kFalse;
                                    if(countVal >= 1)
                                    {
                                        BottomMarkAfterSpray = PagemarginBoxBounds.Top() - VerticalBoxSpacing;
                                        TopMarkAfterSpray = PagemarginBoxBounds.Top();
                                        LeftMarkAfterSpray = ItemFramesStencilMaxBounds.Right() + HorizBoxSpacing;
                                        RightMarkAfterSpray = box_Width + ItemFramesStencilMaxBounds.Right();
                                        
                                    }
                                }
                                
                            }
                            else
                            {	
                                //CA("Verticale Flow");
                                verticalSprayBottomFrameFlag = kFalse;
                                
                                LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
                                BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                                TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                                RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                                
                                /*PMString ASD(" LeftMarkAfterSpray : ");
                                 ASD.AppendNumber(LeftMarkAfterSpray);
                                 ASD.Append("  BottomMarkAfterSpray: ");
                                 ASD.AppendNumber(BottomMarkAfterSpray);
                                 ASD.Append("TopMarkAfterSpray : ");
                                 ASD.AppendNumber(TopMarkAfterSpray);
                                 ASD.Append("RightMarkAfterSpray : ");
                                 ASD.AppendNumber(RightMarkAfterSpray);
                                 CA(ASD);*/
                            }
                            
                            if(MaxRightMarkSprayHZ < RightMarkAfterSpray)
                                MaxRightMarkSprayHZ = RightMarkAfterSpray;
                            
                            selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);
                        }
                    }
                    
                    // Added by Sunil FROM HERE
                    else
                    {
                        //CA("inside horizontal flow = ktrue..");
                        
                        if(BONS > PagemarginBoxBounds.Bottom())
                        {
                            //CA("new sparayed is going out of bottom margin");
                            
                            selectionManager->DeselectAll(nil);
                            
                            layoutSelectionSuite->SelectPageItems(newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
                            deleteThisBoxUIDList(newTempUIDList);
                            --it1;
                            --AlphabetArrayCount;
                            
                            isAutoResized_FrameDeleted = kTrue;							
                        }
                        
                        if(isAutoResized_FrameDeleted)
                        {
                            CCC++;
                            //CA("Frame is resized and deleted");
                            isAutoResized_FrameDeleted = kFalse;
                            
                            Temp_Right2 = 0.0;
                            for(int p = 0; p < ProdBlockBoundList.size(); p++)
                            {
                                Temp_Right2 = ProdBlockBoundList[p].BoxBounds.Right();
                                if(Temp_Right2 > RightOfPreviousColumn)
                                {
                                    //CA("Temp_Right > RightOfPreviousColumn");
                                    RightOfPreviousColumn = Temp_Right2;
                                }
                            }
                            
                            RightOfPreviousColumn = RightOfPreviousColumn + HorizBoxSpacing;
                            
                            left_latest = RightOfPreviousColumn;
                            if(PagemarginBoxBounds.RectIn(origMaxBoxBounds1))
                            {
                                //CA("Template in page");
                                top_latest = origMaxBoxBounds1.Top();
                            }
                            else
                            {
                                //CA("Template not in page");
                                top_latest = PagemarginBoxBounds.Top();
                            }
                            
                            newPageAdded = kFalse;
                            
                            if(left_latest + origMaxBoxBounds1.Width() > PagemarginBoxBounds.Right())
                            {
                                //CA("ders no space for another leading on right");
                                
                                this->addNewPageHere(PagemarginBoxBounds);
                                
                                left_latest = RightOfPreviousColumn = PagemarginBoxBounds.Left();
                                top_latest = PagemarginBoxBounds.Top();
                            }						
                            
                            continue;
                        }
                        else
                        {
                            //CA("isAutoResized_FrameDeleted == kFalse");
                            
                            ProdBlockBoundList.push_back(CurrentFameStruct);
                            
                            left_latest = RONS + HorizBoxSpacing;
                            top_latest = TONS;
                            
                            if(left_latest + ItemFramesStencilMaxBounds.Width() > RightOfPreviousColumn + origMaxBoxBounds1.Width())
                            {
                                CCC++;
                                //CA("qwe");
                                
                                if(BONS + ItemFramesStencilMaxBounds.Height() < PagemarginBoxBounds.Bottom())
                                {
                                    //CA("123");
                                    left_latest = RightOfPreviousColumn;
                                    top_latest = BONS + VerticalBoxSpacing;
                                }
                                else if(RightOfPreviousColumn + origMaxBoxBounds1.Width() + origMaxBoxBounds1.Width() < PagemarginBoxBounds.Right())
                                {
                                    //CA("456");
                                    
                                    newPageAdded = kFalse;
                                    
                                    Temp_Right2 = 0.0;
                                    for(int p = 0; p < ProdBlockBoundList.size(); p++)
                                    {
                                        Temp_Right2 = ProdBlockBoundList[p].BoxBounds.Right();
                                        if(Temp_Right2 > RightOfPreviousColumn)
                                        {
                                            //CA("Temp_Right > RightOfPreviousColumn");
                                            RightOfPreviousColumn = Temp_Right2;
                                        }
                                    }
                                    
                                    RightOfPreviousColumn = RightOfPreviousColumn + HorizBoxSpacing;
                                    
                                    left_latest = RightOfPreviousColumn;
                                    if(PagemarginBoxBounds.RectIn(origMaxBoxBounds1))
                                    {
                                        //CA("Template in page");
                                        top_latest = origMaxBoxBounds1.Top();
                                    }
                                    else
                                    {
                                        //CA("Template not in page");
                                        top_latest = PagemarginBoxBounds.Top();
                                    }
                                }
                                
                                else
                                {
                                    //CA("789");
                                    
                                    this->addNewPageHere(PagemarginBoxBounds);
                                    
                                    left_latest = RightOfPreviousColumn = PagemarginBoxBounds.Left();
                                    top_latest = PagemarginBoxBounds.Top();
                                }
                            }
                            
                            if(top_latest < BottomMarkAfterSprayForNonSprayItemPerFrame && CCC == 0)
                            {
                                //CA("asd");
                                left_latest = RightOfPreviousColumn;
                                top_latest = BONS + HorizBoxSpacing;
                            }
                        }
                        ItemFramesStencilMaxBounds = kZeroRect;
                        result = getMaxLimitsOfBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                        if(result == kFalse)
                            break;
                        
                        LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
                        BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                        TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                        RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                        
                        if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
                            MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
                        
                        //	selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);
                        /*}*/
                        
                        /*if (PagemarginBoxBounds.Right() < ItemFramesStencilMaxBounds.Right())
                         {
                         CA("Going out of Right Margin overflow ");
                         LeftMarkAfterSpray = LeftMark;
                         TopMarkAfterSpray = MaxBottomMarkSprayHZ + VerticalBoxSpacing;
                         RightMarkAfterSpray = LeftMark - HorizBoxSpacing;*/
                        
                        /*	PMString ASD("LeftMarkAfterSpray : " );
                         ASD.AppendNumber(LeftMarkAfterSpray);
                         ASD.Append("   TopMarkAfterSpray : ");
                         ASD.AppendNumber(TopMarkAfterSpray);
                         CA(ASD);*/
                        //if((ItemFramesStencilMaxBounds.Bottom() + box_Height) > PagemarginBoxBounds.Bottom())
                        //{
                        //	//CA("Inside Right MArgin --- Checking box height");
                        //	horizontalSprayBottomFrameFlag = kTrue;
                        //	countVal++;
                        //	continue;
                        
                        //}
                        //else
                        //{
                        //	selectionManager->DeselectAll(nil); // deselect every active CSB
                        //	//layoutSelectionSuite->Select(SecondcopiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);//Commented By Sachin sharma on 2/07/07
                        //	layoutSelectionSuite->SelectPageItems(newTempUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	//Added
                        //	if(ItemFramesStencilMaxBounds.Left() != LeftMark)
                        //	{
                        //		deleteThisBoxUIDList(newTempUIDList);
                        //		--it1;
                        //		--AlphabetArrayCount;
                        //		continue;
                        //	}
                        
                        ///*PBPMPoint NewmoveToPoints(LeftMarkAfterSpray, TopMarkAfterSpray);	
                        //moveBoxes(SecondcopiedBoxUIDList, NewmoveToPoints);
                        //CA("After Moving Boxes ");
                        //		ItemFramesStencilMaxBounds = kZeroRect;
                        //		result = getMaxLimitsOfBoxes(/*SecondcopiedBoxUIDList*/newTempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                        //		if(result == kFalse)
                        //			break;
                        
                        //		LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
                        //		BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                        //		TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                        //		RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                        
                        //		if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
                        //			MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
                        
                        //		selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);
                        //	}
                        
                        //}
                        //						else
                        //						{
                        //							if((PagemarginBoxBounds.Right()- ItemFramesStencilMaxBounds.Right()) < (ItemFramesStencilMaxBounds.Width()/*RightMark - LeftMark*/))
                        //							{  // if there is no space for next frame on right side 
                        //								CA("No Next Frame on Right Side ");
                        //								LeftMarkAfterSpray = LeftMark;
                        //								BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                        //								if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
                        //									MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
                        //
                        //								TopMarkAfterSpray = MaxBottomMarkSprayHZ + VerticalBoxSpacing;
                        //								RightMarkAfterSpray = LeftMark - HorizBoxSpacing;
                        //
                        //								//---------
                        //								/*PMString r("ItemFramesStencilMaxBounds.Bottom()  :  ");
                        //								r.AppendNumber(ItemFramesStencilMaxBounds.Bottom());
                        //								r.Append("\nItemFramesStencilMaxBounds.Top()  :  ");
                        //								r.AppendNumber(ItemFramesStencilMaxBounds.Top());
                        //								r.Append("\nbox_Height  :  ");
                        //								r.AppendNumber(box_Height);
                        //								r.Append("\nPagemarginBoxBounds.Bottom()  :  ");
                        //								r.AppendNumber(PagemarginBoxBounds.Bottom());
                        //								CA(r);*/
                        //								box_Height = ItemFramesStencilMaxBounds.Bottom() - ItemFramesStencilMaxBounds.Top();
                        //								if((ItemFramesStencilMaxBounds.Bottom() + box_Height) > PagemarginBoxBounds.Bottom())
                        //								{
                        //									//CA("Page AAD");
                        //									horizontalSprayBottomFrameFlag = kTrue;
                        //									countVal++;
                        //									ProdBlockBoundList.push_back(CurrentFameStruct);
                        //									continue;
                        //								}
                        //								else
                        //								{
                        //									CA("bottom ");
                        //									horizontalSprayBottomFrameFlag = kFalse;
                        //										
                        //									//BottomMarkAfterSpray = box_Height + ItemFramesStencilMaxBounds.Bottom();
                        //									//TopMarkAfterSpray = ItemFramesStencilMaxBounds.Bottom() + VerticalBoxSpacing;
                        //									//
                        //									//LeftMarkAfterSpray = PagemarginBoxBounds.Left();						//ItemFramesStencilMaxBounds.Left();
                        //									//RightMarkAfterSpray = PagemarginBoxBounds.Left() - HorizBoxSpacing;		//ItemFramesStencilMaxBounds.Right();
                        //
                        //									BottomMarkAfterSpray = ItemFramesStencilMaxBounds.Bottom();
                        //									TopMarkAfterSpray = ItemFramesStencilMaxBounds.Bottom() + VerticalBoxSpacing;
                        //									LeftMarkAfterSpray = newAllSprayedTempFramesForMaxBounds.Right();
                        //
                        //									if(PagemarginBoxBounds.RectIn(origMaxBoxBounds1))
                        //									{
                        //										if(ItemFramesStencilMaxBounds.Right() > origMaxBoxBounds1.Right())
                        //										{
                        //											CA("Bottom k baad ka IF");
                        //											RightMarkAfterSpray = newAllSprayedTempFramesForMaxBounds.Right();
                        //											if(ItemFramesStencilMaxBounds.Right() + ItemFramesStencilMaxBounds.Width() > newAllSprayedTempFramesForMaxBounds.Right())
                        //											{
                        //												RightMarkAfterSpray = newAllSprayedTempFramesForMaxBounds.Left();
                        //												LeftMarkAfterSpray  = newAllSprayedTempFramesForMaxBounds.Left(); 
                        //											}
                        //										}
                        //										else
                        //										{
                        //											CA("Bottom k baad ka ELSE");
                        //											RightMarkAfterSpray = LeftMark- HorizBoxSpacing;; //newAllSprayedTempFramesForMaxBounds.Right()/* - HorizBoxSpacing*/;
                        //											LeftMarkAfterSpray  = LeftMark- HorizBoxSpacing;;
                        //										}
                        //									}
                        //									else
                        //									{
                        //											CA("f");
                        //										RightMarkAfterSpray = ItemFramesStencilMaxBounds.Left()/* - HorizBoxSpacing*/;
                        //										LeftMarkAfterSpray  = ItemFramesStencilMaxBounds.Left() /*- HorizBoxSpacing*/; 
                        //
                        //										if(ItemFramesStencilMaxBounds.Right() + ItemFramesStencilMaxBounds.Width() > newAllSprayedTempFramesForMaxBounds.Right())
                        //										{
                        //											CA("f1");
                        //											if(newAllSprayedTempFramesForMaxBounds.Right() >= ItemFramesStencilMaxBounds.Right())
                        //											{
                        //												RightMarkAfterSpray = newAllSprayedTempFramesForMaxBounds.Left();
                        //												LeftMarkAfterSpray  = newAllSprayedTempFramesForMaxBounds.Left(); 
                        //											}
                        //											else
                        //											{
                        //												RightMarkAfterSpray = newAllSprayedTempFramesForMaxBounds.Right();
                        //												LeftMarkAfterSpray  = newAllSprayedTempFramesForMaxBounds.Right(); 
                        //											}
                        //
                        //										}
                        //									}
                        //									
                        //									--idxHorizontalCount;
                        //									/*if(lastIndx != 1 && (lastIndx != (static_cast<int32>(ItemIDInfo->size() - 1))))
                        //										--idxHorizontalCount;*/
                        //									++idxVerticalCount;
                        //									if(idxHorizontalCount < 0)
                        //										idxHorizontalCount = 0;
                        //PMString n1("idxVerticalCount  :  ");
                        //n1.AppendNumber(idxVerticalCount);
                        //n1.Append("\nidxHorizontalCount  :  ");
                        //n1.AppendNumber(idxHorizontalCount);
                        //CA(n1);
                        //								}
                        //
                        //							}
                        //							else
                        //							{
                        //								CA("Horizontal Flow ");
                        //								horizontalSprayBottomFrameFlag = kFalse;
                        //								//--First Item co-ordinate after ReSize Box ----
                        //								
                        //								//PMReal firstSprayedItemBoxWidth= newAllSprayedTempFramesForMaxBounds.Width();
                        //
                        //								PMReal firstSprayedItemBoxRight;
                        //								PMReal firstSprayedItemBoxLeft;
                        //								if(PagemarginBoxBounds.RectIn(origMaxBoxBounds1))
                        //								{
                        //									firstSprayedItemBoxRight= newAllSprayedTempFramesForMaxBounds.Right();
                        //									firstSprayedItemBoxLeft = newAllSprayedTempFramesForMaxBounds.Left();
                        //								}
                        //								else
                        //								{
                        //									firstSprayedItemBoxRight = PagemarginBoxBounds.Left() + newAllSprayedTempFramesForMaxBounds.Width();
                        //									firstSprayedItemBoxLeft = PagemarginBoxBounds.Left();
                        //
                        //									if(PagemarginBoxBounds.Right() < 0)
                        //									{
                        //										CA("left page........");
                        //										firstSprayedItemBoxRight = newAllSprayedTempFramesForMaxBounds.Left()+newAllSprayedTempFramesForMaxBounds.Width();
                        //										firstSprayedItemBoxLeft = newAllSprayedTempFramesForMaxBounds.Left();
                        //										
                        //										if(firstSprayedItemBoxRight > PagemarginBoxBounds.Width())
                        //										{
                        //											CA("firstSprayedItemBoxRight > PagemarginBoxBounds.Width()");
                        //											firstSprayedItemBoxRight = PagemarginBoxBounds.Left() + newAllSprayedTempFramesForMaxBounds.Width();
                        //											firstSprayedItemBoxLeft = PagemarginBoxBounds.Left();
                        //										}
                        //
                        //									}
                        //								}
                        //								
                        //								PMReal firstSprayedItemBoxBottom= newAllSprayedTempFramesForMaxBounds.Bottom();
                        //								PMReal firstSprayedItemBoxWidth  = newAllSprayedTempFramesForMaxBounds.Width();
                        //								
                        //								PMString g("firstSprayedItemBoxWidth  :  ");
                        //								g.AppendNumber(firstSprayedItemBoxWidth);
                        //								g.Append("\nidxVerticalCount  :  ");
                        //								g.AppendNumber(idxVerticalCount);
                        //								g.Append("\nfirstSprayedItemBoxBottom  :  ");
                        //								g.AppendNumber(firstSprayedItemBoxBottom);
                        //								g.Append("\nItemFramesStencilMaxBounds.Width()  :  ");
                        //								g.AppendNumber(ItemFramesStencilMaxBounds.Width());
                        //								g.Append("\nPagemarginBoxBounds.Right()  :  ");
                        //								g.AppendNumber(PagemarginBoxBounds.Right());
                        //								CA(g); 
                        //								if(firstSprayedItemBoxRight  > (ItemFramesStencilMaxBounds.Right()+ItemFramesStencilMaxBounds.Width()))
                        //								{
                        //									CA("second con..");
                        //									isInsideTempFrameForHorizantalFlow = kTrue;
                        //
                        //									if(ItemFramesStencilMaxBounds.Top()  < firstSprayedItemBoxBottom)
                        //									{
                        //										if((ItemFramesStencilMaxBounds.Bottom()+ ItemFramesStencilMaxBounds.Height()) > PagemarginBoxBounds.Bottom())
                        //										{
                        //											CA("new condition ");
                        //											//if(ItemFramesStencilMaxBounds.Top() < firstSprayedItemBoxBottom )
                        //											
                        //											/*LeftMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right()) ;
                        //											BottomMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Top());
                        //											TopMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Top());
                        //											RightMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right());*/
                        //											
                        //											//if((firstSprayedItemBoxWidth + firstSprayedItemBoxWidth ) > PagemarginBoxBounds.Right())
                        //											PMString r("lastIndx  L : ");
                        //											r.AppendNumber(lastIndx);
                        //											CA(r);
                        //											if(newLeadingSprayedTempFramesForMaxBounds.Right() + newLeadingSprayedTempFramesForMaxBounds.Width() > PagemarginBoxBounds.Right())
                        //											{
                        //												horizontalSprayBottomFrameFlag = kTrue;
                        //											}
                        //											else if(lastIndx == 1)
                        //											{
                        //												CA("lastIndx == 1");
                        //												LeftMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right());
                        //												RightMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right());
                        //												idxVerticalCount = 0;
                        //												//idxHorizontalCount++;
                        //												if(PagemarginBoxBounds.RectIn(newAllSprayedTempFramesForMaxBounds))
                        //												{
                        //													//CA("33Inside my IF");
                        //													TopMarkAfterSpray = newAllSprayedTempFramesForMaxBounds.Top();
                        //													BottomMarkAfterSpray = newAllSprayedTempFramesForMaxBounds.Top();
                        //												}
                        //												else
                        //												{
                        //													CA("333Inside my ELSE");
                        //													TopMarkAfterSpray = PagemarginBoxBounds.Top();
                        //													BottomMarkAfterSpray = PagemarginBoxBounds.Top();
                        //												}
                        //												isInsideTempFrameForHorizantalFlow = kFalse;
                        //											}
                        //											else 
                        //											{
                        //												LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                        //												BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                        //												TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                        //												RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                        //												idxVerticalCount = 1;
                        //											}
                        //																					
                        //										}
                        //										else
                        //										{
                        //											CA("in  else");
                        //											LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left());
                        //											BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                        //											TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom())+VerticalBoxSpacing;
                        //											RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Left()) - HorizBoxSpacing;
                        //											--idxHorizontalCount;
                        //											++idxVerticalCount;
                        //
                        //											LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                        //											BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                        //											TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                        //											RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right()) + HorizBoxSpacing;
                        //											//--idxHorizontalCount;
                        //											//++idxVerticalCount;
                        //											--idxVerticalCount;
                        //											
                        //
                        //											//--TTTTTTTT---
                        //											//if(firstSprayedItemBoxRight  > (ItemFramesStencilMaxBounds.Right()+ItemFramesStencilMaxBounds.Width())
                        //											
                        //											//if((ItemFramesStencilMaxBounds.Right()+ItemFramesStencilMaxBounds.Width()) < PagemarginBoxBounds.Right())
                        //											//{
                        //											//	LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                        //											//	BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                        //											//	TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                        //											//	RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right()) ;
                        //											//	//--idxHorizontalCount;
                        //											//	idxVerticalCount = 0;
                        //											//}
                        //
                        //											
                        //										}
                        //									}
                        //									else
                        //									{
                        //										CA("jjjjjjj");
                        //										LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                        //										BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                        //										TopMarkAfterSpray = ItemFramesStencilMaxBounds.Top();
                        //										RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                        //
                        //										idxVerticalCount = 0;//idxVerticalCount = 1;
                        //
                        //									}	
                        //
                        //
                        //									if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
                        //										MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
                        //								}
                        //								else
                        //								{
                        //									CA("else");
                        //
                        //									if(ItemFramesStencilMaxBounds.Left() < firstSprayedItemBoxRight)
                        //									{
                        //										CA("ItemFramesStencilMaxBounds.Left() < firstSprayedItemBoxRight");
                        //
                        //										if((ItemFramesStencilMaxBounds.Bottom() + ItemFramesStencilMaxBounds.Height() + VerticalBoxSpacing) < PagemarginBoxBounds.Bottom())
                        //										{
                        //											CA("new condition 11");
                        //											/*LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Right()) ;
                        //											BottomMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Bottom());
                        //											TopMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Bottom())+VerticalBoxSpacing;
                        //											RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());*/
                        //
                        //											LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Right()) ;
                        //											BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                        //											TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom()+ VerticalBoxSpacing);
                        //											RightMarkAfterSpray = firstSprayedItemBoxLeft - HorizBoxSpacing;
                        //											idxVerticalCount = 0;
                        //											//	idxVerticalCount ++;
                        //											/*PMString d("ItemIDInfo->size()  :  ");
                        //											d.AppendNumber(static_cast<int32>(ItemIDInfo->size()));
                        //											d.Append("\nl  :  ");
                        //											d.AppendNumber(lastIndx);
                        //											d.Append("\nidxVerticalCount  :  ");
                        //											d.AppendNumber(idxVerticalCount);
                        //											CA(d);*/
                        //											if(lastIndx != (static_cast<int32>(ItemIDInfo->size() - 1)))
                        //												idxHorizontalCount = 0;
                        //																					
                        //										}
                        //										else
                        //										{
                        //											idxVerticalCount = 0;
                        //											/*if(isInsideTempFrameForHorizantalFlow == kTrue)//---comment
                        //											{*/
                        //												if(PagemarginBoxBounds.RectIn(origMaxBoxBounds1) == kTrue)
                        //												{
                        //													//CA("Template is inside the page!!!");
                        //													CA("CASE 1 : isInsideTempFrameForHorizantalFlow == kTrue");
                        //													LeftMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right()) ;
                        //													BottomMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Top());
                        //													TopMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Top());
                        //													RightMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right());
                        //													//isInsideTempFrameForHorizantalFlow = kFalse;
                        //												}
                        //												else
                        //												{
                        //													//CA("see this cond. later Template is not inside the page!!!");
                        //													CA("CASE 2 : isInsideTempFrameForHorizantalFlow == kTrue");
                        //													LeftMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right()) ;//PagemarginBoxBounds.Left();
                        //													BottomMarkAfterSpray = (PagemarginBoxBounds.Top());
                        //													TopMarkAfterSpray = (PagemarginBoxBounds.Top());
                        //													RightMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right());//PagemarginBoxBounds.Left();
                        //												}
                        //											/*}
                        //											else
                        //											{
                        //												CA("isInsideTempFrameForHorizantalFlow == kFalse  Horizantal++");
                        //												LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left()) ;
                        //												BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                        //												TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                        //												RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                        //											}*/
                        //										}
                        //									}
                        //									else
                        //									{
                        //										CA("2222222");
                        //										idxVerticalCount = 0;
                        //										if(isInsideTempFrameForHorizantalFlow == kTrue)
                        //										{
                        //											CA("isInsideTempFrameForHorizantalFlow == kTrue, Horizantal++");
                        //											
                        //												//-------
                        //											if(PagemarginBoxBounds.RectIn(origMaxBoxBounds1))
                        //											{
                        //												LeftMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right()) ;
                        //												BottomMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Top());
                        //												TopMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Top());
                        //												RightMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right());
                        //											}
                        //											else
                        //											{
                        //												LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Right()) ;
                        //												BottomMarkAfterSpray = (PagemarginBoxBounds.Top());
                        //												TopMarkAfterSpray = (PagemarginBoxBounds.Top());
                        //												RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right()) + HorizBoxSpacing;
                        //											}
                        //
                        //											/*LeftMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right()) ;
                        //											BottomMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Top());
                        //											TopMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Top());
                        //											RightMarkAfterSpray = (newAllSprayedTempFramesForMaxBounds.Right());*/
                        //											
                        //
                        //										}
                        //										else
                        //										{
                        //											CA("isInsideTempFrameForHorizantalFlow == kFalse  Horizantal++");
                        //											LeftMarkAfterSpray = (ItemFramesStencilMaxBounds.Left()) ;
                        //											BottomMarkAfterSpray = (ItemFramesStencilMaxBounds.Bottom());
                        //											TopMarkAfterSpray = (ItemFramesStencilMaxBounds.Top());
                        //											RightMarkAfterSpray = (ItemFramesStencilMaxBounds.Right());
                        //										}
                        //
                        //									}
                        //									
                        //
                        //									if(MaxBottomMarkSprayHZ < BottomMarkAfterSpray)
                        //										MaxBottomMarkSprayHZ = BottomMarkAfterSpray;
                        //									
                        //									isInsideTempFrameForHorizantalFlow = kFalse;
                        //
                        //								}
                        //							}
                        //
                        //							selectUIDList.Append(/*SecondcopiedBoxUIDList*/newTempUIDList);
                        //						}
                    }
                    // TILL HERE
                    
                    if(newTempUIDList.size() > 0)
                    {  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
                        for(int q=0; q < newTempUIDList.size(); q++)
                        {					
                            AllFramesOfCurrentLeadingItemIncludingItsChildItemFrames.Append(newTempUIDList[q]);
                        }										
                    }
                    
                    if(isItemHorizontalFlow1 == kFalse)
                        ProdBlockBoundList.push_back(CurrentFameStruct);
                    
                    if(isColumnChange)
                    {
                        if(returnParameterVec[0].fPageAdded)
                            ProdBlockBoundList.clear();
                        
                        if(tmpFrameBoundsList.size() > 0)
                        {
                            for(int32 index = 0; index  < tmpFrameBoundsList.size() ; index++)
                            {
                                ProdBlockBoundList.push_back(tmpFrameBoundsList[index]);
                            }
                        }
                        
                        idxHorizontalCount = tempHorizontalCnt;
                        idxVerticalCount = tempVerticalCount;
                        
                        tmpFrameBoundsList.clear();
                        
                        if(returnParameterVec[0].fPageAdded)
                        {
                            UID newpageUID = returnParameterVec[0].PageUIDList[0];
                            
                            IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
                            if(fntDoc==nil)
                            {
                                ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::fntDoc==nil");	
                                return ;
                            }
                            IDataBase* database = ::GetDataBase(fntDoc);
                            if(database==nil)
                            {
                                ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::database==nil");			
                                return ;
                            }
                            UIDRef temp_pageRef(database, newpageUID);
                            
                            result = this->getMarginBounds(temp_pageRef, PagemarginBoxBounds);
                            if(result == kFalse)
                            {
                                result = this->getPageBounds(temp_pageRef, PagemarginBoxBounds);
                                if(result == kFalse)
                                    break;
                                
                            }
                            
                            pageUidList.push_back(newpageUID);
                            PageCount++;
                            
                        }
                        
                        
                        UIDList tempUIDList(SecondcopiedBoxUIDList);
                        tempUIDList.Clear();
                        
                        if(returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size() > 0)
                        {  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
                            for(int q=0; q < returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn.size(); q++)
                            {					
                                tempUIDList.Append(returnParameterVec[0].NewImageFrameUIDList_WhenFrameMovesToNextColumn[q]);
                            }						
                        }
                        
                        returnParameterVec.clear();
                        
                        ItemFramesStencilMaxBounds = kZeroRect;
                        result = getMaxLimitsOfBoxes(tempUIDList, ItemFramesStencilMaxBounds, ItemFramesBoxBoundVector);
                        if(result == kFalse)
                            break;
                        returnParameterVec.clear();						
                    }
                    
                    
                }
                //CCCCCCCCCCCCC
                
                
                if(AllFramesOfCurrentLeadingItemIncludingItsChildItemFrames.size() > 0)
                {
                    /*PMString tempStr("AllFramesOfCurrentLeadingItemIncludingItsChildItemFrames ka size");
                     tempStr.AppendNumber(static_cast<int32>(AllFramesOfCurrentLeadingItemIncludingItsChildItemFrames.size()));
                     CA(tempStr);*/
                    
                }
                //CA("Deleting the copied frame...");
                deleteThisBoxUIDList(FirstcopiedBoxUIDList);
                //CA("The copied frame is deleted...");
                
            }while(DOWhileLoopBreakFlag);
        }
    }
    
    if(isOriginalFrameSprayedInSprayItemPerFrame == kFalse)
    {
        //CA("isOriginalFrameSprayedInSprayItemPerFrame == kFalse");
        ICommandSequence *seq=CmdUtils::BeginCommandSequence();
        
        PMRect CopiedItemMaxBoxBoundsBforeSpray;
        vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
        UIDList ItemFrameUIDList(selectUIDList.GetDataBase());
        //CA("1");
        result = kFalse;
        result = this->getMaxLimitsOfBoxes(selectUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);
        
        
        UIDRef originalPageUIDRef, originalSpreadUIDRef;
        result = this->getCurrentPage(originalPageUIDRef, originalSpreadUIDRef);
        if(result == kFalse){ 
            SingleItemSprayReturnFlag = kTrue;
            //CA("AP46_ProductFinder::SubSectionSprayer::startSprayingSubSection::!getCurrentPage");
            ptrIAppFramework->LogError("SubSectionSprayer::startSpraying::startSprayingSubSection::!getCurrentPage");
            return;
        }
        
        
        
        PMRect PagemarginBoxBounds;
        result = getMarginBounds(originalPageUIDRef, PagemarginBoxBounds);
        if(result == kFalse)
        {
            result = getPageBounds(originalPageUIDRef, PagemarginBoxBounds);
            if(result == kFalse)
            {
                return;
            }
        }
        PMReal BottomMarkBeforeSprayForLeadingItem = 0.0;
        
        
        PMReal BottomMarkBeforeSprayForNonSprayItemPerFrame = 0.0;
        PMReal BottomMarkBeforeSprayForSprayItemPerFrame = 0.0;
        
        
        UIDList newLeadingItemSprayedUIDList(selectUIDList);
        UIDList newItemSprayedUIDList(selectUIDList);
        
        newLeadingItemSprayedUIDList.Clear();
        newItemSprayedUIDList.Clear();
        
        for(int32 j=0; j<listLength; j++)
        {
            //CA("inside for");
            TagList newAllSprayedTempTagList = itagReader->getTagsFromBox(selectUIDList.GetRef(j));
            if(newAllSprayedTempTagList.size() == 0)
            {
                //CA("newAllSprayedTempTagList.size() == 0");
                continue;
            }
            
            for(int32 k=0; k <newAllSprayedTempTagList.size(); k++)
            {
                //CA("inside inner for");
                UIDRef test1= selectUIDList.GetRef(j);
                
                if(newAllSprayedTempTagList[k].isSprayItemPerFrame == -1)
                {
                    newItemSprayedUIDList.Append(test1.GetUID());
                    break;
                }
                else if(newAllSprayedTempTagList[k].isSprayItemPerFrame == 2)
                {
                    newLeadingItemSprayedUIDList.Append(test1.GetUID());
                    break;
                }
                else
                {
                    
                    //-----Horizontal Flow
                }
            }
            
            //added for clearing the taglist testing
            for(int32 tagIndex = 0 ; tagIndex < newAllSprayedTempTagList.size() ; tagIndex++)
            {
                newAllSprayedTempTagList[tagIndex].tagPtr->Release();
            }			
            //
        }
        
        
        Parameter requiredParameters;		
        
        
        vectorBoxBounds newItemSprayedTempBoxBoundVector;
        PMRect newItemSprayedTempFramesForMaxBounds = kZeroRect;
        
        if(newItemSprayedUIDList.Length() > 0)
        {
            bool16 testResult = getMaxLimitsOfBoxes(newItemSprayedUIDList, newItemSprayedTempFramesForMaxBounds, newItemSprayedTempBoxBoundVector);
            if(testResult == kFalse)
                return;
            
            BottomMarkBeforeSprayForNonSprayItemPerFrame = newItemSprayedTempFramesForMaxBounds.Bottom();
            requiredParameters.fNonSprayItemPerFramePresent = kTrue;
        }
        else
        {
            requiredParameters.fNonSprayItemPerFramePresent = kFalse;
        }
        
        
        vectorBoxBounds newLeadingSprayedTempBoxBoundVector;
        PMRect newLeadingSprayedTempFramesForMaxBounds = kZeroRect;
        
        if(newLeadingItemSprayedUIDList.Length() > 0)
        {
            bool16 testResult = getMaxLimitsOfBoxes(newLeadingItemSprayedUIDList, newLeadingSprayedTempFramesForMaxBounds, newLeadingSprayedTempBoxBoundVector);
            if(testResult == kFalse)
                return;
            
            BottomMarkBeforeSprayForSprayItemPerFrame = newLeadingSprayedTempFramesForMaxBounds.Bottom();
            requiredParameters.BottomMarkAfterSprayForLeadingItem = BottomMarkBeforeSprayForNonSprayItemPerFrame;
            
            requiredParameters.WidthOfSprayItemPerFrames = newLeadingSprayedTempFramesForMaxBounds.Right() - newLeadingSprayedTempFramesForMaxBounds.Left();
        }
        else
        {
            requiredParameters.BottomMarkAfterSprayForLeadingItem = BottomMarkBeforeSprayForNonSprayItemPerFrame;
            
            requiredParameters.WidthOfSprayItemPerFrames = newItemSprayedTempFramesForMaxBounds.Right() - newItemSprayedTempFramesForMaxBounds.Left();
        }	
        
        
        PMReal RightMarkBeforeSprayForLeadingItem = CopiedItemMaxBoxBoundsBforeSpray.Right();
        requiredParameters.RightMarkAfterSprayForLeadingItem = RightMarkBeforeSprayForLeadingItem;
        
        
        requiredParameters.idxHorizontalCount = idxHorizontalCount; 
        requiredParameters.idxVerticalCount = idxVerticalCount;
        
        
        vector<DynFrameStruct>::iterator itrDSDynFrameStruct;
        itrDSDynFrameStruct = requiredParameters.ProdBlockBoundList.begin();
        requiredParameters.ProdBlockBoundList.insert(itrDSDynFrameStruct,ProdBlockBoundList.begin(),ProdBlockBoundList.end());
        
        
        
        requiredParameters.PagemarginBoxBounds = PagemarginBoxBounds;
        requiredParameters.ItemStencilMaxBounds = ItemStencilMaxBounds;
        
        VecParameter vecParameters;
        vecParameters.push_back(requiredParameters);
        
        /*PMString str("before calling setDifferentParameters = ");
         str.AppendNumber(static_cast<int32>(vecParameters.size()));
         CA(str);*/
        
        DataSprayerPtr->setDifferentParameters(vecParameters);
        
        
        
        //CA("Normal Spray!!");
        //ptrIAppFramework->LogError("Normal Spray");
        for(int i=0; i<listLength; i++)
        {
            //CA("---listLength----");
            PMString allInfo;
            tagList=itagReader->getTagsFromBox(selectUIDList.GetRef(i));
            if(tagList.size()<=0)//This can be a Tagged Frame
            {	
                //CA("5397  tagList.size()<=0");
                if(DataSprayerPtr->isFrameTagged(selectUIDList.GetRef(i)))
                {	
                    //CA("isFrameTagged(selectUIDList.GetRef(i))");
                    DataSprayerPtr->sprayForTaggedBox(selectUIDList.GetRef(i));				
                }
                else
                {
                    //CA("5405 else Continue"); // This Else is for Group Frames where we get the "DataSprayerPtr->isFrameTagged" as false;
                    InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
                    if(!iHier)
                    {
                        //CA(" !iHier >> Continue ");
                        continue;
                    }
                    UID kidUID;				
                    int32 numKids=iHier->GetChildCount();				
                    IIDXMLElement* ptr = NULL;
                    //CA_NUM("numkids: ", numKids);
                    
                    for(int j=0;j<numKids;j++)
                    {
                        //CA("Inside For Loop");
                        kidUID=iHier->GetChildUID(j);
                        UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);			
                        TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
                        if(NewList.size()<=0)//This can be a Tagged Frame
                        {
                            if(DataSprayerPtr->isFrameTagged(boxRef))
                            {	
                                //CA("isFrameTagged(selectUIDList.GetRef(i))");
                                DataSprayerPtr->sprayForTaggedBox(boxRef);				
                            }
                            continue;
                        }
                        DataSprayerPtr->sprayForThisBox(boxRef, NewList);
                        //------------
                        for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
                        {
                            NewList[tagIndex].tagPtr->Release();
                        }
                    }
                }
                continue;	
            }		
            //CA("Commenting DataSprayerPtr->sprayForThisBox SubSection Sprayer");
            DataSprayerPtr->sprayForThisBox(selectUIDList.GetRef(i), tagList);
            //CA("After sprayForThisBox");
            //added for testing
            setoverflowflag = static_cast<int32>(DataSprayerPtr->getItemSprayOverflowFlag());
            if(setoverflowflag  == 1)
            {
                //CA("if(setoverflowflag  == kTrue)");
                goto LABEL;
            }
            for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
            {
                tagList[tagIndex].tagPtr->Release();
            }
            //till here
            
        }
        
        //CA("Normal Spray 1");
        CmdUtils::EndCommandSequence(seq);
        //CA("Normal Spray 2");
    }
    
    setoverflowflag = 0;
    DataSprayerPtr->setItemSprayOverflowFlag(0);
    //ptrIAppFramework->LogError("Normal Spray 2");
    //------------
    //for(int32 tagIndex = 0 ; tagIndex < tagList.size() ; tagIndex++)
    //{
    //	tagList[tagIndex].tagPtr->Release();
    //}
}


bool16 SubSectionSprayer::getCurrentPage(UIDRef& pageUIDRef, UIDRef& spreadUIDRef)
{
	bool16 result = kFalse;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}

	do
	{
	
		//InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData()); //Cs3
		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());//Cs4
		if (layoutData == NULL){
			ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCurrentPage::layoutData == NULL");
			break;
		}

		IDocument* document = layoutData->GetDocument();
		if (document == nil){
			ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCurrentPage::document is NULL");		
			break;
		}

		IDataBase* database = ::GetDataBase(document);
		if(!database){
			ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCurrentPage::database is NULL");		
			break;
		}
		
		UID pageUID = layoutData->GetPage();
		if(pageUID == kInvalidUID){
			ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCurrentPage::pageUID is NULL");		
			break;
		}
		
		int32 CurrentPageIndex=static_cast<int32>(pageUidList.size());
		if(CurrentPageIndex== 0 )  //CurrentPageIndex== PageCount)
		{
			UIDRef pageRef(database, pageUID);

			pageUIDRef = pageRef;
				
			///Commented By Sachin SHarma
			/*IGeometry* spreadGeomPtr = layoutData->GetSpread();
			if(spreadGeomPtr == nil){
				ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCurrentPage::spreadGeomPtr is NULL");
				break;
			}*/
			
			UIDRef spreadref = layoutData->GetSpreadRef(); ///*****Added 

			InterfacePtr<IHierarchy> hierarchyPtr(spreadref, IID_IHIERARCHY);
			if(hierarchyPtr == nil){
				ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCurrentPage::hierarchyPtr is NULL");			
				break;
			}

			UID spreadUID = hierarchyPtr->GetSpreadUID();
			UIDRef spreadRef(database, spreadUID);
			spreadUIDRef = spreadRef;
			result = kTrue;		
			break;
		}
		else if(CurrentPageIndex!=PageCount)
		{
			if(pageUidList[CurrentPageIndex-1]!= pageUID)
			{
				UIDRef pageRef(database, pageUID);
				pageUIDRef = pageRef;
				//Commented By Sachin sharma
				/*IGeometry* spreadGeomPtr = layoutData->GetSpread();
				if(spreadGeomPtr == nil){
					ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCrrentPage::spreadGeomPtr is NULL");				
					break;
				}*/
				UIDRef spreadref = layoutData->GetSpreadRef();///****ADDED.

				InterfacePtr<IHierarchy> hierarchyPtr(spreadref, IID_IHIERARCHY);
				if(hierarchyPtr == nil){
					ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCurrentPage::hierarchyPtr is NULL");				
					break;
				}

				UID spreadUID = hierarchyPtr->GetSpreadUID();
				UIDRef spreadRef(database, spreadUID);
				spreadUIDRef = spreadRef;
				result = kTrue;				
				break;		
			}
			else
			{
				//***Commented By sachin sharma
				/*IGeometry* spreadItem = layoutData->GetSpread();
				if(spreadItem == nil){
					ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCurrentPage::spreadItem is NULL");
					return kFalse;
				}*/
				UIDRef spredaref = layoutData->GetSpreadRef();//***ADDED

				InterfacePtr<ISpread> iSpread(/*spreadItem*/spredaref, UseDefaultIID());
				if (iSpread == nil){
					ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCurrentPage::iSpread is NULL");				
					return kFalse;
				}

				int numPages=iSpread->GetNumPages();
				int OldPageIndex= iSpread->GetPageIndex(pageUID);
				if(numPages >= OldPageIndex+1 )
				 pageUID= iSpread->GetNthPageUID(OldPageIndex+1);

				UIDRef pageRef(database, pageUID);
				pageUIDRef = pageRef;

				///***Commented By Sachin Sharma
				/*IGeometry* spreadGeomPtr = layoutData->GetSpread();
				if(spreadGeomPtr == nil){
					ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCurrentPage::spreadGeomPtr is NULL");				
					break;
				}*/
				UIDRef spreadref = layoutData->GetSpreadRef();//**ADDED

				InterfacePtr<IHierarchy> hierarchyPtr(/*spreadGeomPtr*/spreadref, IID_IHIERARCHY);
				if(hierarchyPtr == nil){
					ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getCurrentPage::hierarchyPtr is NULL");				
					break;
				}

				UID spreadUID = hierarchyPtr->GetSpreadUID();
				UIDRef spreadRef(database, spreadUID);
				spreadUIDRef = spreadRef;
				result = kTrue;				
				break;		
			}
		}
		else if(CurrentPageIndex == PageCount)
		{
			//****Commented By Sachin sharma
			/*IGeometry* spreadItem = layoutData->GetSpread();
			if(spreadItem == nil)
				return kFalse;*/
			UIDRef spreadref = layoutData->GetSpreadRef();///*ADDED	

			InterfacePtr<ISpread> iSpread(/*spreadItem*/spreadref, UseDefaultIID());
			if (iSpread == nil)
				return kFalse;

			int numPages=iSpread->GetNumPages();
			
//			if(numPages >= CurrentPageIndex )							
//				pageUID= iSpread->GetNthPageUID(CurrentPageIndex);			
			
			pageUID = iSpread->GetNthPageUID(numPages-1);

			UIDRef pageRef(database, pageUID);
			pageUIDRef = pageRef;

			//****Commented By Sachin Sharma
			/*IGeometry* spreadGeomPtr = layoutData->GetSpread();
			if(spreadGeomPtr == nil)
				break;*/
			UIDRef spreadUIDref= layoutData->GetSpreadRef();  //***ADDED

			InterfacePtr<IHierarchy> hierarchyPtr(/*spreadGeomPtr*/spreadUIDref, IID_IHIERARCHY);
			if(hierarchyPtr == nil)
				break;

			UID spreadUID = hierarchyPtr->GetSpreadUID();
			UIDRef spreadRef(database, spreadUID);
			spreadUIDRef = spreadRef;
			result = kTrue;				
			break;		
		}

	}
	while(kFalse);

	return result;

}

bool16 SubSectionSprayer::getSelectedBoxIds(UIDList& selectUIDList)
{	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CA("ptrIAppFramework == NULL");
		return kFalse;
	}
	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
	{	
		ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getSelectedBoxIds::iSelectionManager is NULL");
		return kFalse;
	}
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(!txtMisSuite)
	{
		ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getSelectedBoxIds::!txtMisSuite");
		return kFalse; 
	}
	
	txtMisSuite->GetUidList(selectUIDList);
	const int32 listLength=selectUIDList.Length();
	//PMString asd("List Length : ");asd.AppendNumber(listLength);CA(asd);
	if(listLength==0){ 
		ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::getSelectedBoxIds::listLength==0");
		return kFalse;
	}

	///////////////////////for passing database pointer////////////////
	//InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());//Cs3
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
	if (layoutData == nil){
		ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getSelectedBoxIds::layoutData == nil");
		return kFalse;
	}

	IDocument* document = layoutData->GetDocument();
	if (document == nil){
		ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getSelectedBoxIds::document == nil");
		return kFalse;
	}
	IDataBase* database = ::GetDataBase(document);
	if(!database){ 
		ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::getSelectedBoxIds::!database");
		return kFalse;
	}
	///////////////////////for passing database pointer////////////////

	/*for(int32 i=0; i<listLength;i++)
	{
		UIDRef boxUIDRef = _selectUIDList->GetRef(i);
		if(this->isBoxParent(boxUIDRef, database)==kFalse)
		{
			CA("Please select only parent boxes and not embedded boxes while spraying.");
			return kFalse;
			
		}
	}*/
	
	//selectUIDList = getParentSelectionsOnly(selectUIDList, database); //required for copying embedded boxes also.

	UIDList TempUIDList(database);

	//if(selectUIDList.Length() <=0 )
	//	return kTrue;

	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::getSelectedBoxIds::Pointre to DataSprayerPtr not found");
		return kFalse;
	}

	//for(int i=0; i<selectUIDList.Length(); i++)
	//{
	//	InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
	//	if(!iHier)
	//	{
	//		continue;
	//	}
	//	UID kidUID;
	//	
	//	int32 numKids=iHier->GetChildCount();
	//	IIDXMLElement* ptr = NULL;
	//	for(int j=0;j<numKids;j++)
	//	{
	//		kidUID=iHier->GetChildUID(j);
	//		UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
	//		//InterfacePtr<ITagReader> itagReader
	//			//((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	//		if(!itagReader)
	//			return kFalse;
	//		TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
	//		if(!DataSprayerPtr->doesExist(ptr, boxRef))
	//		{
	//			selectUIDList.Append(kidUID);					
	//		}
	//	}
	//}

	//TagList NewList; //og

	for(int32 i= selectUIDList.Length()-1; i>=0; i--)
	{
		//TempUIDList.Append(selectUIDList.GetRef(i).GetUID());
		InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		if(!iHier)
		{
			//CA(" !iHier >> Continue ");
			continue;
		}
		UID kidUID;
		
		int32 numKids=iHier->GetChildCount();

		bool16 isGroupFrame = kFalse ;
		isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(i));

		if(isGroupFrame == kTrue) 
		{
			IIDXMLElement* ptr = NULL;
			for(int j=0;j<numKids;j++)
			{
				kidUID=iHier->GetChildUID(j);
				UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);

				InterfacePtr<IHierarchy> iHierarchy(boxRef, UseDefaultIID());
				if(!iHierarchy)
				{
					//CA(" !iHier >> Continue ");
					continue;
				}
				UID newkidUID;
				
				int32 numNewKids=iHierarchy->GetChildCount();
				/*PMString ASD("numKids ................: ");
				ASD.AppendNumber(numKids);
				CA(ASD);*/

				bool16 isGroupFrameAgain = kFalse ;
				isGroupFrameAgain = Utils<IPageItemTypeUtils>()->IsGroup(boxRef);

				if(isGroupFrameAgain == kTrue) 
				{
					IIDXMLElement* newPtr = NULL; //og
				
					for(int k=0;k<numNewKids;k++)
					{
						//CA("Inside For Loop");
						newkidUID=iHierarchy->GetChildUID(k);
						UIDRef childBoxRef(selectUIDList.GetDataBase(), newkidUID);	

					
						//CA("isGroupFrame == kTrue");
						TagList NewList = itagReader->getTagsFromBox(childBoxRef, &newPtr);
						
						/*PMString s("NewList.size() : ");
						s.AppendNumber(NewList.size());
						CA(s);*/		
					

						if(!doesExist(NewList,TempUIDList))
						{
							TempUIDList.Append(newkidUID);				
						}
						//------------
						for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
						{
							NewList[tagIndex].tagPtr->Release();
						}
					}

				}
				else
				{
					//CA("ELSE");
					TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
					
					/*PMString s("NewList.size() : ");
					s.AppendNumber(NewList.size());
					CA(s);*/				
				

					if(!doesExist(NewList,TempUIDList))
					{
						TempUIDList.Append(kidUID);				
					}
					for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
					{
						NewList[tagIndex].tagPtr->Release();
					}
				}			
			}
		}
		else
		{
			//CA("isGroupFrame == kFalse");
			TempUIDList.Append(selectUIDList.GetRef(i).GetUID());
		}
	}
//////
	int32 asd1 = TempUIDList.Length();
	//PMString asd2("tempList Length : ");asd2.AppendNumber(asd1);CA(asd2);

//////

	selectUIDList = TempUIDList;
	return kTrue;
	
	//return kFalse;
}

//UIDList itemsToDeselect(database);
/*itemsToDeselect.Insert(boxUIDRef.GetUID());
_selectUIDList->Remove(i);
listLength = _selectUIDList->Length();*/
/*if(itemsToDeselect.Length() > 0)
{
	//selection->Deselect(itemsToDeselect);
	ICommand* cmdPtr = Utils<ISelectUtils>()->DeselectPageItems(&itemsToDeselect);
	InterfacePtr<ICommand> iCmdPtr(cmdPtr, UseDefaultIID());

	int status = CmdUtils::ProcessCommand(iCmdPtr);
}*/

bool16 SubSectionSprayer::getMaxLimitsOfBoxes(const UIDList& boxList, PMRect& maxBounds, vectorBoxBounds &boxboundlist)
{
	boxboundlist.clear();
	PMReal minTop=0.0, minLeft=0.0 , maxBottom=0.0 , maxRight=0.0;
	UIDRef boxUIDRef = boxList.GetRef(0);
	int index =0;

	InterfacePtr<IGeometry> geometryPtr(boxUIDRef, UseDefaultIID());
	if(!geometryPtr)
	{
		//CA("!geometryPtr First time");
		if(boxList.Length() > 1)  // This is if first frame is deleted after spray // (Ref: New Indicator Spray Functionality)
		{
			boxUIDRef = boxList.GetRef(1);
			InterfacePtr<IGeometry> geometryPtr1(boxUIDRef, UseDefaultIID());
			geometryPtr = geometryPtr1;
			index = index+2;
		}
		else
		{	//CA("Return False from getMaxLimitsOfBoxes");
			return kFalse;
		}
	}
	else
	{
		index = index + 1;
	}

	if(!geometryPtr)
		return kFalse;

	PMRect boxBounds=geometryPtr->GetStrokeBoundingBox(InnerToPasteboardMatrix(geometryPtr));
	
	PMReal top = boxBounds.Top();
	PMReal left = boxBounds.Left();
	PMReal bottom = boxBounds.Bottom();
	PMReal right = boxBounds.Right();

	BoxBounds orgBoxBound;
	orgBoxBound.Top = top;
	orgBoxBound.Left = left;
	orgBoxBound.Right = right;
	orgBoxBound.Bottom = bottom;
	orgBoxBound.BoxUIDRef = boxUIDRef;
	orgBoxBound.compressShift =0;
	orgBoxBound.enlargeShift =0;
	orgBoxBound.shiftUP = kTrue;
	boxboundlist.push_back(orgBoxBound);

	/*PMString ASD("orgBoxBound.Top() : ");
	ASD.AppendNumber((orgBoxBound.Top));
	ASD.Append("  orgBoxBound.Left : ");
	ASD.AppendNumber((orgBoxBound.Left));
	ASD.Append("  orgBoxBound.Bottom : ");
	ASD.AppendNumber((orgBoxBound.Bottom));
	ASD.Append("  orgBoxBound.Right : ");
	ASD.AppendNumber((orgBoxBound.Right));
	CA(ASD);*/

	minTop = top;
	minLeft = left;
	maxBottom = bottom;
	maxRight = right;

	for(index=1;index<boxList.Length();index++)
	{
		boxUIDRef = boxList.GetRef(index);

		InterfacePtr<IGeometry> geometryPtr(boxUIDRef, UseDefaultIID());
		if(!geometryPtr)
		{
			continue;
		}

		PMRect boxBounds=geometryPtr->GetStrokeBoundingBox(InnerToPasteboardMatrix(geometryPtr));
				
		top = boxBounds.Top();
		left = boxBounds.Left();
		bottom = boxBounds.Bottom();
		right = boxBounds.Right();

		BoxBounds orgBoxBound1;
		orgBoxBound1.Top = top;
		orgBoxBound1.Left = left;
		orgBoxBound1.Right = right;
		orgBoxBound1.Bottom = bottom;
		orgBoxBound1.BoxUIDRef = boxUIDRef;
		orgBoxBound1.compressShift =0;
		orgBoxBound1.enlargeShift =0;
		orgBoxBound1.shiftUP = kTrue;
		boxboundlist.push_back(orgBoxBound1);

		if(top < minTop)
			minTop = top;
		if(left < minLeft)
			minLeft = left;
		if(bottom > maxBottom)
			maxBottom = bottom;
		if(right > maxRight)
			maxRight = right;
	}

	maxBounds.Top(minTop);
	maxBounds.Left(minLeft);
	maxBounds.Bottom(maxBottom);
	maxBounds.Right(maxRight);

	/*PMString ASD("maxBounds.Top() : ");
	ASD.AppendNumber((maxBounds.Top()));
	ASD.Append("  maxBounds.Left() : ");
	ASD.AppendNumber((maxBounds.Left()));
	ASD.Append("  maxBounds.Bottom() : ");
	ASD.AppendNumber((maxBounds.Bottom()));
	ASD.Append("  maxBounds.Right() : ");
	ASD.AppendNumber((maxBounds.Right()));
	CA(ASD);*/
	return kTrue;
}


bool16 SubSectionSprayer::getMaxLimitsOfBoxesForProductList(vector<PMRect> boxMarginList, PMRect& maxBounds)
{
	if(boxMarginList.size() == 0)
		return kFalse;

	PMReal minTop=0.0, minLeft=0.0 , maxBottom=0.0 , maxRight=0.0;

	PMReal top = boxMarginList[0].Top();
	PMReal left = boxMarginList[0].Left();
	PMReal bottom = boxMarginList[0].Bottom();
	PMReal right = boxMarginList[0].Right();

	/*PMString ASD("boxMarginList[0].Top() : ");
	ASD.AppendNumber((boxMarginList[0].Top()));
	ASD.Append("  boxMarginList[0].Left() : ");
	ASD.AppendNumber((boxMarginList[0].Left()));
	ASD.Append("  boxMarginList[0].Bottom() : ");
	ASD.AppendNumber((boxMarginList[0].Bottom()));
	ASD.Append("  boxMarginList[0].Right() : ");
	ASD.AppendNumber((boxMarginList[0].Right()));
	CA(ASD);*/
		
	minTop = top;
	minLeft = left;
	maxBottom = bottom;
	maxRight = right;

	for(int i=1;i<boxMarginList.size();i++)
	{
		
		top = boxMarginList[i].Top();
		left = boxMarginList[i].Left();
		bottom = boxMarginList[i].Bottom();
		right = boxMarginList[i].Right();

		/*PMString ASD("boxMarginList[i].Top() : ");
		ASD.AppendNumber((boxMarginList[i].Top()));
		ASD.Append("  boxMarginList[i].Left() : ");
		ASD.AppendNumber((boxMarginList[i].Left()));
		ASD.Append("  boxMarginList[i].Bottom() : ");
		ASD.AppendNumber((boxMarginList[i].Bottom()));
		ASD.Append("  boxMarginList[i].Right() : ");
		ASD.AppendNumber((boxMarginList[i].Right()));
		CA(ASD);*/

		if(top < minTop)
			minTop = top;
		if(left < minLeft)
			minLeft = left;
		if(bottom > maxBottom)
			maxBottom = bottom;
		if(right > maxRight)
			maxRight = right;
	}

	maxBounds.Top(minTop);
	maxBounds.Left(minLeft);
	maxBounds.Bottom(maxBottom);
	maxBounds.Right(maxRight);

	return kTrue;
}


void SubSectionSprayer::getMaxHorizSprayCount(const PMRect& marginBounds, const PMRect& boxBounds, int16& horizSprayCount)
{
	//page bounds
	PMReal maxPageWidth=0.0, maxPageHeight=0.0;

	//CMM Comment
	/*
	InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
	if(iSSSprayer==nil)
	{
		CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
		return;
	}
	*/
	maxPageWidth = abs(marginBounds.Right() - marginBounds.Left());
	maxPageHeight = abs(marginBounds.Bottom() - marginBounds.Top());

	//box bounds
	PMReal maxWidth=0.0, maxHeight=0.0;

	maxWidth = abs(boxBounds.Right() - boxBounds.Left());
	maxHeight = abs(boxBounds.Bottom() - boxBounds.Top());

	//calculation of number of times to spray
	//CMM comment PMReal realCount = Round(maxPageWidth/(maxWidth+iSSSprayer->getHorizontalBoxSpacing()));
	PMReal realCount = Round(maxPageWidth/(maxWidth+MediatorClass ::subSecSpraySttngs.horizontalBoxSpacing ));

	if(realCount<=PMReal(0.0)) // if(realCount<=PMReal(0.0))
	{
		horizSprayCount = 0;
		return;
	}

	PMReal tempCount = realCount - PMReal(1.0);

	//CMM Comment if((((maxWidth+iSSSprayer->getHorizontalBoxSpacing())*(tempCount)) + maxWidth) >/*=*/ maxPageWidth)
	if((((maxWidth+MediatorClass ::subSecSpraySttngs.horizontalBoxSpacing )*(tempCount)) + maxWidth) >/*=*/ maxPageWidth)
	{   
		horizSprayCount = ToInt16(realCount);
		horizSprayCount--;
	}
	else
		horizSprayCount = ToInt16(realCount);
}

void SubSectionSprayer::getMaxVertSprayCount(const PMRect& marginBounds, const PMRect& boxBounds, int16& vertSprayCount)
{
	//page bounds
	PMReal maxPageWidth=0.0, maxPageHeight=0.0;
	//CMM Comment
	/*
	InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
	if(iSSSprayer==nil)
	{
		CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
		return;
	}
	*/

	maxPageWidth = abs(marginBounds.Right() - marginBounds.Left());
	maxPageHeight = abs(marginBounds.Bottom() - marginBounds.Top());

	//box bounds
	PMReal maxWidth=0.0, maxHeight=0.0;

	maxWidth = abs(boxBounds.Right() - boxBounds.Left());
	maxHeight = abs(boxBounds.Bottom() - boxBounds.Top());

	//calculation of number of times to spray
	
	//CMM Comment PMReal realCount = Round(maxPageHeight/(maxHeight + iSSSprayer->getVerticalBoxSpacing()));
	PMReal realCount = Round(maxPageHeight/(maxHeight + MediatorClass ::subSecSpraySttngs.verticalBoxSpacing ));
	if(realCount<=PMReal(0.0))
	{
		vertSprayCount = 0;
		return;
	}

	PMReal tempCount = realCount - PMReal(1.0);

	//CMM Comment if((((maxHeight+iSSSprayer->getVerticalBoxSpacing())*(tempCount)) + maxHeight) >/*=*/ maxPageHeight)
	if((((maxHeight+MediatorClass ::subSecSpraySttngs.verticalBoxSpacing)*(tempCount)) + maxHeight) >/*=*/ maxPageHeight)
	{
		vertSprayCount = ToInt16(realCount);
		vertSprayCount--;
	}
	else
		vertSprayCount = ToInt16(realCount);
}

bool16 SubSectionSprayer::getMarginBounds(const UIDRef& pageUIDRef, PMRect& marginBoxBounds)
{
	bool16 result = kFalse;

	do
	{
		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutData == nil)
			break;

		IDocument* doc = layoutData->GetDocument();
		if (doc == nil)
		{
			ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No document");		
			break;
		}
		InterfacePtr<IPageList> pageList(doc, UseDefaultIID());
		if (pageList == nil)
		{
			ASSERT_FAIL("pageList is invalid");
			break;
		}
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			ASSERT_FAIL("pageGeometry is invalid");
			break;
		}
		PMRect pageBounds = pageGeometry->GetStrokeBoundingBox();

		PMReal leftMargin =0.0,topMargin =0.0,rightMargin = 0.0,bottomMargin =0.0;
		// ... and the page's margins.
		/*InterfacePtr<IMargins> pageMargins(pageGeometry, IID_IMARGINS);
		if (pageMargins == nil)
		{
			ASSERT_FAIL("pageMargins is invalid");
			break;
		}*/
		
		/*pageMargins->GetMargins(&leftMargin,&topMargin,&rightMargin,&bottomMargin);*/

		InterfacePtr<ITransform> transform(pageUIDRef, UseDefaultIID());
		ASSERT(transform);
		if (!transform) {
			break;
		}
		PMRect marginBBox;
		InterfacePtr<IMargins> margins(transform, IID_IMARGINS);
		// Note it's OK if the page does not have margins.
		if (margins) {
			margins->GetMargins(&leftMargin,&topMargin,&rightMargin,&bottomMargin);
		}

		PageType pageType = pageList->GetPageType(pageUIDRef.GetUID()) ;

		PMPoint leftTop;
		PMPoint rightBottom;

		if(pageType == kLeftPage)
		{
			leftTop.X(pageBounds.Left()+rightMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - leftMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);
	
		}
		else if(pageType == kRightPage)
		{
			leftTop.X(pageBounds.Left()+leftMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - rightMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);
		}
		else if(pageType == kUnisexPage)
		{
			leftTop.X(pageBounds.Left()+leftMargin);
			leftTop.Y(pageBounds.Top()+topMargin);
			rightBottom.X(pageBounds.Right() - rightMargin);
			rightBottom.Y(pageBounds.Bottom() - bottomMargin);

		}

		// Place the item into a frame the size of the page margins
		// with origin at the top left margin. Note that the frame
		// is automatically resized to fit the content if the 
		// content is a graphic. Convert the points into the
		// pasteboard co-ordinate space.
		/*CAI("leftMargin", leftMargin);
		CAI("topMargin", topMargin );
		CAI("rightMargin", rightMargin );
		CAI("bottomMargin", bottomMargin );*/
		/*PMPoint leftTop(pageBounds.Left()+leftMargin, pageBounds.Top()+topMargin);
		PMPoint rightBottom(pageBounds.Right() - rightMargin, pageBounds.Bottom() - bottomMargin);*/

		/*CAI("PageleftMargin", pageBounds.Left()+leftMargin);
		CAI("PagetopMargin", pageBounds.Top()+topMargin );
		CAI("PagerightMargin", pageBounds.Right() - rightMargin );
		CAI("PagebottomMargin", pageBounds.Bottom() - bottomMargin );*/
		
		///****Commented By Sachin sharma
		/*::InnerToPasteboard(pageGeometry,&leftTop);
		::InnerToPasteboard(pageGeometry,&rightBottom);*/

		//****ADded
		::TransformInnerPointToPasteboard(pageGeometry,&leftTop);
		::TransformInnerPointToPasteboard(pageGeometry,&rightBottom);

		marginBoxBounds.Left() = leftTop.X();
		marginBoxBounds.Top() = leftTop.Y();
		marginBoxBounds.Right() = rightBottom.X();
		marginBoxBounds.Bottom() = rightBottom.Y();

		result = kTrue;
	}
	while(kFalse);

	return result;
}

bool16 SubSectionSprayer::getPageBounds(const UIDRef& pageUIDRef, PMRect& pageBounds)
{
	bool16 result = kFalse;

	do
	{
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			ASSERT_FAIL("pageGeometry is invalid");
			break;
		}
		
		pageBounds = pageGeometry->GetStrokeBoundingBox();
		result = kTrue;
	}
	while(kFalse);

	return result;
}

void SubSectionSprayer::sprayPage(const UIDRef& pageUIDRef, const UIDList& selectedUIDList, const PMRect& origMaxBoxBounds, const PBPMPoint& maxPageSprayCount, int32& numProducts, RangeProgressBar& progressBar, bool16 toggleFlag)
{
	do
	{
		PMRect marginBoxBounds;
		bool16 result = kFalse;
		int32 numProductsSprayed=0;
		bool16 sprayingDone = kFalse;

		//CMM Comment
		/*
		InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
		if(iSSSprayer==nil)
		{
			CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
			return;
		}
		*/
		result = this->getMarginBounds(pageUIDRef, marginBoxBounds);
		if(result == kFalse)
		{
			result = this->getPageBounds(pageUIDRef, marginBoxBounds);
			if(result == kFalse)
				break;
		}

		/*PMString temp;
		temp.AppendNumber(iSSSprayer->getTopMargin());
		CA(temp);*/

		//CMM Comment
		/*
		marginBoxBounds.Left() += iSSSprayer->getLeftMargin();
		marginBoxBounds.Right() -= iSSSprayer->getRightMargin();
		marginBoxBounds.Top() += iSSSprayer->getTopMargin();
		marginBoxBounds.Bottom() -= iSSSprayer->getBottomMargin();
		//CMM Comment end
		*/
		marginBoxBounds.Left() += 0.0;
		marginBoxBounds.Right() -= 0.0;
		marginBoxBounds.Top() += 0.0;
		marginBoxBounds.Bottom() -= 0.0;
	

		int32 tempNumProducts=0;
		vector<double> tempIdList;
		tempIdList.clear();
		
		result = this->getAllIdForLevel(numProducts, tempIdList);
		
		int16 maxVertCnt=0, maxHorizCnt=0;

		//CMM Comment if(iSSSprayer->getHorizFlowType()==kFalse)
		if(MediatorClass ::subSecSpraySttngs.flowHorizontalFlow == kFalse)
		{
			maxVertCnt = ToInt16(maxPageSprayCount.X());
			maxHorizCnt = ToInt16(maxPageSprayCount.Y());
		}
		else
		{
			maxVertCnt = ToInt16(maxPageSprayCount.Y());
			maxHorizCnt = ToInt16(maxPageSprayCount.X());
		}

		for(int16 vertCnt=0; vertCnt<maxVertCnt; vertCnt++)
		{
			for(int16 horizCnt=0; horizCnt<maxHorizCnt; horizCnt++)
			{

				InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
				InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
				if (!layoutSelectionSuite) {
					break;
				}
				selectionManager->DeselectAll(nil); // deselect every active CSB
				 
				//layoutSelectionSuite->Select(selectedUIDList, Selection::kReplace,  Selection::kDontScrollSelection);
				layoutSelectionSuite->/*Select*/SelectPageItems(selectedUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);		


				//check for overlapping of boxes
				int16 tempHorizCnt=0, tempVertCnt=0;
				
				//CMM Comment if(iSSSprayer->getAlternatingVal())
				if(MediatorClass ::subSecSpraySttngs.alternateForEachPage)
				{
					//CMM Comment if(iSSSprayer->getHorizFlowType())
					if(MediatorClass ::subSecSpraySttngs.flowHorizontalFlow)
					{
						//CMM Comment if(iSSSprayer->getLeftToRightVal()==kTrue)
						if(MediatorClass ::subSecSpraySttngs.flowLeftToRight == kTrue)
						{
							if(toggleFlag)
								tempHorizCnt = (maxHorizCnt-1) - horizCnt;
							else
								tempHorizCnt = horizCnt;
						}
						else
						{
							if(toggleFlag)
								tempHorizCnt = horizCnt;
							else
								tempHorizCnt = (maxHorizCnt-1) - horizCnt;
						}
					}
					else
					{
						//CMM Comment if(iSSSprayer->getLeftToRightVal()==kTrue)
						if(MediatorClass ::subSecSpraySttngs.flowLeftToRight == kTrue)
						{
							if(toggleFlag)
								tempVertCnt = (maxVertCnt-1) - vertCnt;
							else
								tempVertCnt = vertCnt;
						}
						else
						{
							if(toggleFlag)
								tempVertCnt = vertCnt;
							else
								tempVertCnt = (maxVertCnt-1) - vertCnt;
						}
					}
				}
				else
				{
					//CMM Comment if(iSSSprayer->getHorizFlowType())
					 if(MediatorClass ::subSecSpraySttngs.flowHorizontalFlow )
					{
						//CMM Comment if(iSSSprayer->getLeftToRightVal()==kFalse)
						if(MediatorClass ::subSecSpraySttngs.flowLeftToRight ==kFalse)
						{
							tempHorizCnt = (maxHorizCnt-1) - horizCnt;
						}
						else
							tempHorizCnt = horizCnt;
					}
					else
					{
						//CMM Comment if(iSSSprayer->getLeftToRightVal()==kFalse)
						if(MediatorClass ::subSecSpraySttngs.flowLeftToRight ==kFalse)
						{
							tempVertCnt = (maxVertCnt-1) - vertCnt;
						}
						else
							tempVertCnt = vertCnt;
					}
				}
				
				//CMM Comment if(iSSSprayer->getHorizFlowType())
				if(MediatorClass ::subSecSpraySttngs.flowHorizontalFlow )
				{
					//CMM Comment if(iSSSprayer->getTopToBottomVal()==kFalse)
					if(kFalse)
					{
						tempVertCnt = (maxVertCnt-1) - vertCnt;
					}
					else
						tempVertCnt = vertCnt;
				}
				else
				{
					//CMM Comment if(iSSSprayer->getTopToBottomVal()==kFalse)
					if(kFalse)
					{
						tempHorizCnt = (maxHorizCnt-1) - horizCnt;
					}
					else
						tempHorizCnt = horizCnt;
				}

				//CMM Comment if(iSSSprayer->getHorizFlowType()==kFalse)
				if(MediatorClass ::subSecSpraySttngs.flowHorizontalFlow == kFalse)
				{
					int16 temp;

					temp = tempHorizCnt;
					tempHorizCnt = tempVertCnt;
					tempVertCnt = temp;
				}

				PBPMPoint moveToPoints;
				result = this->getBoxPosition(marginBoxBounds, origMaxBoxBounds, tempHorizCnt, tempVertCnt, moveToPoints);
				if(!result)
				{
					//CA("ignored");
					continue;
				}

				if(tempVertCnt>0)
				{
					for(int32 i=0;i<tempVertCnt; i++)
					{
						//CMM Comment moveToPoints.Y() += iSSSprayer->getVerticalBoxSpacing();
						moveToPoints.Y() += MediatorClass ::subSecSpraySttngs.verticalBoxSpacing;
					}
				}
				if(tempHorizCnt>0)
				{
					for(int32 i=0;i<tempHorizCnt; i++)
					{
						//CMM Comment moveToPoints.X() += iSSSprayer->getHorizontalBoxSpacing();
						moveToPoints.X() += MediatorClass ::subSecSpraySttngs.horizontalBoxSpacing;
					}
				}

				//copy the selected items
				this->CopySelectedItems();

				//now get the copied item list
				UIDList copiedBoxUIDList;
				result = this->getSelectedBoxIds(copiedBoxUIDList);
				if(result == kFalse)
					break;

				//now move boxes to appropriate positions
				this->moveBoxes(copiedBoxUIDList, moveToPoints);
				
				//spray for the selected boxes which were just copied
				//start with the second product

				this->getAllBoxIds(copiedBoxUIDList);

				
				//treeCache.isExist(tempIdList[sprayedProductIndex], node);
				//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				
				PMString tempString("Spraying ");
				//tempString +=pNodeDataList[sprayedProductIndex].getName();
				PMString ProductName("");
                
                ProductName = CurrentSectionpNodeDataList[sprayedProductIndex].getName();
				tempString +=ProductName;
				tempString += "...";
				progressBar.SetTaskText(tempString);

				//InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
				if(!DataSprayerPtr)
				{
					ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayPage::Pointre to DataSprayerPtr not found");
					return;
				}
				/*PMString ASD("Publication ID : ");
				ASD.AppendNumber(CurrentSelectedPublicationID);
				CA(ASD);*/
				
				//DataSprayerPtr->FillPnodeStruct(pNodeDataList[sprayedProductIndex],CurrentSelectedSection,CurrentSelectedPublicationID);
				//DataSprayerPtr->FillPnodeStruct(CurrentSectionpNodeDataList[sprayedProductIndex],CurrentSelectedSection,CurrentSelectedPublicationID);
				//9May
					PublicationNode pNode =CurrentSectionpNodeDataList[sprayedProductIndex].convertToPublicationNode();
					if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kTrue)
					{
						DataSprayerPtr->FillPnodeStruct
						(pNode,CurrentSelectedSection,CurrentSelectedPublicationID);	

					}
					else
					{
					DataSprayerPtr->FillPnodeStruct
						(	pNode,
							CurrentSectionpNodeDataList[sprayedProductIndex].sectionID,
							CurrentSectionpNodeDataList[sprayedProductIndex].publicationID 
							
						);
					}
				if(MediatorClass ::subSecSpraySttngs.HorizontalFlowForAllImageSprayFlag)
					DataSprayerPtr->setFlow(kTrue);
				else
					DataSprayerPtr->setFlow(kFalse);

				this->startSpraying();

				//progressBar.SetPosition(sprayedProductIndex-1);
				progressBar.SetPosition(sprayedProductIndex);
				
				numProductsSprayed++;

				if(sprayedProductIndex==numProducts)
				{
					sprayingDone = kTrue;
					break;
				}
			}

			if(sprayingDone)
				break;
		}

		numProducts = numProductsSprayed;
	}
	while(kFalse);
}



void SubSectionSprayer::sprayPageWithResizableFrame(const UIDRef& pageUIDRef, const UIDList& selectedUIDList, const PMRect& origMaxBoxBounds, const PBPMPoint& maxPageSprayCount, int32& numProducts, RangeProgressBar& progressBar, bool16 toggleFlag)
{
 do
 {
	PMRect marginBoxBounds;
	bool16 result = kFalse;
	int32 numProductsSprayed=0;
	bool16 sprayingDone = kFalse;

	//CMM Comment
	/*
	InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
	if(iSSSprayer==nil)
	{
		CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
		return;
	}
	*/

	result = this->getMarginBounds(pageUIDRef, marginBoxBounds);
	if(result == kFalse)
	{
		result = this->getPageBounds(pageUIDRef, marginBoxBounds);
		if(result == kFalse)
			break;
	}

	/*PMString temp;
	temp.AppendNumber(iSSSprayer->getTopMargin());
	CA(temp);*/

	//CMM Comment
	/*
	marginBoxBounds.Left() += iSSSprayer->getLeftMargin();
	marginBoxBounds.Right() -= iSSSprayer->getRightMargin();
	marginBoxBounds.Top() += iSSSprayer->getTopMargin();
	marginBoxBounds.Bottom() -= iSSSprayer->getBottomMargin();
	*/
	marginBoxBounds.Left() += 0.0;
	marginBoxBounds.Right() -= 0.0;
	marginBoxBounds.Top() += 0.0;
	marginBoxBounds.Bottom() -= 0.0;

	int32 tempNumProducts=0;
	vector<double> tempIdList;
	tempIdList.clear();

	PMReal OrgBoxMaxWidth = 0.0, OrgBoxMaxHeight=0.0, maxPageWidth=0.0, maxPageHeight=0.0;
	
	maxPageWidth = abs(marginBoxBounds.Right() - marginBoxBounds.Left());
	maxPageHeight = abs(marginBoxBounds.Bottom() - marginBoxBounds.Top());
	/*PMString ZXC2("maxPageHeight : ");
	ZXC2.AppendNumber(maxPageHeight);
	CA(ZXC2);*/
	
	//CMM Comment PMReal VerticalBoxSpacing = iSSSprayer->getVerticalBoxSpacing();	
	PMReal VerticalBoxSpacing = MediatorClass ::subSecSpraySttngs.verticalBoxSpacing;	

	OrgBoxMaxWidth = abs(origMaxBoxBounds.Right() - origMaxBoxBounds.Left());
	OrgBoxMaxHeight = abs(origMaxBoxBounds.Bottom() - origMaxBoxBounds.Top());

	/*PMString ZXC("OrgBoxMaxHeight : ");
	ZXC.AppendNumber(OrgBoxMaxHeight);
	CA(ZXC);*/

	result = this->getAllIdForLevel(numProducts, tempIdList);

	int16 maxVertCnt=0, maxHorizCnt=0;
	/*if(iSSSprayer->getHorizFlowType()==kFalse)
	{
		maxVertCnt = ToInt16(maxPageSprayCount.X());
		maxHorizCnt = ToInt16(maxPageSprayCount.Y());
	}
	else*/
	{
		maxVertCnt = ToInt16(maxPageSprayCount.Y());
		maxHorizCnt = ToInt16(maxPageSprayCount.X());
	}

	int16 CurrVertCnt=0, CurrHorizCnt=0;
	
	int Condition = 1;
	int16 horizCnt = 0; 
	int16 vertCnt = 0;
	FrameBoundsList ProdBlockBoundList;
	ProdBlockBoundList.clear();

	while(Condition)
	{		
		bool16 GoToNewPage = kFalse;
		//InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		//InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
		//if (!layoutSelectionSuite) {
		//	break;
		//}
		//selectionManager->DeselectAll(nil); // deselect every active CSB
		//	
		////layoutSelectionSuite->Select(selectedUIDList, Selection::kReplace,  Selection::kDontScrollSelection);
		//layoutSelectionSuite->Select(selectedUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);		
		int16 tempHorizCnt=0, tempVertCnt=0;	
		
		
		//CMM Comment if(iSSSprayer->getHorizFlowType())
		if(MediatorClass ::subSecSpraySttngs.flowHorizontalFlow )
		{
			for(int16 horizCnt=0; horizCnt<maxHorizCnt; horizCnt++)
			{
				InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
				InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
				if (!layoutSelectionSuite) {
					break;
				}
				selectionManager->DeselectAll(nil); // deselect every active CSB
					
				//layoutSelectionSuite->Select(selectedUIDList, Selection::kReplace,  Selection::kDontScrollSelection);
				layoutSelectionSuite->/*Select*/SelectPageItems (selectedUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
					
				if(vertCnt > 0 && ProdBlockBoundList.size()!= 0 )
				{
					bool16 result1 = kFalse;
					//PMRect ProductBlockMaxBoxBounds;
					PMReal TotalHeight=0.0;		

					for(int p=0; p<ProdBlockBoundList.size(); p++)
					{
						if(ProdBlockBoundList[p].HorzCnt ==horizCnt)
						{						
							TotalHeight += abs(ProdBlockBoundList[p].BoxBounds.Bottom() - ProdBlockBoundList[p].BoxBounds.Top() + VerticalBoxSpacing);					
						}
					}
					/*PMString ZXC1("(maxPageHeight - TotalHeight ) : ");
					ZXC1.AppendNumber((maxPageHeight - TotalHeight ));
					CA(ZXC1);*/
					if((OrgBoxMaxHeight) > (maxPageHeight - TotalHeight ) )
					{
						//CA("Vertical Area insuffisient please move to next Page");
						if(horizCnt == maxHorizCnt-1)
						{
							//CA("Condition = 0");
							Condition = 0;
						}	
						continue;
					}
				}
				
				//CMM Comment if(iSSSprayer->getAlternatingVal())
				if(MediatorClass ::subSecSpraySttngs.alternateForEachPage )
				{			
					//CMM Comment if(iSSSprayer->getLeftToRightVal()==kTrue)
					if(MediatorClass ::subSecSpraySttngs.flowLeftToRight == kTrue)
					{
						if(toggleFlag)
							tempHorizCnt = (maxHorizCnt-1) - horizCnt;
						else
							tempHorizCnt = horizCnt;
					}
					else
					{
						if(toggleFlag)
							tempHorizCnt = horizCnt;
						else
							tempHorizCnt = (maxHorizCnt-1) - horizCnt;
					}			
				}
				else
				{
					//CMM Comment if(iSSSprayer->getLeftToRightVal()==kFalse)
					if(MediatorClass ::subSecSpraySttngs.flowLeftToRight == kFalse)
					{
						tempHorizCnt = (maxHorizCnt-1) - horizCnt;
					}
					else
						tempHorizCnt = horizCnt;						
				}
				tempVertCnt = vertCnt;

				PBPMPoint moveToPoints;
				if(tempVertCnt == 0)
					result = this->getBoxPosition(marginBoxBounds, origMaxBoxBounds, tempHorizCnt, tempVertCnt, moveToPoints);
				else
					result = this->getBoxPositionForResizableFrame(marginBoxBounds, origMaxBoxBounds, tempHorizCnt, tempVertCnt, ProdBlockBoundList, moveToPoints);
				if(!result)
				{
					ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayPageWithResizableFrame::getBoxPositionForResizableFrame is kFalse");
					continue;
				}

				if(tempHorizCnt>0)
				{
					for(int32 i=0;i<tempHorizCnt; i++)
					{
						//CMM Comment moveToPoints.X() += iSSSprayer->getHorizontalBoxSpacing();
						moveToPoints.X() += MediatorClass ::subSecSpraySttngs.horizontalBoxSpacing;
					}
				}
				
				//copy the selected items
				this->CopySelectedItems();

				//now get the copied item list
				UIDList copiedBoxUIDList;
				result = this->getSelectedBoxIds(copiedBoxUIDList);
				if(result == kFalse)
					break;

				//now move boxes to appropriate positions
				this->moveBoxes(copiedBoxUIDList, moveToPoints);

				vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
				PMRect CopiedItemMaxBoxBoundsBforeSpray;
				result = kFalse;
				result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);


				//spray for the selected boxes which were just copied
				//start with the second product
				this->getAllBoxIds(copiedBoxUIDList);
				
				//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				
				//treeCache.isExist(tempIdList[sprayedProductIndex], node);				
				PMString tempString("Spraying ");
				//tempString +=pNodeDataList[sprayedProductIndex].getName();
				PMString ProductName("");
                
                ProductName = CurrentSectionpNodeDataList[sprayedProductIndex].getName();

				tempString +=ProductName;
				tempString += "...";
				progressBar.SetTaskText(tempString);

				//InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
				if(!DataSprayerPtr)
				{
					ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayPageWithResizableFrame::Pointre to DataSprayerPtr not found");
					return;
				}
				
				/*PMString ASD("sprayedProductIndex Type ID: ");
				ASD.AppendNumber(pNodeDataList[sprayedProductIndex].getTypeId());
				CA(ASD);*/
				//DataSprayerPtr->FillPnodeStruct(pNodeDataList[sprayedProductIndex],CurrentSelectedSection,CurrentSelectedPublicationID);				
				//9May
					PublicationNode pNode =CurrentSectionpNodeDataList[sprayedProductIndex].convertToPublicationNode();
					if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kTrue)
					{
						DataSprayerPtr->FillPnodeStruct
						(pNode,CurrentSelectedSection,CurrentSelectedPublicationID, CurrentSelectedSubSection);	

					}
					else
					{
						//CA("before fillpnode");
					DataSprayerPtr->FillPnodeStruct
						(	pNode,
							CurrentSectionpNodeDataList[sprayedProductIndex].sectionID,
							CurrentSectionpNodeDataList[sprayedProductIndex].publicationID, 
							CurrentSectionpNodeDataList[sprayedProductIndex].subSectionID
						);
						//CA("after fillpnode");
					}				

				//DataSprayerPtr->getAllIds(pNodeDataList[sprayedProductIndex].getPubId());//For these PF, PR, PG ITEM ID's we have to spray the data
				//DataSprayerPtr->getAllIds(CurrentSectionpNodeDataList[sprayedProductIndex].getPubId());//For these PF, PR, PG ITEM ID's we have to spray the data
				
				this->startSpraying();
				

				UIDList TempCopiedBoxUIDList(copiedBoxUIDList.GetDataBase());
				
				for(int32 i=0; i<vectorCopiedBoxBoundsBforeSpray.size(); i++)
				{		
					//InterfacePtr<ITagReader> itagReader
						//((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
					if(!itagReader){
					return ;
					}
					
					for(int32 j=0; j<vectorCopiedBoxBoundsBforeSpray.size(); j++)
					{
						if((vectorCopiedBoxBoundsBforeSpray[j].Left == vectorCopiedBoxBoundsBforeSpray[i].Left) && ( vectorCopiedBoxBoundsBforeSpray[j].Right == vectorCopiedBoxBoundsBforeSpray[i].Right)&& ( vectorCopiedBoxBoundsBforeSpray[j].Top == vectorCopiedBoxBoundsBforeSpray[i].Top) && ( vectorCopiedBoxBoundsBforeSpray[j].Bottom == vectorCopiedBoxBoundsBforeSpray[i].Bottom))
						{
							//CA("before continue");
							continue;
						}

						if((vectorCopiedBoxBoundsBforeSpray[j].Left <= vectorCopiedBoxBoundsBforeSpray[i].Left) && ( vectorCopiedBoxBoundsBforeSpray[j].Right >= vectorCopiedBoxBoundsBforeSpray[i].Right)&& ( vectorCopiedBoxBoundsBforeSpray[j].Top <= vectorCopiedBoxBoundsBforeSpray[i].Top) && ( vectorCopiedBoxBoundsBforeSpray[j].Bottom >= vectorCopiedBoxBoundsBforeSpray[i].Bottom))
						{	

							TagList tList = itagReader->getTagsFromBox_ForRefresh(vectorCopiedBoxBoundsBforeSpray[i].BoxUIDRef);
							if(tList.size() == 0)
							{
								//CA("before continue");
								continue;
							}
							
							if(tList[0].imgFlag == 1 && tList[0].isAutoResize == 1)
							{	
								TempCopiedBoxUIDList.Append(vectorCopiedBoxBoundsBforeSpray[j].BoxUIDRef.GetUID());
								TempCopiedBoxUIDList.Append(vectorCopiedBoxBoundsBforeSpray[i].BoxUIDRef.GetUID());
							}

							//******Added
							for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
							{
								tList[tagIndex].tagPtr->Release();
							}
							//******
						}
					
					}							
				}
				
				UIDList processedItems;
				if(TempCopiedBoxUIDList.Length()> 0)
				{
					K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(TempCopiedBoxUIDList));
					ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
					//CA("3333");
				}

				selectionManager->DeselectAll(nil); // deselect every active CSB
					
				//layoutSelectionSuite->Select(selectedUIDList, Selection::kReplace,  Selection::kDontScrollSelection);
				layoutSelectionSuite->/*Select*/SelectPageItems (copiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
				
				//InterfacePtr<ITagReader> itagReader
					//((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
				if(!itagReader){
				return ;
				}				

				
								
				this->moveAutoResizeBoxAfterSpray(copiedBoxUIDList, vectorCopiedBoxBoundsBforeSpray);
				
				PMRect CopiedItemMaxBoxBounds;
				vectorBoxBounds vectorCopiedBoxBounds;
				result = kFalse;
				result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, CopiedItemMaxBoxBounds, vectorCopiedBoxBounds);
				if(result == kFalse)
				{
					ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayPageWithResizableFrame::getMaxLimitsOfBoxes::result == kFalse");
					break;
				}

				if(marginBoxBounds.Bottom() - CopiedItemMaxBoxBounds.Bottom() < 0 )
				{
					if(vertCnt != 0)
					{
						this->deleteThisBoxUIDList(copiedBoxUIDList);
						//CA("After Deleting Boxes");
						if(horizCnt == maxHorizCnt-1)
						{
							//CA("Condition = 0");
							Condition = 0;
						}
						//CA("before continue");
						continue;
					}
				}
			
				sprayedProductIndex++;
				progressBar.SetPosition(sprayedProductIndex);
				numProductsSprayed++;
				if(sprayedProductIndex==numProducts)
				{	
					//CA("sprayedProductIndex==numProducts");
					Condition = 0;
					sprayingDone = kTrue;
					break;
				}

				//CreateMediaInsert
				if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kFalse)
				{
					//for CAlert
					//PMString num;
					//num.AppendNumber(CurrentSectionpNodeDataList[sprayedProductIndex].pageno);
					//CA("next prod page no is : " + num);
					//end for CAlert
					if(CurrentSectionpNodeDataList[sprayedProductIndex].pageno != pageNoFromDb)
					{
						pageNoFromDb = CurrentSectionpNodeDataList[sprayedProductIndex].pageno;
						//CA("New page product");
						Condition = 0;
						break;
					}
				}
				//end CreateMediaInsert
			
				
				DynFrameStruct CurrentFameStruct;
				CurrentFameStruct.HorzCnt = tempHorizCnt;
				CurrentFameStruct.VertCnt = tempVertCnt;
				CurrentFameStruct.BoxBounds = CopiedItemMaxBoxBounds;
				ProdBlockBoundList.push_back(CurrentFameStruct);
				//CA("33");
			}				

			vertCnt++;
		}		
		//CMM Comment else if(!iSSSprayer->getHorizFlowType())
		else if(!MediatorClass ::subSecSpraySttngs.flowHorizontalFlow)
		{	
			int RowCount =0;
			/*PMString ASD("maxHorizCnt : ");
			ASD.AppendNumber(maxHorizCnt);
			CA(ASD);*/

			for(int16 horizCnt=0; horizCnt<maxHorizCnt; horizCnt++)
			{//for 	
				int condition1 = 1;
				tempVertCnt = 0;
				RowCount++; 
				while(condition1)
				{// while x
					InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
					if (!layoutSelectionSuite) {
						break;
					}
					selectionManager->DeselectAll(nil); // deselect every active CSB
						
					//layoutSelectionSuite->Select(selectedUIDList, Selection::kReplace,  Selection::kDontScrollSelection);
					layoutSelectionSuite->/*Select*/SelectPageItems (selectedUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
					if(tempVertCnt > 0 && ProdBlockBoundList.size()!= 0 )
					{
						bool16 result1 = kFalse;						
						PMReal TotalHeight=0.0;		
						for(int p=0; p<ProdBlockBoundList.size(); p++)
						{
							if(ProdBlockBoundList[p].HorzCnt ==tempHorizCnt)
							{						
								TotalHeight += abs(ProdBlockBoundList[p].BoxBounds.Bottom() - ProdBlockBoundList[p].BoxBounds.Top() + VerticalBoxSpacing);					
							}
						}
						/*PMString ZXC1("(maxPageHeight - TotalHeight ) : ");
						ZXC1.AppendNumber((maxPageHeight - TotalHeight ));
						CA(ZXC1);*/
						if((OrgBoxMaxHeight) > (maxPageHeight - TotalHeight ) )
						{
							//CA("Vertical Area insuffisient please move to next Page");
							condition1 =0;
							if(RowCount < maxHorizCnt)
							continue;
							else
							{
								Condition =0;
								condition1=0;
								GoToNewPage = kTrue;
								//CA("GoToNewPage = kTrue");
								break;
							}
						}
					}
					//CMM Comment if(iSSSprayer->getAlternatingVal())
					if(MediatorClass ::subSecSpraySttngs.alternateForEachPage)
					{			
						//CMM Comment if(iSSSprayer->getLeftToRightVal()==kTrue)
						if(MediatorClass ::subSecSpraySttngs.flowLeftToRight == kTrue)
						{
							if(toggleFlag)
								tempHorizCnt = (maxHorizCnt-1) - horizCnt;
							else
								tempHorizCnt = horizCnt;
						}
						else
						{
							if(toggleFlag)
								tempHorizCnt = horizCnt;
							else
								tempHorizCnt = (maxHorizCnt-1) - horizCnt;
						}			
					}
					else
					{
						//CMM Comment if(iSSSprayer->getLeftToRightVal()==kFalse)
						if(MediatorClass ::subSecSpraySttngs.flowLeftToRight == kFalse)
						{
							tempHorizCnt = (maxHorizCnt-1) - horizCnt;
						}
						else
							tempHorizCnt = horizCnt;						
					}
					//tempVertCnt = vertCnt;

					PBPMPoint moveToPoints;
					if(tempVertCnt == 0)
					result = this->getBoxPosition(marginBoxBounds, origMaxBoxBounds, tempHorizCnt, tempVertCnt, moveToPoints);
					else
					result = this->getBoxPositionForResizableFrame(marginBoxBounds, origMaxBoxBounds, tempHorizCnt, tempVertCnt, ProdBlockBoundList, moveToPoints);
					if(!result)
					{
						//CA("ignored");
						continue;
					}

					/*if(tempVertCnt>0)  // Not Required
					{
						for(int32 i=0;i<tempVertCnt; i++)
							moveToPoints.Y() += iSSSprayer->getVerticalBoxSpacing();
					}*/
					if(tempHorizCnt>0)
					{
						for(int32 i=0;i<tempHorizCnt; i++)
						{
							//CMM Comment moveToPoints.X() += iSSSprayer->getHorizontalBoxSpacing();
							moveToPoints.X() += MediatorClass ::subSecSpraySttngs.horizontalBoxSpacing;
						}
					}
					
					//copy the selected items
					this->CopySelectedItems();

					//now get the copied item list
					UIDList copiedBoxUIDList;
					result = this->getSelectedBoxIds(copiedBoxUIDList);
					if(result == kFalse)
						break;

					//now move boxes to appropriate positions
					this->moveBoxes(copiedBoxUIDList, moveToPoints);

					vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
					PMRect CopiedItemMaxBoxBoundsBforeSpray;
					result = kFalse;
					result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);

					
					//spray for the selected boxes which were just copied
					//start with the second product
					this->getAllBoxIds(copiedBoxUIDList);
					
					//treeCache.isExist(tempIdList[sprayedProductIndex], node);				
					//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
					
					PMString tempString("Spraying ");
					//tempString +=pNodeDataList[sprayedProductIndex].getName();
					PMString ProductName("");
					ProductName = CurrentSectionpNodeDataList[sprayedProductIndex].getName();
					tempString +=ProductName;	
					tempString += "...";
					progressBar.SetTaskText(tempString);

					//InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
					if(!DataSprayerPtr)
					{
						ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayPageWithResizableFrame::Pointre to DataSprayerPtr not found");
						return;
					}
					/*PMString ASD("Publication ID : ");
					ASD.AppendNumber(CurrentSelectedPublicationID);
					CA(ASD);*/
					
					//DataSprayerPtr->FillPnodeStruct(pNodeDataList[sprayedProductIndex],CurrentSelectedSection,CurrentSelectedPublicationID);				
					//DataSprayerPtr->FillPnodeStruct(CurrentSectionpNodeDataList[sprayedProductIndex],CurrentSelectedSection,CurrentSelectedPublicationID, CurrentSelectedSubSection);				
					//9May
					PublicationNode pNode =CurrentSectionpNodeDataList[sprayedProductIndex].convertToPublicationNode();
					if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kTrue)
					{
						DataSprayerPtr->FillPnodeStruct
						(pNode,CurrentSelectedSection,CurrentSelectedPublicationID, CurrentSelectedSubSection);	

					}
					else
					{
					DataSprayerPtr->FillPnodeStruct
						(	pNode,
							CurrentSectionpNodeDataList[sprayedProductIndex].sectionID,
							CurrentSectionpNodeDataList[sprayedProductIndex].publicationID, 
							CurrentSectionpNodeDataList[sprayedProductIndex].subSectionID
						);
					}
					this->startSpraying();

					UIDList TempCopiedBoxUIDList(copiedBoxUIDList.GetDataBase());

					for(int32 i=0; i<vectorCopiedBoxBoundsBforeSpray.size(); i++)
					{		
						//InterfacePtr<ITagReader> itagReader
							//((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
						if(!itagReader){
						return ;
						}
						
						for(int32 j=0; j<vectorCopiedBoxBoundsBforeSpray.size(); j++)
						{
							if((vectorCopiedBoxBoundsBforeSpray[j].Left == vectorCopiedBoxBoundsBforeSpray[i].Left) && ( vectorCopiedBoxBoundsBforeSpray[j].Right == vectorCopiedBoxBoundsBforeSpray[i].Right)&& ( vectorCopiedBoxBoundsBforeSpray[j].Top == vectorCopiedBoxBoundsBforeSpray[i].Top) && ( vectorCopiedBoxBoundsBforeSpray[j].Bottom == vectorCopiedBoxBoundsBforeSpray[i].Bottom))
							{
								//CA("before continue");
								continue;
							}

							if((vectorCopiedBoxBoundsBforeSpray[j].Left <= vectorCopiedBoxBoundsBforeSpray[i].Left) && ( vectorCopiedBoxBoundsBforeSpray[j].Right >= vectorCopiedBoxBoundsBforeSpray[i].Right)&& ( vectorCopiedBoxBoundsBforeSpray[j].Top <= vectorCopiedBoxBoundsBforeSpray[i].Top) && ( vectorCopiedBoxBoundsBforeSpray[j].Bottom >= vectorCopiedBoxBoundsBforeSpray[i].Bottom))
							{	

								TagList tList = itagReader->getTagsFromBox_ForRefresh(vectorCopiedBoxBoundsBforeSpray[i].BoxUIDRef);
								if(tList.size() == 0)
									continue;
								
								if(tList[0].imgFlag == 1 && tList[0].isAutoResize == 1)
								{	
									TempCopiedBoxUIDList.Append(vectorCopiedBoxBoundsBforeSpray[j].BoxUIDRef.GetUID());
									TempCopiedBoxUIDList.Append(vectorCopiedBoxBoundsBforeSpray[i].BoxUIDRef.GetUID());
								}

								//******Added
								for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
								{
									tList[tagIndex].tagPtr->Release();
								}
								//******

							}
						
						}							
					}

					
					UIDList processedItems;
					if(TempCopiedBoxUIDList.Length()> 0)
					{
						K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(TempCopiedBoxUIDList));
						ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
						//CA("3333");
					}


					selectionManager->DeselectAll(nil); // deselect every active CSB
					
					// Table Style Functionality is commented here
					////layoutSelectionSuite->Select(selectedUIDList, Selection::kReplace,  Selection::kDontScrollSelection);
					//layoutSelectionSuite->Select(copiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	

					//TableStyleUtils TabStyleObj;
					//TabStyleObj.SetTableModel(kFalse);
					//TabStyleObj.ApplyTableStyle();
					//TabStyleObj.setTableStyle();

					//progressBar.SetPosition(sprayedProductIndex-1);
					
					
					numProductsSprayed++;
					this->moveAutoResizeBoxAfterSpray(copiedBoxUIDList, vectorCopiedBoxBoundsBforeSpray);

					PMRect CopiedItemMaxBoxBounds;
					vectorBoxBounds vectorCopiedBoxBounds;
					result = kFalse;
					result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, CopiedItemMaxBoxBounds, vectorCopiedBoxBounds);
					if(result == kFalse)
						break;

					if(marginBoxBounds.Bottom() - CopiedItemMaxBoxBounds.Bottom() < 0 )
					{
						if(tempVertCnt != 0)
						{
							this->deleteThisBoxUIDList(copiedBoxUIDList);
							//CA("After Deleting Boxes");
							if(horizCnt == maxHorizCnt-1)
							{
								Condition = 0;
								condition1=0;
								GoToNewPage = kTrue;
							}
							//CA("before break ");
							break;
						}
					}
					sprayedProductIndex++;
					progressBar.SetPosition(sprayedProductIndex);

					if(sprayedProductIndex==numProducts)
					{							
						condition1=0;
						sprayingDone = kTrue;
						//CA("before break ");
						break;
					}
					//CreateMediaInsert
					if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kFalse)
					{//if y
						//for CAlert
						PMString num;
						num.AppendNumber(CurrentSectionpNodeDataList[sprayedProductIndex].pageno);
						//CA("next prod page no is : " + num);
						//end for CAlert
						if(CurrentSectionpNodeDataList[sprayedProductIndex].pageno != pageNoFromDb)
						{//if z
							pageNoFromDb = CurrentSectionpNodeDataList[sprayedProductIndex].pageno;
							//CA("New page product");
							Condition = 0;
							condition1=0;
							break;
						}// if z
					}//if y
				//end CreateMediaInsert
										
					DynFrameStruct CurrentFameStruct;
					CurrentFameStruct.HorzCnt = tempHorizCnt;
					CurrentFameStruct.VertCnt = tempVertCnt;
					CurrentFameStruct.BoxBounds = CopiedItemMaxBoxBounds;
					ProdBlockBoundList.push_back(CurrentFameStruct);
					tempVertCnt++;
					
				}//while x
				if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kFalse)
				{//if yy
					if(Condition == 0 && condition1==0)
					{
						break;
					}

				}//if yy
				if(GoToNewPage || sprayingDone)
				{
					Condition=0;
					//CA("before break");34e
					break;
				}
			}
			Condition=0;
		}



		/*if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kFalse)
		{
			//for CAlert
			PMString num;
			num.AppendNumber(CurrentSectionpNodeDataList[sprayedProductIndex].pageno);
			CA("next prod page no is : " + num);
			//end for CAlert
			if(CurrentSectionpNodeDataList[sprayedProductIndex].pageno != pageNoFromDb)
			{
				CA("New page product");
				Condition = 0;
			}
		}*/
	}

	numProducts = numProductsSprayed;
 }while(kFalse);
}
/*bool16 SubSectionSprayer::willBoxesOverlap(const PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, const int16& horizCnt, const int16& vertCnt, PBPMPoint& moveToPoints)
{
	bool16 result = kTrue;
	
	do
	{
		PMReal maxWidth = origMaxBoxBounds.Right() - origMaxBoxBounds.Left();
		PMReal maxHeight = origMaxBoxBounds.Bottom() - origMaxBoxBounds.Top();

		PMReal offsetWidth = maxWidth * PMReal(horizCnt);
		PMReal offsetHeight = maxHeight * PMReal(vertCnt);
		
		PMReal left = (marginBoxBounds.Left() + offsetWidth);
		PMReal top = (marginBoxBounds.Top() + offsetHeight);
		PMReal right = (left + maxWidth);
		PMReal bottom = (top + maxHeight);

		if(left >= origMaxBoxBounds.Left() && top >= origMaxBoxBounds.Top())
		{
			if(left <= origMaxBoxBounds.Right() && top <= origMaxBoxBounds.Bottom())
				break;
		}

		if(right >= origMaxBoxBounds.Left() && bottom >= origMaxBoxBounds.Top())
		{
			if(right <= origMaxBoxBounds.Right() && bottom <= origMaxBoxBounds.Bottom())
				break;
		}

		if(left >= origMaxBoxBounds.Left() && bottom >= origMaxBoxBounds.Top())
		{
			if(left <= origMaxBoxBounds.Right() && bottom <= origMaxBoxBounds.Bottom())
				break;
		}

		if(right >= origMaxBoxBounds.Left() && top >= origMaxBoxBounds.Top())
		{
			if(right <= origMaxBoxBounds.Right() && top <= origMaxBoxBounds.Bottom())
				break;
		}

		const PBPMPoint points(left, top);

		moveToPoints = points;

		result = kFalse;
	}
	while(kFalse);

	return result;
}*/

bool16 SubSectionSprayer::getBoxPosition(const PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, const int16& horizCnt, const int16& vertCnt, PBPMPoint& moveToPoints)
{
	bool16 result = kFalse;
	
	do
    {
//PMString ASD("origMaxBoxBounds.Top() : ");
//	ASD.AppendNumber((origMaxBoxBounds.Top()));
//	ASD.Append("  origMaxBoxBounds.Left() : ");
//	ASD.AppendNumber((origMaxBoxBounds.Left()));
//	ASD.Append("  origMaxBoxBounds.Bottom() : ");
//	ASD.AppendNumber((origMaxBoxBounds.Bottom()));
//	ASD.Append("  origMaxBoxBounds.Right() : ");
//	ASD.AppendNumber((origMaxBoxBounds.Right()));
//	CA(ASD);

		PMReal maxWidth = origMaxBoxBounds.Right() - origMaxBoxBounds.Left();
		PMReal maxHeight = origMaxBoxBounds.Bottom() - origMaxBoxBounds.Top();

		PMReal offsetWidth = maxWidth * PMReal(horizCnt);
		PMReal offsetHeight = maxHeight * PMReal(vertCnt);
		
		PMReal left = (marginBoxBounds.Left() + offsetWidth);
		PMReal top = (marginBoxBounds.Top() + offsetHeight);
		/*PMReal right = (left + maxWidth);
		PMReal bottom = (top + maxHeight);*/

		/*PMString QWE(" left : ");
		QWE.AppendNumber(left);
		QWE.Append(" top : ");
		QWE.AppendNumber(top);
		CA(QWE);*/

		const PBPMPoint points(left, top);
		moveToPoints = points;
		result = kTrue;
	}
	while(kFalse);

	return result;
}

bool16 SubSectionSprayer::getBoxPositionForResizableFrame(const PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, const int16& horizCnt,const int16& vertCnt,  FrameBoundsList ProdBlockBoundList, PBPMPoint& moveToPoints)
{//CA("2");
	bool16 result = kFalse;
	
	do
	{
		//CMM Comment
		/*
		InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
		if(iSSSprayer==nil)
		{
			CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
			return kFalse;
		}
		*/

		PMReal maxWidth = origMaxBoxBounds.Right() - origMaxBoxBounds.Left();
		//PMReal maxHeight = origMaxBoxBounds.Bottom() - origMaxBoxBounds.Top();

		PMReal offsetWidth = maxWidth * PMReal(horizCnt);
		//PMReal offsetHeight = ProductBlockMaxBoxBounds.Bottom()/*-ProductBlockMaxBoxBounds.Top()*/;
		//		
		PMReal maxHeight = 0.0;
		//CMM Comment PMReal VerticalBoxSpacing = iSSSprayer->getVerticalBoxSpacing();
		PMReal VerticalBoxSpacing = MediatorClass ::subSecSpraySttngs.verticalBoxSpacing;

		for(int p=0; p<ProdBlockBoundList.size(); p++)
		{
			if(ProdBlockBoundList[p].HorzCnt == horizCnt)
			{						
				maxHeight += abs(ProdBlockBoundList[p].BoxBounds.Bottom() - ProdBlockBoundList[p].BoxBounds.Top());
				maxHeight += VerticalBoxSpacing;	
			}
		}

		PMReal left = (marginBoxBounds.Left() + offsetWidth);
		PMReal top = (marginBoxBounds.Top() + maxHeight);
		/*PMReal right = (left + maxWidth);
		PMReal bottom = (top + maxHeight);*/

		const PBPMPoint points(left, top);
		moveToPoints = points;
		result = kTrue;
	}
	while(kFalse);

	return result;


}
bool16 SubSectionSprayer::CopySelectedItems()
{

	ErrorCode status = kFailure;
	
	// We'll use a do-while(0) to break out on bad pointers:
	do
	{
		// Acquire the interfaces we need to use IScrapSuite:
		InterfacePtr<IScrapSuite> scrapSuite(static_cast<IScrapSuite*>(Utils<ISelectionUtils>()->QuerySuite(IID_ISCRAPSUITE)));
		if (scrapSuite == nil)
		{
			//CA("CopySelectedItems: scrapSuite invalid");
			//CA("BscCltCore::CopySelectedItems: scrapSuite invalid");
			break;
		}
		InterfacePtr<IClipboardController> clipController(GetExecutionContextSession()/*gSession*/, UseDefaultIID());//Cs4
		if (clipController == nil)
		{
			//CA("BscCltCore::CopySelectedItems: clipController invalid");
			break;
		}

		//InterfacePtr<IControlView> controlView(::QueryFrontView()); //Cs3
		InterfacePtr<IControlView> controlView(Utils<ILayoutUIUtils>()->QueryFrontView()); //Cs4
		if (controlView == nil)
		{
			//CA("BscCltCore::CopySelectedItems: controlView invalid"); 
			break;
		}


		// Copy and paste the selection:
		
		if(scrapSuite->CanCopy(clipController) != kTrue)
			break;
		if(scrapSuite->Copy(clipController) != kSuccess)
			break;
		if(scrapSuite->CanPaste(clipController) != kTrue)
			break;
		if(scrapSuite->Paste(clipController, controlView) != kSuccess)
			break;

		status = kSuccess;

	} while (false); // Only do once.

	bool16 errored = kFalse;
	if (status != kSuccess)
		errored = kTrue;

	return errored;
}

void SubSectionSprayer::moveBoxes(const UIDList& copiedBoxUIDList, const PBPMPoint& moveToPoints)
{
	do
	{
		PMRect curMaxBoxBounds;
		vectorBoxBounds vectorCurBoxBounds;
		bool8 result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, curMaxBoxBounds, vectorCurBoxBounds);
		if(result == kFalse)
			break;

		//PMString ASD1("moveToPoints.Y() : ");
		//ASD1.AppendNumber(moveToPoints.Y());
		//ASD1.Append(" curMaxBoxBounds.Top() : ");
		//ASD1.AppendNumber(curMaxBoxBounds.Top());
		//CA(ASD1);

		PMReal left = moveToPoints.X() - curMaxBoxBounds.Left();
		PMReal top = moveToPoints.Y() - curMaxBoxBounds.Top();

		/*PMString ASD2("top : ");
		ASD2.AppendNumber(top);
		CA(ASD2);*/
		const PBPMPoint moveByPoints(left, top);
		ErrorCode errorCode; //**Added	
		for(int32 i = copiedBoxUIDList.Length() - 1; i >= 0; --i)
		{
			UIDRef boxUIDRef = copiedBoxUIDList.GetRef(i);

			///***Commented By Sachin Sharma
			//InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
			//if(!transform)
			//{
			//	//CA("!transform");
			//	continue;
			//}
			//MovePageItemRelative(transform, moveByPoints);

			///*Added...
			UIDList moveUIDList(boxUIDRef);
			Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
			PBPMPoint referencePoint(PMPoint(0,0));
			errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( moveUIDList, coordinateSpace, referencePoint, Transform::TranslateBy(moveByPoints.X(),moveByPoints.Y())/*(0, top)*/);
		}

	}
	while(kFalse);
}

void SubSectionSprayer::moveAutoResizeBoxAfterSpray(const UIDList& copiedBoxUIDList, vectorBoxBounds vectorCurBoxBoundsBeforeSpray )
{
	do
	{
		PMRect curMaxBoxBounds;
		vectorBoxBounds vectorCurBoxBounds;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CA("ptrIAppFramework == NULL");
			return ;
		}
		
		bool8 result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, curMaxBoxBounds, vectorCurBoxBounds);
		if(result == kFalse){
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::moveAutoResizeBoxAfterSpray::getMaxLimitsOfBoxes::result==kFlase");		
			break;
		}

		PMReal MaxTop =  curMaxBoxBounds.Top();
		InterfacePtr<ITagReader> itagReader
			((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){
		return ;
		}	
///////////////
		/*PMString ASD("Size of vectorCurBoxBounds : ");
		ASD.AppendNumber(vectorCurBoxBounds.size());
		CA(ASD);*/

		vectorBoxBounds TempVectorBoxBounds;
		vectorBoxBounds TempCurrVectorBoxBoundsBeforeSpray;
		TempCurrVectorBoxBoundsBeforeSpray.clear();
		TempVectorBoxBounds.clear();
		vectorBoxBounds vectorCurBoxBoundsBeforeSprayAfterDeletes;
		if(vectorCurBoxBoundsBeforeSpray.size() != vectorCurBoxBounds.size())
		{
			//CA("Size Diffears");
			for(int32 p=0; p < vectorCurBoxBoundsBeforeSpray.size(); p++)
			{				
				for(int32 q=0; q < vectorCurBoxBounds.size(); q++)
				{
					if(vectorCurBoxBoundsBeforeSpray[p].BoxUIDRef.GetUID() == vectorCurBoxBounds[q].BoxUIDRef.GetUID())
					{						
						vectorCurBoxBoundsBeforeSprayAfterDeletes.push_back(vectorCurBoxBoundsBeforeSpray[p]);
						break;
					}
				}
			}
			if(vectorCurBoxBoundsBeforeSprayAfterDeletes.size() != vectorCurBoxBounds.size())
			{	
				//CA("Still Count Diffarers");
			}
			else
			{
				//CA("Now Count is same");
				vectorCurBoxBoundsBeforeSpray = vectorCurBoxBoundsBeforeSprayAfterDeletes;
			}
		}

		if(vectorCurBoxBoundsBeforeSpray.size() == vectorCurBoxBounds.size())
		{
			for(int32 i=0; i<vectorCurBoxBounds.size(); i++)
			{	
				//if((vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) < 0)
				//{  // When Sprayed Box Height is less than Original Box height
				//	CA("Less Than 0");
					
					TagList tList = itagReader->getFrameTags(vectorCurBoxBounds[i].BoxUIDRef);
					if(tList.size() == 0)
						continue;

					bool16 Flag = kFalse;	
					for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
					{
						if((vectorCurBoxBounds[j].Left == vectorCurBoxBounds[i].Left) && ( vectorCurBoxBounds[j].Right == vectorCurBoxBounds[i].Right)&& ( vectorCurBoxBounds[j].Top == vectorCurBoxBounds[i].Top) && ( vectorCurBoxBounds[j].Bottom == vectorCurBoxBounds[i].Bottom))
						{
							continue;
						}

						if((vectorCurBoxBounds[j].Left <= vectorCurBoxBounds[i].Left) && ( vectorCurBoxBounds[j].Right >= vectorCurBoxBounds[i].Right)&& ( vectorCurBoxBounds[j].Top <= vectorCurBoxBounds[i].Top) && ( vectorCurBoxBounds[j].Bottom >= vectorCurBoxBounds[i].Bottom))
						{	
							/*PMString QWE(" vectorCurBoxBounds[j].Left : " );
							QWE.AppendNumber(vectorCurBoxBounds[j].Left);
							QWE.Append("  vectorCurBoxBounds[i].Left : ");
							QWE.AppendNumber(vectorCurBoxBounds[i].Left);
							QWE.Append(" vectorCurBoxBounds[j].Right : " );
							QWE.AppendNumber(vectorCurBoxBounds[j].Right);
							QWE.Append("  vectorCurBoxBounds[i].Right : ");
							QWE.AppendNumber(vectorCurBoxBounds[i].Right);
							QWE.Append(" vectorCurBoxBounds[j].Top : " );
							QWE.AppendNumber(vectorCurBoxBounds[j].Top);
							QWE.Append("  vectorCurBoxBounds[i].Top : ");
							QWE.AppendNumber(vectorCurBoxBounds[i].Top);
							QWE.Append(" vectorCurBoxBounds[j].Bottom : " );
							QWE.AppendNumber(vectorCurBoxBounds[j].Bottom);
							QWE.Append("  vectorCurBoxBounds[i].Bottom : ");
							QWE.AppendNumber(vectorCurBoxBounds[i].Bottom);
							CA(QWE);*/

							if(tList[0].imgFlag == 1)
							{	
								//CA("Current Box is Inside Other Box");
								Flag = kTrue;	
							}
						}
					}
					if(Flag== kFalse){					
						vectorCurBoxBounds[i].isUnderImage = kFalse;
						TempVectorBoxBounds.push_back(vectorCurBoxBounds[i]);
						TempCurrVectorBoxBoundsBeforeSpray.push_back(vectorCurBoxBoundsBeforeSpray[i]);
					}

					//******Added
					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
					{
						tList[tagIndex].tagPtr->Release();
					}
					//******
					continue;
				//}
				
			}
			vectorCurBoxBounds = TempVectorBoxBounds;
			vectorCurBoxBoundsBeforeSpray = TempCurrVectorBoxBoundsBeforeSpray;
		}
		else
		{   // vectorCurBoxBoundsBeforeSpray.size() != vectorCurBoxBounds.size()
			return;
		}
		/*ASD.Clear();
		ASD.Append("Size of vectorCurBoxBounds : ");
		ASD.AppendNumber(vectorCurBoxBounds.size());
		CA(ASD);*/

///////////////
		
			for(int32 i=0; i<vectorCurBoxBounds.size(); i++)
			{
				if((vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) == 0)
				{	//CA("Equal");
					TagList tList = itagReader->getFrameTags(vectorCurBoxBounds[i].BoxUIDRef);
					if(tList.size() == 0)
						continue;
					else if(tList[0].imgFlag == 1)
					{	
						for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
						{
							if((vectorCurBoxBounds[j].Left > vectorCurBoxBounds[i].Right) || ( vectorCurBoxBounds[j].Right < vectorCurBoxBounds[i].Left))
							{	//CA("shiftUP = kTrue");
								vectorCurBoxBounds[j].shiftUP = kTrue;
								vectorCurBoxBounds[j].isUnderImage = kFalse;
							}
							else
							{	//CA("shiftUP = kFalse");
								vectorCurBoxBounds[j].shiftUP = kFalse;
								vectorCurBoxBounds[j].isUnderImage = kTrue;
							}
						}
					}
					//******Added
					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
					{
						tList[tagIndex].tagPtr->Release();
					}
					//******
					continue;
				}
				else if((vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) < 0)
				{  // When Sprayed Box Height is less than Original Box height
					//CA("Less Than 0");
					
					TagList tList = itagReader->getFrameTags(vectorCurBoxBounds[i].BoxUIDRef);
					if(tList.size() == 0)
						continue;

					if(tList[0].imgFlag == 1)
					{	
						PMReal IncreaseByPoints = abs(vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- abs(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top);
					
						for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
						{	//CA("0000");
							if(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[j].Top <0)
							{	//CA("0001");
								if((vectorCurBoxBounds[j].Left > vectorCurBoxBounds[i].Right) || ( vectorCurBoxBounds[j].Right < vectorCurBoxBounds[i].Left))
								{	//CA("Before Continue");
									vectorCurBoxBounds[j].isUnderImage = kFalse;
									continue;
								}
								else
								{	
									PMReal top = (IncreaseByPoints);
									vectorCurBoxBounds[j].compressShift = top;	
									vectorCurBoxBounds[j].shiftUP = kTrue;
									vectorCurBoxBounds[j].isUnderImage = kTrue;	
									//CA("UnderImage true");
								}
							}
						}
					}



					//******Added
					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
					{
						tList[tagIndex].tagPtr->Release();
					}
					//******
				}
				//else if( (vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) > 0)
				//{ 
				//	// When Sprayed Box Height is Greater Than Original Box Height
				//	//CA("Greater Than 0");
				//	PMReal IncreaseByPoints = abs(vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- abs(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top);
				//	for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
				//	{	
				//		if(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[j].Top <0)
				//		{	
				//			if((vectorCurBoxBounds[j].Left > vectorCurBoxBounds[i].Right) || ( vectorCurBoxBounds[j].Right < vectorCurBoxBounds[i].Left))
				//			{
				//				continue;
				//			}
				//			else
				//			{	//CA("1");
				//				PMReal top = IncreaseByPoints;									
				//				vectorCurBoxBounds[j].enlargeShift += top;								
				//			}
				//		}
				//	}
    //            	
				//}
				
			}
		

///// Now Move the Frames 
//		for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
//		{
//			UIDRef boxUIDRef = vectorCurBoxBounds[j].BoxUIDRef;
//			PMReal TotalShift = /*vectorCurBoxBounds[j].compressShift +*/ vectorCurBoxBounds[j].enlargeShift;
//
//			const PBPMPoint moveByPoints(0, TotalShift);
//
//			InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
//			MovePageItemRelative(transform, moveByPoints);
//
//		}
///////////// 	
		
	
			for(int32 i=0; i<vectorCurBoxBounds.size(); i++)
			{
				if((vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) == 0)
				{	//CA("Equal");					
					continue;				
				}
				else if((vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) < 0)
				{  // When Sprayed Box Height is less than Original Box height
					//CA("Less Than 0");
					
					PMReal IncreaseByPoints = abs(vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- abs(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top);
					
					TagList tList = itagReader->getFrameTags(vectorCurBoxBounds[i].BoxUIDRef);
					if(tList.size() == 0)
						continue;					

					for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
					{	
						if(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[j].Top <0)
						{	
							if((vectorCurBoxBounds[j].Left > vectorCurBoxBounds[i].Right) || ( vectorCurBoxBounds[j].Right < vectorCurBoxBounds[i].Left))
							{	//CA("Before Continue");
								continue;
							}
							else
							{	
								if(vectorCurBoxBounds[j].shiftUP == kFalse)
								{
									//CA("vectorCurBoxBounds[j].shiftUP == kFalse");	
									continue;
								}
								if(vectorCurBoxBounds[j].shiftUP == kTrue && vectorCurBoxBounds[j].isUnderImage == kFalse )
								{  // CA("vectorCurBoxBounds[j].shiftUP == kTrue AND isUnderImage == kFalse");
									UIDRef boxUIDRef = vectorCurBoxBounds[j].BoxUIDRef;
									
									PMReal top = (IncreaseByPoints);
									PMReal left = vectorCurBoxBounds[j].Left;

									const PBPMPoint moveByPoints(0, top);

									//**Commented By Sachin Shrama
									/*InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
									MovePageItemRelative(transform, moveByPoints);*/

									UIDList moveUIDList(boxUIDRef);
									Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
									PBPMPoint referencePoint(PMPoint(0,0));
									ErrorCode errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( moveUIDList, coordinateSpace, referencePoint, Transform::TranslateBy(moveByPoints.X(),moveByPoints.Y())/*(0, top)*/);
								}

								if((vectorCurBoxBounds[j].shiftUP == kTrue) && (vectorCurBoxBounds[j].isUnderImage == kTrue) && (tList[0].imgFlag == 1))
								{ // CA("vectorCurBoxBounds[j].shiftUP == kTrue  AND tList[0].imgFlag == 1");
									UIDRef boxUIDRef = vectorCurBoxBounds[j].BoxUIDRef;
									
									PMReal top = vectorCurBoxBounds[j].compressShift;
									PMReal left = vectorCurBoxBounds[j].Left;

									const PBPMPoint moveByPoints(0, top);

									//****Commented By Sachin Sharma
									/*InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
									MovePageItemRelative(transform, moveByPoints);*/
									
									///*Added...
									UIDList moveUIDList(boxUIDRef);
									Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
									PBPMPoint referencePoint(PMPoint(0,0));
									ErrorCode errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( moveUIDList, coordinateSpace, referencePoint, Transform::TranslateBy(moveByPoints.X(),moveByPoints.Y())/*(0, top)*/);
								}
							}
						}
					}					


					//******Added
					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
					{
						tList[tagIndex].tagPtr->Release();
					}
					//******
				}
				else if( (vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- (vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top) > 0)
				{ 
					// When Sprayed Box Height is Greater Than Original Box Height
					//CA("Greater Than 0");
					PMReal IncreaseByPoints = abs(vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[i].Top)- abs(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[i].Top);
	//Old Logic .....				
					//for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
					//{	//CA("0000");
					//	if(vectorCurBoxBounds[i].Bottom - vectorCurBoxBounds[j].Bottom <0)
					//	{	//CA("0001");
					//		if((vectorCurBoxBounds[j].Left >= vectorCurBoxBounds[i].Left) || ( vectorCurBoxBounds[j].Left < vectorCurBoxBounds[i].Right))
					//		{	//CA("1");
					//			UIDRef boxUIDRef = vectorCurBoxBounds[j].BoxUIDRef;
					//			
					//			/*PMString ASD("Increase By Points : ");
					//			ASD.AppendNumber(IncreaseByPoints);
					//			CA(ASD);*/

					//			/*PMString ASD1("vectorCurBoxBoundsBeforeSpray[j].Top : ");
					//			ASD1.AppendNumber(vectorCurBoxBoundsBeforeSpray[j].Top);
					//			CA(ASD1);*/

					//			PMReal top = /*vectorCurBoxBoundsBeforeSpray[j].Top + */IncreaseByPoints;
					//			PMReal left = vectorCurBoxBounds[j].Left;

					//			const PBPMPoint moveByPoints(0, top);

					//			InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
					//			MovePageItemRelative(transform, moveByPoints);

					//		}
					//	}
					//}
// New Logic.....
					for(int32 j=0; j<vectorCurBoxBounds.size(); j++)
					{	
						if(vectorCurBoxBoundsBeforeSpray[i].Bottom - vectorCurBoxBoundsBeforeSpray[j].Top <0)
						{	
							if((vectorCurBoxBounds[j].Left > vectorCurBoxBounds[i].Right) || ( vectorCurBoxBounds[j].Right < vectorCurBoxBounds[i].Left))
							{	//CA("Before Continue");
								continue;
							}
							else
							{	
								UIDRef boxUIDRef = vectorCurBoxBounds[j].BoxUIDRef;
								
								/*PMString ASD("Increase By Points : ");
								ASD.AppendNumber(IncreaseByPoints);
								CA(ASD);*/

								/*PMString ASD1("vectorCurBoxBoundsBeforeSpray[j].Top : ");
								ASD1.AppendNumber(vectorCurBoxBoundsBeforeSpray[j].Top);
								CA(ASD1);*/

								PMReal top = IncreaseByPoints;
								PMReal left = vectorCurBoxBounds[j].Left;

								const PBPMPoint moveByPoints(0, top);


								///***Commented By Sachin Sharma
								/*InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
								MovePageItemRelative(transform, moveByPoints);*/

								//****Addded
								UIDList moveUIDList(boxUIDRef);
								Transform::CoordinateSpace coordinateSpace = Transform::PasteboardCoordinates() ;
								PBPMPoint referencePoint(PMPoint(0,0));
								ErrorCode errorCode =  Utils<Facade::ITransformFacade>()->TransformItems( moveUIDList, coordinateSpace, referencePoint, Transform::TranslateBy(moveByPoints.X(),moveByPoints.Y())/*(0, top)*/);

							}
						}
					}
                	
				}
				
				/*bool8 result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, curMaxBoxBounds, vectorCurBoxBounds);
				if(result == kFalse)
					break;*/

			}
		
		/*PMReal left = moveToPoints.X() - curMaxBoxBounds.Left();
		PMReal top = moveToPoints.Y() - curMaxBoxBounds.Top();

		const PBPMPoint moveByPoints(left, top);

		for(int32 i = copiedBoxUIDList.Length() - 1; i >= 0; --i)
		{
			UIDRef boxUIDRef = copiedBoxUIDList.GetRef(i);+6			InterfacePtr<ITransform> transform(boxUIDRef, IID_ITRANSFORM);
			MovePageItemRelative(transform, moveByPoints);
		}*/

	}
	while(kFalse);
}


void SubSectionSprayer::startSprayingSubSection(bool16 &isCancelHit)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return;
	}
	do
	{
		//CA("x.1");
		bool16 toggleFlag=kTrue;	//Amit	 

		UIDRef originalPageUIDRef, originalSpreadUIDRef;
		bool16 result = kFalse;
		pageUidList.clear();
		allProductSprayed = kFalse;

		CurrentSectionpNodeDataList.clear();
		CurrentSectionpNodeDataList = pNodeDataList;
		//CA_NUM("size : " ,pNodeDataList.size());
		pageNoFromDb = CurrentSectionpNodeDataList[0].pageno;
		//CA("x.3");

		//get current page UIDRef
		result = this->getCurrentPage(originalPageUIDRef, originalSpreadUIDRef);
		if(result == kFalse){ 
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getCurrentPage returns kFlase");
			break;
		}
		//CA("1");
		PageCount=1;
		UID originalPageUID = originalPageUIDRef.GetUID();
		pageUidList.push_back(originalPageUID);
		//get the list of selected box UIDRef's
		UIDList originalSelUIDList;
		//result = ssSp.getAllPageItemsFromPage(originalPageUIDRef, originalSelUIDList);
		//if(result == kFalse)
			//break;
		//CA("in startspraying subsection");
		result = this->getSelectedBoxIds(originalSelUIDList);
		if(result == kFalse){ 
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getSelectedBoxIds return kFalse");
			break;
		}
		//CA("2");
		if(originalSelUIDList.Length()==0){ 
			ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::originalSelUIDList.Length()==0");
			break;
		}

		if(MediatorClass ::pageOrderSelectedIndex == 1)//spray should start from right page.
		{
			//::AddNewPage(); //Cs3
			Utils<ILayoutUIUtils>()->AddNewPage(); //Cs4
			PageCount = PageCount+ 1;
			UIDRef pageUIDRef;
			UIDRef spreadUIDRef;
			bool16 result = this->getCurrentPage(pageUIDRef, spreadUIDRef);
			if(result == kFalse){
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getCurrentPage returns kFalse");			
				break;
			}

			pageUidList.push_back(pageUIDRef.GetUID());			
		}
		//CA("3");		
		//PMString ASD("originalSelUIDList.Length() : ");
		//ASD.AppendNumber(originalSelUIDList.Length());
		//CA(ASD);

		for(int32 j=0; j<originalSelUIDList.Length();j++)
		{
			//TagReader tReader;
			TagStruct tagInfo;

			InterfacePtr<ITagReader> itagReader
				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReader){
			return ;
			}
			TagList tList=itagReader->getTagsFromBox(originalSelUIDList.GetRef(j));
			if(tList.size()==0)
				tList = itagReader->getFrameTags(originalSelUIDList.GetRef(j));
			if(tList.size()==0)
			{
				/*originalSelUIDList.Remove(j);
				j--;*/							// commented for adding Additional line at bottom of Product Block in Lazboy
				continue;
			}
			tagInfo=tList[0];
			if(tagInfo.isProcessed || tagInfo.parentId!=-1 || tagInfo.numValidFields < NUM_TAGS_FIELDS)
			{
				originalSelUIDList.Remove(j);
				j--;
			}
			//******Added
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			//******
		
		}
		if(originalSelUIDList.Length()==0){ 
			ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::originalSelUIDList.Length()==0");
			break;
		}
		//CA("4");
		//box manipulation
		PMRect origMaxBoxBounds;

		result = this->getMaxLimitsOfBoxes(originalSelUIDList, origMaxBoxBounds, OriginalBoxBoundVector);
		if(result == kFalse){
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getMaxLimitsOfBoxes returns kFalse");		
			break;
		}
//CA("5");		
		//page manipulation
		PMRect marginBoxBounds;
		result = this->getMarginBounds(originalPageUIDRef, marginBoxBounds);
		if(result == kFalse)
		{
			result = this->getPageBounds(originalPageUIDRef, marginBoxBounds);
			if(result == kFalse)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getPageBounds returns kFalse");
				break;
			}
		}
		//CA("6");		
		//////////////////////////////////////////////////////////////////////
		//changing the values of marginBoxBounds according to set values.
		//CMM Comment
		/*
		InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
		if(iSSSprayer==nil)
		{
			CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
			return;
		}
		*/

		//CMM related comment
		/*
		bool16 isOpen = iSSSprayer->callDialog();
		if(!isOpen)
			return;
		*/

		marginBoxBounds.Left() += 0.0/*iSSSprayer->getLeftMargin()*/;
		marginBoxBounds.Right() -= 0.0/*iSSSprayer->getRightMargin()*/;
		marginBoxBounds.Top() += 0.0/*iSSSprayer->getTopMargin()*/;
		marginBoxBounds.Bottom() -= 0.0/*iSSSprayer->getBottomMargin()*/;
		//////////////////////////////////////////////////////////////////////
	
		isProdStencil = kFalse;
		isItemStencil = kFalse;
		isHybridTableStencil = kFalse;
		isAllStencil = kFalse;
		isSectionStencil = kFalse;

		if(MediatorClass ::productStencilUIDList.Length() == 0)
			isProdStencil = kFalse;
		else
			isProdStencil = kTrue;

		if(MediatorClass ::itemStencilUIDList.Length() == 0)
			isItemStencil = kFalse;
		else
			isItemStencil = kTrue;

		if(MediatorClass ::hybridTableStencilUIDList.Length() == 0)
			isHybridTableStencil = kFalse;
		else
			isHybridTableStencil = kTrue;

		if(MediatorClass ::allStencilUIDList.Length() == 0)
			isAllStencil = kFalse;
		else
			isAllStencil = kTrue;

		if(MediatorClass ::sectionStencilUIDList.Length() == 0)
			isSectionStencil = kFalse;
		else
			isSectionStencil = kTrue;


		int16 horizSprayCount = -1, vertSprayCount = -1;
		int16 horizSprayCountProd = -1, vertSprayCountProd= -1;
		int16 horizSprayCountItem = -1, vertSprayCountItem = -1;
		int16 horizSprayCountAll = -1, vertSprayCountAll = -1;
		int16 horizSprayCountHybridTable = -1, vertSprayCountHybridTable = -1;

		CSprayStencilInfoVector.clear();
		CSprayStencilInfo objCSprayStencilInfo;

		if(isItemStencil) 
		{
			//CA("isItemStencil");
			ItemStencilBoxBoundVector.clear();
			ItemStencilMaxBounds = kZeroRect;

			UIDList tempItemSelUIDList(MediatorClass ::itemStencilUIDList.GetDataBase());
			tempItemSelUIDList = MediatorClass ::itemStencilUIDList;

			this->getAllBoxIdsForGroupFrames(tempItemSelUIDList);
			
			result = this->getStencilInfo(/*MediatorClass ::itemStencilUIDList*/tempItemSelUIDList,objCSprayStencilInfo);
			if(result == kFalse)
				break;

			result = this->getMaxLimitsOfBoxes(MediatorClass ::itemStencilUIDList, ItemStencilMaxBounds, ItemStencilBoxBoundVector);
			if(result == kFalse){
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getMaxLimitsOfBoxes returns kFalse");			
				break;
			}

			this->getMaxHorizSprayCount(marginBoxBounds, ItemStencilMaxBounds, horizSprayCountItem);
			this->getMaxVertSprayCount(marginBoxBounds, ItemStencilMaxBounds, vertSprayCountItem);
			
			if(horizSprayCountItem<=0 || vertSprayCountItem<=0)    // if(horizSprayCount<=0 || vertSprayCount<=0)
			{
				CA("The total selected box(es) size does not fit within the margins set for Item Template.  Please increase the margins.");
				break;
			}
		}

		if(isProdStencil)
		{
			//CA("isProdStencil");
			ProdStencilBoxBoundVector.clear();
			ProdStencilMaxBounds = kZeroRect;

			UIDList tempproductSelUIDList(MediatorClass ::productStencilUIDList.GetDataBase());
			tempproductSelUIDList = MediatorClass ::productStencilUIDList;

			this->getAllBoxIdsForGroupFrames(tempproductSelUIDList);

			result = this->getStencilInfo(/*MediatorClass ::productStencilUIDList*/tempproductSelUIDList,objCSprayStencilInfo);
			if(result == kFalse)
				break;

			

			result = this->getMaxLimitsOfBoxes(MediatorClass ::productStencilUIDList, ProdStencilMaxBounds, ProdStencilBoxBoundVector);
			if(result == kFalse){
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getMaxLimitsOfBoxes returns kFalse");			
				break;
			}

			this->getMaxHorizSprayCount(marginBoxBounds, ProdStencilMaxBounds, horizSprayCountProd);
			this->getMaxVertSprayCount(marginBoxBounds, ProdStencilMaxBounds, vertSprayCountProd);
			
			if(horizSprayCountProd<=0 || vertSprayCountProd<=0)    // if(horizSprayCount<=0 || vertSprayCount<=0)
			{
				CA("The total selected box(es) size does not fit within the margins set for Product Template.  Please increase the margins.");
				break;
			}
		}
		
		if(isHybridTableStencil) 
		{
			//CA("isHybridTableStencil");
			HybridTableStencilBoxBoundVector.clear();
			HybridTableStencilMaxBounds = kZeroRect;

			UIDList temphybridTableSelUIDList(MediatorClass::hybridTableStencilUIDList.GetDataBase());
			temphybridTableSelUIDList = MediatorClass::hybridTableStencilUIDList;

			this->getAllBoxIdsForGroupFrames(temphybridTableSelUIDList);

			result = this->getStencilInfo(/*MediatorClass::hybridTableStencilUIDList*/temphybridTableSelUIDList,objCSprayStencilInfo);
			if(result == kFalse)
				break;

			

			result = this->getMaxLimitsOfBoxes(MediatorClass ::hybridTableStencilUIDList, HybridTableStencilMaxBounds, HybridTableStencilBoxBoundVector);
			if(result == kFalse){
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getMaxLimitsOfBoxes returns kFalse");			
				break;
			}

			this->getMaxHorizSprayCount(marginBoxBounds, HybridTableStencilMaxBounds, horizSprayCountItem);
			this->getMaxVertSprayCount(marginBoxBounds, HybridTableStencilMaxBounds, vertSprayCountItem);
			
			if(horizSprayCountItem<=0 || vertSprayCountItem<=0)    // if(horizSprayCount<=0 || vertSprayCount<=0)
			{
				CA("The total selected box(es) size does not fit within the margins set for HybridTable Template.  Please increase the margins.");
				break;
			}
		}

		if(isAllStencil)
		{
			//CA("isAllStencil");
			UIDList tempAllSelUIDList(MediatorClass::allStencilUIDList.GetDataBase());
			tempAllSelUIDList = MediatorClass::allStencilUIDList;

			this->getAllBoxIdsForGroupFrames(tempAllSelUIDList);
			
			result = this->getStencilInfo(/*MediatorClass::allStencilUIDList*/tempAllSelUIDList,objCSprayStencilInfo);
			if(result == kFalse)
				break;			
		}

		//PMString vecSize("CSprayStencilInfoVector.Size = ");
		//vecSize.AppendNumber(CSprayStencilInfoVector.size());
		//CA(vecSize);
		
		if(isSectionStencil)
		{
			SectionStencilBoxBoundVector.clear();
			SectionStencilMaxBounds = kZeroRect;

			UIDList tempsectionSelUIDList(MediatorClass::sectionStencilUIDList.GetDataBase());
			tempsectionSelUIDList = MediatorClass::sectionStencilUIDList;

			this->getAllBoxIdsForGroupFrames(tempsectionSelUIDList);

			result = this->getStencilInfo(/*MediatorClass::sectionStencilUIDList*/tempsectionSelUIDList ,objCSprayStencilInfo);
			if(result == kFalse)
				break;

			

			result = this->getMaxLimitsOfBoxes(MediatorClass ::sectionStencilUIDList, SectionStencilMaxBounds, SectionStencilBoxBoundVector);
			if(result == kFalse){
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getMaxLimitsOfBoxes returns kFalse");			
				break;
			}

			this->getMaxHorizSprayCount(marginBoxBounds, SectionStencilMaxBounds, horizSprayCountProd);
			this->getMaxVertSprayCount(marginBoxBounds, SectionStencilMaxBounds, vertSprayCountProd);
			
			if(horizSprayCountProd<=0 || vertSprayCountProd<=0)    // if(horizSprayCount<=0 || vertSprayCount<=0)
			{
				CA("The total selected box(es) size does not fit within the margins set for Product Template.  Please increase the margins.");
				break;
			}
		}

		if(!(isProdStencil && isItemStencil && isHybridTableStencil ))
		{
//CA("7");
			this->getMaxHorizSprayCount(marginBoxBounds, origMaxBoxBounds, horizSprayCount);
			PMString temp;
			temp.AppendNumber(horizSprayCount);
			//CA(temp);
			this->getMaxVertSprayCount(marginBoxBounds, origMaxBoxBounds, vertSprayCount);
			temp = "";
			temp.AppendNumber(vertSprayCount);
			//CA(temp);

			//////////////////////////////////////////////////////////////

			if(horizSprayCount<=0 || vertSprayCount<=0)    // if(horizSprayCount<=0 || vertSprayCount<=0)
			{
				CA("The total selected box(es) size does not fit within the margins set.  Please increase the margins.");
				break;
			}
		}
		
		CSprayStencilInfoVector.push_back(objCSprayStencilInfo);

		PMReal MaxVertSprayCount, MaxHorizSprayCount;

		MaxVertSprayCount = vertSprayCount;
		if(vertSprayCountProd > MaxVertSprayCount)
			MaxVertSprayCount = vertSprayCountProd;
		if(vertSprayCountItem > MaxVertSprayCount)
			MaxVertSprayCount = vertSprayCountItem;
		
		MaxHorizSprayCount = horizSprayCount;
		if(horizSprayCountProd > MaxHorizSprayCount)
			MaxHorizSprayCount = horizSprayCountProd;
		if(horizSprayCountItem > MaxHorizSprayCount)
			MaxHorizSprayCount = horizSprayCountItem;
		
		PBPMPoint maxPageSprayCount(MaxHorizSprayCount, MaxVertSprayCount);

		//CMM Comment bool16 IsSprayWholeSectionFlag = iSSSprayer->getSprayAllSectionsFlag();
		bool16 IsSprayWholeSectionFlag = kFalse;
		vector<PubData> SectionIDList;
		//was prevously wholeSection.
		if(MediatorClass ::createMediaRadioOption == 2) // Spraying all Sections for Level 2 OR all SubSections for Level 3
		{//start if createMediaRadioOption

			
			// This part is not required for CreateMedia			
			SectionIDList.clear();		
			//for(int32 p=0; p< SectionIDList.size(); p++)
			for(int32 secVecIndex=0;secVecIndex<MediatorClass ::vec_SubSecData.size();secVecIndex++)
			{//for sectionVectorIterator
				//CurrentSelectedSection = SectionIDList[p].getPubId();

				subSectionData & ssdt = MediatorClass ::vec_SubSecData[secVecIndex];

				fillpNodeDataList(ssdt.subSectionID,
				ssdt.sectionID,
				MediatorClass :: currentSelectedPublicationID,
				ssdt.subSectionName);


				CurrentSectionpNodeDataList.clear();	
				CurrentSectionpNodeDataList = pNodeDataList;


				int32 numProducts=0;
				vector<double> tempIdList;
				tempIdList.clear();
				sprayedProductIndex=0;
				//PFTreeDataCache treeCache;
				result = this->getAllIdForLevel(numProducts, tempIdList);
				if(result == kFalse)
				{
					ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::No data found in tree cache.");
					break;
				}
		
				if(tempIdList.size()<=0)
				{
					ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::There are no products to spray");
					continue;
				}

				PMString title("");
				title += ssdt.subSectionName;
				RangeProgressBar progressBar(title, 0, numProducts, kTrue);
				progressBar.SetTaskText("Spraying Products");
//CA("before	1 sprayFromSecondProductOfSubSection");
				sprayFromSecondProductOfSubSection(originalSelUIDList, origMaxBoxBounds, maxPageSprayCount, numProducts, progressBar,toggleFlag);
//CA("after 	1 sprayFromSecondProductOfSubSection");				
				progressBar.SetPosition(numProducts);		
				
				if(progressBar.WasCancelled(kFalse))
				{
					isCancelHit = kTrue;
					break;
				}

			}//end for sectionVectorIterator
			//End This part is not required for CreateMedia 
		}//end if createMediaRadioOption
////////////////////////////////////////////////////////////////////////28-april
		else if(MediatorClass ::createMediaRadioOption == 4)
		{
			
			// This part is for CreateMedia	through ONEsource WhiteBoard transaction.		
			SectionIDList.clear();		
			//for(int32 p=0; p< SectionIDList.size(); p++)
			
			/*PMString ASD("MediatorClass::vec_SubSecData.size() : ");
			ASD.AppendNumber(MediatorClass::vec_SubSecData.size());
			CA(ASD);*/

			for(int32 secVecIndex=0; secVecIndex<MediatorClass::vec_SubSecData.size(); secVecIndex++)
			{//for sectionVectorIterator
				//CurrentSelectedSection = SectionIDList[p].getPubId();
				subSectionData & ssdt = MediatorClass::vec_SubSecData[secVecIndex];
	//CA("before fillpnodedatalist in start spraying sub section");
				// Below functions fill all available product/item list for Subsection.
				fillpNodeDataList(ssdt.subSectionID,
				ssdt.sectionID,
				MediatorClass :: currentSelectedPublicationID,
				ssdt.subSectionName);

				CurrentSectionpNodeDataList.clear();	
				CurrentSectionpNodeDataList = pNodeDataList;

				int32 numProducts=0;
				vector<double> tempIdList;
				tempIdList.clear();
				sprayedProductIndex=0;
				//PFTreeDataCache treeCache;
				result = this->getAllIdForLevel(numProducts, tempIdList);
				if(result == kFalse)
				{
					ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::No data found in tree cache.");
					break;
				}
////2-may
				//PMString asd2;
				//asd2.AppendNumber(numProducts);
				//CA("----------- Number of Products "+asd2);
////2-may
				if(tempIdList.size()<=0)
				{
					ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::There are no products to spray");
					continue;
				}

				PMString title("");
				title += ssdt.subSectionName;
				RangeProgressBar progressBar(title, 0, numProducts, kTrue);
				progressBar.SetTaskText("Spraying Products");
		//CA("before1 sprayFromSecondProductOfSubSection");
				sprayFromSecondProductOfSubSection(originalSelUIDList, origMaxBoxBounds, maxPageSprayCount, numProducts, progressBar,toggleFlag);
		//CA("after1 sprayFromSecondProductOfSubSection++++++++++++++++++++++++++++++++++++++++++++");
				
				if(progressBar.WasCancelled(kFalse))
				{
					isCancelHit = kTrue;
					break;
				}
				//progressBar.SetPosition(numProducts);		

			}//end for sectionVectorIterator
			//End This part is not required for CreateMedia 
		}
////////////////////////////////////////////////////////////////////////28-april
		else if(MediatorClass ::createMediaRadioOption == 5) //2 Spraying all Sections for Level 2 OR all SubSections for Level 3
		{//start if createMediaRadioOption

			
			// This part is not required for CreateMedia			
			SectionIDList.clear();		

			for(int32 secVecIndex=0;secVecIndex < MediatorClass ::vec_SubSecData.size();secVecIndex++)
			{
				//for sectionVectorIterator
				//CurrentSelectedSection = SectionIDList[p].getPubId();
	
				subSectionData & ssdt = MediatorClass ::vec_SubSecData[secVecIndex];
			
				CurrentSelectedSection = ssdt.sectionID;

				fillpNodeDataList(ssdt.subSectionID,
				ssdt.sectionID,
				MediatorClass :: currentSelectedPublicationID,
				ssdt.subSectionName);


				CurrentSectionpNodeDataList.clear();	
				CurrentSectionpNodeDataList = pNodeDataList;


				int32 numProducts=0;
				vector<double> tempIdList;
				tempIdList.clear();
				sprayedProductIndex=0;
				//PFTreeDataCache treeCache;
				result = this->getAllIdForLevel(numProducts, tempIdList);
				if(result == kFalse)
				{
					ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::No data found in tree cache.");
					break;
				}
		
				if(tempIdList.size()<=0)
				{
					ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::There are no products to spray");
					continue;
				}

				PMString title("");
				title += ssdt.subSectionName;
				RangeProgressBar progressBar(title, 0, numProducts, kTrue);
				progressBar.SetTaskText("Spraying Products");
//CA("before	1 sprayFromSecondProductOfSubSection");
				sprayFromSecondProductOfSubSection(originalSelUIDList, origMaxBoxBounds, maxPageSprayCount, numProducts, progressBar,toggleFlag);
//CA("after 	1 sprayFromSecondProductOfSubSection");				
				progressBar.SetPosition(numProducts);	

				if(progressBar.WasCancelled(kFalse))
				{
					isCancelHit = kTrue;
					break;
				}

			}//end for sectionVectorIterator
			//End This part is not required for CreateMedia  
		}
		else   // Spraying selected Section for Level 2 or selected Subsection for level 3
		{
//CA("9");
			int32 numProducts=0;
			vector<double> tempIdList;
			tempIdList.clear();
			//PFTreeDataCache treeCache;
			result = this->getAllIdForLevel(numProducts, tempIdList);
			if(result == kFalse)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::No data found in tree cache.");
				break;
			}
//CA("10");			
			if(tempIdList.size()<=0)
			{
				ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::There are no products to spray");
				break;
			}
//CA("11");
			//show progress bar
			PMString title("");
			if(global_project_level == 3)
			{
				title.Append("Spraying SubSection: ");
		
			}
			else if(global_project_level ==2)
			{
				title.Append("Spraying Section: ");
			}

			//PMString title("Spraying Section: ");
			title += this->selectedSubSection;
			RangeProgressBar progressBar(title, 0, numProducts, kTrue);
			progressBar.SetTaskText("Spraying Products");
			
			//special case
			/*if(tempIdList.size()==1)
			{
				PFTreeDataCache treeCache;
				PublicationNode node;
				treeCache.isExist(tempIdList[0], node);
				
				PMString tempString("Spraying ");
				tempString += node.getName();
				tempString += "...";
				progressBar.SetTaskText(tempString);
				
				sprayFirstProductOfSubSection(originalSelUIDList);
				
				progressBar.SetPosition(1);
			}
			else*/
			{
//CA("before	2 sprayFromSecondProductOfSubSection");				
				sprayFromSecondProductOfSubSection(originalSelUIDList, origMaxBoxBounds, maxPageSprayCount, numProducts, progressBar,toggleFlag);
//CA("after	2 sprayFromSecondProductOfSubSection");

				/*ICommand* cmdPtr = Utils<ISelectUtils>()->SelectPageItems(originalSelUIDList);
				InterfacePtr<ICommand> iCmdPtr(cmdPtr, UseDefaultIID());

				int status = CmdUtils::ProcessCommand(iCmdPtr);
				
				PFTreeDataCache treeCache;
				PublicationNode node;
				treeCache.isExist(tempIdList[0], node);
				
				PMString tempString("Spraying ");
				tempString += node.getName();
				tempString += "...";
				progressBar.SetTaskText(tempString);

				sprayFirstProductOfSubSection(originalSelUIDList);*/
				//CA("35");
				progressBar.SetPosition(numProducts);

				if(progressBar.WasCancelled(kFalse))
				{
					isCancelHit = kTrue;
				}
				//CA("36");
			}
		}
		
		deleteStartPageOfTheDocument(originalPageUIDRef);
		
	}while(kFalse);
}

bool16 SubSectionSprayer::getAllBoxIds(const UIDList& copiedList)
{
	//TagReader tReader;	
	selectUIDList = copiedList;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	return kFalse;

	for(int i=0; i<selectUIDList.Length(); i++)
	{
		InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		if(!iHier)
			continue;

		UID kidUID;
		
		InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
		if(!DataSprayerPtr)
		{
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::getAllBoxIds::Pointre to DataSprayerPtr not found");
			return kFalse;
		}
		//int32 numKids=iHier->GetChildCount();
		//for(int j=0;j<numKids;j++)
		//{
		//	IIDXMLElement* ptr;

		//	kidUID=iHier->GetChildUID(j);
		//	UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
		//	itagReader->getTagsFromBox(boxRef, &ptr);
		//	if(!DataSprayerPtr->doesExist(ptr, boxRef))
		//		selectUIDList.Append(kidUID);		
		//}
	}
	
	return kTrue;

}

void SubSectionSprayer::sprayFirstProductOfSubSection(const UIDList& selectedUIDList)
{
	do
	{
		this->getAllBoxIds(selectedUIDList);

		sprayedProductIndex = 0; //for the first product

		this->startSpraying();
		//CA("First Comleted");
	}
	while(kFalse);
}

void SubSectionSprayer::sprayFromSecondProductOfSubSection(const UIDList& selectedUIDList, const PMRect& origMaxBoxBounds, const PBPMPoint& maxPageSprayCount, int32 numProducts, RangeProgressBar& progressBar,bool16 &toggleFlag)
{
	do
	{
		//bool16 toggleFlag=kTrue;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA(" ptrIAppFramework nil ");
			return;
		}
	//	numProducts--; //one less than actual product count as the first one is sprayed in the end.
		int32 tempProdCount = numProducts;
		int32 prodCount = numProducts + 1 ; // case : only one set of template boxes are drawn in one page...that time the original page doesn't get sprayed so we need one extra page.
		
		/*PMString ProdCountStr = "prodCount = ";
		ProdCountStr.AppendNumber(prodCount);
		CA(ProdCountStr);*/
		//CMM Comment
		/*PMString temp ="";
		temp.AppendNumber(tempProdCount);	
		CA("tempProdCount =  " + temp);*/
		bool16 state = MediatorClass ::subSecSpraySttngs.WithoutPageBreakFlag;

		GetSectionData getSectionData;
        PMString itemFieldIds("");
        PMString itemAssetTypeIds("");
        PMString itemGroupFieldIds("");
        PMString itemGroupAssetTypeIds("");
        PMString listTypeIds("");
        PMString listItemFieldIds("");
        bool16 isSprayItemPerFrameFlag = kFalse;
        
        double langId = ptrIAppFramework->getLocaleId();
		
		vectorCSprayStencilInfo::iterator itr;

		for(itr = CSprayStencilInfoVector.begin();itr != CSprayStencilInfoVector.end(); itr++)
		{
			if(itr->isCopy)
				getSectionData.addCopyFlag = kTrue;
			if(itr->isProductCopy)
				getSectionData.addProductCopyFlag = kTrue;
			if(itr->isSectionCopy)
			{
				getSectionData.addSectionLevelCopyAttrFlag = kTrue;
				getSectionData.addPublicationLevelCopyAttrFlag = kTrue;
				getSectionData.addCatagoryLevelCopyAttrFlag = kTrue;
			}

			if(itr->isAsset)
				getSectionData.addImageFlag = kTrue;
			if(itr->isProductAsset)
				getSectionData.addProductImageFlag = kTrue;
			
			if(itr->isAsset && !itr->isCopy)
				getSectionData.addCopyFlag = kTrue;
	
			if(itr->isProductAsset && !itr->isProductCopy)
				getSectionData.addProductCopyFlag = kTrue;

			if(itr->isBMSAssets)
			{
				getSectionData.addImageFlag = kTrue;
				getSectionData.addItemBMSAssetsFlag = kTrue;
			}
			if(itr->isProductBMSAssets)
			{
				getSectionData.addImageFlag = kTrue;
				getSectionData.addProductBMSAssetsFlag = kTrue;
			}
			if(itr->isSectionLevelBMSAssets)
				getSectionData.addPubLogoAssetFlag = kTrue;

		
			if(itr->isDBTable)
			{
				getSectionData.addDBTableFlag = kTrue;
				//getSectionData.addProductDBTableFlag = kTrue; //for Item Group Lists spray

				getSectionData.addChildCopyAndImageFlag = kTrue;		
			}
			if(itr->isProductDBTable)
			{
				getSectionData.addProductDBTableFlag = kTrue;
				getSectionData.addChildCopyAndImageFlag = kTrue;		
			}
			if(itr->isCustomTablePresent)
				getSectionData.addCustomTablePresentFlag = kTrue;
			
			
			if(itr->isItemPVMPVAssets)
			{
				getSectionData.addItemPVMPVAssetFlag = kTrue;
				if(itr->itemPVAssetIdList.size()>0)
					for(int32 i = 0; i < itr->itemPVAssetIdList.size(); i++)
						getSectionData.itemPVAssetIdList.push_back(itr->itemPVAssetIdList[i]);				
			}
			if(itr->isProductPVMPVAssets)
			{
				getSectionData.addProductPVMPVAssetFlag = kTrue;
				if(itr->productPVAssetIdList.size()>0)
					for(int32 i = 0; i < itr->productPVAssetIdList.size(); i++)
						getSectionData.productPVAssetIdList.push_back(itr->productPVAssetIdList[i]);	
			}
			if(itr->isSectionPVMPVAssets)
			{
				getSectionData.addSectionPVMPVAssetFlag = kTrue;
				if(itr->sectionPVAssetIdList.size()>0)
					for(int32 i = 0; i < itr->sectionPVAssetIdList.size(); i++)
						getSectionData.sectionPVAssetIdList.push_back(itr->sectionPVAssetIdList[i]);	
			}
			if(itr->isPublicationPVMPVAssets)
			{
				getSectionData.addPublicationPVMPVAssetFlag = kTrue;
				if(itr->publicationPVAssetIdList.size()>0)
					for(int32 i = 0; i < itr->publicationPVAssetIdList.size(); i++)
						getSectionData.publicationPVAssetIdList.push_back(itr->publicationPVAssetIdList[i]);	
			}
			if(itr->isCatagoryPVMPVAssets)
			{
				getSectionData.addCatagoryPVMPVAssetFlag = kTrue;
				if(itr->catagoryPVAssetIdList.size()>0)
					for(int32 i = 0; i < itr->catagoryPVAssetIdList.size(); i++)
						getSectionData.catagoryPVAssetIdList.push_back(itr->catagoryPVAssetIdList[i]);	
			}

			if(itr->isCategoryImages)
			{
				getSectionData.addCategoryImages = kTrue;
				if(itr->categoryAssetIdList.size()>0)
					for(int32 i = 0; i < itr->categoryAssetIdList.size(); i++)
						getSectionData.categoryAssetIdList.push_back(itr->categoryAssetIdList[i]);	
			}

			if(itr->isEventSectionImages)
			{
				getSectionData.addEventSectionImages = kTrue;
				if(itr->eventSectionAssetIdList.size()>0)
					for(int32 i = 0; i < itr->eventSectionAssetIdList.size(); i++)
						getSectionData.eventSectionAssetIdList.push_back(itr->eventSectionAssetIdList[i]);	
			}

					
			if(itr->isHyTable)
				getSectionData.addHyTableFlag = kTrue;
			if(itr->isProductHyTable)
				getSectionData.addHyTableFlag = kTrue;
			if(itr->isSectionLevelHyTable)
				getSectionData.addHyTableFlag = kTrue;

			if(itr->isChildTag)
				getSectionData.addChildCopyAndImageFlag = kTrue;

			if(itr->isProductChildTag)
				getSectionData.addProductChildCopyAndImageFlag = kTrue;

			if(itr->isEventField)
				getSectionData.isEventField = kTrue;
            
            // Added by Apsiva for New JSON section Call.
            // collecting listTypes for Item Group and iTem
            if(  itr->dBTypeIds.size() > 0)
            {
                for(int ct =0; ct < itr->dBTypeIds.size(); ct++ )
                {
                    listTypeIds.AppendNumber(PMReal(itr->dBTypeIds.at(ct)));
                    if(ct != (itr->dBTypeIds.size() -1) )
                    {
                        listTypeIds.Append(",");
                    }
                }
            }
            // collecting Copy Attributes for Item Group
            if(  itr->ProductAttributeIds.size() > 0)
            {
                for(int ct =0; ct < itr->ProductAttributeIds.size(); ct++ )
                {
                    itemGroupFieldIds.AppendNumber(PMReal(itr->ProductAttributeIds.at(ct)));
                    if(ct != (itr->ProductAttributeIds.size() -1) )
                    {
                        itemGroupFieldIds.Append(",");
                    }
                }
            }
            
            if(itr->productPVAssetIdList.size()>0)
            {
                if(itemGroupFieldIds.NumUTF16TextChars () > 0)
                {
                    itemGroupFieldIds.Append(",");
                }
                
                for(int32 i = 0; i < itr->productPVAssetIdList.size(); i++)
                {
                    itemGroupFieldIds.AppendNumber(PMReal(itr->productPVAssetIdList[i]));
                    if( i < itr->productPVAssetIdList.size() -1 )
                        itemGroupFieldIds.Append(",");
                }
            }
            
            // collecting Copy Attributes for Item
            if(  itr->itemAttributeIds.size() > 0)
            {
                for(int ct =0; ct < itr->itemAttributeIds.size(); ct++ )
                {
                    itemFieldIds.AppendNumber(PMReal(itr->itemAttributeIds.at(ct)));
                    if(ct != (itr->itemAttributeIds.size() -1) )
                    {
                        itemFieldIds.Append(",");
                    }
                }
            }
            
            if(itr->itemPVAssetIdList.size()>0)
            {
                if(itemFieldIds.NumUTF16TextChars () > 0)
                {
                    itemFieldIds.Append(",");
                }
                
                for(int32 i = 0; i < itr->itemPVAssetIdList.size(); i++)
                {
                    itemFieldIds.AppendNumber(PMReal(itr->itemPVAssetIdList[i]));
                    if( i < itr->itemPVAssetIdList.size() -1 )
                        itemFieldIds.Append(",");
                }
            }
            // collecting Asset type Ids for Item Group
            if(  itr->ProductAssetIds.size() > 0)
            {
                for(int ct =0; ct < itr->ProductAssetIds.size(); ct++ )
                {
                    itemGroupAssetTypeIds.AppendNumber(PMReal(itr->ProductAssetIds.at(ct)));
                    if(ct != (itr->ProductAssetIds.size() -1) )
                    {
                        itemGroupAssetTypeIds.Append(",");
                    }
                }
            }
            // collecting Asset type Ids for Item
            if(  itr->itemAssetIds.size() > 0)
            {
                for(int ct =0; ct < itr->itemAssetIds.size(); ct++ )
                {
                    itemAssetTypeIds.AppendNumber(PMReal(itr->itemAssetIds.at(ct)));
                    if(ct != (itr->itemAssetIds.size() -1) )
                    {
                        itemAssetTypeIds.Append(",");
                    }
                }
            }
            
            // collecting listItemFieldIds for Item
            if(  itr->childItemAttributeIds.size() > 0)
            {
                for(int ct =0; ct < itr->childItemAttributeIds.size(); ct++ )
                {
                    listItemFieldIds.AppendNumber(PMReal(itr->childItemAttributeIds.at(ct)));
                    if(ct != (itr->childItemAttributeIds.size() -1) )
                    {
                        listItemFieldIds.Append(",");
                    }
                }
            }
            //langId = itr->langId;
            
            if(isSprayItemPerFrameFlag == kFalse)
            {
                isSprayItemPerFrameFlag = itr->isSprayItemPerFrame;
            }


		}

		if(getSectionData.addChildCopyAndImageFlag)
		{
			getSectionData.addDBTableFlag = kTrue;
			getSectionData.addProductDBTableFlag = kTrue;
		}

		if(getSectionData.addProductChildCopyAndImageFlag)
		{
			getSectionData.addCustomTablePresentFlag = kTrue;
		}
				
		getSectionData.isOneSource = kFalse;
		
		getSectionData.isGetWholePublicationOrCatagoryDataFlag = kTrue;
		
		getSectionData.addComponentTableFlag = kFalse;
		getSectionData.addAccessoryTableFlag = kFalse;
		getSectionData.addXRefTableFlag = kFalse;
		
		/*getSectionData.addSectionLevelCopyAttrFlag = kFalse;
		getSectionData.addPublicationLevelCopyAttrFlag = kFalse;
		getSectionData.addCatagoryLevelCopyAttrFlag = kFalse;*/

		getSectionData.SectionId = CurrentSelectedSubSection;
		getSectionData.PublicationId = CurrentSelectedPublicationID ;
		
		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			//CAlert::ErrorAlert("Interface for IClientOptions not found.");
			break;
		}

		PMString language_name("");
		getSectionData.languageId = ptrIClientOptions->getDefaultLocale(language_name);

		//getSectionData.languageId = 1;
		getSectionData.CatagoryId = -1;


		
		getSectionData.itemIdList.push_back(-1);
		getSectionData.productIdList.push_back(-1);
		getSectionData.hybridIdList.push_back(-1);

		progressBar.SetTaskText("Retriving Data From Server ...");
		
		AcquireWaitCursor awc ;
		awc.Animate(); 

		//ptrIAppFramework->GetSectionData_getDataForPubOrCat(getSectionData);
        
        ptrIAppFramework->clearAllStaticObjects();

        ptrIAppFramework->EventCache_setCurrentSectionData( CurrentSelectedSection, langId , "" /*currentSectionitemGroupIds*/, "" /*currentSectionitemIds*/, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, kFalse, isSprayItemPerFrameFlag);
        
		for(int16 i=0; i<prodCount;i++)
		{
			
			//make a new page
			//ssSp.createNewPage();
			UIDRef pageUIDRef;
			UIDRef spreadUIDRef;
			if(allProductSprayed == kFalse)		
			{	
				horizontalCount = 0;			
				verticalCount = 0;				
				ProdBlockBoundList.clear();		

				//::AddNewPage();//Cs3 Depricated
				Utils<ILayoutUIUtils>()->AddNewPage(); //Cs4
				PageCount= PageCount+1;

				if(MediatorClass ::subSecSpraySttngs.alternateForEachPage == kTrue)
				{				
					toggleFlag = !toggleFlag;
				}
			}							

			//numProducts is a in and out parameter.  It in's number of products to spray and outs the number of products sprayed.

			bool16 result = this->getCurrentPage(pageUIDRef, spreadUIDRef);
			if(result == kFalse){
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayFromSecondProductOfSubSection::getCurrentPage returns kFalse");			
				break;
			}

			if(allProductSprayed == kFalse)
			{
				pageUidList.push_back(pageUIDRef.GetUID());
			}

		
			//CMM Comment if(iSSSprayer->getAlternatingVal())
			

			//this->sprayPage(pageUIDRef, selectedUIDList, origMaxBoxBounds, maxPageSprayCount, numProducts, progressBar, toggleFlag);
	
			//this->sprayPageWithResizableFrame(pageUIDRef, selectedUIDList, origMaxBoxBounds, maxPageSprayCount, numProducts, progressBar, toggleFlag);
			this->sprayPageWithResizableFrameNew(pageUIDRef, selectedUIDList, origMaxBoxBounds, maxPageSprayCount, numProducts, progressBar, toggleFlag , allProductSprayed );
				
			if(progressBar.WasCancelled(kFalse))
				break;

			tempProdCount = tempProdCount - numProducts;
			numProducts = tempProdCount;
			
			/*PMString temp ="tempProdCount = ";
			temp.AppendNumber(tempProdCount);
			temp.Append(" , numProducts");
			temp.AppendNumber(numProducts);
            CA(temp);*/

			if(numProducts<=0)
			{
				if(state)						//	A
					allProductSprayed = kTrue;	//	A

				ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::sprayFromSecondProductOfSubSection::numProducts<==0");
				break;
			}
			else								//	A
				allProductSprayed = kFalse;		//	A

		}
		//ptrIAppFramework->GetSectionData_clearSectionDataCache();
		ptrIAppFramework->clearAllStaticObjects();
	}
	while(kFalse);

}

/*IHierarchy* hp=iHier->QueryParent();	//returns itself
	hp=hp->QueryParent();			//returns the immediate parent
	hp=hp->QueryParent();
	if(hp)
		return kFalse;
	else
		return kTrue;*/

bool8 SubSectionSprayer::isBoxParent(const UIDRef& frame, IDataBase* database)
{
	InterfacePtr<IHierarchy> itemHier(frame, UseDefaultIID());
	if(itemHier == nil)
		return kFalse;
	
	UID parent = itemHier->GetParentUID();
	if (parent != kInvalidUID)	
	{
		InterfacePtr<IHierarchy> parentHier(itemHier->QueryParent());
		if(parentHier == nil)
		{
			return kFalse;
		}
		
		UIDList parentList(database);
			
		parentHier->GetAncestors(&parentList, IID_IHIERARCHY);
		
		if(parentList.Length()<=1)
			return kTrue;
		else if(parentList.Length()>1)
			return kFalse;
	}

	return kFalse;
}

UIDList SubSectionSprayer::getParentSelectionsOnly(UIDList& theUIDList, IDataBase* database)
{
	do
	{
		//UIDList resultantList = theUIDList;
		UIDList itemsToDeselect(database);
		int32 i=0;

		/*PMString temp;
		CA("this is theUIDList before start");
		for(i=0; i<theUIDList.Length(); i++)
		{
			temp.Clear();
			temp.AppendNumber(theUIDList.GetRef(i).GetUID().Get());
			CA(temp);
		}
		CA("done");*/

		for(i=0; i<theUIDList.Length(); i++)
		{
			InterfacePtr<IHierarchy> iHier(theUIDList.GetRef(i), UseDefaultIID());
			if(!iHier)
				continue;

			itemsToDeselect.Clear();

			iHier->GetDescendents(&itemsToDeselect, IID_IHIERARCHY);

			/*CA("these are first descendants");
			for(int32 cnt=0; cnt<itemsToDeselect.Length(); cnt++)
			{
				temp.Clear();
				temp.AppendNumber(itemsToDeselect.GetRef(cnt).GetUID().Get());
				CA(temp);
			}
			CA("des over");*/

			for(int32 j=(i+1);j<theUIDList.Length();j++)
			{
				for(int32 k=0;k<itemsToDeselect.Length();k++)
				{
					if(theUIDList.GetRef(j)==itemsToDeselect.GetRef(k))
					{
						theUIDList.Remove(j);
						i--;
					}
				}
			}
		}

		/*CA("this is theUIDList again");
		for(i=0; i<theUIDList.Length(); i++)
		{
			temp.Clear();
			temp.AppendNumber(theUIDList.GetRef(i).GetUID().Get());
			CA(temp);
		}*/
		
	}
	while(kFalse);

	return theUIDList;
}

void SubSectionSprayer::callSprayForThisBox(UIDRef boxUIDRef, TagList& theList)
{
	//CA("aya1");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CA("ptrIAppFramework == NULL");
		return ;
	}
	
	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::callSprayForThisBox::Pointre to DataSprayerPtr not found");
		return;
	}
	
	DataSprayerPtr->sprayForThisBox(boxUIDRef, theList);
}

bool16 SubSectionSprayer::callIsFrameTagged(const UIDRef& frameUIDRef)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CA("ptrIAppFramework == NULL");
		return kFalse;
	}
	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::callIsFrameTagged::Pointre to DataSprayerPtr not found");
		return kFalse;
	}
	//CA("aya2");
	return DataSprayerPtr->isFrameTagged(frameUIDRef);
}

bool16 SubSectionSprayer::callSprayForTaggedBox(const UIDRef& taggedBoxUIDRef)
{
	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::callSprayForTaggedBox::Pointre to DataSprayerPtr not found");
		return kFalse;
	}

	return DataSprayerPtr->sprayForTaggedBox(taggedBoxUIDRef);
}

//void SubSectionSprayer::setDataSprayersUIDList(vector<int32> theList)
//{
//	idList = theList;
//}

/*void SubSectionSprayer::setSSIdInPFTreeModel(int32 subSectionId)
{
	PFTreeModel pModel;
	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions==nil)
	{
		CAlert::ErrorAlert("Interface for IClientOptions not found.");
		return;
	}
	
	PMString pubName;
	int32 pubid = ptrIClientOptions->getDefPublication(pubName);
	
	pModel.setRoot(pubid, pubName, subSectionId);
	pModel.GetRootUID();
}
*/

/*bool16 SubSectionSprayer::getAllIdForLevel(int32 level, int32& numProducts, vector<int32>& prodIdList)
{
	PFTreeDataCache treeCache;

	return treeCache.getAllIdForLevel(level, numProducts, prodIdList);
}
*/
/*void SubSectionSprayer::getAllSetOfIds(int32 selectedID, vector<int32>& pfIDList)
{
	PublicationNode pNode;
	//PFInfoCache.isExist(selectedID, pNode);
	int32 tempId=pNode.getParentId();
	PFInfoCache.isExist(tempId, pNode);

	pfIDList.clear();

	pfIDList.push_back(pNode.getParentId());		//Level 1 PF
	pfIDList.push_back(tempId);					//Level 2 PG
	pfIDList.push_back(selectedID);				//Level 3 PR

	int count=0;
	while(PFInfoCache.isExist(selectedID, count, pNode))//Items
	{
		pfIDList.push_back(pNode.getPubId());
		count++;
	}
}*/

void SubSectionSprayer::setPublicationID(double pubid)
{
	//CAlert::InformationAlert("SubSectionSprayer :: setPublicationID function should not be used.");
	//c Mediator md;
	//c md.setPublicationRoot(pubid);
}

void SubSectionSprayer::setImagePath(PMString path)
{
	imagePath = path;

	if(imagePath!="")
	{
		const char *imageP= (imagePath.GetPlatformString().c_str());
		if(imageP[std::strlen(imageP)-1]!='\\' && imageP[std::strlen(imageP)-1]!=':' && imageP[std::strlen(imageP)-1]!='/')
			#ifdef MACINTOSH
			imagePath+=":";
			#else
				imagePath+="\\";
			#endif
	}
}


bool16 SubSectionSprayer::getAllIdForLevel(int32& numIds, vector<double>& idList)
{
	int32 flag=0;
	//9May PublicationNode pNode;
	CreateMediaPublicationNode pNode;
	int32 Count = static_cast<int32>(CurrentSectionpNodeDataList.size());//pNodeDataList.size();
	for (int32 i=0; i<Count; i++)
	{
		flag++;
		//idList.push_back(pNodeDataList[i].getPubId());
		idList.push_back(CurrentSectionpNodeDataList[i].getPubId());

	}
	
	/*for(mapIterator=dataCache->begin(); mapIterator!=dataCache->end(); mapIterator++)
	{
		pNode=(*mapIterator).second;
		if(pNode.getLevel()==level)
		{
			flag++;
			idList.push_back(pNode.getPubId());
		}
	}*/
	numIds=flag;
	return kTrue;

}

int SubSectionSprayer::deleteThisBoxUIDList(UIDList boxUIDList)
{
	for(int32 i=0; i< boxUIDList.Length(); i++)
	{
		InterfacePtr<IScrapItem> scrap(boxUIDList.GetRef(i), UseDefaultIID());
		if(scrap==nil)
			continue;
		InterfacePtr<ICommand> command (scrap->GetDeleteCmd());
		if(!command)
			continue;
		command->SetItemList(UIDList(boxUIDList.GetRef(i)));
		if(CmdUtils::ProcessCommand(command)!=kSuccess)
			continue;
	}
	return 1;
}

ErrorCode SubSectionSprayer::ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut)
{
	ErrorCode status = kFailure;
	do
	{
		if (commandClass == kInvalidClass)
		{
			ASSERT_FAIL("ProcessSimpleCommand: commandClass is invalid"); break;
		}
		InterfacePtr<ICommand> cmd(CmdUtils::CreateCommand(commandClass));
		if (cmd == nil)
		{
			ASSERT(cmd); break;
		}

		cmd->SetItemList(itemsIn);
		status = CmdUtils::ProcessCommand(cmd);

		if (status == kSuccess)
		{
			const UIDList& local_itemsOut = cmd->GetItemListReference();
			itemsOut = local_itemsOut;
		}
		else
		{
			ASSERT_FAIL("ProcessSimpleCommand: The command failed");
		}
	} while (false);
	return status;
}


bool16 deleteStartPageOfTheDocument(UIDRef & startPageUIDRef)
{
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == NULL)
			{
				//CA("ptrIAppFramework == NULL");
				return kFalse;
			}
			InterfacePtr<IDocument> doc(MediatorClass ::currentProcessingDocUIDRef,UseDefaultIID());
			if(doc == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::deleteStartPageOfTheDocument::doc == nil");
				return kFalse;
			}

			
			InterfacePtr<ISpreadList>spreadList(doc,UseDefaultIID());
			if(spreadList == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::deleteStartPageOfTheDocument::spreadList == nil");
				return kFalse;
			}
			IDataBase * database = MediatorClass ::currentProcessingDocUIDRef.GetDataBase();
			if(database == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::SubSectionSprayer::deleteStartPageOfTheDocument::database == nil");
				return kFalse;
			}
			
			//for CAlert
			//PMString num;
			//num.AppendNumber( spreadList->GetSpreadCount());
			//CA("The spreadCount is : "+num);
			//end for CAlert

			if(spreadList->GetSpreadCount()<=0)
			{
				ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::deleteStartPageOfTheDocument::spreadList->GetSpreadCount()<=0");
				return kFalse;
			}
			
			UID spreadUID = spreadList->GetNthSpreadUID(0);
			UIDRef spreadUIDRef(database,spreadUID);
		

		if(MediatorClass :: IskeepSpreadsTogether)
		{
			//
			//InterfacePtr<IDocument> doc(MediatorClass ::currentProcessingDocUIDRef,UseDefaultIID());
			//if(doc == nil)
			//{
			//	CA("doc == nil");
			//	return kFalse;
			//}
			//InterfacePtr<ISpreadList>spreadList(doc,UseDefaultIID());
			//if(spreadList == nil)
			//{
			//	CA("spreadList == nil");
			//	return kFalse;
			//}
			//IDataBase * database = MediatorClass ::currentProcessingDocUIDRef.GetDataBase();
			//if(database == nil)
			//{
			//	CA("database == nil");
			//	return kFalse;
			//}
			//
			UIDList spreadUIDList(database);
			
			//for CAlert
			//PMString num;
			//num.AppendNumber( spreadList->GetSpreadCount());
			//CA("The spreadCount is : "+num);
			//end for CAlert
			for(int32 spreadIndex = 0;spreadIndex < spreadList->GetSpreadCount();spreadIndex++)
			{
				UID spreadUID = spreadList->GetNthSpreadUID(spreadIndex);
				spreadUIDList.Append(spreadUID);
				
				
			}
			InterfacePtr<ICommand> islandCmd(CmdUtils::CreateCommand(kSetIslandSpreadCmdBoss)); 
			if(islandCmd == nil)
			{
				//CA("islandCmd == nil");
				return kFalse;
			}
			bool16 bAllIslands  = kFalse;//kFalse for normal spread
					
			InterfacePtr<IBoolData> boolData(islandCmd, UseDefaultIID());
			if(boolData == nil)
			{
				//CA("boolData == nil");
				return kFalse;
			}
			boolData->Set(!bAllIslands); 
			islandCmd->SetItemList(spreadUIDList); 
			
			ErrorCode status = CmdUtils::ProcessCommand(islandCmd); 
 		}

//////////////////////////////////////////////////////////////////////////
		//
		//InterfacePtr<ICommand>deletePageCmd(CmdUtils :: CreateCommand(kDeletePageCmdBoss));
		//if(deletePageCmd == nil)
		//{
		//	CA("deletePageCmd == nil");
		//	return kFalse;
		//}
		//UIDList startPageUIDList(startPageUIDRef);
		//deletePageCmd->SetItemList(startPageUIDList);
		//ErrorCode status = CmdUtils :: ProcessCommand(deletePageCmd);
		//if(status == kFailure)
		//	return kFalse;
		//
		//test
		//added on 3Nov..
		InterfacePtr<IPageList> iPageList(doc,UseDefaultIID());
		if(iPageList == nil)
		{
			CA("iPageList == nil");
			return kFalse;
		}
		int32 pageCount = iPageList->GetPageCount();
		int32 indexOfLastPageDeleted = -1;
		if(MediatorClass ::pageOrderSelectedIndex == 1)
		{
			indexOfLastPageDeleted = 1;//delete first two pages

		}
		else
			indexOfLastPageDeleted = 0;//delete only the first page.i.e the stencil page.


		UIDList toBeDeletedPagesUIDList(database);
		for(int32 pageIndex =0;pageIndex <=indexOfLastPageDeleted;pageIndex++)
		{
			toBeDeletedPagesUIDList.Append(iPageList->GetNthPageUID(pageIndex));
		}
		InterfacePtr<ICommand> iDeletePageCmd(CmdUtils::CreateCommand(kDeletePageCmdBoss));
        if (iDeletePageCmd == nil)
		{
            return kFalse;
        }
		
        InterfacePtr<IBoolData> iBoolData(iDeletePageCmd,UseDefaultIID());
        if (iBoolData == nil){
            
            return kFalse;
        }
        iBoolData->Set(kFalse);
		
        iDeletePageCmd->SetItemList(toBeDeletedPagesUIDList);
        // process the command
        ErrorCode status = CmdUtils::ProcessCommand(iDeletePageCmd);


		//ended on 2Nov..

		/*
		InterfacePtr<ICommand>deleteSpreadCmd(CmdUtils :: CreateCommand(kDeleteSpreadCmdBoss));
		if(deleteSpreadCmd == nil)
		{
			CA("deleteSpreadCmd == nil");
			return kFalse;
		}
		UIDList startSpreadUIDList(spreadUIDRef);
		deleteSpreadCmd->SetItemList(startSpreadUIDList);
		ErrorCode status = CmdUtils :: ProcessCommand(deleteSpreadCmd);
		if(status == kFailure)
		{
			CA("status == kFailure");
			return kFalse;
		}
		*/
		//end test
		if(status == kFailure)
		{
			return kFalse;
		}

		return kTrue;


}

//8th May
void SubSectionSprayer::startSprayingSubSectionForSpread(void)
{
	do
	{
		UIDRef originalPageUIDRef, originalSpreadUIDRef;
		bool16 result = kFalse;
		pageUidList.clear();
		
		CurrentSectionpNodeDataList.clear();
		CurrentSectionpNodeDataList = pNodeDataList;

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			//CA("ptrIAppFramework == NULL");
			return ;
		}
		//get current page UIDRef
		result = this->getCurrentPage(originalPageUIDRef, originalSpreadUIDRef);
		if(result == kFalse){ 
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSectionForSpread::getCurrentPage returns kFalse");
			break;
		}

		PageCount=1;
		UID originalPageUID = originalPageUIDRef.GetUID();
		pageUidList.push_back(originalPageUID);
		//get the list of selected box UIDRef's
		UIDList originalSelUIDList;
		//result = ssSp.getAllPageItemsFromPage(originalPageUIDRef, originalSelUIDList);
		//if(result == kFalse)
			//break;
		result = this->getSelectedBoxIds(originalSelUIDList);
		if(result == kFalse){
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSectionForSpread::getSelectedBoxIds returns kFalse");
			break;
		}

		if(originalSelUIDList.Length()==0){ 
			ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::originalSelUIDList.Length()==0");
			break;
		}
		
	/*	PMString ASD("originalSelUIDList.Length() : ");
		ASD.AppendNumber(originalSelUIDList.Length());
		CA(ASD);*/

		for(int32 j=0; j<originalSelUIDList.Length();j++)
		{
			//TagReader tReader;
			TagStruct tagInfo;

			//InterfacePtr<ITagReader> itagReader
				//((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReader){
			return ;
			}
			TagList tList=itagReader->getTagsFromBox(originalSelUIDList.GetRef(j));
			if(tList.size()==0)
				tList = itagReader->getFrameTags(originalSelUIDList.GetRef(j));
			if(tList.size()==0)
			{
				/*originalSelUIDList.Remove(j);
				j--;*/							// commented for adding Additional line at bottom of Product Block in Lazboy
				continue;
			}
			tagInfo=tList[0];
			if(tagInfo.isProcessed || tagInfo.parentId!=-1 || tagInfo.numValidFields < NUM_TAGS_FIELDS)
			{
				originalSelUIDList.Remove(j);
				j--;
			}
			//******Added
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			//******
		
		}
		if(originalSelUIDList.Length()==0){ 
			break;
		}
		//box manipulation
		PMRect origMaxBoxBounds;

		result = this->getMaxLimitsOfBoxes(originalSelUIDList, origMaxBoxBounds, OriginalBoxBoundVector);
		if(result == kFalse)
			break;
		
		//page manipulation
		PMRect marginBoxBounds;
		result = this->getMarginBounds(originalPageUIDRef, marginBoxBounds);
		if(result == kFalse)
		{
			result = this->getPageBounds(originalPageUIDRef, marginBoxBounds);
			if(result == kFalse)
				break;
		}
		
		//////////////////////////////////////////////////////////////////////
		//changing the values of marginBoxBounds according to set values.
		//CMM Comment
		/*
		InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
		if(iSSSprayer==nil)
		{
			CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
			return;
		}
		*/

		//CMM related comment
		/*
		bool16 isOpen = iSSSprayer->callDialog();
		if(!isOpen)
			return;
		*/

		marginBoxBounds.Left() += 0.0/*iSSSprayer->getLeftMargin()*/;
		marginBoxBounds.Right() -= 0.0/*iSSSprayer->getRightMargin()*/;
		marginBoxBounds.Top() += 0.0/*iSSSprayer->getTopMargin()*/;
		marginBoxBounds.Bottom() -= 0.0/*iSSSprayer->getBottomMargin()*/;
		//////////////////////////////////////////////////////////////////////

		int16 horizSprayCount, vertSprayCount;
		this->getMaxHorizSprayCount(marginBoxBounds, origMaxBoxBounds, horizSprayCount);
		PMString temp;
		temp.AppendNumber(horizSprayCount);
		//CA(temp);
		this->getMaxVertSprayCount(marginBoxBounds, origMaxBoxBounds, vertSprayCount);
		temp = "";
		temp.AppendNumber(vertSprayCount);
		//CA(temp);
		//////////////////////////////////////////////////////////////

		if(horizSprayCount<=0 || vertSprayCount<=0)    // if(horizSprayCount<=0 || vertSprayCount<=0)
		{
			CA("The total selected box(es) size does not fit within the margins set.  Please increase the margins.");
			break;
		}

		PBPMPoint maxPageSprayCount(horizSprayCount, vertSprayCount);

		//CMM Comment bool16 IsSprayWholeSectionFlag = iSSSprayer->getSprayAllSectionsFlag();
		bool16 IsSprayWholeSectionFlag = kFalse;
		vector<PubData> SectionIDList;
		if(IsSprayWholeSectionFlag) // Spraying all Sections for Level 2 OR all SubSections for Level 3
		{
			/* This part is not required for CreateMedia
			InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
			if(ptrIAppFramework == nil)
				break;

			SectionIDList.clear();
			VectorPubModelPtr vec_pubmodel = NULL;
			if(global_project_level == 3)
			{
				vec_pubmodel = ptrIAppFramework->getAllSubsectionsBySectionIdAndLanguageId(CurrentSelectedSection , global_lang_id);

			}
			else if(global_project_level ==2)
			{
				vec_pubmodel = ptrIAppFramework->getAllSubsectionsByPubIdAndLanguageId(CurrentSelectedPublicationID , global_lang_id);
			}
			//VectorPubInfoPtr pubSubSecInfoVectPtr = ptrIAppFramework->PBMngr_findSectionListByPublicationID(CurrentSelectedPublicationID);
			if(vec_pubmodel)
			{
				
				VectorPubModel::iterator it;
				for(it=vec_pubmodel->begin(); it!=vec_pubmodel->end(); it++)
					{
						PubData pubDataObj;
						int32 sectid=it->getPublicationID();
						pubDataObj.setPubId(sectid);
						PMString pubname=it->getName();
						pubDataObj.setPubName(pubname);
						int32 lvl=0;
						pubDataObj.setPubLvlNo(lvl);
						int32 rootid=it->getRootID();
						pubDataObj.setroot_ID(rootid);
						int32 type_id=it->getTypeID();
						pubDataObj.settype_ID(type_id);
						SectionIDList.push_back(pubDataObj);
					}
			}

			int32 SectionListSize= SectionIDList.size();
			if(SectionListSize == 0)
			{	
				//CA("SectionListSize 0");
				break;
			}
			for(int32 p=0; p< SectionIDList.size(); p++)
			{
				CurrentSelectedSection = SectionIDList[p].getPubId();			
				int32 ParentTypeID = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");
		
				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));

				VectorPubObjectValuePPtr VectorFamilyInfoValuePtr = nil;
				VectorFamilyInfoValuePtr = ptrIAppFramework->PBObjMngr_getProductsForSubSection(SectionIDList[p].getPubId(), ParentTypeID);
				if(VectorFamilyInfoValuePtr == nil)
					break;

				//CA("VectorFamilyInfoValuePtr ssss");

				if(VectorFamilyInfoValuePtr->size()==0)
				{
					//removed comment
					//CA("VectorFamilyInfoValuePtr->size()==0");
					delete VectorFamilyInfoValuePtr;
					continue;
				}
			
				VectorPubObjectValuePointer::iterator it1;
				VectorPubObjectValue::iterator it2;

				//CA("2");
				int count=0;
				CurrentSectionpNodeDataList.clear();
				for(it1 = VectorFamilyInfoValuePtr->begin(); it1 != VectorFamilyInfoValuePtr->end(); it1++)
				{	
					for(it2=(*it1)->begin(); it2 !=(*it1)->end(); it2++)
					{	
						PublicationNode pNodeNew;
						//int32 Sec; 
						//PMString ASD("getLevel_no : ");
						//it2->getObjectValue().getName();
						//CA(it2->getObjectValue().getName());
						//ASD.AppendNumber(it2->getObjectValue().getLevel_no());
						//CA(ASD);
						pNodeNew.setLevel(it2->getObjectValue().getLevel_no());
						pNodeNew.setParentId(it2->getObjectValue().getParent_id());
						pNodeNew.setSequence(it2->getIndex());			
					
						pNodeNew.setPubId(it2->getObjectValue().getObject_id());
						if(!iConverter)
							pNodeNew.setPublicationName(it2->getObjectValue().getName());
						else
							pNodeNew.setPublicationName(iConverter->translateString(it2->getObjectValue().getName()));
				
						pNodeNew.setChildCount(it2->getObjectValue().getChildCount());
						pNodeNew.setReferenceId(it2->getObjectValue().getRef_id());
						pNodeNew.setTypeId(it2->getObjectValue().getObject_type_id());
					//	PMString ASD("getObject_type_id : ");
					//	ASD.AppendNumber(pNode.getTypeId());
					//	CA(ASD);
						pNodeNew.setPBObjectID(it2->getPub_object_id());
						CurrentSectionpNodeDataList.push_back(pNodeNew);
						count++;
					}
				}

///		
				//CA("3");

				int32 numProducts=0;
				vector<int32> tempIdList;
				tempIdList.clear();
				sprayedProductIndex=0;
				//PFTreeDataCache treeCache;
				result = this->getAllIdForLevel(numProducts, tempIdList);
				if(result == kFalse)
				{
					//CA("No data found in tree cache.");
					break;
				}
		
				if(tempIdList.size()<=0)
				{
					//CA("There are no products to spray");
					continue;
				}

				//show progress bar
				//CA("5");
				//PMString title("Spraying Section: ");
				PMString title("");
				if(global_project_level == 3)
				{
					title.Append("Spraying SubSection: ");
			
				}
				else if(global_project_level ==2)
				{
					title.Append("Spraying Section: ");
				}
				title += SectionIDList[p].getPubName();
				RangeProgressBar progressBar(title, 0, numProducts, kTrue);
				progressBar.SetTaskText("Spraying Products");
//CA("before	1 sprayFromSecondProductOfSubSection");
				sprayFromSecondProductOfSubSection(originalSelUIDList, origMaxBoxBounds, maxPageSprayCount, numProducts, progressBar);
//CA("after 	1 sprayFromSecondProductOfSubSection");				
				progressBar.SetPosition(numProducts);		

			}
			//End this part is not required for CreateMedia*/
		}
		else   // Spraying selected Section for Level 2 or selected Subsection for level 3
		{
			int32 numProducts=0;
			vector<double> tempIdList;
			tempIdList.clear();
			//PFTreeDataCache treeCache;
			result = this->getAllIdForLevel(numProducts, tempIdList);
			if(result == kFalse)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::No data found in tree cache.");
				break;
			}
			
			if(tempIdList.size()<=0)
			{
				ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSectionForSpread::There are no products to spray");
				break;
			}

			//show progress bar
			PMString title("");
			if(global_project_level == 3)
			{
				title.Append("Spraying SubSection: ");
		
			}
			else if(global_project_level ==2)
			{
				title.Append("Spraying Section: ");
			}

			//PMString title("Spraying Section: ");
			title += this->selectedSubSection;
			RangeProgressBar progressBar(title, 0, numProducts, kTrue);
			progressBar.SetTaskText("Spraying Products");
			
			//special case
			/*if(tempIdList.size()==1)
			{
				PFTreeDataCache treeCache;
				PublicationNode node;
				treeCache.isExist(tempIdList[0], node);
				
				PMString tempString("Spraying ");
				tempString += node.getName();
				tempString += "...";
				progressBar.SetTaskText(tempString);
				
				sprayFirstProductOfSubSection(originalSelUIDList);
				
				progressBar.SetPosition(1);
			}
			else*/
			{
//CA("before	2 sprayFromSecondProductOfSubSection");	
				bool16 toggleFlag=kTrue;
				sprayFromSecondProductOfSubSection(originalSelUIDList, origMaxBoxBounds, maxPageSprayCount, numProducts, progressBar,toggleFlag);
//CA("after	2 sprayFromSecondProductOfSubSection");

				/*ICommand* cmdPtr = Utils<ISelectUtils>()->SelectPageItems(originalSelUIDList);
				InterfacePtr<ICommand> iCmdPtr(cmdPtr, UseDefaultIID());

				int status = CmdUtils::ProcessCommand(iCmdPtr);
				
				PFTreeDataCache treeCache;
				PublicationNode node;
				treeCache.isExist(tempIdList[0], node);
				
				PMString tempString("Spraying ");
				tempString += node.getName();
				tempString += "...";
				progressBar.SetTaskText(tempString);

				sprayFirstProductOfSubSection(originalSelUIDList);*/
				//CA("35");
				progressBar.SetPosition(numProducts);
				//CA("36");
			}
		}

		deleteStartPageOfTheDocument(originalPageUIDRef);
	
		}
	while(kFalse);
}
//8the May
//#endif

void SubSectionSprayer::sprayPageWithResizableFrameNew(const UIDRef& pageUIDRef, const UIDList& selectedUIDList, const PMRect& origMaxBoxBounds, const PBPMPoint& maxPageSprayCount, int32& numProducts, RangeProgressBar& progressBar, bool16 toggleFlag,bool16 &allProductSprayed)
{
//CA("sprayPageWithResizableFrameNew 1");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CA("ptrIAppFramework == NULL");
		return ;
	}
 do
 {
	PMRect marginBoxBounds;
	bool16 result = kFalse;
	int32 numProductsSprayed=0;
	bool16 sprayingDone = kFalse;

	result = this->getMarginBounds(pageUIDRef, marginBoxBounds);
	if(result == kFalse)
	{
		result = this->getPageBounds(pageUIDRef, marginBoxBounds);
		if(result == kFalse)
			break;
	}

	marginBoxBounds.Left() += 0;//iSSSprayer->getLeftMargin();
	marginBoxBounds.Right() -= 0;//iSSSprayer->getRightMargin();
	marginBoxBounds.Top() += 0;//iSSSprayer->getTopMargin();
	marginBoxBounds.Bottom() -= 0;//iSSSprayer->getBottomMargin();

	int32 tempNumProducts=0;
	vector<double> tempIdList;
	tempIdList.clear();

	PMReal OrgBoxMaxWidth = 0.0, OrgBoxMaxHeight=0.0, maxPageWidth=0.0, maxPageHeight=0.0;
	
	maxPageWidth = abs(marginBoxBounds.Right() - marginBoxBounds.Left());
	maxPageHeight = abs(marginBoxBounds.Bottom() - marginBoxBounds.Top());
	
	/*PMString ZXC2("maxPageHeight : ");
	ZXC2.AppendNumber(maxPageHeight);
	CA(ZXC2);*/
	
	PMReal VerticalBoxSpacing = MediatorClass ::subSecSpraySttngs.verticalBoxSpacing;//iSSSprayer->getVerticalBoxSpacing();	

//	OrgBoxMaxWidth = abs(origMaxBoxBounds.Right() - origMaxBoxBounds.Left());
//	OrgBoxMaxHeight = abs(origMaxBoxBounds.Bottom() - origMaxBoxBounds.Top());

	result = this->getAllIdForLevel(numProducts, tempIdList);

	int16 maxVertCnt=0, maxHorizCnt=0;	
	
	
	int Condition = 1;

//	int16 horizCnt = 0;			//	A
//	int16 vertCnt = 0;			//	A

	int16 horizCnt = horizontalCount;	//	A
	int16 vertCnt = verticalCount;		//	A

	bool16 result11 = MediatorClass ::subSecSpraySttngs.WithoutPageBreakFlag;    
//	FrameBoundsList ProdBlockBoundList;												
	if(result11 == kFalse && allProductSprayed == kFalse)
		ProdBlockBoundList.clear();

	
////////////	Amit 27-12-07	
	bool16 state = kFalse; 
	if(isSectionStencil)
	{
		state = MediatorClass ::subSecSpraySttngs.AddSectionStencilFlag;
	}

	if(state)
	{
		bool16 state1 = MediatorClass ::subSecSpraySttngs.AtTheStartOfSectionFlag;
		bool16 state2 = MediatorClass ::subSecSpraySttngs.AtTheStartOfEachPageFlag;
		bool16 state3 = MediatorClass ::subSecSpraySttngs.AtTheStartOfFirstPageOfSpreadFlag;

		bool16 stat = kTrue;
		if(state1 && sprayedProductIndex == 0 && !(state2 || state3))
		{
			stat = this->sprayPageWithResizableFrameForSection(toggleFlag,marginBoxBounds,ProdBlockBoundList,horizCnt,vertCnt);
		}	
		if((state1 && state2) || state2 )
		{
			//CA("iSSSprayer->getIsAtStartOfEachPageFlag()");
			if(result11 && !state1)
			{
				if(vertCnt == 0 && horizCnt == 0)
				{
					stat = this->sprayPageWithResizableFrameForSection(toggleFlag,marginBoxBounds,ProdBlockBoundList,horizCnt,vertCnt);
				}
			}
			else
			{
				stat = this->sprayPageWithResizableFrameForSection(toggleFlag,marginBoxBounds,ProdBlockBoundList,horizCnt,vertCnt);

			}
		}
		if((state1 && state3) || state3)
		{
			//CA("iSSSprayer->getIsAtStartOfFirstPageFlag()");
			//InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData()); //Cs3
			InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
			if (layoutData == nil)
			{
				ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No layoutData");		
				break;
			}

			IDocument* document = layoutData->GetDocument();
			if (document == nil)
			{
				ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No document");		
				break;
			}

			IDataBase* database = ::GetDataBase(document);
			if(!database)
			{
				ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getCurrentPage::No database");		
				break;
			}
			
	/*IGeometry* spreadItem = layoutData->GetSpread();
	if(spreadItem == nil)
		return 0;*/
			UIDRef spreadUIDref = layoutData->GetSpreadRef();///**ADDed
        
			InterfacePtr<ISpread> iSpread(/*spreadItem*/spreadUIDref, UseDefaultIID());
			if (iSpread == nil)
				break;
			
			UID pageUID = pageUIDRef.GetUID();
			int32  pageIndex = iSpread->GetPageIndex(pageUID);
			if(state1)
			{
				if(sprayedProductIndex == 0 || pageIndex == 0)
					stat = this->sprayPageWithResizableFrameForSection(toggleFlag,marginBoxBounds,ProdBlockBoundList,horizCnt,vertCnt);
			}
			else
			{
				if(pageIndex == 0)
				{
					//CA("pageIndex == 0");
					if(ProdBlockBoundList.size() == 0)
					{
						//CA("ProdBlockBoundList.size() == 0");
						stat = this->sprayPageWithResizableFrameForSection(toggleFlag,marginBoxBounds,ProdBlockBoundList,horizCnt,vertCnt);
					}
				}

			}
		}
		if(stat == kFalse)
		{
			Condition = 0;
		}

	}
///////////		End

	while(Condition)
	{		

		bool16 GoToNewPage = kFalse;
//		int16 tempHorizCnt=0, tempVertCnt=0;	//	A	
		
		
		if(MediatorClass ::subSecSpraySttngs.flowHorizontalFlow)//iSSSprayer->getHorizFlowType())
		{
			//for(int16 horizCnt=0; horizCnt<maxHorizCnt; horizCnt++)
			int condition1 =1;
			bool16 islasthorzFrame = kFalse;
			while(condition1)
			{				
				InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
				InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
				if (!layoutSelectionSuite) {
					break;
				}
				selectionManager->DeselectAll(nil); // deselect every active CSB
				
				bool16 isLeftToRightFlag = kTrue;
				
				if(MediatorClass ::subSecSpraySttngs.alternateForEachPage)//iSSSprayer->getAlternatingVal())
				{			
					if(MediatorClass ::subSecSpraySttngs.flowLeftToRight)//iSSSprayer->getLeftToRightVal()==kTrue)
					{
						if(toggleFlag)
							isLeftToRightFlag = kFalse;
						else
							isLeftToRightFlag = kTrue; 
					}
					else
					{
						if(toggleFlag)
							isLeftToRightFlag = kTrue;
						else
							isLeftToRightFlag = kFalse;
					}			
				}
				else
				{
					if(MediatorClass ::subSecSpraySttngs.flowLeftToRight == kFalse)//iSSSprayer->getLeftToRightVal()==kFalse)
					{
						isLeftToRightFlag = kFalse; 
					}
					else
						isLeftToRightFlag = kTrue;			
				}
								
				int32 isProductFlag = CurrentSectionpNodeDataList[sprayedProductIndex].getIsProduct();
				
				bool16 isSeparateStencil = isProdStencil || isItemStencil || isHybridTableStencil;
				
				int32 StencilNo =0;

				do{
					if(!isSeparateStencil){
						layoutSelectionSuite->/*Select*/SelectPageItems (MediatorClass ::allStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
						StencilNo = 1;
						break;
					}
					else if(isProductFlag == 1 && isProdStencil) 
					{
						layoutSelectionSuite->/*Select*/SelectPageItems(MediatorClass ::productStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
						StencilNo = 2;
						break;
					}
					else if(isProductFlag == 0 && isItemStencil)
					{
						layoutSelectionSuite->/*Select*/SelectPageItems(MediatorClass ::itemStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
						StencilNo = 3;
						break;
					}
					else if(isProductFlag == 2 && isHybridTableStencil)
					{
						
						layoutSelectionSuite->/*Select*/SelectPageItems (MediatorClass ::hybridTableStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
						StencilNo = 4;
						break;
					}
					else{
						layoutSelectionSuite->/*Select*/SelectPageItems (MediatorClass ::allStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
						StencilNo = 1;
						break;
					}		
				}while(0);

				
				PBPMPoint moveToPoints;				

				if(StencilNo == 1)
				{
					OrgBoxMaxHeight = abs(origMaxBoxBounds.Bottom() - origMaxBoxBounds.Top());
				}
				else if(StencilNo == 2)
				{
					OrgBoxMaxHeight = abs(ProdStencilMaxBounds.Bottom() - ProdStencilMaxBounds.Top());	
				}
				else if(StencilNo == 3)
				{
					OrgBoxMaxHeight = abs(ItemStencilMaxBounds.Bottom() - ItemStencilMaxBounds.Top());	
				}

				else if(StencilNo == 4)
				{
					
					OrgBoxMaxHeight = abs(HybridTableStencilMaxBounds.Bottom() - HybridTableStencilMaxBounds.Top());	
				}

				//if(vertCnt > 0 && ProdBlockBoundList.size()!= 0 )
				//{
				//	bool16 result1 = kFalse;
				//	//PMRect ProductBlockMaxBoxBounds;
				//	PMReal TotalHeight=0.0;		

				//	for(int p=0; p<ProdBlockBoundList.size(); p++)
				//	{
				//		if(ProdBlockBoundList[p].HorzCnt ==horizCnt)
				//		{						
				//			TotalHeight += abs(ProdBlockBoundList[p].BoxBounds.Bottom() - ProdBlockBoundList[p].BoxBounds.Top() + VerticalBoxSpacing);					
				//		}
				//	}
				//	/*PMString ZXC1("(maxPageHeight - TotalHeight ) : ");
				//	ZXC1.AppendNumber((maxPageHeight - TotalHeight ));
				//	CA(ZXC1);*/
				//	if((OrgBoxMaxHeight) > (maxPageHeight - TotalHeight ) )
				//	{
				//		//CA("Vertical Area insuffisient please move to next Page");
				//		if(/*horizCnt == maxHorizCnt-1*/islasthorzFrame)
				//		{
				//			//CA("Condition = 0");
				//			condition1 =0;
				//			Condition = 0;
				//		}	
				//		continue;
				//	}
				//}
		

				if(StencilNo ==1)
				{
					result = this->getBoxPositionForResizableFrameNew(marginBoxBounds, origMaxBoxBounds, horizCnt, vertCnt, ProdBlockBoundList, moveToPoints, islasthorzFrame, isLeftToRightFlag);
				}
				else if(StencilNo == 2)
				{
					result = this->getBoxPositionForResizableFrameNew(marginBoxBounds, ProdStencilMaxBounds, horizCnt, vertCnt, ProdBlockBoundList, moveToPoints, islasthorzFrame, isLeftToRightFlag);
				}
				else if(StencilNo == 3)
				{
					result = this->getBoxPositionForResizableFrameNew(marginBoxBounds, ItemStencilMaxBounds, horizCnt, vertCnt, ProdBlockBoundList, moveToPoints, islasthorzFrame, isLeftToRightFlag);
				}
				else if(StencilNo == 4)
				{					
					result = this->getBoxPositionForResizableFrameNew(marginBoxBounds, HybridTableStencilMaxBounds, horizCnt, vertCnt, ProdBlockBoundList, moveToPoints, islasthorzFrame, isLeftToRightFlag);
				}
				
				if(!result)
				{
/////////////////////////////////////////////	This is to delete section stencil of current section when first product/item of current section can't spray 						
						if(sprayedProductIndex == 0 && sectionStencilUIDListToDelete.Length() > 0)
							this->deleteThisBoxUIDList(sectionStencilUIDListToDelete);
///////////////////////////////////////////////////////////////////
					condition1 =0;
					Condition = 0;
					break;
				}				
						
				//copy the selected items
				this->CopySelectedItems();

				//now get the copied item list
				UIDList copiedBoxUIDList;
				result = this->getSelectedBoxIds(copiedBoxUIDList);
				if(result == kFalse)
				{
					condition1=0;
					numProductsSprayed++;
					sprayedProductIndex++;
					//progressBar.SetPosition(sprayedProductIndex);
					if(sprayedProductIndex==numProducts)
					{								
						Condition=0;
						condition1=0;
						sprayingDone = kTrue;							
					}
					
					break;
				}
				//now move boxes to appropriate positions
				this->moveBoxes(copiedBoxUIDList, moveToPoints);

				vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
				PMRect CopiedItemMaxBoxBoundsBforeSpray;
				result = kFalse;
				result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);


				//spray for the selected boxes which were just copied
				//start with the second product
				this->getAllBoxIds(copiedBoxUIDList);
				
				//treeCache.isExist(tempIdList[sprayedProductIndex], node);				
				PMString tempString("Spraying ");
				//tempString +=pNodeDataList[sprayedProductIndex].getName();
				tempString +=CurrentSectionpNodeDataList[sprayedProductIndex].getName();

				tempString += "...";
				progressBar.SetTaskText(tempString);

				InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
				if(!DataSprayerPtr)
				{
					ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayPageWithResizableFrameNew::Pointre to DataSprayerPtr not found");
					return;
				}
				
				PublicationNode pNode =CurrentSectionpNodeDataList[sprayedProductIndex].convertToPublicationNode();
				if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kTrue)
				{
					DataSprayerPtr->FillPnodeStruct
						(pNode,CurrentSelectedSection,CurrentSelectedPublicationID,CurrentSelectedSubSection);	

				}
				else
				{
				DataSprayerPtr->FillPnodeStruct
					(	pNode,
						CurrentSectionpNodeDataList[sprayedProductIndex].sectionID,
						CurrentSectionpNodeDataList[sprayedProductIndex].publicationID,
						CurrentSectionpNodeDataList[sprayedProductIndex].subSectionID						
					);
				}
				DataSprayerPtr->ClearNewImageFrameList();

				if(MediatorClass ::subSecSpraySttngs.HorizontalFlowForAllImageSprayFlag)
					DataSprayerPtr->setFlow(kTrue);
				else
					DataSprayerPtr->setFlow(kFalse);

		
				int32 index = 0;
				if(CurrentSectionpNodeDataList[sprayedProductIndex].getIsProduct() == 0)
				{
					//CA("index = 0");
					//CSprayStencilInfoVector[0];
					index = 0;
				}
				else if(CurrentSectionpNodeDataList[sprayedProductIndex].getIsProduct() == 1)
				{
					//CA("index = 1");
					//CSprayStencilInfoVector[1];
					index = 1;
				}
				else if(CurrentSectionpNodeDataList[sprayedProductIndex].getIsProduct() == 2)
				{
					//CA("index = 2");
					//CSprayStencilInfoVector[2];
					index = 2;
				}
				
				//call new method here for 1 server call per object
				/*if(CSprayStencilInfoVector[index].isCopy)
					CA("isCopy = true");
				else
					CA("isCopy = false");
				
				if(CSprayStencilInfoVector[index].isAsset)
					CA("isAsset = true");
				else
					CA("isAsset = false");
				
				if(CSprayStencilInfoVector[index].isDBTable)
					CA("isDBTable = true");
				else
					CA("isDBTable = false");
				
				if(CSprayStencilInfoVector[index].isHyTable)
					CA("isHyTable = true");
				else
					CA("isHyTable = false");*/

				double objectId = -1;
				//if(CurrentSectionpNodeDataList[sprayedProductIndex].getIsONEsource())
				//	objectId = CurrentSectionpNodeDataList[sprayedProductIndex].getPubId();
				//else
					objectId = CurrentSectionpNodeDataList[sprayedProductIndex].getPBObjectID();
				
				ptrIAppFramework->clearAllStaticObjects();
				//if(CSprayStencilInfoVector.size() >0)
				//{	
				//	//CA("before calling getObjectInfo");
				//	ptrIAppFramework->getObjectInfo(objectId,
				//	CurrentSectionpNodeDataList[sprayedProductIndex].getTypeId(),
				//	CurrentSelectedSection,
				//	kFalse,//CurrentSectionpNodeDataList[sprayedProductIndex].getIsONEsource(),
				//	CSprayStencilInfoVector[index].isCopy,
				//	CSprayStencilInfoVector[index].isAsset,
				//	CSprayStencilInfoVector[index].isDBTable,
				//	CSprayStencilInfoVector[index].isHyTable,
				//	CSprayStencilInfoVector[index].AttributeIds,
				//	CSprayStencilInfoVector[index].AssetIds,
				//	CSprayStencilInfoVector[index].dBTypeIds,
				//	CSprayStencilInfoVector[index].HyTypeIds,
				//	index);
				//}

				this->startSpraying();
			

				//UIDList TempCopiedBoxUIDList(copiedBoxUIDList.GetDataBase());
				InterfacePtr<ITagReader> itagReader
						((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
					if(!itagReader){
					return ;
					}


				if((!MediatorClass::subSecSpraySttngs.SprayItemPerFrameFlag) || (SingleItemSprayReturnFlag == kTrue))	
				{
					UIDList TempCopiedBoxUIDList(copiedBoxUIDList.GetDataBase());
					for(int32 i=0; i<vectorCopiedBoxBoundsBforeSpray.size(); i++)
					{						
						
						for(int32 j=0; j<vectorCopiedBoxBoundsBforeSpray.size(); j++)
						{
							if((vectorCopiedBoxBoundsBforeSpray[j].Left == vectorCopiedBoxBoundsBforeSpray[i].Left) && ( vectorCopiedBoxBoundsBforeSpray[j].Right == vectorCopiedBoxBoundsBforeSpray[i].Right)&& ( vectorCopiedBoxBoundsBforeSpray[j].Top == vectorCopiedBoxBoundsBforeSpray[i].Top) && ( vectorCopiedBoxBoundsBforeSpray[j].Bottom == vectorCopiedBoxBoundsBforeSpray[i].Bottom))
							{
								continue;
							}

							if((vectorCopiedBoxBoundsBforeSpray[j].Left <= vectorCopiedBoxBoundsBforeSpray[i].Left) && ( vectorCopiedBoxBoundsBforeSpray[j].Right >= vectorCopiedBoxBoundsBforeSpray[i].Right)&& ( vectorCopiedBoxBoundsBforeSpray[j].Top <= vectorCopiedBoxBoundsBforeSpray[i].Top) && ( vectorCopiedBoxBoundsBforeSpray[j].Bottom >= vectorCopiedBoxBoundsBforeSpray[i].Bottom))
							{	
								TagList tList = itagReader->getTagsFromBox_ForRefresh(vectorCopiedBoxBoundsBforeSpray[i].BoxUIDRef);
								if(tList.size() == 0)
									continue;
								
								if(tList[0].imgFlag == 1 && tList[0].isAutoResize == 1)
								{	
									TempCopiedBoxUIDList.Append(vectorCopiedBoxBoundsBforeSpray[j].BoxUIDRef.GetUID());
									TempCopiedBoxUIDList.Append(vectorCopiedBoxBoundsBforeSpray[i].BoxUIDRef.GetUID());
								}

								//******Added
								for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
								{
									tList[tagIndex].tagPtr->Release();
								}
								//******
							}
						
						}							
					}
				
					UIDList processedItems;
					if(TempCopiedBoxUIDList.Length()> 0)
					{
						K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(TempCopiedBoxUIDList));
						ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);					
					}

					selectionManager->DeselectAll(nil); // deselect every active CSB
						
					//layoutSelectionSuite->Select(selectedUIDList, Selection::kReplace,  Selection::kDontScrollSelection);
					layoutSelectionSuite->/*Select*/SelectPageItems(copiedBoxUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
					
					
					/*PMString ASD1("sprayedProductIndex : ");
					ASD1.AppendNumber(sprayedProductIndex);
					CA(ASD1);*/
					//progressBar.SetPosition(sprayedProductIndex-1);		
									
					this->moveAutoResizeBoxAfterSpray(copiedBoxUIDList, vectorCopiedBoxBoundsBforeSpray);
					selectUIDList = copiedBoxUIDList;
				}
				else
				{

				}

				PMRect CopiedItemMaxBoxBounds;
				vectorBoxBounds vectorCopiedBoxBounds;
				result = kFalse;

				UIDList newTempUIDList(selectUIDList);
				VectorNewImageFrameUIDList newAddedFrameUIDListAfterSpray = DataSprayerPtr->getNewImageFrameList();

				if(newAddedFrameUIDListAfterSpray.size() > 0)
				{  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
					for(int q=0; q < newAddedFrameUIDListAfterSpray.size(); q++)
					{					
						newTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
					}
				}

				result = this->getMaxLimitsOfBoxes(newTempUIDList, CopiedItemMaxBoxBounds, vectorCopiedBoxBounds);
				
				if(result == kFalse)
				{
					
					sprayedProductIndex++;
					progressBar.SetPosition(sprayedProductIndex);
					numProductsSprayed++;
					if(sprayedProductIndex==numProducts)
					{	
						
						Condition = 0;
						condition1 =0;
						sprayingDone = kTrue;
						break;
					}
					ptrIAppFramework->LogError("AP46_ProductFinder::SubSectionSprayer::sprayPageWithResizableFrameNew::!result");										
					continue;
				}

				if(marginBoxBounds.Bottom() - CopiedItemMaxBoxBounds.Bottom() < 0 )
				{
					if(vertCnt != 0)
					{
						this->deleteThisBoxUIDList(newTempUIDList);
						//CA("After Deleting Boxes");
						StncilOverlengthFlag = kTrue;
						OverlengthHorizCount++;
						OverLengthoffsetWidth = CopiedItemMaxBoxBounds;
						horizCnt++;
						if(/*horizCnt == maxHorizCnt-1*/islasthorzFrame )
						{
							//CA("Condition = 0");
							horizCnt++;
							condition1 =0;
							Condition = 0;
						}				
						continue;
					}
					else
					{
						this->AdjustMaxLimitsOfBoxes(copiedBoxUIDList,marginBoxBounds); 
					}
				}
			
				sprayedProductIndex++;
				progressBar.SetPosition(sprayedProductIndex);
				numProductsSprayed++;
				if(sprayedProductIndex==numProducts)
				{	
					//CA("32");
					if(result11)
					{
						DynFrameStruct CurrentFameStruct;
						CurrentFameStruct.HorzCnt = horizCnt;
						CurrentFameStruct.VertCnt = vertCnt;
						CurrentFameStruct.BoxBounds = CopiedItemMaxBoxBounds;
						CurrentFameStruct.isLastHorzFrame = islasthorzFrame;
						ProdBlockBoundList.push_back(CurrentFameStruct);
					}
					Condition = 0;
					condition1 =0;
					sprayingDone = kTrue;
					break;
				}
			
				

				DynFrameStruct CurrentFameStruct;
				CurrentFameStruct.HorzCnt = horizCnt;
				CurrentFameStruct.VertCnt = vertCnt;
				CurrentFameStruct.BoxBounds = CopiedItemMaxBoxBounds;
				CurrentFameStruct.isLastHorzFrame = islasthorzFrame;
				ProdBlockBoundList.push_back(CurrentFameStruct);
				if(islasthorzFrame)
				{
					//CA("islasthorzFrame kTrue ");					
					vertCnt++;
					horizCnt=0;
				}
				else
				{	//CA("islasthorzFrame kFalse ");
					horizCnt++;
				}
				//CA("33");
				if(progressBar.WasCancelled(kFalse))
					return;
			}			
			vertCnt++;

		}		
		else if(!(MediatorClass::subSecSpraySttngs.flowHorizontalFlow))//iSSSprayer->getHorizFlowType())
		{	
			//CA("For Vertical Flow");
			int RowCount =0;
			bool16 islasthorzFrame = kFalse;
			/*PMString ASD("maxHorizCnt : ");
			ASD.AppendNumber(maxHorizCnt);
			CA(ASD);*/

			//for(int16 horizCnt=0; horizCnt<maxHorizCnt; horizCnt++)
			int condition2 = 1;
			while(condition2)
			{	
				int condition1 = 1;
				//vertCnt = 0;								//commented by Amit
										
				bool16 isLeftToRightFlag = kTrue;

				while(condition1)
				{	
					InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
					InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
					if (!layoutSelectionSuite) {
						break;
					}
					selectionManager->DeselectAll(nil); // deselect every active CSB
										
					int32 isProductFlag = CurrentSectionpNodeDataList[sprayedProductIndex].getIsProduct();
					bool16 isSeparateStencil = isProdStencil || isItemStencil || isHybridTableStencil;					
					int32 StencilNo =0;

					do{
						if(!isSeparateStencil){
							layoutSelectionSuite->/*Select*/SelectPageItems(MediatorClass ::allStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
							StencilNo = 1;
							break;
						}
						else if(isProductFlag ==1 && isProdStencil) 
						{
							layoutSelectionSuite->/*Select*/SelectPageItems(MediatorClass ::productStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
							StencilNo = 2;
							break;
						}
						else if(isProductFlag == 0 && isItemStencil)
						{
							layoutSelectionSuite->/*Select*/SelectPageItems(MediatorClass ::itemStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
							StencilNo = 3;
							break;
						}
						else if(isProductFlag == 2 && isHybridTableStencil)
						{
							/*PMString A("MediatorClass ::hybridTableStencilUIDList::");
							A.AppendNumber(MediatorClass ::hybridTableStencilUIDList.Length());
							CA(A);*/
							layoutSelectionSuite->/*Select*/SelectPageItems(MediatorClass ::hybridTableStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
							StencilNo = 4;
							break;
						}
						else{
							layoutSelectionSuite->/*Select*/SelectPageItems(MediatorClass ::allStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);	
							StencilNo = 1;
							break;
						}		
					}while(0);
				
					PBPMPoint moveToPoints;		
					if(StencilNo == 1)
					{
						OrgBoxMaxHeight = abs(origMaxBoxBounds.Bottom() - origMaxBoxBounds.Top());
					}
					else if(StencilNo == 2)
					{
						OrgBoxMaxHeight = abs(ProdStencilMaxBounds.Bottom() - ProdStencilMaxBounds.Top());	
					}
					else if(StencilNo == 3)
					{
						OrgBoxMaxHeight = abs(ItemStencilMaxBounds.Bottom() - ItemStencilMaxBounds.Top());	
					}
					else if(StencilNo == 4)
					{
						OrgBoxMaxHeight = abs(HybridTableStencilMaxBounds.Bottom() - HybridTableStencilMaxBounds.Top());	
					}


					if(vertCnt > 0 && ProdBlockBoundList.size()!= 0 )
					{
						bool16 result1 = kFalse;						
						PMReal TotalHeight=0.0;		
						for(int p=0; p<ProdBlockBoundList.size(); p++)
						{
							if(ProdBlockBoundList[p].HorzCnt ==horizCnt)
							{						
								if(TotalHeight < abs(ProdBlockBoundList[p].BoxBounds.Bottom()- marginBoxBounds.Top()))
								{	
									TotalHeight = abs(ProdBlockBoundList[p].BoxBounds.Bottom()- marginBoxBounds.Top());
									TotalHeight += VerticalBoxSpacing;

								}
								//TotalHeight += abs(ProdBlockBoundList[p].BoxBounds.Bottom() - ProdBlockBoundList[p].BoxBounds.Top() + VerticalBoxSpacing);					
							}
						}
						/*PMString ZXC1("(maxPageHeight - TotalHeight ) : ");
						ZXC1.AppendNumber((maxPageHeight - TotalHeight ));
						CA(ZXC1);*/

						if((OrgBoxMaxHeight) > (maxPageHeight - TotalHeight ) )
						{
							//CA("Vertical Area insuffisient please move to next Page");
							condition1 =0;
							if(/*RowCount < maxHorizCnt*/!islasthorzFrame)
							{ 
								//CA("Before Continue");
								horizCnt++;
								vertCnt = 0;	///	A
								continue;
							}
							else
							{
								Condition =0;
								condition1=0;
								condition2=0;
								GoToNewPage = kTrue;
								//CA("GoToNewPage = kTrue");
								break;
							}
						}
					}
					if(MediatorClass ::subSecSpraySttngs.alternateForEachPage)//iSSSprayer->getAlternatingVal())
					{			
						if(MediatorClass ::subSecSpraySttngs.flowLeftToRight)//iSSSprayer->getLeftToRightVal()==kTrue)
						{
							if(toggleFlag)
								isLeftToRightFlag = kFalse; /*tempHorizCnt = (maxHorizCnt-1) - horizCnt;*/
							else
								isLeftToRightFlag = kTrue; /*tempHorizCnt = horizCnt;*/
						}
						else
						{
							if(toggleFlag)
								isLeftToRightFlag = kTrue; /*tempHorizCnt = horizCnt;*/
							else
								isLeftToRightFlag = kFalse; /*tempHorizCnt = (maxHorizCnt-1) - horizCnt;*/
						}			
					}
					else
					{
						if(!MediatorClass ::subSecSpraySttngs.flowLeftToRight)//iSSSprayer->getLeftToRightVal()==kFalse)
						{
							isLeftToRightFlag = kFalse;/*tempHorizCnt = (maxHorizCnt-1) - horizCnt;*/
						}
						else
							isLeftToRightFlag = kTrue; /*tempHorizCnt = horizCnt;	*/					
					}
					//tempVertCnt = vertCnt;
					
					/*if(tempVertCnt == 0)
					result = this->getBoxPosition(marginBoxBounds, origMaxBoxBounds, tempHorizCnt, tempVertCnt, moveToPoints);
					else
					result = this->getBoxPositionForResizableFrame(marginBoxBounds, origMaxBoxBounds, tempHorizCnt, tempVertCnt, ProdBlockBoundList, moveToPoints);*/
					
					if(StencilNo ==1)
					{
						result = this->getBoxPositionForResizableFrameNewVerticalFlow(marginBoxBounds, origMaxBoxBounds, horizCnt, vertCnt, ProdBlockBoundList, moveToPoints, islasthorzFrame, isLeftToRightFlag);
					}
					else if(StencilNo == 2)
					{
						result = this->getBoxPositionForResizableFrameNewVerticalFlow(marginBoxBounds, ProdStencilMaxBounds, horizCnt, vertCnt, ProdBlockBoundList, moveToPoints, islasthorzFrame, isLeftToRightFlag);
					}
					else if(StencilNo == 3)
					{
						result = this->getBoxPositionForResizableFrameNewVerticalFlow(marginBoxBounds, ItemStencilMaxBounds, horizCnt, vertCnt, ProdBlockBoundList, moveToPoints, islasthorzFrame, isLeftToRightFlag);
					}
					else if(StencilNo == 4)
					{
						//CA("F");
						result = this->getBoxPositionForResizableFrameNewVerticalFlow(marginBoxBounds, HybridTableStencilMaxBounds, horizCnt, vertCnt, ProdBlockBoundList, moveToPoints, islasthorzFrame, isLeftToRightFlag);
						
						/*PMString temp("Left = ");
						temp.AppendNumber(HybridTableStencilMaxBounds.Left());
						temp.Append(" ,Top  = ");
						temp.AppendNumber(HybridTableStencilMaxBounds.Top());
						temp.Append(" ,Right  = ");
						temp.AppendNumber(HybridTableStencilMaxBounds.Right());
						temp.Append(" ,Bottom  = ");
						temp.AppendNumber(HybridTableStencilMaxBounds.Bottom());
						CA(temp);*/


						
					}
					if(!result)
					{ 
/////////////////////////////////////////////	This is to delete section stencil of current section when first product/item of current section can't spray 						
						if(sprayedProductIndex == 0 && sectionStencilUIDListToDelete.Length() > 0)
							this->deleteThisBoxUIDList(sectionStencilUIDListToDelete);
///////////////////////////////////////////////////////////////////
						Condition =0;
						condition1=0;
						condition2=0;
						GoToNewPage = kTrue;
						break;
					}
			
					/*if(tempHorizCnt>0)
					{
						for(int32 i=0;i<tempHorizCnt; i++)
							moveToPoints.X() += iSSSprayer->getHorizontalBoxSpacing();
					}*/
					
					//copy the selected items
		//CA("G");
			
					this->CopySelectedItems();
		//CA("H");
					//now get the copied item list
					UIDList copiedBoxUIDList;
					result = this->getSelectedBoxIds(copiedBoxUIDList);
		//CA("I");
					if(result == kFalse)
					{
						//CA("result == kFalse  1111111111");					
						condition1=0;
						numProductsSprayed++;
						sprayedProductIndex++;
						//progressBar.SetPosition(sprayedProductIndex);
						if(sprayedProductIndex==numProducts)
						{								
							condition2=0;
							condition1=0;
							sprayingDone = kTrue;							
						}
						break;
					}

					//now move boxes to appropriate positions
					this->moveBoxes(copiedBoxUIDList, moveToPoints);
	//	CA("J");
					vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
					PMRect CopiedItemMaxBoxBoundsBforeSpray;
					result = kFalse;
					result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);
	//	CA("K");			
					//spray for the selected boxes which were just copied
					//start with the second product
					this->getAllBoxIds(copiedBoxUIDList);
	//	CA("L");			
					//treeCache.isExist(tempIdList[sprayedProductIndex], node);				
					PMString tempString("Spraying ");
					//tempString +=pNodeDataList[sprayedProductIndex].getName();
					tempString +=CurrentSectionpNodeDataList[sprayedProductIndex].getName();
					tempString += "...";
					progressBar.SetTaskText(tempString);

					InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
					if(!DataSprayerPtr)
					{
						ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayPageWithResizableFrameNew::Pointre to DataSprayerPtr not found");
						return;
					}					
					
					//DataSprayerPtr->FillPnodeStruct(pNodeDataList[sprayedProductIndex],CurrentSelectedSection,CurrentSelectedPublicationID);				
					//DataSprayerPtr->FillPnodeStruct(CurrentSectionpNodeDataList[sprayedProductIndex],CurrentSelectedSection,CurrentSelectedPublicationID, CurrentSelectedSubSection);				
					PublicationNode pNode =CurrentSectionpNodeDataList[sprayedProductIndex].convertToPublicationNode();
					if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kTrue)
					{
						DataSprayerPtr->FillPnodeStruct
							(pNode,CurrentSelectedSection,CurrentSelectedPublicationID, CurrentSelectedSubSection);	

					}
					else
					{
					DataSprayerPtr->FillPnodeStruct
						(	pNode,
							CurrentSectionpNodeDataList[sprayedProductIndex].sectionID,
							CurrentSectionpNodeDataList[sprayedProductIndex].publicationID, 
							CurrentSectionpNodeDataList[sprayedProductIndex].subSectionID
						);
					}

					if(MediatorClass ::subSecSpraySttngs.HorizontalFlowForAllImageSprayFlag)
						DataSprayerPtr->setFlow(kTrue);
					else
						DataSprayerPtr->setFlow(kFalse);

					DataSprayerPtr->ClearNewImageFrameList();
			//CA("M");

					int32 index = 0;
					if(CurrentSectionpNodeDataList[sprayedProductIndex].getIsProduct() == 0)
					{
						//CA("index = 0");
						//CSprayStencilInfoVector[0];
						index = 0;
					}
					else if(CurrentSectionpNodeDataList[sprayedProductIndex].getIsProduct() == 1)
					{
						//CA("index = 1");
						//CSprayStencilInfoVector[1];
						index = 1;
					}
					else if(CurrentSectionpNodeDataList[sprayedProductIndex].getIsProduct() == 2)
					{
						//CA("index = 2");
						//CSprayStencilInfoVector[2];
						index = 2;
					}
					
					//call new method here for 1 server call per object
					/*if(CSprayStencilInfoVector[index].isCopy)
						CA("isCopy = true");
					else
						CA("isCopy = false");
					
					if(CSprayStencilInfoVector[index].isAsset)
						CA("isAsset = true");
					else
						CA("isAsset = false");
					
					if(CSprayStencilInfoVector[index].isDBTable)
						CA("isDBTable = true");
					else
						CA("isDBTable = false");
					
					if(CSprayStencilInfoVector[index].isHyTable)
						CA("isHyTable = true");
					else
						CA("isHyTable = false");*/

					double objectId = -1;
					//if(CurrentSectionpNodeDataList[sprayedProductIndex].getIsONEsource())
					//	objectId = CurrentSectionpNodeDataList[sprayedProductIndex].getPubId();
					//else
						objectId = CurrentSectionpNodeDataList[sprayedProductIndex].getPBObjectID();
					
					ptrIAppFramework->clearAllStaticObjects();
				
					//if(CSprayStencilInfoVector.size() >0)
					//{	
					//	//CA("before calling getObjectInfo");
					//	ptrIAppFramework->getObjectInfo(objectId,
					//	CurrentSectionpNodeDataList[sprayedProductIndex].getTypeId(),
					//	CurrentSelectedSection,
					//	kFalse,//CurrentSectionpNodeDataList[sprayedProductIndex].getIsONEsource(),
					//	CSprayStencilInfoVector[index].isCopy,
					//	CSprayStencilInfoVector[index].isAsset,
					//	CSprayStencilInfoVector[index].isDBTable,
					//	CSprayStencilInfoVector[index].isHyTable,
					//	CSprayStencilInfoVector[index].AttributeIds,
					//	CSprayStencilInfoVector[index].AssetIds,
					//	CSprayStencilInfoVector[index].dBTypeIds,
					//	CSprayStencilInfoVector[index].HyTypeIds,
					//	index);
					//}

					//CA("before StartSpraying..........");
					this->startSpraying();
			
			//CA("N");
					if((!MediatorClass ::subSecSpraySttngs.SprayItemPerFrameFlag) || (SingleItemSprayReturnFlag == kTrue))  
					{
						UIDList TempCopiedBoxUIDList(copiedBoxUIDList.GetDataBase());

						for(int32 i=0; i<vectorCopiedBoxBoundsBforeSpray.size(); i++)
						{		
							InterfacePtr<ITagReader> itagReader
								((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
							if(!itagReader){
							return ;
							}
							
							for(int32 j=0; j<vectorCopiedBoxBoundsBforeSpray.size(); j++)
							{
								if((vectorCopiedBoxBoundsBforeSpray[j].Left == vectorCopiedBoxBoundsBforeSpray[i].Left) && ( vectorCopiedBoxBoundsBforeSpray[j].Right == vectorCopiedBoxBoundsBforeSpray[i].Right)&& ( vectorCopiedBoxBoundsBforeSpray[j].Top == vectorCopiedBoxBoundsBforeSpray[i].Top) && ( vectorCopiedBoxBoundsBforeSpray[j].Bottom == vectorCopiedBoxBoundsBforeSpray[i].Bottom))
								{
									continue;
								}

								if((vectorCopiedBoxBoundsBforeSpray[j].Left <= vectorCopiedBoxBoundsBforeSpray[i].Left) && ( vectorCopiedBoxBoundsBforeSpray[j].Right >= vectorCopiedBoxBoundsBforeSpray[i].Right)&& ( vectorCopiedBoxBoundsBforeSpray[j].Top <= vectorCopiedBoxBoundsBforeSpray[i].Top) && ( vectorCopiedBoxBoundsBforeSpray[j].Bottom >= vectorCopiedBoxBoundsBforeSpray[i].Bottom))
								{	

									TagList tList = itagReader->getTagsFromBox_ForRefresh(vectorCopiedBoxBoundsBforeSpray[i].BoxUIDRef);
									if(tList.size() == 0)
										continue;
									
									if(tList[0].imgFlag == 1 && tList[0].isAutoResize == 1)
									{	
										TempCopiedBoxUIDList.Append(vectorCopiedBoxBoundsBforeSpray[j].BoxUIDRef.GetUID());
										TempCopiedBoxUIDList.Append(vectorCopiedBoxBoundsBforeSpray[i].BoxUIDRef.GetUID());
									}
									//******Added
									for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
									{
										tList[tagIndex].tagPtr->Release();
									}
									//******
								}
							
							}							
						}
				//	CA("N");	
						UIDList processedItems;
						if(TempCopiedBoxUIDList.Length()> 0)
						{
							K2::scoped_ptr<UIDList> listOfFrames(Utils<IFrameContentUtils>()->CreateListOfFrames(TempCopiedBoxUIDList));
							ErrorCode status =  this->ProcessSimpleCommand(kFitFrameToContentCmdBoss, *listOfFrames, processedItems);
							//CA("3333");
						}

						selectionManager->DeselectAll(nil); // deselect every active CSB
						
						//progressBar.SetPosition(sprayedProductIndex-1);					
						
						
						this->moveAutoResizeBoxAfterSpray(copiedBoxUIDList, vectorCopiedBoxBoundsBforeSpray);
			//	CA("O");
						selectUIDList = copiedBoxUIDList;
					}
					else
					{
							// not do anything its alreday handled in StartSpraying function.
					}
					PMRect CopiedItemMaxBoxBounds;
					vectorBoxBounds vectorCopiedBoxBounds;
					result = kFalse;
					UIDList newTempUIDList(selectUIDList);

					VectorNewImageFrameUIDList newAddedFrameUIDListAfterSpray = DataSprayerPtr->getNewImageFrameList();
					if(newAddedFrameUIDListAfterSpray.size() > 0)
					{  //CA(" newAddedFrameUIDListAfterSpray.Length() > 0 ");
						for(int q=0; q < newAddedFrameUIDListAfterSpray.size(); q++)
						{					
							newTempUIDList.Append(newAddedFrameUIDListAfterSpray[q]);
						}
					}
//CA("P");
					result = this->getMaxLimitsOfBoxes(newTempUIDList, CopiedItemMaxBoxBounds, vectorCopiedBoxBounds);
					if(result == kFalse)
					{
						sprayedProductIndex++;
						numProductsSprayed++;
						progressBar.SetPosition(sprayedProductIndex);
						if(sprayedProductIndex==numProducts)
						{				
							condition2=0;
							condition1=0;
							sprayingDone = kTrue;
							break;
						}
						continue;
					}
					if(marginBoxBounds.Bottom() - CopiedItemMaxBoxBounds.Bottom() < 0 )
					{
						if(vertCnt != 0)
						{
							this->deleteThisBoxUIDList(newTempUIDList);
							StncilOverlengthFlag = kTrue;
							OverlengthHorizCount++;
							OverLengthoffsetWidth = CopiedItemMaxBoxBounds;
							horizCnt++;

							if(/*horizCnt == maxHorizCnt-1*/islasthorzFrame)
							{
								condition2=0;
								Condition = 0;
								condition1=0;
								GoToNewPage = kTrue;
							}
							break;
						}
						else
						{
							this->AdjustMaxLimitsOfBoxes(copiedBoxUIDList,marginBoxBounds); 
						}
					}
//CA("Q");
					numProductsSprayed++;
					sprayedProductIndex++;
					progressBar.SetPosition(sprayedProductIndex);

					if(sprayedProductIndex==numProducts)
					{			
						if(result11)		
						{
							DynFrameStruct CurrentFameStruct;
							CurrentFameStruct.HorzCnt = horizCnt;
							CurrentFameStruct.VertCnt = vertCnt;
							CurrentFameStruct.BoxBounds = CopiedItemMaxBoxBounds;
							CurrentFameStruct.isLastHorzFrame = islasthorzFrame;
							ProdBlockBoundList.push_back(CurrentFameStruct);
							vertCnt++;
						}
						condition2=0;
						condition1=0;
						sprayingDone = kTrue;
						break;
					}
										
					DynFrameStruct CurrentFameStruct;
					CurrentFameStruct.HorzCnt = horizCnt;
					CurrentFameStruct.VertCnt = vertCnt;
					CurrentFameStruct.BoxBounds = CopiedItemMaxBoxBounds;
					CurrentFameStruct.isLastHorzFrame = islasthorzFrame;
					ProdBlockBoundList.push_back(CurrentFameStruct);

					if(islasthorzFrame)
					{
						//CA("islasthorzFrame kTrue ");						
						//horizCnt=0;
						vertCnt++;
					}
					else
					{	//CA("islasthorzFrame kFalse ");
						vertCnt++;
					}	

					if(progressBar.WasCancelled(kFalse))
					{
						//CA("Cancelled");
						return;	
					}
				}
				if(GoToNewPage || sprayingDone)
				{
					Condition=0;
					break;
				}
			}
			Condition=0;
		}
	}
	horizontalCount=horizCnt;
	verticalCount=vertCnt;
	numProducts = numProductsSprayed;
 }while(kFalse);
 //CA("R");
}



bool16 SubSectionSprayer::getBoxPositionForResizableFrameNew(const PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, int16& horizCnt, int16& vertCnt,  FrameBoundsList ProdBlockBoundList, PBPMPoint& moveToPoints, bool16 &isLastHorizFrame, bool16 IsLeftTORightFlag)
{
	//CA("Inside getBoxPositionForResizableFrameNew ");
	
	//PMString ASD(" horizCnt : ");
	//ASD.AppendNumber(horizCnt);
	//ASD.Append("  vertCnt : ");
	//ASD.AppendNumber(vertCnt);
	//CA(ASD);

	bool16 result = kFalse;
	bool16 isLeftToRight = IsLeftTORightFlag;
	do
	{
		//InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
		//if(iSSSprayer==nil)
		//{
		//	CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
		//	return kFalse;
		//}

		PMReal maxWidth = origMaxBoxBounds.Right() - origMaxBoxBounds.Left();
		PMReal boxHeight = abs(origMaxBoxBounds.Bottom() - origMaxBoxBounds.Top());
		PMReal maxHeight = 0.0;

		PMReal maxPageWidth = marginBoxBounds.Right() - marginBoxBounds.Left();
		PMReal maxPageHeight = marginBoxBounds.Bottom() - marginBoxBounds.Top();

		PMReal offsetWidth = 0.0; /* = maxWidth * PMReal(horizCnt) ;*/		
			
		PMReal maxTotalHeight = 0.0;
		PMReal maxTotalWidth = 0.0;

		PMReal left = 0.0;
		PMReal top = 0.0;

		PMReal VerticalBoxSpacing = MediatorClass ::subSecSpraySttngs.verticalBoxSpacing;//iSSSprayer->getVerticalBoxSpacing();
		PMReal HorizBoxSpacing = MediatorClass ::subSecSpraySttngs.horizontalBoxSpacing;//iSSSprayer->getHorizontalBoxSpacing();

		if(isLeftToRight)
		{
			if(ProdBlockBoundList.size() == 0)
			{
				//CA(" ProdBlockBoundList.size() == 0 ");
				OverlengthHorizCount = 0;
				StncilOverlengthFlag = kFalse;
				offsetWidth = 0.0;
				maxHeight = 0.0;
				PMReal remanigWidth = maxPageWidth - (maxWidth + HorizBoxSpacing);
				isLastHorizFrame = kFalse;
				
			}
			else if(ProdBlockBoundList.size() > 0)
			{
				//CA("ProdBlockBoundList.size() > 0 ");
				bool16 islastHorizontalFrame = kFalse;
				int j = static_cast<int>(ProdBlockBoundList.size()-1);
				if(ProdBlockBoundList[j].isLastHorzFrame == kFalse)
				{
					//CA(" ProdBlockBoundList[j].isLastHorzFrame == kFalse ");
					PMReal TempWidth = 0.0;

					TempWidth  = ProdBlockBoundList[j].BoxBounds.Right() + (maxWidth + HorizBoxSpacing) - marginBoxBounds.Left();
					if(StncilOverlengthFlag)
					{ 
						//CA("StncilOverlengthFlag == kTrue");
						TempWidth  = OverLengthoffsetWidth.Right() + (maxWidth + HorizBoxSpacing) - marginBoxBounds.Left();
					}
					if(TempWidth > maxPageWidth)
					{
						//CA("TempWidth > maxPageWidth");
						if(StncilOverlengthFlag)
						{
							//CA("Returning False ");
							OverlengthHorizCount = 0;
							StncilOverlengthFlag = kFalse;
							OverLengthoffsetWidth.Right() = 0.0;
							return kFalse;
						}

						ProdBlockBoundList[j].isLastHorzFrame = kTrue;
						vertCnt++;
						horizCnt =0;
						offsetWidth = 0.0;
						isLastHorizFrame = kFalse;				
					}
					else
					{
						//CA("!!TempWidth > maxPageWidth");
						
						if(StncilOverlengthFlag)
						{
							offsetWidth =  OverLengthoffsetWidth.Right() + HorizBoxSpacing - marginBoxBounds.Left();				
						}
						else
							offsetWidth = ProdBlockBoundList[j].BoxBounds.Right() + HorizBoxSpacing - marginBoxBounds.Left() ;
						isLastHorizFrame = kFalse;					
					}
				}
				else if(ProdBlockBoundList[j].isLastHorzFrame == kTrue)
				{
					//CA("ProdBlockBoundList[j].isLastHorzFrame == kTrue");
					offsetWidth = 0.0;				
					isLastHorizFrame = kFalse;				
				}

				PMReal currentLeft = (marginBoxBounds.Left() + offsetWidth)+ 1;
				PMReal currentRight = (marginBoxBounds.Left() + offsetWidth) + maxWidth -1;
				bool16 cond12 = kFalse;
				do
				{
					if(vertCnt > 0)
					{
						//CA("vertCnt > 0");
						bool16 isFlagFirst = kTrue;
						for(int p=0; p<ProdBlockBoundList.size(); p++)
						{
							//if(ProdBlockBoundList[p].HorzCnt == horizCnt)
							if(((ProdBlockBoundList[p].BoxBounds.Left() < currentLeft) && (currentLeft < ProdBlockBoundList[p].BoxBounds.Right()))
								|| ((ProdBlockBoundList[p].BoxBounds.Left() < currentRight) && (currentRight < ProdBlockBoundList[p].BoxBounds.Right()))
								|| ((ProdBlockBoundList[p].BoxBounds.Left() > currentLeft) && (currentRight > ProdBlockBoundList[p].BoxBounds.Right())))
							{				
								
								//maxHeight += abs(ProdBlockBoundList[p].BoxBounds.Bottom() - ProdBlockBoundList[p].BoxBounds.Top());
								//maxHeight += VerticalBoxSpacing;	
								if(isFlagFirst)
								{	
									maxHeight =  abs(ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top());
									maxHeight += VerticalBoxSpacing;	
									isFlagFirst = kFalse;
								}else
								{								
									if(maxHeight < (abs(ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top())))
									{	
										maxHeight =  abs(ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top());
										maxHeight += VerticalBoxSpacing;
									}
								}
								
								if( (maxPageHeight - maxHeight)  <  boxHeight )
								{									
									if(isLastHorizFrame == kFalse)
									{									
										cond12 = kTrue;
										PMReal TempWidth  = currentRight + (maxWidth + HorizBoxSpacing) - marginBoxBounds.Left();
										if(TempWidth > maxPageWidth)
										{
											//CA("returnning False");
											cond12 = kFalse;
											return kFalse;
										}
										else
										{
											//CA("new one");
											horizCnt++;
											currentLeft += maxWidth + HorizBoxSpacing;
											currentRight += maxWidth + HorizBoxSpacing;
											offsetWidth = currentLeft -1 - marginBoxBounds.Left();
										}
									}									
								}
								else
								{
									cond12 =kFalse;
								}								
							}
						}
					}
				}while(cond12);
			}		
			
			left = (marginBoxBounds.Left() + offsetWidth);
			top = (marginBoxBounds.Top() + maxHeight);
			

		}
		else
		{
			if(ProdBlockBoundList.size() == 0)
			{
				OverlengthHorizCount = 0;
				StncilOverlengthFlag = kFalse;
				offsetWidth = maxWidth;
				maxHeight = 0.0;
				PMReal remanigWidth = maxPageWidth - (maxWidth + HorizBoxSpacing);				
				isLastHorizFrame = kFalse;				
			}
			else if(ProdBlockBoundList.size() > 0)
			{
				int j = static_cast<int> (ProdBlockBoundList.size()-1);
				if(ProdBlockBoundList[j].isLastHorzFrame == kFalse)
				{
					PMReal TempWidth = 0.0;
					TempWidth  =  ProdBlockBoundList[j].BoxBounds.Left() - (maxWidth + HorizBoxSpacing);
					if(StncilOverlengthFlag)
					{ 
						//CA("StncilOverlengthFlag == kTrue");
						TempWidth  = OverLengthoffsetWidth.Left() - (maxWidth + HorizBoxSpacing);
					}
					if(TempWidth < marginBoxBounds.Left())
					{
						if(StncilOverlengthFlag)
						{
							//CA("Returning False ");
							OverlengthHorizCount = 0;
							StncilOverlengthFlag = kFalse;
							OverLengthoffsetWidth.Left() = maxWidth;
							return kFalse;
						}
						ProdBlockBoundList[j].isLastHorzFrame = kTrue;
						vertCnt++;
						offsetWidth = maxWidth;					
							isLastHorizFrame = kFalse;				
					}
					else
					{
						if(StncilOverlengthFlag)
						{					
							offsetWidth =  marginBoxBounds.Right() - (OverLengthoffsetWidth.Left() - HorizBoxSpacing - maxWidth);				
						}
						else
							offsetWidth = marginBoxBounds.Right() - (ProdBlockBoundList[j].BoxBounds.Left() - HorizBoxSpacing - maxWidth);
						isLastHorizFrame = kFalse;				
					}
				}
				else if(ProdBlockBoundList[j].isLastHorzFrame == kTrue)
				{
					offsetWidth = maxWidth;					
					isLastHorizFrame = kFalse;				
				}

				
				PMReal currentLeft = (marginBoxBounds.Right() - offsetWidth)+ 1;
				PMReal currentRight =  (marginBoxBounds.Right() - offsetWidth)+ maxWidth -1;
				bool16 cond13 = kFalse;
				do
				{
					if(vertCnt > 0)
					{
						//CA("vertCnt > 0");
						bool16 isFlagFirst = kTrue;
						for(int p=0; p<ProdBlockBoundList.size(); p++)
						{
							//if(ProdBlockBoundList[p].HorzCnt == horizCnt)
							if(((ProdBlockBoundList[p].BoxBounds.Left() < currentLeft) && (currentLeft < ProdBlockBoundList[p].BoxBounds.Right()))
								|| ((ProdBlockBoundList[p].BoxBounds.Left() < currentRight) && (currentRight < ProdBlockBoundList[p].BoxBounds.Right()))
								|| ((ProdBlockBoundList[p].BoxBounds.Left() > currentLeft) && (currentRight > ProdBlockBoundList[p].BoxBounds.Right())))
							{				
								//CA("1234");
								//maxHeight += abs(ProdBlockBoundList[p].BoxBounds.Bottom() - ProdBlockBoundList[p].BoxBounds.Top());
								//maxHeight += VerticalBoxSpacing;	
								if(isFlagFirst)
								{
									maxHeight =  abs(ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top());
									maxHeight += VerticalBoxSpacing;	
									isFlagFirst = kFalse;
								}else
								{
									if(maxHeight < (abs(ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top())))
									{
										maxHeight =  abs(ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top());
										maxHeight += VerticalBoxSpacing;
									}
								}
								if( (maxPageHeight - maxHeight)  <  boxHeight )
								{									
									if(isLastHorizFrame == kFalse)
									{									
										cond13 = kTrue;
										PMReal TempWidth  = currentLeft - (maxWidth + HorizBoxSpacing)-1;
										if(TempWidth < marginBoxBounds.Left())
										{
											//CA("returnning False");
											cond13 = kFalse;
											return kFalse;
										}
										else
										{
											//CA("new one");
											horizCnt++;
											currentLeft -= (maxWidth + HorizBoxSpacing);
											currentRight -= (maxWidth + HorizBoxSpacing);											
											offsetWidth = marginBoxBounds.Right() - ( currentLeft -1);
										}
									}									
								}
								else
								{
									cond13 =kFalse;
								}	
							}
						}
					}
				}while(cond13);
			}

			left = (marginBoxBounds.Right() - offsetWidth);
			top = (marginBoxBounds.Top() + maxHeight);
	
		}
		
		/*PMReal right = (left + maxWidth);
		PMReal bottom = (top + maxHeight);*/

		const PBPMPoint points(left, top);
		moveToPoints = points;
		result = kTrue;
	}
	while(kFalse);
	return result;
}



bool16 SubSectionSprayer::getBoxPositionForResizableFrameNewVerticalFlow(const PMRect& marginBoxBounds, const PMRect& origMaxBoxBounds, int16& horizCnt, int16& vertCnt,  FrameBoundsList ProdBlockBoundList, PBPMPoint& moveToPoints, bool16 &isLastHorizFrame, bool16 IsLeftTORightFlag)
{
	/*CA("Inside getBoxPositionForResizableFrameNewVerticalFlow ");	
	PMString ASD(" horizCnt : ");
	ASD.AppendNumber(horizCnt);
	ASD.Append("  vertCnt : ");
	ASD.AppendNumber(vertCnt);
	CA(ASD);*/

	bool16 result = kFalse;
	bool16 isLeftToRight = IsLeftTORightFlag;
	do
	{
		/*InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
		if(iSSSprayer==nil)
		{
			CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
			return kFalse;
		}*/
		PMReal maxWidth = origMaxBoxBounds.Right() - origMaxBoxBounds.Left();
		PMReal boxHeight = abs(origMaxBoxBounds.Bottom() - origMaxBoxBounds.Top());
		PMReal maxHeight = 0.0;

		PMReal maxPageWidth = marginBoxBounds.Right() - marginBoxBounds.Left();
		PMReal offsetWidth = 0.0; /* = maxWidth * PMReal(horizCnt) ;*/		
			
		PMReal maxTotalHeight = 0.0;
		PMReal maxTotalWidth = 0.0;

		PMReal left = 0.0;
		PMReal top = 0.0;

		PMReal VerticalBoxSpacing = MediatorClass ::subSecSpraySttngs.verticalBoxSpacing;;
		PMReal HorizBoxSpacing = MediatorClass ::subSecSpraySttngs.horizontalBoxSpacing;

		if(isLeftToRight)
		{
			bool16 overFlag1 = kFalse;
			static int count1 =1;
			if(ProdBlockBoundList.size() == 0)
			{
				//CA(" ProdBlockBoundList.size() == 0 ");
				OverlengthHorizCount = 0;
				StncilOverlengthFlag = kFalse;
				offsetWidth = 0.0;
				maxHeight = 0.0;

				PMReal remaningWidth = maxPageWidth - (maxWidth + HorizBoxSpacing);
				if(remaningWidth >= maxWidth)
					isLastHorizFrame = kFalse;	
				else
					isLastHorizFrame = kTrue;			
			}
			else if(ProdBlockBoundList.size() > 0)
			{
				//CA("ProdBlockBoundList.size() > 0 ");
				if(vertCnt > 0)
				{
					//CA("vertCnt > 0");
					bool16 flagFirst1= kFalse;
					for(int p=0; p<ProdBlockBoundList.size(); p++)
					{
						if(ProdBlockBoundList[p].HorzCnt == horizCnt)
						{						
							//maxHeight += abs(ProdBlockBoundList[p].BoxBounds.Bottom() - ProdBlockBoundList[p].BoxBounds.Top());
							//maxHeight += VerticalBoxSpacing;	
							if(flagFirst1 == kFalse)
							{
								maxHeight = ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top();
								maxHeight += VerticalBoxSpacing;	
								flagFirst1 = kTrue;
							}
							else
							{
								if(maxHeight < (ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top()))
								{
									maxHeight = ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top();
									maxHeight += VerticalBoxSpacing;	
								}
							}
						}
					}
				}
				else if(vertCnt == 0)
					maxHeight =0.0;
				top = (marginBoxBounds.Top() + maxHeight) +1;	
				PMReal newBottom = top + boxHeight ;

				if(horizCnt == 0)
				{
					offsetWidth = 0.0;
					//PMReal remaningWidth = maxPageWidth - (maxWidth + HorizBoxSpacing);
					PMReal remaningWidth = maxPageWidth - (maxWidth + HorizBoxSpacing);
					if(remaningWidth >= maxWidth)
						isLastHorizFrame = kFalse;	
					else
						isLastHorizFrame = kTrue;				
				}
				else
				{
					bool16 condtion123 = kFalse;
					bool16 widthFound = kFalse;
					do
					{
						//CA("12345");
						condtion123 = kFalse;
						widthFound = kFalse;
						bool16 FirstTime = kTrue;
						offsetWidth = 0.0;
						for(int p=0; p<ProdBlockBoundList.size(); p++)
						{
							//if(ProdBlockBoundList[p].HorzCnt == horizCnt-1 && ProdBlockBoundList[p].VertCnt == vertCnt)
							/*if( ((ProdBlockBoundList[p].BoxBounds.Top() <  top) && (ProdBlockBoundList[p].BoxBounds.Bottom() > top)) || (( ProdBlockBoundList[p].BoxBounds.Top() < newBottom ) && (ProdBlockBoundList[p].BoxBounds.Bottom() > newBottom))
								||((ProdBlockBoundList[p].BoxBounds.Top() < top) && (ProdBlockBoundList[p].BoxBounds.Bottom() > newBottom) ))*/
							if(((ProdBlockBoundList[p].BoxBounds.Top() >  top) && (ProdBlockBoundList[p].BoxBounds.Top() < newBottom)) || (( ProdBlockBoundList[p].BoxBounds.Top() < top ) && (ProdBlockBoundList[p].BoxBounds.Bottom() > newBottom))
								||((ProdBlockBoundList[p].BoxBounds.Bottom() > top) && (ProdBlockBoundList[p].BoxBounds.Bottom() < newBottom) ) || ((ProdBlockBoundList[p].BoxBounds.Top() > top) && (ProdBlockBoundList[p].BoxBounds.Bottom() < newBottom) ))
							{				
								//CA("Found Crack Left to Right");							
								PMReal TempWidth  = ProdBlockBoundList[p].BoxBounds.Right() + (maxWidth + HorizBoxSpacing) - marginBoxBounds.Left();
								if(TempWidth > maxPageWidth)
								{
									//CA("TempWidth > maxPageWidth");
									ProdBlockBoundList[p].isLastHorzFrame = kTrue;
									vertCnt++;	
									top = ProdBlockBoundList[p].BoxBounds.Bottom() + VerticalBoxSpacing ;
									maxHeight = ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top() + VerticalBoxSpacing ;
									if( (marginBoxBounds.Bottom()- top)  < boxHeight )
									{
										ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::getBoxPositionForResizableFrameNewVerticalFlow::returnning False1");
										return kFalse;
									}
									isLastHorizFrame = kFalse;	
									condtion123 = kTrue;
									top +=1;
									newBottom = top + boxHeight ;
									//CA("Before Break");
									if(overFlag1 == kTrue)
									{
										//CA(" overFlag == kTrue ");										
										vertCnt++;	
										count1++;										
									//	top += count1 * (boxHeight + VerticalBoxSpacing) ;
									//	maxHeight +=  boxHeight + VerticalBoxSpacing ;
										if( (marginBoxBounds.Bottom()-/* top*/(top + count1 * (boxHeight + VerticalBoxSpacing)))  < boxHeight )
										{
											ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::getBoxPositionForResizableFrameNewVerticalFlow::returnning False2");
											return kFalse;
										}
										isLastHorizFrame = kFalse;	
				
									}
									if(overFlag1 == kFalse)
										overFlag1 = kTrue;
									break;
								}
								else
								{
									//CA("!!TempWidth > maxPageWidth");
									widthFound = kTrue;
									if(FirstTime)
									{	//CA("First Time");
										offsetWidth = ( ProdBlockBoundList[p].BoxBounds.Right() + HorizBoxSpacing) -  marginBoxBounds.Left();
										FirstTime = kFalse;
									}
									else
									{	//CA("Second Time");
										if(offsetWidth < (( ProdBlockBoundList[p].BoxBounds.Right() + HorizBoxSpacing) -  marginBoxBounds.Left()) )
											offsetWidth = ( ProdBlockBoundList[p].BoxBounds.Right() + HorizBoxSpacing) -  marginBoxBounds.Left();
									}
									isLastHorizFrame = kFalse;	
									condtion123 = kFalse;
								}
							}
						}
						if(widthFound == kFalse)
						{	
							//CA("widthFound == kFalse......");
							offsetWidth =( ProdBlockBoundList[ProdBlockBoundList.size()-1].BoxBounds.Left()) - marginBoxBounds.Left();
						}
					}while(condtion123);
				}
			}
			left = (marginBoxBounds.Left() + offsetWidth);
			top = (marginBoxBounds.Top() + maxHeight);

			PMReal NewBottom =  top + boxHeight ;

			Bool16 reDoFlag = kFalse;
			for(int p=0; p<ProdBlockBoundList.size(); p++)
			{	//CA("101");
				/*if(((ProdBlockBoundList[p].BoxBounds.Top()> top) && ( ProdBlockBoundList[p].BoxBounds.Top() < NewBottom )))
					CA("Got it");*/
				if(((ProdBlockBoundList[p].BoxBounds.Top()> top) && ( ProdBlockBoundList[p].BoxBounds.Top() < NewBottom )) && ((ProdBlockBoundList[p].BoxBounds.Left()< left) && (left < ProdBlockBoundList[p].BoxBounds.Right()))  )
				{	//	CA("1234");	
					reDoFlag = kTrue;
					maxHeight = abs(ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top());
					maxHeight += VerticalBoxSpacing;	
				}
			}
			if(reDoFlag)
			{
				//CA("5678");
				top = (marginBoxBounds.Top() + maxHeight) +1;	
				for(int p=0; p<ProdBlockBoundList.size(); p++)
				{
					if( (ProdBlockBoundList[p].BoxBounds.Top() <  top) && (ProdBlockBoundList[p].BoxBounds.Bottom() > top) )
					{				
						//CA("Found Crack");
						offsetWidth = ProdBlockBoundList[p].BoxBounds.Right() + HorizBoxSpacing - marginBoxBounds.Left() ;
						PMReal remaningWidth = maxPageWidth - offsetWidth - maxWidth;
						isLastHorizFrame = kFalse;					
					}
				}
				left = (marginBoxBounds.Left() + offsetWidth);
				top = (marginBoxBounds.Top() + maxHeight);

			}
			
			overFlag1 = kFalse;
			count1 = 1;
		}
		else
		{
			bool16 overFlag = kFalse;
			static int count =1;
			if(ProdBlockBoundList.size() == 0)
			{
				offsetWidth = maxWidth;
				maxHeight = 0.0;
				isLastHorizFrame = kFalse;			
			}
			else if(ProdBlockBoundList.size() > 0)
			{					
				if(vertCnt > 0)
				{
					//CA("vertCnt > 0");
					bool16 flagFirst1= kFalse;
					for(int p=0; p<ProdBlockBoundList.size(); p++)
					{
						if(ProdBlockBoundList[p].HorzCnt == horizCnt)
						{						
							//maxHeight += abs(ProdBlockBoundList[p].BoxBounds.Bottom() - ProdBlockBoundList[p].BoxBounds.Top());
							//maxHeight += VerticalBoxSpacing;	
							if(flagFirst1 == kFalse)
							{
								maxHeight = ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top();
								maxHeight += VerticalBoxSpacing;	
								flagFirst1 = kTrue;
							}
							else
							{
								if(maxHeight < (ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top()))
								{
									maxHeight = ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top();
									maxHeight += VerticalBoxSpacing;	
								}
							}
						}
					}
				}
				else if(vertCnt == 0)
					maxHeight =0.0;
				
				top = (marginBoxBounds.Top() + maxHeight) +1;
				PMReal newBottom = top + boxHeight -2;

				if(horizCnt == 0)
				{
					offsetWidth = maxWidth;						
					isLastHorizFrame = kFalse;				
				}
				else
				{
					bool16 condtion123 = kFalse;	
					bool16 widthFound = kFalse;
					do
					{
						condtion123 = kFalse;
						widthFound = kFalse;
						offsetWidth =0.0;
						bool16 FirstTime = kTrue;
						for(int p=0; p<ProdBlockBoundList.size(); p++)
						{							
							/*if(((ProdBlockBoundList[p].BoxBounds.Top() <  top) && (ProdBlockBoundList[p].BoxBounds.Bottom() > top)) || (( ProdBlockBoundList[p].BoxBounds.Top() < newBottom ) && (ProdBlockBoundList[p].BoxBounds.Bottom() > newBottom))
								||((ProdBlockBoundList[p].BoxBounds.Top() < top) && (ProdBlockBoundList[p].BoxBounds.Bottom() > newBottom) ) || ((ProdBlockBoundList[p].BoxBounds.Top() < top) && (ProdBlockBoundList[p].BoxBounds.Bottom() < newBottom) ))*/
							if(((ProdBlockBoundList[p].BoxBounds.Top() >  top) && (ProdBlockBoundList[p].BoxBounds.Top() < newBottom)) || (( ProdBlockBoundList[p].BoxBounds.Top() < top ) && (ProdBlockBoundList[p].BoxBounds.Bottom() > newBottom))
								||((ProdBlockBoundList[p].BoxBounds.Bottom() > top) && (ProdBlockBoundList[p].BoxBounds.Bottom() < newBottom) ) || ((ProdBlockBoundList[p].BoxBounds.Top() > top) && (ProdBlockBoundList[p].BoxBounds.Bottom() < newBottom) ))

							{				
								//CA("Found Crack");
								PMReal TempWidth  =  ProdBlockBoundList[p].BoxBounds.Left() - (maxWidth + HorizBoxSpacing);
								if(TempWidth < marginBoxBounds.Left())
								{	
									//CA("TempWidth < marginBoxBounds.Left()");
									ProdBlockBoundList[p].isLastHorzFrame = kTrue;
									vertCnt++;	
									top = ProdBlockBoundList[p].BoxBounds.Bottom() + VerticalBoxSpacing ;
									maxHeight = ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top() + VerticalBoxSpacing ;
									if( (marginBoxBounds.Bottom()- top)  < boxHeight )
									{
										ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::getBoxPositionForResizableFrameNewVerticalFlow::returnning False3");
										return kFalse;
									}
									isLastHorizFrame = kFalse;	
									condtion123 = kTrue;
									top +=1;
									//CA("Before Break");
									if(overFlag == kTrue)
									{
										//CA(" overFlag == kTrue ");										
										vertCnt++;	
										count++;										
										top += count * (boxHeight + VerticalBoxSpacing) ;
										maxHeight +=  boxHeight + VerticalBoxSpacing ;
										if( (marginBoxBounds.Bottom()- top)  < boxHeight )
										{
											ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::getBoxPositionForResizableFrameNewVerticalFlow::returnning False4");
											return kFalse;
										}
										isLastHorizFrame = kFalse;	
				
									}
									if(overFlag == kFalse)
										overFlag = kTrue;
									break;
								}
								else
								{
									//CA("else part");
									widthFound = kTrue;
									if(FirstTime)
									{	//CA("First Time");
										offsetWidth = marginBoxBounds.Right() - ( ProdBlockBoundList[p].BoxBounds.Left() - (HorizBoxSpacing + maxWidth));
										FirstTime = kFalse;
									}
									else
									{	//CA("Second Time");
										if(offsetWidth < (marginBoxBounds.Right() - ( ProdBlockBoundList[p].BoxBounds.Left() - (HorizBoxSpacing + maxWidth))) )
											offsetWidth = marginBoxBounds.Right() - ( ProdBlockBoundList[p].BoxBounds.Left() - (HorizBoxSpacing + maxWidth));
									}

									condtion123 = kFalse;
									isLastHorizFrame = kFalse;										
								}
							}
							
						}
						if(widthFound == kFalse)
						{	
							//CA("widthFound == kFalse......");
							offsetWidth = marginBoxBounds.Right() - ( ProdBlockBoundList[ProdBlockBoundList.size()-1].BoxBounds.Left());
						}
					}while(condtion123);
					
				}					
			}			
			left = (marginBoxBounds.Right() - offsetWidth);
			top = (marginBoxBounds.Top() + maxHeight);

			PMReal NewBottom =  top + boxHeight ;
			PMReal NewRight = left + maxWidth;

			Bool16 reDoFlag = kFalse;
			for(int p=0; p<ProdBlockBoundList.size(); p++)
			{	
				if(((ProdBlockBoundList[p].BoxBounds.Top()> top) && ( ProdBlockBoundList[p].BoxBounds.Top() < NewBottom )) && ((ProdBlockBoundList[p].BoxBounds.Left()< left) && (left < ProdBlockBoundList[p].BoxBounds.Right()))  )
				{	//CA("1234");	
					reDoFlag = kTrue;
					maxHeight = abs(ProdBlockBoundList[p].BoxBounds.Bottom() - marginBoxBounds.Top());
					maxHeight += VerticalBoxSpacing;	
				}
			}
			if(reDoFlag)
			{
				//CA("5678");
				top = (marginBoxBounds.Top() + maxHeight) +1;	
				for(int p=0; p<ProdBlockBoundList.size(); p++)
				{
					if( (ProdBlockBoundList[p].BoxBounds.Top() <  top) && (ProdBlockBoundList[p].BoxBounds.Bottom() > top) )
					{				
						//CA("Found Crack");
						offsetWidth = marginBoxBounds.Right() - ( ProdBlockBoundList[p].BoxBounds.Left() - (HorizBoxSpacing + maxWidth));
						isLastHorizFrame = kFalse;					
					}
				}
				left = (marginBoxBounds.Right() - offsetWidth);
				top = (marginBoxBounds.Top() + maxHeight);
			}
			
			overFlag = kFalse;
			count = 1;
		}	
		const PBPMPoint points(left, top);
		moveToPoints = points;
		result = kTrue;
	}
	while(kFalse);

	return result;
}



ErrorCode SubSectionSprayer::AdjustMaxLimitsOfBoxes(const UIDList& boxList, PMRect PagemaxBounds)
{
	ErrorCode result1 = kFailure;	
	PMReal minTop=0.0, minLeft=0.0 , maxBottom=0.0 , maxRight=0.0;	

	minTop = PagemaxBounds.Top();
	minLeft = PagemaxBounds.Left();
	maxBottom = PagemaxBounds.Bottom();
	maxRight = PagemaxBounds.Right();

	/*PMString ASD("PagemaxBounds.Top() : ");
	ASD.AppendNumber((PagemaxBounds.Top()));
	ASD.Append("  PagemaxBounds.Left() : ");
	ASD.AppendNumber((PagemaxBounds.Left()));
	ASD.Append("  PagemaxBounds.Bottom() : ");
	ASD.AppendNumber((PagemaxBounds.Bottom()));
	ASD.Append("  PagemaxBounds.Right() : ");
	ASD.AppendNumber((PagemaxBounds.Right()));
	CA(ASD);*/

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return result1;
	}

	for(int i=0;i<boxList.Length();i++)
	{
		UIDRef boxUIDRef = boxList.GetRef(i);

		InterfacePtr<IGeometry> geometryPtr(boxUIDRef, UseDefaultIID());
		if(!geometryPtr)
		{
			continue;
		}

		PMRect boxBounds=geometryPtr->GetStrokeBoundingBox(InnerToPasteboardMatrix(geometryPtr));
				
		PMReal top = boxBounds.Top();
		PMReal left = boxBounds.Left();
		PMReal bottom = boxBounds.Bottom();
		PMReal right = boxBounds.Right();
		
		/*PMString ASD("boxBounds.Top() : ");
		ASD.AppendNumber((boxBounds.Top()));
		ASD.Append("  boxBounds.Left() : ");
		ASD.AppendNumber((boxBounds.Left()));
		ASD.Append("  boxBounds.Bottom() : ");
		ASD.AppendNumber((boxBounds.Bottom()));
		ASD.Append("  boxBounds.Right() : ");
		ASD.AppendNumber((boxBounds.Right()));
		CA(ASD);*/

		if(bottom > maxBottom){ 
			//CA("Box Bounds are greater Than Page Bottom Bound");

			PMReal newBottom  = bottom - maxBottom;
			PMRect NewBoxBounds;
			NewBoxBounds.Top(top);
			NewBoxBounds.Left(left);
			NewBoxBounds.Bottom(maxBottom );
			NewBoxBounds.Right(right);

			//PMString ASD("NewBoxBounds.Top() : ");
			//ASD.AppendNumber((NewBoxBounds.Top()));
			//ASD.Append("  NewBoxBounds.Left() : ");
			//ASD.AppendNumber((NewBoxBounds.Left()));
			//ASD.Append("  NewBoxBounds.Bottom() : ");
			//ASD.AppendNumber((NewBoxBounds.Bottom()));
			//ASD.Append("  NewBoxBounds.Right() : ");
			//ASD.AppendNumber((NewBoxBounds.Right()));
			//CA(ASD);

			result1 = geometryPtr->SetStrokeBoundingBox( InnerToPasteboardMatrix(geometryPtr), NewBoxBounds/*, IGeometry::kResizeItemAndChildren*/);
			
		}
		
	}
		
	return result1;
}

void SubSectionSprayer::getRelativeMoveAmountOfxAndy(int32 &X,int32 &Y,int32 numberOfPagesInCurrentSpread,int32 CurrentX,int32 CurrentY,int32 pageWidth,int32 sideOfPage)
{
	if(X == 0 && Y == 0 && CurrentX == 0 && CurrentY == 0)
	{
		//CA("dont do anything"); ///old
		if(numberOfPagesInCurrentSpread ==2)
		{
			X = - pageWidth;
		}
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX > 0) || (CurrentY != 0 && CurrentY > 0 && CurrentX > 0)) && numberOfPagesInCurrentSpread == 1)
	{
		//CA("1");					//old
		X = X - CurrentX;
		Y = Y - CurrentY;
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX > 0) || (CurrentY != 0 && CurrentY > 0 && CurrentX > 0)) && numberOfPagesInCurrentSpread == 2)
	{
		//CA("5");					
		X = -(pageWidth + CurrentX);
		Y = -CurrentY;
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX < 0) || (CurrentY != 0 && CurrentY < 0)) && numberOfPagesInCurrentSpread == 2)
	{
		//CA("3");
		X = 0 - (CurrentX + pageWidth);
		Y = - CurrentY;
	}
	else if(X == 0 && Y == 0 && ((CurrentX != 0 && CurrentX < 0) || (CurrentY != 0 && CurrentY < 0)) && numberOfPagesInCurrentSpread == 1)
	{
		//CA(" for left page");
		X = - (pageWidth + CurrentX);
		Y = - CurrentY;
	}
	else if(X != 0 && Y != 0 && ((CurrentX != 0 && CurrentX < 0) || (CurrentY != 0 && CurrentY < 0)))
	{
		//CA("4");
		X = X - (CurrentX + pageWidth);
		Y = Y - CurrentY;
	}
	else if(X == 0 && Y != 0 && numberOfPagesInCurrentSpread == 1)
	{
		//CA("new one for left page == 1");
		if(sideOfPage == 0)
		{
			X = -(pageWidth + CurrentX);			
			Y = Y - CurrentY;
		}
		else
		{
		//	CA("right page");
			X = X - CurrentX;			
			Y = Y - CurrentY;
		}

	}
	else if(X != 0 && Y == 0 && numberOfPagesInCurrentSpread == 1)
	{
		//CA("new one for left page ");
		if(sideOfPage == 0)
		{
		//	CA("left page");
			X = X - (pageWidth + CurrentX); 
			Y = Y - CurrentY;
		}
		else
		{
			//CA("right page");
			X = X - CurrentX;			
			Y = Y - CurrentY;
		}
	}
	else if(numberOfPagesInCurrentSpread == 1)
	{	
		//CA("numberOfPagesInCurrentSpread == 1");
		X = X - CurrentX;			//old
		Y = Y - CurrentY;
	}
	else if(numberOfPagesInCurrentSpread == 2)
	{
		//CA("numberOfPagesInCurrentSpread == 2");
		X = X - pageWidth - CurrentX;
		Y = Y - CurrentY;
	}
	return;
}
//////////////////////////////////////////////////////////////////////////////////////////////
int32 SubSectionSprayer::getNoOfPagesfromCurrentSpread()
{
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
	if (layoutData == nil)
		return 0;

	////****Commented By Sachin Sharma
	/*IGeometry* spreadItem = layoutData->GetSpread();
	if(spreadItem == nil)
		return 0;*/
	UIDRef spreadUIDref = layoutData->GetSpreadRef();///**ADDed
		
	InterfacePtr<ISpread> iSpread(/*SpreadUIDref*/spreadUIDref, UseDefaultIID());
	if (iSpread == nil)
		return 0;

	int numPages=iSpread->GetNumPages();
	return numPages;
}

//////////////////////////////////////////////////////////////////////////////////////////////
////26-april


//////////////////////////////////////////////////////////////////////////////28-april
void SubSectionSprayer::sprayFromSecondProductOfSubSectionWhite(const UIDList& selectedUIDList, const PMRect& origMaxBoxBounds, const PBPMPoint& maxPageSprayCount, int32 numProducts, RangeProgressBar& progressBar)
{
/*-
	//CA(" Inside SubSectionSprayer::sprayFromSecondProductOfSubSection");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CA("ptrIAppFramework == NULL");
		return ;
	}
	do
	{
		bool16 toggleFlag=kTrue;
				
		fillpNodeDataListForWhite();		

		CurrentSectionpNodeDataListForCatalogPlanning.clear();	
	
		CreateMediaPublicationNodeList tempNodeList;
		tempNodeList = pNodeDataList;
		int32 sizeOfNodeList = static_cast<int32> (pNodeDataList.size ());
		numProducts = sizeOfNodeList;  // total numProducts are now changed to total aviaable Pub comments.

		for(int32 currentSpread = 1; currentSpread <= currentSelectedSpreadCount ; currentSpread++)
		{
			int32 numOfProductsForCurrentSpread=0;

			int32 sizeOfCPDataList = static_cast<int32> (CPDataList.size ());
			CurrentSectionpNodeDataListForCatalogPlanning.clear ();
			apPubCommentsBySpreadList.clear ();
			for(int32 j=0; j<sizeOfCPDataList; j++)
			{
				if(CPDataList[j].apPubCommentObj.spreadNumber == currentSpread )
				{
					numOfProductsForCurrentSpread++;
					CurrentSectionpNodeDataListForCatalogPlanning .push_back (CPDataList[j].createMediaPublicationNodeObj);
					apPubCommentsBySpreadList.push_back (CPDataList [j].apPubCommentObj);
				}
			}
		
			bool16 add2ndPage = kTrue; 
			//::AddNewPage(); //Cs3 Depricated
			Utils<ILayoutUIUtils>()->AddNewPage(); //Cs4
			PageCount = PageCount+1;
			UIDRef pageUIDRef1,pageUIDRef2;
			UIDRef spreadUIDRef1,spreadUIDRef2;
			bool16 result = this->getCurrentPage (pageUIDRef1 ,spreadUIDRef1);
			if(result == kFalse)
			{
				//CA("result = this->getCurrentPage kFalse");
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayFromSecondProductOfSubSection::getCurrentPage returns kFalse");			
				break;
			}
			pageUidList.push_back (pageUIDRef1 .GetUID());
//14-may chetan
			//::AddNewPage(); //Cs3 Depricated
			Utils<ILayoutUIUtils>()->AddNewPage(); //Cs4
			PageCount = PageCount+1;
			bool16 resultt = this->getCurrentPage (pageUIDRef2 ,spreadUIDRef2);
			if(resultt == kFalse)
			{
				//CA("result = this->getCurrentPage kFalse");
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayFromSecondProductOfSubSection::getCurrentPage returns kFalse");			
				break;
			}
			pageUidList .push_back (pageUIDRef2 .GetUID());
//14-may
			InterfacePtr<IGeometry> pageGeometry(pageUIDRef1, UseDefaultIID());
			if (pageGeometry == NULL)
			{
				//ASSERT_FAIL("pageGeometry is invalid");
				break;
			}
			
			PMRect pageBounds = pageGeometry->GetStrokeBoundingBox();	
			PMPoint leftTop;
			leftTop = pageBounds.LeftTop();
			::TransformInnerPointToPasteboard(pageGeometry,&leftTop);
			PMReal X1 = leftTop .X();
			PMReal Y1 = leftTop.Y();

			//CA("11");
			for(int32 currentProduct=0; currentProduct < numOfProductsForCurrentSpread; currentProduct++)
			{
				if(apPubCommentsBySpreadList[currentProduct].TOP_X_COORDINATE > 612)
				{//CA("12");
					//commented on 14-may -- to add both the pages on a spread before spraying 
					//if(add2ndPage)
					//{
					//	::AddNewPage();
					//	PageCount = PageCount+1;
					//	bool16 result = this->getCurrentPage (pageUIDRef2 ,spreadUIDRef2);
					//	if(result == kFalse){
					//		ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayFromSecondProductOfSubSection::getCurrentPage returns kFalse");			
					//		break;
					//	}
					//	pageUidList.push_back (pageUIDRef2.GetUID());
					//	add2ndPage = kFalse;
					//}
					this->sprayPageWithResizableFrameNewWhite(pageUIDRef2 , origMaxBoxBounds, currentProduct , currentSpread , progressBar, X1, Y1);
				//CA("13");
				}
				else
				{	
					//CA("14");
					this->sprayPageWithResizableFrameNewWhite(pageUIDRef1 , origMaxBoxBounds, currentProduct , currentSpread , progressBar, X1, Y1);
					//CA("15");
				}
			}
		}		
	}
	while(kFalse);
-*/
}

////////////////////////////////////////////////////////////////////////////28-april

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////
void SubSectionSprayer::sprayPageWithResizableFrameNewWhite(const UIDRef& pageUIDRef, const PMRect& origMaxBoxBounds, int32& currentProduct,int32& currentSpread, RangeProgressBar& progressBar, PMReal TopX, PMReal TopY)
{//CA("SubSectionSprayer::sprayPageWithResizableFrameNew");
/*-
    do
	{
		
		bool16 result = kFalse;
	
		InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			break;
		}
		//selectionManager->DeselectAll(nil); // deselect every active CSB
							  
		int32 isProductFlag = CurrentSectionpNodeDataListForCatalogPlanning[currentProduct].getIsProduct();
		bool16 isSeparateStencil = isProdStencil || isItemStencil;					
		int32 StencilNo =0;
	//CA("2");
		do{
			if(!isSeparateStencil){
				layoutSelectionSuite->SelectPageItems(MediatorClass ::allStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
				StencilNo = 1;
				break;
			}
			else if(isProductFlag == 1 && isProdStencil) 
			{
				layoutSelectionSuite->SelectPageItems(MediatorClass ::productStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
				StencilNo = 2;
				break;
			}
			else if(isProductFlag ==0 && isItemStencil)
			{
				layoutSelectionSuite->SelectPageItems(MediatorClass ::itemStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
				StencilNo = 3;
				break;
			}
			else{
				layoutSelectionSuite->SelectPageItems(MediatorClass ::allStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);
				StencilNo = 1;
				break;
			}		
		}while(0);

		PBPMPoint moveToPoints;
		APpubComment pubComment = apPubCommentsBySpreadList[currentProduct];
		PMReal topX = pubComment.TOP_X_COORDINATE + TopX;
		PMReal topY = pubComment.TOP_Y_COORDINATE + TopY;
		
		PMPoint points(topX , topY );
		moveToPoints = points;


		this->CopySelectedItems();

		//now get the copied item list
		UIDList copiedBoxUIDList;
		result = this->getSelectedBoxIds(copiedBoxUIDList);
		if(result == kFalse){
			//CA("result == kFalse  ");
			break;
		}

		//now move boxes to appropriate positions
		this->moveBoxes(copiedBoxUIDList, moveToPoints);
		vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
		PMRect CopiedItemMaxBoxBoundsBforeSpray;
		result = kFalse;
		result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);
		
		//spray for the selected boxes which were just copied
		//start with the second product
		this->getAllBoxIds(copiedBoxUIDList);
						
		//treeCache.isExist(tempIdList[sprayedProductIndex], node);				
		PMString tempString("Spraying ");
		//tempString +=pNodeDataList[sprayedProductIndex].getName();
		tempString +=CurrentSectionpNodeDataListForCatalogPlanning[currentProduct].getName();
		tempString += "...";
		progressBar.SetTaskText(tempString);

		InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
		if(!DataSprayerPtr)
		{
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayPageWithResizableFrameNew::Pointre to DataSprayerPtr not found");
			return;
		}					
		
		//DataSprayerPtr->FillPnodeStruct(pNodeDataList[sprayedProductIndex],CurrentSelectedSection,CurrentSelectedPublicationID);				
		//DataSprayerPtr->FillPnodeStruct(CurrentSectionpNodeDataListForCatalogPlanning[sprayedProductIndex],CurrentSelectedSection,CurrentSelectedPublicationID, CurrentSelectedSubSection);				
		PublicationNode pNode =CurrentSectionpNodeDataListForCatalogPlanning[currentProduct].convertToPublicationNode();

		{
			//CA("MediatorClass ::radioCreateMediaBySectionWidgetSelected == kFalse");
			DataSprayerPtr->FillPnodeStruct
			(	pNode,
				CurrentSectionpNodeDataListForCatalogPlanning[currentProduct].sectionID,
				CurrentSectionpNodeDataListForCatalogPlanning[currentProduct].publicationID,
				CurrentSectionpNodeDataListForCatalogPlanning[currentProduct].subSectionID
				
			);
		}
		//CA("Before Start Spraying called");
		this->startSpraying();
		sprayedProductIndex++;		
		
		progressBar.SetPosition(sprayedProductIndex);
		
	}while(kFalse);
	//CA("After Return From sprayPageWithResizableFrameNewWhite");
 -*/
}

/////////////////////////////////

PMPoint SubSectionSprayer::PageToSpreadCoOrdinates(const UIDRef& pageUIDRef, const PMPoint& boundsInPageCoords)
{
	PMPoint result(boundsInPageCoords);
	do {
		InterfacePtr<ITransform> transform(pageUIDRef, UseDefaultIID());
		ASSERT(transform);
		if (!transform) {
			break;
		}		
		/*::InnerToParent(transform, &result);*/ ///**Commented
		::TransformInnerPointToParent (transform, &result);///*added
	} while (false);
	return result;
}

bool16 SubSectionSprayer::getStencilInfo(const UIDList& SelUIDList,CSprayStencilInfo& objCSprayStencilInfo)
{
	//CA("SubSectionSprayer::getStencilInfo");
	bool16 result = kFalse;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return result;
	}

	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		return result;
	}

	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogDebug("ProductFinderPalete::SubSectionSprayer::getStencilInfo:Pointer to DataSprayerPtr not found");//
		return result;
	}


	do
	{
		for(int32 index = 0; index < SelUIDList.Length(); index++)
		{
			UIDRef boxUIDRef = SelUIDList.GetRef(index);
			
			TagList tList=itagReader->getTagsFromBox(boxUIDRef);
			int numTags=static_cast<int>(tList.size());
			if(numTags<0)
			{
				//CA("numTags<0");
			}

			/*PMString numOfTags("numTags = ");
			numOfTags.AppendNumber(numTags);
			CA(numOfTags);*/

			if(numTags<=0)//This can be a Tagged Frame
			{	
				tList=itagReader->getFrameTags(boxUIDRef);							
			}	

			int numTags1=static_cast<int>(tList.size());
			if(numTags1<0)
			{
				//CA("numTags1<0");
				continue;
			}

			bool16 isCustomTablePresent = kFalse;
			TagList tableTagList1;
			for(int32 tagIndex = 0; tagIndex < tList.size(); tagIndex++)
			{
				TagStruct & tagInfo = tList[tagIndex];
				objCSprayStencilInfo.langId = tagInfo.languageID;

				if(tagInfo.isSprayItemPerFrame != -1)	
				{
					objCSprayStencilInfo.isChildTag = kTrue;
					objCSprayStencilInfo.isCustomTablePresent = kTrue;
                    objCSprayStencilInfo.isSprayItemPerFrame = kTrue;
				}


				if(tagInfo.isTablePresent)
				{
					//CA("Table Present");
					if(tagInfo.whichTab == 3 && tagInfo.tableType == 2)
					{
						isCustomTablePresent = kTrue;
						objCSprayStencilInfo.isProductDBTable = kTrue;
                        if(tagInfo.typeId > 0)
                            objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);
						
						TagList tableTagList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxUIDRef);
						int numTags=static_cast<int>(tableTagList.size());
						if(numTags<0)
						{
							//CA("numTags<0");
							continue;
						}

						//PMString numOfTags("numTags 33333333333= ");
						//numOfTags.AppendNumber(numTags);
						//CA(numOfTags);

						for(index = 0; index < numTags; index++)
						{
							TagStruct & tagInfoo = tableTagList[index];
							tableTagList1.push_back(tagInfoo);
						}						
					}
					else if(tagInfo.whichTab == 4  && tagInfo.tableType == 2)
					{
						isCustomTablePresent = kTrue;

						objCSprayStencilInfo.isDBTable = kTrue;
						objCSprayStencilInfo.isCustomTablePresent = kTrue;
                        if(tagInfo.typeId > 0)
                            objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);

						TagList tableTagList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxUIDRef);
						int numTags=static_cast<int>(tableTagList.size());
						if(numTags<0)
						{
							//CA("numTags<0");
							continue;
						}

						//PMString numOfTags("numTags 2222222222= ");
						//numOfTags.AppendNumber(numTags);
						//CA(numOfTags);

						for(int32 index = 0; index < numTags; index++)
						{
							TagStruct & tagInfoo = tableTagList[index];
							tableTagList1.push_back(tagInfoo);
						}	
					}

					
					continue;
				}
			}

			
			if(isCustomTablePresent)
			{
				int numTags=static_cast<int32>(tableTagList1.size());
				if(numTags > 0)
				{
					//CA("numTags<0");
					//PMString numOfTags("numTags = ");
					//numOfTags.AppendNumber(numTags);
					//CA(numOfTags);

					for(int32 index = 0; index < numTags; index++)
					{
						TagStruct & tagInfoo = tableTagList1[index];
						tList.push_back(tagInfoo);
					}	
									
					int numTagsnew=static_cast<int>(tList.size());
					if(numTagsnew < 0)
					{
						//CA("numTags<0");
						
					}

					//PMString numOfTagss("numTags 11111111111= ");
					//numOfTagss.AppendNumber(numTagsnew);
					//CA(numOfTagss);
				}
			}

			for(int32 tagIndex = 0; tagIndex < tList.size(); tagIndex++)
			{
				TagStruct & tagInfo = tList[tagIndex];
				
				if(tagInfo.isTablePresent)
				{
					//CA("Table Present");
					if(tagInfo.elementId == -115 && tagInfo.whichTab == 3)
					{
						objCSprayStencilInfo.isProductHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.elementId == -115 && tagInfo.whichTab == 4)
					{
						objCSprayStencilInfo.isHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.elementId == -115 && tagInfo.whichTab == 5)
					{
						objCSprayStencilInfo.isSectionLevelHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.whichTab == 3)
					{
						objCSprayStencilInfo.isProductDBTable = kTrue;
                        if(tagInfo.typeId > 0)
                            objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.whichTab == 4)
					{
						objCSprayStencilInfo.isDBTable = kTrue;
                        if(tagInfo.typeId > 0)
                            objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);
					}
                    if(tagInfo.whichTab == 3)
                    {
                        objCSprayStencilInfo.isProductCopy = kTrue;
                    }
                    
                    objCSprayStencilInfo.isCopy = kTrue;
                    objCSprayStencilInfo.isProductChildTag = kTrue;
                    
                    if( (tagInfo.childTag == 1) && (tagInfo.whichTab == 4))
                        objCSprayStencilInfo.isChildTag = kTrue;
					continue;
				}

				if(tagInfo.imgFlag == 1)
				{
					//CA("image Present");
					//objCSprayStencilInfo.isAsset = kTrue;
                    if(tagInfo.typeId > 0)
                        objCSprayStencilInfo.AssetIds.push_back(tagInfo.typeId);
					
					if(tagInfo.whichTab == 3)
					{
                        if(tagInfo.typeId > 0)
                            objCSprayStencilInfo.ProductAssetIds.push_back(tagInfo.typeId);
						if(tagInfo.typeId <= -207 && tagInfo.typeId >= -221)
							objCSprayStencilInfo.isProductBMSAssets = kTrue;
						else if(tagInfo.elementId > 0)
						{
							objCSprayStencilInfo.isProductPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.productPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.productPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.productPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.productPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.productPVAssetIdList.push_back(tagInfo.elementId);
						}
						else
							objCSprayStencilInfo.isProductAsset = kTrue;

					}
					else if(tagInfo.whichTab == 4)
					{
                        if(tagInfo.typeId > 0)
                            objCSprayStencilInfo.itemAssetIds.push_back(tagInfo.typeId);
                        
						objCSprayStencilInfo.isProductChildTag = kTrue;

						if(tagInfo.typeId <= -207 && tagInfo.typeId >= -221)
							objCSprayStencilInfo.isBMSAssets = kTrue;
						else if(tagInfo.elementId > 0)
						{
							//CA("tagInfo.elementId > 0");
							objCSprayStencilInfo.isItemPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.itemPVAssetIdList.size()>0)
							{
								//CA("objCSprayStencilInfo.itemPVAssetIdList.size()>0");
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.itemPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.itemPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.itemPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
							{
								//CA("objCSprayStencilInfo.itemPVAssetIdList.push_back");
								objCSprayStencilInfo.itemPVAssetIdList.push_back(tagInfo.elementId);
							}
						}
						else
							objCSprayStencilInfo.isAsset = kTrue;

					}
					else if(tagInfo.whichTab == 5)
					{
						if(tagInfo.typeId == -222 || tagInfo.typeId == -223)
							objCSprayStencilInfo.isSectionLevelBMSAssets = kTrue;
						else if(tagInfo.colno == -28)
						{
							objCSprayStencilInfo.isSectionPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.sectionPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.sectionPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.sectionPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.sectionPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.sectionPVAssetIdList.push_back(tagInfo.elementId);
						}
						else if(tagInfo.colno == -27)
						{
							objCSprayStencilInfo.isPublicationPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.publicationPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.publicationPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.publicationPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.publicationPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.publicationPVAssetIdList.push_back(tagInfo.elementId);
						}
						else if(tagInfo.colno > 0)
						{
							objCSprayStencilInfo.isCatagoryPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.catagoryPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.catagoryPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.catagoryPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.catagoryPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.catagoryPVAssetIdList.push_back(tagInfo.elementId);
						}
						else if(tagInfo.catLevel < 0 )
						{
							objCSprayStencilInfo.isEventSectionImages = kTrue;
							if(objCSprayStencilInfo.eventSectionAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.eventSectionAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.eventSectionAssetIdList[i] == tagInfo.typeId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
                                {
                                    if(tagInfo.typeId > 0)
									objCSprayStencilInfo.eventSectionAssetIdList.push_back(tagInfo.typeId);
                                }
							}
							else
                            {
                                if(tagInfo.typeId > 0)
								objCSprayStencilInfo.eventSectionAssetIdList.push_back(tagInfo.typeId);
                            }
						}
						else if(tagInfo.catLevel > 0)
						{
							objCSprayStencilInfo.isCategoryImages = kTrue;
							if(objCSprayStencilInfo.categoryAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.categoryAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.categoryAssetIdList[i] == tagInfo.typeId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
                                {
                                    if(tagInfo.typeId > 0)
									objCSprayStencilInfo.categoryAssetIdList.push_back(tagInfo.typeId);
                                }
							}
                            else
                            {
                                if(tagInfo.typeId > 0)
								objCSprayStencilInfo.categoryAssetIdList.push_back(tagInfo.typeId);
                            }
						}


					}	

					continue;
				}
				
				//CA("copy Attribute");
                if(tagInfo.elementId > 0)
				objCSprayStencilInfo.AttributeIds.push_back(tagInfo.elementId);

				if(tagInfo.whichTab == 3)
                {
					objCSprayStencilInfo.isProductCopy = kTrue;
                    if(tagInfo.elementId > 0)
                        objCSprayStencilInfo.ProductAttributeIds.push_back(tagInfo.elementId);
                }
				if(tagInfo.whichTab == 4)
				{
					objCSprayStencilInfo.isCopy = kTrue;
					objCSprayStencilInfo.isProductChildTag = kTrue;
                    if(tagInfo.elementId > 0)
                        objCSprayStencilInfo.itemAttributeIds.push_back(tagInfo.elementId);
                    else if( tagInfo.elementId == -703 || tagInfo.elementId == -704)
                    {
                        double eventPriceId = -1;
                        double regularPriceId = -1;
                        eventPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_SALE_PRICE");
                        regularPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_REGULAR_PRICE");
                        objCSprayStencilInfo.itemAttributeIds.push_back(eventPriceId);
                        objCSprayStencilInfo.itemAttributeIds.push_back(regularPriceId);
                    }
				}
				if(tagInfo.whichTab == 5)
					objCSprayStencilInfo.isSectionCopy = kTrue;
				
				if(!isSpreadBasedLetterKeys)
				{
					if(tagInfo.elementId == -803 && tagInfo.typeId == 1)
					{
						isSpreadBasedLetterKeys = kTrue;
					}
					else 
					{
						isSpreadBasedLetterKeys = kFalse;
					}
				}

				if(tagInfo.childTag == 1)
                {
					objCSprayStencilInfo.isChildTag = kTrue;
                    if(tagInfo.elementId > 0)
                        objCSprayStencilInfo.childItemAttributeIds.push_back(tagInfo.elementId);
                    else if( tagInfo.elementId == -703 || tagInfo.elementId == -704)
                    {
                        double eventPriceId = -1;
                        double regularPriceId = -1;
                        eventPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_SALE_PRICE");
                        regularPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_REGULAR_PRICE");
                        objCSprayStencilInfo.childItemAttributeIds.push_back(eventPriceId);
                        objCSprayStencilInfo.childItemAttributeIds.push_back(regularPriceId);
                    }
                }

				if(tagInfo.isEventField == 1)
				{
					//CA("objCSprayStencilInfo.isEventField = kTrue");
					objCSprayStencilInfo.isEventField = kTrue;
				}

				if(tagInfo.isSprayItemPerFrame != -1)	
				{
					objCSprayStencilInfo.isChildTag = kTrue;
					objCSprayStencilInfo.isCustomTablePresent = kTrue;
                    objCSprayStencilInfo.isSprayItemPerFrame = kTrue;
				}
				

			}
			//------------
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}
	}while(kFalse);
	result = kTrue;
	return result;
}

bool16 SubSectionSprayer::sprayPageWithResizableFrameForSection(bool16 toggleFlag,PMRect &marginBoxBounds,FrameBoundsList &ProdBlockBoundList,int16 &horizCnt,int16 &vertCnt)
{
//	CA("sprayPageWithResizableFrameForSection");

	bool16 sprayingDone = kFalse;
	bool16 islasthorzFrame = kFalse;
	PMReal OrgBoxMaxWidth = 0.0, OrgBoxMaxHeight=0.0, maxPageWidth=0.0, maxPageHeight=0.0;
	
	maxPageWidth = abs(marginBoxBounds.Right() - marginBoxBounds.Left());
	maxPageHeight = abs(marginBoxBounds.Bottom() - marginBoxBounds.Top());
	PMReal VerticalBoxSpacing = MediatorClass ::subSecSpraySttngs.verticalBoxSpacing;	
	

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}

	InterfacePtr<ISelectionManager>	selectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(selectionManager==nil)
	{
		return kFalse;
	}
	InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
	if (!layoutSelectionSuite) 
	{
		ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::sprayPageWithResizableFrameNew::!layoutSelectionSuite");										
		return kFalse; 
	}

	bool16 isLeftToRightFlag = kTrue;
	if(MediatorClass ::subSecSpraySttngs.flowHorizontalFlow)
	{
		//CA("MediatorClass ::subSecSpraySttngs.flowHorizontalFlow	==	kTrue");
		int Condition = 1;
		while(Condition)
		{	
			bool16 islasthorzFrame = kFalse;
					
			selectionManager->DeselectAll(nil); // deselect every active CSB
			
			bool16 isLeftToRightFlag = kTrue;
				

			if(MediatorClass ::subSecSpraySttngs.alternateForEachPage)
			{			
				if(MediatorClass ::subSecSpraySttngs.flowLeftToRight)
				{
					if(toggleFlag)
						isLeftToRightFlag = kFalse;
					else
						isLeftToRightFlag = kTrue; 
				}
				else
				{
					if(toggleFlag)
						isLeftToRightFlag = kTrue;
					else
						isLeftToRightFlag = kFalse;
				}			
			}
			else
			{
				if(MediatorClass ::subSecSpraySttngs.flowLeftToRight == kFalse)
				{
					isLeftToRightFlag = kFalse; 
				}
				else
					isLeftToRightFlag = kTrue;	
			}
				
			layoutSelectionSuite->/*Select*/SelectPageItems(MediatorClass::sectionStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);		//

			OrgBoxMaxHeight = abs(SectionStencilMaxBounds.Bottom() - SectionStencilMaxBounds.Top());	//

			PBPMPoint moveToPoints;				

			bool16 result = this->getBoxPositionForResizableFrameNew(marginBoxBounds, SectionStencilMaxBounds, horizCnt, vertCnt, ProdBlockBoundList, moveToPoints, islasthorzFrame, isLeftToRightFlag);
			if(!result)
				break;
				
			//copy the selected items
			this->CopySelectedItems();
			//now get the copied item list
		
			UIDList copiedBoxUIDList;
			
			result = this->getSelectedBoxIds(copiedBoxUIDList);
			if(result == kFalse)
				break;

			//now move boxes to appropriate positions
			this->moveBoxes(copiedBoxUIDList, moveToPoints);

			vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
			PMRect CopiedItemMaxBoxBoundsBforeSpray;

			result = kFalse;
			result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);

			//spray for the selected boxes which were just copied
			//start with the second product
			this->getAllBoxIds(copiedBoxUIDList);
			
			InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
			if(!DataSprayerPtr)
			{
				ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::sprayPageWithResizableFrameNew::Pointre to DataSprayerPtr not found");
				return kFalse;
			}
			
			PublicationNode pNode =CurrentSectionpNodeDataList[sprayedProductIndex].convertToPublicationNode();
			
			DataSprayerPtr->FillPnodeStruct
					(pNode,CurrentSelectedSection,CurrentSelectedPublicationID,CurrentSelectedSubSection);	
			
		//	int32 objectId = -1;
		//	objectId = CurrentSectionpNodeDataList[sprayedProductIndex].getPBObjectID();
			//CA("Before startSpraying");

			this->startSpraying();
			
			this->moveAutoResizeBoxAfterSpray(copiedBoxUIDList, vectorCopiedBoxBoundsBforeSpray);
			selectUIDList = copiedBoxUIDList;			
			
			
			PMRect CopiedItemMaxBoxBounds;
			vectorBoxBounds vectorCopiedBoxBounds;
			result = kFalse;

			result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, CopiedItemMaxBoxBounds, vectorCopiedBoxBounds);
			if(result == kFalse)
			{
				ptrIAppFramework->LogError("AP46_ProductFinder::SubSectionSprayer::sprayPageWithResizableFrameNew::!result");										
				break;
			}

			if( (marginBoxBounds.Bottom() - CopiedItemMaxBoxBounds.Bottom() < 0) || (marginBoxBounds.Right() - CopiedItemMaxBoxBounds.Right() < 0) )
			{
				//CA("Box going out of Page Length");
				if(vertCnt != 0)
				{
					this->deleteThisBoxUIDList(selectUIDList);
					//CA("After Deleting Boxes");
					StncilOverlengthFlag = kTrue;
					OverlengthHorizCount++;
					OverLengthoffsetWidth = CopiedItemMaxBoxBounds;
					horizCnt++;
					if(islasthorzFrame)
					{
						//CA("Condition = 0");
						horizCnt++;
//						condition1 =0;
						Condition = 0;
						break;
					}	
					continue;
				}
				else
					this->AdjustMaxLimitsOfBoxes(copiedBoxUIDList,marginBoxBounds); 
			}
		
			DynFrameStruct CurrentFameStruct;
			CurrentFameStruct.HorzCnt = horizCnt;
			CurrentFameStruct.VertCnt = vertCnt;
			CurrentFameStruct.BoxBounds = CopiedItemMaxBoxBounds;
			CurrentFameStruct.isLastHorzFrame = islasthorzFrame;
			ProdBlockBoundList.push_back(CurrentFameStruct);
			if(islasthorzFrame)
			{
				//CA("islasthorzFrame kTrue ");					
				vertCnt++;
				horizCnt=0;
			}
			else
			{
				//CA("islasthorzFrame kFalse ");
				horizCnt++;
			}
			vertCnt++;
			sprayingDone = kTrue;
			Condition = 0;

			sectionStencilUIDListToDelete = selectUIDList;
		}
	}
	else if(!MediatorClass ::subSecSpraySttngs.flowHorizontalFlow)
	{	
		//CA("MediatorClass ::subSecSpraySttngs.flowHorizontalFlow == kFalse");
		
		int condition1 = 1;
		bool16 islasthorzFrame = kFalse;
		while(condition1)
		{	
			bool16 isLeftToRightFlag = kTrue;

			selectionManager->DeselectAll(nil); // deselect every active CSB
			layoutSelectionSuite->/*Select*/SelectPageItems(MediatorClass::sectionStencilUIDList, Selection::kReplace,  Selection::kDontScrollLayoutSelection);  ///Added Amit
		
			PBPMPoint moveToPoints;	
			OrgBoxMaxHeight = abs(SectionStencilMaxBounds.Bottom() - SectionStencilMaxBounds.Top());		
		
			if(vertCnt >= 0 && ProdBlockBoundList.size()!= 0 )
			{
				bool16 result1 = kFalse;						
				PMReal TotalHeight=0.0;		
				for(int p=0; p<ProdBlockBoundList.size(); p++)
				{
					if(ProdBlockBoundList[p].HorzCnt == horizCnt)
					{						
						if(TotalHeight < abs(ProdBlockBoundList[p].BoxBounds.Bottom()- marginBoxBounds.Top()))
						{	
							TotalHeight = abs(ProdBlockBoundList[p].BoxBounds.Bottom()- marginBoxBounds.Top());
							TotalHeight += VerticalBoxSpacing;
						}
											
					}
				}
				if((OrgBoxMaxHeight) > (maxPageHeight - TotalHeight))
				{
					//CA("OrgBoxMaxHeight > (maxPageHeight - TotalHeight)");
					if(!islasthorzFrame)
					{
						horizCnt++;
						vertCnt = 0;
						continue;
					}
					else
					{
						condition1=0;
						return kFalse;
					}
				}
			}

			if(MediatorClass ::subSecSpraySttngs.alternateForEachPage)
			{			
				if(MediatorClass ::subSecSpraySttngs.flowLeftToRight)
				{
					if(toggleFlag)
						isLeftToRightFlag = kFalse; 	
					else
						isLeftToRightFlag = kTrue;
				}
				else
				{
					if(toggleFlag)
						isLeftToRightFlag = kTrue; 
					else
						isLeftToRightFlag = kFalse;
				}			
			}
			else
			{
				if(!MediatorClass ::subSecSpraySttngs.flowLeftToRight)
				{
					isLeftToRightFlag = kFalse;
				}
				else
					isLeftToRightFlag = kTrue;				
			}

			bool16 result = this->getBoxPositionForResizableFrameNewVerticalFlow(marginBoxBounds, SectionStencilMaxBounds, horizCnt, vertCnt, ProdBlockBoundList, moveToPoints, islasthorzFrame, isLeftToRightFlag);	///	Added Amit
			if(result == kFalse)
				return kFalse;
						
			this->CopySelectedItems();

			UIDList copiedBoxUIDList;
			
			result = this->getSelectedBoxIds(copiedBoxUIDList);
			if(result == kFalse)
				return kFalse; 
			
			this->moveBoxes(copiedBoxUIDList, moveToPoints);
			
			vectorBoxBounds vectorCopiedBoxBoundsBforeSpray;
			PMRect CopiedItemMaxBoxBoundsBforeSpray;
			result = kFalse;
			result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, CopiedItemMaxBoxBoundsBforeSpray, vectorCopiedBoxBoundsBforeSpray);
			if(!result)
				return kFalse;
	
	//		//spray for the selected boxes which were just copied
	//		//start with the second product
	
			this->getAllBoxIds(copiedBoxUIDList);

			InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
			if(!DataSprayerPtr)
			{
				ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::sprayPageWithResizableFrameNew::Pointre to DataSprayerPtr not found");
				return kFalse;
			}					
			
			PublicationNode pNode = CurrentSectionpNodeDataList[sprayedProductIndex].convertToPublicationNode();

			DataSprayerPtr->FillPnodeStruct
					(pNode,CurrentSelectedSection,CurrentSelectedPublicationID, CurrentSelectedSubSection);	
				
		//	int32 objectId = -1;
		//	objectId = CurrentSectionpNodeDataList[sprayedProductIndex].getPBObjectID();

			this->startSpraying();


			selectionManager->DeselectAll(nil);
			this->moveAutoResizeBoxAfterSpray(copiedBoxUIDList, vectorCopiedBoxBoundsBforeSpray);
			selectUIDList = copiedBoxUIDList;

			PMRect CopiedItemMaxBoxBounds;
			vectorBoxBounds vectorCopiedBoxBounds;

			result = kFalse;
			result = this->getMaxLimitsOfBoxes(copiedBoxUIDList, CopiedItemMaxBoxBounds, vectorCopiedBoxBounds);
			if(!result)
				return kFalse;
			
			this->AdjustMaxLimitsOfBoxes(copiedBoxUIDList,marginBoxBounds); 
			
			if((marginBoxBounds.Bottom() - CopiedItemMaxBoxBounds.Bottom() < 0) || (marginBoxBounds.Right() - CopiedItemMaxBoxBounds.Right() < 0) )
			{
				//CA("page width or page height not sufficient");
				if(vertCnt != 0)
				{
					//CA("aaaaa");
					this->deleteThisBoxUIDList(selectUIDList);
					StncilOverlengthFlag = kTrue;
					OverlengthHorizCount++;
					OverLengthoffsetWidth = CopiedItemMaxBoxBounds;
					horizCnt++;
					if(islasthorzFrame)
					{
						//CA("bbbb");
						condition1=0;
						sprayingDone = kFalse;
						return kFalse;
					}
					continue;
				}
				else
					this->AdjustMaxLimitsOfBoxes(copiedBoxUIDList,marginBoxBounds); 
				
			}
							
			DynFrameStruct CurrentFameStruct;
			CurrentFameStruct.HorzCnt = horizCnt;
			CurrentFameStruct.VertCnt = vertCnt;
			CurrentFameStruct.BoxBounds = CopiedItemMaxBoxBounds;
			CurrentFameStruct.isLastHorzFrame = islasthorzFrame;
			ProdBlockBoundList.push_back(CurrentFameStruct);

			if(islasthorzFrame)
			{
				vertCnt++;	///		Added
			}
			else
			{	
				vertCnt++;
			}	
			sprayingDone = kTrue;
			condition1=0;

			sectionStencilUIDListToDelete = selectUIDList;
		}
	}
	return sprayingDone;
}





void SubSectionSprayer::startSprayingSubSectionNew(bool16 &toggleFlag,PMRect &marginBoxBounds,bool16 &isCancelHit)
{
//CA("startSprayingSubSectionNew");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return;
	}
	do
	{

		CurrentSectionpNodeDataList.clear();
		CurrentSectionpNodeDataList = pNodeDataList;

	//	pageNoFromDb = CurrentSectionpNodeDataList[0].pageno;
		
		//get the list of selected box UIDRef's
		UIDList originalSelUIDList;
		bool16 result = kFalse;
		result = this->getSelectedBoxIds(originalSelUIDList);
		if(result == kFalse){ 
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getSelectedBoxIds return kFalse");
			break;
		}
		if(originalSelUIDList.Length()==0){ 
			ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::originalSelUIDList.Length()==0");
			break;
		}
			
			
		//CA("3");		
		//PMString ASD("originalSelUIDList.Length() : ");
		//ASD.AppendNumber(originalSelUIDList.Length());
		//CA(ASD);

		for(int32 j=0; j<originalSelUIDList.Length();j++)
		{
			//TagReader tReader;
			TagStruct tagInfo;

			InterfacePtr<ITagReader> itagReader
				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReader){
			return ;
			}
			TagList tList=itagReader->getTagsFromBox(originalSelUIDList.GetRef(j));
			if(tList.size()==0)
				tList = itagReader->getFrameTags(originalSelUIDList.GetRef(j));
			if(tList.size()==0)
			{
				/*originalSelUIDList.Remove(j);
				j--;*/							// commented for adding Additional line at bottom of Product Block in Lazboy
				continue;
			}
			tagInfo=tList[0];
			if(tagInfo.isProcessed || tagInfo.parentId!=-1 || tagInfo.numValidFields < NUM_TAGS_FIELDS)
			{
				originalSelUIDList.Remove(j);
				j--;
			}

			//******Added
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			//******
		
		}
		if(originalSelUIDList.Length()==0){ 
			ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::originalSelUIDList.Length()==0");
			break;
		}
		//CA("4");
		//box manipulation
		PMRect origMaxBoxBounds;

		result = this->getMaxLimitsOfBoxes(originalSelUIDList, origMaxBoxBounds, OriginalBoxBoundVector);
		if(result == kFalse){
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getMaxLimitsOfBoxes returns kFalse");		
			break;
		}
//CA("5");		
		//page manipulation

		//CA("6");		
		//////////////////////////////////////////////////////////////////////
		//changing the values of marginBoxBounds according to set values.
		//CMM Comment
		/*
		InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
		if(iSSSprayer==nil)
		{
			CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
			return;
		}
		*/

		//CMM related comment
		/*
		bool16 isOpen = iSSSprayer->callDialog();
		if(!isOpen)
			return;
		*/

//		marginBoxBounds.Left() += 0.0/*iSSSprayer->getLeftMargin()*/;
//		marginBoxBounds.Right() -= 0.0/*iSSSprayer->getRightMargin()*/;
//		marginBoxBounds.Top() += 0.0/*iSSSprayer->getTopMargin()*/;
//		marginBoxBounds.Bottom() -= 0.0/*iSSSprayer->getBottomMargin()*/;
		//////////////////////////////////////////////////////////////////////
	
		isProdStencil = kFalse;
		isItemStencil = kFalse;
		isHybridTableStencil = kFalse;
		isAllStencil = kFalse;
		isSectionStencil = kFalse;

		if(MediatorClass ::productStencilUIDList.Length() == 0)
			isProdStencil = kFalse;
		else
			isProdStencil = kTrue;

		if(MediatorClass ::itemStencilUIDList.Length() == 0)
			isItemStencil = kFalse;
		else
			isItemStencil = kTrue;

		if(MediatorClass ::hybridTableStencilUIDList.Length() == 0)
			isHybridTableStencil = kFalse;
		else
			isHybridTableStencil = kTrue;

		if(MediatorClass ::allStencilUIDList.Length() == 0)
			isAllStencil = kFalse;
		else
			isAllStencil = kTrue;

		if(MediatorClass ::sectionStencilUIDList.Length() == 0)
			isSectionStencil = kFalse;
		else
			isSectionStencil = kTrue;


		int16 horizSprayCount = -1, vertSprayCount = -1;
		int16 horizSprayCountProd = -1, vertSprayCountProd= -1;
		int16 horizSprayCountItem = -1, vertSprayCountItem = -1;
		int16 horizSprayCountAll = -1, vertSprayCountAll = -1;
		int16 horizSprayCountHybridTable = -1, vertSprayCountHybridTable = -1;

		CSprayStencilInfoVector.clear();
		CSprayStencilInfo objCSprayStencilInfo;

		if(isItemStencil) 
		{
			//CA("isItemStencil");
			ItemStencilBoxBoundVector.clear();
			ItemStencilMaxBounds = kZeroRect;
			
			
			result = this->getStencilInfo(MediatorClass ::itemStencilUIDList,objCSprayStencilInfo);
			if(result == kFalse)
				break;

			

			result = this->getMaxLimitsOfBoxes(MediatorClass ::itemStencilUIDList, ItemStencilMaxBounds, ItemStencilBoxBoundVector);
			if(result == kFalse){
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getMaxLimitsOfBoxes returns kFalse");			
				break;
			}

			this->getMaxHorizSprayCount(marginBoxBounds, ItemStencilMaxBounds, horizSprayCountItem);
			this->getMaxVertSprayCount(marginBoxBounds, ItemStencilMaxBounds, vertSprayCountItem);
			
			if(horizSprayCountItem<=0 || vertSprayCountItem<=0)    // if(horizSprayCount<=0 || vertSprayCount<=0)
			{
				CA("The total selected box(es) size does not fit within the margins set for Item Template.  Please increase the margins.");
				break;
			}
		}

		if(isProdStencil)
		{
			//CA("isProdStencil");
			ProdStencilBoxBoundVector.clear();
			ProdStencilMaxBounds = kZeroRect;

			
			result = this->getStencilInfo(MediatorClass ::productStencilUIDList,objCSprayStencilInfo);
			if(result == kFalse)
				break;

			

			result = this->getMaxLimitsOfBoxes(MediatorClass ::productStencilUIDList, ProdStencilMaxBounds, ProdStencilBoxBoundVector);
			if(result == kFalse){
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getMaxLimitsOfBoxes returns kFalse");			
				break;
			}

			this->getMaxHorizSprayCount(marginBoxBounds, ProdStencilMaxBounds, horizSprayCountProd);
			this->getMaxVertSprayCount(marginBoxBounds, ProdStencilMaxBounds, vertSprayCountProd);
			
			if(horizSprayCountProd<=0 || vertSprayCountProd<=0)    // if(horizSprayCount<=0 || vertSprayCount<=0)
			{
				CA("The total selected box(es) size does not fit within the margins set for Product Template.  Please increase the margins.");
				break;
			}
		}
		
		if(isHybridTableStencil) 
		{
			//CA("isHybridTableStencil");
			HybridTableStencilBoxBoundVector.clear();
			HybridTableStencilMaxBounds = kZeroRect;

			
			result = this->getStencilInfo(MediatorClass ::hybridTableStencilUIDList,objCSprayStencilInfo);
			if(result == kFalse)
				break;

			

			result = this->getMaxLimitsOfBoxes(MediatorClass ::hybridTableStencilUIDList, HybridTableStencilMaxBounds, HybridTableStencilBoxBoundVector);
			if(result == kFalse){
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getMaxLimitsOfBoxes returns kFalse");			
				break;
			}

			this->getMaxHorizSprayCount(marginBoxBounds, HybridTableStencilMaxBounds, horizSprayCountItem);
			this->getMaxVertSprayCount(marginBoxBounds, HybridTableStencilMaxBounds, vertSprayCountItem);
			
			if(horizSprayCountItem<=0 || vertSprayCountItem<=0)    // if(horizSprayCount<=0 || vertSprayCount<=0)
			{
				CA("The total selected box(es) size does not fit within the margins set for HybridTable Template.  Please increase the margins.");
				break;
			}
		}

		if(isAllStencil)
		{
			//CA("isAllStencil");
			
			result = this->getStencilInfo(MediatorClass ::allStencilUIDList,objCSprayStencilInfo);
			if(result == kFalse)
				break;
			
		}

		//PMString vecSize("CSprayStencilInfoVector.Size = ");
		//vecSize.AppendNumber(CSprayStencilInfoVector.size());
		//CA(vecSize);
		
		if(isSectionStencil)
		{
			//CA("isSectionStencil");
			SectionStencilBoxBoundVector.clear();
			SectionStencilMaxBounds = kZeroRect;

			//CSprayStencilInfo objCSprayStencilInfo;
			//result = this->getStencilInfo(MediatorClass ::sectionStencilUIDList ,objCSprayStencilInfo);
			//if(result == kFalse)
			//	break;

			//CSprayStencilInfoVector.push_back(objCSprayStencilInfo);

			result = this->getMaxLimitsOfBoxes(MediatorClass ::sectionStencilUIDList, SectionStencilMaxBounds, SectionStencilBoxBoundVector);
			if(result == kFalse){
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getMaxLimitsOfBoxes returns kFalse");			
				break;
			}

			this->getMaxHorizSprayCount(marginBoxBounds, SectionStencilMaxBounds, horizSprayCountProd);
			this->getMaxVertSprayCount(marginBoxBounds, SectionStencilMaxBounds, vertSprayCountProd);
			
			if(horizSprayCountProd<=0 || vertSprayCountProd<=0)    // if(horizSprayCount<=0 || vertSprayCount<=0)
			{
				CA("The total selected box(es) size does not fit within the margins set for Product Template.  Please increase the margins.");
				break;
			}
		}

		if(!(isProdStencil && isItemStencil && isHybridTableStencil ))
		{
//CA("7");
			this->getMaxHorizSprayCount(marginBoxBounds, origMaxBoxBounds, horizSprayCount);
			PMString temp;
			temp.AppendNumber(horizSprayCount);
			//CA(temp);
			this->getMaxVertSprayCount(marginBoxBounds, origMaxBoxBounds, vertSprayCount);
			temp = "";
			temp.AppendNumber(vertSprayCount);
			//CA(temp);

			//////////////////////////////////////////////////////////////

			if(horizSprayCount<=0 || vertSprayCount<=0)    // if(horizSprayCount<=0 || vertSprayCount<=0)
			{
				CA("The total selected box(es) size does not fit within the margins set.  Please increase the margins.");
				break;
			}
		}
		
		CSprayStencilInfoVector.push_back(objCSprayStencilInfo);

		PMReal MaxVertSprayCount, MaxHorizSprayCount;

		MaxVertSprayCount = vertSprayCount;
		if(vertSprayCountProd > MaxVertSprayCount)
			MaxVertSprayCount = vertSprayCountProd;
		if(vertSprayCountItem > MaxVertSprayCount)
			MaxVertSprayCount = vertSprayCountItem;
		
		MaxHorizSprayCount = horizSprayCount;
		if(horizSprayCountProd > MaxHorizSprayCount)
			MaxHorizSprayCount = horizSprayCountProd;
		if(horizSprayCountItem > MaxHorizSprayCount)
			MaxHorizSprayCount = horizSprayCountItem;
		
		PBPMPoint maxPageSprayCount(MaxHorizSprayCount, MaxVertSprayCount);

		//CMM Comment bool16 IsSprayWholeSectionFlag = iSSSprayer->getSprayAllSectionsFlag();
		bool16 IsSprayWholeSectionFlag = kFalse;
		vector<PubData> SectionIDList;
		//was prevously wholeSection.

///////////////////////////////////////////////////////////////////////  A 2/4/08		//Following code is for return to current page(last sprayed page)
		InterfacePtr<IDocument>doc(MediatorClass ::currentProcessingDocUIDRef,UseDefaultIID());
		if(!doc)
		{
			//CA("Doc is null");
			break;
		}
		InterfacePtr<ISpreadList>spreadList(MediatorClass ::currentProcessingDocUIDRef,UseDefaultIID());
		if(!spreadList)
		{
			//CA("Spread List is null");
			break;
		}
		int32 spreadCount=spreadList->GetSpreadCount();
		
		IDataBase * db = MediatorClass ::currentProcessingDocUIDRef.GetDataBase();
		if(db == nil)
		{
			//CA("db == nil");
			break;
		}

		InterfacePtr<ISpread>spread(db,spreadList->GetNthSpreadUID(spreadCount-1),IID_ISPREAD);
		if(!spread)
		{
			//CA("Spread is null");
			break;
		}	

		int32 noOfPagesInSpread = spread->GetNumPages();
		UIDList itemsTemp(db);
		spread->GetItemsOnPage(noOfPagesInSpread-1,&itemsTemp,kFalse,kFalse); 

		/*InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(Utils<ISelectionUtils>()->QuerySuite(IID_ILAYOUTSELECTION_ISUITE ),UseDefaultIID());
		if (layoutSelectionSuite == nil) 
		{
			//CA("layoutSelectionSuite == nil");
			break; 
		}
		*/	//Amit

		InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
		if (selectionManager == nil)
		{
			//CA("selectionManager == nil");
			break;
		}

		// Deselect everything.
		selectionManager->DeselectAll(nil); // deselect every active CSB
		// Make a layout selection.
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			break;
		}

		layoutSelectionSuite->SelectPageItems(itemsTemp,Selection::kReplace,Selection::kAlwaysCenterInView); 
		
//////////////////////////////////////////////////////////////////////////	A 2/4/08 End	

//		if(MediatorClass ::createMediaRadioOption == 5) //2 Spraying all Sections for Level 2 OR all SubSections for Level 3
//		{//start if createMediaRadioOption

			
			// This part is not required for CreateMedia			
			SectionIDList.clear();		

			for(int32 secVecIndex=0;secVecIndex < MediatorClass ::vec_SubSecData.size();secVecIndex++)
			{
				//for sectionVectorIterator
				//CurrentSelectedSection = SectionIDList[p].getPubId();
	
				subSectionData & ssdt = MediatorClass ::vec_SubSecData[secVecIndex];
			
				//CurrentSelectedSection = ssdt.sectionID;

				//fillpNodeDataList(ssdt.subSectionID,
				//ssdt.sectionID,
				//MediatorClass :: currentSelectedPublicationID,
				//ssdt.subSectionName);


				//CurrentSectionpNodeDataList.clear();	
				//CurrentSectionpNodeDataList = pNodeDataList;


				int32 numProducts=0;
				vector<double> tempIdList;
				tempIdList.clear();
				sprayedProductIndex=0;
				//PFTreeDataCache treeCache;
				result = this->getAllIdForLevel(numProducts, tempIdList);
				if(result == kFalse)
				{
					ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::No data found in tree cache.");
					break;
				}
		
				if(tempIdList.size()<=0)
				{
					ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::There are no products to spray");
					continue;
				}

				PMString title("");
				title += ssdt.subSectionName;
				RangeProgressBar progressBar(title, 0, numProducts, kTrue);
				progressBar.SetTaskText("Spraying Products");
//CA("before	1 sprayFromSecondProductOfSubSection");
				sprayFromSecondProductOfSubSection(originalSelUIDList, origMaxBoxBounds, maxPageSprayCount, numProducts, progressBar,toggleFlag);
//CA("after 	1 sprayFromSecondProductOfSubSection");				
				progressBar.SetPosition(numProducts);		
				
				if(progressBar.WasCancelled(kFalse)){
					isCancelHit = kTrue;
				}
			}//end for sectionVectorIterator
			//End This part is not required for CreateMedia  
//		}
	}
	while(kFalse);
}

void SubSectionSprayer::startSprayingSubSectionForSpecSheet()
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return;
	}
	do
	{

		UIDRef originalPageUIDRef, originalSpreadUIDRef;
		bool16 result = kFalse;
		pageUidList.clear();
        PageCount=0;

		CurrentSectionpNodeDataList.clear();
		CurrentSectionpNodeDataList = pNodeDataList;
	
		//get current page UIDRef
		result = this->getCurrentPage(originalPageUIDRef, originalSpreadUIDRef);
		if(result == kFalse){ 
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getCurrentPage returns kFlase");
			break;
		}

		UID originalPageUID = originalPageUIDRef.GetUID();
		//pageUidList.push_back(originalPageUID);
        //PageCount++;
		
		//get the list of selected box UIDRef's
		UIDList originalSelUIDList;
		
        /*-
		result = this->getSelectedBoxIds(originalSelUIDList);
		if(result == kFalse){ 
			ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::getSelectedBoxIds return kFalse");
			break;
		}
		
		if(originalSelUIDList.Length()==0){ 
			ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::originalSelUIDList.Length()==0");
			break;
		}
        -*/
        originalSelUIDList = MediatorClass::allStencilUIDList;
		selectUIDList = originalSelUIDList;
		//PMString ASD("originalSelUIDList.Length() : ");
		//ASD.AppendNumber(originalSelUIDList.Length());
		//CA(ASD);

		for(int32 j=0; j<originalSelUIDList.Length();j++)
		{
			TagStruct tagInfo;

			InterfacePtr<ITagReader> itagReader
				((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReader){
				return ;
			}
			
			TagList tList=itagReader->getTagsFromBox(originalSelUIDList.GetRef(j));
			if(tList.size()==0)
				tList = itagReader->getFrameTags(originalSelUIDList.GetRef(j));
			if(tList.size()==0)
			{
				continue;
			}

			/*tagInfo=tList[0];
			if(tagInfo.isProcessed || tagInfo.parentId!=-1 || tagInfo.numValidFields < NUM_TAGS_FIELDS)
			{
				originalSelUIDList.Remove(j);
				j--;
			}*/

			//******Added

			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			//******		
		}
		if(originalSelUIDList.Length()==0){ 
			//CA("originalSelUIDList.Length()==0");
			ptrIAppFramework->LogInfo("AP46CreateMedia::SubSectionSprayer::startSprayingSubSection::originalSelUIDList.Length()==0");
			break;
		}
	
	//	//show progress bar
		PMString title("");
		if(global_project_level == 3)
		{
			title.Append("Spraying SubSection: ");
	
		}
		else if(global_project_level ==2)
		{
			title.Append("Spraying Section: ");
		}
		title.SetTranslatable(kFalse);
		//PMString title("Spraying Section: ");
		//title += this->selectedSubSection;
		//RangeProgressBar progressBar(title, 0, numProducts, kTrue);
		//progressBar.SetTaskText("Spraying Products");

	//	for(int16 i=0; i<numProducts;i++)
	//	{
		
			//this->getAllBoxIds(copiedBoxUIDList);
				
			/*PMString tempString("Spraying ");
			tempString +=CurrentSectionpNodeDataList[i].getName();
			tempString += "...";
			progressBar.SetTaskText(tempString);*/

			InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
			if(!DataSprayerPtr)
			{
				ptrIAppFramework->LogError("AP46CreateMedia::SubSectionSprayer::sprayPageWithResizableFrameNew::Pointre to DataSprayerPtr not found");
				return;
			}
			
			PublicationNode pNode =CurrentSectionpNodeDataList[sprayedProductIndex].convertToPublicationNode();
			if(MediatorClass ::radioCreateMediaBySectionWidgetSelected == kTrue)
			{
				DataSprayerPtr->FillPnodeStruct
					(pNode,CurrentSelectedSection,CurrentSelectedPublicationID,CurrentSelectedSubSection);	

			}
			else
			{
				DataSprayerPtr->FillPnodeStruct
					(	pNode,
						CurrentSectionpNodeDataList[sprayedProductIndex].sectionID,
						CurrentSectionpNodeDataList[sprayedProductIndex].publicationID,
						CurrentSectionpNodeDataList[sprayedProductIndex].subSectionID						
					);
			}
			DataSprayerPtr->ClearNewImageFrameList();

			//if(MediatorClass ::subSecSpraySttngs.HorizontalFlowForAllImageSprayFlag)
				DataSprayerPtr->setFlow(kTrue);
			//else
				//DataSprayerPtr->setFlow(kFalse);

	
			//int32 index = 0;
			//if(CurrentSectionpNodeDataList[i].getIsProduct() == 0)
			//{
			//	//CA("index = 0");
			//	//CSprayStencilInfoVector[0];
			//	index = 0;
			//}
			//else if(CurrentSectionpNodeDataList[i].getIsProduct() == 1)
			//{
			//	//CA("index = 1");
			//	//CSprayStencilInfoVector[1];
			//	index = 1;
			//}
			//else if(CurrentSectionpNodeDataList[i].getIsProduct() == 2)
			//{
			//	//CA("index = 2");
			//	//CSprayStencilInfoVector[2];
			//	index = 2;
			//}
			
			//call new method here for 1 server call per object
			
			double objectId = -1;
			objectId = CurrentSectionpNodeDataList[sprayedProductIndex].getPBObjectID();
			
			//ptrIAppFramework->clearAllStaticObjects();
			/*if(CSprayStencilInfoVector.size() >0)
			{	
				CA("before calling getObjectInfo");
				ptrIAppFramework->getObjectInfo(objectId,
				CurrentSectionpNodeDataList[sprayedProductIndex].getTypeId(),
				CurrentSelectedSection,
				kFalse,
				CSprayStencilInfoVector[index].isCopy,
				CSprayStencilInfoVector[index].isAsset,
				CSprayStencilInfoVector[index].isDBTable,
				CSprayStencilInfoVector[index].isHyTable,
				CSprayStencilInfoVector[index].AttributeIds,
				CSprayStencilInfoVector[index].AssetIds,
				CSprayStencilInfoVector[index].dBTypeIds,
				CSprayStencilInfoVector[index].HyTypeIds,
				index);
			}*/

			this->startSpraying();

			//testing
			originalSelUIDList.Release();

	//	}
	}while(kFalse);
}
bool16 doesExist(TagList &tagList,const UIDList &selectUIDList)
{
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		return kFalse;
	}
	//TagList tList; //og
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		TagList tList = itagReader->getTagsFromBox(selectUIDList.GetRef(i));

		if(tList.size()==0||tagList.size()==0 || !tList[0].tagPtr || !tagList[0].tagPtr )
		{
			//CA("conti");
			//added by avinash
			for(int32 tagIndex = 0 ; tagIndex < tList.size(); tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
			// till here
			continue;
		}

		if(tagList[0].tagPtr == tList[0].tagPtr )
		{
			//CA("return kTrue");
			return kTrue;
		}

		//added by avinash
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		// till here
	}
	//CA("return kFalse");
	return kFalse;
}


bool16 SubSectionSprayer::getAllBoxIdsForGroupFrames(UIDList& tempFrameList)
{
	//TagReader tReader;	
	//selectUIDList = copiedList;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return kFalse;
	}
	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogDebug("AP46_ProductFinder::SubSectionSprayer::getAllBoxIds::!itagReader");	
		return kFalse;
	}

	UIDList tempList(tempFrameList.GetDataBase());
	for(int i=0; i<tempFrameList.Length(); i++)
	{
		InterfacePtr<IHierarchy> iHier(tempFrameList.GetRef(i), UseDefaultIID());
		if(!iHier)
		{
			//CA(" !iHier >> Continue ");
			continue;
		}
		UID kidUID;
		
		int32 numKids=iHier->GetChildCount();
////PMString ASD("numKids : ");
////ASD.AppendNumber(numKids);
////CA(ASD);

		bool16 isGroupFrame = kFalse ;
		isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(tempFrameList.GetRef(i));

		if(isGroupFrame == kTrue) 
		{
			IIDXMLElement* ptr = NULL;
			for(int j=0;j<numKids;j++)
			{
				kidUID=iHier->GetChildUID(j);
				UIDRef boxRef(tempFrameList.GetDataBase(), kidUID);	


				InterfacePtr<IHierarchy> iHierarchy(boxRef, UseDefaultIID());
				if(!iHierarchy)
				{
					//CA(" !iHier >> Continue ");
					continue;
				}
				UID newkidUID;
				
				int32 numNewKids=iHierarchy->GetChildCount();
				/*PMString ASD("numKids ................: ");
				ASD.AppendNumber(numKids);
				CA(ASD);*/

				bool16 isGroupFrameAgain = kFalse ;
				isGroupFrameAgain = Utils<IPageItemTypeUtils>()->IsGroup(boxRef);

				if(isGroupFrameAgain == kTrue) 
				{
					IIDXMLElement* newPtr = NULL;
					for(int k=0;k<numNewKids;k++)
					{
						//CA("Inside For Loop");
						newkidUID=iHierarchy->GetChildUID(k);
						UIDRef childBoxRef(tempFrameList.GetDataBase(), newkidUID);	

					
						//CA("isGroupFrame == kTrue");
						TagList NewList = itagReader->getTagsFromBox(childBoxRef, &newPtr);
						
						/*PMString s("NewList.size() : ");
						s.AppendNumber(NewList.size());
						CA(s);*/
					
					

						if(!doesExist(NewList,tempList))
						{
							tempList.Append(newkidUID);				
						}
						//------------
						for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
						{
							NewList[tagIndex].tagPtr->Release();
						}
					}
				}
				else
				{
					TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
					
					/*PMString s("NewList.size() : ");
					s.AppendNumber(NewList.size());
					CA(s);*/
				
				

					if(!doesExist(NewList,tempList))
					{
						tempList.Append(kidUID);				
					}
					for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
					{
						NewList[tagIndex].tagPtr->Release();
					}
				}			
			}
		}
		else
		{
			//CA("isGroupFrame == kFalse");
			tempList.Append(tempFrameList.GetRef(i).GetUID());
		}
	}
	tempFrameList = tempList;
	
	
	return kTrue;

}

int32 SubSectionSprayer::checkIsSprayItemPerFrameTag(const UIDList &selectUIDList , bool16 &isItemHorizontalFlow)
{
    //CA("ProductSpray::doesExist");
    int32 result = 0;
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == NULL)
    {
        CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
        return result;
    }
    InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
    if(!itagReader)
    {
        ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::doesExist::!itagReader");
        return result;
    }
    TagList tList;
    for(int i=0; i<selectUIDList.Length(); i++)
    {
        tList = itagReader->getTagsFromBox(selectUIDList.GetRef(i));
        
        if(tList.size()==0 )
        {
            //CA("continue");
            continue;
        }
        
        for(int32 j=0; j <tList.size(); j++)
        {
            if(tList[j].whichTab == 4)
            {
                if(tList[j].isSprayItemPerFrame == 1)
                {
                    isItemHorizontalFlow = kTrue;
                    result = 1;
                    break;
                }
                else if(tList[j].isSprayItemPerFrame == 2)
                {
                    isItemHorizontalFlow = kFalse;
                    result = 1;
                    break;
                }
                
            }
            if(tList[j].isSprayItemPerFrame == 3) // for PV image spray in way of SprayItemPerFrame functionality.
            {
                if(tList[j].flowDir == 0)
                {
                    isItemHorizontalFlow = kTrue;
                }
                else
                {
                    isItemHorizontalFlow = kFalse;
                }
                
                result = 2;
                break;
            }
        }
        //added for clearing the taglist testing
        for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
        {
            tList[tagIndex].tagPtr->Release();
        }
        
        if(result)
            break;
        
    }
    //CA("return kFalse");
    return result;
}

bool16 SubSectionSprayer::checkIsHorizontalFlowFlagPresent(const UIDList &selectUIDList)
{
    //CA("ProductSpray::doesExist");
    bool16 isItemHorizontalFlowFlag = kFalse;
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == NULL)
    {
        CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
        return kFalse;
    }
    InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
    if(!itagReader)
    {
        ptrIAppFramework->LogDebug("AP7_DataSprayer::CDataSprayer::doesExist::!itagReader");
        return kFalse;
    }
    TagList tList;
    for(int i=0; i<selectUIDList.Length(); i++)
    {
        tList = itagReader->getTagsFromBox(selectUIDList.GetRef(i));
        
        if(tList.size()==0 )
        {
            //CA("continue");
            continue;
        }
        
        for(int32 j=0; j <tList.size(); j++)
        {
            if(tList[j].whichTab == 4 || tList[j].isSprayItemPerFrame == 3)
            {
                if(tList[j].isSprayItemPerFrame == 1 )
                {
                    //CA("if(tList[j].isSprayItemPerFrame == 1)");
                    isItemHorizontalFlowFlag = kTrue;
                    //return kTrue;
                }
                else if(tList[j].isSprayItemPerFrame == 2 )
                {
                    //CA("if(tList[j].isSprayItemPerFrame == 2}");
                    isItemHorizontalFlowFlag = kFalse;
                    //return kTrue;
                }
                else if(tList[j].flowDir == 0)
                {
                    //CA("else if(tList[j].flowDir == 0)");
                    isItemHorizontalFlowFlag = kTrue;
                    //return kTrue;
                }
                else if(tList[j].flowDir == 1)
                {
                    //CA("else if(tList[j].flowDir == 1)");
                    isItemHorizontalFlowFlag = kFalse;
                    //return kTrue;
                }
            }
        }
        //added for clearing the taglist testing
        for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
        {
            tList[tagIndex].tagPtr->Release();
        }
        
    }
    
    return isItemHorizontalFlowFlag;
}


bool16 SubSectionSprayer::addNewPageHere(PMRect& PagemarginBoxBounds)
{
    //CA("New Page will be added now..");
    
    bool16 result = kFalse;
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA(" ptrIAppFramework nil ");
        return result;
    }
    Utils<ILayoutUIUtils>()->AddNewPage();
    PageCount= PageCount+1;
    
    //CA_NUM("Page is now added and page count is : ", PageCount);
    
    newPageAdded = kTrue;
    
    IDocument* fntDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
    if(fntDoc==nil)
    {
        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::fntDoc==nil");
        return result;
    }
    IDataBase* database = ::GetDataBase(fntDoc);
    if(database==nil)
    {
        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::database==nil");
        return result;
    }
    InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
    if (iSpreadList==nil)
    {
        ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::iSpreadList==nil");
        return result;
    }
    
    UID pageUID;
    UIDRef pageRef = UIDRef::gNull;
    UIDRef spreadUIDRef = UIDRef::gNull;
    
    for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
    {
        if( (iSpreadList->GetSpreadCount()-1) > numSp )
        {
            continue;
        }
        UIDRef temp_spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
        spreadUIDRef = temp_spreadUIDRef;
        
        InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
        if(!spread)
        {
            ptrIAppFramework->LogDebug("AP7_ProductFinder::SubSectionSprayer::startSpraying::!spread");
            return result;
        }
        int numPages=spread->GetNumPages();
        pageUID = spread->GetNthPageUID(numPages-1);
        UIDRef temp_pageRef(database, pageUID);
        pageRef = temp_pageRef;
        
    }
    
    pageUidList.push_back(pageUID);
    ProdBlockBoundList.clear();
    
    result = this->getMarginBounds(pageRef, PagemarginBoxBounds);
    if(result == kFalse)
    {
        result = this->getPageBounds(pageRef, PagemarginBoxBounds);
        if(result == kFalse)
            return result;
        
    }	
    
    return result;
}



ErrorCode SubSectionSprayer::CreatePages(const IDocument* iDocument, const UIDRef spreadToAddTo,const int16 numPagesToInsert, const int16 pageToInsertAt, const bool16 allowShuffle)
{
    //CA("Create page Call");
    ErrorCode status = kFailure;
    do {
        // different page sizes within a document leads to undefined behaviour
        // go to the document preferences to get the current page size
        InterfacePtr<IPageSetupPrefs> iPageSetupPrefs(static_cast<IPageSetupPrefs *>(::QueryPreferences(IID_IPAGEPREFERENCES, iDocument)));
        if (iPageSetupPrefs == nil){
            //CA("iPageSetupPrefs == nil");
            break;
        }
        
        InterfacePtr<ICommand> iNewPageCmd(CmdUtils::CreateCommand(kNewPageCmdBoss));
        if (iNewPageCmd == nil){
            //CA("iNewPageCmd == nil");
            break;
        }
        InterfacePtr<IPageCmdData> iPageCmdData(iNewPageCmd,UseDefaultIID());
        if (iPageCmdData == nil){
            //CA("iNewPageCmd == nil");
            break;
        }
        
        ISpread::BindingSide bindingSide = ISpread::kDefaultBindingSide;
        
        iPageCmdData->SetNewPageCmdData (spreadToAddTo,numPagesToInsert,pageToInsertAt);
        
        
        InterfacePtr<IBoolData> iBoolData(iNewPageCmd,UseDefaultIID());
        if (iBoolData == nil){
            //CA("iBoolData == nil");
            break;
        }
        iBoolData->Set(allowShuffle);
        
        status = CmdUtils::ProcessCommand(iNewPageCmd);
        
    } while(false);
    return status;	
}

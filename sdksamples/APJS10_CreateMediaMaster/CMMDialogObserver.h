
#include "VCPlugInHeaders.h"


// Interface includes:

#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "ITextControlData.h"
#include "IDialogController.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"

#include "IDocumentCommands.h"
//book
#include "BookID.h"
#include "IBook.h"
#include "IBookUtils.h"
#include "IBookManager.h"
#include "IBookContentMgr.h"
#include "IBookContentCmdData.h"
#include "IBookPaginateOptions.h"
#include "IIntData.h"

//opening template document
#include "IDocument.h"
#include "ISpreadList.h"
#include "ISpread.h"
#include "ISelectionManager.h"
#include "ILayoutSelectionSuite.h"
#include "ISelectionUtils.h"
#include "IDataBase.h"
#include "IDocumentUtils.h"
#include "IDocumentList.h"
#include "IApplication.h"

//rahul's test code includes
#include "IDocFileType.h"
#include "ISysFileData.h"
#include "IUIFlagData.h"

#include "IPMLockFileData.h"
#include "IPMLockFile.h"
#include "AcquireModalCursor.h"
//end rahul's test code includes

//list box modification on 3Jun
#include "IListControlData.h"
//end 3Jun


//2Nov..for AddNewPage();
//#include "LayoutUIUtils.h" //CS3 Depricated
#include "ILayoutUIUtils.h" //Cs4
//end 2Nov..

// General includes:
// Project includes:


//common sdk files
#include "SDKFileHelper.h"
//#include "SDKListBoxHelper.h"
#include "SDKFileHelper.h"
#include "SDKLayoutHelper.h"
#include "SDKUtilities.h"

//PrintSource4.5 interfaces
#include "IAppFramework.h"
#include "IClientOptions.h"
#include "ISpecialChar.h"

//local class files
#include "MediatorClass.h"
#include "CDialogObserver.h"
#include "MediatorClass.h"
#include "SectionData.h"
#include "SubSectionSprayer.h"	
#include "CMMActionComponent.h"

////
#include "INewLayerCmdData.h"
#include "ILayerList.h"
#include "IDocumentLayer.h"
#include "SnpGraphicHelper.h"
#include "InstStrokeFillID.h"
#include "ISpreadLayer.h"
#include "ISpread.h"
//#include "LayoutUIUtils.h" //Cs3 Depricated
#include "ILayoutUIUtils.h" //Cs4
#include "IPageList.h"
#include "ICopyCmdData.h"
#include "IClipboardController.h"
#include "ITransformFacade.h"
#include "IPathUtils.h"
#include "Utils.h"
#include "IResizeItemsCmdData.h"
#include "IBoundsData.h"
//#include "PageItemUtils.h" //Cs3 Depricated
#include "IPageItemUtils.h" //Cs4
#include "IActiveContext.h"
#include "ITableUtility.h"
#include "ITableModelList.h"
#include "ITableUtils.h"
////
#include "IUIFontSpec.h"


//PrintSource4.5 id files
#include "OptnsDlgID.h"
#include "AFWJSID.h"
#include "CMMID.h"
//#include "SCTID.h"

//copy templates to master..script..function
#include "ICopyCmdData.h"
#include "IPageItemScrapData.h"
#include "IScrapSuite.h"
#include "IClipBoardController.h"
////17-april
#include "FileUtils.h"
//17-april
//-#include "CreateInddPreview.h"
#include "PlatformChar.h"
#include "IMasterSpreadList.h"
#include "IMasterSpread.h"
#include "IApplyMasterCmdData.h"
#include "IScrapItem.h"


#include "CMMID.h"
#include "CatPlanningData.h"  //7-may
#include "CAlert.h"


#include "IMovePageData.h"
#include "IPageLayoutPrefs.h"
#include "StreamUtil.h"
#include "IDOMElement.h"
#include "ISnippetImport.h"
//-  #include "IRefreshContent.h"

#include "StencilFileInfo.h"

class CMMDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CMMDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~CMMDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);
		void populatePublicationDropDownList(double SelectedLocaleID);
		void populateSubSectionList(bool16 shall_I_Fill_Data_Into_ListBox,double rootId);

		//1Nov...
		void populateSubSectionListForSpread(bool16 shall_I_Fill_Data_Into_ListBox,double rootId);
		//end 1Nov..
//17-april
		void populateSubSectionListForWhiteboard(bool16 shall_I_Fill_Data_Into_ListBox,double rootId);
//17-april

		//26-Dec
		void populateSubSectionListForPriceBook(bool16 shall_I_Fill_Data_Into_ListBox,double rootId);

		void populateSubSectionListForSpecSheet(bool16 shall_I_Fill_Data_Into_ListBox,double rootId);
		
		void refreshWhiteBoardMedia();
		void refreshWhiteBoardMediaForPageBased();	

		void populateSubSectionListForSectionLevel(bool16 shall_I_Fill_Data_Into_ListBox,double sectionId);
		void populateSubSectionListForSpreadForSectionLevel(bool16 shall_I_Fill_Data_Into_ListBox,double sectionId);
};
//========================================================================================
//  
//  $File: $
//  
//  Owner: Yogesh Joshi
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

//SDK interfaces
#include "IDialog.h"

#include "CDialogController.h"



//PrintSource4.5 ID files
#include "CMMID.h"

//local class files
#include "MediatorClass.h"


//



#include "CAlert.h"
void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line);
#define FILENAME			PMString("CMMSpraySettingsDialogController.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
//extern IDialog* dialog1;

/** CMMDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.

	
	@ingroup createmediamaster
*/
class CMMSpraySettingsDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CMMSpraySettingsDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~CMMSpraySettingsDialogController() 
		{
		
		//CA("destructor");
		}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
		//void updateListBox(bool16 isAttaching);
		//virtual void DialogClosing();


		
};

CREATE_PMINTERFACE(CMMSpraySettingsDialogController, kCMMSpraySettingsDialogControllerImpl)

/* ApplyFields
*/
void CMMSpraySettingsDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	
	//CA("dialog controller 1");
	
	do
	{
		
		CDialogController::InitializeDialogFields(dlgContext);
		InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
		if(dlgController==nil)
		{
			CA("dlgController == nil");
			break;
		}

		dlgController->SetTextValue(kHorizontalSpacingWidgetID, 0.0);
		dlgController->SetTextValue(kVerticalSpacingWidgetID,  0.0);
				
		dlgController->SetTriStateControlData(kLeftToRightWidgetID, kTrue, nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kRightToLeftWidgetID, kFalse, nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kAlternateValueWidgetID, kFalse, nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kHorizontalFlowWidgetID, kTrue, nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kVerticalFlowWidgetID, kFalse, nil, kTrue, kFalse);
		
	
	
	
	}while(kFalse);
	//CA("dialog controller 2");

	//listHelper.AddElement("1",kTemplateFileNameTextWidgetID,0);
	// Put code to initialize widget values here.
}

/* ValidateFields
*/
WidgetID CMMSpraySettingsDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	return result;
}

/* ApplyFields
*/
void CMMSpraySettingsDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{

	
		// TODO add code that gathers widget values and applies them.
	do
	{
		
		InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
		if(dialogController == nil)
		{
			CA("dialogController == nil");
			break;
		}
		PMString verticalSpacing = dialogController->GetTextControlData(kVerticalSpacingWidgetID);
		PMReal verticalSpacingReal(0.0);
		if(verticalSpacing != "")
		{
			verticalSpacingReal = dialogController->GetTextValue(kVerticalSpacingWidgetID);				
		}

		PMString horizontalSpacing = dialogController->GetTextControlData(kHorizontalSpacingWidgetID);
		PMReal horizontalSpacingReal(0.0);
		if(horizontalSpacing != "")
		{
			horizontalSpacingReal = dialogController->GetTextValue(kHorizontalSpacingWidgetID);				
		}

		
		ITriStateControlData::TriState flowLeftToRightState = dialogController->GetTriStateControlData(kLeftToRightWidgetID);
		bool16 isFlowLeftToRightSelected = kTrue;
		if(flowLeftToRightState == ITriStateControlData::kSelected)
			isFlowLeftToRightSelected = kTrue;
		else
			isFlowLeftToRightSelected = kFalse;
		
		ITriStateControlData::TriState theAlternateState= dialogController->GetTriStateControlData(kAlternateValueWidgetID);
		bool16 isTheAlternateCheckBoxSelected = kTrue;
		if(theAlternateState == ITriStateControlData::kSelected)
			isTheAlternateCheckBoxSelected = kTrue;
		else
			isTheAlternateCheckBoxSelected = kFalse;

		ITriStateControlData::TriState theHorizFlowState = dialogController->GetTriStateControlData(kHorizontalFlowWidgetID);
		bool16 istheHorizFlowStateSelected = kTrue;
		if(theHorizFlowState == ITriStateControlData::kSelected)
			istheHorizFlowStateSelected = kTrue;
		else
			istheHorizFlowStateSelected = kFalse;


		if(MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected)
		{
			//CA("fill data into list box");
			MediatorClass :: vector_subSectionSprayerListBoxParameters[MediatorClass :: selectedIndexOfListBoxElement].ssss.setAll
				(
					verticalSpacingReal,
					horizontalSpacingReal,
					isFlowLeftToRightSelected,
					isTheAlternateCheckBoxSelected,
					istheHorizFlowStateSelected
				);
		
		}
		else
		{
			//if(MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected)
			//{
				MediatorClass ::subSecSpraySttngs.setAll
				(
					verticalSpacingReal,
					horizontalSpacingReal,
					isFlowLeftToRightSelected,
					isTheAlternateCheckBoxSelected,
					istheHorizFlowStateSelected
				);
				
			//}

			//
			//CA("don't fill data into list box");
			//vector<subSectionSprayerListBoxParameters> & subSecSpraLbParams =  MediatorClass :: vector_subSectionSprayerListBoxParameters;
			//
			//PMString data;

			//PMString size;
			//size.AppendNumber(subSecSpraLbParams.size());
			//CA(size);


			//for(int32 index = 0;index < subSecSpraLbParams.size();index++)
			//{
			//	
			//	
			//	data.AppendNumber(index);
			//	data.Append("\n");
			//	data.Append("verticalSpacingReal : ");
			//	data.AppendNumber(verticalSpacingReal);
			//	data.Append("\n");

			//	data.Append("horizontalSpacingReal : ");
			//	data.AppendNumber(horizontalSpacingReal);
			//	data.Append("\n");

			//	data.Append("isFlowLeftToRightSelected : ");
			//	data.AppendNumber(isFlowLeftToRightSelected);
			//	data.Append("\n");

			//	data.Append("isTheAlternateCheckBoxSelected : ");
			//	data.AppendNumber(isTheAlternateCheckBoxSelected);
			//	data.Append("\n");
			//	
			//	data.Append("istheHorizFlowStateSelected : ");
			//	data.AppendNumber(istheHorizFlowStateSelected);
			//	data.Append("=========================");
			//	data.Append("\n");
			//	//data.Append("\n");

			//	


			//	subSecSpraLbParams[index].ssss.setAll
			//		(
			//			verticalSpacingReal,
			//			horizontalSpacingReal,
			//			isFlowLeftToRightSelected,
			//			isTheAlternateCheckBoxSelected,
			//			istheHorizFlowStateSelected
			//		);
			//}
			//CA(data);
			//
		}
		

	}while(kFalse);
	

	
}


//void CMMSpraySettingsDialogController::DialogClosing()
//{
	//CA("dialog clsoing 1");
	//dialog1->SetDeleteOnClose(kTrue);
	//CA("dialog closing 2");
		
//}




//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/basicdialog/SpraySettingsDialogController.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:

#include "SystemUtils.h"
#include "SpraySettingDialogController.h"

#include "MediatorClass.h"

// Project includes:
#include "CMMID.h"
#include "ITextControlData.h"
#include "IAppFramework.h"

#include "CAlert.h"




void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line);
#define FILENAME			PMString("BscDlgController.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
//extern IDialog* dialog1;

/** Implements IDialogController based on the partial implementation CDialogController; 
	its methods allow for the initialization, validation, and application of dialog widget values.
  
	The methods take an additional parameter for 3.0, of type IActiveContext.
	See the design document for an explanation of the rationale CMMCustomIconWidgetEH this
	new parameter and the renaming of the methods that CDialogController supports.
	
	
	@ingroup basicdialog	
*/
//class SpraySettingsDialogController : public CDialogController
//{
//	public:
//		/**
//			Constructor.
//			@param boss interface ptr from boss object on which this interface is aggregated.
//		*/
//		SpraySettingsDialogController(IPMUnknown* boss) : CDialogController(boss) {}
//
//		/**
//			Initializes each widget in the dialog with its default value.
//			Called when the dialog is opened.
//			@param Context
//		*/
//		virtual void InitializeDialogFields( IActiveContext* dlgContext);
//
//		/**
//			Validate the values in the widgets. 
//			By default, the widget with ID kOKButtonWidgetID causes 
//			this method to be called. When all widgets are valid, 
//			ApplyFields will be called.		
//			@param myContext
//			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.
//		*/
//		virtual WidgetID ValidateDialogFields( IActiveContext* myContext);
//
//		/**
//			Retrieve the values from the widgets and act on them.
//			@param myContext
//			@param widgetId identifies the widget on which to act.
//		*/
//		virtual void ApplyDialogFields( IActiveContext* myContext, const WidgetID& widgetId);
//};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(SpraySettingsDialogController, kSpraySettingsDialogControllerImpl)

/* ApplyFields
*/
void SpraySettingsDialogController::InitializeDialogFields( IActiveContext* dlgContext) 
{
	// Put code to initialize widget values here.
	//CA("dialog controller 1");
	
	do
	{
		
		CDialogController::InitializeDialogFields(dlgContext);
		InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
		if(dlgController==nil)
		{
			CA("dlgController == nil");
			break;
		}

		subSectionSprayerSettings & ssss = MediatorClass :: vector_subSectionSprayerListBoxParameters[MediatorClass :: selectedIndexOfListBoxElement].ssss;

		dlgController->SetTextValue(kHorizontalSpacingWidgetID, ssss.horizontalBoxSpacing);
		dlgController->SetTextValue(kVerticalSpacingWidgetID,  ssss.verticalBoxSpacing);
				
		/*dlgController->SetTriStateControlData(kLeftToRightWidgetID, ssss.flowLeftToRight, nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kRightToLeftWidgetID, !(ssss.flowLeftToRight), nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kAlternateValueWidgetID, ssss.alternateForEachPage, nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kCMMHorizontalFlowWidgetID, ssss.flowHorizontalFlow, nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kVerticalFlowWidgetID, !(ssss.flowHorizontalFlow), nil, kTrue, kFalse);*/
		
		dlgController->SetTriStateControlData(kCMMAltVerticalFLowRadioButtonWidgetID, ssss.AltVerticalFlowFlag, nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kCMMVerticalFlowRadioButtonWidgetID, ssss.VerticalFlowFlag, nil, kTrue, kFalse);		
		dlgController->SetTriStateControlData(kCMMAltHorzintalFlowRadioButtonWidgetID, ssss.AltHorizontalFlowFlag, nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kCMMHorzintalFlowRadioButtonWidgetID, ssss.HorizontalFlowFlag, nil, kTrue, kFalse);

		dlgController->SetTriStateControlData(kIsSprayItemPerFrameWidgetID, ssss.SprayItemPerFrameFlag , nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kIsHorizontalFlowForAllImageSprayWidgetID, ssss.HorizontalFlowForAllImageSprayFlag , nil, kTrue, kFalse);
		
		dlgController->SetTriStateControlData(kspraySectionWithOutPageBreakWidgetID, ssss.WithoutPageBreakFlag , nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kAddSectionStencilWidgetID, ssss.AddSectionStencilFlag , nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kAtTheStartOfSectionWidgetID, ssss.AtTheStartOfSectionFlag , nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kAtTheStartOfEachPageWidgetID, ssss.AtTheStartOfEachPageFlag , nil, kTrue, kFalse);
		dlgController->SetTriStateControlData(kAtTheStartOfFirstPageAndSpreadWidgetID, ssss.AtTheStartOfFirstPageOfSpreadFlag , nil, kTrue, kFalse);
//New code added for price book on 27/12/07
		
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if(!panelControlData) 
		{
			break;
		}
		IControlView * atTheStartOfSectionCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfSectionWidgetID);
		if(atTheStartOfSectionCheckBoxControlView == nil)
		{
			CA("atTheStartOfSectionCheckBoxControlView == nil");
			break;
		}
		IControlView * atTheStartOfEachPageCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfEachPageWidgetID);
		if(atTheStartOfEachPageCheckBoxControlView == nil)
		{
			CA("atTheStartOfEachPageCheckBoxControlView == nil");
			break;
		}
		IControlView * atTheStartOfFirstPageOfSpreadCheckBoxControlView = panelControlData->FindWidget(kAtTheStartOfFirstPageAndSpreadWidgetID);
		if(atTheStartOfFirstPageOfSpreadCheckBoxControlView == nil)
		{
			CA("atTheStartOfFirstPageOfSpreadCheckBoxControlView == nil");
			break;
		}
		
		IControlView * atWithoutPageBreakCheckBoxControlView = panelControlData->FindWidget(kspraySectionWithOutPageBreakWidgetID);
		if(atWithoutPageBreakCheckBoxControlView == nil)
		{
			CA("atWithoutPageBreakCheckBoxControlView == nil");
			break;
		}

		if(MediatorClass::createMediaRadioOption == 5 )
			atWithoutPageBreakCheckBoxControlView->Enable();
		else
			atWithoutPageBreakCheckBoxControlView->Disable();

		if(ssss.AddSectionStencilFlag)
		{
			atTheStartOfSectionCheckBoxControlView->Enable();
			atTheStartOfEachPageCheckBoxControlView->Enable();
			atTheStartOfFirstPageOfSpreadCheckBoxControlView->Enable();			
		}
		else
		{
			atTheStartOfSectionCheckBoxControlView->Disable();
			atTheStartOfEachPageCheckBoxControlView->Disable();
			atTheStartOfFirstPageOfSpreadCheckBoxControlView->Disable();
		}

		//------LALIT--------
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CA("ptrIAppFramework == nil");
			return;
		}
		IControlView * sectionSprayProductOrItemTextControlView = panelControlData->FindWidget(kSectionSprayForProductOrItemTextWidgetID);
		if(sectionSprayProductOrItemTextControlView == nil)
		{
			CA("sectionSprayProductOrItemTextControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>sectionSprayProductOrItemControlData(sectionSprayProductOrItemTextControlView,UseDefaultIID());
		if(sectionSprayProductOrItemControlData == nil)
		{
			CA("sectionSprayProductOrItemControlData == nil");
			break;
		}
		PMString insertText("");
		insertText.SetTranslatable(kFalse);
		insertText.Clear();
		insertText=" Spacing between ";
		insertText.Append("Families");
		insertText.Append("/");
		insertText.Append("Items");
		//insertText.Append(" ");
		insertText.Append(kCMMBlankSpaceStringKey);
		insertText.SetTranslatable(kFalse);
		sectionSprayProductOrItemControlData->SetString(insertText);


		IControlView * sprayPerItemFrameTextControlView = panelControlData->FindWidget(kSprayPerItemFrameStaticTextWidgetID);
		if(sprayPerItemFrameTextControlView == nil)
		{
			CA("sprayPerItemFrameTextControlView == nil");
			break;
		}
		InterfacePtr<ITextControlData>sprayPerItemFrameControlData(sprayPerItemFrameTextControlView,UseDefaultIID());
		if(sprayPerItemFrameControlData == nil)
		{
			CA("sprayPerItemFrameControlData == nil");
			break;
		}
		insertText.Clear();
		insertText="Spray ";
		insertText.Append("Item");
		insertText.Append(" Per Frame");
		insertText.SetTranslatable(kFalse);
		sprayPerItemFrameControlData->SetString(insertText);

	
	}while(kFalse);
	//CA("dialog controller 2");

	//listHelper.AddElement("1",kListBoxMasterFileNameTextWidgetID,0);
	// Put code to initialize widget values here.
}


/* ValidateFields
*/
WidgetID SpraySettingsDialogController::ValidateDialogFields( IActiveContext* myContext) 
{
	WidgetID result = kNoInvalidWidgets;


	// Put code to validate widget values here.


	return result;
}

/* ApplyFields
*/
void SpraySettingsDialogController::ApplyDialogFields( IActiveContext* myContext, const WidgetID& widgetId) 
{
	//CA("in apply dialog fields");
	do{
		InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
		if(dialogController == nil)
		{
			CA("dialogController == nil");
			break;
		}
		PMString verticalSpacing = dialogController->GetTextControlData(kVerticalSpacingWidgetID);
		PMReal verticalSpacingReal(0.0);
		if(verticalSpacing != "")
		{
			verticalSpacingReal = dialogController->GetTextValue(kVerticalSpacingWidgetID);				
		}

		PMString horizontalSpacing = dialogController->GetTextControlData(kHorizontalSpacingWidgetID);
		PMReal horizontalSpacingReal(0.0);
		if(horizontalSpacing != "")
		{
			horizontalSpacingReal = dialogController->GetTextValue(kHorizontalSpacingWidgetID);				
		}
		
		ITriStateControlData::TriState AltVerticalFlowState = dialogController->GetTriStateControlData(kCMMAltVerticalFLowRadioButtonWidgetID);
		bool16 isAltVerticalFlowStateSelected = kFalse;
		if(AltVerticalFlowState == ITriStateControlData::kSelected)
			isAltVerticalFlowStateSelected = kTrue;
		else
			isAltVerticalFlowStateSelected = kFalse;

		ITriStateControlData::TriState VerticalFlowState = dialogController->GetTriStateControlData(kCMMVerticalFlowRadioButtonWidgetID);
		bool16 isVerticalFlowStateSelected = kFalse;
		if(VerticalFlowState == ITriStateControlData::kSelected)
			isVerticalFlowStateSelected = kTrue;
		else
			isVerticalFlowStateSelected = kFalse;
		
		ITriStateControlData::TriState AltHorizontalFlowState = dialogController->GetTriStateControlData(kCMMAltHorzintalFlowRadioButtonWidgetID);
		bool16 isAltHorizontalFlowStateSelected = kFalse;
		
		if(AltHorizontalFlowState == ITriStateControlData::kSelected)
			isAltHorizontalFlowStateSelected = kTrue;
		else
			isAltHorizontalFlowStateSelected = kFalse;

		ITriStateControlData::TriState HorizontalFlowState = dialogController->GetTriStateControlData(kCMMHorzintalFlowRadioButtonWidgetID);
		bool16 isHorizontalFlowStateSelected = kFalse;
		if(HorizontalFlowState == ITriStateControlData::kSelected)
			isHorizontalFlowStateSelected = kTrue;
		else
			isHorizontalFlowStateSelected = kFalse;

		bool16 isFlowLeftToRightSelected = kTrue;
		bool16 isTheAlternateCheckBoxSelected = kTrue;
		bool16 istheHorizFlowStateSelected = kTrue;

		
		/*ITriStateControlData::TriState flowLeftToRightState = dialogController->GetTriStateControlData(kLeftToRightWidgetID);
		bool16 isFlowLeftToRightSelected = kTrue;
		if(flowLeftToRightState == ITriStateControlData::kSelected)
			isFlowLeftToRightSelected = kTrue;
		else
			isFlowLeftToRightSelected = kFalse;
		
		ITriStateControlData::TriState theAlternateState= dialogController->GetTriStateControlData(kAlternateValueWidgetID);
		bool16 isTheAlternateCheckBoxSelected = kTrue;
		if(theAlternateState == ITriStateControlData::kSelected)
			isTheAlternateCheckBoxSelected = kTrue;
		else
			isTheAlternateCheckBoxSelected = kFalse;

		ITriStateControlData::TriState theHorizFlowState = dialogController->GetTriStateControlData(kCMMHorizontalFlowWidgetID);
		bool16 istheHorizFlowStateSelected = kTrue;
		if(theHorizFlowState == ITriStateControlData::kSelected)
			istheHorizFlowStateSelected = kTrue;
		else
			istheHorizFlowStateSelected = kFalse;*/

		if(isAltVerticalFlowStateSelected)
		{
			isFlowLeftToRightSelected = kTrue;
			isTheAlternateCheckBoxSelected = kTrue;
			istheHorizFlowStateSelected = kFalse;
		}
		else if(isVerticalFlowStateSelected)
		{
			isFlowLeftToRightSelected = kTrue;
			isTheAlternateCheckBoxSelected = kFalse;
			istheHorizFlowStateSelected = kFalse;
		}
		else if(isAltHorizontalFlowStateSelected)
		{
			isFlowLeftToRightSelected = kTrue;
			isTheAlternateCheckBoxSelected = kTrue;
			istheHorizFlowStateSelected = kTrue;
		}
		else if(isHorizontalFlowStateSelected)
		{
			isFlowLeftToRightSelected = kTrue;
			isTheAlternateCheckBoxSelected = kFalse;
			istheHorizFlowStateSelected = kTrue;
		}

		ITriStateControlData::TriState isHorizontalFlowForAllImageSpray = dialogController->GetTriStateControlData(kIsHorizontalFlowForAllImageSprayWidgetID);
		ITriStateControlData::TriState isSprayItemPerFrame = dialogController->GetTriStateControlData(kIsSprayItemPerFrameWidgetID);
		
		ITriStateControlData::TriState isWithoutPageBreakFlag = dialogController->GetTriStateControlData(kspraySectionWithOutPageBreakWidgetID);
		ITriStateControlData::TriState isAddSectionStencilFlag = dialogController->GetTriStateControlData(kAddSectionStencilWidgetID);
		ITriStateControlData::TriState isAtTheStartOfSectionFlag = dialogController->GetTriStateControlData(kAtTheStartOfSectionWidgetID);
		ITriStateControlData::TriState isAtTheStartOfEachPageFlag = dialogController->GetTriStateControlData(kAtTheStartOfEachPageWidgetID);
		ITriStateControlData::TriState isAtTheStartOfFirstPageOfSpreadFlag = dialogController->GetTriStateControlData(kAtTheStartOfFirstPageAndSpreadWidgetID);

		MediatorClass :: vector_subSectionSprayerListBoxParameters[MediatorClass :: selectedIndexOfListBoxElement].ssss.setAll
		(
			verticalSpacingReal,
			horizontalSpacingReal,
			isFlowLeftToRightSelected,
			isTheAlternateCheckBoxSelected,
			istheHorizFlowStateSelected,
			
			isAltVerticalFlowStateSelected,
			isVerticalFlowStateSelected,
			isAltHorizontalFlowStateSelected,
			isHorizontalFlowStateSelected,
			isHorizontalFlowForAllImageSpray,
			isSprayItemPerFrame,

			isWithoutPageBreakFlag,
			isAddSectionStencilFlag,
			isAtTheStartOfSectionFlag,
			isAtTheStartOfEachPageFlag,
			isAtTheStartOfFirstPageOfSpreadFlag
							
		);

		/* 
		//30Oct..the stencil selection is shifted in the list box.
		PMString allStencilFilePath("");
		PMString itemStencilFilePath("");
		PMString productStencilFilePath("");
		ITriStateControlData::TriState isAllStencilSelected = dialogController->GetTriStateControlData(kALLRadioButtonWidgetID);
		bool16 isAllItemProductStencilSelected;
		if(isAllStencilSelected == ITriStateControlData::kSelected)
		{
			isAllItemProductStencilSelected = kTrue;
			allStencilFilePath = dialogController->GetTextControlData(kALLRealEditBoxWidgetID);			
		}			
		else
		{
			isAllItemProductStencilSelected = kFalse;
			itemStencilFilePath = dialogController->GetTextControlData(kItemStencilRealEditBoxWidgetID);
			productStencilFilePath = dialogController->GetTextControlData(kProductStencilRealEditBoxWidgetID);		
		}
		if(MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected)
		{
			//CA("fill data into list box");
			MediatorClass :: vector_subSectionSprayerListBoxParameters[MediatorClass :: selectedIndexOfListBoxElement].ssss.setAll
				(
					verticalSpacingReal,
					horizontalSpacingReal,
					isFlowLeftToRightSelected,
					isTheAlternateCheckBoxSelected,
					istheHorizFlowStateSelected,
					allStencilFilePath,
					productStencilFilePath,
					itemStencilFilePath,
					isAllItemProductStencilSelected					
				);
		
		}
		else
		{
			//if(MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected)
			//{
				MediatorClass ::subSecSpraySttngs.setAll
				(
					verticalSpacingReal,
					horizontalSpacingReal,
					isFlowLeftToRightSelected,
					isTheAlternateCheckBoxSelected,
					istheHorizFlowStateSelected,
					allStencilFilePath,
					productStencilFilePath,
					itemStencilFilePath,
					isAllItemProductStencilSelected						
				);	
			//}

			//
			//CA("don't fill data into list box");
			//vector<subSectionSprayerListBoxParameters> & subSecSpraLbParams =  MediatorClass :: vector_subSectionSprayerListBoxParameters;
			//
			//PMString data;

			//PMString size;
			//size.AppendNumber(subSecSpraLbParams.size());
			//CA(size);


			//for(int32 index = 0;index < subSecSpraLbParams.size();index++)
			//{
			//	
			//	
			//	data.AppendNumber(index);
			//	data.Append("\n");
			//	data.Append("verticalSpacingReal : ");
			//	data.AppendNumber(verticalSpacingReal);
			//	data.Append("\n");

			//	data.Append("horizontalSpacingReal : ");
			//	data.AppendNumber(horizontalSpacingReal);
			//	data.Append("\n");

			//	data.Append("isFlowLeftToRightSelected : ");
			//	data.AppendNumber(isFlowLeftToRightSelected);
			//	data.Append("\n");

			//	data.Append("isTheAlternateCheckBoxSelected : ");
			//	data.AppendNumber(isTheAlternateCheckBoxSelected);
			//	data.Append("\n");
			//	
			//	data.Append("istheHorizFlowStateSelected : ");
			//	data.AppendNumber(istheHorizFlowStateSelected);
			//	data.Append("=========================");
			//	data.Append("\n");
			//	//data.Append("\n");

			//	


			//	subSecSpraLbParams[index].ssss.setAll
			//		(
			//			verticalSpacingReal,
			//			horizontalSpacingReal,
			//			isFlowLeftToRightSelected,
			//			isTheAlternateCheckBoxSelected,
			//			istheHorizFlowStateSelected
			//		);
			//}
			//CA(data);
			//
		}
		
		MediatorClass :: AllStencilFilePath		 = allStencilFilePath;
		MediatorClass :: ItemStencilFilePath	 = itemStencilFilePath;
		MediatorClass :: ProductStencilFilePath  = productStencilFilePath;
		MediatorClass :: isSingleStencilFileForProductAndItem = isAllItemProductStencilSelected;
		//end 30Oct..
		*/

	}while(kFalse);
	}

// End, SpraySettingsDialogController.cpp.




#ifndef __MediatorClass_h__
#define __MediatorClass_h__

//sdk interfaces
#include "IControlView.h"
//sdk classes
#include "vector"
#include "IPanelControlData.h"
//PrintSource4.5 Interfaces
#include "IAppFramework.h"
//PrintSource4.5 ID files
//ProjectSpecific files
#include "subSectionSprayerSettings.h"
#include "CreateMediaPublicationNode.h"
#include "IDialogController.h"
//#include "AP_pub_comment.h"


class MediatorClass
{

public:
	static IControlView * PublicationTemplateMappingListBox;
	static bool16 IsCommonMasterForAllPublicationRadioWidgetSelected;
	static VectorPubModel global_vector_pubmodel;
	static double global_lang_id;
	static int32 publicationDropDownIndexSelected;
	static vector<double> publicationVector;
	static vector<subSectionSprayerListBoxParameters> vector_subSectionSprayerListBoxParameters;
	static int32 selectedIndexOfListBoxElement;

	//modification to intergrate already developed subsectionspraying functionality
	static double currentSelectedPublicationID;
	static double currentSelectedSectionID;
	static vector<CreateMediaPublicationNode> pNodeDataList;

	static int32 global_project_level;

	static subSectionSprayerSettings subSecSpraySttngs;
	static bool16 Is_UseDifferentTemplateForEachSubSectionRadioSelected;

	static bool16 radioCreateMediaBySectionWidgetSelected;
	static bool16 IskeepSpreadsTogether;
	static int32  pageOrderSelectedIndex;
	static vector<int32> vecPageNo;
	static vector<CreateMediaPublicationNodeList> vectorOfpNodeDataList;
	static PMString templateFilePathForSpreadSpray;

	static UIDRef currentProcessingDocUIDRef;
	static IControlView * selectAllSectionCheckBoxControlView;

	static UIDList itemStencilUIDList;
	static UIDList productStencilUIDList;
	static UIDList allStencilUIDList;
	static UIDList hybridTableStencilUIDList;
	static UIDList sectionStencilUIDList;

	static  PMString AllStencilFilePath;		
	static  PMString ItemStencilFilePath;	
	static  PMString ProductStencilFilePath;
	static  PMString HybridTableStencilFilePath;

	static  PMString SectionStencilFilePath;

	static bool16 isSingleStencilFileForProductAndItem;

	static bool16 IsEventSourceInListBox;
	static int32  createMediaRadioOption ;
	static bool16  placeItemsOrProductsByPageAssignment ;

	static vector<subSectionData> vec_SubSecData;
	static IPanelControlData* iPanelCntrlDataPtr;
	static IPanelControlData* iPrimaryPanelCntrlDataPtr;
    static IPanelControlData* iMainPanelCntrlDataPtr;
    static IDialogController* iMainDaialogControllerPtr;

//-	static vector<APpubComment> vector_apPubComment;
//-	static vector<vector<APpubComment> > vector_vector_apPubComment;

	static bool16 isCommentsAutoSpray;

//Price Book Addition
	static bool16 AtTheStartOfSectionState;
	static bool16 AtTheStartOfEachPageState;
	static bool16 AtTheStartOfEachPageAndSpreadState;
	static bool16 AtSpraySectionWithOutPageBreakState;
	static bool16 IsPageBased;
	static bool16 IsLeftHandOddPage;

	static bool16 IsAutomaticallyExportToPDF;
	static int32 sectionLevel;
	~MediatorClass();
	


	

};
#endif

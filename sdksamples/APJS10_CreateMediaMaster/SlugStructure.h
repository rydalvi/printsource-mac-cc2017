#ifndef __SLUGSTRUCT_H__
#define __SLUGSTRUCT_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class SlugStruct
{
public:
	int32 elementId;
	int32 typeId;
	int32 parentId;
	int32 imgflag;
	int32 parentTypeId;
    PMString elementName;
	PMString TagName;
	int32 LanguageID;			//PMString colName;
	int16 whichTab;
	int32 imgFlag;
	int32 sectionID;
	int32 isAutoResize;

	//added on 16Jun for Item table spray in tabbed text format
	int32 tableFlag;
	int32 row_no;
	int32 col_no;
	int32 tagStartPos;
	int32 tagEndPos;
	//end 16Jun
	int32 tableTypeId;  // added for attaching correct type id for table.

	SlugStruct()
	{
		elementId = -1;
		typeId = -1;
		parentId = -1;
		imgflag = -1;
		parentTypeId = -1;
		elementName.Clear();
		TagName.Clear();
		LanguageID = -1;//colName.Clear();
		whichTab = -1;
		imgFlag = -1;
		sectionID = -1;
		isAutoResize = -1;

	//added on 16Jun for Item table spray in tabbed text format
		tableFlag = 0;
		row_no=-1;
		col_no=-1;
		tagStartPos= -1;
		tagEndPos = -1;
	//end 16Jun
		tableTypeId = -1;
	}
};

typedef vector<SlugStruct> SlugList;

#endif
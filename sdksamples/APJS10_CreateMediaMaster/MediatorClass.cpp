#include "VCPlugInHeaders.h"
#include "MediatorClass.h"

#include "CAlert.h"
#define CA(X) CAlert::InformationAlert(X)
#include <UIDList.h>


IControlView * MediatorClass :: PublicationTemplateMappingListBox;
bool16 MediatorClass :: IsCommonMasterForAllPublicationRadioWidgetSelected= kTrue;
VectorPubModel MediatorClass :: global_vector_pubmodel ;
double MediatorClass ::global_lang_id = -1;
int32 MediatorClass :: publicationDropDownIndexSelected = -1;
vector<double> MediatorClass :: publicationVector;
vector<subSectionSprayerListBoxParameters> MediatorClass :: vector_subSectionSprayerListBoxParameters;
int32 MediatorClass :: selectedIndexOfListBoxElement= -1;

//modification to intergrate already developed subsectionspraying functionality
double MediatorClass :: currentSelectedPublicationID=-1;
double MediatorClass :: currentSelectedSectionID=-1;
//9May vector<PublicationNode> MediatorClass :: pNodeDataList;
vector<CreateMediaPublicationNode> MediatorClass :: pNodeDataList;
int32 MediatorClass :: global_project_level = -1;
subSectionSprayerSettings MediatorClass :: subSecSpraySttngs;
bool16 MediatorClass ::Is_UseDifferentTemplateForEachSubSectionRadioSelected = kFalse;

//IPMUnknown* MediatorClass :: CMMActionComponentIPMUnknown = nil;
//8th May 06
 bool16 MediatorClass :: radioCreateMediaBySectionWidgetSelected = kTrue;
 bool16 MediatorClass :: IskeepSpreadsTogether = kFalse;
 int32 MediatorClass :: pageOrderSelectedIndex = 1;
 vector<int32> MediatorClass :: vecPageNo;
 //9May vector<PublicationNodeList> MediatorClass ::vectorOfpNodeDataList;
 vector<CreateMediaPublicationNodeList> MediatorClass ::vectorOfpNodeDataList;
 PMString MediatorClass :: templateFilePathForSpreadSpray="";
 UIDRef MediatorClass ::currentProcessingDocUIDRef = UIDRef ::gNull;

 //added on 6Jun
 IControlView * MediatorClass ::selectAllSectionCheckBoxControlView = nil;
 //ended on 6Jun

 UIDList MediatorClass ::itemStencilUIDList;
 UIDList MediatorClass ::productStencilUIDList;
 UIDList MediatorClass ::allStencilUIDList;	
 UIDList MediatorClass ::hybridTableStencilUIDList;
 UIDList MediatorClass ::sectionStencilUIDList;

PMString MediatorClass :: AllStencilFilePath = "";		
PMString MediatorClass :: ItemStencilFilePath = "";	
PMString MediatorClass :: ProductStencilFilePath = "";

PMString MediatorClass :: SectionStencilFilePath= ""; //22 December
bool16 MediatorClass ::isSingleStencilFileForProductAndItem = kFalse;

//added on 30Sept..
bool16 MediatorClass ::IsEventSourceInListBox = kFalse;
//end on 30Sept..

//30Oct..
int32 MediatorClass :: createMediaRadioOption = -1;
bool16 MediatorClass :: placeItemsOrProductsByPageAssignment = kTrue;
//end 30Oct..

//19-jan
IPanelControlData* MediatorClass ::iPanelCntrlDataPtr = nil;
//19-jan end

//25-jan
IPanelControlData* MediatorClass ::iPrimaryPanelCntrlDataPtr = nil;
IDialogController* MediatorClass::iMainDaialogControllerPtr = nil;

IPanelControlData* MediatorClass ::iMainPanelCntrlDataPtr = nil;
//25-jan end

MediatorClass ::	~MediatorClass()
{
	//CA("in mediatorClass destructor");
}

//3Nov..
vector<subSectionData> MediatorClass :: vec_SubSecData;


//-vector<APpubComment > MediatorClass :: vector_apPubComment;
//-vector<vector<APpubComment> > MediatorClass :: vector_vector_apPubComment;

bool16  MediatorClass::isCommentsAutoSpray = kTrue;
//// Chetan -- 03/10

//Added for price book on 27/12/07
bool16 MediatorClass :: AtTheStartOfSectionState = kFalse;
bool16 MediatorClass :: AtTheStartOfEachPageState = kFalse;
bool16 MediatorClass :: AtTheStartOfEachPageAndSpreadState = kFalse;
bool16 MediatorClass :: AtSpraySectionWithOutPageBreakState = kFalse;

bool16 MediatorClass :: IsPageBased = kFalse;
bool16 MediatorClass :: IsLeftHandOddPage = kFalse;

bool16 MediatorClass::IsAutomaticallyExportToPDF = kTrue;
int32 MediatorClass::sectionLevel =-1;
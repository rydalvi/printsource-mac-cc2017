#ifndef __CMMPlugInEntrypoint_h__
#define __CMMPlugInEntrypoint_h__

#include "PlugIn.h"
#include "GetPlugin.h"
#include "ISession.h"

class CMMPlugInEntrypoint : public PlugIn
{
public:
	virtual bool16 Unload();

#ifdef WINDOWS
	static ITypeLib* fSPTypeLib;
#endif                    
};

#endif
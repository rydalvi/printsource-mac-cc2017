#ifndef __CREATEINDDPREVIEW_H__
#define __CREATEINDDPREVIEW_H__
#include "SnapshotUtils.h"
/** \li How to take a preview JPEG of an InDesign document
 * 
 * 	This snippet class uses SnapshotUtils to make a preview of
 * 	the 1st page of an InDesign document.
 * 
 *	@ingroup sdk_snippet
 *	@ingroup sdk_graphic
*/

class CreateInddPreview 
{
public:

	/** Constructor.  
	 */
	CreateInddPreview() :  fSnapshotUtils(nil) {}

	/** Destructor.
	 * 	Cleans up the internal SnapshotUtils member.
	 */
	virtual ~CreateInddPreview() {
		this->CleanUp();
	}

	////following function is added by vijay on 27-1-2007
	//ErrorCode CreatePagePreview( const PMRect& maxBounds,const IDFile& jpegFile,
	//															UIDRef boxUIDRef);

	////following function is added by vijay on 31-1-2007 //to take snapshot of page
	//ErrorCode CreatePagePreview(IDFile& jpegFile , UIDRef boxUIDRef);
ErrorCode CreatePagePreview  (  const IDFile &  documentFile,  
  const IOpenFileCmdData::OpenFlags  docOpenFlags,  
  const IDFile &  jpegFile,  
  const PMReal &  xScale = 1.0,  
  const PMReal &  yScale = 1.0,  
  const PMReal &  desiredRes = 36.0 ,
  PMString outPutFolderpath = "" , PMString subDirectoryPath = "" , PMString toMoveJPG = ""
 );

  bool16 getMarginBounds(const UIDRef& pageUIDRef, PMRect& pageBounds, PMRect& marginBoxBounds);

 ErrorCode CreatePagePreviewForPAGE_BASED_WB  (  const IDFile &  documentFile,  
  const IOpenFileCmdData::OpenFlags  docOpenFlags,  
  const IDFile &  jpegFile,  
  const PMReal &  xScale ,  
  const PMReal &  yScale ,  
  const PMReal &  desiredRes, 
  PMString outPutFolderpath, PMString subDirectoryPath, PMString toMoveJPG
 );

private:
	
	ErrorCode SetPageToSnapshot(UIDRef & PageUIDRef , const PMReal& xScale = 1.0, const PMReal& yScale = 1.0 , const PMReal& desiredRes = 36.0);
	/**	Exports the snapshot to JPEG file.
	 * 	@param jpegSysFile IN File to write.
	 * 	@return kSuccess on success, kFailure otherwise.
	 */
	ErrorCode ExportImageToJPEG(const IDFile& jpegSysFile);

	/** Deletes internal instance of SnapshotUtils.
	 */
	void CleanUp();

	/** Internal snapshot utils member
	 */
	SnapshotUtils* fSnapshotUtils;
};

#endif
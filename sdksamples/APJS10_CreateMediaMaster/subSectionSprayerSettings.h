#ifndef __subSectionSprayerSettings_h__
#define __subSectionSprayerSettings_h__
class subSectionSprayerSettings
{
public:
	PMReal verticalBoxSpacing;
	PMReal horizontalBoxSpacing;
	bool16 flowLeftToRight;
	bool16 alternateForEachPage;
	bool16 flowHorizontalFlow;

	bool16 AltVerticalFlowFlag;
	bool16 VerticalFlowFlag;
	bool16 AltHorizontalFlowFlag; 
	bool16 HorizontalFlowFlag;	
	bool16 HorizontalFlowForAllImageSprayFlag;
	bool16 SprayItemPerFrameFlag;

	bool16 WithoutPageBreakFlag;
	bool16 AddSectionStencilFlag;
	bool16 AtTheStartOfSectionFlag;
	bool16 AtTheStartOfEachPageFlag;
	bool16 AtTheStartOfFirstPageOfSpreadFlag;

	//30Oct..
	/*
	PMString AllStencilFilePath;			// All-Option Stencil filepath	
	PMString ProductStencilFilePath;		// Product Stencil Filepath
	PMString ItemStencilFilePath;			// Item Stencil Filepath
	bool16 isSingleStencilFileForProductAndItem;
	*/
	//end 30Oct..


	subSectionSprayerSettings()
	{
		verticalBoxSpacing  = 5.0;
		horizontalBoxSpacing = 5.0;
		flowLeftToRight = kTrue;
		alternateForEachPage = kFalse;
		flowHorizontalFlow =kFalse;    //19-jan

		AltVerticalFlowFlag = kFalse;
		VerticalFlowFlag = kTrue;
		AltHorizontalFlowFlag = kFalse;
		HorizontalFlowFlag = kFalse;	
		HorizontalFlowForAllImageSprayFlag = kFalse;
		SprayItemPerFrameFlag = kFalse;


		WithoutPageBreakFlag = kFalse;
		AddSectionStencilFlag = kFalse;
		AtTheStartOfSectionFlag = kFalse;
		AtTheStartOfEachPageFlag = kFalse;
		AtTheStartOfFirstPageOfSpreadFlag = kFalse;
		
		//30Oct..
		/*
		AllStencilFilePath = "";	
		ProductStencilFilePath = "";
		ItemStencilFilePath = "";	
		isSingleStencilFileForProductAndItem = kTrue;//kFalse..changed to kTrue on 30Oct..
		*/
		//end 30Oct..

	}
	void setAll(
			PMReal paramVerticalBoxSpacing,
			PMReal paramHorizontalBoxSpacing,
			bool16 paramFlowLeftToRight,
			bool16 paramAlternateForEachPage,
			bool16 paramFlowHorizontalFlow,
			//30Oct..Stencil seleciton is shifted in the list box.
			/*
				PMString paramAllStencilPath,
				PMString paramProductStencilPath,
				PMString paramItemStencilPath,
				bool16 isProdItemFlag
			*/
			//end 30Oct..
			bool16 paramAltVerticalFlowFlag,
			bool16 paramVerticalFlowFlag,
			bool16 paramAltHorizontalFlowFlag,
			bool16 paramHorizontalFlowFlag,
			bool16 paramHorizontalFlowForAllImageSprayFlag,
			bool16 paramSprayItemPerFrameFlag,

			bool16 paramWithoutPageBreakFlag,
			bool16 paramAddSectionStencilFlag,
			bool16 paramAtTheStartOfSectionFlag,
			bool16 paramAtTheStartOfEachPageFlag,
			bool16 paramAtTheStartOfFirstPageOfSpreadFlag
			
		)
	{
		verticalBoxSpacing =paramVerticalBoxSpacing;
		horizontalBoxSpacing =paramHorizontalBoxSpacing;
		flowLeftToRight = paramFlowLeftToRight;
		alternateForEachPage = paramAlternateForEachPage;
		flowHorizontalFlow = paramFlowHorizontalFlow;
		//30Oct..Stencil seleciton is shifted in the list box.
		/*
		AllStencilFilePath = paramAllStencilPath;
		ProductStencilFilePath = paramProductStencilPath;
		ItemStencilFilePath = paramItemStencilPath;
		isSingleStencilFileForProductAndItem = isProdItemFlag;
		*/
		//end 30Oct..
		AltVerticalFlowFlag = paramAltVerticalFlowFlag;
		VerticalFlowFlag = paramVerticalFlowFlag;		
		AltHorizontalFlowFlag = paramAltHorizontalFlowFlag;
		HorizontalFlowFlag = paramHorizontalFlowFlag;

		HorizontalFlowForAllImageSprayFlag = paramHorizontalFlowForAllImageSprayFlag;
		SprayItemPerFrameFlag = paramSprayItemPerFrameFlag;
		
		WithoutPageBreakFlag = paramWithoutPageBreakFlag;
		AddSectionStencilFlag = paramAddSectionStencilFlag;
		AtTheStartOfSectionFlag = paramAtTheStartOfSectionFlag;
		AtTheStartOfEachPageFlag = paramAtTheStartOfEachPageFlag;
		AtTheStartOfFirstPageOfSpreadFlag = paramAtTheStartOfFirstPageOfSpreadFlag;
	}
									 
									 
};

//1Nov...
struct subSectionData
{
	double sectionID;
	double subSectionID;
	PMString subSectionName;	
	PMString sectionName;
	PMString number;
};
//1Nov...

class subSectionSprayerListBoxParameters
{
public:
	bool16 isSelected;
	//int32 sectionID;
	//int32 subSectionID;
	//PMString subSectionName;	
	//PMString sectionName;
	//PMString number;
	PMString masterFileWithCompletePath;	// Master TemplateFilePath	
	
	//1Nov...
	vector<subSectionData> vec_ssd;
	PMString displayName;
	//1Nov...
	PMString AllStencilFilePath;			// All-Option Stencil filepath	
	PMString ProductStencilFilePath;		// Product Stencil Filepath
	PMString ItemStencilFilePath;			// Item Stencil Filepath
	PMString HybridTableStencilFilePath;	// Hybrid Table Stencil Filepath
	bool16 isSingleStencilFileForProductAndItem;
	PMString SectionStencilFilePath;
	//end 30Oct..

//2-may
	int32 spreadCount;
//2-may
//14-may
	int32 whiteboard_Stage;
//14-may
//// Chetan -- 28-09
	bool16 isCommentAutoSpray;
	subSectionSprayerSettings ssss;	

	subSectionSprayerListBoxParameters()
	{
		isSelected = kTrue;
		//subSectionID = -1;;
		//subSectionName = "";
		masterFileWithCompletePath = "";	
		//sectionID = -1;
		//sectionName = "";

		//30Oct..
		//number = "-1";
		AllStencilFilePath = "";			// All-Option Stencil filepath	
		ProductStencilFilePath="";		// Product Stencil Filepath
		ItemStencilFilePath="";			// Item Stencil Filepath
		SectionStencilFilePath="";
		isSingleStencilFileForProductAndItem = kTrue;
		//end 30Oct..

		//1Nov..
		vec_ssd.clear();
		displayName = "";
		//1Nov..
//2-may
		int32 spreadCount = -1;
//2-may
//// Chetan -- 28-09
	   isCommentAutoSpray = kTrue;
//// Chetan -- 28-09
	}
	

};



									 
#endif								 
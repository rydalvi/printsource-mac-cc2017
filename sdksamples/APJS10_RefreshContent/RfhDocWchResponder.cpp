//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/docwatch/RfhDocWchResponder.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: yjoshi $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: 1.4 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IClassIDData.h"
#include "ISignalMgr.h"
#include "IDocumentSignalData.h"
#include "IUIDData.h"
#include "ILayoutUIUtils.h"
// Implementation includes:
#include "CreateObject.h"
#include "CResponder.h"
#include "RfhID.h"
#include "CAlert.h"
#include "DocWchUtils.h"
#include "RfhSelectionObserver.h"
#include "RfhActionComponent.h"
#include "LayoutUIID.h"


#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("RfhDocWchResponder.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

extern bool16 FlgToCheckMenuAction;
extern bool16 ISRfreshBookDlgOpen;


/** RfhDocWchResponder
	Handles signals related to document file actions.  The file action 
	signals it receives are dictated by the DocWchServiceProvider class.

	RfhDocWchResponder implements IResponder based on
	the partial implementation CResponder.


	@ingroup docwatch
	@author John Hake
*/
class RfhDocWchResponder : public CResponder
{
	public:
	
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		RfhDocWchResponder(IPMUnknown* boss);

		/**
			Respond() handles the file action signals when they
			are dispatched by the signal manager.  This implementation
			simply creates alerts to display each signal.

			@param signalMgr Pointer back to the signal manager to get
			additional information about the signal.
		*/
		virtual void Respond(ISignalMgr* signalMgr);

};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(RfhDocWchResponder, kRfhDocWchResponderImpl)

/* DocWchActionComponent Constructor
*/
RfhDocWchResponder::RfhDocWchResponder(IPMUnknown* boss) :
	CResponder(boss)
{
}

/* Respond
*/
void RfhDocWchResponder::Respond(ISignalMgr* signalMgr)
{
	// Exit if the responder should do nothing
	if (DocWchUtils::QueryDocResponderMode() != kTrue)
		return;

	// Get the service ID from the signal manager
	ServiceID serviceTrigger = signalMgr->GetServiceID();

	// Get a UIDRef for the document.  It will be an invalid UIDRef
	// for BeforeNewDoc, BeforeOpenDoc, AfterSaveACopy, and AfterCloseDoc because
	// the document doesn't exist at that point.
	/*InterfacePtr<IDocumentSignalData> docData(signalMgr, UseDefaultIID());
	if (docData == nil)
	{
		ASSERT_FAIL("Invalid IDocumentSignalData* - RfhDocWchResponder::Respond");
		return;
	}
	*/
	if(ISRfreshBookDlgOpen)
		return;	

	/*UIDRef doc = docData->GetDocument();*/
	RfhSelectionObserver SELObj(this);


	// Take action based on the service ID
	switch (serviceTrigger.Get())
	{
	//	case kBeforeNewDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kBeforeNewDocSignalStringKey);
	//		break;
	//	}
	//	case kDuringNewDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kDuringNewDocSignalStringKey);
	//		break;
	//	}
		//case kAfterNewDocSignalResponderService:
		//{	//CA("kAfterNewDocSignalResponderService");
		//	if(ISRfreshBookDlgOpen)
		//		break;
		//	FlgToCheckMenuAction= kTrue;			
		//	SELObj.loadPaletteData();
		//	
		//	IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();
		//	if(iDoc== nil)
		//	{	
		//		SELObj.EnableAll();				
		//	}
		//	//DocWchUtils::DwAlert(doc, kAfterNewDocSignalStringKey);
		//	break;
		//}
	//	case kBeforeOpenDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kBeforeOpenDocSignalStringKey);
	//		break;
	//	}
	//	case kDuringOpenDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kDuringOpenDocSignalStringKey);
	//		break;
	//	}
		//case kAfterOpenDocSignalResponderService:
		//{	//CA("After document open");
		//	if(ISRfreshBookDlgOpen)
		//		break;
		//	
		//	FlgToCheckMenuAction= kTrue;			
		//	SELObj.loadPaletteData();
		//	
		//	IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();
		//	if(iDoc== nil)
		//	{	
		//		SELObj.EnableAll();
		//		
		//	}
		//	//DocWchUtils::DwAlert(doc, kAfterOpenDocSignalStringKey);
		//	break;
		//}
	//	case kBeforeSaveDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kBeforeSaveDocSignalStringKey);
	//		break;
	//	}
	//	case kAfterSaveDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kAfterSaveDocSignalStringKey);
	//		break;
	//	}
	//	case kBeforeSaveAsDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kBeforeSaveAsDocSignalStringKey);
	//		break;
	//	}
	//	case kAfterSaveAsDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kAfterSaveAsDocSignalStringKey);
	//		break;
	//	}
	//	case kBeforeSaveACopyDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kBeforeSaveACopyDocSignalStringKey);
	//		break;
	//	}
	//	case kDuringSaveACopyDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kDuringSaveACopyDocSignalStringKey);
	//		break;
	//	}
	//	case kAfterSaveACopyDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kAfterSaveACopyDocSignalStringKey);
	//		break;
	//	}
	//	case kBeforeRevertDocSignalResponderService:
	//	{	CA("Before Revert Doc signal");
	//		//DocWchUtils::DwAlert(doc, kBeforeRevertDocSignalStringKey);
	//		break;
	//	}
		//case kAfterRevertDocSignalResponderService:
		//{	//CA("After revert doc signal");
		//	if(ISRfreshBookDlgOpen)
		//		break;
		//	FlgToCheckMenuAction= kTrue;
		//	SELObj.loadPaletteData();
		//	
		//	
		//	//DocWchUtils::DwAlert(doc, kAfterRevertDocSignalStringKey);
		//	break;
		//}
		case kBeforeCloseDocSignalResponderService:
		{	//CA("Before close Doc 1 ");
			if(ISRfreshBookDlgOpen)
				break;

			//CA("Breaking Rule for Close Doc..............");
			FlgToCheckMenuAction= kTrue;
			SELObj.loadPaletteData();
			/////added by shail
			/*IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(iDoc== nil)
			{	
				SELObj.DisableAll();

			}*/
			//DocWchUtils::DwAlert(doc, kBeforeCloseDocSignalStringKey);
			break;
		}
	//	case kAfterCloseDocSignalResponderService:
	//	{	FlgToCheckMenuAction= kTrue;
	//		SELObj.loadPaletteData();
	//		//DocWchUtils::DwAlert(doc, kAfterCloseDocSignalStringKey);
	//		break;
	//	}

		case kOpenLayoutSignalServiceID:
		{	//CA("After document open");
			if(ISRfreshBookDlgOpen)
				break;
			
			FlgToCheckMenuAction= kTrue;			
			SELObj.loadPaletteData();
			
			IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(iDoc != nil)
			{	
				SELObj.EnableAll();				
			}
			//DocWchUtils::DwAlert(doc, kAfterOpenDocSignalStringKey);
			break;
		}

        default:
			{	
				break;
			}
	}
}

// End, RfhDocWchResponder.cpp.




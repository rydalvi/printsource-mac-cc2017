#ifndef _IRfhSuite_
#define _IRfhSuite_

class IPMUnknown;
class PMRect;
//class K2Vector;
class ITextFocus;

class IRfhSuite : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IRfhSUITE };

	typedef PMRect TextInset;

	typedef K2Vector<TextInset> TextInsets;

public:
	virtual bool16 CanGetTextInsets(void) = 0;

	virtual void GetTextInsets(TextInsets& textInsets) = 0;

	virtual bool16 CanApplyTextInset(void) = 0;

	virtual ErrorCode ApplyTextInset(const PMReal& insetValue) = 0;
	virtual ErrorCode GetTextFocus(ITextFocus * & Ifocus)=0;

};	

#endif // _IRfhSuite_





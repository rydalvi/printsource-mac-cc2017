//========================================================================================
//  
//  $File: $
//  
//  Owner: Shelendra Singh
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
#include "IPaletteMgr.h"
#include "IApplication.h"
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IAppFramework.h"
#include "IActionStateList.h"
#include "ILayoutUIUtils.h"
// Project includes:
#include "RefreshContent31ID.h"
#include "RfhActionComponent.h"
#include "IWindow.h"
#include "MediatorClass.h"
#include "ITextControlData.h"
#include "RfhSelectionObserver.h"
#include "IPanelControlData.h"
#include "DocWchUtils.h"
#include "RefreshData.h"
/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup refreshcontent31

*/
bool16 FlgToCheckMenuAction= kFalse;
int32 Flag1;
IPaletteMgr* RfhActionComponent::palettePanelPtr=0;
bool16 GroupFlag= kFalse;
bool16 HiliteFlag= kFalse;
extern RefreshDataList rDataList;

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(RfhActionComponent, kRefreshContent31ActionComponentImpl)

/* RefreshContent31ActionComponent Constructor
*/
RfhActionComponent::RfhActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
/* DoAction
*/
void RfhActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{	//CA("Inside Do Action");
	switch (actionID.Get())
	{

		
		//case kRfhAboutActionID:
	//	{
		//	this->DoAbout();
	//		break;
	//	}
					
		
		case kRfhMenuItem1ActionID:
		{	//CA("kRfhMenuItem1ActionID");
			this->DoMenuItem1(ac);
			break;
		}

		case kRfhMenuItem2ActionID:
		{	//CA("kRfhMenuItem2ActionID");
			this->DoMenuItem2(ac);
			break;
		}

		case kRfhMenuItem3ActionID:
		{	//CA("kRfhMenuItem3ActionID");
			this->DoMenuItem3(ac);
			break;
		}
		case kRfhContentPanelWidgetActionID:
		{	//CA("kRfhContentPanelWidgetActionID");
			FlgToCheckMenuAction= kTrue;
			this->DoPalette();
			break;
		}


		default:
		{	//CA("Default");
			break;
		}
	}
}

/* DoAbout
*/
void RfhActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kRefreshContent31AboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}

/* DoMenuItem1
*/
void RfhActionComponent::DoMenuItem1(IActiveContext* ac)
{
	RfhSelectionObserver qwe(this);	
	qwe.reSortTheListForObject();
	qwe.fillDataInListBox(kFalse);
	GroupFlag= kFalse;	
}

void RfhActionComponent::DoMenuItem2(IActiveContext* ac)
{
	GroupFlag= kTrue;
	RfhSelectionObserver qwe(this);
	qwe.reSortTheListForElem();
	qwe.fillDataInListBox(kTrue);
	//CAlert::InformationAlert("Group by Element");
}

void RfhActionComponent::DoMenuItem3(IActiveContext* ac)
{
	if(!HiliteFlag)
		HiliteFlag= kTrue;
	else
		HiliteFlag=kFalse;
	//CAlert::InformationAlert("Hilite changed content");
}

void RfhActionComponent::DoPalette()
{
	do
	{	
		InterfacePtr<IApplication> 	iApplication(gSession->QueryApplication());
		if(iApplication==nil)
		{
			
			break;
		}
		InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(iPaletteMgr==nil)
		{
			
			break;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		if(iPanelMgr == nil)
		{			
			break;
		}

		PMLocaleId nLocale=LocaleSetting::GetLocale();
		iPanelMgr->ShowPanelByMenuID(kRfhContentPanelWidgetActionID);
		RfhActionComponent::palettePanelPtr=iPaletteMgr;
		DocWchUtils::StartDocResponderMode();
	}while(kFalse);
}

void RfhActionComponent::UpdateActionStates (IActionStateList* iListPtr)
{	//CA("Inside UpdateActionStates");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	bool16 result=ptrIAppFramework->LOGINMngr_getLoginStatus();

	IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();

	if(result  && iDoc)
	{
		//CA("After Login");
		iListPtr->SetNthActionState(0,kEnabledAction); //Hilite changed content
		iListPtr->SetNthActionState(1,kEnabledAction); // Group by Element
		

		if(GroupFlag)
		{
			iListPtr->SetNthActionState(0,kSelectedAction);
			iListPtr->SetNthActionState(1,kEnabledAction);
		}
		else
		{
			iListPtr->SetNthActionState(1,kSelectedAction);
			iListPtr->SetNthActionState(0,kEnabledAction);
		}

		
	}
	else
	{
		iListPtr->SetNthActionState(0,kDisabled_Unselected);
		iListPtr->SetNthActionState(1,kDisabled_Unselected);
	}

	

/*	if(HiliteFlag)
		iListPtr->SetNthActionState(0,kSelectedAction);
	else
		iListPtr->SetNthActionState(0,kEnabledAction);*/

}

void RfhActionComponent::CloseRefreshPalette(void)
{
	do
	{	
		//DoPalette();
	// Added By Awasthi
		InterfacePtr<IApplication> 	iApplication(gSession->QueryApplication());
		if(iApplication==nil)
			break;
		InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(iPaletteMgr==nil)
			break;
		InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		//Mediator::iPanelCntrlDataPtr = iPanelMgr;
	// End Awasthi
		if(Mediator::iPanelCntrlDataPtr== nil)
			break;
		
		/* Clear all listboxes and disable all controls */
		
		// MAke comment Awasthi
		/*IControlView* dropdownCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kTPLDropDownWidgetID);
		
		if(!dropdownCtrlView)
			break;
		
		IControlView* refreshBtnCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kTPLRefreshIconSuiteWidgetID);
		if(!refreshBtnCtrlView)
			break;
		
		IControlView* appendRadCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kAppendRadBtnWidgetID);
		if(!appendRadCtrlView)
			break;
		
		IControlView* embedRadCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kEmbedRadBtnWidgetID);
		if(!embedRadCtrlView)
			break;
		
		IControlView* tagFrameChkboxCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kTagFrameWidgetID);
		if(!tagFrameChkboxCtrlView)
			break;
		*/

		// End making Commments Awasthi
	//	IControlView* classTreeCtrlView=
	//		Mediator::iPanelCntrlDataPtr->FindWidget(kTPLClassTreeIconSuiteWidgetID);
	//	if(!classTreeCtrlView)
	//		break;
		
		Mediator::dropdownCtrlView->Disable();
		Mediator::RfhRefreshButtonView->Disable();		
		
//		SDKListBoxHelper listHelper(this, kRfhPluginID);	
		
	//	Mediator::listControlView->Hide();		
// 
		if(iPaletteMgr)//TPLActionComponent::palettePanelPtr)
		{	
			InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr/*TPLActionComponent::palettePanelPtr*/, UseDefaultIID()); 
			if(iPanelMgr == nil)
				break;
			
			if(iPanelMgr->IsPanelWithMenuIDVisible(kRfhContentPanelWidgetActionID))
			{				
				iPanelMgr->HidePanelByMenuID(kRfhContentPanelWidgetActionID);				
			}
		}
		
	}while(kFalse);
	
}



//  Code generated by DollyXS code generator


//  Code generated by DollyXs code generator

#include "VCPluginHeaders.h"

#ifndef __COMMONFUNCTIONS_H__
#define __COMMONFUNCTIONS_H__


#include "SlugStructure.h"
#include "XMLReference.h"
#include "TagStruct.h"
#include "ITextModel.h"

//added on 7Aug..ItemTable Handling
#include "XMLTagAttributeValue.h"

#include "PublicationNode.h"
#include "IAppFramework.h"


PMString prepareTagName(PMString name);
PMString keepOnlyAlphaNumeric(PMString name);
void  addAttributesToANewlyCreatedTag(SlugStruct & newTagToBeAdded ,XMLReference & createdElement);
void  sprayItemTableInTabbedTextForm(const TagStruct & tagInfo,InterfacePtr<ITextModel> & textModel);
ErrorCode RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTable(const UIDRef& boxUIDRef, TagStruct& slugInfo);

//added on 12July..
void GetItemTableInTabbedTextForm(TagStruct & tagInfo,PMString & tabbedTextData);


//added on 7Aug..ItemTable Handling
void setAllXMLTagAttributeValues(XMLReference & xmlTagReference ,XMLTagAttributeValue & tagAttrVal);
void createAllXMLTagAttributes(XMLReference & xmlRef,XMLTagAttributeValue & tagAttrVal);
void getAllXMLTagAttributeValues(XMLReference & xmlTagReference,XMLTagAttributeValue & tagAttrVal);

//added on 8 Aug..ItemTable Handling
void AddTagToCellText
				(
					const GridAddress & cellAddr,
					const InterfacePtr<ITableModel> & tableModel,
                    PMString & dataToBeSprayed,
					InterfacePtr<ITextModel> & textModel,
					const XMLTagAttributeValue xmlTagAttrVal
				);
void sprayItemItemTableScreenPrintInTabbedTextForm
	( 
		TagStruct & tagInfo,
		InterfacePtr<ITextModel> & textModel
	);

void handleSprayingOfEventPriceRelatedAdditions(PublicationNode & pNode,TagStruct & tagInfo,PMString & itemAttirbuteValue);
bool8 getAttributeIDFromNotesNew(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotes);

void sprayCMedCustomTableScreenPrintInTabbedTextForm(TagStruct & tagInfo,InterfacePtr<ITextModel> &textModel);//Added On 17/11 For Med Custom Table in tab text form refresh

//added by Tushar on 26/12/06
bool8 getAttributeIDFromNotesNewByElementId(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotes,double eventPriceTableTypeID);

//added by Tushar on 15/01/07 
bool8 getAttributeIDFromNotes(PublicationNode & pNode ,const double & sectionid,const  double & selectedItemID,PMString & attributeIDfromNotesSales,PMString & attributeIDfromNotesRegular,double SalePriceTabletypeId,double RegularPriceTabletypeId);

ErrorCode RefreshIndividualItemAndNonItemCopyAttributesPresentInsideTableNewVersion(const UIDRef& boxUIDRef, TagStruct& slugInfo);
void makeTheTagImpotent(IIDXMLElement * xmlPtr);
void makeTagInsideCellImpotent(IIDXMLElement * tableXMLElementPtr);

ErrorCode RefreshCustomTabbedText(const UIDRef& boxID,TagStruct& tagInfo);
ErrorCode RefreshCustomTabbedTextNewoption(const UIDRef& boxID,TagStruct& tagInfo);//By Amarjit patil
ErrorCode RefreshAllStandardTables(const UIDRef& boxID,TagStruct& tagInfo);
void sprayForAllStandardTable(UIDRef boxUIDRef,PublicationNode pNode,InterfacePtr<ITextModel> textModel);

ErrorCode RefreshMMYCustomTable(const UIDRef& boxID,TagStruct& tagInfo);
int32 findOutHowManyMMYTablesInTextFrame(TagStruct& tagInfo);

//int32 TotalNoOfItemsPerMake(const CMMYTable* MMYTablePtr, const int32 selectedTableIndex);
bool16 InspectChars(InterfacePtr<ITextModel>& textModel,TextIndex startIndex, TextIndex endIndex, TextIndex& tableStartIndex, TextIndex& tableEndIndex);
PMString GetCharacter(UTF32TextChar character);

bool16 deleteTableAndContents(TagStruct& tagInfo, int32 noOfTablesToBeDelete);
bool16 addTableToFrame(InterfacePtr<ITextModel>& textModel, int32 noOfTablesToBeAdded);
bool16 NeedToRefreshMMYCustomTable(const UIDRef& boxUIDRef,TagStruct& tagInfo);

ErrorCode RefreshMultipleListsSprayedInOneTable(const UIDRef& boxUIDRef, TagStruct& slugInfo);
ErrorCode RefreshMultipleListsSprayedInOneTableNew(const UIDRef& boxUIDRef, TagStruct& slugInfo);

ErrorCode ReplaceText(ITextModel* textModel, const TextIndex position, const int32 length, const /*K2*/boost::shared_ptr<WideString>& text);

ErrorCode refreshAdvancedTableByCell(const UIDRef& boxUIDRef, TagStruct& slugInfo);

#endif

#ifndef __RfhDlgDialogController_H_DEFINED__
#define __RfhDlgDialogController_H_DEFINED__

#include "IBookContentMgr.h"
#include "CDialogController.h"
#include <vector>

class BookContentDocInfo
{
public:
	PMString DocumentName;
	IDFile DocFile;
	int32 index;
	UID DocUID;
	//IDocument* documentPtr;
};

typedef vector<BookContentDocInfo> BookContentDocInfoVector;
class RfhDlgDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		RfhDlgDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/**
			Initializes each widget in the dialog with its default value.
			Called when the dialog is opened.
			@param dlgContext
		*/
		virtual void InitializeDialogFields( IActiveContext* dlgContext);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			this method to be called. When all widgets are valid, 
			ApplyFields will be called.		
			@param myContext
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields( IActiveContext* myContext);

		/**
			Retrieve the values from the widgets and act on them.
			@param myContext
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields( IActiveContext* myContext, const WidgetID& widgetId);
		void StartBookRefresh();
		BookContentDocInfoVector* GetBookContentDocInfo(IBookContentMgr* bookContentMgr);
		bool16 reSortTheListForBookUniqueAttr(void);
		bool16 fillDataInListBox(int GroupFlag);


};
#endif

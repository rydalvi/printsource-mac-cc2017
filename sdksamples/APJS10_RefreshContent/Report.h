#ifndef __REPORT_H__
#define __REPORT_H__

#include "VCPluginHeaders.h"
#include "vector"

using namespace std;

class Report
{
public:
	double id;
	double attributeId;
	Report();
};

typedef vector<Report> itemList;
typedef vector<Report> productList;	

inline Report::Report()
{
	id = 0;
	attributeId = 0;
}

#endif
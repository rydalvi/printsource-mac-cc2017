#include "VCPluginHeaders.h"
#include "MediatorClass.h"
#include "CAlert.h"

int16 Mediator::selectedRadioButton=-1;
int32 Mediator::refreshPageNumber=-1;
IDialog* Mediator::dialogPtr=nil;
IControlView* Mediator::listControlView=nil;
IControlView *Mediator::editBoxView=nil;
IControlView *Mediator::ChkHighlightView=nil;
bool16 Mediator::isGroupByObj=kTrue;
IPanelControlData* Mediator::iPanelCntrlDataPtr=nil;
IPanelControlData* Mediator::iPanelCntrlDataPtrTemp=nil;
IControlView* Mediator::dropdownCtrlView=nil;
bool16 Mediator::loadData=kFalse;
IControlView* Mediator::RfhRefreshButtonView=nil;
ITextControlData* Mediator::EditBoxTextControlData=nil;
IControlView* Mediator::RfhLocateButtonView=nil;
IControlView* Mediator::BooklistControlView=nil;
IControlView* Mediator::RfhDlgRefreshButtonView=nil;
IControlView* Mediator::RfhDlgLocateButtonView=nil;
IControlView* Mediator::RfhDlgHiliteChkBoxView=nil;
IControlView* Mediator::RfhDlgDeletChkBoxView=nil;

//Added on 30May by Yogesh
IControlView* Mediator::selectAllCheckBoxView = nil;
//end 30May

//Added by vijay on 5 OCT 2006
IControlView* Mediator::RfhReloadButtonView=nil;

IPanelControlData* Mediator::catalogRefrshPanelControlData= nil;

bool16 Mediator::isSilentModeSelected = kTrue;
bool16 Mediator::isFrameRefresh = kFalse;

void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line)
{
	
	PMString pluginNameString("PluginName : ");
	pluginNameString.Append("AP45_RefreshContent.pln");

	PMString fileNameString("FileName : ");
	fileNameString.Append(fileName);

	PMString functionNameString("FunctionName : ");
	functionNameString.Append(functionName);

	PMString lineNumber("Line Number : ");
	lineNumber.AppendNumber(line);
	lineNumber.Append("\n***********************");

	PMString messageString("");
	messageString.Append(message);

	CAlert::InformationAlert(messageString);
	
}
#ifndef __PAGEDATA_H__
#define __PAGEDATA_H__

#include "VCPluginHeaders.h"
#include "TagStruct.h"
#include "IDFile.h"
#include "vector"


using namespace std;

class TagInfo
{
public:
	double		elementID;
	PMString	elementName;
	int32       StartIndex;
	int32       EndIndex;
	double		typeID;
	int32		whichTab;
	bool16		isTaggedFrame;
	int32		tableFlag;
	int32		imageFlag;
	double		languageid;

	//int32		header;		
	//bool16		isEventField; 
	int32		dataType;	
	//int32		imageIndex;	
	//int32		flowDir;	
	int32		childTag;		
	int32		tableType;	

	double parentId;
	int32 catLevel;

	int32 isAutoResize;
	int32 isSprayItemPerFrame;

	double parentTypeId;
		
		
	//int32 field1;
	//int32 field2;
	//int32 field3;
	//int32 field4;
	//int32 field5;


	TagInfo()
	{
		elementID=-1;
		typeID=-1;
		isTaggedFrame=kFalse;
		whichTab=-1;
		languageid = -1;

		//header = -1;		
		//isEventField = kFalse; 
		dataType = -1;	
		//imageIndex = -1;	
		//flowDir = -1;	
		//childTag = -1;	
		tableType = -1;

		parentId = -1;
		catLevel = -1;

		isAutoResize = -1;
		isSprayItemPerFrame = -1;

		parentTypeId = -1;
				
		//field1 = -1;
		//field2 = -1;
		//field3 = -1;
		//field4 = -1;
		//field5 = -1;
	}
};

typedef vector<TagInfo> ElementInfoList;

class ObjectData
{
public:
	double			objectID;
	double			objectTypeID;
	double			publicationID;
	PMString		objectName;
	bool16			isTableFlag;
	bool16			isImageFlag;
	int32			whichTab;


	ElementInfoList elementList;
	ObjectData()
	{
		objectID=-1;
		objectTypeID=-1;
		publicationID=-1;
		whichTab=-1;		
	}
};



typedef vector<ObjectData> ObjectDataList;

class PageData
{
public:
	UIDRef BoxUIDRef;
	UID boxID;
	UIDRef DocumentUIDRef;
	IDFile sysFile;
	ObjectDataList objectDataList;
	PageData()
	{
		boxID=kInvalidUID;
	}
};
class TextVector
{
public:
PMString Word;
UIDRef BoxUIDRef;
int32 StartIndex;
int32 EndIndex;
TextVector()
{
StartIndex=0;
EndIndex=0;
}
};
typedef vector<TextVector> TextVectorList;

class CSprayStencilInfo
{
public:
	bool16 isCopy;
	bool16 isProductCopy;
	bool16 isSectionCopy;
	bool16 isEventField;

	bool16 isAsset;
	bool16 isProductAsset;
	bool16 isSectionLevelAsset;

	bool16 isDBTable;
	bool16 isProductDBTable;
	bool16 isCustomTablePresent;

	bool16 isHyTable;
	bool16 isProductHyTable;
	bool16 isSectionLevelHyTable;
	
	bool16 isChildTag;
	bool16 isProductChildTag;

	bool16 isBMSAssets;
	bool16 isProductBMSAssets;
	bool16 isSectionLevelBMSAssets;

	bool16 isItemPVMPVAssets;
	bool16 isProductPVMPVAssets;
	bool16 isSectionPVMPVAssets;
	bool16 isPublicationPVMPVAssets;
	bool16 isCatagoryPVMPVAssets;

	bool16 isEventSectionImages;
	bool16 isCategoryImages;
	bool16 isSprayItemPerFrame;
	
	double langId;

	vector<double > AttributeIds;
	vector<double> ProductAttributeIds;
	vector<double> itemAttributeIds;
	vector<double> childItemAttributeIds;
	vector<double > AssetIds;
	vector<double> ProductAssetIds;
	vector<double> itemAssetIds;
	vector<double > dBTypeIds;
	vector<double > HyTypeIds;	

	vector<double > itemPVAssetIdList;
	vector<double > productPVAssetIdList;
	vector<double > sectionPVAssetIdList;
	vector<double > publicationPVAssetIdList;
	vector<double > catagoryPVAssetIdList;
	vector<double > categoryAssetIdList;
	vector<double > eventSectionAssetIdList;

	CSprayStencilInfo()
	{
		isCopy = kFalse;
		isProductCopy = kFalse;
		isSectionCopy = kFalse;

		isAsset = kFalse;
		isProductAsset = kFalse;
		isSectionLevelAsset = kFalse;

		isDBTable = kFalse;
		isProductDBTable = kFalse;
		isCustomTablePresent = kFalse;

		isHyTable = kFalse;
		isProductHyTable = kFalse;
		isSectionLevelHyTable = kFalse;
	
		isChildTag = kFalse;
		isProductChildTag = kFalse;

		isBMSAssets = kFalse;
		isProductBMSAssets = kFalse;
		isSectionLevelBMSAssets = kFalse;

		isItemPVMPVAssets = kFalse;
		isProductPVMPVAssets = kFalse;
		isSectionPVMPVAssets = kFalse;
		isPublicationPVMPVAssets = kFalse;
		isCatagoryPVMPVAssets = kFalse;

		isEventSectionImages = kFalse;
		isCategoryImages = kFalse;
		langId = 91; // default lang
		isSprayItemPerFrame = kFalse;

	}	
};
typedef vector<CSprayStencilInfo> vectorCSprayStencilInfo;

#endif
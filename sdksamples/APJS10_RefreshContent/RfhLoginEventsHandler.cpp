#include "VCPlugInHeaders.h"
#include "RfhID.h"
#include "ILoginEvent.h"
#include "IRegisterLoginEvent.h"
#include "CAlert.h"
#include "RfhLoginEventsHandler.h"
#include "RfhSelectionObserver.h"
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IWindow.h"
#include "IApplication.h"
#include "RfhActionComponent.h"
#include "IWidgetParent.h"
#include "DocWchUtils.h"
#include "MediatorClass.h"
#include "RFHTreeDataCache.h"
#include "ILayoutUIUtils.h"
#include "K2Vector.tpp" 


#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("RfhLoginEventsHandler.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

extern bool16 FlgToCheckMenuAction;
extern bool16 IsDeleteFlagSelected;

extern K2Vector<double> sectionIDInDoc; 

CREATE_PMINTERFACE(RfhLoginEventsHandler,kRfhLoginEventsHandlerImpl)



RfhLoginEventsHandler::RfhLoginEventsHandler
(IPMUnknown* boss):CPMUnknown<ILoginEvent>(boss)
{
	//CA("RfhLoginEventsHandler::RfhLoginEventsHandler");	
}

RfhLoginEventsHandler::~RfhLoginEventsHandler()
{}

bool16 RfhLoginEventsHandler::userLoggedIn(void)
{
	do
	{
		/* Check if the Template palette is on screen */
/*		InterfacePtr<IApplication> iApplication(gSession->QueryApplication());
		if(!iApplication)
		{
			CA("1");
			return kFalse;
		}

		InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(!iPaletteMgr)
		{
			CA("2");
			return kFalse;
		}
		
		InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		if(!iPanelMgr)
		{
			CA("3");
			return kFalse;
		}
		
		PMLocaleId nLocale = LocaleSetting::GetLocale();	

		//iPaletteMgr->Startup(nLocale);
		IWindow* wndPtr=iPaletteMgr->GetPalette(0);
		if(!wndPtr)
		{
			CA("4");
			return kFalse;
		}
CA("No Probs at all");
		// Template Palette is present in the screen */

		IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(iDoc == NULL)
		{
			return kFalse;
		}

//CA("user logged in");
		RfhSelectionObserver selObserver(this);
		selObserver.loadPaletteData();	
	}while(kFalse);			
	return kTrue;
}

bool16 RfhLoginEventsHandler::userLoggedOut(void)
{	
	// Awasthi
	RfhActionComponent actionComponetObj(this);
	actionComponetObj.CloseRefreshPalette();

	RFHTreeDataCache dc;
	dc.clearMap();

	return kTrue;	
}

bool16 RfhLoginEventsHandler::resetPlugIn(void)
{
	//CA("RfhLoginEventsHandler::resetPlugIn");
	do{

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil){ 
			//CA("No iApplication");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhLoginEventsHandler::resetPlugIn::iApplication==nil");
			break;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); 
		if(iPanelMgr == nil){ 
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhLoginEventsHandler::resetPlugIn::iPanelMgr == nil");
			break;
		}

		bool16 isPanelOpen = iPanelMgr->IsPanelWithMenuIDMostlyVisible(kRfhPanelWidgetActionID);
		if(!isPanelOpen){//CA("!isPanelOpen");
			break;
		}

		RfhActionComponent actionComponetObj(this);
		actionComponetObj.CloseRefreshPalette();
		

		Mediator::listControlView=nil;
		Mediator::editBoxView=nil;
		Mediator::ChkHighlightView=nil;
	
		Mediator::iPanelCntrlDataPtr=nil;
		Mediator::iPanelCntrlDataPtrTemp=nil;
		Mediator::dropdownCtrlView=nil;
		
		Mediator::RfhRefreshButtonView=nil;
		Mediator::EditBoxTextControlData=nil;
		Mediator::RfhLocateButtonView=nil;
		Mediator::BooklistControlView=nil;
		Mediator::RfhDlgRefreshButtonView=nil;
		Mediator::RfhDlgLocateButtonView=nil;
		Mediator::RfhDlgHiliteChkBoxView=nil;
		Mediator::RfhDlgDeletChkBoxView=nil;

		FlgToCheckMenuAction= kTrue;

		PMLocaleId nLocale=LocaleSetting::GetLocale();
		iPanelMgr->ShowPanelByMenuID(kRfhPanelWidgetActionID);
		DocWchUtils::StartDocResponderMode();
		//Mangesh Code for resizing initially
		if(FlgToCheckMenuAction)
		{
			IControlView* icontrol = iPanelMgr->GetVisiblePanel(kRfhPanelWidgetID);
			if(!icontrol)
			{
				//CA("!icontrol");
				return kFalse;
			}
			icontrol->Resize(PMPoint(PMReal(207),PMReal(291)));
			//icontrol->Invalidate();
		}
		//end Mangesh Code for resizing initially
//		RfhActionComponent::palettePanelPtr=iPaletteMgr;	//Commented By Amit..	
		IsDeleteFlagSelected = kTrue;




	}while(kFalse);
	return kTrue;
}
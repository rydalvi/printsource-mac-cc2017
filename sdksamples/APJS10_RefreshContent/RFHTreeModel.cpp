#include "VCPlugInHeaders.h"
#include "IHierarchy.h"	
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
#ifdef	 DEBUG
#include "IPlugInList.h"
#include "IObjectModel.h"
#endif
#include "ILayoutUtils.h" //Cs4
#include "RfhID.h"
#include "RFHTreeModel.h"
#include "CAlert.h"
#include "RefreshData.h"
//#include "TPLMediatorClass.h"


#include "RfhDataNode.h"
//#include "IClientOptions.h"
//#include "IAppFrameWork.h"
#include "RFHTreeDataCache.h"
//#include "ISpecialChar.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("RFHTreeModel.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

RFHTreeDataCache dc;

int32 RFHTreeModel::root=0;
PMString RFHTreeModel::FieldName;
int32 RFHTreeModel::FieldId=0;
extern RefreshDataList UniqueDataList;
int32 RowCountForTree = -1;

int32 TreeDisplayOption = -1; // Values: 1 = Frame Refresh Field Tree, 2 = Book Refresh Document Tree, 3 = Book Refresh Field Tree
extern RefreshDataList UniqueBookDataList;
extern RefreshDataList BookDocumentDataList;



RFHTreeModel::RFHTreeModel()
{
	//root=-1;
	//classId=11002;
}

RFHTreeModel::~RFHTreeModel() 
{
}


int32 RFHTreeModel::GetRootUID() const
{	
	if(FieldId<=0)
		return 0;// kInvalidUID;
	
	RfhDataNode pNode;
	if(dc.isExist(root, pNode))
	{
		return root;
	}
	
	dc.clearMap();
	

	
	RefreshDataList CurrentNodeList;
	CurrentNodeList.clear();

	if(TreeDisplayOption == -1)
		return 0;

	if((TreeDisplayOption == 1) && (UniqueDataList.size() <= 0))
		return 0;

	if((TreeDisplayOption == 2) && (BookDocumentDataList.size() <= 0))
		return 0;

	if((TreeDisplayOption == 3) && (UniqueBookDataList.size() <= 0))
		return 0;

	if(TreeDisplayOption == 1)
		CurrentNodeList = UniqueDataList;
	else if(TreeDisplayOption == 2)
		CurrentNodeList = BookDocumentDataList;
	else if(TreeDisplayOption == 3)
		CurrentNodeList = UniqueBookDataList;

	RowCountForTree = 0;
	PMString name("Root");
	pNode.setFieldId(root);
	pNode.setChildCount(static_cast<int32>(CurrentNodeList.size()));
	/*pNode.setLevel(0);*/
	pNode.setParentId(-2);
	pNode.setFieldName(name);
	pNode.setSequence(0);
	dc.add(pNode);

	int32 count=0;
	for(int i =0 ; i<CurrentNodeList.size(); i++)
	{ //CAlert::InformationAlert(" 123");
			/*pNode.setLevel(1);*/
			
				pNode.setParentId(root);
				pNode.setSequence(i);
				count++;
				pNode.setFieldId(count/*UniqueDataList[i].elementID*/);
				PMString ASD(CurrentNodeList[i].name);
				pNode.setFieldName(ASD);
				//CA(ASD);
				pNode.setChildCount(0);
                pNode.setHitCount(0);
				pNode.setIsSelected(CurrentNodeList[i].isSelected);
			dc.add(pNode);
		}
	return root;
}


int32 RFHTreeModel::GetRootCount() const
{
	int32 retval=0;
	RfhDataNode pNode;
	if(dc.isExist(root, pNode))
	{
		return pNode.getChildCount();
	}
	return 0;
}

int32 RFHTreeModel::GetChildCount(const int32& uid) const
{
	RfhDataNode pNode;

	if(dc.isExist(uid, pNode))
	{
		return pNode.getChildCount();
	}
	return -1;
}

int32 RFHTreeModel::GetParentUID(const int32& uid) const
{
	RfhDataNode pNode;

	if(dc.isExist(uid, pNode))
	{
		if(pNode.getParentId()==-2)
			return 0; //kInvalidUID;
		return pNode.getParentId();
	}
	return 0; //kInvalidUID;
}


int32 RFHTreeModel::GetChildIndexFor(const int32& parentUID, const int32& childUID) const
{
	RfhDataNode pNode;
	int32 retval=-1;
	
	if(dc.isExist(childUID, pNode))
		return pNode.getSequence();
	return -1;
}


int32 RFHTreeModel::GetNthChildUID(const int32& parentUID, const int32& index) const
{
	RfhDataNode pNode;
	int32 retval=-1;

	if(dc.isExist(parentUID, index, pNode))
		return pNode.getFieldId();
	return 0; // kInvalidUID;
}


int32 RFHTreeModel::GetNthRootChild(const int32& index) const
{
	RfhDataNode pNode;
	if(dc.isExist(root, index, pNode))
	{
		return pNode.getFieldId();
	}
	return 0; //kInvalidUID;
}

int32 RFHTreeModel::GetIndexForRootChild(const int32& uid) const
{
	RfhDataNode pNode;
	if(dc.isExist(uid, pNode))
		return pNode.getSequence();
	return -1;
}

PMString RFHTreeModel::ToString(const int32& uid, int32 *Rowno) const
{
	RfhDataNode pNode;
	PMString name("");

	if(dc.isExist(uid, pNode))
	{	
		*Rowno= pNode.getSequence();
		//*Rowno = pNode.getHitCount();
		/*PMString ASD("getHitCount in ToolString : ");
		ASD.AppendNumber(pNode.getHitCount());
		CA(ASD);*/
		return pNode.getFieldName();
	}
	return name;
}


int32 RFHTreeModel::GetNodePathLengthFromRoot(const int32& uid) 
{
	RfhDataNode pNode;
	if(uid==root)
		return 0;
	if(dc.isExist(uid, pNode))
		return (1);
	return -1;
}

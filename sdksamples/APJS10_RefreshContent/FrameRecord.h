#ifndef __FRAMERECORD_H__
#define __FRAMERECORD_H__

#include "VCPluginHeaders.h"
#include <vector>

class FrameRecord{
public:
	UID frameUID;
	int32 pageNumber;
	int32 frameType;	// 0 = text Frame , 1 =  Table Fame , 2 Graphic Frame
	PMString parentID;
	PMString Data;
	bool16 isSelected;
	bool16 isProcessed;
	double langId;
	double sectionID;

	FrameRecord(){
		frameUID = kInvalidUID;
		pageNumber = -1;
		frameType = -1;
		parentID = "";
		Data = "";
		isSelected = kFalse;
		isProcessed = kFalse;
		langId = -1;
		sectionID = -1;
	}

};

typedef vector<FrameRecord> vecFrameRecord;


class UniqueItemOnDoc{
public:
	double itemID;
	double sectionID;
	double langaugeID;
	bool8 isNewflagON;
	UniqueItemOnDoc(){
		itemID = -1;
		sectionID = -1;
		langaugeID = -1;
		isNewflagON = kFalse;
	}
};
typedef vector<UniqueItemOnDoc> vecUniqueItemOnDoc;
typedef vector<vector<double> > vecNewItemIDS;

#endif
#include "VCPluginHeaders.h"
#include "BookReportData.h"
#include "SDKLayoutHelper.h"
#include "IHierarchy.h"
#include "IAppFramework.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "Utils.h"
#include "ITblBscSuite.h"
#include "ILayoutSelectionSuite.h"
#include "ITableUtils.h"
#include "Utils.h"
#include "ITextModel.h"
#include "ITextFrameColumn.h"
#include "ITableCommands.h"
#include "ITextMiscellanySuite.h"
#include "TPLID.h"
#include "ITextEditSuite.h"
#include "ITextModelCmds.h"
#include "ICommand.h"
#include "IRangeData.h"
#include "IUIDData.h"
#include "IGeometry.h"
#include "SDKUtilities.h"
#include "ILayoutUIUtils.h"
#include "URI.h"
#include "IURIUtils.h"
#include "IDocument.h"
#include "CmdUtils.h"
#include "IPlaceGun.h"
#include "IImportResourceCmdData.h"
#include "IReplaceCmdData.h"
#include "ITableTextSelection.h"
#include "IConcreteSelection.h"
#include "ITextSelectionSuite.h"
#include "IBookManager.h"
#include "IBook.h"
#include "IBookContentCmdData.h"
#include "IBookContentMgr.h"
#include "IBookPaginateOptions.h"
#include "IIntData.h"
#include "IBoolData.h"
#include "ITableCommands.h"
#include "ITableModelList.h"
#include "IBookContent.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#include "ITableSelectionSuite.h"
#include "ITextAttributeSuite.h"
#include "ITableSuite.h"
#include "ITextAttrRealNumber.h"
#include "ITextAttrFont.h"
#include "IApplication.h"
#include "IDocFontMgr.h"
#include "IActiveContext.h"
#include "IWorkspace.h"
#include "textiterator.h"
#include "ITagReader.h"
#include "ITextAttributes.h"
#include "ITextAttrUID.h"
#include "IApplyMultAttributesCmdData.h"
#include "IGraphicAttrRealNumber.h"
#include "IGraphicAttributeUtils.h"
#include "Utils.h"
#include "ISpread.h"
#include "ISpreadList.h"
#include "IStyleGroupManager.h"
#include "ITextTarget.h"
#include "IAttrReport.h"
#include "IComposeScanner.h"

#define FILENAME			PMString("Refresh.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);   
PMString  numToPMString(int32 num);

#define CA(X) CAlert::InformationAlert \
	( \
	PMString("Refresh.cpp") + PMString("\n") + \
  PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + PMString("\n") + X)

#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


PMString BookReportData::currentbookName ="" ;
int32 BookReportData::currntRowIndex =0;

UIDRef newFrameUIDRef1 = UIDRef::gNull;
vector<BookReportData> BookReportData::vecReportRows;

short int BookReportData::reportType =0;                   //0 - Update; 1- New , 2 - Deletes;
//bool16 BookReportData::isNewPageAdded = kFalse;
PMString BookReportData::currentSection = "";
int32 BookReportData::currentTableIndex = 0;

double BookReportData::langaugeID = -1;
vector<vector<BookReportData> > BookReportData::entireReport;
vector<TagList> BookReportData::deletedItemTagList;
vector<double> BookReportData::newItemidList;
vector<TagList> BookReportData::documentTagList;
vector<vector<double> > BookReportData::entireNewItemList;
vector<vector<TagList> > BookReportData::entireBookTagList;
vector<double> BookReportData::ielementID;
extern bool16 refreshTableByAttribute; 

UIDRef BookReportData::CreateTextFrame(ILayoutControlData *layoutControlData,  const PMRect &boundsInPageCoords)
{
	UIDRef result = UIDRef::gNull;
     SDKLayoutHelper layoutHelper;
	
	 do {
		 InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		 if(ptrIAppFramework == nil)
		 {
			 //CA("ptrIAppFramework == nil");			
			break;
		 }
	
		 InterfacePtr<IHierarchy> activeSpreadLayerHierarchy(layoutControlData->QueryActiveLayer());
		 ASSERT(activeSpreadLayerHierarchy != nil);
         if (activeSpreadLayerHierarchy == nil) {
			 ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTextFrame:activeSpreadLayerHierarchy == nil");
             break;
         }
         UIDRef parentUIDRef = ::GetUIDRef(activeSpreadLayerHierarchy);		
         if (layoutControlData->GetPage() == kInvalidUID) {
			 ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTextFrame:layoutControlData->GetPage() == kInvalidUID");
             break;
         }
		 UIDRef pageUIDRef(parentUIDRef.GetDataBase(), layoutControlData->GetPage());
		 PMRect boundsInParentCoords = layoutHelper.PageToSpread(pageUIDRef, boundsInPageCoords);
		 // Create the frame.
         result = layoutHelper.CreateTextFrame(parentUIDRef, boundsInParentCoords, 0);
		
	} while(false);
	
	return result;
	
}
bool16 BookReportData::selectFrame(const UIDRef& frameUIDRef)
{
	bool16 result = kFalse;
	do 
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break;
		}
		//Select the frame.
		InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
		if (selectionManager == nil)
		{
			//CA("selectionManager == nil");
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::selectionManager == nil");
			break;
		}

		// Deselect everything.
		selectionManager->DeselectAll(nil); // deselect every active CSB

		// Make a layout selection.
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::!layoutSelectionSuite == nil");
			break;
		}

		layoutSelectionSuite->SelectPageItems(UIDList(frameUIDRef), 
			Selection::kReplace,  Selection::kDontScrollLayoutSelection);				
		result = kTrue;

	} while(false);
	//CA("FrameIS Selected");
	return result;
}

UIDRef BookReportData::CreateTable(const UIDRef& storyRef, 
										const TextIndex at,
										const int32 numRows, 
										const int32 numCols,
										const PMReal rowHeight,
                                        const PMReal colWidth, 
										const CellType cellType,
										PMString headerSectionOrPageText,
										PMString headerBookNameText,
										 short int reporttype     )
{
	//CA("BookReportData::CreateTable");		
	ErrorCode status = kFailure;
	UIDRef tableUIDRef = UIDRef::gNull;
	do {		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break;
		}
		InterfacePtr<IHierarchy> graphicFrameHierarchy(storyRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			//CA("graphicFrameHierarchy is NULL");	
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!graphicFrameHierarchy == nil");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			//CA("multiColumnItemHierarchy is NULL");	
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!multiColumnItemHierarchy == nil");
			break;
		}
					
		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			//CA("multiColumnItemTextFrame is NULL");		
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!multiColumnItemTextFrame == nil");
			break;
		}
		InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			//CA("frameItemHierarchy is NULL");			
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!frameItemHierarchy == nil");
			break;
		}
		
		InterfacePtr<ITextFrameColumn>frameItemTFC(frameItemHierarchy, UseDefaultIID());
		if (!frameItemTFC) {
			//CA("!!ITextFrameColumn");			
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!frameItemTFC == nil");
			break;
		}

		InterfacePtr<ITextModel> textModel(frameItemTFC->QueryTextModel());
		if(!textModel)
		{
			//CA("!textModel" );	
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!frameItemTFC == nil");
			break;
		}
		

		TextIndex startIndex = frameItemTFC->TextStart();
		TextIndex txtSpan( frameItemTFC->TextSpan()); 
		// 1026750: Instead of processing kNewTableCmdBoss use the ITableUtils facade.
		Utils<ITableUtils> tableUtils;
		if (!tableUtils) {
			//CA("tableUtils == nil");
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!tableUtils == nil");
			break;
		}
		tableUtils->InsertTable (textModel,
									at,
									0, 
									numRows,
									numCols, 
									rowHeight,
									551/3/*colWidth*/, 
									cellType,
									ITableUtils::eSetSelectionInFirstCell);
		status = kSuccess;		
		BookReportData::currentTableIndex++;
		tableUIDRef = tableUtils->GetTableModel(textModel,startIndex);	
						
		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==NULL)
		{
			//CA("Table Model List is Null");
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!tableList == nil");
			break;
		}
		int32	tableIndex = tableList->GetModelCount() - 1;

		tableIndex = BookReportData::currentTableIndex -1;

		/*PMString asf("In CreateTable TableIndex = ");
		asf.AppendNumber(tableIndex);
		CA(asf);*/

		if(tableIndex<0) //This check is very important...  this ascertains if the table is in box or not.
		{
			//CA("No Table in Frame");
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!tableIndex<0");
			break;
		}
		/*else
			CA("Table Index is > 0");*/

		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
		if (tableModel == NULL)
		{
			//CA("Err: invalid interface pointer ITableFrame");
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!tableModel== NULL");
			break;
		}	
		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
		{
			//CA("Err: invalid interface pointer ITableCommands");
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!tableCommands==NULL");
			break;
		}	

		UIDRef tableRef(::GetUIDRef(tableModel));

		tableUIDRef = tableRef;		
		{		
			//CA("reporttype == 0)"); //**** Means Update
			
			headerSectionOrPageText = BookReportData::getCurrentBookName();
			headerSectionOrPageText.Append(" / ");
//			if(reportType != 1 )
			headerSectionOrPageText.Append(BookReportData::currentSection);

			headerBookNameText   = "Book Name : ";
			headerBookNameText.Append(BookReportData::getCurrentBookName());

			WideString* wStrSection=new WideString(headerSectionOrPageText);
			WideString* wStrBookName=new WideString(headerBookNameText);
			WideString* wStrUpDates;
			if(reporttype == 0) 
				 wStrUpDates=new WideString("Updates  ");
			else if(reporttype == 1) 
				 wStrUpDates=new WideString("New  ");
			else
				 wStrUpDates=new WideString("Deletes ");
			for(int32 rowIndex = 0;rowIndex <1;rowIndex++)
			{			
				for(int32 colIndex = 0;colIndex < 3;colIndex++)
				{				
					if(colIndex ==  0){						
						tableCommands->SetCellText(*wStrSection, GridAddress(rowIndex, 0));	
						headerSectionOrPageText.clear();
					}
					if(colIndex ==  2){										
						tableCommands->SetCellText(*wStrUpDates, GridAddress(rowIndex,2));
					}
				}
			}								
			delete wStrSection;
			delete wStrBookName;
			delete wStrUpDates;

			WideString* wStrField=new WideString("");   //"Fields : ");
			WideString* wStrDocumentValue=new WideString("Document Value : ");
			WideString* wONEsourcealue=new WideString("ONEsource Value : ");
			
			if(reporttype == 0){
				tableCommands->SetCellText(*wStrField, GridAddress(1,0));
				tableCommands->SetCellText(*wStrDocumentValue, GridAddress(1,1));
				tableCommands->SetCellText(*wONEsourcealue, GridAddress(1,2));
			}
			else if(reporttype == 1)  //****New
			{
				//tableCommands->SetCellText(*wStrField, GridAddress(/*2*/1,0));
				//tableCommands->SetCellText(*wStrDocumentValue, GridAddress(1,1));
				tableCommands->SetCellText(*wONEsourcealue, GridAddress(1,1));
			}
			else  //**** Deletes
			{
				//tableCommands->SetCellText(*wStrField, GridAddress(/*2*/1,0));
				tableCommands->SetCellText(*wStrDocumentValue, GridAddress(1/*2*/,1));
				//tableCommands->SetCellText(*wONEsourcealue, GridAddress(1,1));
			}

			delete wStrField;
			delete wStrDocumentValue;
			delete wONEsourcealue;
			

			PMReal real(0.25);
			BookReportData::selectTableBodyColumns(tableModel,0,3);
			//CA("Making 10 Points");
			BookReportData::ChangeFontSizeOfSelectedTextInFrame(10);  //** Font siz e== 10
			BookReportData::SetCellStroke(Tables::eBottomSide,real,tableUIDRef,tableModel);
			//CA("Making 10 PointsAfter ");

			RowRange rg(0,2);
			if(tableModel->CanConvertToHeaderRows(rg)){											
				tableModel->ConvertToHeaderRows(rg);
			}
						
			BookReportData::currntRowIndex =3;

			//**** Now Making All Header Row Bold.						
			InterfacePtr<ITableSelectionSuite>	iTableSelectionSuite (Utils<ISelectionUtils> ()->QueryActiveTableSelectionSuite());
			if(iTableSelectionSuite == nil)
			{
				//CA("iTableSelectionSuite ==nil");
				ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!iTableSelectionSuite==NULL");
				break;
			}
			iTableSelectionSuite->SelectAllHeaderRows(tableModel);

			InterfacePtr<ITextAttributeSuite> textAttributeSuite((ITextAttributeSuite*)Utils<ISelectionUtils>()->QuerySuite(ITextAttributeSuite::kDefaultIID));	
			if (textAttributeSuite == nil)
			{
				//CA("textAttributeSuite == nil");
				ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!textAttributeSuite==NULL");
				break;
			}
			textAttributeSuite->ToggleBold();	

			

//			InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>
//																	(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));
//			if(!tblSelSuite){
//				//CA("!tblSelSuite ");
//				ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!tblSelSuite==NULL");
//				break;
//			}
//			//CA("Selected..");			
//			tblSelSuite->SelectRows(0,2,tableModel,kTrue,kTrue);
//			BookReportData::ChangeFontSizeOfSelectedTextInFrame(10);		
		}			
	} while (false);

	return tableUIDRef;	
}

PMString BookReportData::getCurrentBookName(){return currentbookName;}
void BookReportData::setCurrentBookName(const PMString other){currentbookName = other;}

bool16 BookReportData::addRowInTable(const UIDRef& tableUIDRef,int32 numRows,int32 numCols,int32 height,int32 width)
{
	//CA("BookReportData::addRowInTable..");
	bool16 isRowAdded = kFalse;
	
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break;
		}
		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
			//CA("Err: invalid interface pointer ITableFrame");
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!Err: invalid interface pointer ITableFrame");
			break;
		}
		int32 ColWidth =183.6;
		int32 RowHt =20.0;

		PMReal ColumnWidth(ColWidth);
		PMReal RowHeight(RowHt);

		int32 presentRows =tableModel->GetTotalRows().count;
		int32 presentCols = tableModel->GetTotalCols().count;

		/*PMString asd("presentRows  ");
		asd.AppendNumber(presentRows);
		asd.Append("\npresentCols  ");
		asd.AppendNumber(presentCols);
		asd.Append("\nnumRows ");
		asd.AppendNumber(numRows);
		CA(asd);*/

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL){
			//CA("Err: invalid interface pointer ITableFrame");
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreateTable::!Err: invalid interface pointer ITableFrame");
			break;
		}
		ErrorCode result = tableCommands->InsertRows(RowRange(presentRows-1,1), Tables::eAfter, 0);

		if(result == kSuccess){		
			isRowAdded = kTrue;
		}
	}while(kFalse);	
	return isRowAdded;
	
}

UIDRef BookReportData::CreatePictureBoxInCurrentCell(int32 rowindex,int32 colIndex,const PMString fullImagePath )
{
	//CA(" BookReportData::CreatePictureBoxInCurrentCell");
	UIDRef newPictureBox = UIDRef::gNull;
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break;
		}

		UIDRef ref;
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	//CA("!iSelectionManager");
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreatePictureBoxInCurrentCell::!iSelectionManager == NIL");
			break;
		}

		InterfacePtr<ITextMiscellanySuite> txtSelectionSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(IID_ITPLTEXTMISCELLANYSUITEE,iSelectionManager)));
		if(!txtSelectionSuite){
			//CA("Please Select a Type Tool");
			ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreatePictureBoxInCurrentCell::!txtSelectionSuite == NIL");
			break;
		}
		else
		{
			//CA("else");
			bool16 ISTextFrame = txtSelectionSuite->GetFrameUIDRef(ref);
			if(	ISTextFrame)
			{
				//CA("ISTextFrame");
				InterfacePtr<ITextEditSuite> textEditSuite(iSelectionManager, UseDefaultIID());
				int32 start=0;
				txtSelectionSuite->GetCaretPosition(start);
				int32 end=start;
				
				InterfacePtr<IHierarchy> graphicFrameHierarchy(ref, UseDefaultIID());
				if (graphicFrameHierarchy == nil) 
				{
					//CA("graphicFrameHierarchy is NULL");
					ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreatePictureBoxInCurrentCell::!graphicFrameHierarchy == NIL");
					break;
				}

				InterfacePtr<IHierarchy>
				multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
				if (!multiColumnItemHierarchy) {
					//CA("multiColumnItemHierarchy is NULL");
					ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreatePictureBoxInCurrentCell::!multiColumnItemHierarchy == NIL");
					break;
				}

				InterfacePtr<IMultiColumnTextFrame>
				frameItemTFC(multiColumnItemHierarchy, UseDefaultIID());
				if (!frameItemTFC) {
					//CA("!!!ITextFrameColumn");
					ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreatePictureBoxInCurrentCell::!frameItemTFC == NIL");
					break;
				}

				InterfacePtr<ITextModel> txtModel(frameItemTFC->QueryTextModel());
				if(!txtModel)
				{
					//CA("!txtModel" );
					ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreatePictureBoxInCurrentCell::!txtModel == NIL");
					break;
				}
				UIDRef ref1=::GetUIDRef(frameItemTFC);
				bool16 isInsideTable = kFalse;
				isInsideTable=Utils<ITableUtils>()->InsideTable(txtModel, start);
				if(isInsideTable)
				{
					if(!frameItemTFC)
					{	
						//CA("!frameItemTFC" );
						ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::CreatePictureBoxInCurrentCell::!frameItemTFC == NIL");
						break;
					}
					TextIndex TablStartIndex = Utils<ITableUtils>()->TableToPrimaryTextIndex(txtModel,start);
					UIDRef tableRef=( Utils<ITableUtils>()->GetTableModel(txtModel, TablStartIndex));

					UIDRef textStoryUIDRef = ::GetUIDRef(txtModel);
					UIDRef ref1=::GetUIDRef(frameItemTFC);
					int32 uidref = ref1.GetUID().Get();
					
					BookReportData::InsertInline(textStoryUIDRef,start,fullImagePath);										
					
				}
			}			
		}		
	}while(kFalse);	
	return newPictureBox;
}

ErrorCode BookReportData::InsertInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex,const PMString &fullImagepath)	
{
	//CA("BookReportData::InsertInline");
	ErrorCode status = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		//ptrIAppFramework->LogDebug("AP7_RefreshConrtent::BookReportData::InsertInline::!frameItemTFC == NIL");
		return status;
	}
	do {
		
		newFrameUIDRef1 = UIDRef::gNull;
		status = BookReportData::CreateFrame(storyUIDRef.GetDataBase(), newFrameUIDRef1);		
		if (status != kSuccess) {
			//CA("status != kSuccess");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::InsertInline::status != kSuccess");
			break;
		}
		status = BookReportData::ChangeToInline(storyUIDRef, whereTextIndex, newFrameUIDRef1);	
		if(status == kSuccess){
			//CA("fullImagepath	:	"+fullImagepath);
			BookReportData::selectFrame(newFrameUIDRef1);
			BookReportData::ImportFileInFrame(newFrameUIDRef1,fullImagepath);
			
			BookReportData::fitImageInBox(newFrameUIDRef1,kTrue);
			BookReportData::deSelectFrame(newFrameUIDRef1);
		}		
	} while (false);	
	return status;
	
}
ErrorCode BookReportData::CreateFrame(IDataBase* database, UIDRef& newFrameUIDRef)
{
	PMRect bounds(0, 0, 40, 40);
	SDKLayoutHelper layoutHelper;
	newFrameUIDRef = layoutHelper.CreateRectangleFrame(UIDRef(database, kInvalidUID), bounds);
	if (newFrameUIDRef)		
		return kSuccess;
	else
		return kFailure;
}

ErrorCode BookReportData::ChangeToInline(const UIDRef& storyUIDRef, const TextIndex& whereTextIndex, const UIDRef& frameUIDRef)
{
	//CA("BookReportData::ChangeToInline");
	ErrorCode status = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return status;
	}
	do {
		// Validate parameters.
		InterfacePtr<ITextModel> textModel(storyUIDRef, UseDefaultIID());
		ASSERT(textModel != nil);
		if(!textModel) 
		{
			//CA("!textModel..");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ChangeToInline::!textModel");		
			break;
		}
		InterfacePtr<IGeometry> pageItemGeometry(frameUIDRef, UseDefaultIID());		
		if (pageItemGeometry == nil) 
		{
			//CA("pageItemGeometry == nil");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ChangeToInline::pageItemGeometry == nil");		
			break;
		}

		// Insert character in text flow to anchor the inline.
		/*K2*/boost::shared_ptr<WideString>	insertMe(new WideString);	//---CS5--

		insertMe->Append(kTextChar_Inline); 
    	InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
		if(!textModelCmds){
			//CA("!textModelCmds");		
			break;
		}
		InterfacePtr<ICommand> insertTextCmd(textModelCmds->InsertCmd(whereTextIndex, insertMe));
		if(!insertTextCmd){
			//CA("!insertTextCmd");
			break;
		}
		//ASSERT(insertTextCmd != nil);
		ErrorCode status1 = CmdUtils::ProcessCommand(insertTextCmd);	
		if(status1 != kSuccess) 
		{
			//CA("status1 != kSuccess");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ChangeToInline::status1 != kSuccess");		
			break;
		}

		// Change the page item into an inline.
		InterfacePtr<ICommand> changeILGCmd(CmdUtils::CreateCommand(kChangeILGCmdBoss));		
		if (changeILGCmd == nil) 
		{
			//CA("changeILGCmd == nil");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ChangeToInline::changeILGCmd == nil");		
			break;
		}
		InterfacePtr<IRangeData> rangeData(changeILGCmd, UseDefaultIID());		
		if (rangeData == nil)
		{
			//CA("rangeData == nil");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ChangeToInline::rangeData == nil");
			break;
		}
		rangeData->Set(whereTextIndex, whereTextIndex);
		InterfacePtr<IUIDData> ilgUIDData(changeILGCmd, UseDefaultIID());		
		if (ilgUIDData == nil) 
		{
			//CA("ilgUIDData == nil");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ChangeToInline::ilgUIDData == nil");		
			break;
		}
		
		ilgUIDData->Set(frameUIDRef);
		changeILGCmd->SetItemList(UIDList(textModel));
		status = CmdUtils::ProcessCommand(changeILGCmd);
	} while(kFalse);
	return status;
}

bool16 BookReportData::ImportFileInFrame(const UIDRef& imageBox, const PMString& fromPath)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kFalse;
//CA("fromPath  = "+fromPath);
	bool16 fileExists = SDKUtilities::FileExistsForRead(fromPath) == kSuccess;
	if(!fileExists) 
	{
		ptrIAppFramework->LogError("AP7_RefreshContent::BookReportData::ImportFileInFrame::!fileExists");	
		return kFalse;
	}	
	IDocument *docPtr=Utils<ILayoutUIUtils>()->GetFrontDocument(); 
	IDataBase* db = ::GetDataBase(docPtr);
	if (db == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ImportFileFrame::db is nil");	
		return kFalse;
	}

	IDFile sysFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&fromPath));	
	InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
	if(!importCmd)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ImportFileInFrame::!importCmd");	
		return kFalse;
	}
	URI tmpURI;
	Utils<IURIUtils>()->IDFileToURI(sysFile, tmpURI);
	InterfacePtr<IImportResourceCmdData> importFileCmdData(importCmd, IID_IIMPORTRESOURCECMDDATA); // no kDefaultIID	
	if (importFileCmdData == nil) {
		//CA(" importFileCmdData == nil ");
		return kFalse;
	}	
	importFileCmdData->Set(db,tmpURI,kMinimalUI);

	ErrorCode err = CmdUtils::ProcessCommand(importCmd);
	if(err != kSuccess)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ImportFileInFrame::err != kSuccess");	
		return kFalse;
	}

	InterfacePtr<IPlaceGun> placeGun(db, db->GetRootUID(), UseDefaultIID());
	if(!placeGun)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ImportFileInFrame:::!placeGun");
		return kFalse;
	}
	

	UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());
	InterfacePtr<ICommand> replaceCmd(CmdUtils::CreateCommand(kReplaceCmdBoss));
	if (replaceCmd == nil)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ImportFileInFrame::replaceCmd is nil");	
		return kFalse;
	}

	InterfacePtr<IReplaceCmdData>iRepData(replaceCmd, IID_IREPLACECMDDATA);
	if(!iRepData)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ImportFileInFrame::iRepData is nil");	
		return kFalse;
	}
	
	iRepData->Set(db, imageBox.GetUID(), placedItem.GetUID(), kFalse);

	ErrorCode status = CmdUtils::ProcessCommand(replaceCmd);
	if(status==kFailure)
	{
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::ImportFileInFrame::status==kFailure");		
		return kFalse;
	}
	return kTrue;
}

void BookReportData::MoveCursorAtPosition(const UIDRef& tableUIDRef,int32 rowIndex ,int32 colIndex)
{
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break;
		}
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection ());
		if( !iSelectionManager )
		{	
			//CA("Slection NULL");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::MoveCursorAtPosition::iSelectionManager==NULL");		
			return;
		}
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(iSelectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			//CA("!layoutSelectionSuite");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::MoveCursorAtPosition::layoutSelectionSuite==NULL");		
			return;
		}
		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
			//CA("Err: invalid interface pointer ITableFrame");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::MoveCursorAtPosition::tableModel==NULL");		
			break;
		}
	
		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
		{
			//CA("Err: invalid interface pointer ITableCommands");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::MoveCursorAtPosition::tableCommands==NULL");
			break;
		}
		GridAddress  gridAddress(rowIndex,colIndex);
		
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(!pTextSel)
		{
			//CA("pTextSel is null");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::MoveCursorAtPosition::pTextSel==NULL");
			break;
		}
		InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
		if(!tblTxtSel)
		{
			//CA("tblTxtSel is null");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::MoveCursorAtPosition::tblTxtSel==NULL");
			break;
		}
		tblTxtSel->SelectTextInCell( tableModel,gridAddress);
	}while(kFalse);
}




ErrorCode  BookReportData::addReportToBook(K2Vector<IDFile> contentFileList)
{
	ErrorCode status = kFailure;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		 //CA("ptrIAppFramework == nil");			
		return status;
	}
	InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID());
	if (bookManager == nil) 
	{ 		
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::bookManager==NULL");
		return status; 
	}
	IBook * currentActiveBook = bookManager->GetCurrentActiveBook();
	if(currentActiveBook == nil)
	{		
		ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::currentActiveBook==NULL");
		return status;			
	}
	 int32  firstPageNum =-1;
	 PMString pageRangeString;
	 
	 IDFile activeBookIDFile = currentActiveBook->GetBookFileSpec();													
	do
	{
		InterfacePtr<ICommand>piCreateContentCmd(CmdUtils::CreateCommand(kConstructContentCmdBoss) );
		if (!piCreateContentCmd)
		{			
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::piCreateContentCmd==NULL");
			break;
		}	
		InterfacePtr<IBookContentCmdData>
		piBookContentCmdData(piCreateContentCmd, IID_IBOOKCONTENTCMDDATA);
		if(piBookContentCmdData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::piBookContentCmdData==NULL");
			break;
		}
		piBookContentCmdData->SetTargetBook(activeBookIDFile);
		piBookContentCmdData->SetContentFile(contentFileList);
		
		
	
		if ((status = CmdUtils::ProcessCommand(piCreateContentCmd)) !=kSuccess){
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::status ==NULL");
			break;
		}
		
		
		const UIDList *list = piCreateContentCmd->GetItemList();
		if(list == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::list ==NULL");
			break;
		}
		if(list->Length()<= 0)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::llist->Length()<= 0");
			break;
		}

		UIDList* nonConstList = const_cast<UIDList*>(list);
		

		InterfacePtr<ICommand> addDocToBookCmd(CmdUtils::CreateCommand(kAddDocToBookCmdBoss));
		if(addDocToBookCmd == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::addDocToBookCmd=s= 0");
			break;
		}
		InterfacePtr<IBookContentCmdData> bookContentCmdData(addDocToBookCmd, UseDefaultIID());
		if(bookContentCmdData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::bookContentCmdData=s= 0");
			break;
		}						
		bookContentCmdData->SetTargetBook(activeBookIDFile);
		//CA("upto last 2");
		bookContentCmdData->SetContentFile(contentFileList);
		//CA("upto last 2.1");
		bookContentCmdData->SetContentList(nonConstList);
		//CA("upto last 2.2");
		InterfacePtr<IBookContentMgr> contentMgr(currentActiveBook, UseDefaultIID());
		if(contentMgr == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::contentMgr==nil");
			break;
		}
		//CA("upto last 3.3");
		int32 destPosition = contentMgr->GetContentCount() - 1; 

		bookContentCmdData->SetDestPosition(destPosition); 
		//CA("upto last 3.5");
		status = CmdUtils::ProcessCommand(addDocToBookCmd); 
					
					
		//CA("6");
		const UIDList *addList = addDocToBookCmd->GetItemList();
		if (!(status == kSuccess && addList->Length() > 0))
		{
			//ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::contentMgr==nil");
			break;
		}
		

		UIDRef bookUIDRef = ::GetUIDRef(currentActiveBook);		
		{
			InterfacePtr<ICommand>saveBookCmd(CmdUtils::CreateCommand(kSaveBookCmdBoss));
			if(saveBookCmd == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::saveBookCmd==nil");
					break;
			}

			bookUIDRef = ::GetUIDRef(currentActiveBook);
			UIDList bookUIDList(bookUIDRef);
			saveBookCmd->SetItemList(bookUIDList);
			status = CmdUtils::ProcessCommand(saveBookCmd); 

		}

		InterfacePtr<ICommand>
		piRepaginateCmd(CmdUtils::CreateCommand(kSetRepaginationCmdBoss));
		if (!piRepaginateCmd)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::piRepaginateCmd==nil");
			break;
		}
	
		InterfacePtr<IBookPaginateOptions> piPaginateOptions(currentActiveBook,IID_IBOOKPAGINATEOPTIONS);
		IBookPaginateOptions::Options options =	piPaginateOptions->GetPaginateOptions();
		bool16 bInsert = piPaginateOptions->GetInsertBlankPage();
		bool16 bAutoRepaginate =piPaginateOptions->GetAutoRepaginateFlag();
		bool16 bForceToRepaginate = kTrue;

		InterfacePtr<IBookPaginateOptions>piPaginateCmdData(piRepaginateCmd, IID_IBOOKPAGINATEOPTIONS);
		piPaginateCmdData->SetPaginateOptions(IBookPaginateOptions::kNextPage );
		piPaginateCmdData->SetInsertBlankPage(kFalse);
		piPaginateCmdData->SetAutoRepaginateFlag(kTrue);
			
		piPaginateCmdData->SetCurrentBookUIDRef(bookUIDRef);

		InterfacePtr<IIntData> piPosition(piRepaginateCmd,IID_IINTDATA);
		piPosition->Set(destPosition);

		InterfacePtr<IBoolData> piForceRepaginate(piRepaginateCmd,IID_IBOOLDATA);
		piForceRepaginate->Set(bForceToRepaginate);

		status = CmdUtils::ProcessCommand(piRepaginateCmd);

		InterfacePtr<ICommand>saveBookCmd(CmdUtils::CreateCommand(kSaveBookCmdBoss));
		if(saveBookCmd == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::addReportToBook::saveBookCmd==nil");
				break;
		}

		bookUIDRef = ::GetUIDRef(currentActiveBook);
		UIDList bookUIDList(bookUIDRef);
		saveBookCmd->SetItemList(bookUIDList);
		status = CmdUtils::ProcessCommand(saveBookCmd); 

	}while(kFalse);	
	return status;	
}

void BookReportData::addHeaderDataOnNewPage(UIDRef& tableUIDRef /*,vector<PMString>& data*/,InterfacePtr<ITableCommands>& tblcommand/*,int32 rowstartIndex*/)
{
	//CA("BookReportData::addHeaderDataOnNewPage");

	
//PMString asd1("BookReportData::currntRowIndex = ");
//asd1.AppendNumber(BookReportData::currntRowIndex);
//CA(asd1);
//asd1.clear();
//	
//	//headerData.push_back("Current Section Name ");	
//	BookReportData::addRowInTable(tableUIDRef,1,3,20.0,183.6);
////	BookReportData::currntRowIndex++;
//
////asd1.Apped("BookReportData::currntRowIndex  ");
////asd1asd1.AppendNumber(BookReportData::currntRowIndex);
////CA(asd1);
//
//	BookReportData::MoveCursorAtPosition(tableUIDRef,++BookReportData::currntRowIndex,0);
//	WideString* wStrsecName=new WideString("Current Section Name ");			
//	tblcommand->SetCellText(*wStrsecName, GridAddress(BookReportData::currntRowIndex,0 ));	
//	delete wStrsecName;
//
//
//
//	//headerData.push_back("         ");
//	WideString* wStrsblank=new WideString("       ");			
//	tblcommand->SetCellText(*wStrsblank, GridAddress(BookReportData::currntRowIndex,1 ));	
//	delete wStrsblank;
//
//
////	headerData.push_back(BookReportData::getCurrentBookName());
//	PMString asd("Book Name : ");
//	asd.Append(BookReportData::getCurrentBookName());
//	WideString* wStrsbkname=new WideString(asd);			
//	tblcommand->SetCellText(*wStrsbkname, GridAddress(BookReportData::currntRowIndex,2 ));	
//	delete wStrsbkname;
//
//	BookReportData::addRowInTable(tableUIDRef,1,3,20.0,183.6);
//	BookReportData::currntRowIndex++;	
//	
//	PMString dummy("");
//	
//	if(BookReportData::reportType == 0)
//		dummy.Append("Updates");
//	else if(BookReportData::reportType == 1)
//		dummy.Append("New");
//	else
//		dummy.Append("Deletes");
//
//
//	WideString* wStrRefreshType=new WideString(dummy);			
//	tblcommand->SetCellText(*wStrRefreshType, GridAddress(BookReportData::currntRowIndex,0 ));	
//	delete wStrRefreshType;
//	
//	
//	BookReportData::addRowInTable(tableUIDRef,1,3,20.0,183.6);
//	BookReportData::currntRowIndex++;
//
//	WideString* wStrfield=new WideString("Fi");			
//	tblcommand->SetCellText(*wStrfield, GridAddress(BookReportData::currntRowIndex,0 ));	
//	delete wStrfield;
//
//	
//	
////	headerData.push_back("    ");
////	headerData.push_back("    ");
//	
//	BookReportData::addRowInTable(tableUIDRef,1,3,20.0,183.6);
//	BookReportData::currntRowIndex++;	

//	headerData.push_back("Field ");
//	headerData.push_back("Document Value ");
//	headerData.push_back("ONEsource Value ");
	//**********
	
	//for(int32 rowindex=rowstartIndex ;rowindex <= BookReportData::currntRowIndex ;rowindex++ )
	//{	
	//	//CA("Inner Loop");
	//	int curRow=3;
	//	for(int32 colIndex=0 ;colIndex < 3 ;colIndex++,curRow-- )
	//	{
	//		PMString str = headerData[index++];
	//		CA("Value "+str);
	//		BookReportData::MoveCursorAtPosition(tableUIDRef,BookReportData::currntRowIndex-curRow,colIndex);
	//		WideString* wStrcolData=new WideString(str);			
	//		tblcommand->SetCellText(*wStrcolData, GridAddress(rowindex,colIndex ));	
	//		delete wStrcolData;
	//	}
	//}
}

bool16 BookReportData::isRefreshReportExistInBook(PMString& docName)
{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			return kFalse;
		}
		InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID()); //Cs4
		if (bookManager == nil) 
		{ 
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::isRefreshReportExistInBook::bookManager==nil");
			return kFalse; 
		}
		
		IBook * activeBook = bookManager->GetCurrentActiveBook();
		if(activeBook == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::isRefreshReportExistInBook::activeBook==nil");
			return kFalse;			
		}

		InterfacePtr<IBookContentMgr> bookContentMgr(activeBook, UseDefaultIID());
		if (bookContentMgr == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::isRefreshReportExistInBook::bookContentMgr==nil");
			return kFalse;
		}
		PMString bookName = activeBook->GetBookTitleName();
		bookName.Append(BookReportData::getCurrentBookName());

		bookName.Append("_Refresh_Report.indd");
		
		K2Vector<PMString> mybookContentNames = BookReportData::GetBookContentNames(bookContentMgr);
			
		int32 bookContentsCount = mybookContentNames.size();
			
		if (bookContentsCount < 0)
		{
			//CA("This book doesn't have any content!");
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::isRefreshReportExistInBook::bookContentsCount < 0");
			return kTrue;
		}
		

		for(int32 bkIndex=0 ;bkIndex<= bookContentsCount; bkIndex++ )
		{
			//PMString asf("MyBookName = ");
			//asf.Append(mybookContentNames[bkIndex]);
			//CA(asf);
			if(bookName.Compare(kTrue,mybookContentNames[bkIndex]) ==0 )
			{
				//asf.clear();
				//asf.Append(mybookContentNames[bkIndex]+ " == "+bookName);
				return kTrue;
			}
		}
		return kFalse;
}

//*********
K2Vector<PMString> BookReportData::GetBookContentNames(IBookContentMgr* bookContentMgr)
{
	K2Vector<PMString> bookContentNames;
	do {
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break;
		}
		if (bookContentMgr == nil) 
		{
			//ASSERT(bookContentMgr);
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::GetBookContentNames::bookContentMgr==nil");
			break;
		}

		// get the book's database (same for IBook)
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		if (bookDB == nil) 
		{
			//ASSERT_FAIL("bookDB is nil - wrong database?"); 
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::GetBookContentNames::bookDB==nil");
			break;
		}

		int32 contentCount = bookContentMgr->GetContentCount();
		for (int32 i = 0 ; i < contentCount ; i++) 
		{
			UID contentUID = bookContentMgr->GetNthContent(i);
			if (contentUID == kInvalidUID) 
			{
				// somehow, we got a bad UID
				continue; // just goto the next one
			}
			
			//*****Added for Cs4
			InterfacePtr<IBookContent> bookContent(bookDB, contentUID, UseDefaultIID());
			if (bookContent == nil) 
			{			
				ptrIAppFramework->LogDebug("AP7_RefreshContent::BookReportData::GetBookContentNames::bookContent==nil");
				break; 
			}
			PMString baseName = bookContent->GetShortName();
			
			bookContentNames.push_back(baseName);			
		}

	} while (false);
	return bookContentNames;
}
//**********
void BookReportData::fitImageInBox(const UIDRef& boxUIDRef, bool16 isInterFaceCall)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;
	do 
	{
		InterfacePtr<IHierarchy> iHier(boxUIDRef, IID_IHIERARCHY);
		if(!iHier)
		{
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::fitImageInBox::!iHier ");
			return;
		}

		if(iHier->GetChildCount()==0)
		{
			//There may not be any image at all????
			return;
		}

		UID childUID=iHier->GetChildUID(0);
		UIDRef newChildUIDRef(boxUIDRef.GetDataBase(), childUID);
		if(isInterFaceCall == kFalse)
		{
			InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
			if(!iAlignCmd)
			{
				ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::fitImageInBox::!iAlignCmd ");
				return;
			}			
			iAlignCmd->SetItemList(UIDList(newChildUIDRef));
			CmdUtils::ProcessCommand(iAlignCmd);
		}
		else
		{
			InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentToFrameCmdBoss));
			if(!iAlignCmd)
			{
				ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::fitImageInBox::!iAlignCmd ");
				return;
			}

			iAlignCmd->SetItemList(UIDList(newChildUIDRef));
			CmdUtils::ProcessCommand(iAlignCmd);
		}	
	} while (false);
}

PMString BookReportData::getcurrentSectionName(){return BookReportData::currentSection; }
void BookReportData::setcurrentSectionName(PMString sacName){BookReportData::currentSection = sacName;}

void BookReportData::removeDashFromString(PMString& name)
{
	PMString strDummy;
	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{		
		PlatformChar ch = name.GetChar(i);		
		
		if(ch == '_')
		{			
			strDummy.Append(' ');
		}
		else
			strDummy.Append(ch);
	}
	name = strDummy;	
}

void BookReportData::SetCellStroke(const Tables::ESelectionSides sides, const PMReal strokeWeight,const UIDRef& tableUIDRef,const ITableModel *tableModel)
{
	//CA(__FUNCTION__);
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break ;
		}
		InterfacePtr<ITableSuite> tableSuite(static_cast<ITableSuite*>
											 (Utils<ISelectionUtils>()->QuerySuite(ITableSuite::kDefaultIID))); 
		if (tableSuite == nil)
		{
			//CA("tableSuite");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::SetCellStroke::tableSuite == nil ");
			break;
		}

		// Be extremely paranoid; we shouldn't be in here if we can't do this but check again.
		if (!tableSuite->CanApplyCellOverrides())
		{
			//CA("!tableSuite->CanApplyCellOverrides()");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::SetCellStroke::!tableSuite->CanApplyCellOverrides()");
			break;
		}
		//*** Left
		ICellStrokeAttrData::Data dataLeft;
		dataLeft.sides = Tables::eLeftSide;/*sides*/   // Tables::eLeftSide; Tables::eRightSide; Tables::eTopSide; Tables::eBottomSide;

		PMReal dummy(0.25);
		dataLeft.attrs.Set(ICellStrokeAttrData::eWeight);
		dataLeft.weight = dummy/*strokeWeight*/;

		//****Right
		ICellStrokeAttrData::Data dataRight;
		dataRight.sides = Tables::eRightSide;
		dataRight.attrs.Set(ICellStrokeAttrData::eWeight);
		dataRight.weight = dummy;

		//****Top
		ICellStrokeAttrData::Data dataTop;
		dataTop.sides = Tables::eTopSide;
		dataTop.attrs.Set(ICellStrokeAttrData::eWeight);
		dataTop.weight = dummy;

		//****Bottom
		ICellStrokeAttrData::Data dataBottom;
		dataBottom.sides = Tables::eBottomSide;
		dataBottom.attrs.Set(ICellStrokeAttrData::eWeight);
		dataBottom.weight = dummy;

		bool16 canApplyStrokesLeft  = tableSuite->CanApplyCellStrokes(dataLeft);
		bool16 canApplyStrokesRight = tableSuite->CanApplyCellStrokes(dataRight);
		bool16 canApplyStrokesBottom= tableSuite->CanApplyCellStrokes(dataBottom);
		bool16 canApplyStrokesTop   = tableSuite->CanApplyCellStrokes(dataTop);
		

		if (canApplyStrokesLeft == kTrue
			&& canApplyStrokesRight == kTrue
			&& canApplyStrokesBottom == kTrue
			&& canApplyStrokesTop == kTrue)
		{

			tableSuite->ApplyCellStrokes(dataLeft);
			tableSuite->ApplyCellStrokes(dataRight);
			tableSuite->ApplyCellStrokes(dataTop);
			tableSuite->ApplyCellStrokes(dataBottom);							
			{							
				//CA("tableSuite->GetCellStrokesBySelection();my COUNDITION>>>");	
				InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));
				if(!tblSelSuite)
				{
					ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::SetCellStroke::!tblSelSuite");
					break;
				}
				tblSelSuite->SelectBodyColumns(0,2,tableModel, kTrue,kTrue);	
	
				GridArea grdAreaTable;
				InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
				if(tableCommands==nil)
				{
					//CA("AP7_RefreshContent::BookReportData::SetCellStroke::tableCommands==nil");		
					ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::SetCellStroke::!tableCommands");
					break;
				}
				InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
				if (tableModel == NULL)
				{
					//CA("Err: tableModel == NULL");
					ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::SetCellStroke::!tableModel");
					break;
				}
				grdAreaTable=tblSelSuite->GetSelection() ; //tableModel->GetBodyArea();		
				tableCommands->ApplyCellStrokes(grdAreaTable,dataLeft);
				tableCommands->ApplyCellStrokes(grdAreaTable,dataRight);
				tableCommands->ApplyCellStrokes(grdAreaTable,dataTop);
				tableCommands->ApplyCellStrokes(grdAreaTable,dataBottom);
				
				int32 totalBodyRows = tableModel->GetTotalRows().count;
				for(int32 rowindx =0; rowindx < totalBodyRows;rowindx++ )
				{
					BookReportData::selectTableBodyRows(tableModel,rowindx,1);	
					grdAreaTable=tblSelSuite->GetSelection() ; 	
					tableCommands->ApplyCellStrokes(grdAreaTable,dataLeft);
					tableCommands->ApplyCellStrokes(grdAreaTable,dataRight);
					tableCommands->ApplyCellStrokes(grdAreaTable,dataTop);
					tableCommands->ApplyCellStrokes(grdAreaTable,dataBottom);
				}	

				int32 totalBodyCols= tableModel->GetTotalCols().count;				
				for(int32 colindx =0; colindx < totalBodyCols;colindx++ )
				{
					BookReportData::selectTableBodyColumns(tableModel,colindx,1);						
					grdAreaTable=tblSelSuite->GetSelection() ;
					tableCommands->ApplyCellStrokes(grdAreaTable,dataLeft);
					tableCommands->ApplyCellStrokes(grdAreaTable,dataRight);
					tableCommands->ApplyCellStrokes(grdAreaTable,dataTop);
					tableCommands->ApplyCellStrokes(grdAreaTable,dataBottom);
				}
			}
		}
		else
		{
			//CA("Could not apply cell strokes!");
		}
	}while(kFalse);	
	//CA("Done not apply cell strokes!");
}

ErrorCode BookReportData::ChangeFontSizeOfSelectedTextInFrame(int32 fontSize)
{
	//CA(__FUNCTION__);
	ErrorCode status = kFailure;
	do {
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break ;
		}
		InterfacePtr<ISelectionManager> iSelectionMnger(Utils<ISelectionUtils>()->QueryActiveSelection());
		if (iSelectionMnger==nil)
		{
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::ChangeFontSizeOfSelectedTextInFrame::!iSelectionMnger");
			break;
		}
		
		InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
		( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionMnger))); 
		if(!txtMisSuite)
		{		
			//CA("!txtMisSuite");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::ChangeFontSizeOfSelectedTextInFrame::!txtMisSuite");
			break;
		}						
		//To change properties of the text in the selection
		InterfacePtr<ITextAttributeSuite> textAttr(txtMisSuite,UseDefaultIID());
		if(textAttr == NULL)
		{
			//CA("textAttr == NULL");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::ChangeFontSizeOfSelectedTextInFrame::!textAttr");
			break;
		}
		
		// First, we create a text attribute real number to hold the font size:	
		InterfacePtr<ITextAttrRealNumber>textAttrRealNumber(::CreateObject2<ITextAttrRealNumber>(kTextAttrPointSizeBoss));
		if (textAttrRealNumber == nil)
		{
			//CA("textAttrRealNumber invalid");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::ChangeFontSizeOfSelectedTextInFrame::!textAttrRealNumber");
			break;
		}

		// Now we'll set the attribute's value:
		const PMReal font_size(10); // The size we want to change to.			
		textAttrRealNumber->SetRealNumber(font_size);
		textAttr->ApplyAttribute(textAttrRealNumber);		
		status = kSuccess;
	}while(false);
	return status;
}



void BookReportData::selectTableBodyColumns(ITableModel *tableModel,int32 startcolumn,int32 colsToSelect)
{
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break ;
		}
		InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
		if(!tblSelSuite)
		{
			//CA(" Source Table Selection suite is null");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::selectTableBodyColumns::!tblSelSuite");
			break;
		}
		tblSelSuite->SelectBodyColumns(startcolumn,colsToSelect,tableModel, kFalse,kTrue);
	
	}while(kFalse);
}
//**** CAn S Change the Font Type : Regular , Italic / Bold Blah blah... 
ErrorCode BookReportData::setFontForSelectedTable(const UIDRef& tableUIDRef,ITableModel* tblModel,ITextModel *txtModel,PMString fontName)
{
	ErrorCode result = kFailure;
	fontName.clear();
	fontName = "Myriad Pro";//"MyriadPro-Cond";
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break ;
		}
		InterfacePtr<ISelectionManager> iSelectionManager1 (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager1)
		{ 
			//CA("!!!CurrentSelection...");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::!iSelectionManager1");
			return result;
		}		
		InterfacePtr<ITableSelectionSuite> tableSelectionSuite(iSelectionManager1, UseDefaultIID());
		if (!tableSelectionSuite) 
		{	
			//CA("!!!tableSelectionSuite...");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::!tableSelectionSuite");
			return result;			
		}		
		InterfacePtr<ITextAttributeSuite> suite(iSelectionManager1, UseDefaultIID());
		if (suite == nil)
		{
			//CA("You do not have a text attribute suite. Exiting...");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::!suite");
			return result;
		}		
		
		//if (!textAttributeSuite->CanApplyAttributes())
		//{
		//	CA("Can't apply attributes.");
		//	break;
		//}
		//
		//// query the text target from the current context
		//InterfacePtr<ITextTarget> textTarget(iSelectionManager, UseDefaultIID());
		//if (textTarget == nil) 
		//{
		//	CA("textTarget == nil ");
		//	break;
		//}
		//// query the text focus (bridge method)
		//InterfacePtr<ITextFocus> textFocus(textTarget->QueryTextFocus());
		//if (textFocus == nil) 
		//{
		//	CA("textFocus == nil");
		//	break;
		//}

		//// Get the text range
		//InDesign::TextRange textRange(textFocus);
		//if (textRange.IsValid() == kFalse)
		//{
		//	CA("textRange.IsValid() == kFalse");
		//	break;
		//}
		PMString localTypeStyle(fontName);
		localTypeStyle.SetTranslatable(kFalse);
		
		int32 nFonts = suite->CountFonts();
		//PMString aa("nFonts = ");
		//aa.AppendNumber(nFonts);
		//CA(aa);
        for (int32 i = 0; i < nFonts ; i++)
        {
			if (suite->GetNthFont(i) == localTypeStyle)
			{		
				// Here's the tricky part: if there is one instance of the variant in the 
				// current text selection, and it's not the font style we're trying to apply, then we don't
				// want to remove it.  If the font style does match, we want to toggle the style
				// be setting it to "normal" type style, which is done with an empty string
				localTypeStyle = "Regular";
				localTypeStyle.SetTranslatable(kFalse);
			//	CA("Lacale Type = "+localTypeStyle);

				break; // out of for-loop
			}
//			CA("Nth Font : "+suite->GetNthFont(i)); 
		}

		InterfacePtr<ITextAttrFont>	attrFont(::CreateObject2<ITextAttrFont>(/*kTextAttrFontUIDBoss*/ kTextAttrFontStyleBoss));
		if (attrFont == nil){
			//CA("attrFont == nil");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::!attrFont == nil");
			return result;
		}

//CA("Befor Set Font Name = "+attrFont->GetFontName());
		// set the font style.
//		attrFont->SetFontName(localTypeStyle, kFalse);
//CA("After Font Name = "+attrFont->GetFontName());
		// apply the attribute
//		result = suite->ApplyAttribute(attrFont);		

//		if(result){ 
//			CA("Apply Attributes is Successfull.");
//		}
//		else
//			CA("Apply Attributes is FAiled");
				
		//**** Setting Font GroupName
		IActiveContext* ac =GetExecutionContextSession()->GetActiveContext(); //Cs4
		/*IDataBase *pDB = ::GetDataBase( tblModel );
		InterfacePtr<IDocument>pDocument( pDB, pDB->GetRootUID(), UseDefaultIID() );
		if(!pDocument)
		{
			CA("pDocument == NULL");
			break;
		}*/
		/*InterfacePtr<IWorkspace>iWorkspace( pDocument->GetDocWorkSpace(), UseDefaultIID() );
		if(!iWorkspace)
		{
			CA("pWorkspace == NULL");
			break;
		}*/
		IWorkspace* iWorkspace = ac->GetContextWorkspace();
		
		InterfacePtr<IDocFontMgr> iDocFontMgr(iWorkspace,UseDefaultIID());
		if (iDocFontMgr == nil){
			//CA("iDocFontMgr == nil");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::iDocFontMgr == nil");
			break;
		}
		
		int32 numPersistFontGroups = iDocFontMgr->GetFontGroupCount();

		//PMString asd("numPersistFontGroups = ");
		//asd.AppendNumber(numPersistFontGroups);
		//CA(asd);
		//IFontFamily::FontFaceMappingInfo  strFontFaceMappingInfo ; 
		//strFontFaceMappingInfo.fPlatformName = "Myriad Pro";
		//strFontFaceMappingInfo.fFontFaceName = "Myriad Pro";
		//strFontFaceMappingInfo.fStyle = IFontMgr/*::FontStyleBits*/::kItalic; 
		//strFontFaceMappingInfo.fOS = IFontMgr::kOriginalWinOS ;

		for (int32 loop = 0; loop < numPersistFontGroups; loop++){

			UID fontFamily = iDocFontMgr->GetNthFontGroupUID(loop);
			
			InterfacePtr<IFontFamily> iFontFamily(::GetDataBase(iWorkspace),fontFamily,UseDefaultIID());
			if (iFontFamily == nil){
				ASSERT(iFontFamily);
				ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::iFontFamily == nil");
				break;
			}
	
			//PMString ft("Befor Font FamilyName = ");
			//ft.Append(iFontFamily->GetFamilyName());
			//ft.Append("\nBefor Font FamilyName = ");
			//ft.Append(iFontFamily->GetDisplayFamilyName());		
			//ft.Append("\nFamilyNameNative = ");
			//ft.Append(iFontFamily->GetFamilyNameNative());
			//CA(ft);
			//BookReportData::selectTableBodyColumns(tblModel,0,3);
//			iFontFamily->SetFamilyName("MyriadPro-Cond","Myriad Pro",kFalse);
//			ft =iFontFamily->GetFamilyName();
//			CA("After Font = "+ft);		
//			iFontFamily->SetFontFaceMappingInfo(strFontFaceMappingInfo);
//CA("After strFontFaceMappingInfo");
		}

		//*****************
		//IDataBase *pDB = ::GetDataBase( tblModel );
		//InterfacePtr<IDocument>document( pDB, pDB->GetRootUID(), UseDefaultIID() );
		//if(!document)
		//{
		//	CA("pDocument == NULL");
		//	break;
		//}	
		//InterfacePtr<IStyleGroupManager> styleNameTable_para(document->GetDocWorkSpace(), IID_IPARASTYLEGROUPMANAGER);
		//if (styleNameTable_para == nil)
		//{
		//	CA("styleNameTable_para invalid");
		//	break;
		//}
		//UID styleUID = kInvalidUID;
		//styleUID = styleNameTable_para->FindByName("Regular"/*styleName*/);
		//if(styleUID == kInvalidUID)
		//	CA("Style Not Found");
		//else
		//	CA("Style Found.");
		//ErrorCode result = suite->ApplyStyle(styleUID);
		//if(result == kSuccess)
		//	CA("Apply dsduccesss");
		//else
		//	CA("Apply Fails");
	
		//***************************
		
		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
		if(!iSelectionManager)
		{	//CA("AP7_CatalogIndex::CreateIndex::CreatePageEntry::!iSelectionManager");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::iSelectionManager == nil");
			break;
		}

		InterfacePtr<ITextSelectionSuite> txtSelSuite(Utils<ISelectionUtils>()->QueryActiveTextSelectionSuite()/*,UseDefaultIID()*/);//Amit
		if(!txtSelSuite)
		{
			//CA("AP7_CatalogIndex::CreateIndex::CreatePageEntry::Text Selection Suite is null");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::txtSelSuite == nil");
			break;
		}
		RangeData range = txtModel->GetStoryThreadRange(0);
		const UIDRef TextModelUIDRef = ::GetUIDRef(txtModel);
		txtSelSuite->SetTextSelection(TextModelUIDRef,/*rData*/range,Selection::kScrollIntoView/*kDontScrollSelection*/ ,nil);
		InterfacePtr<IConcreteSelection>pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if (pTextSel == nil)
		{
			//CA("AP7_CatalogIndex::CreateIndex::CreatePageEntry::pTextSel is nil");	
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::pTextSel == nil");
			break;
		}

		InterfacePtr<ITextTarget> textTarget( pTextSel,UseDefaultIID());
		if (textTarget == nil) 
		{
			//CA("You do not have a text selection.  Exiting...");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::textTarget == nil");
			break;
		}
		RangeData range1 = textTarget->GetRange();
		// query the compose scanner so we can query attributes
		InterfacePtr<IComposeScanner> composeScanner(txtModel, UseDefaultIID());
		if (composeScanner == nil) 
		{
			//CA("composeScanner == nil"); 
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::composeScanner == nil");
			break;
		}
		// get the font family UID
		InterfacePtr<const IAttrReport> fontUIDAttrReport(composeScanner->QueryAttributeAt(range, kTextAttrFontUIDBoss));
		if (fontUIDAttrReport == nil)
		{
			//CA("fontUIDAttrReport == Nil"); 
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::fontUIDAttrReport == nil");
			break;
		}
		InterfacePtr<ITextAttrUID> fontUIDAttr(fontUIDAttrReport,UseDefaultIID());
		if (fontUIDAttr == nil)
		{
			//CA("fontUIDAttr == nil");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::fontUIDAttr == nil");
			break;
		}
		UID fontUID = fontUIDAttr->Get();
//CA("After Stting Font");
//		fontUIDAttr->Set(fontUID);
		// get the font style name
		InterfacePtr<const IAttrReport> fontStyleAttrReport(composeScanner->QueryAttributeAt(range, kTextAttrFontStyleBoss));
		if (fontStyleAttrReport == nil)
		{
			//CA("!!fontStyleAttrReport== NIL");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::fontStyleAttrReport == nil");
			break;
		}
		InterfacePtr<ITextAttrFont> fontStyleAttr(fontStyleAttrReport, UseDefaultIID());
		if (fontStyleAttr == nil)
		{
			//CA("Missing ITextAttrFont in QueryFont()  ==  NIL");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::fontStyleAttr == nil");
			break;
		}
		// get the font family
        InterfacePtr<IFontFamily> fontFamily(::GetDataBase(txtModel), fontUID, UseDefaultIID());
		if (fontFamily == nil)
		{
			//CA("fontFamily == NIl"); 
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::setFontForSelectedTable::fontFamily == nil");
			break;
		}

//		PMString fontStyleName = fontStyleAttr->GetFontName();
		//CA("Font Style Name Befor = "+fontStyleName);

//		fontStyleAttr->SetFontName("MyriadPro-BoldCond");
//		fontStyleName = fontStyleAttr->GetFontName();

//CA("Font Style Name After = "+fontStyleName);
		
		//****************

//		InterfacePtr<ITextAttributes> defaultTextAttributes(iWorkspace, UseDefaultIID());
//		if (defaultTextAttributes == nil) 
//		{
//			CA("Can't find ITextAttributes on the workspace boss! Using blank defaults.");
//			break; // out of do-while
//		}
//		InterfacePtr<ITextAttrUID> defaultFontUID((ITextAttrUID*)defaultTextAttributes->QueryByClassID(kTextAttrFontUIDBoss, ITextAttrUID::kDefaultIID));
//		if (defaultFontUID == nil) 
///		{
//			CA("Can't find kTextAttrFontUIDBoss on the default text attributes! Using blank defaults.");
//			break; // out of do-while
//		}
//		InterfacePtr<ITextAttrFont> defaultFontStyle((ITextAttrFont*)defaultTextAttributes->QueryByClassID(kTextAttrFontStyleBoss, ITextAttrFont::kDefaultIID));
//		if (defaultFontStyle == nil) 
//		{
//			CA("Can't find kTextAttrFontStyleBoss on the default text attributes! Using blank defaults.");
//			break; // out of do-while MyriadPro-Regular
//		}
//		CA("After Setting Font Name=  "+defaultFontStyle->GetFontName());
//		defaultFontStyle->SetFontName("MyriadPro-Cond");
//CA("From HEre");
//		InterfacePtr<IFontMgr> fontMgr(GetExecutionContextSession(), UseDefaultIID());
//		if (fontMgr == nil)
//		{
//			CA("WatermarkDrwEvtHandler::DrawWatermark: fontMgr invalid");
//			return kFalse;
//		}
//		PMString fontStyle="Regular";
//		WideString wstr("Myriad Pro");
//		InterfacePtr<IPMFont> font(fontMgr->QueryFontFromDisplayName(wstr,fontStyle));
//		if (font == nil ) {
//			// Try getting the default font instead
//			font = fontMgr->QueryFont(fontMgr->GetDefaultFontName());
//		}
//		int32 fontcount = fontMgr->GetNumFontGroups();
//		if(font){
//			CA("Font is Exist.");
//			
//			PMString ad("Font Count = ");
//			ad.AppendNumber(fontcount);
//			CA(ad);	
//		
//		}
//		
//		
//		for(int32 i =0; i < fontcount; i++){
//			InterfacePtr<IFontGroup>fontGroup(fontMgr->QueryFontGroup(i));
//			if (fontGroup == nil) {
//				break;
//			}
//			//CA("Font Name =  "+fontGroup->GetGroupName());	
//			if(fontName.Compare(kTrue,fontGroup->GetGroupName())){
//				//CA("Myriad Pro Found ");
//				PMString realFamilyName("Myriad Pro");
//				font->AppendFamilyName(realFamilyName);
//				font->AppendFamilyName(realFamilyName);
//							
//				PMString realStyleName("Regular");
//				font->AppendStyleName(realStyleName);
//				//CA("After Done");
//			}
//		}
//		InterfacePtr<IFontInstance> fontInst(fontMgr->QueryFontInstance(font, fontMatrix));
//		if (fontInst == nil)
//		{
//			CA("Nil IFontInstance*");
//			return kFalse; 
//		}
	
		// query the compose scanner so we can query attributes
		//InterfacePtr<IComposeScanner> composeScanner(txtModel, UseDefaultIID());
		//if (composeScanner == nil) 
		//{
		//	CA("composeScanner == nil"); break;
		//}
		
		// get the font family UID

//		InterfacePtr<const IAttrReport> fontUIDAttrReport(composeScanner->QueryAttributeAt(100.0, kTextAttrFontUIDBoss));
//		if (fontUIDAttrReport == nil)
//		{
//			CA("fontUIDAttrReport == Nil"); break;
//		}
//		InterfacePtr<ITextAttrUID> fontUIDAttr(fontUIDAttrReport, UseDefaultIID());
//		if (fontUIDAttr == nil)
//		{
//			CA("fontUIDAttr ==  nil");
//			break;
//		}	
//		UID fontUID = fontUIDAttr->Get();
//		if(fontUID != kInvalidUID)
//				CA( "fontUID is invalid");
//		else
//			CA("Font is Valid.");

	}while(kFalse);
		return result;
}


void BookReportData::fillVecNewReportData()
{
//	CA("BookReportData::fillVecNewReportData");	
	BookReportData::newItemidList.clear();
	vector<double> databaseItemID;
	vector<TagList>::iterator itrTagList;
	vector<vector<TagList> >::iterator itrEntirebkTgList;
	static int32 i=0;

	VectorPubObjectValuePtr ptrVectorFamilyInfoValuePtr;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)			
		return;			
	do{
		double elementID=-1 ;	
//		for(itrEntirebkTgList = BookReportData::entireBookTagList.begin();itrEntirebkTgList != BookReportData::entireBookTagList.end();itrEntirebkTgList++)
		{
//			BookReportData::documentTagList = *itrEntirebkTgList;
			for(itrTagList = BookReportData::documentTagList.begin();itrTagList!= BookReportData::documentTagList.end();itrTagList++)
			{
				TagList	tList=*itrTagList;
				//PMString hh("tList[0].sectionID = ");
				//hh.AppendNumber(tList[0].sectionID);
				//CA(hh);
				ptrVectorFamilyInfoValuePtr=ptrIAppFramework->getProductsAndItemsForSection(tList[0].sectionID,tList[0].languageID);
				if(ptrVectorFamilyInfoValuePtr)	{						
					if(BookReportData::ielementID.size() == 0 ){
						for(int32 i=0 ; i < tList.size(); i++)
						{
							BookReportData::ielementID.push_back(tList[i].elementId);
						}
					}
					break; 				
				}
			}
			//PMString asd("New Item Object ID  - ");
			
			if(ptrVectorFamilyInfoValuePtr){
				VectorPubObjectValue::iterator it2;
				for(it2 = ptrVectorFamilyInfoValuePtr->begin(); it2 != ptrVectorFamilyInfoValuePtr->end(); it2++)	
				{
					BookReportData::newItemidList.push_back(it2->getObject_id());
					//PMString asd("New Item Object ID  - ");
					//asd.Append("\nNew Item Object ID  - ");
					//asd.AppendNumber(it2->getObject_id());
				}
			}	
			//CA(asd);
			//PMString asd1("BookReportData::newItemidList..size == ");
			//asd1.AppendNumber(static_cast<int32>(BookReportData::newItemidList.size()));
			//CA(asd1);
			vector<double>::iterator itrNewItemList1;			
			for(itrTagList = BookReportData::documentTagList.begin();itrTagList!= BookReportData::documentTagList.end();itrTagList++)
			{
				/*if(BookReportData::documentTagList.size()== 0)
					break;*/
				TagList tList = *itrTagList;
				for(itrNewItemList1=BookReportData::newItemidList.begin();itrNewItemList1!=BookReportData::newItemidList.end();itrNewItemList1++)
				{
					if(BookReportData::newItemidList.size()==0)	{
						//CA("BookReportData::newItemidList.size()==0");
						break;
					}
					if(tList[0].typeId == *itrNewItemList1)	
					{
						//CA("ccc");
						BookReportData::newItemidList.erase(itrNewItemList1);
//						elementID = tList[0].elementId;
//						itrNewItemList1=BookReportData::newItemidList.begin();
//						PMString asd("Erase From New Item Object ID  - ");
//						asd.AppendNumber(*itrNewItemList1);
//						CA(asd);
					}
				}
			}
		}		
		if(BookReportData::newItemidList.size() > 0){
			BookReportData::entireNewItemList.push_back(BookReportData::newItemidList);
			BookReportData::ielementID.push_back(elementID);
		}

//		PMString asd("Entire List Size = ");
//		asd.AppendNumber(static_cast<int32>(BookReportData::newItemidList.size()));
//		CA(asd);

		BookReportData::newItemidList.clear();
	}while(kFalse);	

	if(ptrVectorFamilyInfoValuePtr)
		delete ptrVectorFamilyInfoValuePtr;
}
void BookReportData::fillDocumentTagList(UIDRef& boxID, PMString& imagePath, bool16 isInterFaceCall)
{
	TagList tList,tList_checkForHybrid;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)			
		return;	
	do{
		//ptrIAppFramework->clearAllStaticObjects();

		InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
		{ 
			ptrIAppFramework->LogDebug("AP7_RefreshContent::BookRportData::fillVecNewReportData::tagreader nil ");
			return;
		}
		if(!isInterFaceCall)
		{
			//CA("!isInterFaceCall");			
			tList_checkForHybrid = itagReader->getTagsFromBox_ForRefresh(boxID);
			if(tList_checkForHybrid.size() == 0)
			{
				break;
			}
			if(refreshTableByAttribute /*&& tList_checkForHybrid[0].tableType != 3*/){
				//CA("True");
				tList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxID);
			}
			else{
				//CA("False");
				tList=itagReader->getTagsFromBox_ForRefresh(boxID);
			}
		}
		else
			tList=itagReader->getTagsFromBox_ForRefresh(boxID);

		if(tList.size()==0)
		{	
			//CA(" Refresh::refreshThisBox  tList.size()==0 ");
			tList=itagReader->getFrameTags(boxID);
			if(tList.size()==0)
			{
				ptrIAppFramework->LogInfo("AP7_RefreshContent::BookReportData::fillDocumentTagList::tList.size()==0");		
				break;
			}
			//CA("11.5");
//			refreshTaggedBox(boxID, imagePath, isInterFaceCall);
			
		}	
		if(tList.size() > 0){
			BookReportData::documentTagList.push_back(tList);			
		}

		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		for(int32 tagIndex = 0 ; tagIndex < tList_checkForHybrid.size() ; tagIndex++)
		{
			tList_checkForHybrid[tagIndex].tagPtr->Release();
		}
	}while(kFalse);	
}
bool16 BookReportData::isFileExist(const PMString path,const PMString name)
{
	PMString theName("");
	theName.Append(path);
	theName.Append(name);
    
    #ifdef MACINTOSH
    {
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == NULL)
        {
            //CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
            return kFalse;
        }
        
        ptrIAppFramework->getUnixPath(theName);
    }
    #endif

	const char *file=theName.GetPlatformString().c_str()/*GrabCString()*/;

	FILE *fp=NULL;

	fp=std::fopen(file, "r");
	if(fp!=NULL)
	{
		std::fclose(fp);
		return kTrue;
	}
	return kFalse;
}

void BookReportData::changeTableStrokeWeight(const PMReal& strokeWeight)
{
	InterfacePtr<IApplyMultAttributesCmdData> fIApplyMultAttributesCmdData;
	//CA(__FUNCTION__);
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break ;
		}
		InterfacePtr<IGraphicAttrRealNumber> strokeWeightAttribute(Utils<IGraphicAttributeUtils>()->CreateStrokeWeightAttribute(strokeWeight));
		if(!strokeWeightAttribute)			
		{
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::changeTableStrokeWeight::strokeWeightAttribute == nil");
			break;	
		}
		if (fIApplyMultAttributesCmdData == nil) {
			// Create a command that can change graphic attributes.
			//CA("Create a command that can change graphic attributes.");
			InterfacePtr<ICommand> gfxApplyMultAttributesCmd(CmdUtils::CreateCommand(kGfxApplyMultAttributesCmdBoss));
			ASSERT(gfxApplyMultAttributesCmd);
			if (!gfxApplyMultAttributesCmd) {
				//CA("!gfxApplyMultAttributesCmd");
				ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::changeTableStrokeWeight::gfxApplyMultAttributesCmd == nil");
				break;
			}
			InterfacePtr<IApplyMultAttributesCmdData> applyMultAttributesCmdData(gfxApplyMultAttributesCmd, UseDefaultIID());
			ASSERT(applyMultAttributesCmdData);
			if (!applyMultAttributesCmdData) {
				//CA("!applyMultAttributesCmdData");
				ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::changeTableStrokeWeight::applyMultAttributesCmdData == nil");
				break;
			}		
			fIApplyMultAttributesCmdData = applyMultAttributesCmdData;
		}
		fIApplyMultAttributesCmdData->AddAnAttribute(strokeWeightAttribute);	
	}while(kFalse);		
}

void BookReportData::selectTableBodyRows(ITableModel *tableModel,int32 startrows,int32 rowsToSelect)
{
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break ;
		}
		InterfacePtr<ITableSelectionSuite>tblSelSuite(static_cast<ITableSelectionSuite *>(Utils<ISelectionUtils>()->QueryActiveTableSelectionSuite()));//,UseDefaultIID());
		if(!tblSelSuite)
		{
			//CA(" Source Table Selection suite is null");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::selectTableBodyRows::tblSelSuite == nil");
			break;
		}
		tblSelSuite->SelectRows(startrows,1/*rowsToSelect*/,tableModel, kTrue,kTrue);
	
	}while(kFalse);
}
void BookReportData::deSelectAllPAgeItems()
{
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break ;
		}
		InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
		if (selectionManager == nil)
		{
			//CA("selectionManager == nil");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::deSelectAllPAgeItems::selectionManager == nil");
			break;
		}
		// Deselect everything.
		selectionManager->DeselectAll(nil); // deselect every active CSB	
	}while(kFalse);	
}
void BookReportData::deSelectFrame(const UIDRef& frameuidref)	
{
	do{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			 //CA("ptrIAppFramework == nil");			
			break ;
		}
		//Select the frame.
		InterfacePtr<ISelectionManager> selectionManager(Utils<ISelectionUtils>()->QueryActiveSelection ());
		if (selectionManager == nil)
		{
			//CA("selectionManager == nil");
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::deSelectFrame::selectionManager == nil");
			break;
		}

		// Deselect everything.
		selectionManager->DeselectAll(nil); // deselect every active CSB

		// Make a layout selection.
		InterfacePtr<ILayoutSelectionSuite> layoutSelectionSuite(selectionManager, UseDefaultIID());
		if (!layoutSelectionSuite) {
			ptrIAppFramework->LogDebug(" AP7_RefreshContent::BookReportData::deSelectFrame::layoutSelectionSuite == nil");
			break;
		}
		layoutSelectionSuite-> DeselectPageItems(UIDList(frameuidref));						
	}while(kFalse);
	
}
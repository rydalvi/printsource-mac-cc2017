#ifndef __RfhLoginEventsHandler_h__
#define __RfhLoginEventsHandler_h__

#include "VCPlugInHeaders.h"
#include "IRegisterLoginEvent.h"
#include "RfhID.h"
#include "ILoginEvent.h"

class RfhLoginEventsHandler : public CPMUnknown<ILoginEvent>
{
public:
	RfhLoginEventsHandler(IPMUnknown* );
	~RfhLoginEventsHandler();
	bool16 userLoggedIn();
	bool16 userLoggedOut();
	bool16 resetPlugIn();
};

#endif
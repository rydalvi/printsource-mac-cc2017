//========================================================================================
//  
//  $File: $
//  
//  Owner: Shelendra Singh
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __RefreshContent31ID_h__
#define __RefreshContent31ID_h__

#include "SDKDef.h"

// Company:
#define kRefreshContent31CompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kRefreshContent31CompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kRefreshContent31PluginName	"RefreshContent31"			// Name of this plug-in.
#define kRefreshContent31PrefixNumber	0x61074 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kRefreshContent31Version		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kRefreshContent31Author		"Shelendra Singh"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kRefreshContent31PrefixNumber above to modify the prefix.)
#define kRefreshContent31Prefix		RezLong(kRefreshContent31PrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kRefreshContent31StringPrefix	SDK_DEF_STRINGIZE(kRefreshContent31PrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kRefreshContent31MissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kRefreshContent31MissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kRefreshContent31PluginID, kRefreshContent31Prefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kRefreshContent31ActionComponentBoss, kRefreshContent31Prefix + 0)
DECLARE_PMID(kClassIDSpace, kRefreshContent31PanelWidgetBoss, kRefreshContent31Prefix + 1)
DECLARE_PMID(kClassIDSpace, kRfhPanelListBoxWidgetBoss, kRefreshContent31Prefix + 3)
DECLARE_PMID(kClassIDSpace, kRfhDocWchResponderServiceBoss, kRefreshContent31Prefix + 4)
DECLARE_PMID(kClassIDSpace, kRfhLoginEventsHandler, kRefreshContent31Prefix + 5)
DECLARE_PMID(kClassIDSpace, kRfhIconSuiteWidgetBoss, kRefreshContent31Prefix + 6)
DECLARE_PMID(kClassIDSpace, kTextSuiteBoss, kRefreshContent31Prefix + 7)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 8)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 9)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 10)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 11)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 12)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 13)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 14)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 15)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 16)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 17)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 18)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 19)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 20)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 21)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 22)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 23)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 24)
//DECLARE_PMID(kClassIDSpace, kRefreshContent31Boss, kRefreshContent31Prefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IRfhSUITE, kRefreshContent31Prefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IRfhContentSELECTIONOBSERVER, kRefreshContent31Prefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_IRfhWIDGETOBSERVER, kRefreshContent31Prefix + 2)
DECLARE_PMID(kInterfaceIDSpace, IID_ILOGINEVENT, kRefreshContent31Prefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IREFRESHCONTENT31INTERFACE, kRefreshContent31Prefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kRefreshContent31ActionComponentImpl, kRefreshContent31Prefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kRfhContentSelectionObserverImpl, kRefreshContent31Prefix + 1)
DECLARE_PMID(kImplementationIDSpace, kRfhPanelListBoxObserverImpl, kRefreshContent31Prefix + 2)
DECLARE_PMID(kImplementationIDSpace, kRfhSuiteTextCSBImpl, kRefreshContent31Prefix + 3)
DECLARE_PMID(kImplementationIDSpace, kRfhSuiteASBImpl, kRefreshContent31Prefix + 4)
DECLARE_PMID(kImplementationIDSpace, kRfhWidgetObserverImpl, kRefreshContent31Prefix + 5)
DECLARE_PMID(kImplementationIDSpace, kRfhDocWchServiceProviderImpl, kRefreshContent31Prefix + 6)
DECLARE_PMID(kImplementationIDSpace, kRfhDocWchResponderImpl, kRefreshContent31Prefix + 7)
DECLARE_PMID(kImplementationIDSpace, kRfhLoginEventsHandlerImpl, kRefreshContent31Prefix + 8)
DECLARE_PMID(kImplementationIDSpace, kRfhIconWidgetObserverImpl, kRefreshContent31Prefix + 9)
DECLARE_PMID(kImplementationIDSpace, kEventHandlerImpl, kRefreshContent31Prefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kRefreshContent31Impl, kRefreshContent31Prefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kRefreshContent31AboutActionID, kRefreshContent31Prefix + 0)
DECLARE_PMID(kActionIDSpace, kRfhContentPanelWidgetActionID, kRefreshContent31Prefix + 1)
DECLARE_PMID(kActionIDSpace, kRefreshContent31Separator1ActionID, kRefreshContent31Prefix + 2)
DECLARE_PMID(kActionIDSpace, kRefreshContent31PopupAboutThisActionID, kRefreshContent31Prefix + 3)//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 5)
DECLARE_PMID(kActionIDSpace, kRfhMenuItem1ActionID, kRefreshContent31Prefix + 6)
DECLARE_PMID(kActionIDSpace, kRfhMenuItem2ActionID, kRefreshContent31Prefix + 7)
DECLARE_PMID(kActionIDSpace,  kRfhMenuItem3ActionID, kRefreshContent31Prefix + 8)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 9)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 10)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 11)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 12)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 13)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 14)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 15)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 16)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 17)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 18)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 19)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 20)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 21)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 22)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 23)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 24)
//DECLARE_PMID(kActionIDSpace, kRefreshContent31ActionID, kRefreshContent31Prefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kRefreshContent31PanelWidgetID, kRefreshContent31Prefix + 0)
DECLARE_PMID(kWidgetIDSpace, kRfhOptionsDropDownWidgetID, kRefreshContent31Prefix + 2)
DECLARE_PMID(kWidgetIDSpace, kRfhPageNumWidgetID, kRefreshContent31Prefix + 3)
DECLARE_PMID(kWidgetIDSpace, kRfhPanelLstboxWidgetID, kRefreshContent31Prefix + 4)
DECLARE_PMID(kWidgetIDSpace, kRfhRefreshButtonWidgetID, kRefreshContent31Prefix + 5)
DECLARE_PMID(kWidgetIDSpace, kRfhPanelListParentWidgetId, kRefreshContent31Prefix + 6)
DECLARE_PMID(kWidgetIDSpace, kRfhUnCheckIconWidgetID, kRefreshContent31Prefix + 7)
DECLARE_PMID(kWidgetIDSpace, kRfhCheckIconWidgetID, kRefreshContent31Prefix + 8)
DECLARE_PMID(kWidgetIDSpace, kRfhPanelTextWidgetID, kRefreshContent31Prefix + 9)
DECLARE_PMID(kWidgetIDSpace, kRfhLocateButtonWidgetID, kRefreshContent31Prefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kRefreshContent31WidgetID, kRefreshContent31Prefix + 25)


// "About Plug-ins" sub-menu:
#define kRefreshContent31AboutMenuKey			kRefreshContent31StringPrefix "kRefreshContent31AboutMenuKey"
#define kRefreshContent31AboutMenuPath		kSDKDefStandardAboutMenuPath kRefreshContent31CompanyKey

// "Plug-ins" sub-menu:
#define kRefreshContent31PluginsMenuKey 		kRefreshContent31StringPrefix "kRefreshContent31PluginsMenuKey"
#define kRefreshContent31PluginsMenuPath		kSDKDefPlugInsStandardMenuPath kRefreshContent31CompanyKey kSDKDefDelimitMenuPath kRefreshContent31PluginsMenuKey

// Menu item keys:
#define kRfhMenuItem1MenuItemKey		kRefreshContent31StringPrefix "kRfhMenuItem1MenuItemKey"
#define kRfhMenuItem2MenuItemKey		kRefreshContent31StringPrefix "kRfhMenuItem2MenuItemKey"
#define kRfhMenuItem3MenuItemKey		kRefreshContent31StringPrefix "kRfhMenuItem3MenuItemKey"

// Other StringKeys:
#define kRefreshContent31AboutBoxStringKey	kRefreshContent31StringPrefix "kRefreshContent31AboutBoxStringKey"
#define kRefreshContent31PanelTitleKey					kRefreshContent31StringPrefix	"kRefreshContent31PanelTitleKey"
#define kRefreshContent31StaticTextKey kRefreshContent31StringPrefix	"kRefreshContent31StaticTextKey"
#define kRefreshContent31InternalPopupMenuNameKey kRefreshContent31StringPrefix	"kRefreshContent31InternalPopupMenuNameKey"
#define kRefreshContent31TargetMenuPath kRefreshContent31InternalPopupMenuNameKey

// Menu item positions:

#define	kRefreshContent31Separator1MenuItemPosition		10.0
#define kRefreshContent31AboutThisMenuItemPosition		11.0

#define kRfhMenuItem1MenuItemPosition			21.0
#define kRfhMenuItem2MenuItemPosition			22.0
#define	kRfhSeparator1MenuItemPosition			23.0
#define kRfhMenuItem3MenuItemPosition			24.0

#define kRfhPanelLstBoxRsrcID				1090
#define kRfhRefreshIconID					11000
#define kRfhUnChkIconID						11001
#define kRfhChkIconID						11002
#define kRfhlocatIconID						11003
// "Plug-ins" sub-menu item positions:
#define kLNGDialogMenuItemPosition	1.0

// Initial data format version numbers
#define kRefreshContent31FirstMajorFormatNumber  RezLong(1)
#define kRefreshContent31FirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kRefreshContent31CurrentMajorFormatNumber kRefreshContent31FirstMajorFormatNumber
#define kRefreshContent31CurrentMinorFormatNumber kRefreshContent31FirstMinorFormatNumber

#endif // __RefreshContent31ID_h__

//  Code generated by DollyXs code generator

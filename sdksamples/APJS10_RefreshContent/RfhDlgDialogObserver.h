#include "VCPlugInHeaders.h"

#ifndef __RfhDlgDialogObserver__
#define __RfhDlgDialogObserver__
// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "ISelectionManager.h"

// General includes:
#include "CDialogObserver.h"
#include "CAlert.h"
#include "IBookContentMgr.h"
#include "K2Vector.tpp" 


class RfhDlgDialogObserver : public CDialogObserver
{
	public:

		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		RfhDlgDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {			
		}

		/** Destructor. */
		virtual ~RfhDlgDialogObserver() {			
		}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. 
				Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);

		virtual K2Vector<PMString> GetBookContentNames(IBookContentMgr* bookContentMgr);
//		void RefreshBookWithInteractiveMode();
		void fillFrameRecordVector(int32 refreshMode); // 1 = delete , 2 = Update ,  3 = New
//		void OpenInteractiveRefreshDialog();
		void fillRefreshModeListBox(InterfacePtr<IPanelControlData> panelControlData);
		void selectNextFrame();

		void dummyRefrsehForRepoert(int32 casNo);
		void processUpdateNew();
		void goForNextDocument();
		void goForPreviousDocument();
		void selectPreviousFrame();
		void getNewItemIDs();
		void openContentSprayer();
		void closeContentSprayer(double sectionID,double langID);
};

#endif
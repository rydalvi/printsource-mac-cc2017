#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ILayoutUtils.h"
#include "ISubject.h"
#include "ISelectableDialogSwitcher.h"
#include "IAppFramework.h"
#include "ListBoxHelper.h"
#include "IListBoxController.h"
#include "IListControlData.h"
#include "ITextControlData.h"
#include "MediatorClass.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
#include "RfhSelectionObserver.h"
#include "IListBoxController.h"
#include "ILayoutUtils.h"
#include "MediatorClass.h"
#include "Refresh.h"
#include "RefreshData.h"
#include "RfhID.h"
#include "IFrameUtils.h"
#include "ILayoutUIUtils.h"
#include "IWorkspace.h"
#include "ISwatchList.h"
#include "ITextAttrUID.h"
#include "ICommand.h"
#include "GlobalData.h"
#include "ILayoutControlData.h"
#include "ISpread.h"
#include "IGeometry.h"
#include "RfhActionComponent.h"
#include "ISpreadList.h"
//#include "ITextFrame.h"
#include "SystemUtils.h"
#include "ILayoutUtils.h"		 //Cs4
#include "ITextAttrUtils.h"
#include "IPanelControlData.h"
#include "DocWchUtils.h"
#include "ITextValue.h"
#include "AcquireModalCursor.h" 
#include "ILayoutUIUtils.h" //Cs4
#include "ITextFrameColumn.h"
#include "IHierarchy.h"
#include "IMultiColumnTextFrame.h"
#include "IGraphicFrameData.h"
#include "IDialogMgr.h"
#include "IWindow.h"
#include "ITreeViewMgr.h"
#include "RFHTreeModel.h"
//#include "CAlert.h"
//#include "IMessageServer.h"
#include "Report.h"
#include "RFHTreeDataCache.h"
#include "IntNodeID.h"

extern itemList tempItemList;
extern productList tempProductList;

extern vector<double>  itemsUpdated;
extern vector<double>  itemsDeleted;
extern vector<double>  productsUpdated;
extern vector<double>  productsDeleted;

extern vector<double> itemImagesDeleted;
extern vector<double> productImagesDeleted;
extern vector<double> itemImagesReLinked;
extern vector<double> productImagesReLinked;
extern bool16 refreshTableByAttribute;    //----------
bool16 issrtucturebuttenselected; //= kFalse;
bool16 iscontentbuttonselected;// = kFalse;
vector<int32>textboxvaluevec;//by Amarjit patil
vector<double>AttributeIdsForFrameRefresh;//by amarjit patil
map<double,double>UniqueAttributes;
vector<double>ItemgroupAttributeIdsforframerefresh;
vector<double>ItemidsforpageRangevec;
vector<double>ItemGroupIDsForPageRange;
map<int32,int32>mapforpagerange;
map<double,double>MapForItemGroup;
static int32 Newvar;

#define FILENAME			PMString("RfhSelectionObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);

bool16 Framelevelstructurevariable=kFalse;
bool16 Framelevelcontentvariable=kFalse;
int32 ReloadState;
PMString numToPMString(int32 num);

bool16 IsFrameRefreshSelected;
int32 myglobal = 1;
bool16 strutureIsSelected = kFalse;
extern set<NodeID> UniqueNodeIds;

extern int32 TreeDisplayOption;
extern bool16 isEmptySectionFound;

PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}

#define CA(X) CAlert::InformationAlert(X)// 
	/*( \
	PMString("RfhSelectionObserver.cpp") + PMString("\n") + \
    PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + X)*/

//#define CA(x) CAlert::InformationAlert(PMString(__FUNCTION__)+ PMString("\n") + \
//	PMString().AppendNumber(__LINE__) + PMString("\n") + x )
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


extern int32 Flag1;
extern RefreshDataList rDataList;
extern RefreshDataList OriginalrDataList;

extern int16 pgno,cursel,seltext,FlagParentID;
extern RefreshDataList ColorDataList;
extern bool16 FlgToCheckMenuAction;
extern TextVectorList ChangedTextList;
extern bool16 IsDeleteFlagSelected;
extern bool16 CheckBoxFlage;
bool16 SelectBoxFlage = kTrue;
bool16 RefreshFlag = kFalse;
RefreshDataList ElementGroupList;
RefreshDataList UniqueDataList;

bool16 isElementoptSelected = kFalse;

extern int GroupFlag;
IsMultipleSectionsDataAvailable isMultipleSectionsAvailable;

extern UniqueIds sectionIds;
extern multiSectionMap MapForItemsPerSection;
extern multiSectionMap MapForProductsPerSection;

extern bool16 IsBookrefreshSelected;
extern bool16 showSummaryDialog;
extern IDialog* Summarydialog;



void DoRfhPreferenceDialog();
//// Global Pointers
//extern IAppFramework* ptrIAppFramework;
///////////

InterfacePtr<IPanelControlData> PanelControlDataforList;

CREATE_PMINTERFACE(RfhSelectionObserver, kRfhContentSelectionObserverImpl)

RfhSelectionObserver::RfhSelectionObserver(IPMUnknown *boss) :
ActiveSelectionObserver(boss/*, IID_IRfhContentSELECTIONOBSERVER*/) {}//("RfhSelectionObserver::RfhSelectionObserver");}

RfhSelectionObserver::~RfhSelectionObserver() {}

void RfhSelectionObserver::HandleSelectionChanged(const ISelectionMessage* message)
{
	//CA("in handle selection changed");
	UpdatePanel();
}

void RfhSelectionObserver::AutoAttach()
{	
	ActiveSelectionObserver::AutoAttach();
	Mediator::loadData=kTrue;
	
	/*if(Mediator::iPanelCntrlDataPtr==nil)
	{*/	
		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData==nil)
		{
			//CA("iPanelControlData is nil");
			return;
		}
		/* Storing IPanelControlData in Mediator */
		Mediator::iPanelCntrlDataPtr=iPanelControlData;
		
		AttachWidget(Mediator::iPanelCntrlDataPtr, kRfhOptionsDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		//AttachWidget(Mediator::iPanelCntrlDataPtr, kTPLClassTreeIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(Mediator::iPanelCntrlDataPtr, kRfhRefreshButtonWidgetID, IID_ITRISTATECONTROLDATA); // 21 Feb Vaibhav
		/////////////////////nitin
		AttachWidget(Mediator::iPanelCntrlDataPtr, kRfhSelectionBoxtWidgetID, IID_ITRISTATECONTROLDATA);
		///////////
		AttachWidget(Mediator::iPanelCntrlDataPtr, kRfhPageNumWidgetID, IID_ITEXTCONTROLDATA );
		AttachWidget(Mediator::iPanelCntrlDataPtr, kRfhReloadIconWidgetID, IID_ITRISTATECONTROLDATA );
		AttachWidget(Mediator::iPanelCntrlDataPtr, kRefreshCellRadioButtonWidgetID, IID_ITRISTATECONTROLDATA );
		AttachWidget(Mediator::iPanelCntrlDataPtr, kRefreshFullTableRadioButtonWidgetID, IID_ITRISTATECONTROLDATA );
		//AttachWidget(Mediator::iPanelCntrlDataPtr, kRfhLocateButtonWidgetID, IID_ITRISTATECONTROLDATA); // 21 Feb Vaibhav
	

	/*}*/
	
	loadPaletteData();		
}

void RfhSelectionObserver::loadPaletteData(void)
{	
	//CA("Inside loadPaletteData");
    	//Mediator md;
	static int i;
	
	if(Mediator::loadData==kFalse)
	Flag1 = 1;
	if(Mediator::loadData==kTrue)
	{
		do
		{	
	    	//this->DisableAll();			
			/* Check if the user has LoggedIn successfully */
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
				return;
	
			bool16 result=ptrIAppFramework->getLoginStatus();		

			//if(result != kTrue)
			//	return;
			//Mediator::iPanelCntrlDataPtr=nil;
/****************************************************************/
			if(Mediator::iPanelCntrlDataPtr==nil)
			{	
				InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
				if (iPanelControlData==nil)
				{
					//CA("iPanelControlData is nil");
					Mediator::iPanelCntrlDataPtr=Mediator::iPanelCntrlDataPtrTemp;
					//return;					
				}				
				else
				{
					/* Storing IPanelControlData in Mediator */
					Mediator::iPanelCntrlDataPtr=iPanelControlData;//
					Mediator::iPanelCntrlDataPtrTemp=iPanelControlData;
				}
				i++;
			}
	 
			RfhActionComponent actionObsever(this);
			actionObsever.DoPalette();
	
			Mediator::dropdownCtrlView=nil;
			Mediator::RfhRefreshButtonView=nil;
			Mediator::listControlView = nil;
			Mediator::RfhReloadButtonView = nil; 

			IControlView* dropdownCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kRfhOptionsDropDownWidgetID);
			if(!dropdownCtrlView)
				break;
	
			//IControlView* listControlView1 =	Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPanelLstboxWidgetID); //og
			InterfacePtr<IControlView>listControlView1(Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPanelTreeViewWidgetID),UseDefaultIID());
			if(!listControlView1){ 
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::!listControlView1");
				break;
			}
			//IControlView* refreshBtnCtrlView= Mediator::iPanelCntrlDataPtr->FindWidget(kRfhRefreshButtonWidgetID); //og
			InterfacePtr<IControlView>refreshBtnCtrlView(Mediator::iPanelCntrlDataPtr->FindWidget(kRfhRefreshButtonWidgetID),UseDefaultIID());
			if(!refreshBtnCtrlView)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::!refreshBtnCtrlView");
				break;
			}

			//IControlView* selectionBoxCtrlView = Mediator::iPanelCntrlDataPtr->FindWidget(kRfhSelectionBoxtWidgetID); //og
			InterfacePtr<IControlView>selectionBoxCtrlView(Mediator::iPanelCntrlDataPtr->FindWidget(kRfhSelectionBoxtWidgetID),UseDefaultIID());
			if(!selectionBoxCtrlView)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::!selectionBoxCtrlView");
				break;
			}
			//added by vijay on 5 OCT 2006//////////////////Frm Here
			//IControlView* reloadBtnCtrlView = Mediator::iPanelCntrlDataPtr->FindWidget(kRfhReloadIconWidgetID); //og
			InterfacePtr<IControlView>reloadBtnCtrlView(Mediator::iPanelCntrlDataPtr->FindWidget(kRfhReloadIconWidgetID),UseDefaultIID());
			if(!reloadBtnCtrlView)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::!reloadBtnCtrlView");			
				break;
			}
			////////////////////////////////////////////////upto here
			/*IControlView* LocateBtnCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kRfhLocateButtonWidgetID);
			if(!LocateBtnCtrlView)
				break;*/ // 21 Feb Vaibhav
			//Mediator::RfhLocateButtonView = NULL; //LocateBtnCtrlView;			

			//IControlView* EditBoxCtrlView = Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPageNumWidgetID); //og
			InterfacePtr<IControlView>EditBoxCtrlView(Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPageNumWidgetID),UseDefaultIID());
			if(!EditBoxCtrlView)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::!EditBoxCtrlView");						
				break;
			}

			//**Commented By Sachin Sharma 15/07/08
			/*IControlView* PreferenceIconCtrlView = Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPreferenceIconWidgetID);
			if(!PreferenceIconCtrlView)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::!PreferenceIconCtrlView");						
				break;
			}*/

			InterfacePtr<ITextControlData> obj(EditBoxCtrlView, UseDefaultIID());
			Mediator::EditBoxTextControlData=obj;
			
			Mediator::dropdownCtrlView=dropdownCtrlView;
			Mediator::RfhRefreshButtonView=refreshBtnCtrlView;
			Mediator::listControlView=listControlView1;	
			Mediator::editBoxView=EditBoxCtrlView;
			//Added by vijay on 5 OCT 2006/////////////
			Mediator::RfhReloadButtonView = reloadBtnCtrlView;

			
			//IControlView* cellRadioControlView = Mediator::iPanelCntrlDataPtr->FindWidget(kRefreshCellRadioButtonWidgetID); //og
			InterfacePtr<IControlView>cellRadioControlView(Mediator::iPanelCntrlDataPtr->FindWidget(kRefreshFullTableRadioButtonWidgetID),UseDefaultIID());
			if(!cellRadioControlView){ 
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::!cellRadioControlView");
				break;
			}

			/*if(iscontentbuttonselected==kTrue)
			{
				CA("Content is True");
			}
			else
			{
				CA("Content is False");
			}*/
			if(iscontentbuttonselected==kFalse)
			{
				InterfacePtr<ITriStateControlData> itristatecontroldata(cellRadioControlView, UseDefaultIID());
				if(itristatecontroldata==nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::itristatecontroldata is nil");				
					break;
				}
				itristatecontroldata->Select();
			}

			refreshTableByAttribute = kTrue;

			//IControlView* radioGroupControlView = Mediator::iPanelCntrlDataPtr->FindWidget(kSPRadioButtonGroupPanelWidgetID); //og
			InterfacePtr<IControlView>radioGroupControlView(Mediator::iPanelCntrlDataPtr->FindWidget(kSPRadioButtonGroupPanelWidgetID),UseDefaultIID());
			if(!radioGroupControlView){ 
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::!radioGroupControlView");
				break;
			}


			IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(iDoc== nil)
			{
				//CA("iDoc== nil");
			    //DisableAll();
				//return;
			}
 
			if(!(result && iDoc))
			{   
				if(!result && !iDoc)
				{
					//CA("Invalid selection.Please log into PRINTsource and open a document before continuing.");
				}
				else 
				{
					if(!result)
					{
					//CA("Invalid selection.  Please log into PRINTsource before continuing.");
					}
					else if(result && !iDoc){
						//CA("Invalid selection.  Please open a document before continuing.");
					}
				}

				
				/*refreshBtnCtrlView->Disable();
				listControlView1->Disable();
				EditBoxCtrlView->Disable();
				reloadBtnCtrlView->Disable();*/
				
				//PreferenceIconCtrlView->Disable(); //**Commented By sachin Sharma 15/07/-8

				InterfacePtr<IDropDownListController> RefreshDropListCntrler(dropdownCtrlView, UseDefaultIID());
				if(RefreshDropListCntrler==NULL)
				{
					//CA("RefreshDropListCntrler==NULL");
					break;
				}
				//CA("A");
				RefreshDropListCntrler->Select(0);
				dropdownCtrlView->Disable();

				selectionBoxCtrlView->Disable();
				
				radioGroupControlView->Disable();
				/*RfhActionComponent actionObsever(this);
				actionObsever.CloseRefreshPalette();*/

				Flag1 = 1;
				break;
			}			
			else
			{	
				//PreferenceIconCtrlView->Enable(); //**Commented By sa Sharma 15/07/08
				selectionBoxCtrlView->Enable();
				dropdownCtrlView->Enable();
			//	refreshBtnCtrlView->Enable();
				listControlView1->Enable();
				reloadBtnCtrlView->Enable();
				
				radioGroupControlView->Enable();

				if(Mediator::selectedRadioButton==2)
                	EditBoxCtrlView->Enable();
				else
					EditBoxCtrlView->Disable();

				if(rDataList.size()>0)
					Mediator::RfhRefreshButtonView->Enable();
				else
					Mediator::RfhRefreshButtonView->Disable();

				if(FlgToCheckMenuAction)
				{	
					InterfacePtr<IDropDownListController> RefreshDropListCntrler(dropdownCtrlView, UseDefaultIID());
					if(RefreshDropListCntrler==nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::RefreshDropListCntrler==nil");						
						break;
					}
					//CA("B");
					if(ReloadState==0)
						RefreshDropListCntrler->Select(0);
					else
						RefreshDropListCntrler->Select(ReloadState);

					FlgToCheckMenuAction= kFalse;
					EditBoxCtrlView->Disable();
					//InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
					//if (iPanelControlData==nil)
					//{
					//	//CA("iPanelControlData is nil");
					//	//	break;
					//}
					
					SDKListBoxHelper listBox(this, kRfhPluginID);
					listBox.EmptyCurrentListBox(Mediator::listControlView);	// this is a False one....
	
				}
			}			
/***************************************************************/			
			if(result && iDoc)
			{ 	

			//	AttachWidget(Mediator::iPanelCntrlDataPtr, kRfhOptionsDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
			////	AttachWidget(Mediator::iPanelCntrlDataPtr, kTPLClassTreeIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);
			//	AttachWidget(Mediator::iPanelCntrlDataPtr, kRfhRefreshButtonWidgetID, IID_ITRISTATECONTROLDATA);
			//	AttachWidget(Mediator::iPanelCntrlDataPtr, kRfhPageNumWidgetID, IID_ITEXTCONTROLDATA );
			//	AttachWidget(Mediator::iPanelCntrlDataPtr, kRfhLocateButtonWidgetID, IID_ITRISTATECONTROLDATA); // commented 21 Feb Vaibhav

				IControlView* iControlView = Mediator::iPanelCntrlDataPtr->FindWidget(kRfhOptionsDropDownWidgetID);
				if (iControlView == nil)
				{
					//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::iControlView == nil");										
					break;
				}

				InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
				if (iDropDownListController == nil)
				{
					ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::iDropDownListController == nil");														
					break;
				}

				if(Flag1 == 1)
				{		
					//CA("C");
					iDropDownListController->Select(0);
					Flag1 =0;					
				}
				else if(Flag1 == 0)
				{						
					/*InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
					if (iPanelControlData == nil)
					{
						ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::loadPaletteData::iPanelControlData == nil");																			
						break;					
					}*/
				}

			}
			else
			{		
				/*RfhActionComponent actionObsever(this);
				actionObsever.CloseRefreshPalette();*/
				
			}
		
		} while (kFalse);
	}
	
}

void RfhSelectionObserver::AutoDetach()
{
	ActiveSelectionObserver::AutoDetach();
	Mediator::loadData=kFalse;
	do
	{	
		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData==nil)
		{
		//	CA("iPanelControlData is nil");
			return;
		}
		DetachWidget(iPanelControlData, kRfhOptionsDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		DetachWidget(iPanelControlData, kRfhRefreshButtonWidgetID, IID_ITRISTATECONTROLDATA); // 21 Feb Vaibhav
		//DetachWidget(Mediator::iPanelCntrlDataPtr, kRfhRefreshButtonWidgetID, IID_IBOOLEANCONTROLDATA);
		DetachWidget(iPanelControlData, kRfhPageNumWidgetID, IID_ITEXTCONTROLDATA );
		//DetachWidget(Mediator::iPanelCntrlDataPtr, kRfhLocateButtonWidgetID, IID_ITRISTATECONTROLDATA); //21 Feb Vaibhav
		DetachWidget(iPanelControlData, kRfhReloadIconWidgetID, IID_ITRISTATECONTROLDATA );
		DetachWidget(iPanelControlData, kRfhSelectionBoxtWidgetID, IID_ITRISTATECONTROLDATA );
		DetachWidget(iPanelControlData, kRefreshCellRadioButtonWidgetID, IID_ITRISTATECONTROLDATA ); 
		DetachWidget(iPanelControlData, kRefreshFullTableRadioButtonWidgetID, IID_ITRISTATECONTROLDATA ); 
		
	} while (kFalse);

	DocWchUtils::StopDocResponderMode();
}

void RfhSelectionObserver::setTriState
(const WidgetID&  widgetID, ITriStateControlData::TriState state)
{
	do 
	{
		//CA("RfhSelectionObserver::setTriState");
		/*
		InterfacePtr<IPanelControlData> iPanelControlData(this, UseDefaultIID());
		if(iPanelControlData==nil) 
			break;

		//Mediator::iPanelCntrlDataPtr=iPanelControlData;
		*/
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
            break;
		}
		IControlView * iControlView=Mediator::iPanelCntrlDataPtr->FindWidget(widgetID);
		if(iControlView==nil) 
		{
			//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::setTriState::iControlView is nil");		
			break;
		}
		
		InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
		if(itristatecontroldata==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::setTriState::itristatecontroldata is nil");				
			break;
		}

		itristatecontroldata->SetState(state);
	} while(kFalse);	
}

void RfhSelectionObserver::Update
(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{
	ActiveSelectionObserver::Update(theChange, theSubject, protocol, changedBy);
	
	do
	{	
		//CA("RfhSelectionObserver::Update");
		AttributeIdsForFrameRefresh.clear();
		UniqueAttributes.clear();
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
            break;
		}

		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::controlView is nil");						
			return;	
		}

		WidgetID theSelectedWidget = controlView->GetWidgetID();

		IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/;		 //Cs4
			if(iDoc== NULL) { 
				//CA("iDoc== NULL in Handle Drop Down ---1");
			return;
			}

		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData is nil");									
			break;
		}

		IControlView * editBox = iPanelControlData->FindWidget(kRfhPageNumWidgetID);
		
//added by sagar for updating a list 

		
		IControlView* contentRadioBtnView = iPanelControlData->FindWidget(kRefreshCellRadioButtonWidgetID);
		if(!contentRadioBtnView)
		{		
			ptrIAppFramework->LogDebug("AP7_RefreshContent::contentRadioBtnView::Update::contentRadioBtnView == nil");
			return;
		}
		InterfacePtr<ITriStateControlData>contentRadioTriStatenew(contentRadioBtnView,UseDefaultIID());
		if(contentRadioTriStatenew == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::contentRadioTriStatenew::Update::contentRadioTriStatenew == nil");
			return;
		}
		bool16 isContent = contentRadioTriStatenew->IsSelected();
		if(isContent == kFalse)
		{
			//CA("structure only");
			strutureIsSelected =kTrue ;
		}
		else
		if(isContent == kTrue)
		{
			//CA("content selected");
			strutureIsSelected = kFalse;

		}
//till here


		if(theSelectedWidget==kRfhOptionsDropDownWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			//CA("theSelectedWidget==kRfhOptionsDropDownWidgetID");
			//ReloadState = -1;
			int32 ReloadstateNew=ReloadState;
			HandleDropDownListClick(theChange,theSubject,protocol,changedBy);
			break;
		}
		//CA("RfhSelectionObserver::Update 1");
		if(editBox->IsEnabled()==kTrue)
		{
			if(theSelectedWidget==kRfhPageNumWidgetID && protocol==IID_ITEXTCONTROLDATA )
			{
			//CA("page number text box");
			textboxvaluevec.clear();
			bool16 result1 = ptrIAppFramework->getLoginStatus();
			if(!result1)
				return;

			IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/;		 //Cs4
			if(iDoc== NULL) { 
				//CA("iDoc== NULL in Handle Drop Down ---1");
			return;
			}

			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData is nil");									
				break;
			}

			IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
			SDKListBoxHelper listBox(this, kRfhPluginID);
			InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());
			Mediator::EditBoxTextControlData=obj;
			PMString pageNumber;
			pageNumber=obj->GetString();
			PMString a("PageNumber : ");
			a.Append(pageNumber);
			//CA(a);
			if(!pageNumber.NumUTF16TextChars())
			{

				//CA("if(!pageNumber.NumUTF16TextChars())");
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::!pageNumber.NumUTF16TextChars");									;
				break;
			}
			char *ptr;
			
			ptr=strtok(const_cast<char *> (pageNumber.GetPlatformString().c_str()),"-");
			while(ptr!=NULL)
			{
				 //std::string str3(ptr);
				 int32 x = atoi(ptr);
				 textboxvaluevec.push_back(x);
				 ptr = strtok (NULL, "-");
			}
			int32 size1=static_cast<int32>(textboxvaluevec.size());
			if(size1==1)
			{
				//CA("if(size1==1)");
			
				Mediator::refreshPageNumber=pageNumber.GetAsNumber();
				Mediator::refreshPageNumber--;
				
				UID pageUID;
				Refresh refresh;
				if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
				{	
					ptrIAppFramework->LogError("AP7_RefreshContent::RfhSelectionObserver::Update::!refresh.isValidPageNumber");									
					break;
				} 
				Mediator::selectedRadioButton=2;
				refresh.GetPageDataInfo(Mediator::selectedRadioButton);
				
				if(GroupFlag==3)
				{
					//CA("if(GroupFlag==3)");
					reSortTheListForElem();
					fillDataInListBox(3);
					Mediator::isGroupByObj=kFalse;
					isObjectGroup=kFalse;
					isElementoptSelected= kTrue;
				}
				else if(GroupFlag==2)
				{	
					//CA("if(GroupFlag==2)");
					reSortTheListForObject();
					fillDataInListBox(2);
					Mediator::isGroupByObj=kTrue;
					isObjectGroup=kTrue;
					isElementoptSelected= kFalse;

				}
				else if(GroupFlag==1)
				{
					//CA("else if(GroupFlag==1)");
					reSortTheListForUniqueAttr();
					fillDataInListBox(1);
					/*Mediator::isGroupByObj=kTrue;
					isObjectGroup=kTrue;
					isElementoptSelected= kFalse;*/
					//CA("else 1.69");
				}
			}
			
			break;
			}		

		}
		//Added by Amarjit patil for content option
		if ( theSelectedWidget == kRfhRefreshButtonWidgetID && theChange == kTrueStateMessage)
		{	
			//CA("theSelectedWidget == kRfhRefreshButtonWidgetID");
			//ptrIAppFramework->clearAllStaticObjects();

			bool16 result1 = ptrIAppFramework->getLoginStatus();
			if(!result1)
				return;

			IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/;		 //Cs4
			if(iDoc== NULL) { 
				//CA("iDoc== NULL in Handle Drop Down ---1");
			return;
			}

			if(ReloadState == -1)
			{
				 ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhSelectionObserver::Update::ReloadState == -1");															
				return;
			}

			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");															
				return;
			}

			Refresh refresh;
			//SDKListBoxHelper listBox(this, kRfhPluginID);

			switch(ReloadState)
			{
				case 1/*0*/:	
						{
							Mediator::editBoxView->Disable();
							Mediator::EditBoxTextControlData->SetString("");
							
							//listBox.EmptyCurrentListBox(Mediator::listControlView);
							rDataList.clear();
							OriginalrDataList.clear();
							UniqueDataList.clear();
							Mediator::RfhRefreshButtonView->Disable();
							
						}
					break;
				case 0/*1*/:
					{	
						Mediator::selectedRadioButton = 3;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						if(GroupFlag==3)
						{
							reSortTheListForElem();						
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag==2)
						{
							reSortTheListForObject();
							fillDataInListBox(2);
						    Mediator::isGroupByObj=kTrue;
						    isObjectGroup=kTrue;
						    isElementoptSelected= kFalse;
						}	
						else if(GroupFlag==1)
						{
							//reSortTheListForUniqueAttr();
							//fillDataInListBox(1);
						}	
						
					}
					break;
				case 2:
					{
						Mediator::selectedRadioButton = 1;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
				
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						if(GroupFlag == 3)
						{
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{	
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);							
						}	
			        }
					break;

				case 3:
					{	
						//CA("case 3rfh");
						/*Mediator::selectedRadioButton=2;
						listBox.EmptyCurrentListBox(Mediator::listControlView);
						IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
						editBox->Enable(kTrue, kFalse);
						InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());						
						PMString pg("");
						obj->SetString(pg);*/

						/*rDataList.clear();
						OriginalrDataList.clear();*/

						//iPanelControlData->SetKeyboardFocus(kRfhPageNumWidgetID);

						//Mediator::RfhRefreshButtonView->Disable();
						//Mediator::isGroupByObj=kTrue;
						//isObjectGroup=kTrue;
						//isElementoptSelected= kFalse;						
						//////////////////////////////////////
						InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
						if (iPanelControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");																					
							break;
						}

						IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
						SDKListBoxHelper listBox(this, kRfhPluginID);
						InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());
						Mediator::EditBoxTextControlData=obj;
						PMString pageNumber;
						pageNumber=obj->GetString();
						if(!pageNumber.NumUTF16TextChars())
						{
							//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::!pageNumber.NumUTF16TextChars");																					
							break;
						}
						Mediator::refreshPageNumber=pageNumber.GetAsNumber();
						Mediator::refreshPageNumber--;
						
						UID pageUID;
						Refresh refresh;
						if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
						{	
							//ptrIAppFramework->LogError("AP7_RefreshContent::RfhSelectionObserver::Update::!refresh.isValidPageNumber");																					
							break;
						} 
						
						Mediator::selectedRadioButton=2;
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						if(GroupFlag==3)
						{
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag==2)
						{	
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;

						}
						else if(GroupFlag==1)
						{				
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						}	
			
					}					
					break;

				case 4:
						Mediator::selectedRadioButton = 4;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						if(GroupFlag == 3)
						{
							//CA("group flag");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("else 11");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						  
						}	
					break;				
			}
			//======================================End  Reload==================================

			//DoRfhPreferenceDialog();//** Added By Sachin Sharma 15/07/08
			InterfacePtr<IPanelControlData> iPanelCntrlDataPtr(this, UseDefaultIID());
			if(!iPanelCntrlDataPtr) 
			{
				break;
			}
			IControlView * iControlView=iPanelCntrlDataPtr->FindWidget(kRefreshCellRadioButtonWidgetID);
			if(iControlView==nil) 
			{
				//CA("iControlView==nil");
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::fillDataInListBox::iControlView is nil");		
				return;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
			if(itristatecontroldata==nil)
			{
				//CA("itristatecontroldata==nil"s);
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::fillDataInListBox::itristatecontroldata is nil");				
				return;
			}
			//for the selection of Content radio button
			if(itristatecontroldata->IsSelected())
			{
				//CA("-------content--------");
				Framelevelcontentvariable=kTrue;
				Framelevelstructurevariable=kFalse;
				//ptrIAppFramework->clearAllStaticObjects();

				bool16 result1 = ptrIAppFramework->getLoginStatus();
				if(!result1)
				return;

				IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/;//Cs4
				if(iDoc== NULL) { 
					//CA("iDoc== NULL in Handle Drop Down ---1");
				return;
			}

			int32 size2=static_cast<int32>(textboxvaluevec.size());
			if(size2==1)
			{
				AcquireWaitCursor awc;
				awc.Animate();
				ChangedTextList.clear();
				RefreshFlag = kTrue;
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
				{
           			break;
				}
				//ptrIAppFramework->POSSIBLEVALUECACHE_clearInstance();
				
				itemsUpdated.clear();
				itemsDeleted.clear();

				productsUpdated.clear();
				productsDeleted.clear();
				productImagesReLinked.clear();

				itemImagesDeleted.clear();
				productImagesDeleted.clear();
				itemImagesReLinked.clear();
			
				tempItemList.clear();
				tempProductList.clear();
				//*****Aded
				int32 size1=static_cast<int32>(rDataList.size());
				int32 i=0;
				while(i<rDataList.size())
				{
					if(rDataList[i].whichTab==4)
					{
						rDataList[i].isSelected=kTrue;
						double elementId = rDataList[i].elementID;
						PMString a("elementId1 :");
						a.AppendNumber(elementId);
						//CA(a);
						if(elementId!=-1)
						{
							//CA("1");
							//AttributeIdsForFrameRefresh.push_back(elementId);
						    UniqueAttributes.insert(map<double,double>::value_type(elementId,elementId));
						}
					}
					if(rDataList[i].whichTab==3)
					{
						rDataList[i].isSelected=kTrue;
						double elementId = rDataList[i].elementID;
						//if(elementId!=-1)
							ItemgroupAttributeIdsforframerefresh.push_back(elementId);
					}
					i++;
				}			
				//CA("Before calling HandleDropDownListClick ");
				HandleDropDownListClick/*Newoption*/(theChange,theSubject,protocol,changedBy);
				ShowRefreshSummary();//*ADded By Sachin Sharma..
			}
			else if(size2==2)
			{
				int32 start;
				int32 end;
				for(int32 k=0;k<1;k++)
				{
					start=textboxvaluevec.at(k);
					end=textboxvaluevec.at(k+1);
				}
				for(int32 pageNo=Newvar;pageNo<=end;pageNo++)
				{
					Mediator::refreshPageNumber=pageNo;
					Mediator::refreshPageNumber--;
				
					UID pageUID;
					Refresh refresh;
					if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
					{	
						ptrIAppFramework->LogError("AP7_RefreshContent::RfhSelectionObserver::Update::!refresh.isValidPageNumber");									
						break;
					} 
				
					Mediator::selectedRadioButton=2;
					refresh.GetPageDataInfo(Mediator::selectedRadioButton);
				
					if(GroupFlag==3)
					{
						reSortTheListForElem();
						fillDataInListBox(3);
						Mediator::isGroupByObj=kFalse;
						isObjectGroup=kFalse;
						isElementoptSelected= kTrue;
					}
					else if(GroupFlag==2)
					{
						reSortTheListForObject();
						fillDataInListBox(2);
						Mediator::isGroupByObj=kTrue;
						isObjectGroup=kTrue;
						isElementoptSelected= kFalse;

					}
					else if(GroupFlag==1)
					{
						reSortTheListForUniqueAttr();
						fillDataInListBox(1);
					}
					AcquireWaitCursor awc;
					awc.Animate();
					ChangedTextList.clear();
					RefreshFlag = kTrue;
					InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
					if(ptrIAppFramework == nil)
					{
               			break;
					}
					//ptrIAppFramework->POSSIBLEVALUECACHE_clearInstance();
				
					itemsUpdated.clear();
					itemsDeleted.clear();

					productsUpdated.clear();
					productsDeleted.clear();
					productImagesReLinked.clear();

					itemImagesDeleted.clear();
					productImagesDeleted.clear();
					itemImagesReLinked.clear();
				
					tempItemList.clear();
					tempProductList.clear();

					int32 size1=static_cast<int32>(rDataList.size());//real video
					int32 i=0;
					if(size1>0)
					{
						while(i<rDataList.size())
						{
							if(rDataList[i].whichTab==4)
							{
								rDataList[i].isSelected=kTrue;
								double elementId = rDataList[i].elementID;
								PMString a("elementId2 :");
								a.AppendNumber(elementId);
								//CA(a);
								if(elementId!=-1)
								{
									//CA("2");
									//AttributeIdsForFrameRefresh.push_back(elementId);
									UniqueAttributes.insert(map<double,double>::value_type(elementId,elementId));
								}
							}
							if(rDataList[i].whichTab==3)
							{
								rDataList[i].isSelected=kTrue;
								double elementId = rDataList[i].elementID;
								/*if(elementId!=-1)
								{*/
									ItemgroupAttributeIdsforframerefresh.push_back(elementId);
									PMString q1("ElementId1 :");
									q1.AppendNumber(elementId);
									//CA(q1);
								//}
							}
							i++;
						}			
						HandleDropDownListClick/*Newoption*/(theChange,theSubject,protocol,changedBy);
						//ShowRefreshSummary();//*ADded By Sachin Sharma..
					}
				}
				ShowRefreshSummary();//*ADded By Sachin Sharma..
			}
			else
			{
				//ptrIAppFramework->clearAllStaticObjects();

				bool16 result1 = ptrIAppFramework->getLoginStatus();
				if(!result1)
					return;

				IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/;//Cs4
				if(iDoc== NULL) { 
					//CA("iDoc== NULL in Handle Drop Down ---1");
				return;
				}

				//CA("theSelectedWidget == kRfhRefreshButtonWidgetID");
				//IsDeleteFlagSelected = kTrue;

				AcquireWaitCursor awc;
				awc.Animate();
				ChangedTextList.clear();
				RefreshFlag = kTrue;
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
				{
               		break;
				}
				//CA("kTPLRefreshIconSuiteWidgetID");
				//bool16 retVal=0;	
				
				//retVal=ptrIAppFramework->TYPECACHE_clearInstance();
				//ptrIAppFramework->GETCommon_refreshClientCache();
				
				//ptrIAppFramework->POSSIBLEVALUECACHE_clearInstance();
				/*if(retVal==0)
				{
				   ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhSelectionObserver::Update::POSSIBLEVALUECACHE_clearInstance's retval ==0");												
					break;
				}*/
	//*****Added
				itemsUpdated.clear();
				itemsDeleted.clear();

				productsUpdated.clear();
				productsDeleted.clear();
				productImagesReLinked.clear();

				itemImagesDeleted.clear();
				productImagesDeleted.clear();
				itemImagesReLinked.clear();
			
				tempItemList.clear();
				tempProductList.clear();
	//*****Aded
				int32 size1=static_cast<int32>(rDataList.size());//real video
					int32 i=0;
					if(size1>0)
					{
						while(i<rDataList.size())
						{
							if(rDataList[i].whichTab==4)
							{
								rDataList[i].isSelected=kTrue;
								double elementId = rDataList[i].elementID;
								PMString a("elementId3 :");
						a.AppendNumber(elementId);
						//CA(a);
								if(elementId!=-1)
								{
									//CA("3");
									//AttributeIdsForFrameRefresh.push_back(elementId);
									UniqueAttributes.insert(map<double,double>::value_type(elementId,elementId));
								}
							}
							if(rDataList[i].whichTab==3)
							{
								rDataList[i].isSelected=kTrue;
								double elementId = rDataList[i].elementID;
								/*if(elementId!=-1)
								{*/
									ItemgroupAttributeIdsforframerefresh.push_back(elementId);
									PMString q1("ElementId11 :");
									q1.AppendNumber(elementId);
									//CA(q1);
								//}
							}
							i++;
						}			
						HandleDropDownListClick/*Newoption*/(theChange,theSubject,protocol,changedBy);
						//ShowRefreshSummary();//*ADded By Sachin Sharma..
					}
				ShowRefreshSummary();//*ADded By Sachin Sharma..

			}
			}
			IControlView * iControlView1=iPanelCntrlDataPtr->FindWidget(kRefreshFullTableRadioButtonWidgetID);
			if(iControlView1==nil) 
			{
				//CA("iControlView==nil");
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::fillDataInListBox::iControlView is nil");		
				return;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldata1(iControlView1, UseDefaultIID());
			if(itristatecontroldata1==nil)
			{
				//CA("itristatecontroldata==nil"s);
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::fillDataInListBox::itristatecontroldata is nil");				
				return;
			}
//for the selection of structre radio button
			if(itristatecontroldata1->IsSelected())
			{
				//CA("-----Structure------");
				Framelevelstructurevariable=kTrue;
				Framelevelcontentvariable=kFalse;
				//ptrIAppFramework->clearAllStaticObjects();

				bool16 result1 = ptrIAppFramework->getLoginStatus();
				if(!result1)
					return;

				IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/;//Cs4
				if(iDoc== NULL) { 
					//CA("iDoc== NULL in Handle Drop Down ---1");
				return;
				}

				//CA("theSelectedWidget == kRfhRefreshButtonWidgetID");
				//IsDeleteFlagSelected = kTrue;

				int32 size2=static_cast<int32>(textboxvaluevec.size());
				if(size2==1)
				{
					AcquireWaitCursor awc;
					awc.Animate();
					ChangedTextList.clear();
					RefreshFlag = kTrue;
					InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
					if(ptrIAppFramework == nil)
					{
               			break;
					}
					//ptrIAppFramework->POSSIBLEVALUECACHE_clearInstance();
					/*if(retVal==0)
					{
					   ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhSelectionObserver::Update::POSSIBLEVALUECACHE_clearInstance's retval ==0");												
						break;
					}*/
					//*****Added
					itemsUpdated.clear();
					itemsDeleted.clear();

					productsUpdated.clear();
					productsDeleted.clear();
					productImagesReLinked.clear();

					itemImagesDeleted.clear();
					productImagesDeleted.clear();
					itemImagesReLinked.clear();
				
					tempItemList.clear();
					tempProductList.clear();
					//*****Aded
					int32 size1=static_cast<int32>(rDataList.size());
					

					int32 i=0;
					while(i<rDataList.size())
					{
						if(rDataList[i].whichTab==4)
						{
							rDataList[i].isSelected=kTrue;
							double elementId = rDataList[i].elementID;
							PMString a("elementId4 :");
							a.AppendNumber(elementId);
							//CA(a);
							if(elementId!=-1)
							{
								//CA("4");
								//AttributeIdsForFrameRefresh.push_back(elementId);
								UniqueAttributes.insert(map<double,double>::value_type(elementId,elementId));
							}
						}
						if(rDataList[i].whichTab==3)
						{
							rDataList[i].isSelected=kTrue;
							double elementId = rDataList[i].elementID;
							/*if(elementId!=-1)
							{*/
								ItemgroupAttributeIdsforframerefresh.push_back(elementId);
								PMString q1("ElementId12 :");
								q1.AppendNumber(elementId);
								//CA(q1);
							//}
						}
						i++;
					}			
					//CA("Before calling HandleDropDownListClick ");
					HandleDropDownListClick(theChange,theSubject,protocol,changedBy);
					//CA("MM");
					ShowRefreshSummary();//*ADded By Sachin Sharma..
			}
			else if(size2==2)
			{

				int32 start;
				int32 end;
				for(int32 k=0;k<1;k++)
				{
				start=textboxvaluevec.at(k);
				end=textboxvaluevec.at(k+1);
				}
				for(int32 pageNo=start;pageNo<=end;pageNo++)
				{
					Mediator::refreshPageNumber=pageNo;
					Mediator::refreshPageNumber--;
				
					UID pageUID;
					Refresh refresh;
					if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
					{	
						ptrIAppFramework->LogError("AP7_RefreshContent::RfhSelectionObserver::Update::!refresh.isValidPageNumber");									
						break;
					} 
				
					Mediator::selectedRadioButton=2;
					refresh.GetPageDataInfo(Mediator::selectedRadioButton);
				
					if(GroupFlag==3)
					{
						//CA("1");
						reSortTheListForElem();
						fillDataInListBox(3);
						Mediator::isGroupByObj=kFalse;
						isObjectGroup=kFalse;
						isElementoptSelected= kTrue;
					}
					else if(GroupFlag==2)
					{	//CA("2");
						reSortTheListForObject();
						fillDataInListBox(2);
						Mediator::isGroupByObj=kTrue;
						isObjectGroup=kTrue;
						isElementoptSelected= kFalse;

					}
					else if(GroupFlag==1)
					{
						//CA("else 11");
						reSortTheListForUniqueAttr();
						fillDataInListBox(1);
						/*Mediator::isGroupByObj=kTrue;
						isObjectGroup=kTrue;
						isElementoptSelected= kFalse;*/
						//CA("else 1.69");
					}
			AcquireWaitCursor awc;
			awc.Animate();
			ChangedTextList.clear();
			RefreshFlag = kTrue;
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
               	break;
			}
			//ptrIAppFramework->POSSIBLEVALUECACHE_clearInstance();
			
			itemsUpdated.clear();
			itemsDeleted.clear();

			productsUpdated.clear();
			productsDeleted.clear();
			productImagesReLinked.clear();

			itemImagesDeleted.clear();
			productImagesDeleted.clear();
			itemImagesReLinked.clear();
		
			tempItemList.clear();
			tempProductList.clear();
//*****Aded
			int32 size1=static_cast<int32>(rDataList.size());
			PMString w1("rDataList.size() :");
			w1.AppendNumber(size1);
			//CA(w1);

			int32 i=0;
			if(size1>0)
			{
			while(i<rDataList.size())
			{
				if(rDataList[i].whichTab==4)
				{
					rDataList[i].isSelected=kTrue;
					double elementId = rDataList[i].elementID;
					PMString a("elementId5 :");
						a.AppendNumber(elementId);
						//CA(a);
					if(elementId!=-1)
					{
						//CA("5");
						//AttributeIdsForFrameRefresh.push_back(elementId);
						UniqueAttributes.insert(map<double,double>::value_type(elementId,elementId));
					}
				}
				if(rDataList[i].whichTab==3)
				{
					rDataList[i].isSelected=kTrue;
					double elementId = rDataList[i].elementID;
					/*if(elementId!=-1)
					{*/
						ItemgroupAttributeIdsforframerefresh.push_back(elementId);
						PMString q1("ElementId13 :");
						q1.AppendNumber(elementId);
						//CA(q1);
					//}
				}
				i++;
			}			
			//CA("Before calling HandleDropDownListClick ");
			HandleDropDownListClick(theChange,theSubject,protocol,changedBy);
			//ShowRefreshSummary();//*ADded By Sachin Sharma..
			}
			}
				//CA("NN");
				ShowRefreshSummary();//*ADded By Sachin Sharma..
			}
			else 
			{
				//CA("else else else");
				//ptrIAppFramework->clearAllStaticObjects();

				bool16 result1 = ptrIAppFramework->getLoginStatus();
				if(!result1)
					return;

				IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/;//Cs4
				if(iDoc== NULL) { 
					//CA("iDoc== NULL in Handle Drop Down ---1");
				return;
				}

				//CA("theSelectedWidget == kRfhRefreshButtonWidgetID");
				//IsDeleteFlagSelected = kTrue;

				AcquireWaitCursor awc;
				awc.Animate();
				ChangedTextList.clear();
				RefreshFlag = kTrue;
				InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
				if(ptrIAppFramework == nil)
				{
               		break;
				}
				//CA("kTPLRefreshIconSuiteWidgetID");
				//bool16 retVal=0;	
				
				//retVal=ptrIAppFramework->TYPECACHE_clearInstance();
				//ptrIAppFramework->GETCommon_refreshClientCache();
				
				//ptrIAppFramework->POSSIBLEVALUECACHE_clearInstance();
				/*if(retVal==0)
				{
				   ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhSelectionObserver::Update::POSSIBLEVALUECACHE_clearInstance's retval ==0");												
					break;
				}*/
//*****Added
				itemsUpdated.clear();
				itemsDeleted.clear();

				productsUpdated.clear();
				productsDeleted.clear();
				productImagesReLinked.clear();

				itemImagesDeleted.clear();
				productImagesDeleted.clear();
				itemImagesReLinked.clear();
			
				tempItemList.clear();
				tempProductList.clear();
	//*****Aded


				HandleDropDownListClick(theChange,theSubject,protocol,changedBy);
				//CA("BB");
				ShowRefreshSummary();//*ADded By Sachin Sharma..

			}
			}
			
		}

///Following code is added by vijay on 5 OCT 2006////////////////////////From here
		if (theSelectedWidget == kRfhReloadIconWidgetID && theChange == kTrueStateMessage)
		{	
			//ptrIAppFramework->clearAllStaticObjects();

			bool16 result1 = ptrIAppFramework->getLoginStatus();
			if(!result1)
				return;

			IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/;		 //Cs4
			if(iDoc== NULL) { 
				//CA("iDoc== NULL in Handle Drop Down ---1");
			return;
			}

			if(ReloadState == -1)
			{
				 ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhSelectionObserver::Update::ReloadState == -1");															
				return;
			}

			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");															
				return;
			}

			Refresh refresh;
			SDKListBoxHelper listBox(this, kRfhPluginID);
			SelectBoxFlage = kTrue;
			switch(ReloadState)
			{
				// Awasthi swap 0 and 3
				case 1/*0*/:	
						{
							//CA("case 0");
							Mediator::editBoxView->Disable();
							Mediator::EditBoxTextControlData->SetString("");
							
							listBox.EmptyCurrentListBox(Mediator::listControlView);
							rDataList.clear();
							OriginalrDataList.clear();
							UniqueDataList.clear();
							Mediator::RfhRefreshButtonView->Disable();
							
						}
					break;
				case 0/*1*/:
					{	
						//CA("case 1");
						Mediator::selectedRadioButton = 3;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						//CA("case 1.1");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
                         //CA("case 1.5");
						if(GroupFlag==3)
						{
							//CA("group flag");
							reSortTheListForElem();						
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
							///CA("case 1.6");
						}
						else if(GroupFlag==2)
						{
							//CA("else 11");
							reSortTheListForObject();
							fillDataInListBox(2);
						    Mediator::isGroupByObj=kTrue;
						    isObjectGroup=kTrue;
						    isElementoptSelected= kFalse;
							//CA("else 1.66");
						}	
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						}	
						
					}
					break;
				case 2:
					{
						//CA("case 2");
						Mediator::selectedRadioButton = 1;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
				
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						if(GroupFlag == 3)
						{
							//CA("group flag");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{	//CA("Not group flag");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);							
						}	
			        }
					break;

				case 3:
					{	
						//CA("case 3rfh");
						/*Mediator::selectedRadioButton=2;
						listBox.EmptyCurrentListBox(Mediator::listControlView);
						IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
						editBox->Enable(kTrue, kFalse);
						InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());						
						PMString pg("");
						obj->SetString(pg);*/

						/*rDataList.clear();
						OriginalrDataList.clear();*/

						//iPanelControlData->SetKeyboardFocus(kRfhPageNumWidgetID);

						//Mediator::RfhRefreshButtonView->Disable();
						//Mediator::isGroupByObj=kTrue;
						//isObjectGroup=kTrue;
						//isElementoptSelected= kFalse;						
						//////////////////////////////////////
						InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
						if (iPanelControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");																					
							break;
						}

						IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
						SDKListBoxHelper listBox(this, kRfhPluginID);
						InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());
						Mediator::EditBoxTextControlData=obj;
						PMString pageNumber;
						pageNumber=obj->GetString();
						if(!pageNumber.NumUTF16TextChars())
						{
							//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::!pageNumber.NumUTF16TextChars");																					
							break;
						}
						Mediator::refreshPageNumber=pageNumber.GetAsNumber();
						Mediator::refreshPageNumber--;
						
						UID pageUID;
						Refresh refresh;
						if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
						{	
							//ptrIAppFramework->LogError("AP7_RefreshContent::RfhSelectionObserver::Update::!refresh.isValidPageNumber");																					
							break;
						} 
						
						Mediator::selectedRadioButton=2;
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						if(GroupFlag==3)
						{
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag==2)
						{	
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;

						}
						else if(GroupFlag==1)
						{				
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						}	
			
					}					
					break;

				case 4:
						Mediator::selectedRadioButton = 4;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						if(GroupFlag == 3)
						{
							//CA("group flag");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("else 11");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						  
						}	
					break;				
			}
		}

/////////////////////////////////////////////////////////////////////////upto here

/////Following code is added by Nitin on 27/08/07 ////////////////////////From here
		if(theSelectedWidget == kRfhSelectionBoxtWidgetID && ((theChange ==kFalseStateMessage )||(theChange == kTrueStateMessage)) && protocol == IID_ITRISTATECONTROLDATA)
    	{			
			//CA("theSelectedWidget == kRfhSelectionBoxtWidgetID");
			//if(SelectBoxFlage==kTrue)
			//{
			//	CA("SelectBoxFlage==kTrue");
			//	SelectBoxFlage=kFalse;
			//	//break;
			//}
			//else
			//	SelectBoxFlage=kTrue;
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				//CA("ptrIAppFramework == nil");
				break;
			}
			//SDKListBoxHelper sList(this, kRfhPluginID);
			//
			//InterfacePtr<IListBoxController> listCntl(Mediator::listControlView,IID_ILISTBOXCONTROLLER);
			//if(listCntl == nil) 
			//{
			//	//CA("listCntl == nil");
			//	break;
			//}			
			//listCntl->Select(0,kTrue,kTrue);
			//K2Vector<int32> curSelection ;			
			//listCntl->GetSelected(curSelection ) ;			
			//const int kSelectionLength =  curSelection.Length();			
			//if(kSelectionLength<=0)	
			//{
			//	//CA("kSelectionLength<=0");
			//	break;
			//}
			IControlView * iControlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kRfhSelectionBoxtWidgetID);
			if(iControlView==nil) 
			{
				//CA("iControlView==nil");
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::fillDataInListBox::iControlView is nil");		
				break;
			}

			InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
			if(itristatecontroldata==nil)
			{
				//CA("itristatecontroldata==nil");
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::fillDataInListBox::itristatecontroldata is nil");				
				break;
			}

			/*InterfacePtr<IListControlData> listControlData(Mediator::listControlView, UseDefaultIID());
			ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveElementAt() Found listbox but not control data?");
			if(listControlData==nil) 
				break;*/
			
			if(theChange==kTrueStateMessage)
			{	
				SelectBoxFlage=kTrue;
				if(GroupFlag != 1)
				{
					int32 i=0;
					while(i<rDataList.size())
					{
						if(rDataList[i].whichTab==4)
						{
							rDataList[i].isSelected=kTrue;
							double elementId = rDataList[i].elementID;

							PMString a("elementId6 :");
							a.AppendNumber(elementId);
							//CA(a);
							
							//AttributeIdsForFrameRefresh.push_back(elementId);
							//CA("6");
							UniqueAttributes.insert(map<double,double>::value_type(elementId,elementId));
							//sList.CheckUncheckRow(Mediator::listControlView, i, kTrue);
							i++;
						}
						if(rDataList[i].whichTab==3)
						{
							rDataList[i].isSelected=kTrue;
							double elementId = rDataList[i].elementID;
							
							ItemgroupAttributeIdsforframerefresh.push_back(elementId);
							//sList.CheckUncheckRow(Mediator::listControlView, i, kTrue);
							i++;
						}else{
							rDataList[i].isSelected=kTrue;
						//sList.CheckUncheckRow(Mediator::listControlView, i, kTrue);
						i++;
						}
					}				
				}
				else
				{
					//CA("GroupFlag == 1");
					int32 i=0;
					while(i<UniqueDataList.size())
					{
						if(UniqueDataList[i].whichTab==4)
						{
							rDataList[i].isSelected=kTrue;
							double elementId = UniqueDataList[i].elementID;
							PMString a("elementId7 :");
							a.AppendNumber(elementId);
							//CA(a);
							
							//AttributeIdsForFrameRefresh.push_back(elementId);
							//CA("7");
							UniqueAttributes.insert(map<double,double>::value_type(elementId,elementId));
							UniqueDataList[i].isSelected=kTrue;						
							//sList.CheckUncheckRow(Mediator::listControlView, i, kTrue);
							i++;
						}if(UniqueDataList[i].whichTab==3)
						{
							rDataList[i].isSelected=kTrue;
							double elementId = UniqueDataList[i].elementID;
							
							ItemgroupAttributeIdsforframerefresh.push_back(elementId);
							UniqueDataList[i].isSelected=kTrue;						
							//sList.CheckUncheckRow(Mediator::listControlView, i, kTrue);
							i++;
						}else{
							rDataList[i].isSelected=kTrue;
							UniqueDataList[i].isSelected=kTrue;						
							//sList.CheckUncheckRow(Mediator::listControlView, i, kTrue);
							i++;
						}

					}
				}
			}
			else
			{
				//CA("else....ooo......");
				////else part//////for the change is false	/////////	
				SelectBoxFlage=kFalse;
				if(GroupFlag != 1)
				{						
					int32 i=0;
					while(i<rDataList.size())
					{
						if(rDataList[i].whichTab==4)
						{
							rDataList[i].isSelected=kFalse;
							double elementId=rDataList[i].elementID;
							PMString q1("ElementId8 :");
							q1.AppendNumber(elementId);
							//CA(q1);
							//AttributeIdsForFrameRefresh.push_back(elementId);
							//CA("8");
							UniqueAttributes.insert(map<double,double>::value_type(elementId,elementId));
							//sList.CheckUncheckRow(Mediator::listControlView, i, kFalse);
							i++;
						}
						if(rDataList[i].whichTab==3)
						{
							rDataList[i].isSelected=kFalse;
							double elementId=rDataList[i].elementID;
							
							ItemgroupAttributeIdsforframerefresh.push_back(elementId);
							//sList.CheckUncheckRow(Mediator::listControlView, i, kFalse);
							i++;
						}else{
							rDataList[i].isSelected=kFalse;
							//sList.CheckUncheckRow(Mediator::listControlView, i, kFalse);
							i++;
						}
					}
				}
				else
				{
									
					int32 i=0;
					//while(i<rDataList.size())
					while(i<UniqueDataList.size())
					{
						//CA("i<UniqueDataList.size()");
						//rDataList[i].isSelected=kFalse;
						
						UniqueDataList[i].isSelected=kFalse;
						//sList.CheckUncheckRow(Mediator::listControlView, i, kFalse);
						i++;
					}
					for(int32 x=0; x< rDataList.size(); x++)
					{
						rDataList[x].isSelected=kFalse;
					}
				}
			}			
			
			fillDataInListBox(1);


		}
		
		if(theSelectedWidget == kRefreshCellRadioButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("kRefreshCellRadioButtonWidgetID");
			int32 size1=static_cast<int32>(textboxvaluevec.size());
			//PMString a2("size1 :");
			//a2.AppendNumber(size1);
			//CA(a2);
			//CA("theSelectedWidget == kRefreshCellRadioButtonWidgetID");
			refreshTableByAttribute = kTrue;
			iscontentbuttonselected = kTrue;
			issrtucturebuttenselected= kFalse;

			IsBookrefreshSelected=kFalse;
			IsFrameRefreshSelected=kTrue;

			//ptrIAppFramework->clearAllStaticObjects();

			bool16 result1 = ptrIAppFramework->getLoginStatus();
			if(!result1)
				return;

			IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();//Cs4		 
			if(iDoc== NULL) { 
				//CA("iDoc== NULL in Handle Drop Down ---1");
			return;
			}

			if(ReloadState == -1)
			{
				 ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhSelectionObserver::Update::ReloadState == -1");															
				return;
			}

			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");															
				return;
			}

			Refresh refresh;
			//SDKListBoxHelper listBox(this, kRfhPluginID);

			switch(ReloadState)
			{
				// Awasthi swap 0 and 3
				case 1 /*0*/:	
						{
							//CA("Content case 0");
							Mediator::editBoxView->Disable();
							Mediator::EditBoxTextControlData->SetString("");
							
							//listBox.EmptyCurrentListBox(Mediator::listControlView);
							rDataList.clear();
							OriginalrDataList.clear();
							UniqueDataList.clear();
							Mediator::RfhRefreshButtonView->Disable();
							
						}
					break;
				case 0 /*1*/:
					{	
						//CA("Content case 1");
						Mediator::selectedRadioButton = 3;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						//CA("case 1.1");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
                         //CA("case 1.5");
						if(GroupFlag==3)
						{
							//CA("group flag 3");
							reSortTheListForElem();						
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
							///CA("case 1.6");
						}
						else if(GroupFlag==2)
						{
							//CA("GroupFlag==2");
							reSortTheListForObject();
							fillDataInListBox(2);
						    Mediator::isGroupByObj=kTrue;
						    isObjectGroup=kTrue;
						    isElementoptSelected= kFalse;
							//CA("else 1.66");
						}	
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						}	
						
					}
					break;
				case 2:
					{
						//CA("Content case 2");
						Mediator::selectedRadioButton = 1;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
				
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						if(GroupFlag == 3)
						{
							//CA("GroupFlag == 3");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{	
							//CA("GroupFlag == 2");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);							
						}	
			        }
					break;

				case 3:
					{	
						//CA("Content case 3");
						//this code is added because when we select "page Range" ex.1-10 and 4 th page doesnt have tag then then list box 
						//is empty .then only 1-3 pages are refresh so that to handle this condition to refresh all 1-10 pages this code is 
						//written

						int32 size1=static_cast<int32>(textboxvaluevec.size());
						int32 siz=static_cast<int32>(rDataList.size());
						
						int32 start;
						int32 end;
						
						if(size1==2 && siz==0)
						{
							for(int32 k=0;k<1;k++)
							{
								start=textboxvaluevec.at(k);
								end=textboxvaluevec.at(k+1);
							}
							for(int32 pageNo=start;pageNo<=end;pageNo++)
							{
								InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
								if (iPanelControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");																					
									break;
								}

								IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
								SDKListBoxHelper listBox(this, kRfhPluginID);
								InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());
								Mediator::EditBoxTextControlData=obj;
								PMString pageNumber;
								pageNumber=obj->GetString();
								if(!pageNumber.NumUTF16TextChars())
								{
									//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::!pageNumber.NumUTF16TextChars");																					
									break;
								}
								Mediator::refreshPageNumber=pageNo;
								Mediator::refreshPageNumber--;
								
								UID pageUID;
								Refresh refresh;
								if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
								{	
									//ptrIAppFramework->LogError("AP7_RefreshContent::RfhSelectionObserver::Update::!refresh.isValidPageNumber");																					
									break;
								} 
								Mediator::selectedRadioButton=2;//Newvar
								refresh.GetPageDataInfo(Mediator::selectedRadioButton);
								int32 siz=static_cast<int32>(rDataList.size());
								
								if(siz>0)
								{
									Newvar=pageNo;
									if(GroupFlag==3)
									{
										//CA("GroupFlag==3");
										reSortTheListForElem();
										fillDataInListBox(3);
										Mediator::isGroupByObj=kFalse;
										isObjectGroup=kFalse;
										isElementoptSelected= kTrue;
									}
									else if(GroupFlag==2)
									{	
										//CA("GroupFlag==2");
										reSortTheListForObject();
										fillDataInListBox(2);
										Mediator::isGroupByObj=kTrue;
										isObjectGroup=kTrue;
										isElementoptSelected= kFalse;

									}
									else if(GroupFlag==1)
									{			
										//CA("GroupFlag==1");
										reSortTheListForUniqueAttr();
										fillDataInListBox(1);
									}
								}
								if(siz>0)
									break;
						  
							}
						}
						else if(size1==1)
						{
							InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
							if (iPanelControlData == nil)
							{
								ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");																					
								break;
							}

							IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
							SDKListBoxHelper listBox(this, kRfhPluginID);
							InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());
							Mediator::EditBoxTextControlData=obj;
							PMString pageNumber;
							pageNumber=obj->GetString();
							if(!pageNumber.NumUTF16TextChars())
							{
								//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::!pageNumber.NumUTF16TextChars");																					
								break;
							}
							Mediator::refreshPageNumber=pageNumber.GetAsNumber();
							Mediator::refreshPageNumber--;
							Newvar=Mediator::refreshPageNumber;
						
							UID pageUID;
							Refresh refresh;
							if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
							{	
								//ptrIAppFramework->LogError("AP7_RefreshContent::RfhSelectionObserver::Update::!refresh.isValidPageNumber");																					
								break;
							} 
							Mediator::selectedRadioButton=2;
							refresh.GetPageDataInfo(Mediator::selectedRadioButton);
							if(GroupFlag==3)
							{
								//CA("GroupFlag==3");
								reSortTheListForElem();
								fillDataInListBox(3);
								Mediator::isGroupByObj=kFalse;
								isObjectGroup=kFalse;
								isElementoptSelected= kTrue;
							}
							else if(GroupFlag==2)
							{	
								//CA("GroupFlag==2");
								reSortTheListForObject();
								fillDataInListBox(2);
								Mediator::isGroupByObj=kTrue;
								isObjectGroup=kTrue;
								isElementoptSelected= kFalse;

							}
							else if(GroupFlag==1)
							{			
								//CA("GroupFlag==1");
								reSortTheListForUniqueAttr();
								fillDataInListBox(1);
							}	
						}
					}					
					break;
				case 4:
					//CA("Content case 4");
						Mediator::selectedRadioButton = 4;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						if(GroupFlag == 3)
						{
							//CA("GroupFlag == 3");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{
							//CA("GroupFlag == 2");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						  
						}	
					break;				
			}
		}
		
		if(theSelectedWidget == kRefreshFullTableRadioButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kRefreshFullTableRadioButtonWidgetID");
			refreshTableByAttribute = kFalse;
			iscontentbuttonselected = kFalse;
			issrtucturebuttenselected= kTrue;

			IsFrameRefreshSelected=kTrue;
			IsBookrefreshSelected=kFalse;


		//	ptrIAppFramework->clearAllStaticObjects();

			bool16 result1 = ptrIAppFramework->getLoginStatus();
			if(!result1)
				return;

			IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();//Cs4
			if(iDoc== NULL) { 
				//CA("iDoc== NULL in Handle Drop Down ---1");
			return;
			}

			if(ReloadState == -1)
			{
				 ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhSelectionObserver::Update::ReloadState == -1");															
				return;
			}

			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");															
				return;
			}

			Refresh refresh;
			SDKListBoxHelper listBox(this, kRfhPluginID);

			switch(ReloadState)
			{
				// Awasthi swap 0 and 3
				case 1/*0*/:	
						{
							//CA("Structure case 0");
							Mediator::editBoxView->Disable();
							Mediator::EditBoxTextControlData->SetString("");
							
							listBox.EmptyCurrentListBox(Mediator::listControlView);
							rDataList.clear();
							OriginalrDataList.clear();
							UniqueDataList.clear();
							Mediator::RfhRefreshButtonView->Disable();
							
						}
					break;
				case 0/*1*/:
					{	
						//CA("Structure Refesh case 1");
						Mediator::selectedRadioButton = 3;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						//CA("case 1.1");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
                       //  CA("case 1.5");
						if(GroupFlag==3)
						{
							//CA("group flag 3");
							reSortTheListForElem();						
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
							//CA("case 1.6");
						}
						else if(GroupFlag==2)
						{
							//CA("GroupFlag==2");
							reSortTheListForObject();
							fillDataInListBox(2);
						    Mediator::isGroupByObj=kTrue;
						    isObjectGroup=kTrue;
						    isElementoptSelected= kFalse;
							//CA("else 1.66");
						}	
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						}	
						
					}
					break;
				case 2:
					{
						//CA("Structure Refesh case 2");
						Mediator::selectedRadioButton = 1;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
				
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						if(GroupFlag == 3)
						{
							//CA("GroupFlag == 3");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{	
							//CA("GroupFlag == 2");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);							
						}	
			        }
					break;

				case 3:
					{	
						//CA("Content case 3");
						//this code is added because when we select "page Range" ex.1-10 and 4 th page doesnt have tag then then list box 
						//is empty .then only 1-3 pages are refresh so that to handle this condition to refresh all 1-10 pages this code is 
						//written

						int32 size1=static_cast<int32>(textboxvaluevec.size());
						int32 siz=static_cast<int32>(rDataList.size());
						//PMString a3("rDataList.size :");
						//a3.AppendNumber(siz);
						////CA(a3);
						int32 start;
						int32 end;
						
						if(size1==2 && siz==0)
						{
							for(int32 k=0;k<1;k++)
							{
							start=textboxvaluevec.at(k);
							end=textboxvaluevec.at(k+1);
							}
							for(int32 pageNo=start;pageNo<=end;pageNo++)
							{
								InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
								if (iPanelControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");																					
									break;
								}

								IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
								SDKListBoxHelper listBox(this, kRfhPluginID);
								InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());
								Mediator::EditBoxTextControlData=obj;
								PMString pageNumber;
								pageNumber=obj->GetString();
								if(!pageNumber.NumUTF16TextChars())
								{
									//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::!pageNumber.NumUTF16TextChars");																					
									break;
								}
								Mediator::refreshPageNumber=pageNo;
								Mediator::refreshPageNumber--;
								
								UID pageUID;
								Refresh refresh;
								if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
								{	
									//ptrIAppFramework->LogError("AP7_RefreshContent::RfhSelectionObserver::Update::!refresh.isValidPageNumber");																					
									break;
								} 
								Mediator::selectedRadioButton=2;//Newvar
								refresh.GetPageDataInfo(Mediator::selectedRadioButton);
								int32 siz=static_cast<int32>(rDataList.size());
								if(siz>0)
								{
									Newvar=pageNo;
									if(GroupFlag==3)
									{
										//CA("GroupFlag==3");
										reSortTheListForElem();
										fillDataInListBox(3);
										Mediator::isGroupByObj=kFalse;
										isObjectGroup=kFalse;
										isElementoptSelected= kTrue;
									}
									else if(GroupFlag==2)
									{	
										//CA("GroupFlag==2");
										reSortTheListForObject();
										fillDataInListBox(2);
										Mediator::isGroupByObj=kTrue;
										isObjectGroup=kTrue;
										isElementoptSelected= kFalse;

									}
									else if(GroupFlag==1)
									{			
										//CA("GroupFlag==1");
										reSortTheListForUniqueAttr();
										fillDataInListBox(1);
									}
								}
								if(siz>0)
									break;
						  
							}
						}
						else
						{
							
						InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
						if (iPanelControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");																					
							break;
						}

						IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
						SDKListBoxHelper listBox(this, kRfhPluginID);
						InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());
						Mediator::EditBoxTextControlData=obj;
						PMString pageNumber;
						pageNumber=obj->GetString();
						if(!pageNumber.NumUTF16TextChars())
						{
							//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::!pageNumber.NumUTF16TextChars");																					
							break;
						}
						Mediator::refreshPageNumber=pageNumber.GetAsNumber();
						Mediator::refreshPageNumber--;
						Newvar=Mediator::refreshPageNumber;
						
						UID pageUID;
						Refresh refresh;
						if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
						{	
							//ptrIAppFramework->LogError("AP7_RefreshContent::RfhSelectionObserver::Update::!refresh.isValidPageNumber");																					
							break;
						} 
						Mediator::selectedRadioButton=2;
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						if(GroupFlag==3)
						{
							//CA("GroupFlag==3");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag==2)
						{	
							//CA("GroupFlag==2");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;

						}
						else if(GroupFlag==1)
						{			
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						}	
						}
			
					}					
					break;

				case 4:
					//CA("Structure Refesh case 4");
						Mediator::selectedRadioButton = 4;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						if(GroupFlag == 3)
						{
							//CA("GroupFlag == 3");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{
							//CA("GroupFlag == 2");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						  
						}	
					break;				
			}
	
		}

		//***Commented By sachin Sharma on 15/07/08
		//if (theSelectedWidget == kRfhPreferenceIconWidgetID && theChange == kTrueStateMessage)
		//{
		//	//CA("Preference");
		//	//DoRfhPreferenceDialog(); //Commented By Sachin Sharma on 15/07/08

		//}
		

//up to here-----------------

		/*if (theSelectedWidget == kRfhLocateButtonWidgetID
				&& theChange == kTrueStateMessage)
		{
			static int32 i=0;
			if(RefreshFlag)
				i=0;
			RefreshFlag = kFalse;
			Refresh refresh;
			
			if (i< ChangedTextList.size())
			{	
				refresh.SetFocusForText(ChangedTextList[i].BoxUIDRef,ChangedTextList[i].StartIndex, ChangedTextList[i].EndIndex );
				i++;
			}
			else
				Mediator::RfhLocateButtonView->Disable();

		}*/ // 21 Feb Vaibhav

	} while (kFalse);
}

void RfhSelectionObserver::UpdatePanel()
{
	//CA("updatepanel");
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
            break;
		}
		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::UpdatePanel::iPanelControlData is nil");		
			break;
		}

		IControlView* iControlView = iPanelControlData->FindWidget(kRfhOptionsDropDownWidgetID);
		if (iControlView == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::UpdatePanel::iControlView == nil");				
			break;
		}

		InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
		if (iDropDownListController == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::UpdatePanel::iDropDownListController == nil");						
			break;
		}
		InterfacePtr<IStringListControlData> iStringListControlData(iDropDownListController, UseDefaultIID());
		if (iStringListControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::UpdatePanel::iStringListControlData == nil");								
			break;
		}
	}while(kFalse);
}

IPanelControlData* RfhSelectionObserver::QueryPanelControlData()
{
	//CA("RfhSelectionObserver::QueryPanelControlData");
	IPanelControlData* iPanel = nil;
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
            break;
		}
		InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID());
		if (iWidgetParent == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::QueryPanelControlData::iWidgetParent == nil");										
			break;
		}

		InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
		if (iPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::QueryPanelControlData::iPanelControlData == nil");												
			break;
		}
		iPanelControlData->AddRef();
		iPanel = iPanelControlData;
	}
	while (false); 
	return iPanel;
}

void RfhSelectionObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	//CA("RfhSelectionObserver::AttachWidget");
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); 
}

void RfhSelectionObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	//CA("RfhSelectionObserver::DetachWidget");
	do
	{
		
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}

void RfhSelectionObserver::HandleDropDownListClick
(const ClassID& theChange, 
ISubject* theSubject, 
const PMIID& protocol, 
void* changedBy)
{
	//CA("HandleDropDownListClick");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
        return;
	}
	do
	{	
		AcquireWaitCursor awc;
		awc.Animate();
		
		//CA("RfhSelectionObserver::HandleDropDownListClick");
		InterfacePtr<IControlView> iControlView(theSubject, UseDefaultIID());
		if (iControlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::iControlView == nil");														
			break;
		}
		WidgetID widgetID = iControlView->GetWidgetID();
           
		if (widgetID.Get() == kRfhOptionsDropDownWidgetID)
		{	
			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::iPanelControlData == nil");																	
				break;
			}
			IControlView* iControlView = iPanelControlData->FindWidget(kRfhOptionsDropDownWidgetID);
			if (iControlView == nil)
			{
				break;
			}
			InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
			if (iDropDownListController == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::iDropDownListController == nil");																	
				break;
			}

			int32 selectedRowIndex=0; 
			selectedRowIndex = iDropDownListController->GetSelected(); 
			ReloadState = selectedRowIndex;
			//SelectedRowNo = selectedRowIndex; 

			PMString j("selectedRowIndex : ");
			j.AppendNumber(selectedRowIndex);
			//CA(j);

			Refresh refresh;
			SDKListBoxHelper listBox(this, kRfhPluginID);

			if (widgetID.Get() != kRfhRefreshButtonWidgetID)
				SelectBoxFlage = kTrue;

			switch(selectedRowIndex)
			{
				// Awasthi swap 0 and 3
				case  1/*0*/:	
						{
							//CA("---Select---");
							Mediator::editBoxView->Disable();
							Mediator::EditBoxTextControlData->SetString("");
							listBox.EmptyCurrentListBox(Mediator::listControlView);
							rDataList.clear();
							OriginalrDataList.clear();
							UniqueDataList.clear();
							Mediator::RfhRefreshButtonView->Disable();
							Mediator::RfhReloadButtonView->Disable();
							//Mediator::RfhLocateButtonView->Disable(); 21 Feb Vaibhav
							//isElementoptSelected= kFalse;
						}
					break;
				case 0/*1*/:
					{	
						//CA(" Selected Frames ");
						bool16 result1 = ptrIAppFramework->getLoginStatus();
						if(!result1)
							return;

						IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();		//Cs4
						if(iDoc== NULL) { 
							//CA("iDoc== NULL in Handle Drop Down ---1");
						return;
						}
						Mediator::RfhReloadButtonView->Enable();
						Mediator::selectedRadioButton = 3;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						//CA("RfhSelectionObserver::HandleDropDownListClick::case 1");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
                        //CA("case 1.5");
						if (widgetID.Get() != kRfhRefreshButtonWidgetID)
						{
							if(GroupFlag==3)
							{
								//CA("GroupFlag==3");
								reSortTheListForElem();						
								fillDataInListBox(3);
								Mediator::isGroupByObj=kFalse;
								isObjectGroup=kFalse;
								isElementoptSelected= kTrue;
								//CA("case 1.6");
							}
							else if(GroupFlag==2)
							{
								//CA("GroupFlag==2");
								reSortTheListForObject();
								fillDataInListBox(2);
								Mediator::isGroupByObj=kTrue;
								isObjectGroup=kTrue;
								isElementoptSelected= kFalse;
								//CA("else 1.66");
							}	
							else if(GroupFlag==1)
							{
								//CA("GroupFlag==1");
								reSortTheListForUniqueAttr();
								fillDataInListBox(1);
							  //  Mediator::isGroupByObj=kTrue;
							 /*   isObjectGroup=kTrue;
								isElementoptSelected= kFalse;*/
							}	
						}
					}
					break;
				case 2:
					{
						//CA(" Current Page ");
						bool16 result1 = ptrIAppFramework->getLoginStatus();
						if(!result1)
							return;

						IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();		//Cs4
						if(iDoc== NULL) { 
							//CA("iDoc== NULL in Handle Drop Down ---1");
						return;
						}
						Mediator::RfhReloadButtonView->Enable();
						Mediator::selectedRadioButton = 1;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
					////	InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
					////	if (layoutData == nil)
					////		break;
					////	UID pageUID = layoutData->GetPage();
					////	if(pageUID == kInvalidUID)
					////		break;
					////	IGeometry* spreadItem = layoutData->GetSpread();
					////	if(spreadItem == nil)
					////		break;
					////	InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
					/////	if (iSpread == nil)
					////		break;
					////	int pageNum=iSpread->GetPageIndex(pageUID);
					////	Mediator::refreshPageNumber=pageNum;		
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						if(GroupFlag == 3)
						{
							//CA("group flag");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{	//CA("Not group flag");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);							
						   /* Mediator::isGroupByObj=kTrue;
						    isObjectGroup=kTrue;
						    isElementoptSelected= kFalse;*/
							//CA("else 1.67");
						}	
			        }
					break;

				case 3:
					{	
						//CA("Page Range");
						bool16 result1 = ptrIAppFramework->getLoginStatus();
						if(!result1)
							return;

						//IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();		//Cs4 //og

						InterfacePtr<IDocument> iDoc(Utils<ILayoutUIUtils>()->GetFrontDocument());
						if(iDoc== NULL) { 
							//CA("iDoc== NULL in Handle Drop Down ---1");
						return;
						}
						Mediator::RfhReloadButtonView->Enable();
						Mediator::selectedRadioButton=2;
						listBox.EmptyCurrentListBox(Mediator::listControlView);
						IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
						editBox->Enable(kTrue, kFalse);
						InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());						
						PMString pg("");
						obj->SetString(pg);

						rDataList.clear();
						OriginalrDataList.clear();

						iPanelControlData->SetKeyboardFocus(kRfhPageNumWidgetID);

						//Mediator::RfhLocateButtonView->Disable();  21 febVaibhav
						Mediator::RfhRefreshButtonView->Disable();
						/*InterfacePtr<IDialogController> theCntrller(this,IID_IDIALOGCONTROLLER);
						if(theCntrller==nil)
						{
							CA("theCntrller is nil");
							return;
						}
						theCntrller->SetTextValueControlState(kRfhPageNumWidgetID, ITextValue::kNormal );*/

/*						InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());
						
						PMString pageNumber;
						pageNumber=obj->GetString();
						if(!pageNumber.ObsoleteLength())
						{
						//	CAlert::InformationAlert("Please enter the Page Number you want to refresh");
							break;
						}
						
						Mediator::refreshPageNumber=pageNumber.GetAsNumber();
						Mediator::refreshPageNumber--;
						
						UID pageUID;
						if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
						{
							CAlert::InformationAlert("Please enter a valid page number");
							break;
						} 
						
						//Try to set the spread as the current spread

					//	if(!setSpreadFromPage(pageUID))
					//	{
					//		CAlert::InformationAlert("Some unknown error occured");
					//		break;
					//	}
						
					//	InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
					//	if (layoutData == nil)
					//		break;
						
					//	if(pageUID == kInvalidUID)
					//		break;
						
					//	IGeometry* spreadItem = layoutData->GetSpread();
					//	if(spreadItem == nil)
					//		break;
					//	CA("Step1Valid12");
					//	InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
					//	if (iSpread == nil)
					//		break;
					//	CA("Step1Valid13");
					//	int pageNum=iSpread->GetPageIndex(pageUID);
						
					//	Mediator::refreshPageNumber=pageNum;//This is the actual page position
												
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						fillDataInListBox(iPanelControlData);
*/
						//CA("last");
						Mediator::isGroupByObj=kTrue;
						isObjectGroup=kTrue;
						isElementoptSelected= kFalse;						
					}					
					break;

				case 4:
					{
						//CA("Entire Document ");
						Mediator::isFrameRefresh = kTrue;
						bool16 result1 = ptrIAppFramework->getLoginStatus();
						if(!result1)
							return;

						IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();		//Cs4
						if(iDoc== NULL) { 
							//CA("iDoc== NULL in Handle Drop Down ---1");
						return;
						}
						Mediator::RfhReloadButtonView->Enable();
						Mediator::selectedRadioButton = 4;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						if(GroupFlag == 3)
						{
							//CA("group flag");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{
							//CA("gp");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("else 11");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						   /* Mediator::isGroupByObj=kTrue;
						    isObjectGroup=kTrue;
						    isElementoptSelected= kFalse;*/
							//CA("else 1.68");
						}	
						//CA("case 4 selected");
					}
					break;
				// End Awasthi Swap
			}
		}

		if (widgetID.Get() == kRfhRefreshButtonWidgetID)
		{	
			PMString optionSelected("");
			if(Mediator::selectedRadioButton == 1)			
				optionSelected.Append("for Current Page option ****");
			else if(Mediator::selectedRadioButton == 2)
				optionSelected.Append("for Page Number option ****");				
			else if(Mediator::selectedRadioButton == 3)
				optionSelected.Append("for Selected Frame option ****");
			else if(Mediator::selectedRadioButton == 4)
				optionSelected.Append("for Entire Document option ****");

			ptrIAppFramework->LogDebug("**** starting refresh process  " + optionSelected);	
	
	//		if(!isObjectGroup)
	//			reSortTheListForObject();
			//CA("Inside widgetID.Get() == kRfhRefreshButtonWidgetID");

			bool16 result1 = ptrIAppFramework->getLoginStatus();
			if(!result1)
				return;


			sectionIds.clear();
			MapForItemsPerSection.clear();
			MapForProductsPerSection.clear();

			//ptrIAppFramework->GetSectionData_clearSectionDataCache();
			//get Section Data
			GetSectionData getSectionData;
				
			getSectionData.addCustomTablePresentFlag = kTrue;
			getSectionData.addChildCopyAndImageFlag = kTrue;	
			getSectionData.addProductChildCopyAndImageFlag = kTrue;
			getSectionData.addDBTableFlag = kTrue;
			getSectionData.addProductDBTableFlag = kTrue;
			getSectionData.addCopyFlag = kTrue;
			getSectionData.addProductCopyFlag = kTrue;
			
			getSectionData.isOneSource = kFalse;
			
			vec_GetMultipleSectionDataPtr vec_ptr = new vec_GetMultipleSectionData; 
			if(vec_ptr == NULL)
			{
				return;
			}
			double prevSectionId = -1;

			//int32 itemTypeId =  ptrIAppFramework->TYPEMngr_getObjectTypeID("ITEM_LEVEL");
			//int32 productTypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");
			//
			//PMString str("itemTypeId = ");
			//str.AppendNumber(itemTypeId);
			//str.Append(", productTypeId = ");
			//str.AppendNumber(productTypeId);
			//CA(str);

			// Code to set check box selected in unique & rdatalist.

			if(UniqueNodeIds.size() > 0)
			{
			
				set<NodeID>::iterator it;
				for(it=UniqueNodeIds.begin(); it!=UniqueNodeIds.end(); it++ )
				{
					NodeID nid= (NodeID)*it;

					InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::listControlView, UseDefaultIID());
					if(!treeViewMgr)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::!treeViewMgr");					
						continue ;
					}
					//QueryWidgetFromNode 

					InterfacePtr<IPanelControlData> panelControlData(treeViewMgr->QueryWidgetFromNode(nid), UseDefaultIID());
					ASSERT(panelControlData);
					if(panelControlData==nil)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::panelControlData is nil");		
						continue ;
					}

					IControlView* checkBoxCntrlView=panelControlData->FindWidget(kRfhCheckBoxWidgetID);
					if(checkBoxCntrlView==nil) 
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::checkBoxCntrlView == nil");
						continue ;
					}
		
					InterfacePtr<ITriStateControlData> itristatecontroldata(checkBoxCntrlView, UseDefaultIID());
					if(itristatecontroldata==nil)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::itristatecontroldata is nil");		
						continue ;
					}

					TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);

					int32 uid= uidNodeIDTemp->Get();
					int32 TextFrameuid = uidNodeIDTemp->Get();
					RfhDataNode rNode;
					RFHTreeDataCache dc1;

					dc1.isExist(uid, rNode);

					PMString fieldName = rNode.getFieldName();

					for(int32 k =0; k < UniqueDataList.size(); k ++)
					{
						if(UniqueDataList[k].name == fieldName)
						{
							if(itristatecontroldata->IsSelected())
								UniqueDataList[k].isSelected = kTrue;
							else
								UniqueDataList[k].isSelected = kFalse;

							for(int j=0; j<rDataList.size(); j++)
							{				
								if(UniqueDataList[k].elementID == rDataList[j].elementID /*&& UniqueDataList[k].TypeID ==  rDataList[j].TypeID*/ && UniqueDataList[k].whichTab == rDataList[j].whichTab)
								{	
									if((UniqueDataList[k].whichTab != 4 || UniqueDataList[k].isImageFlag == kTrue || UniqueDataList[k].isTableFlag == kTrue) && !(UniqueDataList[k].elementID == - 121 && UniqueDataList[k].isTableFlag == kFalse))
									{						
										if( UniqueDataList[k].TypeID !=  rDataList[j].TypeID)
											continue;							
									}
									if(UniqueDataList[k].whichTab == 4 && UniqueDataList[k].elementID == -101 ) // for Item Table in Tabbed text
									{						
										if( UniqueDataList[k].TypeID !=  rDataList[j].TypeID)
											continue;							
									}					
									rDataList[j].isSelected = UniqueDataList[k].isSelected;				
								}
							}
						}
					}
					
				}
			}


			
			

			
			ptrIAppFramework->LogDebug("**** Scanning rDataList started ****");	
			for(int i=0; i<rDataList.size(); i++)
			{ 
				if(rDataList[i].isSelected)
				{
					if(rDataList[i].publicationID == -1 || rDataList[i].objectID == -1){
						continue;
					}
					getSectionData.isEventField = kTrue;
					//////////for SectionIds,publicationId,languageId
					if(prevSectionId != rDataList[i].publicationID)
					{
						bool16 sectionIdAlreadyExist = kFalse;
						UniqueIds::iterator itr;
						itr = sectionIds.find(rDataList[i].publicationID);
						if(itr == sectionIds.end())
						{
							sectionIds.insert(rDataList[i].publicationID);
						}
						else
						{
							sectionIdAlreadyExist = kTrue;
						}

						if(!sectionIdAlreadyExist)
						{
							GetMultipleSectionData ObjGetMultipleSectionData;
							ObjGetMultipleSectionData.sectionId = rDataList[i].publicationID;
							ObjGetMultipleSectionData.languageId = rDataList[i].LanguageID;
							
							CPubModel  cPubModelObj = ptrIAppFramework->getpubModelByPubID(rDataList[i].publicationID,rDataList[i].LanguageID);
							ObjGetMultipleSectionData.publicationId = cPubModelObj.getRootID();	

							vec_ptr->push_back(ObjGetMultipleSectionData);
						}

						prevSectionId = rDataList[i].publicationID;
					}					

					if(rDataList[i].whichTab == 3)
					{
						multiSectionMap::iterator itr;
						UniqueIds::iterator productIdsItr;

						itr = MapForProductsPerSection.find(rDataList[i].publicationID);
						if(itr != MapForProductsPerSection.end())
						{
							productIdsItr = itr->second.find(rDataList[i].objectID);
							if(productIdsItr == itr->second.end())
								itr->second.insert(rDataList[i].objectID);
						}
						else
						{
							UniqueIds productIds;
							productIds.insert(rDataList[i].objectID);

							MapForProductsPerSection.insert(multiSectionMap::value_type(rDataList[i].publicationID, productIds));
						}

						/*bool16 idFound = kFalse;
						for(int32 IdIndex = 0; IdIndex < getSectionData.productIdList.size(); IdIndex++)
						{
							if(getSectionData.productIdList[IdIndex] == rDataList[i].objectID)
							{
								idFound = kTrue;
								break;
							}
						}
						if(!idFound)
							getSectionData.productIdList.push_back(rDataList[i].objectID);*/
					}
					else if(rDataList[i].childTag == 1)
					{						
						if(rDataList[i].whichTab == 4) //if(rDataList[i].parentTypeId == itemTypeId)
						{
							multiSectionMap::iterator itr;
							UniqueIds::iterator itemIdsItr;
							itr = MapForItemsPerSection.find(rDataList[i].publicationID);
							if(itr != MapForItemsPerSection.end())
							{
								if(rDataList[i].isImageFlag == 1 && rDataList[i].isSprayItemPerFrame != -1 && (rDataList[i].objectID != rDataList[i].isAutoResize))
								{
									// This image frame is of child item in SprayItemPerFrame. In this case we have parentItemId in isAutoResize.
									itemIdsItr = itr->second.find(rDataList[i].isAutoResize);
									if(itemIdsItr == itr->second.end())
									{
										itr->second.insert(rDataList[i].isAutoResize);
									}
								}								
								else
								{
									itemIdsItr = itr->second.find(rDataList[i].objectID);
									if(itemIdsItr == itr->second.end())
									{
										itr->second.insert(rDataList[i].objectID);
									}
								}
							}
							else
							{
								if(rDataList[i].isImageFlag == 1 && rDataList[i].isSprayItemPerFrame != -1 && (rDataList[i].objectID != rDataList[i].isAutoResize))
								{
									itemIdsItr = itr->second.find(rDataList[i].isAutoResize);
									if(itemIdsItr == itr->second.end())
									{
										itr->second.insert(rDataList[i].isAutoResize);
									}
								}								
								else
								{
									UniqueIds itemIds;
									itemIds.insert(rDataList[i].objectID);

									MapForItemsPerSection.insert(multiSectionMap::value_type(rDataList[i].publicationID, itemIds));
								}
							}

							/*bool16 idFound = kFalse;
							for(int32 IdIndex = 0; IdIndex < getSectionData.itemIdList.size(); IdIndex++)
							{
								if(getSectionData.itemIdList[IdIndex] == rDataList[i].objectID)
								{
									idFound = kTrue;
									break;
								}

							}
							if(!idFound)
								getSectionData.itemIdList.push_back(rDataList[i].objectID);*/
						}
						else if(rDataList[i].whichTab == 3) //if(rDataList[i].parentTypeId == productTypeId)
						{
							multiSectionMap::iterator itr;
							UniqueIds::iterator productIdsItr;

							itr = MapForProductsPerSection.find(rDataList[i].publicationID);
							if(itr != MapForProductsPerSection.end())
							{
								productIdsItr = itr->second.find(rDataList[i].objectID);
								if(productIdsItr == itr->second.end())
									itr->second.insert(rDataList[i].objectID);
							}
							else
							{
								UniqueIds productIds;
								productIds.insert(rDataList[i].objectID);

								MapForProductsPerSection.insert(multiSectionMap::value_type(rDataList[i].publicationID, productIds));
							}

							/*bool16 idFound = kFalse;
							for(int32 IdIndex = 0; IdIndex < getSectionData.productIdList.size(); IdIndex++)
							{
								if(getSectionData.productIdList[IdIndex] == rDataList[i].objectID)
								{
									idFound = kTrue;
									break;
								}
							}
							if(!idFound)
								getSectionData.productIdList.push_back(rDataList[i].objectID);*/
						}
					}
					else if(rDataList[i].whichTab == 4)
					{
						multiSectionMap::iterator itr;
						UniqueIds::iterator itemIdsItr;
						itr = MapForItemsPerSection.find(rDataList[i].publicationID);
						if(itr != MapForItemsPerSection.end())
						{
							if(rDataList[i].isImageFlag == 1 && rDataList[i].isSprayItemPerFrame != -1 && (rDataList[i].objectID != rDataList[i].isAutoResize))
							{
								// this image frame is of child item in SprayItemPerFrame.In this case we have parentItemId in isAutoResize.
								itemIdsItr = itr->second.find(rDataList[i].isAutoResize);
								if(itemIdsItr == itr->second.end())
								{
									itr->second.insert(rDataList[i].isAutoResize);
								}

							}							
							else
							{
								itemIdsItr = itr->second.find(rDataList[i].objectID);
								if(itemIdsItr == itr->second.end())
								{
									itr->second.insert(rDataList[i].objectID);
								}
							}
						}
						else
						{
							//commented by Amarjit patil
							//if(rDataList[i].isImageFlag == 1 && rDataList[i].isSprayItemPerFrame != -1 && (rDataList[i].objectID != rDataList[i].isAutoResize))
							//{
							//	// this image frame is of child item in SprayItemPerFrame.In This case parent Item Id is present in isAutoResize.
							//	itemIdsItr = itr->second.find(rDataList[i].isAutoResize);
							//	if(itemIdsItr == itr->second.end())
							//	{
							//		itr->second.insert(rDataList[i].isAutoResize);
							//	}
							//}							
							//else
							//{
							double ttype=rDataList[i].TypeID;
							 PMString ww("typeId123 :");
							 ww.AppendNumber(ttype);
							 //CA(ww);

								UniqueIds itemIds;
								itemIds.insert(rDataList[i].objectID);

								MapForItemsPerSection.insert(multiSectionMap::value_type(rDataList[i].publicationID, itemIds));
							//}
						}
						
						/*bool16 idFound = kFalse;
						for(int32 IdIndex = 0; IdIndex < getSectionData.itemIdList.size(); IdIndex++)
						{
							if(getSectionData.itemIdList[IdIndex] == rDataList[i].objectID)
							{
								idFound = kTrue;
								break;
							}

						}
						if(!idFound)
							getSectionData.itemIdList.push_back(rDataList[i].objectID);*/
					}
					else if(rDataList[i].whichTab == 5)
					{
						/*bool16 idFound = kFalse;
						for(int32 IdIndex = 0; IdIndex < getSectionData.hybridIdList.size(); IdIndex++)
						{
							if(getSectionData.hybridIdList[IdIndex] == rDataList[i].objectID)
							{
								idFound = kTrue;
								break;
							}

						}
						if(!idFound)
							getSectionData.hybridIdList.push_back(rDataList[i].objectID);*/
					}


					if(rDataList[i].isTableFlag)
					{
						//CA("Table Present");
						if(rDataList[i].elementID == -115 && rDataList[i].whichTab == 3)
						{
							getSectionData.addHyTableFlag = kTrue;							
						}
						else if(rDataList[i].elementID == -115 && rDataList[i].whichTab == 4)
						{
							getSectionData.addHyTableFlag = kTrue;							
						}
						else if(rDataList[i].elementID == -115 && rDataList[i].whichTab == 5)
						{
							getSectionData.addHyTableFlag = kTrue;							
						}
						else if(rDataList[i].whichTab == 3)
						{
							getSectionData.addProductDBTableFlag = kTrue;												
						}
						else if(rDataList[i].whichTab == 4)
						{
							getSectionData.addDBTableFlag = kTrue;							
						}
						continue;
					}
					if(rDataList[i].isImageFlag == 1)
					{
						//objCSprayStencilInfo.AssetIds.push_back(tagInfo.typeId);
						
						if(rDataList[i].whichTab == 3)
						{
							if(rDataList[i].TypeID <= -207 && rDataList[i].TypeID >= -221)
								getSectionData.addProductBMSAssetsFlag = kTrue;
							else if(rDataList[i].elementID > 0)
							{
								getSectionData.addProductPVMPVAssetFlag = kTrue;
								if(getSectionData.productPVAssetIdList.size()>0)
								{
									bool16 isAlreadyPresent = kFalse;
									for(int32 index = 0; index < getSectionData.productPVAssetIdList.size(); index++)
									{
										if(getSectionData.productPVAssetIdList[index] == rDataList[i].elementID)
										{
											isAlreadyPresent = kTrue;
											break;
										}
									}

									if(!isAlreadyPresent)
										getSectionData.productPVAssetIdList.push_back(rDataList[i].elementID);
								}
								else
									getSectionData.productPVAssetIdList.push_back(rDataList[i].elementID);
							}
							else
								getSectionData.addProductImageFlag = kTrue;

						}
						else if(rDataList[i].whichTab == 4)
						{
							getSectionData.addProductChildCopyAndImageFlag = kTrue;

							if(rDataList[i].TypeID <= -207 && rDataList[i].TypeID >= -221)
							{
								getSectionData.addImageFlag = kTrue;
								getSectionData.addItemBMSAssetsFlag = kTrue;
							}
							else if(rDataList[i].elementID > 0)
							{
								getSectionData.addItemPVMPVAssetFlag = kTrue;
								if(getSectionData.itemPVAssetIdList.size()>0)
								{
									bool16 isAlreadyPresent = kFalse;
									for(int32 index = 0; index < getSectionData.itemPVAssetIdList.size(); index++)
									{
										if(getSectionData.itemPVAssetIdList[index] == rDataList[i].elementID)
										{
											isAlreadyPresent = kTrue;
											break;
										}
									}

									if(!isAlreadyPresent)
										getSectionData.itemPVAssetIdList.push_back(rDataList[i].elementID);
								}
								else
									getSectionData.itemPVAssetIdList.push_back(rDataList[i].elementID);
							}
							else
								getSectionData.addImageFlag = kTrue;

						}
						else if(rDataList[i].whichTab == 5)
						{
							if(rDataList[i].TypeID == -222 || rDataList[i].TypeID == -223)
								getSectionData.addPubLogoAssetFlag = kTrue;
							//else if(tagInfo.colno == -28)
							{
								getSectionData.addSectionPVMPVAssetFlag = kTrue;
								/*if(getSectionData.sectionPVAssetIdList.size()>0)
								{
									bool16 isAlreadyPresent = kFalse;
									for(int32 index = 0; index < getSectionData.sectionPVAssetIdList.size(); index++)
									{
										if(getSectionData.sectionPVAssetIdList[index] == rDataList[i].elementID)
										{
											isAlreadyPresent = kTrue;
											break;
										}
									}

									if(!isAlreadyPresent)
										getSectionData.sectionPVAssetIdList.push_back(rDataList[i].elementID);
								}
								else
									getSectionData.sectionPVAssetIdList.push_back(rDataList[i].elementID);*/
							}
							//else if(tagInfo.colno == -27)
							{
								getSectionData.addPublicationPVMPVAssetFlag = kTrue;
								/*if(getSectionData.publicationPVAssetIdList.size()>0)
								{
									bool16 isAlreadyPresent = kFalse;
									for(int32 index = 0; index < getSectionData.publicationPVAssetIdList.size(); index++)
									{
										if(getSectionData.publicationPVAssetIdList[index] == rDataList[i].elementID)
										{
											isAlreadyPresent = kTrue;
											break;
										}
									}

									if(!isAlreadyPresent)
										getSectionData.publicationPVAssetIdList.push_back(rDataList[i].elementID);
								}
								else
									getSectionData.publicationPVAssetIdList.push_back(rDataList[i].elementID);*/
							}
							//else if(tagInfo.colno > 0)
							{
								getSectionData.addCatagoryPVMPVAssetFlag = kTrue;
								/*if(getSectionData.catagoryPVAssetIdList.size()>0)
								{
									bool16 isAlreadyPresent = kFalse;
									for(int32 index = 0; index < getSectionData.catagoryPVAssetIdList.size(); index++)
									{
										if(getSectionData.catagoryPVAssetIdList[index] == rDataList[i].elementID)
										{
											isAlreadyPresent = kTrue;
											break;
										}
									}

									if(!isAlreadyPresent)
										getSectionData.catagoryPVAssetIdList.push_back(rDataList[i].elementID);
								}
								else
									getSectionData.catagoryPVAssetIdList.push_back(rDataList[i].elementID);*/
							}
							/*else*/ if(rDataList[i].catLevel < 0 )
							{
								getSectionData.addEventSectionImages = kTrue;
								if(getSectionData.eventSectionAssetIdList.size()>0)
								{
									bool16 isAlreadyPresent = kFalse;
									for(int32 index = 0; index < getSectionData.eventSectionAssetIdList.size(); index++)
									{
										if(getSectionData.eventSectionAssetIdList[index] == rDataList[i].TypeID)
										{
											isAlreadyPresent = kTrue;
											break;
										}
									}

									if(!isAlreadyPresent)
										getSectionData.eventSectionAssetIdList.push_back(rDataList[i].TypeID);
								}
								else
									getSectionData.eventSectionAssetIdList.push_back(rDataList[i].TypeID);
							}
							else if(rDataList[i].catLevel > 0)
							{
								getSectionData.addCategoryImages = kTrue;
								if(getSectionData.categoryAssetIdList.size()>0)
								{
									bool16 isAlreadyPresent = kFalse;
									for(int32 index = 0; index< getSectionData.categoryAssetIdList.size(); index++)
									{
										if(getSectionData.categoryAssetIdList[index] == rDataList[i].TypeID)
										{
											isAlreadyPresent = kTrue;
											break;
										}
									}

									if(!isAlreadyPresent)
										getSectionData.categoryAssetIdList.push_back(rDataList[i].TypeID);
								}
								else
									getSectionData.categoryAssetIdList.push_back(rDataList[i].TypeID);
							}
						}	
						continue;
					}					

					if(rDataList[i].isEventField)
						getSectionData.isEventField = kTrue;
				}
			}

			ptrIAppFramework->LogDebug("**** Scanning rDataList finished ****");	

			getSectionData.isGetWholePublicationOrCatagoryDataFlag = kFalse;	

			if(vec_ptr->size() == 1)
			{	
				isMultipleSectionsAvailable = kFalse;
				getSectionData.SectionId = vec_ptr->at(0).sectionId;
				getSectionData.PublicationId = vec_ptr->at(0).publicationId;
				getSectionData.languageId = vec_ptr->at(0).languageId;	

				

				multiSectionMap::iterator itr;
				itr = MapForItemsPerSection.find(getSectionData.SectionId);
				if(itr != MapForItemsPerSection.end())
				{
					UniqueIds itemIds = itr->second;
					UniqueIds::iterator itemIdsItr;
					for(itemIdsItr = itemIds.begin(); itemIdsItr != itemIds.end(); ++itemIdsItr)
						getSectionData.itemIdList.push_back(*itemIdsItr);
				}
				
				itr = MapForProductsPerSection.find(getSectionData.SectionId);
				if(itr != MapForProductsPerSection.end())
				{
					UniqueIds::iterator productIdItr;
					UniqueIds productIds = itr->second;
					for(productIdItr = productIds.begin(); productIdItr != productIds.end(); ++productIdItr)
						getSectionData.productIdList.push_back(*productIdItr);
				}
				
				ptrIAppFramework->LogDebug("**** going to GetSectionData ****");	
				//ptrIAppFramework->GetSectionData_getDataForPubOrCat(getSectionData);
				ptrIAppFramework->LogDebug("**** returned form GetSectionData ****");	
			}else if(vec_ptr->size() > 1)
			{
				isMultipleSectionsAvailable = kTrue;
				
				multiSectionMap::iterator itr;
				for(int32 i = 0; i < vec_ptr->size(); ++i)
				{						
					itr = MapForItemsPerSection.find(vec_ptr->at(i).sectionId);
					if(itr != MapForItemsPerSection.end())
					{
						vec_ptr->at(i).itemIds = itr->second;
					}
					
					itr = MapForProductsPerSection.find(vec_ptr->at(i).sectionId);
					if(itr != MapForProductsPerSection.end())
					{
						vec_ptr->at(i).productIds = itr->second;
					}
				}

				ptrIAppFramework->LogDebug("**** going to getMultipleSectionData ****");	
				//ptrIAppFramework->getMultipleSectionData(*vec_ptr,getSectionData);
				ptrIAppFramework->LogDebug("**** returned from getMultipleSectionData ****");	
			}

			/*if(vec_ptr->size() == 1)
			{	
				isMultipleSectionsAvailable = kFalse;
				getSectionData.SectionId = vec_ptr->at(0).sectionId;
				getSectionData.PublicationId = vec_ptr->at(0).publicationId;
				getSectionData.languageId = vec_ptr->at(0).languageId;				
				
				ptrIAppFramework->GetSectionData_getDataForPubOrCat(getSectionData);
			}else if(vec_ptr->size() > 1)
			{
				isMultipleSectionsAvailable = kTrue;
				ptrIAppFramework->getMultipleSectionData(*vec_ptr,getSectionData);				
			}*/

			delete vec_ptr;
			vec_ptr = NULL;


		
			Refresh refresh;
			refresh.doRefresh(Mediator::selectedRadioButton);
		}
	}while(false); 
}
void RfhSelectionObserver::HandleDropDownListClickNewoption
(const ClassID& theChange, 
ISubject* theSubject, 
const PMIID& protocol, 
void* changedBy)
{
	//CA("HandleDropDownListClick");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
        return;
	}
	do
	{	
		AcquireWaitCursor awc;
		awc.Animate();
		
		//CA("RfhSelectionObserver::HandleDropDownListClick");
		InterfacePtr<IControlView> iControlView(theSubject, UseDefaultIID());
		if (iControlView == nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::iControlView == nil");														
			break;
		}
		WidgetID widgetID = iControlView->GetWidgetID();
           
		if (widgetID.Get() == kRfhOptionsDropDownWidgetID)
		{	
			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::iPanelControlData == nil");																	
				break;
			}
			IControlView* iControlView = iPanelControlData->FindWidget(kRfhOptionsDropDownWidgetID);
			if (iControlView == nil)
			{
				break;
			}
			InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
			if (iDropDownListController == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::iDropDownListController == nil");																	
				break;
			}

			int32 selectedRowIndex=0; 
			selectedRowIndex = iDropDownListController->GetSelected(); 
			ReloadState = selectedRowIndex;
			
			//SelectedRowNo = selectedRowIndex; 
			Refresh refresh;
			SDKListBoxHelper listBox(this, kRfhPluginID);

			switch(selectedRowIndex)
			{
				// Awasthi swap 0 and 3
				case 1/*0*/:	
						{
							//CA("---Select---");
							Mediator::editBoxView->Disable();
							Mediator::EditBoxTextControlData->SetString("");
							listBox.EmptyCurrentListBox(Mediator::listControlView);
							rDataList.clear();
							OriginalrDataList.clear();
							UniqueDataList.clear();
							Mediator::RfhRefreshButtonView->Disable();
							Mediator::RfhReloadButtonView->Disable();
							//Mediator::RfhLocateButtonView->Disable(); 21 Feb Vaibhav
							//isElementoptSelected= kFalse;
						}
					break;
				case 0/*1*/:
					{	
						//CA("Refresh Btn case 1 :");
						bool16 result1 = ptrIAppFramework->getLoginStatus();
						if(!result1)
							return;

						IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();		//Cs4
						if(iDoc== NULL) { 
							//CA("iDoc== NULL in Handle Drop Down ---1");
						return;
						}
						Mediator::RfhReloadButtonView->Enable();
						Mediator::selectedRadioButton = 3;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						//CA("RfhSelectionObserver::HandleDropDownListClick::case 1");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
                        //CA("case 1.5");
						if(GroupFlag==3)
						{
							//CA("GroupFlag==3");
							reSortTheListForElem();						
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
							//CA("case 1.6");
						}
						else if(GroupFlag==2)
						{
							//CA("GroupFlag==2");
							reSortTheListForObject();
							fillDataInListBox(2);
						    Mediator::isGroupByObj=kTrue;
						    isObjectGroup=kTrue;
						    isElementoptSelected= kFalse;
							//CA("else 1.66");
						}	
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						  //  Mediator::isGroupByObj=kTrue;
						 /*   isObjectGroup=kTrue;
						    isElementoptSelected= kFalse;*/
						}	
					}
					break;
				case 2:
					{
						//CA("Refresh Btn case 2");
						bool16 result1 = ptrIAppFramework->getLoginStatus();
						if(!result1)
							return;

						IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();		//Cs4
						if(iDoc== NULL) { 
							//CA("iDoc== NULL in Handle Drop Down ---1");
						return;
						}
						Mediator::RfhReloadButtonView->Enable();
						Mediator::selectedRadioButton = 1;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
					////	InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
					////	if (layoutData == nil)
					////		break;
					////	UID pageUID = layoutData->GetPage();
					////	if(pageUID == kInvalidUID)
					////		break;
					////	IGeometry* spreadItem = layoutData->GetSpread();
					////	if(spreadItem == nil)
					////		break;
					////	InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
					/////	if (iSpread == nil)
					////		break;
					////	int pageNum=iSpread->GetPageIndex(pageUID);
					////	Mediator::refreshPageNumber=pageNum;		
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						if(GroupFlag == 3)
						{
							//CA("group flag");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{	//CA("Not group flag");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);							
						   /* Mediator::isGroupByObj=kTrue;
						    isObjectGroup=kTrue;
						    isElementoptSelected= kFalse;*/
							//CA("else 1.67");
						}	
			        }
					break;

				case 3:
					{	
						//CA("Refresh Btn case 3");
						bool16 result1 = ptrIAppFramework->getLoginStatus();
						if(!result1)
							return;

						IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument();		//Cs4
						if(iDoc== NULL) { 
							//CA("iDoc== NULL in Handle Drop Down ---1");
						return;
						}
						Mediator::RfhReloadButtonView->Enable();
						Mediator::selectedRadioButton=2;
						listBox.EmptyCurrentListBox(Mediator::listControlView);
						IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
						editBox->Enable(kTrue, kFalse);
						InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());						
						PMString pg("");
						obj->SetString(pg);

						rDataList.clear();
						OriginalrDataList.clear();

						iPanelControlData->SetKeyboardFocus(kRfhPageNumWidgetID);

						//Mediator::RfhLocateButtonView->Disable();  21 febVaibhav
						Mediator::RfhRefreshButtonView->Disable();
						/*InterfacePtr<IDialogController> theCntrller(this,IID_IDIALOGCONTROLLER);
						if(theCntrller==nil)
						{
							CA("theCntrller is nil");
							return;
						}
						theCntrller->SetTextValueControlState(kRfhPageNumWidgetID, ITextValue::kNormal );*/

/*						InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());
						
						PMString pageNumber;
						pageNumber=obj->GetString();
						if(!pageNumber.ObsoleteLength())
						{
						//	CAlert::InformationAlert("Please enter the Page Number you want to refresh");
							break;
						}
						
						Mediator::refreshPageNumber=pageNumber.GetAsNumber();
						Mediator::refreshPageNumber--;
						
						UID pageUID;
						if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
						{
							CAlert::InformationAlert("Please enter a valid page number");
							break;
						} 
						
						//Try to set the spread as the current spread

					//	if(!setSpreadFromPage(pageUID))
					//	{
					//		CAlert::InformationAlert("Some unknown error occured");
					//		break;
					//	}
						
					//	InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
					//	if (layoutData == nil)
					//		break;
						
					//	if(pageUID == kInvalidUID)
					//		break;
						
					//	IGeometry* spreadItem = layoutData->GetSpread();
					//	if(spreadItem == nil)
					//		break;
					//	CA("Step1Valid12");
					//	InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
					//	if (iSpread == nil)
					//		break;
					//	CA("Step1Valid13");
					//	int pageNum=iSpread->GetPageIndex(pageUID);
						
					//	Mediator::refreshPageNumber=pageNum;//This is the actual page position
												
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						fillDataInListBox(iPanelControlData);
*/
						//CA("last");
						Mediator::isGroupByObj=kTrue;
						isObjectGroup=kTrue;
						isElementoptSelected= kFalse;						
					}					
					break;

				case 4:
					{
						//CA("Refresh Btn case 4");
						Mediator::isFrameRefresh = kTrue;
						bool16 result1 = ptrIAppFramework->getLoginStatus();
						if(!result1)
							return;

						IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();		//Cs4
						if(iDoc== NULL) { 
							//CA("iDoc== NULL in Handle Drop Down ---1");
						return;
						}
						Mediator::RfhReloadButtonView->Enable();
						Mediator::selectedRadioButton = 4;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						if(GroupFlag == 3)
						{
							//CA("group flag");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{
							//CA("gp");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("else 11");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						   /* Mediator::isGroupByObj=kTrue;
						    isObjectGroup=kTrue;
						    isElementoptSelected= kFalse;*/
							//CA("else 1.68");
						}	
						//CA("case 4 selected");
					}
					break;
				// End Awasthi Swap
			}
		}

		if (widgetID.Get() == kRfhRefreshButtonWidgetID)
		{	
			PMString optionSelected("");
			if(Mediator::selectedRadioButton == 1)			
				optionSelected.Append("for Current Page option ****");
			else if(Mediator::selectedRadioButton == 2)
				optionSelected.Append("for Page Number option ****");				
			else if(Mediator::selectedRadioButton == 3)
				optionSelected.Append("for Selected Frame option ****");
			else if(Mediator::selectedRadioButton == 4)
				optionSelected.Append("for Entire Document option ****");

			ptrIAppFramework->LogDebug("**** starting refresh process  " + optionSelected);	
	
	//		if(!isObjectGroup)
	//			reSortTheListForObject();
			//CA("Inside widgetID.Get() == kRfhRefreshButtonWidgetID");

			bool16 result1 = ptrIAppFramework->getLoginStatus();
			if(!result1)
				return;


//			sectionIds.clear();
//			MapForItemsPerSection.clear();
//			MapForProductsPerSection.clear();
//            CA("vvv");
//			ptrIAppFramework->GetSectionData_clearSectionDataCache();
//			//get Section Data
//			GetSectionData getSectionData;
//				
//			getSectionData.addCustomTablePresentFlag = kTrue;
//			getSectionData.addChildCopyAndImageFlag = kTrue;	
//			getSectionData.addProductChildCopyAndImageFlag = kTrue;
//			getSectionData.addDBTableFlag = kTrue;
//			getSectionData.addProductDBTableFlag = kTrue;
//			getSectionData.addCopyFlag = kTrue;
//			getSectionData.addProductCopyFlag = kTrue;
//			
//			getSectionData.isOneSource = kFalse;
//			
//			vec_GetMultipleSectionDataPtr vec_ptr = new vec_GetMultipleSectionData; 
//			if(vec_ptr == NULL)
//			{
//				return;
//			}
//			int32 prevSectionId = -1;
//            CA("A9");
//			int32 itemTypeId =  ptrIAppFramework->TYPEMngr_getObjectTypeID("ITEM_LEVEL");
//			int32 productTypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");
//			
//			PMString str("itemTypeId = ");
//			str.AppendNumber(itemTypeId);
//			str.Append(", productTypeId = ");
//			str.AppendNumber(productTypeId);
//			//CA(str);
//
////PMString t("rDataList.size()  :  ");			
////t.AppendNumber(static_cast<int32>(rDataList.size()));
////t.Append("\nDrop Down : UniqueDataList.size()	:	");
////t.AppendNumber(static_cast<int32>(UniqueDataList.size()));
////CA(t);
//			/*for(int32 int32 x= 0 ; x< UniqueDataList.size(); x++)
//			{
//				
//			}*/
//			ptrIAppFramework->LogDebug("**** Scanning rDataList started ****");	
//			for(int i=0; i<rDataList.size(); i++)
//			{ 
//				if(rDataList[i].isSelected)
//				{
//					if(rDataList[i].publicationID == -1 || rDataList[i].objectID == -1){
//						continue;
//					}
//					getSectionData.isEventField = kTrue;
//					//////////for SectionIds,publicationId,languageId
//					if(prevSectionId != rDataList[i].publicationID)
//					{
//						bool16 sectionIdAlreadyExist = kFalse;
//						UniqueIds::iterator itr;
//						itr = sectionIds.find(rDataList[i].publicationID);
//						if(itr == sectionIds.end())
//						{
//							sectionIds.insert(rDataList[i].publicationID);
//						}
//						else
//						{
//							sectionIdAlreadyExist = kTrue;
//						}
//
//						if(!sectionIdAlreadyExist)
//						{
//							GetMultipleSectionData ObjGetMultipleSectionData;
//							ObjGetMultipleSectionData.sectionId = rDataList[i].publicationID;
//							ObjGetMultipleSectionData.languageId = rDataList[i].LanguageID;
//							CA("A10");
//							CPubModel  cPubModelObj = ptrIAppFramework->getpubModelByPubID(rDataList[i].publicationID,rDataList[i].LanguageID);
//							ObjGetMultipleSectionData.publicationId = cPubModelObj.getRootID();	
//
//							vec_ptr->push_back(ObjGetMultipleSectionData);
//						}
//
//						prevSectionId = rDataList[i].publicationID;
//					}					
//
//					if(rDataList[i].whichTab == 3)
//					{
//						multiSectionMap::iterator itr;
//						UniqueIds::iterator productIdsItr;
//
//						itr = MapForProductsPerSection.find(rDataList[i].publicationID);
//						if(itr != MapForProductsPerSection.end())
//						{
//							productIdsItr = itr->second.find(rDataList[i].objectID);
//							if(productIdsItr == itr->second.end())
//								itr->second.insert(rDataList[i].objectID);
//						}
//						else
//						{
//							UniqueIds productIds;
//							productIds.insert(rDataList[i].objectID);
//
//							MapForProductsPerSection.insert(multiSectionMap::value_type(rDataList[i].publicationID, productIds));
//						}
//
//						/*bool16 idFound = kFalse;
//						for(int32 IdIndex = 0; IdIndex < getSectionData.productIdList.size(); IdIndex++)
//						{
//							if(getSectionData.productIdList[IdIndex] == rDataList[i].objectID)
//							{
//								idFound = kTrue;
//								break;
//							}
//						}
//						if(!idFound)
//							getSectionData.productIdList.push_back(rDataList[i].objectID);*/
//					}
//					else if(rDataList[i].childTag == 1)
//					{						
//						if(rDataList[i].parentTypeId == itemTypeId)
//						{
//							multiSectionMap::iterator itr;
//							UniqueIds::iterator itemIdsItr;
//							itr = MapForItemsPerSection.find(rDataList[i].publicationID);
//							if(itr != MapForItemsPerSection.end())
//							{
//								if(rDataList[i].isImageFlag == 1 && rDataList[i].isSprayItemPerFrame != -1 && (rDataList[i].objectID != rDataList[i].isAutoResize))
//								{
//									// This image frame is of child item in SprayItemPerFrame. In this case we have parentItemId in isAutoResize.
//									itemIdsItr = itr->second.find(rDataList[i].isAutoResize);
//									if(itemIdsItr == itr->second.end())
//									{
//										itr->second.insert(rDataList[i].isAutoResize);
//									}
//								}								
//								else
//								{
//									itemIdsItr = itr->second.find(rDataList[i].objectID);
//									if(itemIdsItr == itr->second.end())
//									{
//										itr->second.insert(rDataList[i].objectID);
//									}
//								}
//							}
//							else
//							{
//								if(rDataList[i].isImageFlag == 1 && rDataList[i].isSprayItemPerFrame != -1 && (rDataList[i].objectID != rDataList[i].isAutoResize))
//								{
//									itemIdsItr = itr->second.find(rDataList[i].isAutoResize);
//									if(itemIdsItr == itr->second.end())
//									{
//										itr->second.insert(rDataList[i].isAutoResize);
//									}
//								}								
//								else
//								{
//									UniqueIds itemIds;
//									itemIds.insert(rDataList[i].objectID);
//
//									MapForItemsPerSection.insert(multiSectionMap::value_type(rDataList[i].publicationID, itemIds));
//								}
//							}
//
//							/*bool16 idFound = kFalse;
//							for(int32 IdIndex = 0; IdIndex < getSectionData.itemIdList.size(); IdIndex++)
//							{
//								if(getSectionData.itemIdList[IdIndex] == rDataList[i].objectID)
//								{
//									idFound = kTrue;
//									break;
//								}
//
//							}
//							if(!idFound)
//								getSectionData.itemIdList.push_back(rDataList[i].objectID);*/
//						}
//						else if(rDataList[i].parentTypeId == productTypeId)
//						{
//							multiSectionMap::iterator itr;
//							UniqueIds::iterator productIdsItr;
//
//							itr = MapForProductsPerSection.find(rDataList[i].publicationID);
//							if(itr != MapForProductsPerSection.end())
//							{
//								productIdsItr = itr->second.find(rDataList[i].objectID);
//								if(productIdsItr == itr->second.end())
//									itr->second.insert(rDataList[i].objectID);
//							}
//							else
//							{
//								UniqueIds productIds;
//								productIds.insert(rDataList[i].objectID);
//
//								MapForProductsPerSection.insert(multiSectionMap::value_type(rDataList[i].publicationID, productIds));
//							}
//
//							/*bool16 idFound = kFalse;
//							for(int32 IdIndex = 0; IdIndex < getSectionData.productIdList.size(); IdIndex++)
//							{
//								if(getSectionData.productIdList[IdIndex] == rDataList[i].objectID)
//								{
//									idFound = kTrue;
//									break;
//								}
//							}
//							if(!idFound)
//								getSectionData.productIdList.push_back(rDataList[i].objectID);*/
//						}
//					}
//					else if(rDataList[i].whichTab == 4)
//					{
//						multiSectionMap::iterator itr;
//						UniqueIds::iterator itemIdsItr;
//						itr = MapForItemsPerSection.find(rDataList[i].publicationID);
//						if(itr != MapForItemsPerSection.end())
//						{
//							if(rDataList[i].isImageFlag == 1 && rDataList[i].isSprayItemPerFrame != -1 && (rDataList[i].objectID != rDataList[i].isAutoResize))
//							{
//								// this image frame is of child item in SprayItemPerFrame.In this case we have parentItemId in isAutoResize.
//								itemIdsItr = itr->second.find(rDataList[i].isAutoResize);
//								if(itemIdsItr == itr->second.end())
//								{
//									itr->second.insert(rDataList[i].isAutoResize);
//								}
//
//							}							
//							else
//							{
//								itemIdsItr = itr->second.find(rDataList[i].objectID);
//								if(itemIdsItr == itr->second.end())
//								{
//									itr->second.insert(rDataList[i].objectID);
//								}
//							}
//						}
//						else
//						{
//							if(rDataList[i].isImageFlag == 1 && rDataList[i].isSprayItemPerFrame != -1 && (rDataList[i].objectID != rDataList[i].isAutoResize))
//							{
//								// this image frame is of child item in SprayItemPerFrame.In This case parent Item Id is present in isAutoResize.
//								itemIdsItr = itr->second.find(rDataList[i].isAutoResize);
//								if(itemIdsItr == itr->second.end())
//								{
//									itr->second.insert(rDataList[i].isAutoResize);
//								}
//							}							
//							else
//							{
//								UniqueIds itemIds;
//								itemIds.insert(rDataList[i].objectID);
//
//								MapForItemsPerSection.insert(multiSectionMap::value_type(rDataList[i].publicationID, itemIds));
//							}
//						}
//						
//						/*bool16 idFound = kFalse;
//						for(int32 IdIndex = 0; IdIndex < getSectionData.itemIdList.size(); IdIndex++)
//						{
//							if(getSectionData.itemIdList[IdIndex] == rDataList[i].objectID)
//							{
//								idFound = kTrue;
//								break;
//							}
//
//						}
//						if(!idFound)
//							getSectionData.itemIdList.push_back(rDataList[i].objectID);*/
//					}
//					else if(rDataList[i].whichTab == 5)
//					{
//						/*bool16 idFound = kFalse;
//						for(int32 IdIndex = 0; IdIndex < getSectionData.hybridIdList.size(); IdIndex++)
//						{
//							if(getSectionData.hybridIdList[IdIndex] == rDataList[i].objectID)
//							{
//								idFound = kTrue;
//								break;
//							}
//
//						}
//						if(!idFound)
//							getSectionData.hybridIdList.push_back(rDataList[i].objectID);*/
//					}
//
//
//					if(rDataList[i].isTableFlag)
//					{
//						//CA("Table Present");
//						if(rDataList[i].elementID == -115 && rDataList[i].whichTab == 3)
//						{
//							getSectionData.addHyTableFlag = kTrue;							
//						}
//						else if(rDataList[i].elementID == -115 && rDataList[i].whichTab == 4)
//						{
//							getSectionData.addHyTableFlag = kTrue;							
//						}
//						else if(rDataList[i].elementID == -115 && rDataList[i].whichTab == 5)
//						{
//							getSectionData.addHyTableFlag = kTrue;							
//						}
//						else if(rDataList[i].whichTab == 3)
//						{
//							getSectionData.addProductDBTableFlag = kTrue;												
//						}
//						else if(rDataList[i].whichTab == 4)
//						{
//							getSectionData.addDBTableFlag = kTrue;							
//						}
//						continue;
//					}
//
//					if(rDataList[i].isImageFlag == 1)
//					{
//						//objCSprayStencilInfo.AssetIds.push_back(tagInfo.typeId);
//						
//						if(rDataList[i].whichTab == 3)
//						{
//							if(rDataList[i].TypeID <= -207 && rDataList[i].TypeID >= -221)
//								getSectionData.addProductBMSAssetsFlag = kTrue;
//							else if(rDataList[i].elementID > 0)
//							{
//								getSectionData.addProductPVMPVAssetFlag = kTrue;
//								if(getSectionData.productPVAssetIdList.size()>0)
//								{
//									bool16 isAlreadyPresent = kFalse;
//									for(int32 index = 0; index < getSectionData.productPVAssetIdList.size(); index++)
//									{
//										if(getSectionData.productPVAssetIdList[index] == rDataList[i].elementID)
//										{
//											isAlreadyPresent = kTrue;
//											break;
//										}
//									}
//
//									if(!isAlreadyPresent)
//										getSectionData.productPVAssetIdList.push_back(rDataList[i].elementID);
//								}
//								else
//									getSectionData.productPVAssetIdList.push_back(rDataList[i].elementID);
//							}
//							else
//								getSectionData.addProductImageFlag = kTrue;
//
//						}
//						else if(rDataList[i].whichTab == 4)
//						{
//							getSectionData.addProductChildCopyAndImageFlag = kTrue;
//
//							if(rDataList[i].TypeID <= -207 && rDataList[i].TypeID >= -221)
//							{
//								getSectionData.addImageFlag = kTrue;
//								getSectionData.addItemBMSAssetsFlag = kTrue;
//							}
//							else if(rDataList[i].elementID > 0)
//							{
//								getSectionData.addItemPVMPVAssetFlag = kTrue;
//								if(getSectionData.itemPVAssetIdList.size()>0)
//								{
//									bool16 isAlreadyPresent = kFalse;
//									for(int32 index = 0; index < getSectionData.itemPVAssetIdList.size(); index++)
//									{
//										if(getSectionData.itemPVAssetIdList[index] == rDataList[i].elementID)
//										{
//											isAlreadyPresent = kTrue;
//											break;
//										}
//									}
//
//									if(!isAlreadyPresent)
//										getSectionData.itemPVAssetIdList.push_back(rDataList[i].elementID);
//								}
//								else
//									getSectionData.itemPVAssetIdList.push_back(rDataList[i].elementID);
//							}
//							else
//								getSectionData.addImageFlag = kTrue;
//
//						}
//						else if(rDataList[i].whichTab == 5)
//						{
//							if(rDataList[i].TypeID == -222 || rDataList[i].TypeID == -223)
//								getSectionData.addPubLogoAssetFlag = kTrue;
//							//else if(tagInfo.colno == -28)
//							{
//								getSectionData.addSectionPVMPVAssetFlag = kTrue;
//								/*if(getSectionData.sectionPVAssetIdList.size()>0)
//								{
//									bool16 isAlreadyPresent = kFalse;
//									for(int32 index = 0; index < getSectionData.sectionPVAssetIdList.size(); index++)
//									{
//										if(getSectionData.sectionPVAssetIdList[index] == rDataList[i].elementID)
//										{
//											isAlreadyPresent = kTrue;
//											break;
//										}
//									}
//
//									if(!isAlreadyPresent)
//										getSectionData.sectionPVAssetIdList.push_back(rDataList[i].elementID);
//								}
//								else
//									getSectionData.sectionPVAssetIdList.push_back(rDataList[i].elementID);*/
//							}
//							//else if(tagInfo.colno == -27)
//							{
//								getSectionData.addPublicationPVMPVAssetFlag = kTrue;
//								/*if(getSectionData.publicationPVAssetIdList.size()>0)
//								{
//									bool16 isAlreadyPresent = kFalse;
//									for(int32 index = 0; index < getSectionData.publicationPVAssetIdList.size(); index++)
//									{
//										if(getSectionData.publicationPVAssetIdList[index] == rDataList[i].elementID)
//										{
//											isAlreadyPresent = kTrue;
//											break;
//										}
//									}
//
//									if(!isAlreadyPresent)
//										getSectionData.publicationPVAssetIdList.push_back(rDataList[i].elementID);
//								}
//								else
//									getSectionData.publicationPVAssetIdList.push_back(rDataList[i].elementID);*/
//							}
//							//else if(tagInfo.colno > 0)
//							{
//								getSectionData.addCatagoryPVMPVAssetFlag = kTrue;
//								/*if(getSectionData.catagoryPVAssetIdList.size()>0)
//								{
//									bool16 isAlreadyPresent = kFalse;
//									for(int32 index = 0; index < getSectionData.catagoryPVAssetIdList.size(); index++)
//									{
//										if(getSectionData.catagoryPVAssetIdList[index] == rDataList[i].elementID)
//										{
//											isAlreadyPresent = kTrue;
//											break;
//										}
//									}
//
//									if(!isAlreadyPresent)
//										getSectionData.catagoryPVAssetIdList.push_back(rDataList[i].elementID);
//								}
//								else
//									getSectionData.catagoryPVAssetIdList.push_back(rDataList[i].elementID);*/
//							}
//							/*else*/ if(rDataList[i].catLevel < 0 )
//							{
//								getSectionData.addEventSectionImages = kTrue;
//								if(getSectionData.eventSectionAssetIdList.size()>0)
//								{
//									bool16 isAlreadyPresent = kFalse;
//									for(int32 index = 0; index < getSectionData.eventSectionAssetIdList.size(); index++)
//									{
//										if(getSectionData.eventSectionAssetIdList[index] == rDataList[i].TypeID)
//										{
//											isAlreadyPresent = kTrue;
//											break;
//										}
//									}
//
//									if(!isAlreadyPresent)
//										getSectionData.eventSectionAssetIdList.push_back(rDataList[i].TypeID);
//								}
//								else
//									getSectionData.eventSectionAssetIdList.push_back(rDataList[i].TypeID);
//							}
//							else if(rDataList[i].catLevel > 0)
//							{
//								getSectionData.addCategoryImages = kTrue;
//								if(getSectionData.categoryAssetIdList.size()>0)
//								{
//									bool16 isAlreadyPresent = kFalse;
//									for(int32 index = 0; index< getSectionData.categoryAssetIdList.size(); index++)
//									{
//										if(getSectionData.categoryAssetIdList[index] == rDataList[i].TypeID)
//										{
//											isAlreadyPresent = kTrue;
//											break;
//										}
//									}
//
//									if(!isAlreadyPresent)
//										getSectionData.categoryAssetIdList.push_back(rDataList[i].TypeID);
//								}
//								else
//									getSectionData.categoryAssetIdList.push_back(rDataList[i].TypeID);
//							}
//						}	
//						continue;
//					}					
//
//					if(rDataList[i].isEventField)
//						getSectionData.isEventField = kTrue;
//				}
//			}
//
//			ptrIAppFramework->LogDebug("**** Scanning rDataList finished ****");	
//
//			getSectionData.isGetWholePublicationOrCatagoryDataFlag = kFalse;	
//
//			if(vec_ptr->size() == 1)
//			{	
//				isMultipleSectionsAvailable = kFalse;
//				getSectionData.SectionId = vec_ptr->at(0).sectionId;
//				getSectionData.PublicationId = vec_ptr->at(0).publicationId;
//				getSectionData.languageId = vec_ptr->at(0).languageId;	
//
//				
//
//				multiSectionMap::iterator itr;
//				itr = MapForItemsPerSection.find(getSectionData.SectionId);
//				if(itr != MapForItemsPerSection.end())
//				{
//					UniqueIds itemIds = itr->second;
//					UniqueIds::iterator itemIdsItr;
//					for(itemIdsItr = itemIds.begin(); itemIdsItr != itemIds.end(); ++itemIdsItr)
//						getSectionData.itemIdList.push_back(*itemIdsItr);
//				}
//				
//				itr = MapForProductsPerSection.find(getSectionData.SectionId);
//				if(itr != MapForProductsPerSection.end())
//				{
//					UniqueIds::iterator productIdItr;
//					UniqueIds productIds = itr->second;
//					for(productIdItr = productIds.begin(); productIdItr != productIds.end(); ++productIdItr)
//						getSectionData.productIdList.push_back(*productIdItr);
//				}
//				
//				ptrIAppFramework->LogDebug("**** going to GetSectionData ****");	
//				ptrIAppFramework->GetSectionData_getDataForPubOrCat(getSectionData);
//				ptrIAppFramework->LogDebug("**** returned form GetSectionData ****");	
//			}else if(vec_ptr->size() > 1)
//			{
//				isMultipleSectionsAvailable = kTrue;
//				
//				multiSectionMap::iterator itr;
//				for(int32 i = 0; i < vec_ptr->size(); ++i)
//				{						
//					itr = MapForItemsPerSection.find(vec_ptr->at(i).sectionId);
//					if(itr != MapForItemsPerSection.end())
//					{
//						vec_ptr->at(i).itemIds = itr->second;
//					}
//					
//					itr = MapForProductsPerSection.find(vec_ptr->at(i).sectionId);
//					if(itr != MapForProductsPerSection.end())
//					{
//						vec_ptr->at(i).productIds = itr->second;
//					}
//				}
//
//				ptrIAppFramework->LogDebug("**** going to getMultipleSectionData ****");	
//				ptrIAppFramework->getMultipleSectionData(*vec_ptr,getSectionData);
//				ptrIAppFramework->LogDebug("**** returned from getMultipleSectionData ****");	
//			}
//
//			/*if(vec_ptr->size() == 1)
//			{	
//				isMultipleSectionsAvailable = kFalse;
//				getSectionData.SectionId = vec_ptr->at(0).sectionId;
//				getSectionData.PublicationId = vec_ptr->at(0).publicationId;
//				getSectionData.languageId = vec_ptr->at(0).languageId;				
//				
//				ptrIAppFramework->GetSectionData_getDataForPubOrCat(getSectionData);
//			}else if(vec_ptr->size() > 1)
//			{
//				isMultipleSectionsAvailable = kTrue;
//				ptrIAppFramework->getMultipleSectionData(*vec_ptr,getSectionData);				
//			}*/
//
//			delete vec_ptr;
//			vec_ptr = NULL;


		
			Refresh refresh;
		//	CA("before dorefresh 1");
			//refresh.doRefreshNewoption(Mediator::selectedRadioButton); // Apsiva Comment
			refresh.doRefresh(Mediator::selectedRadioButton);
		//	CA("After dorefresh 1");

			ptrIAppFramework->LogDebug("**** refresh process completed ****");	
		}
	}while(false); 
}


//This function is not used.
//See the overloaded function.
bool16 RfhSelectionObserver::fillDataInListBox(InterfacePtr<IPanelControlData> panelControlData, bool16 isGroupByElem)
{	
	//CA("RfhSelectionObserver::fillDataInListBox");
	if(!Mediator::listControlView)
		return kFalse;	
	
	SDKListBoxHelper listBox(this, kRfhPluginID);
	listBox.EmptyCurrentListBox(Mediator::listControlView);
	 ////Mediator::RfhLocateButtonView->Disable(); 21 Feb Vaibhav
	if(rDataList.size()>0)
		Mediator::RfhRefreshButtonView->Enable();
	else
		Mediator::RfhRefreshButtonView->Disable();
	for(int i=0; i<rDataList.size(); i++)
	{
		PMString objectName;
		if(!isGroupByElem)
		{
			if(rDataList[i].isObject)
				objectName= rDataList[i].name;
			else
				objectName="        " + rDataList[i].name;
			
		}
		else
		{
			if(rDataList[i].isObject)
				objectName="        " + rDataList[i].name;
			else
				objectName= rDataList[i].name;		
		}
        //CA("RfhSelectionObserver::fillDataInListBox  1");
		/*objectName+=" , ";
		objectName.AppendNumber(rDataList[i].boxID.Get());
		objectName+=" , ";
		objectName.AppendNumber(rDataList[i].elementID);
		objectName+=" , ";
		objectName.AppendNumber(rDataList[i].objectID);
		objectName+=" , ";
		objectName.AppendNumber(rDataList[i].publicationID);
		*/
		listBox.AddElement(Mediator::listControlView, objectName, kRfhPanelTextWidgetID, i, kTrue);
		
		//CA("RfhSelectionObserver::fillDataInListBox  2");
		listBox.CheckUncheckRow(Mediator::listControlView, i, kTrue);
		//CA("RfhSelectionObserver::fillDataInListBox  3");
		rDataList[i].isSelected=kTrue;
		rDataList[i].isProcessed=kFalse;
	}
	return kTrue;
}

bool16 RfhSelectionObserver::setSpreadFromPage(const UID& pageUID)
{
	//CA("RfhSelectionObserver::setSpreadFromPage");
	IDocument* doc = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
	IDataBase* db= ::GetDataBase(doc); 

	InterfacePtr<ISpreadList> spreadList(doc, IID_ISPREADLIST); 
	int32 spreadNo = -1; 

	for(int32 i=0; i<spreadList->GetSpreadCount(); ++i) 
	{ 
		UIDRef spreadRef(db, spreadList->GetNthSpreadUID(i)); 
		InterfacePtr<ISpread> spread(spreadRef, IID_ISPREAD); 
		if(spread->GetPageIndex(pageUID)!= -1) 
		{	
			spreadNo = i; 
			break; 
		}
	}

	if(spreadNo==-1)
		return kFalse;
	InterfacePtr<ILayoutControlData>layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());

	if (layoutData == nil)
		return kFalse;
	//layoutData->SetSpread(spreadList->QueryNthSpread(spreadNo),kFalse));
	return kTrue;
}

bool16 RfhSelectionObserver::reSortTheListForElem(void)//We want to group by element
{
	//CA("RfhSelectionObserver::reSortTheListForElem");
	//if(isElementoptSelected!= kFalse)
	//		return kTrue;
	rDataList = OriginalrDataList;
	RefreshDataList theDataList=rDataList;

	rDataList.clear();
	ElementGroupList.clear();
	
	for(int i=0; i<theDataList.size(); i++)
	{
		if(theDataList[i].isObject || theDataList[i].isProcessed)
			continue;

		theDataList[i].isProcessed=kTrue;
		theDataList[i].isSelected = kTrue;
		rDataList.push_back(theDataList[i]);//Added the element
		// Changed by Rahul		
		RefreshData ElementToAdd;
		ElementToAdd =theDataList[i];
		ElementToAdd.isSelected = kTrue;
		//ElementToAdd.isObject= kTrue;
		ElementToAdd.EndIndex=0;
		ElementGroupList.push_back(ElementToAdd);
		PMString ObjectName("");
		RefreshData nodeToAdd;
		
		for(int j=0; j<theDataList.size(); j++)//Now adding the objects which have these elements
		{	
			if(theDataList[j].isObject)
			{
				nodeToAdd=theDataList[j];
				nodeToAdd.isSelected = kTrue;
				ObjectName.Clear();
				ObjectName= theDataList[j].name;
				continue;
			}
			if((theDataList[i].isTableFlag!=1 && theDataList[i].isImageFlag!=1 ) && theDataList[i].whichTab==theDataList[j].whichTab)
			{
				
				if(theDataList[i].elementID==theDataList[j].elementID && theDataList[j].objectID==nodeToAdd.objectID && theDataList[i].whichTab==theDataList[j].whichTab /*&& theDataList[i].TypeID==theDataList[j].TypeID*/)
				{
					if(theDataList[i].whichTab != 4  && theDataList[i].dataType != 4/*theDataList[i].elementID != -121*/)
					{
						if( theDataList[i].TypeID!=theDataList[j].TypeID)
							continue;
					}
					if(theDataList[i].whichTab == 4 && theDataList[i].elementID == -101 ) // for Item Table in Tabbed text
					{
						if( theDataList[i].TypeID != theDataList[j].TypeID)
							continue;
					}

					
					theDataList[j].isProcessed=kTrue;

				// changed by Rahul
					RefreshData ElementToAdd1;
					ElementToAdd1 = theDataList[j];
					ElementToAdd1.isSelected = kTrue;
					ElementToAdd1.name = ObjectName;
					ElementGroupList.push_back(ElementToAdd1);
					nodeToAdd.elementID = ElementToAdd1.elementID;
					rDataList.push_back(nodeToAdd);//Added the Object
				//	
				}
			}
			else
			{				
				if(theDataList[i].TypeID==theDataList[j].TypeID && theDataList[j].objectID==nodeToAdd.objectID && theDataList[i].whichTab==theDataList[j].whichTab)
				{
					//rDataList.push_back(nodeToAdd);//Added the Object
					theDataList[j].isProcessed=kTrue;

				// Changed by Rahul
					RefreshData ElementToAdd1;
					ElementToAdd1 = theDataList[j];
					ElementToAdd1.isSelected = kTrue;
					ElementToAdd1.name = ObjectName;
					nodeToAdd.elementID = ElementToAdd1.elementID;
					rDataList.push_back(nodeToAdd);//Added the Object
				//
				}
			}
		}
	}
	isElementoptSelected = kTrue;
	Mediator::isGroupByObj=kFalse;
	return kTrue;
}


bool16 RfhSelectionObserver::reSortTheListForUniqueAttr(void)//We want to group by element
{
//CA("RfhSelectionObserver::reSortTheListForUniqueAttr");	
	//CA("global count");
	//CAI(myglobal);
		if(myglobal == 1 || strutureIsSelected == kTrue)
		{
			myglobal = 2;
			//CA("11111111111111111111111");
			refreshTableByAttribute = kFalse;
			iscontentbuttonselected = kFalse;
			issrtucturebuttenselected= kTrue;

			IsFrameRefreshSelected=kTrue;
			IsBookrefreshSelected=kFalse;


			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				return kFalse;
			}

			//ptrIAppFramework->clearAllStaticObjects();

			bool16 result1 = ptrIAppFramework->getLoginStatus();
			if(!result1)
				return kFalse;

			IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();//Cs4
			if(iDoc== NULL) { 
				//CA("iDoc== NULL in Handle Drop Down ---1");
			return kFalse;
			}

			if(ReloadState == -1)
			{
				 ptrIAppFramework->LogInfo("AP7_RefreshContent::RfhSelectionObserver::Update::ReloadState == -1");															
				return kFalse;
			}

			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");															
				return kFalse;
			}

			Refresh refresh;
			SDKListBoxHelper listBox(this, kRfhPluginID);

			switch(ReloadState)
			{
				// Awasthi swap 0 and 3
				case 1/*0*/:	
						{
							//CA("Structure case 0");
							Mediator::editBoxView->Disable();
							Mediator::EditBoxTextControlData->SetString("");
							
							listBox.EmptyCurrentListBox(Mediator::listControlView);
							rDataList.clear();
							OriginalrDataList.clear();
							UniqueDataList.clear();
							Mediator::RfhRefreshButtonView->Disable();
							
						}
					break;
				case 0/*1*/:
					{	
						//CA("Structure Refesh case 1");
						Mediator::selectedRadioButton = 3;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						//CA("case 1.1");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
                       //  CA("case 1.5");
						if(GroupFlag==3)
						{
						//	CA("group flag 3");
							reSortTheListForElem();						
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
							//CA("case 1.6");
						}
						else if(GroupFlag==2)
						{
							//CA("GroupFlag==2");
							reSortTheListForObject();
							fillDataInListBox(2);
						    Mediator::isGroupByObj=kTrue;
						    isObjectGroup=kTrue;
						    isElementoptSelected= kFalse;
						//	CA("else 1.66");
						}	
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttrNew();
							fillDataInListBox(1);
						}	
						
					}
					break;
				case 2:
					{
						//CA("Structure Refesh case 2");
						Mediator::selectedRadioButton = 1;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
				
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						if(GroupFlag == 3)
						{
							//CA("GroupFlag == 3");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{	
							//CA("GroupFlag == 2");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttrNew();
							fillDataInListBox(1);							
						}	
			        }
					break;

				case 3:
					{	
						//CA("Content case 3");
						//this code is added because when we select "page Range" ex.1-10 and 4 th page doesnt have tag then then list box 
						//is empty .then only 1-3 pages are refresh so that to handle this condition to refresh all 1-10 pages this code is 
						//written

						int32 size1=static_cast<int32>(textboxvaluevec.size());
						int32 siz=static_cast<int32>(rDataList.size());
						//PMString a3("rDataList.size :");
						//a3.AppendNumber(siz);
						////CA(a3);
						int32 start;
						int32 end;
						
						if(size1==2 && siz==0)
						{
							for(int32 k=0;k<1;k++)
							{
							start=textboxvaluevec.at(k);
							end=textboxvaluevec.at(k+1);
							}
							for(int32 pageNo=start;pageNo<=end;pageNo++)
							{
								InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
								if (iPanelControlData == nil)
								{
									ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");																					
									break;
								}

								IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
								SDKListBoxHelper listBox(this, kRfhPluginID);
								InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());
								Mediator::EditBoxTextControlData=obj;
								PMString pageNumber;
								pageNumber=obj->GetString();
								if(!pageNumber.NumUTF16TextChars())
								{
									//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::!pageNumber.NumUTF16TextChars");																					
									break;
								}
								Mediator::refreshPageNumber=pageNo;
								Mediator::refreshPageNumber--;
								
								UID pageUID;
								Refresh refresh;
								if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
								{	
									//ptrIAppFramework->LogError("AP7_RefreshContent::RfhSelectionObserver::Update::!refresh.isValidPageNumber");																					
									break;
								} 
								Mediator::selectedRadioButton=2;//Newvar
								refresh.GetPageDataInfo(Mediator::selectedRadioButton);
								int32 siz=static_cast<int32>(rDataList.size());
								if(siz>0)
								{
									Newvar=pageNo;
									if(GroupFlag==3)
									{
										//CA("GroupFlag==3");
										reSortTheListForElem();
										fillDataInListBox(3);
										Mediator::isGroupByObj=kFalse;
										isObjectGroup=kFalse;
										isElementoptSelected= kTrue;
									}
									else if(GroupFlag==2)
									{	
										//CA("GroupFlag==2");
										reSortTheListForObject();
										fillDataInListBox(2);
										Mediator::isGroupByObj=kTrue;
										isObjectGroup=kTrue;
										isElementoptSelected= kFalse;

									}
									else if(GroupFlag==1)
									{			
										//CA("GroupFlag==1");
										reSortTheListForUniqueAttrNew();
										fillDataInListBox(1);
									}
								}
								if(siz>0)
									break;
						  
							}
						}
						else
						{
							
						InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
						if (iPanelControlData == nil)
						{
							ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::iPanelControlData == nil");																					
							break ;
						}

						IControlView * editBox =iPanelControlData->FindWidget(kRfhPageNumWidgetID);
						SDKListBoxHelper listBox(this, kRfhPluginID);
						InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());
						Mediator::EditBoxTextControlData=obj;
						PMString pageNumber;
						pageNumber=obj->GetString();
						if(!pageNumber.NumUTF16TextChars())
						{
							//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::Update::!pageNumber.NumUTF16TextChars");																					
							break;
						}
						Mediator::refreshPageNumber=pageNumber.GetAsNumber();
						Mediator::refreshPageNumber--;
						Newvar=Mediator::refreshPageNumber;
						
						UID pageUID;
						Refresh refresh;
						if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
						{	
							//ptrIAppFramework->LogError("AP7_RefreshContent::RfhSelectionObserver::Update::!refresh.isValidPageNumber");																					
							break;
						} 
						Mediator::selectedRadioButton=2;
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						if(GroupFlag==3)
						{
							//CA("GroupFlag==3");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag==2)
						{	
							//CA("GroupFlag==2");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;

						}
						else if(GroupFlag==1)
						{			
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttrNew();
							fillDataInListBox(1);
						}	
						}
			
					}					
					break;

				case 4:
					//CA("Structure Refesh case 4");
						Mediator::selectedRadioButton = 4;
						Mediator::editBoxView->Disable();
						Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						if(GroupFlag == 3)
						{
							//CA("GroupFlag == 3");
							reSortTheListForElem();
							fillDataInListBox(3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;
						}
						else if(GroupFlag == 2)
						{
							//CA("GroupFlag == 2");
							reSortTheListForObject();
							fillDataInListBox(2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)
						{
							//CA("GroupFlag==1");
							reSortTheListForUniqueAttrNew();
							fillDataInListBox(1);
						  
						}	
					break;				
			}
			//CA("pleeeeeeeeeeeeeeeeeeeeeeessssssssssssssssss");
			
		}
		else
		{
			//CA("2222222222222222222222222222222");
			rDataList = OriginalrDataList;
			RefreshDataList theDataList=rDataList;
			rDataList.clear();
			UniqueDataList.clear();
			RefreshData nodeToAdd;
			RefreshDataList tempDataList;
		//PMString a("theDataList.size()	:	");
		//a.AppendNumber(static_cast<int32>(theDataList.size()));
		//CA(a);
			for(int j=0; j<theDataList.size(); j++)
			{	
				if(theDataList[j].isObject)
				{
					//CA("continue");
					continue;
				}
				RefreshData nodeToAdd;
				if(UniqueDataList.size()==0)
				{	
					//CA("UniqueDataList.size()==0");
					nodeToAdd =theDataList[j]; 
					nodeToAdd.isSelected = kTrue;
					UniqueDataList.push_back(nodeToAdd);
					rDataList.push_back(nodeToAdd);			
				}
				else
				{	
					//CA("else");
					bool16 CheckFlag = kFalse;
					for(int p=0; p<UniqueDataList.size(); p++)
					{	
						if(UniqueDataList[p].elementID == theDataList[j].elementID && UniqueDataList[p].whichTab == theDataList[j].whichTab /*&& UniqueDataList[p].TypeID == theDataList[j].TypeID*/ )
						{	
							if((theDataList[j].whichTab != 4 || theDataList[j].isImageFlag == kTrue || theDataList[j].isTableFlag == kTrue) && !(/*theDataList[j].elementID == -121*/theDataList[j].dataType == 4 && theDataList[j].isTableFlag == kFalse))	// For All Standard Table  theDataList[j].elementID != -121 && theDataList[j].isTableFlag == kFalse
							{
								if( UniqueDataList[p].TypeID != theDataList[j].TypeID)
										continue;
							}
							if(theDataList[j].whichTab == 4 && UniqueDataList[p].elementID == -101 ) // for Item Table in Tabbed text
							{
								if( UniqueDataList[p].TypeID != theDataList[j].TypeID)
										continue;
							}
							//CA("2224");
							nodeToAdd = theDataList[j];
							nodeToAdd.isSelected = kTrue;
							rDataList.push_back(nodeToAdd);	
							CheckFlag = kTrue;
							break;
						}
					}
					if(!CheckFlag)
					{	
						//CA("2234");
						nodeToAdd = theDataList[j];
						nodeToAdd.isSelected = kTrue;
						UniqueDataList.push_back(nodeToAdd);
						rDataList.push_back(nodeToAdd);
					}
				}
			}

			tempDataList.clear();
			for(int32 j=0; j<UniqueDataList.size(); j++)
			{			
				if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kFalse && UniqueDataList[j].elementID == -2)
					tempDataList.push_back(UniqueDataList[j]);			
			}
			for(int32 j=0; j<UniqueDataList.size(); j++)
			{			
				if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kFalse && UniqueDataList[j].elementID == -1 && UniqueDataList[j].elementID != -2)
					tempDataList.push_back(UniqueDataList[j]);			
			}
			for(int32 j=0; j<UniqueDataList.size(); j++)
			{			
				if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kFalse && UniqueDataList[j].elementID != -1 && UniqueDataList[j].elementID != -2)
					tempDataList.push_back(UniqueDataList[j]);			
			}
			for(int32 j=0; j<UniqueDataList.size(); j++)
			{			
				if(UniqueDataList[j].isTableFlag == kTrue )
					tempDataList.push_back(UniqueDataList[j]);			
			}
			for(int32 j=0; j<UniqueDataList.size(); j++)
			{			
				if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kTrue )
					tempDataList.push_back(UniqueDataList[j]);
			}			
			UniqueDataList = tempDataList;
		//PMString a1("UniqueDataList.size()	:	");
		//a1.AppendNumber(static_cast<int32>(UniqueDataList.size()));
		//CA(a1);

			//for(int i=0; i<theDataList.size(); i++)
			//{
			//	if(!theDataList[i].isObject || theDataList[i].isProcessed)
			//		continue;		
			//	
			//	theDataList[i].isProcessed=kTrue;
			//	RefreshData referenceNode=theDataList[i];
			//	bool16 ReturnFlag= kFalse;
			//	for(int k=0; k<CurrentObjectDataList.size(); k++)
			//	{
			//		if(CurrentObjectDataList[k].objectID ==theDataList[i].objectID)
			//			ReturnFlag= kTrue;
			//	}
			//	if(ReturnFlag)
			//		continue;
			//	rDataList.push_back(theDataList[i]);//Added the Object
			//	RefreshData nodeToAdd;
			//	CurrentObjectDataList.push_back(referenceNode);

			//	for(int j=0; j<theDataList.size();j++)//Now adding the elements which have these elements
			//	{	
			//		if(!theDataList[j].isObject)
			//		{
			//			nodeToAdd=theDataList[j];
			//		//	continue;			
			//		
			//			if(referenceNode.objectID==theDataList[j].objectID)
			//			{
			//				//nodeToAdd.objectID=referenceNode.objectID;
			//				if(!theDataList[j].isProcessed)
			//				{
			//					nodeToAdd.isSelected=theDataList[j].isSelected;
			//					rDataList.push_back(nodeToAdd);//Added the Element
			//					theDataList[j].isProcessed=kTrue;
			//				}
			//			}
			//		}
			//	}
			//}
			
			/*isElementoptSelected = kFalse;
			Mediator::isGroupByObj=kTrue;	*/
	}
	return kTrue;
}


bool16 RfhSelectionObserver::reSortTheListForObject(void)//Only to be called when the list has been resorted as Element
{
	//CA("RfhSelectionObserver::reSortTheListForObject");
		//if(isElementoptSelected== kFalse)
		//return kTrue;
		//rDataList = OriginalrDataList;
		//RefreshDataList theDataList=rDataList;
		//rDataList.clear();
		//

		//for(int i=0; i<theDataList.size(); i++)
		//{
		//	if(!theDataList[i].isObject || theDataList[i].isProcessed)
		//		continue;		
		//	theDataList[i].isProcessed=kTrue;
		//	RefreshData referenceNode=theDataList[i];
		//	rDataList.push_back(theDataList[i]);//Added the Object
		//	RefreshData nodeToAdd;

		//	for(int j=0; j<theDataList.size();j++)//Now adding the elements which have these elements
		//	{	
		//		if(!theDataList[j].isObject)
		//		{
		//			nodeToAdd=theDataList[j];
		//			continue;
		//		}
		//		
		//		if(theDataList[i].objectID==theDataList[j].objectID)
		//		{
		//			nodeToAdd.objectID=referenceNode.objectID;
		//			nodeToAdd.isSelected=theDataList[j].isSelected;
		//			rDataList.push_back(nodeToAdd);//Added the Element
		//			theDataList[j].isProcessed=kTrue;
		//		}
		//	}
		//}
		//isElementoptSelected = kFalse;
		//return kTrue;
	
	//if(isElementoptSelected== kFalse)
	//	return kTrue;
		
		rDataList = OriginalrDataList;
		RefreshDataList theDataList=rDataList;
		rDataList.clear();
		RefreshDataList CurrentObjectDataList;

		for(int i=0; i<theDataList.size(); i++)
		{
			if(!theDataList[i].isObject || theDataList[i].isProcessed)
				continue;		
			
			theDataList[i].isProcessed=kTrue;
			RefreshData referenceNode=theDataList[i];
			bool16 ReturnFlag= kFalse;
			for(int k=0; k<CurrentObjectDataList.size(); k++)
			{
				if(CurrentObjectDataList[k].objectID ==theDataList[i].objectID)
					ReturnFlag= kTrue;
			}
			if(ReturnFlag)
				continue;
			rDataList.push_back(theDataList[i]);//Added the Object
			RefreshData nodeToAdd;
			CurrentObjectDataList.push_back(referenceNode);

			for(int j=0; j<theDataList.size();j++)//Now adding the elements which have these elements
			{	
				if(!theDataList[j].isObject)
				{
					nodeToAdd=theDataList[j];
				//	continue;
								
					if(referenceNode.objectID==theDataList[j].objectID)
					{
						//nodeToAdd.objectID=referenceNode.objectID;
						if(!theDataList[j].isProcessed)
						{
							nodeToAdd.isSelected=theDataList[j].isSelected;
							rDataList.push_back(nodeToAdd);//Added the Element
							theDataList[j].isProcessed=kTrue;
						}
					}
				}
			}
		}
	
		isElementoptSelected = kFalse;
		Mediator::isGroupByObj=kTrue;
		return kTrue;
}


void RfhSelectionObserver::ChangeColorOfAllText(int c , PMString  altColorSwatch)
{
	do
	{	
		//CA("RfhSelectionObserver::ChangeColorOfAllText");
		UIDRef	BoxRefUID1 = ColorDataList[c].BoxUIDRef; 
	//	int32 LObjectID1 = ColorDataList[c].objectID;
		double LElementID1 = ColorDataList[c].elementID;
		int32 StartText1 = ColorDataList[c].StartIndex;
		int32 EndText1 = ColorDataList[c].EndIndex;
		
		/*InterfacePtr<IPMUnknown> unknown(BoxRefUID1, IID_IUNKNOWN);
		UID textFrameUID1 = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
		UID textFrameUID1 = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxRefUID1, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID1 = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID1 == kInvalidUID)
			break;
		/*
		InterfacePtr<ITextFrame> textFrame1(BoxRefUID1.GetDataBase(), textFrameUID1, ITextFrame::kDefaultIID);
		if (textFrame1 == nil)
			break;	*/	
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			return;
		}
		InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxRefUID1, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("Refresh::RfhSelectionObserver::ChangeColorOfAllText::!graphicFrameHierarchy");
			return;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("Refresh::RfhSelectionObserver::ChangeColorOfAllText::!multiColumnItemHierarchy");
			return;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("Refresh::RfhSelectionObserver::ChangeColorOfAllText::!multiColumnItemTextFrame");
			//CA("Its Not MultiColumn");
			return;
		}
		
		InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("Refresh::RfhSelectionObserver::ChangeColorOfAllText::!frameItemHierarchy");
			return;
		}

		InterfacePtr<ITextFrameColumn>textFrame1(frameItemHierarchy, UseDefaultIID());
		if (!textFrame1) {
			ptrIAppFramework->LogDebug("Refresh::RfhSelectionObserver::ChangeColorOfAllText::!textFrame1");
			return;
		}

////////////////	End
		TextIndex startIndex2 = textFrame1->TextStart();	
		InterfacePtr<ITextModel> textModel1(textFrame1->QueryTextModel());
		if (textModel1 == nil)
			break;
		
		InterfacePtr<IWorkspace> workSpace(Utils<ILayoutUIUtils>()->QueryActiveWorkspace());
 
		InterfacePtr<ISwatchList> swatchList(workSpace,UseDefaultIID());
		if (swatchList == nil)
		{	
			ASSERT_FAIL("Unable to get swatch list from workspace!");
			break;
		}
		
		InterfacePtr<ITextAttrUID>
		textAttrUID(::CreateObject2<ITextAttrUID>(kTextAttrColorBoss));
		UID colorUID;
		UID altColorUID = kInvalidUID;
		
		UIDRef altColorRef = swatchList->FindSwatch (altColorSwatch);
		if (altColorRef.GetUID() == kInvalidUID)
		{
			ASSERT_FAIL("Unable to get alternate swatch in DeleteColor!");
			break;
		}
		else
		{
			altColorUID = altColorRef.GetUID();
		}		

		textAttrUID->Set(altColorUID);
		int32 charCount = EndText1 -StartText1;
		
		InterfacePtr<ICommand> applyCmd(
						Utils<ITextAttrUtils>()->
						BuildApplyTextAttrCmd(textModel1,
						StartText1,
						charCount,
						textAttrUID,
						kCharAttrStrandBoss) );
		
		ErrorCode err = CmdUtils::ProcessCommand(applyCmd);
		
	}while(0);
}

//This function is used.
bool16 RfhSelectionObserver::fillDataInListBox(int GroupFlag)
{	
	//CA("RfhSelectionObserver::fillDataInListBox");		
			//edit by nitin
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CA("ptrIAppFramework == nil");
		//return;
	}
	IControlView * iControlView=
		Mediator::iPanelCntrlDataPtr->FindWidget(kRfhSelectionBoxtWidgetID);
	if(iControlView==nil) 
	{
		//CA("iControlView==nil");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::fillDataInListBox::iControlView is nil");		
		//break;
	}
	InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
	if(itristatecontroldata==nil)
	{
		//CA("itristatecontroldata==nil");
		ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::fillDataInListBox::itristatecontroldata is nil");				
		//break;
	}
			//itristatecontroldata->Select();
			//itristatecontroldata->Deselect ();
	//upto here.....

	//CA_NUM("GroupFlag : ",GroupFlag);

	InterfacePtr<IPanelControlData> panelControlData(QueryPanelControlData());
			//if (panelControlData == nil)
				//return kFalse;
	if(!Mediator::listControlView)
		return kFalse;
	
	//SDKListBoxHelper listBox(this, kRfhPluginID);
	//listBox.EmptyCurrentListBox(Mediator::listControlView);
	// Mediator::RfhLocateButtonView->Disable(); 21 Feb vaibhav
//DebugTestAlert Print rDatalist
/*	PMString printData;
	printData.Append("rDataList is : \n");
	printData.Append("size : ");
	printData.AppendNumber(rDataList.size());
	for(int32 rDataListElementIndex = 0;rDataListElementIndex < rDataList.size();rDataListElementIndex++)
	{
		printData.Append("\n");
		printData.Append(rDataList[rDataListElementIndex].name);
		
		//printData.AppendNumber(rDataList[rDataListElementIndex].elementID);
		//rDataList[rDataListElementIndex].
	}
	//CA(printData);
*/
//end DebugTestAlert
	
	if(GroupFlag != 1)
	{
		//CA("GroupFlag != 1");
		if(rDataList.size()==0)
		{
			//CA("RfhSelectionObserver::fillDataInListBox->rDataLsit.size==0");
		}
		if(rDataList.size()>0)
		{
			//CA("select");
			Mediator::RfhRefreshButtonView->Enable();
			if(SelectBoxFlage)
				itristatecontroldata->Select();
			else
				itristatecontroldata->Deselect();
		}
		else
		{
			//CA("deselect");
			Mediator::RfhRefreshButtonView->Disable();
			itristatecontroldata->Deselect ();
		}
		for(int i=0; i<rDataList.size(); i++)
		{
			PMString objectName;
			objectName.SetTranslatable(kFalse);
			if(GroupFlag == 2)
			{
				//CA("GroupFlag == 2");
				if(rDataList[i].isObject)
				{
					//CA("RfhSelectionObserver::fillDataInListBox 1.1");
					objectName= rDataList[i].name;
					//CA(objectName);
				}
				else
				{
					//CA("RfhSelectionObserver::fillDataInListBox 1.2");
					objectName="        " + rDataList[i].name;			
					// CA(objectName);
				}

			}
			else if(GroupFlag == 3)
			{
				//CA("GroupFlag == 3");
				if(rDataList[i].isObject)
					objectName="        " + rDataList[i].name;
				else
					objectName= rDataList[i].name;		
				
				//CA(objectName);
			}

			/*objectName+=" , ";
			objectName.AppendNumber(rDataList[i].boxID.Get());
			objectName+=" , ";
			objectName.AppendNumber(rDataList[i].elementID);
			objectName+=" , ";
			objectName.AppendNumber(rDataList[i].objectID);
			objectName+=" , ";
			objectName.AppendNumber(rDataList[i].publicationID);
			*/
			//listBox.AddElement(Mediator::listControlView, objectName, kRfhPanelTextWidgetID, i, kTrue);
			//listBox.CheckUncheckRow(Mediator::listControlView, i, kTrue);
			rDataList[i].isSelected=kTrue;
			rDataList[i].isProcessed=kFalse;
		}
	}
	else if(GroupFlag == 1)
	{	
		//CA("GroupFlag == 1");
		/*if(UniqueDataList.size()==0)
		{
			CA("RfhSelectionObserver::fillDataInListBox->rDataLsit.size==0");
		}*/
		
		if(UniqueDataList.size()>0)
		{
			
			Mediator::RfhRefreshButtonView->Enable();
			if(SelectBoxFlage)
				itristatecontroldata->Select();
			else
				itristatecontroldata->Deselect();
		}
		else
		{
			//CA("UniqueDataList.size()<0");
			itristatecontroldata->Deselect();
			Mediator::RfhRefreshButtonView->Disable();
		}
		
		for(int i=0; i<UniqueDataList.size(); i++)
		{	
			PMString objectName;
			if(GroupFlag == 1)
			{	
				if(UniqueDataList[i].isObject)
				{
					//CA("RfhSelectionObserver::fillDataInListBox 1.1");
					objectName= UniqueDataList[i].name;
					//CA(objectName);
				}
				else
				{
					//CA("RfhSelectionObserver::fillDataInListBox 1.2");
					objectName="        " + UniqueDataList[i].name;			
					//CA(objectName);
				}

			}
			
			//CA("objectName = "+objectName);
			//listBox.AddElement(Mediator::listControlView, objectName, kRfhPanelTextWidgetID, i, kTrue);
			//listBox.CheckUncheckRow(Mediator::listControlView, i, kTrue);
			/*UniqueDataList[i].isSelected=kTrue;
			UniqueDataList[i].isProcessed=kFalse;	*/	

			
		}
		for(int i=0; i<UniqueDataList.size(); i++)
		{
			for(int j=0; j<rDataList.size(); j++)
			{
				if(rDataList[j].isProcessed == kTrue)
					continue;
				if(UniqueDataList[i].elementID == rDataList[j].elementID /*&& UniqueDataList[i].TypeID ==  rDataList[j].TypeID */&& UniqueDataList[i].whichTab == rDataList[j].whichTab)
				{	

					if(UniqueDataList[i].whichTab != 4 || UniqueDataList[i].isImageFlag == kTrue || UniqueDataList[i].isTableFlag == kTrue)
					{
						if( UniqueDataList[i].TypeID !=  rDataList[j].TypeID)
							continue;
					}
					if(UniqueDataList[i].whichTab == 4 && UniqueDataList[i].elementID == -101 ) // for Item Table in Tabbed text
					{
						if( UniqueDataList[i].TypeID !=  rDataList[j].TypeID)
							continue;
					}
				
					rDataList[j].isSelected = UniqueDataList[i].isSelected;
					rDataList[j].isProcessed = kTrue;
					
				}
			}
		}

	}
//CA("returning true");

	TreeDisplayOption = 1; // Frame Refresh Field Tree.

	InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::listControlView, UseDefaultIID());
	if(!treeViewMgr)
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
		return kFalse;
	}
	RFHTreeDataCache dc;
	dc.clearMap();

	RFHTreeModel pModel;
	PMString pfName("Root");
	pModel.setRoot(-1, pfName, 1);
	treeViewMgr->ClearTree(kTrue);
	pModel.GetRootUID();
	treeViewMgr->ChangeRoot();

	return kTrue;
}

void RfhSelectionObserver::DisableAll()
{	
	//CA("Disable All Refresh ");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
        return;
	}
	if(Mediator::loadData == kFalse)
	{
		//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::DisableAll::Mediator::loadData == kFalse");	
		return;
	}

	if(Mediator::iPanelCntrlDataPtr != nil && FlgToCheckMenuAction== kTrue)
	{	
		IControlView* dropdownCtrlView=
		Mediator::iPanelCntrlDataPtr->FindWidget(kRfhOptionsDropDownWidgetID);
		if(!dropdownCtrlView)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::DisableAll::!dropdownCtrlView");			
			return;
		}
	
		IControlView* listControlView1=
			Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPanelTreeViewWidgetID);
		if(!listControlView1)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::DisableAll::!listControlView1");			
			return;
		}
	
		IControlView* refreshBtnCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kRfhRefreshButtonWidgetID);
		if(!refreshBtnCtrlView)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::DisableAll::!refreshBtnCtrlView");					
			return;
		}
	
		IControlView* EditBoxCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPageNumWidgetID);
		if(!EditBoxCtrlView)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::DisableAll::!EditBoxCtrlView");							
			return;
		}
//following code is added by vijay on 5 OCT 2006 ////////////////////from here
		IControlView* reloadBtnCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kRfhReloadIconWidgetID);
		if(!reloadBtnCtrlView)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::DisableAll::!reloadBtnCtrlView");									
			return;
		}
/////////////////////////////////////////////////////////////////////upto here
		//**Commented By Sachin Sharma 15/07/08
		/*IControlView* PreferenceIconButtonCtrlView  = Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPreferenceIconWidgetID);
		if(!PreferenceIconButtonCtrlView)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::DisableAll::!PreferenceIconButtonCtrlView");									
			return;
		}
		PreferenceIconButtonCtrlView->Disable();*/

		dropdownCtrlView->Disable();
		listControlView1->Disable();
		refreshBtnCtrlView->Disable();
		EditBoxCtrlView->Disable();
		reloadBtnCtrlView->Disable();
	
	}
}

void RfhSelectionObserver::EnableAll()
{	
	//CA("RfhSelectionObserver::EnableAll");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	bool16 result=ptrIAppFramework->getLoginStatus();		
	if(!result)
	{
		//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::EnableAll::!result");									
		return;
	}

	

	if(Mediator::iPanelCntrlDataPtr != nil && FlgToCheckMenuAction== kTrue)
	{	
		IControlView* dropdownCtrlView=
		Mediator::iPanelCntrlDataPtr->FindWidget(kRfhOptionsDropDownWidgetID);
		if(!dropdownCtrlView)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::EnableAll::!dropdownCtrlView");											
			return;
		}
	
		IControlView* listControlView1=
			Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPanelTreeViewWidgetID);
		if(!listControlView1)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::EnableAll::!listControlView1");														
			return;
		}
	
		IControlView* refreshBtnCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kRfhRefreshButtonWidgetID);
		if(!refreshBtnCtrlView)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::EnableAll::!refreshBtnCtrlView");																
			return;
		}
	
		IControlView* EditBoxCtrlView=
			Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPageNumWidgetID);
		if(!EditBoxCtrlView)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::EnableAll::!EditBoxCtrlView");																		
			return;
		}
		///***Commented By sachin Sharma on 15/07/08
		/*IControlView* PreferenceIconButtonCtrlView  = Mediator::iPanelCntrlDataPtr->FindWidget(kRfhPreferenceIconWidgetID);
		if(!PreferenceIconButtonCtrlView)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::DisableAll::!PreferenceIconButtonCtrlView");									
			return;
		}
		PreferenceIconButtonCtrlView->Disable();*/

		dropdownCtrlView->Enable();

		dropdownCtrlView->Enable();
		listControlView1->Enable();
		refreshBtnCtrlView->Enable();
		EditBoxCtrlView->Disable();

		InterfacePtr<IDropDownListController> RefreshDropListCntrler(dropdownCtrlView, UseDefaultIID());
		if(RefreshDropListCntrler==nil)
		{
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::EnableAll::!RefreshDropListCntrler");																		
			return;
		}
		//CA("D");
		RefreshDropListCntrler->Select(0);
		FlgToCheckMenuAction= kFalse;

		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData==nil)
		{
			//CA("iPanelControlData is nil");
			//	break;
		}
		SDKListBoxHelper listBox(this, kRfhPluginID);
		listBox.EmptyCurrentListBox(Mediator::listControlView);		
	}
}

////***Added By Sachin Sharma 9/07/08 To Showing Refresh Summary After Doicument Refresh
void RfhSelectionObserver::ShowRefreshSummary()
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	//**** Opening the Refresh Summary Dialog******
		do
		{
			InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
			if (application == nil) 						
				break;

			//InterfacePtr<IDialogMgr> dialogMgr1(application, UseDefaultIID());
			//if (dialogMgr1 == nil) 
			//{				
			//	break;
			//}

			showSummaryDialog = kTrue;
			//bool16 isDialogOpen = dialogMgr1->IsModelessDialogInFront ();
			//if(isDialogOpen && Summarydialog != NULL)
			//{				
			//	//CA("Dialog already open");
			//	Summarydialog->Close();
			//	Summarydialog = NULL;
			//}

			if(isEmptySectionFound)  // Asserting to user about Empty section while refresh
			{
				CAlert::InformationAlert("Some frame(s) may not have refreshed properly as the Section ID no longer exists.");
			}

			
			RfhActionComponent rfhActionComponent = new RfhActionComponent(application);
			rfhActionComponent.DoDialog();
						

			//**Setting the Title of Dialog	
			InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
			if (dialogMgr == nil) 
			{				
				break;
			}
			IWindow* iWindow = dialogMgr->GetFrontmostDialogWindow();
			if(iWindow == nil )
			{				
			ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::iWindow == nil");
				break;
			}

			PMString title("Refresh Summary");
			title.SetTranslatable(kFalse);
			iWindow->SetTitle(title);
			
			showSummaryDialog = kFalse;

			if(Mediator::catalogRefrshPanelControlData == nil)
			{
				//CA("Mediator::catalogRefrshPanelControlData RfhSelectionObserver::ShowRefreshSummary");
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::Mediator::catalogRefrshPanelControlData == nil");
				break;
			}
			IControlView * kRfhlastGroupPanelWidgetIDctrlview = Mediator::catalogRefrshPanelControlData->FindWidget(kRfhlastGroupPanelWidgetID);
			if(kRfhlastGroupPanelWidgetIDctrlview == nil)
			{				
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::zerothGroupPanelWidgetID == nil");
				break;
			}
			kRfhlastGroupPanelWidgetIDctrlview->HideView();
		

			IControlView * RfhnewGroupPanelWidgetIDctrlview = Mediator::catalogRefrshPanelControlData->FindWidget(kRfhnewGroupPanelWidgetID);
			if(RfhnewGroupPanelWidgetIDctrlview == nil)
			{				
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::zerothGroupPanelWidgetID == nil");
				break;
			}
			RfhnewGroupPanelWidgetIDctrlview->HideView();
		
			IControlView * zerothGroupPanelWidgetID = Mediator::catalogRefrshPanelControlData->FindWidget(kRfhZerothGroupPanelWidgetID);
			if(zerothGroupPanelWidgetID == nil)
			{				
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::zerothGroupPanelWidgetID == nil");
				break;
			}
			zerothGroupPanelWidgetID->HideView();

			IControlView * midBookListGroupPanelControlData = Mediator::catalogRefrshPanelControlData->FindWidget(kRfhMidBookListGroupPanelWidgetID);
			if(midBookListGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::midBookListGroupPanelControlData == nil");
				break;
			}
			midBookListGroupPanelControlData->HideView();
			
				
			//*****Added From Here
			IControlView * secondGroupPanelControlData = Mediator::catalogRefrshPanelControlData->FindWidget(kRfhSecondGroupPanelWidgetID);
			if(secondGroupPanelControlData == nil)
			{				
		//CA("secondGroupPanelControlData ==  nil");
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::zerothGroupPanelWidgetID == nil");
				return;
			}			
			secondGroupPanelControlData->HideView();
			IControlView * firstGroupPanelControlData = Mediator::catalogRefrshPanelControlData->FindWidget(kRfhFirstGroupPanelWidgetID);
			if(firstGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::zerothGroupPanelWidgetID == nil");
	//CA("firstGroupPanelControlData ==  nil");
				return;
			}
			firstGroupPanelControlData->HideView();
		
		//******
			IControlView * firstInteractiveGroupPanelControlData = Mediator::catalogRefrshPanelControlData->FindWidget(kFirstInteractiveGroupPanelWidgetID);
			if(firstInteractiveGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::firstInteractiveGroupPanelControlData == nil");
				break;
			}
			firstInteractiveGroupPanelControlData->HideView();

			IControlView * interactiveListNilGroupPanelControlData = Mediator::catalogRefrshPanelControlData->FindWidget(kInteractiveListNilGroupPanelWidgetID);
			if(interactiveListNilGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::interactiveListNilGroupPanelControlData == nil");
				break;
			}
			interactiveListNilGroupPanelControlData->HideView();

			//kInteractiveoptionGroupPanelWidgetID
			IControlView * interactiveoptionGroupPanelControlData = Mediator::catalogRefrshPanelControlData->FindWidget(kInteractiveoptionGroupPanelWidgetID);
			if(interactiveoptionGroupPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::interactiveoptionGroupPanelControlData == nil");
				break;
			}
			interactiveoptionGroupPanelControlData->HideView();

			//Show the ThirdGroupPanel
			IControlView * thirdGroupPanelControlView = Mediator::catalogRefrshPanelControlData->FindWidget(kRfhThirdGroupPanelWidgetID);
			if(thirdGroupPanelControlView == nil)
			{				
				ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::ShowRefreshSummary::thirdGroupPanelControlView == nil");
				break;
			}			
			thirdGroupPanelControlView->ShowView();
			
			//****** Now filling the Refresh Summary dialog		
			IControlView* TextWidget1 = Mediator::catalogRefrshPanelControlData->FindWidget(kItemUpdatedWidgetID);
			if(!TextWidget1)
			{
				//CA("TextWidget1 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget1,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text1;
				int32/*int64*/ TotalUpdatedItem = static_cast<int32>/*tempItemList.size()*/(itemsUpdated.size()); 
				text1.AppendNumber(TotalUpdatedItem);
				textcontrol->SetString(text1);			
			}

			IControlView* TextWidget2 = Mediator::catalogRefrshPanelControlData->FindWidget(kItemDeletedWidgetID);
			if(!TextWidget2)
			{
				//CA("TextWidget2 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget2,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text2;
				int32/*int64*/ TotalDeletedItem =static_cast<int32>(itemsDeleted.size()); 
				text2.AppendNumber(TotalDeletedItem);
				textcontrol->SetString(text2);			
			}

			IControlView* TextWidget3 = Mediator::catalogRefrshPanelControlData->FindWidget(kProdUpdatedWidgetID);
			if(!TextWidget3)
			{
				//CA("TextWidget3 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget3,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}				
				PMString text3;
				int32/*int64*/ TotalUpdatedProd = static_cast<int32>/*tempProductList.size()*/(productsUpdated.size()); 
				text3.AppendNumber(TotalUpdatedProd);
				textcontrol->SetString(text3);			
			}

			IControlView* TextWidget4 = Mediator::catalogRefrshPanelControlData->FindWidget(kProdDeletedWidgetID);
			if(!TextWidget4)
			{
				//CA("TextWidget4 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}

				PMString text4;
				int32/*int64*/ TotalDeletedProd =static_cast<int32> (productsDeleted.size()); 
				text4.AppendNumber(TotalDeletedProd);
				textcontrol->SetString(text4);			
			}

			IControlView* TextWidget5 = Mediator::catalogRefrshPanelControlData->FindWidget(kItemImagesRelinkWidgetID);
			if(!TextWidget5)
			{
				//CA("TextWidget5 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget5,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}

				PMString text5;
				int32/*int64*/ TotalItemImagesRelink =static_cast<int32>(itemImagesReLinked.size()); 
				text5.AppendNumber(TotalItemImagesRelink);
				textcontrol->SetString(text5);			
			}

			IControlView* TextWidget6 = Mediator::catalogRefrshPanelControlData->FindWidget(kProdImagesRelinkWidgetID);
			if(!TextWidget6)
			{
				//CA("TextWidget6 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget6,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text6;
				int32/*int64*/ TotalProdImagesRelink =static_cast<int32> (productImagesReLinked.size());
				text6.AppendNumber(TotalProdImagesRelink);
				textcontrol->SetString(text6);			
			}

			IControlView* TextWidget7 = Mediator::catalogRefrshPanelControlData->FindWidget(kItemImagesDeletedWidgetID);
			if(!TextWidget7)
			{
				//CA("TextWidget7 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget7,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}
				
				PMString text7;
				int32/*int64*/ TotalItemImagesDeleted =static_cast<int32> (itemImagesDeleted.size()); 
				text7.AppendNumber(TotalItemImagesDeleted);
				textcontrol->SetString(text7);			
			}

			IControlView* TextWidget8 = Mediator::catalogRefrshPanelControlData->FindWidget(kProdImagesDeletedWidgetID);
			if(!TextWidget8)
			{
				//CA("TextWidget8 == nil");
			}
			else
			{	
				InterfacePtr<ITextControlData> textcontrol(TextWidget8,UseDefaultIID());
				if(!textcontrol)
				{
					//CA("textcontrol == nil");
					return;
				}				
				PMString text8;
			int32/*int64*/ TotalProdImagesDeleted =static_cast<int32> (productImagesDeleted.size()); 
				text8.AppendNumber(TotalProdImagesDeleted);
				textcontrol->SetString(text8);			
			}
		}while(false);


}
//new sagar
bool16 RfhSelectionObserver::reSortTheListForUniqueAttrNew(void)//We want to group by element
{

		{
			//CA("4444444444444444444444444");
			rDataList = OriginalrDataList;
			RefreshDataList theDataList=rDataList;
			rDataList.clear();
			UniqueDataList.clear();
			RefreshData nodeToAdd;
			RefreshDataList tempDataList;
		//PMString a("theDataList.size()	:	");
		//a.AppendNumber(static_cast<int32>(theDataList.size()));
		//CA(a);
			for(int j=0; j<theDataList.size(); j++)
			{	
				if(theDataList[j].isObject)
				{
					//CA("continue");
					continue;
				}
				RefreshData nodeToAdd;
				if(UniqueDataList.size()==0)
				{	
					//CA("UniqueDataList.size()==0");
					nodeToAdd =theDataList[j]; 
					nodeToAdd.isSelected = kTrue;
					UniqueDataList.push_back(nodeToAdd);
					rDataList.push_back(nodeToAdd);			
				}
				else
				{	
					//CA("else");
					bool16 CheckFlag = kFalse;
					for(int p=0; p<UniqueDataList.size(); p++)
					{	
						if(UniqueDataList[p].elementID == theDataList[j].elementID && UniqueDataList[p].whichTab == theDataList[j].whichTab /*&& UniqueDataList[p].TypeID == theDataList[j].TypeID*/ )
						{	
							if((theDataList[j].whichTab != 4 || theDataList[j].isImageFlag == kTrue || theDataList[j].isTableFlag == kTrue) && !(/*theDataList[j].elementID == -121*/theDataList[j].dataType == 4 && theDataList[j].isTableFlag == kFalse))	// For All Standard Table  theDataList[j].elementID != -121 && theDataList[j].isTableFlag == kFalse
							{
								if( UniqueDataList[p].TypeID != theDataList[j].TypeID)
										continue;
							}
							if(theDataList[j].whichTab == 4 && UniqueDataList[p].elementID == -101 ) // for Item Table in Tabbed text
							{
								if( UniqueDataList[p].TypeID != theDataList[j].TypeID)
										continue;
							}
							//CA("2224");
							nodeToAdd = theDataList[j];
							nodeToAdd.isSelected = kTrue;
							rDataList.push_back(nodeToAdd);	
							CheckFlag = kTrue;
							break;
						}
					}
					if(!CheckFlag)
					{	
						//CA("2234");
						nodeToAdd = theDataList[j];
						nodeToAdd.isSelected = kTrue;
						UniqueDataList.push_back(nodeToAdd);
						rDataList.push_back(nodeToAdd);
					}
				}
			}

			tempDataList.clear();
			for(int32 j=0; j<UniqueDataList.size(); j++)
			{			
				if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kFalse && UniqueDataList[j].elementID == -2)
					tempDataList.push_back(UniqueDataList[j]);			
			}
			for(int32 j=0; j<UniqueDataList.size(); j++)
			{			
				if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kFalse && UniqueDataList[j].elementID == -1 && UniqueDataList[j].elementID != -2)
					tempDataList.push_back(UniqueDataList[j]);			
			}
			for(int32 j=0; j<UniqueDataList.size(); j++)
			{			
				if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kFalse && UniqueDataList[j].elementID != -1 && UniqueDataList[j].elementID != -2)
					tempDataList.push_back(UniqueDataList[j]);			
			}
			for(int32 j=0; j<UniqueDataList.size(); j++)
			{			
				if(UniqueDataList[j].isTableFlag == kTrue )
					tempDataList.push_back(UniqueDataList[j]);			
			}
			for(int32 j=0; j<UniqueDataList.size(); j++)
			{			
				if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kTrue )
					tempDataList.push_back(UniqueDataList[j]);
			}			
			UniqueDataList = tempDataList;
		//PMString a1("UniqueDataList.size()	:	");
		//a1.AppendNumber(static_cast<int32>(UniqueDataList.size()));
		//CA(a1);

			//for(int i=0; i<theDataList.size(); i++)
			//{
			//	if(!theDataList[i].isObject || theDataList[i].isProcessed)
			//		continue;		
			//	
			//	theDataList[i].isProcessed=kTrue;
			//	RefreshData referenceNode=theDataList[i];
			//	bool16 ReturnFlag= kFalse;
			//	for(int k=0; k<CurrentObjectDataList.size(); k++)
			//	{
			//		if(CurrentObjectDataList[k].objectID ==theDataList[i].objectID)
			//			ReturnFlag= kTrue;
			//	}
			//	if(ReturnFlag)
			//		continue;
			//	rDataList.push_back(theDataList[i]);//Added the Object
			//	RefreshData nodeToAdd;
			//	CurrentObjectDataList.push_back(referenceNode);

			//	for(int j=0; j<theDataList.size();j++)//Now adding the elements which have these elements
			//	{	
			//		if(!theDataList[j].isObject)
			//		{
			//			nodeToAdd=theDataList[j];
			//		//	continue;			
			//		
			//			if(referenceNode.objectID==theDataList[j].objectID)
			//			{
			//				//nodeToAdd.objectID=referenceNode.objectID;
			//				if(!theDataList[j].isProcessed)
			//				{
			//					nodeToAdd.isSelected=theDataList[j].isSelected;
			//					rDataList.push_back(nodeToAdd);//Added the Element
			//					theDataList[j].isProcessed=kTrue;
			//				}
			//			}
			//		}
			//	}
			//}
			
			/*isElementoptSelected = kFalse;
			Mediator::isGroupByObj=kTrue;	*/
	}
	return kTrue;
}


#ifndef	__TABLEUTILITY_H__
#define	__TABLEUTILITY_H__

#include "VCPluginHeaders.h"

class TableUtility
{
public:
	doesBoxContainTable();
	getNthTableUID();
	fillDataInCell();
	resizeExistingTable();
};

#endif
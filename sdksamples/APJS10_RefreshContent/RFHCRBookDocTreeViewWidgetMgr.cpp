#include "VCPlugInHeaders.h"
#include "IControlView.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IntNodeID.h"
#include "CTreeViewWidgetMgr.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SysControlIds.h"
#include "RfhID.h"
#include "RFHTreeModel.h"
#include "RfhDataNode.h"
//#include "TPLMediatorClass.h"
#include "CAlert.h"
#include "RFHTreeDataCache.h"
#include "ISpecialChar.h"
#include "ITriStateControlData.h"

#include "CAlert.h"
#include "IAppFramework.h"
#include <set>
//#include "IMessageServer.h"
//#define FILENAME			PMString("RFHCRBookDocTreeViewWidgetMgr.cpp")
//#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
//#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
#define CA(X) CAlert::InformationAlert(X); 


set<NodeID> UniqueCRBookDocNodeIds;

class RFHCRBookDocTreeViewWidgetMgr: public CTreeViewWidgetMgr
{
public:
	RFHCRBookDocTreeViewWidgetMgr(IPMUnknown* boss);
	virtual ~RFHCRBookDocTreeViewWidgetMgr() {}
	virtual	IControlView*	CreateWidgetForNode(const NodeID& node) const;
	virtual	WidgetID		GetWidgetTypeForNode(const NodeID& node) const;
	virtual	bool16 ApplyNodeIDToWidget
		( const NodeID& node, IControlView* widget, int32 message = 0 ) const;
	virtual PMReal GetIndentForNode(const NodeID& node) const;
private:
	PMString getNodeText(const int32& uid, int32 *RowNo) const;
	void indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const;
	enum {ePFTreeIndentForNode=3};
};	

CREATE_PMINTERFACE(RFHCRBookDocTreeViewWidgetMgr, kRFHCRBookDocTreeViewWidgetMgrImpl)

RFHCRBookDocTreeViewWidgetMgr::RFHCRBookDocTreeViewWidgetMgr(IPMUnknown* boss) :
	CTreeViewWidgetMgr(boss)
{	//CA("Inside WidgetManager constructor");
}

IControlView* RFHCRBookDocTreeViewWidgetMgr::CreateWidgetForNode(const NodeID& node) const
{	//CA("CreateWidgetForNode");
	IControlView* retval =
		(IControlView*) ::CreateObject(::GetDataBase(this),
							RsrcSpec(LocaleSetting::GetLocale(), 
							kRfhPluginID, 
							kViewRsrcType, 
							kRFHTreePanelNodeRsrcID),IID_ICONTROLVIEW);
	ASSERT(retval);
	return retval;
}

WidgetID RFHCRBookDocTreeViewWidgetMgr::GetWidgetTypeForNode(const NodeID& node) const
{	//CA("GetWidgetTypeForNode");
	return kRfhTreePanelNodeWidgetID;
}

bool16 RFHCRBookDocTreeViewWidgetMgr::ApplyNodeIDToWidget
(const NodeID& node, IControlView* widget, int32 message) const
{	//CA("ApplyNodeToWidget");
	CTreeViewWidgetMgr::ApplyNodeIDToWidget( node, widget );
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return kFalse;
	}
	do
	{
		UniqueCRBookDocNodeIds.insert(set<NodeID> ::value_type(node));

		InterfacePtr<IPanelControlData> panelControlData(widget, UseDefaultIID());
		ASSERT(panelControlData);
		if(panelControlData==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::RFHCRBookDocTreeViewWidgetMgr::ApplyNodeIDToWidget::panelControlData is nil");		
			break;
		}

		IControlView*   expanderWidget = panelControlData->FindWidget(kRfhTreeNodeExpanderWidgetID);
		ASSERT(expanderWidget);
		if(expanderWidget == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::RFHCRBookDocTreeViewWidgetMgr::expanderWidget is nil");		
			break;
		}

	/*	IControlView*  UnCheckIconWidget = panelControlData->FindWidget(kRfhUnCheckIconWidgetID);
		ASSERT(UnCheckIconWidget);
		if(UnCheckIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::RFHCRBookDocTreeViewWidgetMgr::ApplyNodeIDToWidget::UnCheckIconWidget is nil");
			break;
		}*/

		IControlView*   CheckBoxWidget = panelControlData->FindWidget(kRfhCheckBoxWidgetID);
		ASSERT(CheckBoxWidget);
		if(CheckBoxWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::RFHCRBookDocTreeViewWidgetMgr::ApplyNodeIDToWidget::CheckIconWidget is nil");		
			break;
		}

				

		InterfacePtr<const ITreeViewHierarchyAdapter>   adapter(this, UseDefaultIID());
		ASSERT(adapter);
		if(adapter==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::RFHCRBookDocTreeViewWidgetMgr::ApplyNodeIDToWidget::adapter is nil");		
			break;
		}

		RFHTreeModel model;
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(node);
		int32 uid= uidNodeIDTemp->Get();

		RfhDataNode pNode;
		RFHTreeDataCache dc;

		dc.isExist(uid, pNode);

		TreeNodePtr<IntNodeID>  uidNodeID(node);
		ASSERT(uidNodeID);
		if(uidNodeID == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::RFHCRBookDocTreeViewWidgetMgr::ApplyNodeIDToWidget::uidNodeID is nil");		
			break;
		}

		int32 *RowNo= NULL;
		int32 LocalRowNO;
		RowNo = &LocalRowNO;

		PMString stringToDisplay( this->getNodeText(uidNodeID->Get(), RowNo)/*"Amit Awasthi"*/);
		stringToDisplay.SetTranslatable( kFalse );
		int result = -1; 

		InterfacePtr<ITriStateControlData> itristatecontroldata(CheckBoxWidget, UseDefaultIID());
		if(itristatecontroldata==nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::Update::itristatecontroldata == nil");			
			break;
		}
		if(itristatecontroldata->IsSelected())
		{
			itristatecontroldata->Deselect();
		}
		if(pNode.getIsSelected() == 0)
		{
			/*UnCheckIconWidget->ShowView();
			CheckIconWidget->HideView();*/
			itristatecontroldata->Deselect();
		}
		else
		{
			/*UnCheckIconWidget->HideView();
			CheckIconWidget->ShowView();*/
			itristatecontroldata->Select();
		}

		expanderWidget->HideView();	
		

		IControlView* displayStringView = panelControlData->FindWidget( kRfhPanelTextWidgetID );
		ASSERT(displayStringView);
		if(displayStringView == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::RFHCRBookDocTreeViewWidgetMgr::ApplyNodeIDToWidget::displayStringView is nil");
			break;
		}
		InterfacePtr<ITextControlData>  textControlData( displayStringView, UseDefaultIID() );
		ASSERT(textControlData);
		if(textControlData== nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::RFHCRBookDocTreeViewWidgetMgr::ApplyNodeIDToWidget::textControlData is nil");		
			break;		
		}
		
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		break;
		PMString stringToInsert("");
		stringToInsert.Append(iConverter->handleAmpersandCase(stringToDisplay));
		
        stringToDisplay.ParseForEmbeddedCharacters();
		textControlData->SetString(stringToDisplay);
		
		this->indent( node, widget, displayStringView );
	} while(kFalse);
	return kTrue;
}

PMReal RFHCRBookDocTreeViewWidgetMgr::GetIndentForNode(const NodeID& node) const
{	//CA("Inside GetIndentForNode");
	do
	{
		TreeNodePtr<IntNodeID>  uidNodeID(node);
		ASSERT(uidNodeID);
		if(uidNodeID == nil) 
			break;
		
		RFHTreeModel model;
		int nodePathLengthFromRoot = model.GetNodePathLengthFromRoot(uidNodeID->Get());

		if( nodePathLengthFromRoot <= 0 ) 
			return 0.0;
		
		return  PMReal((nodePathLengthFromRoot * ePFTreeIndentForNode)+0.5);
	} while(kFalse);
	return 0.0;
}

PMString RFHCRBookDocTreeViewWidgetMgr::getNodeText(const int32& uid, int32 *RowNo) const
{	//CA("getNodeText");
	RFHTreeModel model;
	return model.ToString(uid, RowNo);
}

void RFHCRBookDocTreeViewWidgetMgr::indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const
{	//CA("Inside indent");
	const PMReal indent = this->GetIndent(node);	
	PMRect widgetFrame = widget->GetFrame();
	widgetFrame.Left() = indent;
	widget->SetFrame( widgetFrame );
	staticTextWidget->WindowChanged();
	PMRect staticTextFrame = staticTextWidget->GetFrame();
	staticTextFrame.Right( widgetFrame.Right()+1500 );
	
	widgetFrame.Right(widgetFrame.Right()+1500);
	widget->SetFrame(widgetFrame);
	
	staticTextWidget->SetFrame( staticTextFrame );
}
	

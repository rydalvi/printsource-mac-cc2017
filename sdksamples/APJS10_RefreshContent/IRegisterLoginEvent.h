#ifndef __IRegisterLoginEvent_h__
#define __IRegisterLoginEvent_h__

#include "VCPlugInHeaders.h"
#include "IPMUnknown.h"
#include "LNGID.h"
#include "ILoginEvent.h"

class IRegisterLoginEvent : public IPMUnknown
{
	public:	
		enum { kDefaultIID = IID_IREGISTERLOGINEVENT };
		virtual void registerLoginEvent(int32)=0;
};
#endif
#ifndef __REFRESH_H__
#define __REFRESH_H__

#include "VCPluginHeaders.h"
#include "PageData.h"
#include "IIDXMLElement.h"
#include "UIDList.h"
#include "vector"
#include "ITextModel.h"
#include "ISpecialChar.h"

#include "ProgressBar.h"
#include "TagStruct.h"

using namespace std;

class Refresh
{
public:
		

	
	bool16 refreshThisBox(UIDRef&, PMString&, bool16 isInterFaceCall = kFalse);
	//bool16 refreshThisBoxNewoption(UIDRef&, PMString&, bool16 isInterFaceCall = kFalse);//by Amarjit
	bool16 GetPageDataInfo(int);
	void doRefresh(int,PMString BookName = "",PMString documentName="");
	//void doRefreshNewoption(int,PMString BookName = "",PMString documentName="");
	//void doRefreshNewoption(int);//by amarjit sir
	bool16 isValidPageNumber(int32);
	bool16 isValidPageNumber(int32, UID&);
	enum textAction{AtEnd, AtBegin, OverWrite};
	bool16 GetTextstoryFromBox(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story);
	void ChangeColorOfText(InterfacePtr<ITextModel> textModel1 ,int32 StartText1, int32 EndText1, PMString  altColorSwatch);
	bool16 getDocumentSelectedBoxIds(RangeProgressBar *progress = NULL);
	void SetFocusForText(const UIDRef& boxUIDRef ,int32 , int32 );
	void appendToGlobalList(const PageData&);
	int deleteThisBox(UIDRef boxUIDRef);
	ErrorCode ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut);
	ErrorCode RefreshUserTableAsPerCellTagsForOneItem(const UIDRef& boxUIDRef, TagStruct& slugInfo);
	void ToggleBold_or_Italic(InterfacePtr<ITextModel> textModel, VectorHtmlTrackerPtr vectHtmlValuePtr, int32 StartTextIndex);
	void sprayProductKitComponentTableScreenPrintInTabbedTextForm(TagStruct & tagInfo,const UIDRef& curBox, bool16 isKitTable);
	void sprayXrefTableInTabbedTextForm(TagStruct & tagInfo,const UIDRef& curBox);
	void sprayAccessoryTableScreenPrintInTabbedTextForm(TagStruct & tagInfo ,const UIDRef& curBox);
	void sprayHybridTableScreenPrintInTabbedTextForm(TagStruct & tagInfo ,const UIDRef& curBox);
	
	UIDList selectUIDList;
	bool16 doesExist(TagList &tagList);
	UIDRef createNewTableInsideTextFrame(UIDRef &textframeUIDRef,UIDRef& tableUIDref,int32 reportType=0);
	void fillImageInBox(const UIDRef&, TagStruct&, double, PMString, bool16 isInterfacecall = kFalse);

	bool16 getStencilInfo(const UIDList& SelUIDList,CSprayStencilInfo& objCSprayStencilInfo);
	bool16 getStencilInfoNewOption(const UIDList& SelUIDList,CSprayStencilInfo& objCSprayStencilInfo);//By Amarjit Patil
	bool16 getMultipleSectionData();
	bool16 getDataFromDBNewoption(PMString&, TagStruct&, double,double);//By amarjit patil
	ErrorCode DeleteImageFrame(const UIDRef &  graphicFrameUIDRef);//By amarjit patil
	ErrorCode DeleteTextFrame(const UIDRef &  graphicFrameUIDRef);//By amarjit patil
	bool16 refreshCutomTableInBoxNew(const UIDRef& tableUIDRef, int32 isInterFaceCall, double tableTypeId,double& tableId, double CurrSectionid, TagStruct tagStruct, const UIDRef& BoxUIDRef);//by sagar
	bool16 getSectionDataforBoxList(UIDList selectUIDList );
    PMString converIntVectorToUniqueIdString(vector<double> idVector);


private:
	void fitImageInBox(const UIDRef& boxUIDRef, bool16 isInterFaceCall = kFalse);
	int32 getAllItemIDs(double, double, vector<double>&);
	int32 parseTheText(PMString&, vector< vector<PMString> >&);
	bool16 setTextInBox(UIDRef& boxID, PMString& textToInsert, enum textAction);
	bool16 refreshTaggedBox(UIDRef&, PMString&,bool16 isInterFaceCall = kFalse);
	bool16 getDataFromDB(PMString&, TagStruct&, double);
	
	bool16 getAllBoxIds(void);
	void showTagInfo(UIDRef);
	void fillDataInBox(const UIDRef&, TagStruct&, double);
	
	bool16 ImportFileInFrame(const UIDRef&, const PMString&);
	bool16 shouldRefresh(/*const */TagStruct&, const UID&, bool16 isTaggedFrame=kFalse);
	bool16 getAllPageItemsFromPage(int32);
	bool16 doesExist(IIDXMLElement* );
	bool16 doesExist(IIDXMLElement * ptr, UIDRef BoxRef);

	bool16 fillBMSImageInBox(const UIDRef& boxUIDRef, TagStruct& slugInfo, double objectId);
	void customTableRefreshByCell(const UIDRef& curBox, TagStruct& slugInfo, double objectId);
	void customTableRefreshByCellNewoption(const UIDRef& curBox, TagStruct& slugInfo, double objectId);//by Amarjit patil
	

	

};

#endif
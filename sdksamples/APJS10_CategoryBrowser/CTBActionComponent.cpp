//========================================================================================
//  
//  $File: $
//  
//  Owner: RD
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:
#include "IApplication.h"
#include "PaletteRefUtils.h"
#include "PaletteRef.h"
#include "IPanelMgr.h"
#include "ClassMediator.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "LocaleSetting.h"
#include "IWindow.h"
#include "CoreResTypes.h" // kViewRsrcType
#include "ActionDefs.h"	// kDontTranslateChar
#include "RsrcSpec.h"

#include "CActionComponent.h"
#include "CAlert.h"
#include "CTBActionComponent.h"
// Project includes:
#include "CTBID.h"
#include "IAppFramework.h"

#include "IWidgetParent.h"
#include "IPanelDetailController.h"
#include "IPalettePanelUtils.h"
#include "ITreeViewMgr.h"
#include "ClassTreeModel.h"
#include "ClassCache.h"
#include "AcquireModalCursor.h"
#include "CTBSelectionObserver.h"

//extern ClassCache clsCache;
/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup apjs9_categorybrowser

*/
//class CTBActionComponent : public CActionComponent
//{
//public:
///**
// Constructor.
// @param boss interface ptr from boss object on which this interface is aggregated.
// */
//		CTBActionComponent(IPMUnknown* boss);
//
//		/** The action component should perform the requested action.
//			This is where the menu item's action is taken.
//			When a menu item is selected, the Menu Manager determines
//			which plug-in is responsible for it, and calls its DoAction
//			with the ID for the menu item chosen.
//
//			@param actionID identifies the menu item that was selected.
//			@param ac active context
//			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
//			@param widget contains the widget that invoked this action. May be nil. 
//			*/
//		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
//
//	private:
//		/** Encapsulates functionality for the about menu item. */
//		void DoAbout();
//		
//
//
//};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(CTBActionComponent, kCTBActionComponentImpl)

/* CTBActionComponent Constructor
*/
CTBActionComponent::CTBActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
	
}

CTBActionComponent::~CTBActionComponent()
{
	//CAlert::InformationAlert("Calling Action Component Destructor");
	//clsCache.clearMap();
}

/* DoAction
*/
void CTBActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		case kCTBPopupAboutThisActionID:
		case kCTBAboutActionID:
		{
			this->DoAbout();
			break;
		}		

		case kCTBMenuActionID:
		{
			this->DoPallete();
			break;
		}

		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void CTBActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kCTBAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}


void CTBActionComponent::DoPallete(int32 TopBound, int32 LeftBound, int32 BottomBound, int32 RightBound)
{
	//CA("CTBActionComponent::DoPallete");
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return;
	}
	do
	{	
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if(iApplication==nil)
		{
			//CA("iApplication==nil");
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::DoPallete::iApplication == nil");
			break;
		}
		
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); //Amit
		if(iPanelMgr == nil)
		{
			//CA("iPanelMgr==nil");
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::DoPallete::iPanelMgr == nil");				
			break;
		}
		const ActionID MyPalleteActionID = kCTBPanelWidgetActionID;
		IControlView* iControlView = NULL;
		iControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
		if(iControlView==NULL)
		{
			//CA("AP7_CategoryBrowser::CTBActionComponent::DoPallete::iControlView == nil");		
			break;
		}
		
		PaletteRef palRef = iPanelMgr->GetPaletteRefContainingPanel(iControlView );
		if(!palRef.IsValid())
		{
			//CA("palRef is invalid");
			break;
		}
		PaletteRef parentRef = PaletteRefUtils::GetParentOfPalette( palRef ); 
		if(!parentRef.IsValid())
		{
			//CA("parentRef is invalid");
			break;
		}

		PaletteRef parentparentRef = PaletteRefUtils::GetParentOfPalette( parentRef ); 
		if(!parentparentRef.IsValid())
		{
			//CA("parentparentRef is invalid");
			break;
		}
				
		PMLocaleId nLocale=LocaleSetting::GetLocale();
		iPanelMgr->ShowPanelByWidgetID(kCTBPanelWidgetID);

				
		/*SysRect PalleteBound = PaletteRefUtils::GetPaletteBounds(parentparentRef);
		Int32Rect NewRect = (Int32Rect)PalleteBound;
		int32 TemplateTop = NewRect.top;
		int32 TemplateLeft = NewRect.left;
		int32 TemplateBottom = NewRect.bottom;
		int32 TemplateRight = NewRect.right;*/

		//InterfacePtr<IWidgetParent> panelWidgetParent( iControlView, UseDefaultIID());
		//if(panelWidgetParent == NULL)
		//{
		//	//CA("panelWidgetParent is NULL");
		//	break;
		//}
		//InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
		//if(palette == NULL)
		//{
		//	//CA("palette is NULL");
		//	break;
		//}
		//int32 TemplateTop;
		//int32 TemplateLeft;
		//int32 TemplateBottom;
		//int32 TemplateRight;

		//if(palette)
		//{
		//	palette->AddRef();
		//	GSysRect PalleteBounds = palette->GetFrameBBox();
		//	
		//	#ifdef  WINDOWS
		//	TemplateTop		= PalleteBounds.top;
		//	TemplateLeft	= PalleteBounds.left;
		//	TemplateRight	= PalleteBounds.right;
		//	TemplateBottom	= PalleteBounds.bottom;
		//	#else
		//		TemplateTop = SysRectTop(PalleteBounds);
		//		TemplateLeft = SysRectLeft(PalleteBounds);
		//		TemplateRight= SysRectRight(PalleteBounds);
		//		TemplateBottom= SysRectBottom(PalleteBounds);
		//	#endif
		//}
		//
		//int32 NewLeftBound=0;
		//if(LeftBound - 207 < 0)
		//{		
		//	NewLeftBound = RightBound /*-10 */;
		//}
		//else
		//{			
		//	NewLeftBound = LeftBound - (TemplateRight - TemplateLeft + 15);
		//}
		
		//PaletteRefUtils::SetPalettePosition(parentparentRef,NewLeftBound,TopBound);


	}while(kFalse);

}


void CTBActionComponent::ClosePallete(void)
{
	//CA("CTBActionComponent::ClosePallete");
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return;
	}
	do
	{	
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if(iApplication==nil)
		{
			//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::ClosePallete::iApplication == nil");										
			break;
		}

		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager());
		if(ClassMediator::iPanelCntrlDataPtr == nil)
		{
			//CA("ClassMediator::iPanelCntrlDataPtr == nil");
			//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::ClosePallete::iPanelCntrlDataPtr == nil");										
			break;
		}

		if(ClassMediator::ClassTreeCntrlView == nil)
		{
			//CA("ClassMediator::ClassTreeCntrlView == nil");
			//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::ClosePallete::ClassTreeCntrlView == nil");										
			break;
		}
	

		if(iApplication->QueryPanelManager())
		{ 		
//			InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
			InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager());
			if(iPanelMgr == nil)
			{
				//CA("iPanelMgr == nil");
				//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::ClosePallete::iPanelMgr == nil");													
				break;
			}
		 
			//if(iPanelMgr->IsPanelWithMenuIDVisible(kCTBPanelWidgetActionID))
			if(iPanelMgr->IsPanelWithMenuIDShown(kCTBPanelWidgetActionID))
			{				
				//CA("IsPanelWithMenuIDShown return kTrue");
				iPanelMgr->HidePanelByMenuID(kCTBPanelWidgetActionID);				
			}
		}

		/*if(itemProductCategoryCountPtr)
		{
			delete itemProductCategoryCountPtr;
			itemProductCategoryCountPtr = NULL;
		}
		if(itemProductSectionCountPtr)
		{
			delete itemProductSectionCountPtr;
			itemProductSectionCountPtr = NULL;
		}*/

	}while(kFalse);	
}

void CTBActionComponent::DoPallete()
{
	//CA("DoPalleteDefault");
	
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return;
	}
	do
	{	
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if(iApplication==nil)
		{
			//CA("iApplication==nil");
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::DoPallete::iApplication == nil");
			break;
		}
		
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); //Amit
		if(iPanelMgr == nil)
		{
			//CA("iPanelMgr==nil");
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBActionComponent::DoPallete::iPanelMgr == nil");				
			break;
		}
		const ActionID MyPalleteActionID = kCTBPanelWidgetActionID;
		IControlView* iControlView = NULL;
		iControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
		if(iControlView==NULL)
		{
			//CA("AP7_CategoryBrowser::CTBActionComponent::DoPallete::iControlView == nil");		
			break;
		}
		
		PaletteRef palRef = iPanelMgr->GetPaletteRefContainingPanel(iControlView );
		if(!palRef.IsValid())
		{
			//CA("palRef is invalid");
			break;
		}
		PaletteRef parentRef = PaletteRefUtils::GetParentOfPalette( palRef ); 
		if(!parentRef.IsValid())
		{
			//CA("parentRef is invalid");
			break;
		}

		PaletteRef parentparentRef = PaletteRefUtils::GetParentOfPalette( parentRef ); 
		if(!parentparentRef.IsValid())
		{
			//CA("parentparentRef is invalid");
			break;
		}

		//CA("aaaa");
		PMLocaleId nLocale=LocaleSetting::GetLocale();
		iPanelMgr->ShowPanelByWidgetID(kCTBPanelWidgetID);
			
		
	}while(kFalse);
}

void CTBActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint, IPMUnknown* widget)
{
	if (ac == nil)
	{
		ASSERT(ac);
		return;
	}

	for(int32 iter = 0; iter < listToUpdate->Length(); iter++) 
	{
		ActionID actionID = listToUpdate->GetNthAction(iter);
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			return;

		bool16 result=ptrIAppFramework->getLoginStatus();		
	}

}

#include "VCPlugInHeaders.h"
#include "ClassCache.h"
#include "CAlert.h"
#include "IAppFramework.h"


#define CA(Z) CAlert::InformationAlert(Z)
map<double, ClassificationNode>* ClassCache::mpClassCache=NULL;
//map<int32, ClassificationNode>* ClassCache::mpNodeClassCache = NULL;

ClassCache::ClassCache()
{
	if(mpClassCache)
		return;
	mpClassCache=new map<double, ClassificationNode>;

	//---------
	/*if(mpNodeClassCache)
		return;
	mpNodeClassCache=new map<int32, ClassificationNode>;*/

}

bool16 ClassCache::isExist(double id, ClassificationNode& classNode)
{
	if(!mpClassCache)
	{
		mpClassCache=new map<double, ClassificationNode>;
		return kFalse;
	}

	map<double, ClassificationNode>::iterator mapIterator;

	mapIterator=mpClassCache->find(id);
	if(mapIterator==mpClassCache->end())
		return kFalse;

	classNode=(*mapIterator).second;
	return kTrue;
}

bool16 ClassCache::add(ClassificationNode& pNodeToAdd)
{
	if(!mpClassCache)
		mpClassCache=new map<double, ClassificationNode>;

	map<double, ClassificationNode>::iterator mapIterator;
	ClassificationNode firstNode, anyNode;
	double removalId=-1;

	if((mpClassCache->size()+1)<mpClassCache->max_size())//Cache has some space left
	{
		mapIterator=mpClassCache->find(pNodeToAdd.getClassId());
		if(mapIterator==mpClassCache->end())//Not found. Insert it!!
		{
			mpClassCache->insert(map<double, ClassificationNode>::value_type(pNodeToAdd.getClassId(), pNodeToAdd));
			return kTrue;
		}
		//Node exists...Increase the hit count
		firstNode=(*mapIterator).second;
		return kTrue;
	}

	//We do not have any space left...Remove the element which is MOST accessed

	mapIterator=mpClassCache->begin();
	firstNode=(*mapIterator).second;

	removalId=firstNode.getClassId();
	
	for(mapIterator=mpClassCache->begin(); mapIterator!=mpClassCache->end(); mapIterator++)
	{
		anyNode=(*mapIterator).second;
	}
	mpClassCache->erase(removalId);
	mpClassCache->insert(map<double, ClassificationNode>::value_type(pNodeToAdd.getClassId(), pNodeToAdd));
	return kTrue;
}

bool16 ClassCache::clearMap(void)
{
	if(!mpClassCache)
		return kFalse;
	mpClassCache->erase(mpClassCache->begin(), mpClassCache->end());
	delete mpClassCache;
	mpClassCache=NULL;
	return kTrue;
}

bool16 ClassCache::isExist(double parentId, int32 sequence, ClassificationNode& clsNode)
{
	int flag=0;
	if(!mpClassCache)
	{
		mpClassCache=new map<double, ClassificationNode>;
		return kFalse;
	}
	map<double, ClassificationNode>::iterator mapIterator;

	for(mapIterator=mpClassCache->begin(); mapIterator!=mpClassCache->end(); mapIterator++)
	{
		clsNode=(*mapIterator).second;
		if(clsNode.getParentId()==parentId) 
		{
			if(clsNode.getSequence()==(sequence+1))
			{
				clsNode=(*mapIterator).second;
				flag=1;
				break;
			}
		}
	}

	if(!flag)
		return kFalse;
	return kTrue;
}


//------------

//bool16 ClassCache::addNodeID(ClassificationNode& pNodeToAdd)
//{
//	//CA("ClassCache::addNodeID");
//	if(!mpNodeClassCache)
//		mpNodeClassCache = new map<int32 , ClassificationNode>;
//
//	map<int32, ClassificationNode>::iterator mapNodeIterator;
//
//	ClassificationNode firstNode, anyNode;
//	int32 removalId=-1;
//
//	if((mpNodeClassCache->size()+1)<mpNodeClassCache->max_size())//Cache has some space left
//	{
//		//CA("(mpNodeClassCache->size()+1)<mpNodeClassCache->max_size()");
//		mapNodeIterator=mpNodeClassCache->find(pNodeToAdd.getClassId());
//		if(mapNodeIterator==mpNodeClassCache->end())//Not found. Insert it!!
//		{
//			//CA("insert");
//			mpNodeClassCache->insert(map<int32, ClassificationNode>::value_type(pNodeToAdd.getClassId(), pNodeToAdd));
//			return kTrue;
//		}
//		//Node exists...Increase the hit count
//		firstNode=(*mapNodeIterator).second;
//		return kTrue;
//	}
//
//	//We do not have any space left...Remove the element which is MOST accessed
//	mapNodeIterator=mpNodeClassCache->begin();
//	firstNode=(*mapNodeIterator).second;
//
//	removalId=firstNode.getClassId();
//	
//	for(mapNodeIterator=mpNodeClassCache->begin(); mapNodeIterator!=mpNodeClassCache->end(); mapNodeIterator++)
//	{
//		anyNode=(*mapNodeIterator).second;
//	}
//	mpNodeClassCache->erase(removalId);
//	mpNodeClassCache->insert(map<int32, ClassificationNode>::value_type(pNodeToAdd.getClassId(), pNodeToAdd));
//	
//	return kTrue;
//}
//bool16 ClassCache::isNodeExist(int32 id, ClassificationNode& classNode)
//{
//	//CA("ClassCache::isNodeExist");
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		//ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassCache::isNodeExist::ptrIAppFramework == nil");
//		return kFalse;
//	}
//	if(!mpNodeClassCache)
//	{
//		//ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassCache::isNodeExist::!mpNodeClassCache");
//		mpNodeClassCache=new map<int32, ClassificationNode>;
//		return kFalse;
//	}
//
//	map<int32, ClassificationNode>::iterator mapNodeIterator;
//
//	mapNodeIterator=mpNodeClassCache->find(id);
//	if(mapNodeIterator==mpNodeClassCache->end())
//	{
//		//ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassCache::isNodeExist::mapNodeIterator==mpNodeClassCache->end()");
//		return kFalse;
//	}
//
//	classNode=(*mapNodeIterator).second;
//	return kTrue;
//}
//
//bool16 ClassCache::clearNodeMap(void)
//{
//	if(!mpNodeClassCache)
//		return kFalse;
//	mpNodeClassCache->erase(mpNodeClassCache->begin(), mpNodeClassCache->end());
//	delete mpNodeClassCache;
//	mpNodeClassCache=NULL;
//	return kTrue;
//}


vector<ClassificationNode>* ClassCache::getAllChildren(double parentId)
{
	int flag=0;
	if(!mpClassCache)
	{
		return NULL;
	}

	ClassificationNode clsNode;
	vector<ClassificationNode>* pClassificationNodeVec = new vector<ClassificationNode>;
	int32 count=1;//Added
	map<double, ClassificationNode>::iterator mapIterator;
	for(mapIterator=mpClassCache->begin(); mapIterator!=mpClassCache->end(); mapIterator++)
	{
		
		clsNode=(*mapIterator).second;		
		if(clsNode.getParentId()==parentId) 
		{
			flag = true;
			//By amarjit patil
			//if(count==clsNode.getSequence())       // commented
			pClassificationNodeVec->push_back(clsNode);
			
		}
		//count++;        //commented by mahesh
	}

	if(!flag)
	{
		delete pClassificationNodeVec;
		return NULL;
	}
	return pClassificationNodeVec;
}

int32 ClassCache::getSize(){
	return static_cast<int32>(mpClassCache->size());
}
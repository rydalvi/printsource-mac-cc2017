#ifndef __CTBLoginEventsHandler_h__
#define __CTBLoginEventsHandler_h__

#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "CTBID.h"
#include "ILoginEvent.h"

class CTBLoginEventsHandler : public CPMUnknown<ILoginEvent>
{
public:
	CTBLoginEventsHandler(IPMUnknown* );
	~CTBLoginEventsHandler();
	bool16 userLoggedIn();
	bool16 userLoggedOut();
	bool16 resetPlugIn();
};

#endif
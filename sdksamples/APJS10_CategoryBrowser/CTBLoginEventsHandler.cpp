#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "CTBID.h"
#include "ILoginEvent.h"
#include "CAlert.h"
#include "CTBLoginEventsHandler.h"
#include "CTBSelectionObserver.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IWindow.h"
#include "IApplication.h"
#include "CTBActionComponent.h"
#include "IWidgetParent.h"
#include "ClassMediator.h"
//#include "ILoggerFile.h"

#define CA(z) CAlert::InformationAlert(z)

extern int32 node_count ;
extern int32 isContentSprayerFlag ;
extern double currPublicationID ;
extern PMString currPublicationName ;
CREATE_PMINTERFACE(CTBLoginEventsHandler,kCTBLoginEventsHandlerImpl)

//extern int32 check;


CTBLoginEventsHandler::CTBLoginEventsHandler
(IPMUnknown* boss):CPMUnknown<ILoginEvent>(boss)
{
}

CTBLoginEventsHandler::~CTBLoginEventsHandler()
{}

bool16 CTBLoginEventsHandler::userLoggedIn(void)
{
	//CA("user is loggin in");
	do
	{
		//check = 1;
		//CA("user is loggin in ProductFinder");
		CTBSelectionObserver selObserver(this);
		//CA("user is loggin 3");
		//ClassMediator::LoadData = kTrue;
		selObserver.loadPaletteData();
		//CA("after user logged in");

	}while(kFalse);
	return kTrue;
}

bool16 CTBLoginEventsHandler::userLoggedOut(void)
{
	//CA("User logged out");
	//check = 0;
	CTBActionComponent actionComponetObj(this);
	actionComponetObj.ClosePallete();
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return kTrue;
	ptrIAppFramework->setLocaleId(-1);

	return kTrue;	
}

bool16 CTBLoginEventsHandler::resetPlugIn(void)
{
	//CA("CTBLoginEventsHandler::resetPlugIn");
		

	CTBActionComponent actionComponetObj(this);
	actionComponetObj.ClosePallete();

	ClassMediator::clasId=0;
	ClassMediator::clasName="";
	ClassMediator::hier="";
	ClassMediator::whichMode =0;
	ClassMediator::languageID = 1;
	ClassMediator::ClassTreeCntrlView=nil;
	ClassMediator::iPanelCntrlDataPtr=nil;
	ClassMediator::iPanelCntrlDataPtrTemp=nil;
	ClassMediator::PrFamilyDropDownView = NULL;
	ClassMediator::PrGroupDropDownView = NULL;
	ClassMediator::ClassPathStringView = NULL;
	ClassMediator::pathToSend = "";

	node_count  = 0 ;
	currPublicationID = -1;
	currPublicationName = "";


	CTBSelectionObserver selObj(this);
	selObj.loadPaletteData();


	return kTrue;
}



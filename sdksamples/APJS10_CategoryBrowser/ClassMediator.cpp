#include "VCPluginHeaders.h"
#include "ClassMediator.h"

double ClassMediator::clasId=0;
PMString ClassMediator::clasName="";
PMString ClassMediator::hier="";
int32 ClassMediator::whichMode =0;
double ClassMediator::languageID = 1;
IControlView* ClassMediator::ClassTreeCntrlView=nil;
IPanelControlData* ClassMediator::iPanelCntrlDataPtr=nil;
IPanelControlData* ClassMediator::iPanelCntrlDataPtrTemp=nil;
IControlView* ClassMediator::PrFamilyDropDownView = NULL;
IControlView* ClassMediator::PrGroupDropDownView = NULL;
IControlView* ClassMediator::ClassPathStringView = NULL;
PMString ClassMediator::pathToSend = "";
bool16 ClassMediator::shouldLoad = kFalse;

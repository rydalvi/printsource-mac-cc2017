#include "VCPlugInHeaders.h"
#include "IHierarchy.h"	
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
//#include "LayoutUtils.h" //Cs3 Depricated
#include "ILayoutUtils.h" //CS4
#include "CTBID.h"
#include "ClassTreeModel.h"
#include "CAlert.h"
#include "ClassificationNode.h"
#include "ClassCache.h"
#include "IAppFrameWork.h"
//#include "ISpecialChar.h"
#include "ClassMediator.h"

extern int32 isContentSprayerFlag ; //1 ContentSprayer:Event Tree, 2: TemplateBuilder:Class tree, 3: Search ?

#define CA(X) CAlert::InformationAlert(X)
//--------30/09/06 It is an Adaptee in Adapter pattern...The data model, such as a tree model representing the XML logical structure of a document ----//
ClassCache clsCache;

double ClassTreeModel::root=-1;
PMString ClassTreeModel::classificationName;

static double rootNew;

//extern int32 currSelLanguageID ;

//extern int32 isCheckCount;
int32 EventLevel = 3;

//ItemProductCountMapPtr itemProductCategoryCountPtr = NULL;
//ItemProductCountMapPtr itemProductSectionCountPtr = NULL;

ClassTreeModel::ClassTreeModel(){}

ClassTreeModel::~ClassTreeModel()
{	
}

double ClassTreeModel::GetRootUID() const
{
	//CA("in GetRootUID()");
	ClassificationNode clsNode;
	double retval=0; //kInvalidUID;

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return retval;
	}
	//ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetRootUID::entering ");
	
	if(clsCache.isExist(root, clsNode))
	{
		//CA("found in cache");
		retval=root;
	}
	else 
	{
		//ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetRootUID ::Not Exist");

		double masterID = -2;
		double eventClassID = -3;
		PMString rootName("Master");

		clsNode.setClassId(masterID);
		clsNode.setChildCount(1);
		clsNode.setClassName(rootName);
		clsNode.setLevel(0);
	 	clsNode.setParentId(-1);
		clsNode.setSequence(0);
		clsNode.setIsPUB(kFalse);
		clsNode.setIsEventType(kFalse);
		
		clsCache.add(clsNode);
		
		int count=1;
		int countNew=1;
		/*CClassificationValue classVal;
		classVal.setClass_id(masterID);*/
		
		if(root == -2)		
		{
			root=-2;
		}
		else
		{
			root=-1;
			ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetRootUID ::root=-1;");
			return retval;
		}
		
	//	if(isCheckCount)
	//	{
	//		if(isContentSprayerFlag == 2 || isContentSprayerFlag == 3)
	//		{
	//			if(itemProductCategoryCountPtr != NULL)
	//			{
	//				delete itemProductCategoryCountPtr;
	//				itemProductCategoryCountPtr = NULL;
	//			}

	//			itemProductCategoryCountPtr = ptrIAppFramework->GETPROJECTPRODUCTS_getItemProductCountForSection(1);
	//		}
	//		if(isContentSprayerFlag == 1 || isContentSprayerFlag == 3)
	//		{
	//			if(itemProductCategoryCountPtr != NULL)
	//			{
	//				delete itemProductCategoryCountPtr;
	//				itemProductCategoryCountPtr = NULL;
	//			}
	//			itemProductSectionCountPtr = ptrIAppFramework->GETPROJECTPRODUCTS_getItemProductCountForSection(0);
	//			ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetRootUID::after GETPROJECTPRODUCTS_getItemProductCountForSection");
	//		}

	//		if(itemProductCategoryCountPtr == NULL && (isContentSprayerFlag == 2 || isContentSprayerFlag == 3))
	//		{
	//			delete itemProductCategoryCountPtr;
	//			return retval;
	//		}
	//		if(itemProductSectionCountPtr == NULL && (isContentSprayerFlag == 1 || isContentSprayerFlag == 3))
	//		{
	//			delete itemProductSectionCountPtr;
	//			return retval;
	//		}
	//	}

		if(isContentSprayerFlag == 2 /*|| isContentSprayerFlag == 3*/)	//----------
		{
			//CA("GetRootUID  ::  isContentSprayerFlag == 2");
			VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(1/*currSelLanguageID*/);
			if(vectClassInfoValuePtr==nil)
			{//CA("vectClassInfoValuePtr==nil");
				ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetRootUID::ClassificationTree_getRoot'svectClassInfoValuePtr==nil");
				return retval;
			}

			VectorClassInfoValue::iterator it;
			//CA("A");

			for(it=vectClassInfoValuePtr->begin(); it!=vectClassInfoValuePtr->end(); it++)
			{
				clsNode.setClassificationValue(*it);
				clsNode.setClassId(it->getClass_id());
				clsNode.setChildCount(it->getChildCount());

				PMString tempclassificationName;
				
				tempclassificationName = it->getClassification_name();				

				clsNode.setClassName(tempclassificationName);
				//CA(tempclassificationName);
				clsNode.setLevel(/*it->getClass_level()*/1);
				clsNode.setParentId(masterID);
				clsNode.setSequence(countNew);
				clsNode.setIsEventType(kFalse);
				clsNode.setIsPUB(kFalse);

				rootNew=it->getClass_id();
				clsCache.add(clsNode);
				countNew++;

			}
			if(vectClassInfoValuePtr)
				delete vectClassInfoValuePtr;

			VectorClassInfoPtr vect = ptrIAppFramework->ClassificationTree_getAllChildren(rootNew, /*currSelLanguageID*/ 1);
			if(vect==nil)
			{
				//CA("vect==nil");
				ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetRootUID::vect==nil");
				return retval;
			}

			VectorClassInfoValue::iterator it1;
		    //CA("B");

			//-----------
			int32 prodctSize = 0;
			int32 item_Size  = 0;
			int32 item_productSize = 0;
			bool16 val_flag = kFalse;

			count=1;
			for(it1=vect->begin(); it1!=vect->end(); it1++)
			{
				double class_id = it1->getClass_id();
				clsNode.setClassificationValue(*it1);
				clsNode.setClassId(class_id);
				clsNode.setChildCount(it1->getChildCount());
				
				PMString tempclassificationName("");				
				tempclassificationName = it1->getClassification_name();

				//if(isCheckCount)
				//{
				//	//CA("isCheckCount == kTrue");
				//	map<int32, int32>::iterator mapItr;
				//	mapItr = itemProductCategoryCountPtr->find(class_id);
				//	int32 catChild_Count = mapItr->second;

				//	tempclassificationName.Append(" (");
				//	tempclassificationName.AppendNumber(catChild_Count);
				//	tempclassificationName.Append(")");
				//	clsNode.setClassName(tempclassificationName);
				//}
				//else
				//{
					clsNode.setClassName(tempclassificationName);
				//}
				//clsNode.setClassName(tempclassificationName);

				clsNode.setLevel(/*it1->getClass_level()*/2);
				clsNode.setParentId(rootNew);
				clsNode.setSequence(count);
				clsNode.setIsEventType(kFalse);
				clsNode.setIsPUB(kFalse);
				clsCache.add(clsNode);

				count++;
				/*if(vectorObjectValuePtr)
				{
					delete vectorObjectValuePtr;
					vectorObjectValuePtr = NULL;
				}
				if(vectorItemModelPtr)
				{
					delete vectorItemModelPtr;
					vectorItemModelPtr = NULL;
				}*/

			}
			if(vect)
				delete vect;
		}
	//	
	//	//---- Adding Event Node --------
		else if(isContentSprayerFlag == 1 /*|| isContentSprayerFlag == 3*/)	
		{
			//ptrIAppFramework->LogDebug("isContentSprayerFlag == 1");
			PMString childName("Events");

			ClassificationNode clsEventNode;
			clsEventNode.setClassId(eventClassID);
			clsEventNode.setClassName(childName);
			clsEventNode.setLevel(1);
	 		clsEventNode.setParentId(masterID);
		
			//if(isContentSprayerFlag == 1)
			clsEventNode.setSequence(1);
			//else
			//	clsEventNode.setSequence(2);

			clsEventNode.setIsEventType(kFalse);
			clsEventNode.setIsPUB(kTrue);
			
			VectorPubModelPtr vec_pubModelPtr = ptrIAppFramework->ProjectCache_getAllProjects(/*currSelLanguageID*/1);
			if(vec_pubModelPtr == NULL)
			{
				//CA("vec_pubModelPtr == NULL");
				ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetRootUID ::vec_pubModelPtr == NULL");
				return retval;
			}

			//int32 PM_Project_Level = ptrIAppFramework->getPM_Project_Levels();
			
			//if(ptrIAppFramework->getConfigCM_DISPLAY_PUB_TYPES_GROUP_TREE())
			if(ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("CM_DISPLAY_PUB_TYPES_GROUP_TREE"))
			{
				//CA("ptrIAppFramework->getConfigCM_DISPLAY_PUB_TYPES_GROUP_TREE()");
				EventLevel = 4;

				VectorTypeInfoValue pubTypeVector;

				VectorPubModel::iterator itr3;
				PMString tempTypeName = "";
				for(itr3 = vec_pubModelPtr->begin(); itr3 != vec_pubModelPtr->end(); itr3++)
				{
						ClassificationNode clsPubNode;
						clsPubNode.setCPubModel(*itr3);

						CTypeModel pubTypeObj;
						pubTypeObj.setTypeId((itr3)->getPubTypeId());
						pubTypeObj.setName((itr3)->getEventType());

						if(pubTypeVector.size() == 0)
						{
							pubTypeVector.push_back(pubTypeObj);
						}

						bool16 isExistFlag =  kFalse;
						for(int i=0; i < pubTypeVector.size(); i ++)
						{

							if(pubTypeVector.at(i).getTypeId() == itr3->getPubTypeId())
							{
								isExistFlag = kTrue;
								break;
							}
						}

						if(!isExistFlag)
							pubTypeVector.push_back(pubTypeObj);
				}


				//VectorTypeInfoPtr vecTypeInfoPtr = ptrIAppFramework->TypeCache_getPubTypeList();
				//if(vecTypeInfoPtr != NULL)
				if(pubTypeVector.size() >0)
				{
					//CA("vecTypeInfoPtr != NULL");
					int32 vec_size = static_cast<int32>(pubTypeVector.size());
					
					clsEventNode.setChildCount(vec_size);

					//clsEventNode.setChildCount(0);
					clsCache.add(clsEventNode);
		
					double groupId = -4;
					int32 seq = 1;
					//VectorTypeInfoValue::iterator itr;
					//for(itr = vecTypeInfoPtr->begin(); itr != vecTypeInfoPtr->end(); itr++,groupId--)
					for(int j =0; j < pubTypeVector.size(); j++, groupId-- )
					{
						//CA("inside for");
						ClassificationNode clsTypeNode;
						
						double typeId = pubTypeVector.at(j).getTypeId();

						clsTypeNode.setClassId(groupId);
						PMString tempPubnName("");
						//if(!iConverter)
							tempPubnName = pubTypeVector.at(j).getName();
						/*else{
							tempPubnName = iConverter->translateString(pubTypeVector.at(j).getName());
							tempPubnName = iConverter->handleAmpersandCase(tempPubnName);
						}*/
						clsTypeNode.setClassName(tempPubnName);
						clsTypeNode.setLevel(3);
	 					clsTypeNode.setParentId(eventClassID);
						clsTypeNode.setSequence(seq);
						clsTypeNode.setIsEventType(kTrue);
						clsTypeNode.setIsPUB(kTrue);
						
						int32 childCount = 0;
						int32 pubSeq = 0;
						VectorPubModel::iterator itr1;
						PMString tempTypeName = "";
						for(itr1 = vec_pubModelPtr->begin(); itr1 != vec_pubModelPtr->end(); itr1++)
						{
							
							if(itr1->getPubTypeId() == typeId)
							{
								//CA("itr1->getpub_type_id() == typeId");
								childCount++;
								pubSeq++;

								ClassificationNode clsPubNode;
								
								clsPubNode.setCPubModel(*itr1);
								double pubId = itr1->getEventId();
	
								clsPubNode.setClassId(pubId);

								//if(!iConverter)
									tempTypeName = itr1->getName();
								/*else{
									tempTypeName = iConverter->translateString(itr1->getName());
									tempTypeName = iConverter->handleAmpersandCase(tempTypeName);
								}*/
								clsPubNode.setClassName(tempTypeName);
								clsPubNode.setLevel(4);
		 						
								clsPubNode.setParentId(groupId);
								clsPubNode.setSequence(pubSeq);
								clsPubNode.setIsEventType(kTrue);
								clsPubNode.setIsPUB(kTrue);
								//if(PM_Project_Level == 2)
								//{
								//	VectorPubModelPtr vec_pubmodelptr = ptrIAppFramework->ProjectCache_getAllChildren(pubId,currSelLanguageID/*1*/);
								//	if(vec_pubmodelptr != NULL)
								//	{
								//		VectorPubModel::iterator itrr = vec_pubmodelptr->begin();
								//		VectorPubModelPtr vec_pubmodelptr1 = ptrIAppFramework->ProjectCache_getAllChildren(itrr->getPublicationID(),currSelLanguageID/*1*/);
								//		if(vec_pubmodelptr1 != NULL)
								//		{
								//			clsPubNode.setChildCount(static_cast<int32>(vec_pubmodelptr1->size()));
								//			
								//			int32 secSeq = 1;
								//			VectorPubModel::iterator sectionItr;
								//			PMString tempSecName = "";
								//			for(sectionItr = vec_pubmodelptr1->begin(); sectionItr != vec_pubmodelptr1->end(); sectionItr++)
								//			{
								//				ClassificationNode clsSecNode;
								//				
								//				clsSecNode.setCPubModel(*sectionItr);
								//				int32 secId = sectionItr->getPublicationID();
								//				clsSecNode.setClassId(secId);
								//				
								//				if(!iConverter)
								//					tempSecName = sectionItr->getName();
								//				else{
								//					tempSecName = iConverter->translateString(sectionItr->getName());
								//					tempSecName = iConverter->handleAmpersandCase(tempSecName);
								//				}

								//				//--------
								//				if(isCheckCount)
								//				{
								//					map<int32, int32>::iterator mapItr;
								//					mapItr = itemProductSectionCountPtr->find(secId);
								//					int32 secChild_Count = mapItr->second;

								//					tempSecName.Append(" (");
								//					tempSecName.AppendNumber(secChild_Count);
								//					tempSecName.Append(")");
								//					clsSecNode.setClassName(tempSecName);

								//				}
								//				else
								//				{
								//					clsSecNode.setClassName(tempSecName);
								//				}

								//				//clsSecNode.setClassName(tempSecName);
								//			//	CA(itr1->getName());
								//				clsSecNode.setLevel(5);
 							//					clsSecNode.setParentId(pubId);
								//				clsSecNode.setSequence(secSeq);
								//				clsSecNode.setIsEventType(kTrue);
								//				clsSecNode.setIsPUB(kTrue);
								//				
								//				VectorPubModelPtr vec_pubmodelptr2 = ptrIAppFramework->ProjectCache_getAllChildren(secId,currSelLanguageID/*1*/);
								//				if(vec_pubmodelptr2 != NULL)
								//					clsSecNode.setChildCount(static_cast<int32>(vec_pubmodelptr2->size()));
								//				else
								//					clsSecNode.setChildCount(0);
								//				secSeq++;
								//				clsCache.add(clsSecNode);
								//				
								//				/*if(vectorGroupInfoValuePtr)
								//				{
								//					delete vectorGroupInfoValuePtr;
								//					vectorGroupInfoValuePtr = NULL;
								//				}*/
								//			}

								//		}
								//		else
								//			clsPubNode.setChildCount(0);
								//	}
								//	else
								//		clsPubNode.setChildCount(0);
								//}
								//else
									clsPubNode.setChildCount(itr1->getchild_Count());
								clsCache.add(clsPubNode);
							}
						}

						clsTypeNode.setChildCount(childCount);	
						clsCache.add(clsTypeNode);
						seq++;		
					
					}

				}
				
			}
			else
			{
				//CA("Else Type");
				EventLevel = 2;
				if(vec_pubModelPtr != NULL)
				{
					//ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetRootUID ::vec_pubModelPtr != NULL  ");
					

					VectorPubModel::iterator itr;
					int32 count=1;
					for(itr = vec_pubModelPtr->begin(); itr != vec_pubModelPtr->end(); itr++)
					{

						ClassificationNode clsPubNode;						
						clsPubNode.setCPubModel(*itr);
						double publicationID = itr->getEventId();
						clsPubNode.setClassId(publicationID);
						PMString tempPubnName("");						
						tempPubnName = itr->getName();
						clsPubNode.setClassName(tempPubnName);
						clsPubNode.setLevel(2);
						clsPubNode.setParentId(eventClassID);
						clsPubNode.setSequence(count);
						clsPubNode.setIsEventType(kTrue);
						clsPubNode.setIsPUB(kTrue);
					
						clsPubNode.setChildCount(itr->getchild_Count());

						ClassificationNode dummyNode;
						if(clsCache.isExist(publicationID, dummyNode))
							continue;

						clsCache.add(clsPubNode);
						count++;

								VectorPubModelPtr vec_SecChildPtr = ptrIAppFramework->ProjectCache_getAllChildren(/*itrVect->getEventId()*/publicationID,/*currSelLanguageID*/1);
								if(vec_SecChildPtr != NULL)
								{
									/*PMString str("vec_SecChildPtr->size() = ");
									str.AppendNumber(static_cast<int32>(vec_SecChildPtr->size()));
									CA(str);*/

									//clsPubNode.setChildCount(static_cast<int32>(vec_SecChildPtr->size()));
									int32 seqCount = 1;
									
									VectorPubModel::iterator sectionItr;

									for(sectionItr = vec_SecChildPtr->begin(); sectionItr != vec_SecChildPtr->end(); sectionItr++)
									{
										ClassificationNode clsSectionNode;
										PMString tempSecName("");
										
										clsSectionNode.setCPubModel(*sectionItr);
										double sectionID = sectionItr->getEventId();
										clsSectionNode.setClassId(sectionID);
										tempSecName = sectionItr->getName();										
										
										clsSectionNode.setClassName(tempSecName);
									
										clsSectionNode.setLevel(3);
										clsSectionNode.setParentId(publicationID);
										clsSectionNode.setSequence(seqCount);
										clsSectionNode.setIsEventType(kTrue);
										clsSectionNode.setIsPUB(kTrue);
										clsSectionNode.setChildCount(sectionItr->getchild_Count());

										ClassificationNode dummyNode;
										if(clsCache.isExist(sectionID, dummyNode))
											continue;

										clsCache.add(clsSectionNode);
										seqCount++;
										
									}
									
									//-------
									//if(vec_SecChildPtr)
									//	delete vec_SecChildPtr;
								
							}
										
						
					}
					
					clsEventNode.setChildCount(count-1/*static_cast<int32>(vec_pubModelPtr->size())*/);
					clsCache.add(clsEventNode);

					PMString str2("FOR PUBLICATION     Child count = ");
					str2.AppendNumber(count/*static_cast<int32>(vec_pubModelPtr->size())*/);
					ptrIAppFramework->LogError(  str2);

					PMString str1("");
					str1.AppendNumber(clsCache.getSize());
					ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetRootUID ::  clsCache.getSize() = "  +  str1);
				}
			}
			
			//--------
			if(vec_pubModelPtr)
				delete vec_pubModelPtr;

		}

		retval=root;
	}

	return retval;
}

//Clear till here

int32 ClassTreeModel::GetRootCount() const
{
	//CA("in GetRootCount()");
	ClassificationNode clsNode;
	int32 retval=-1;

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return retval;
	}
	//ptrIAppFramework->LogDebug("AP7_CategoryBrowser::ClassTreeModel::GetRootCount::entering ");	

	if(clsCache.isExist(root, clsNode))
	{
		return clsNode.getChildCount();
	}
	else
	{
		/*CClassificationValue classVal;
		classVal.setClass_id(root);*/		
		//ptrIAppFramework->LogDebug("ClassTreeModel::GetRootCount");

		VectorClassInfoPtr vect = ptrIAppFramework->ClassificationTree_getAllChildren(root, /*currSelLanguageID*/1); //CLASSMngr_getAllClassesOfParent(classVal);
		if(vect==nil)
		{
			ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetRootCount::ClassificationTree_getAllChildren's vect==nil");
			return retval;
		}

		VectorClassInfoValue::iterator it1;
	
		int count=1;
		for(it1=vect->begin(); it1!=vect->end(); it1++)
		{
			clsNode.setClassId(it1->getClass_id());
			clsNode.setChildCount(it1->getChildCount());
			
			PMString tempclassificationName("");
			
			tempclassificationName = it1->getClassification_name();			
			clsNode.setClassName(tempclassificationName);
			clsNode.setLevel(it1->getClass_level());
			clsNode.setParentId(root);
			clsNode.setSequence(count);
			clsNode.setIsEventType(kFalse);
			clsNode.setIsPUB(kFalse);
			clsCache.add(clsNode);
			count++;
		}

		if(vect)
			delete vect;

		retval = (count-1);
	}
	return retval;
}

int32 ClassTreeModel::GetChildCount(const double& uid) const
{
	//CA("in GetChildCount()");
	ClassificationNode clsNode;
	int32 retval=-1;

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return retval;
	}
	//ptrIAppFramework->LogDebug("AP7_CategoryBrowser::ClassTreeModel::GetChildCount::entering ");	
	
	if(clsCache.isExist(uid, clsNode))
	{
		PMString child("");
		int32 numberOfChild = clsNode.getChildCount();
		child.AppendNumber(numberOfChild);
		//CA("No of Children = " + child);
		//CA(child);
		return clsNode.getChildCount();
	}
	return retval;
}

double ClassTreeModel::GetParentUID(const double& uid) const
{
	//CA("in GetParentUID()");
	double retval = 0; //kInvalidUID;
	ClassificationNode clsNode;

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return retval;
	}
	//ptrIAppFramework->LogDebug("AP7_CategoryBrowser::ClassTreeModel::GetParentUID::entering ");

	

	if(clsCache.isExist(uid, clsNode))
	{
		retval = clsNode.getParentId();
	}
	if(retval == 0)
		return 0; //kInvalidUID;
	return retval;
}

//CLEAR

int32 ClassTreeModel::GetChildIndexFor(const double& parentUID, const double& childUID) const
{
	//CA("in GetChildIndexFor");
	ClassificationNode clsNode;
	int32 retval=-1;

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return retval;
	}
	//ptrIAppFramework->LogDebug("AP7_CategoryBrowser::ClassTreeModel::GetChildIndexFor::entering ");


	

	if(clsCache.isExist(childUID, clsNode))
	{
		retval=clsNode.getSequence();
	}
	else
	{
		/*InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			CAlert::ErrorAlert("Interface for IAppFramework not found.");
			return retval;
		}*/

		//ptrIAppFramework->LogDebug("ClassTreeModel::GetChildIndexFor");
		
		/*CClassificationValue classVal;
		classVal.setClass_id(parentUID.Get());*/
		
		VectorClassInfoPtr vect=
			ptrIAppFramework->ClassificationTree_getAllChildren(parentUID, /*currSelLanguageID*/ 1);//CLASSMngr_getAllClassesOfParent(classVal);

		if(vect==nil)
		{
			ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetChildIndexFor::ClassificationTree_getAllChildren's vect == nil");
			return retval;
		}

		VectorClassInfoValue::iterator it;
		int count=1;
		for(it=vect->begin(); it!=vect->end(); it++)
		{
			if(it->getClass_id()==childUID)
				retval=count;

			clsNode.setClassId(it->getClass_id());
			clsNode.setChildCount(it->getChildCount());
			
				
			PMString tempclassificationName("");
			
			tempclassificationName = it->getClassification_name();	

			clsNode.setClassName(tempclassificationName);
			clsNode.setLevel(it->getClass_level());
			clsNode.setParentId(parentUID);
			clsNode.setSequence(count);
			clsNode.setIsEventType(kFalse);
			clsNode.setIsPUB(kFalse);
			clsCache.add(clsNode);
			count++;
		}
		if(vect)
			delete vect;
	}
	return (retval-1);
}

double ClassTreeModel::GetNthChildUID(const double& parentUID, const int32& index) const
{
	//CA("GetNthChildUID");
	ClassificationNode clsNode;	
	double retval=0; //kInvalidUID;

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return retval;
	}
	//ptrIAppFramework->LogDebug("AP7_CategoryBrowser::ClassTreeModel::GetNthChildUID::entering ");


	if(clsCache.isExist(parentUID, index, clsNode))
	{
		//CA("exist");
		retval=clsNode.getClassId();
	}
	else
	{
		//CA("else");
		ClassificationNode ParentClsNode;
		clsCache.isExist(parentUID, ParentClsNode);
					
		/*InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetNthChildUID::ptrIAppFramework == nil");
			return retval;
		}*/

		//ptrIAppFramework->LogDebug("ClassTreeModel::GetNthChildUID");

		if(!ParentClsNode.getIsPUB())
		{
			//ptrIAppFramework->LogDebug("!!!!!!!!!!ParentClsNode.getIsPUB()");
			/*CClassificationValue classVal;
			classVal.setClass_id(parentUID.Get());*/

			VectorClassInfoPtr vect = ptrIAppFramework->ClassificationTree_getAllChildren(parentUID/*.Get()*/, /*currSelLanguageID*/ 1);
			if(vect==nil)
			{
				/*PMString str("parentUID.Get() = ");
				str.AppendNumber(parentUID.Get());
				str.Append("languageId = ");
				str.AppendNumber(currSelLanguageID);*/
				
				ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetNthChildUID::ClassificationTree_getAllChildren's vect == nil"/* + str*/);
				return retval;
			}

			VectorClassInfoValue::iterator it;
			int count=1;

			for(it=vect->begin(); it!=vect->end(); it++)
			{
				if(count==(index+1))
					retval=it->getClass_id();

				clsNode.setClassificationValue(*it);
				clsNode.setClassId(it->getClass_id());
				clsNode.setChildCount(it->getChildCount());

				PMString tempclassificationName("");
				
				tempclassificationName = it->getClassification_name();
				
				//--------
				/*if(isCheckCount)
				{
					map<int32, int32>::iterator mapItr;
					mapItr = itemProductCategoryCountPtr->find(it->getClass_id());
					int32 catChild_Count = mapItr->second;

					tempclassificationName.Append(" (");
					tempclassificationName.AppendNumber(catChild_Count);
					tempclassificationName.Append(")");
					clsNode.setClassName(tempclassificationName);

				}
				else
				{*/
				clsNode.setClassName(tempclassificationName);
				//}

				//clsNode.setClassName(tempclassificationName);
				clsNode.setLevel(/*it->getClass_level()*/ParentClsNode.getLevel() +1);
				clsNode.setParentId(parentUID/*.Get()*/);
				clsNode.setSequence(count);
				clsNode.setIsEventType(kFalse);
				clsNode.setIsPUB(kFalse);

				clsCache.add(clsNode);
				count++;
			}
			/*if(vect)
				delete vect;*/

		}
		else
		{
			//ptrIAppFramework->LogDebug("ParentClsNode.getIsPUB()");

			/*CClassificationValue classVal;
			classVal.setClass_id(parentUID.Get());*/

			VectorPubModelPtr  vectChild = ptrIAppFramework->ProjectCache_getAllChildren(ParentClsNode.getClassId(), /*currSelLanguageID*/1);
			if(vectChild==NULL)
			{
				/*PMString str("ParentClsNode.getClassId() = ");
				str.AppendNumber(ParentClsNode.getClassId());
				str.Append("languageId = ");
				str.AppendNumber(currSelLanguageID);*/
				
				ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetNthChildUID::ClassificationTree_getAllChildren's vectChild == nil"/* + str*/);
				return retval;
			}

			VectorPubModel::iterator itr;
			int count=1;

			for(itr=vectChild->begin(); itr!=vectChild->end(); itr++)
			{

				if(count==(index+1))
					retval=itr->getEventId();

				clsNode.setCPubModel(*itr);
				clsNode.setClassId(itr->getEventId());
				clsNode.setChildCount(itr->getchild_Count());

				//VectorPubModelPtr vec_SecChild2Ptr = ptrIAppFramework->ProjectCache_getAllChildren(itr->getEventId(),/*currSelLanguageID*//*1*/);
				//if(vec_SecChild2Ptr == NULL)
				//{
				//	clsNode.setChildCount(-1);
				//}else
				//{
				//	clsNode.setChildCount(static_cast<int32>(vec_SecChild2Ptr->size()));
				//}

				PMString tempclassificationName("");
				
				tempclassificationName = itr->getName();				

				//--------
				/*if(isCheckCount)
				{
					map<int32, int32>::iterator mapItr;
					mapItr = itemProductSectionCountPtr->find(itr->getPublicationID());
					int32 secChild_Count = mapItr->second;

					tempclassificationName.Append(" (");
					tempclassificationName.AppendNumber(secChild_Count);
					tempclassificationName.Append(")");
					clsNode.setClassName(tempclassificationName);

				}
				else
				{*/
//CA("");
					clsNode.setClassName(tempclassificationName);
				/*}*/

				//clsNode.setClassName(tempclassificationName);
				clsNode.setLevel(ParentClsNode.getLevel() + 1);
				//clsNode.setParentId(ParentClsNode.getParentId());	//---------	
				clsNode.setParentId(parentUID/*.Get()*/);
				clsNode.setSequence(count);
				clsNode.setIsEventType(kTrue);
				clsNode.setIsPUB(kTrue);

				clsCache.add(clsNode);
				count++;
			}
			if(vectChild)
				delete vectChild;
			
		}
	}

	if(retval== 0)
		return 0; //kInvalidUID;
	return (retval);
}

double ClassTreeModel::GetNthRootChild(const int32& index) const
{
	//CA("GetNthRootChild");
	ClassificationNode clsNode;
	double retval=0; //kInvalidUID;

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return retval;
	}
	//ptrIAppFramework->LogDebug("AP7_CategoryBrowser::ClassTreeModel::GetNthRootChild::entering ");

	

	if(clsCache.isExist(root, index, clsNode))
	{
		retval=clsNode.getClassId();
	}
	else
	{
		/*InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			CAlert::ErrorAlert("Interface for IAppFramework not found.");
			return retval;
		}*/
		PMString ASD1("index : ");
		ASD1.AppendNumber(index);
		ASD1.Append("  root: ");
		ASD1.AppendNumber(root);

		ptrIAppFramework->LogDebug("ClassTreeModel::GetNthRootChild " + ASD1);

		/*CClassificationValue classVal;
		classVal.setClass_id(root);*/

		VectorClassInfoPtr vect = ptrIAppFramework->ClassificationTree_getAllChildren(root, /*currSelLanguageID*/ 1); //CLASSMngr_getAllClassesOfParent(classVal);
		if(vect==nil)
		{
			ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetNthRootChild::ClassificationTree_getAllChildren's vect == nil");
			return retval;
		}

		VectorClassInfoValue::iterator it1;

		int count=1;

		for(it1=vect->begin(); it1!=vect->end(); it1++)
		{
			
			clsNode.setClassId(it1->getClass_id());
			clsNode.setChildCount(it1->getChildCount());

			PMString tempclassificationName("");
			tempclassificationName = it1->getClassification_name();			
			ptrIAppFramework->LogDebug("Inside for  Loop " + tempclassificationName );
			clsNode.setClassName(tempclassificationName);
			clsNode.setLevel(it1->getClass_level());
			clsNode.setParentId(root);
			clsNode.setSequence(count);
			
			if(count==(index+1))
				retval=clsNode.getClassId();

			clsNode.setIsEventType(kFalse);
			clsNode.setIsPUB(kFalse);
			clsCache.add(clsNode);
			count++;

		}

		if(vect)
			delete vect;
	}
	if(retval== 0)
		return 0; //kInvalidUID;

	return (retval);
}

int32 ClassTreeModel::GetIndexForRootChild(const double& uid) const
{
	//CA("GetIndexForRootChild");
	int32 retval=-1;
	ClassificationNode clsNode;

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return retval;
	}
	//ptrIAppFramework->LogDebug("AP7_CategoryBrowser::ClassTreeModel::GetIndexForRootChild::entering ");	

	if(clsCache.isExist(uid/*.Get()*/, clsNode))
	{
		retval=clsNode.getSequence();
	}
	else
	{
		/*InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
		{
			CAlert::ErrorAlert("Interface for IAppFramework not found.");
			return retval;
		}*/

		//ptrIAppFramework->LogDebug("ClassTreeModel::GetIndexForRootChild");
		
		/*CClassificationValue classVal;
		classVal.setClass_id(root);*/

		VectorClassInfoPtr vect = ptrIAppFramework->ClassificationTree_getAllChildren(root, /*currSelLanguageID*/ 1); //CLASSMngr_getAllClassesOfParent(classVal);

		if(vect==nil)
		{
			ptrIAppFramework->LogError("AP7_CategoryBrowser::ClassTreeModel::GetIndexForRootChild::ClassificationTree_getAllChildren's vect == nil");
			return retval;
		}

		VectorClassInfoValue::iterator it1;

		int count=1;

		for(it1=vect->begin(); it1!=vect->end(); it1++)
		{
			clsNode.setClassId(it1->getClass_id());
			clsNode.setChildCount(it1->getChildCount());
			
			PMString tempclassificationName("");
			tempclassificationName = it1->getClassification_name();
			

			clsNode.setClassName(tempclassificationName);
			clsNode.setLevel(it1->getClass_level());
			clsNode.setParentId(root);
			clsNode.setSequence(count);
			clsNode.setIsEventType(kFalse);
			clsNode.setIsPUB(kFalse);
			clsCache.add(clsNode);
			if(clsNode.getClassId()==uid/*.Get()*/)
				retval=count;
			count++;
		
			/*				
			PMString msgStr1("Category ID ");
			msgStr1.AppendNumber(it1->getcategory_id());
			msgStr1.Append(" Chd cnt ");
			msgStr1.AppendNumber(it1->getchildcount());
			msgStr1.Append(" Class Name ");
			msgStr1.Append(it1->getdisplay_name());
			msgStr1.Append(" Class Lvl ");
			msgStr1.AppendNumber(it1->getlevel_no());
			msgStr1.Append(" Parent id ");
			msgStr1.AppendNumber(it1->getparent_id());
			CA(msgStr1);*/

		}

		//if(vect)
		//	delete vect;
	}

	return (retval-1);
}

PMString ClassTreeModel::ToString(const double& uid) const
{
	//CA("ToString");
	ClassificationNode clsNode;
	PMString retval("Unknown");

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return retval;
	}
	//ptrIAppFramework->LogDebug("AP7_CategoryBrowser::ClassTreeModel::ToString::entering ");




	if(clsCache.isExist(uid/*.Get()*/, clsNode))
	{
		if(uid/*.Get()*/== root)
		{
			retval=classificationName;
		}
		else
			retval=clsNode.getClassName();
	}
		return retval;
}

int32 ClassTreeModel::GetNodePathLengthFromRoot(const double& uid) 
{
	//CA("GetNodePathLengthFromRoot");
	
	ClassificationNode clsNode;
	int32 retval=0;

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return retval;
	}
	//ptrIAppFramework->LogDebug("AP7_CategoryBrowser::ClassTreeModel::GetNodePathLengthFromRoot::entering ");



	if(clsCache.isExist(uid/*.Get()*/, clsNode))
	{
		retval=clsNode.getLevel();
	}
	else
	{
		return -1;
	}
	return retval-1;
}

bool16 ClassTreeModel::isPubNode(const double& uid)
{
	ClassificationNode clsNode;

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
	{
		CAlert::ErrorAlert("Interface for IAppFramework not found.");
		return kFalse;
	}
	//ptrIAppFramework->LogDebug("AP7_CategoryBrowser::ClassTreeModel::isPubNode::entering ");


	
	if(clsCache.isExist(uid/*.Get()*/, clsNode))
	{
		return clsNode.getIsPUB();
	}
	else
	{
		return kFalse;
	}

}

#include "VCPlugInHeaders.h"
#include "CTBPlugInEntrypoint.h"
#include "CAlert.h"
#include "CTBID.h"
#include "CTBLoginEventsHandler.h"
#include "LNGID.h"
#include "ILoginEvent.h"

#define CA(z) CAlert::InformationAlert(z)

#ifdef WINDOWS
	ITypeLib* CTBPlugInEntrypoint::fCTBTypeLib = nil;
#endif

bool16 CTBPlugInEntrypoint::Load(ISession* theSession)
{
	bool16 retVal=kFalse;
	do
	{
		InterfacePtr<IRegisterLoginEvent> regEvt
		((IRegisterLoginEvent*) ::CreateObject(kLNGLoginEventsHandler,IID_IREGISTERLOGINEVENT));
		if(!regEvt)
		{
			//CA("Invalid regEvt");
			
			break;
		}
		regEvt->registerLoginEvent(kCTBLoginEventsHandler); 
	//	CA("Successful register in PSP");
	}while(kFalse);
	return kTrue;
}

/* Global
*/
static CTBPlugInEntrypoint gPlugIn;

/* GetPlugIn
	The application calls this function when the plug-in is installed 
	or loaded. This function is called by name, so it must be called 
	GetPlugIn, and defined as C linkage. See GetPlugIn.h.
*/
IPlugIn* GetPlugIn()
{
	return &gPlugIn;
}

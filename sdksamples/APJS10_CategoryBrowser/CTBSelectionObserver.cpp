#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "CTBID.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "IAppFramework.h"
#include "IListBoxController.h"
#include "ITextControlData.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
#include "CTBSelectionObserver.h"
#include "IListBoxController.h"
#include "IApplication.h"
#include "ITreeViewMgr.h"
#include "ClassTreeModel.h"
#include "IClientOptions.h"
#include "AcquireModalCursor.h"
#include "IPanelControlData.h"
#include "ClassMediator.h"
#include "CTBActionComponent.h"
#include "ClassCache.h"

//-------
#include "ITreeViewController.h"
#include "CTBTreeNodeID.h"
#include "IPanelMgr.h"
#include "IContentSprayer.h"
//#include "SEAID.h"
//#include "IOneSourceSearch.h"
//#include "ISpecialChar.h"
#include "ITreeViewHierarchyAdapter.h"
#include "ITemplatesDialogCloser.h"
#include "ILoginHelper.h"
#include "IContentSprayer.h"



#ifdef MACINTOSH
	#include "K2Vector.tpp"
#endif


#define CA(z) CAlert::InformationAlert(z)
#define CAI(x) {PMString str;\
			str.AppendNumber(x);\
			CAlert::InformationAlert(str);}\

//following is added by vijay
extern ClassCache clsCache;

double currSelLanguageID = -1;
PMString currSelLangName = "";
extern double currPublicationID;
extern PMString currPublicationName;
extern int32 isContentSprayerFlag ;
extern int32 isCheckCount;
//extern ItemProductCountMapPtr itemProductSectionCountPtr;

CREATE_PMINTERFACE(CTBSelectionObserver, kCTBSelectionObserverImpl)

CTBSelectionObserver::CTBSelectionObserver(IPMUnknown *boss) :
	ActiveSelectionObserver(boss) {}

CTBSelectionObserver::~CTBSelectionObserver() {}

void CTBSelectionObserver::AutoAttach()
{	
//	CA("CTBSelectionObserver::AutoAttach");
	ClassMediator::shouldLoad = kTrue;
	ActiveSelectionObserver::AutoAttach();	
	InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
	if (iPanelControlData==nil)
	{
		//	CA("iPanelControlData is nil");
		return;
	}
	
	AttachWidget(iPanelControlData, kCTBStaticTextWidgetID, IID_ISTRINGLISTCONTROLDATA);
	AttachWidget(iPanelControlData, kCTBRefreshButtonWidgetID, IID_ITRISTATECONTROLDATA);
	ClassMediator::iPanelCntrlDataPtr=iPanelControlData;
	AttachWidget(iPanelControlData, kCTBSelectlanguageDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
	AttachWidget(iPanelControlData, kCTBSaveButtonWidgetID, IID_ITRISTATECONTROLDATA);
	AttachWidget(iPanelControlData, kCTBCloseButtonWidgetID, IID_ITRISTATECONTROLDATA);
	loadPaletteData();	
}

void CTBSelectionObserver::loadPaletteData(void)
{	
	do
	{	
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			return;

		////ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::loadPaletteData");
		//CA("CTBSelectionObserver::loadPaletteData");
		
		//added to clear cache
		if(ClassMediator::shouldLoad == kFalse)
		{
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::ClassMediator::shouldLoad == kFalse");
			break;
		}

		/*clsCache.clearNodeMap();
		clsCache.clearMap();*/
		
		bool16 result=ptrIAppFramework->getLoginStatus();
		if(result != kTrue)
			return;

		//ClassMediator::iPanelCntrlDataPtr=nil;//19/01

		if(ClassMediator::iPanelCntrlDataPtr==nil)
		{		
			//CA("iPanelControlData1111");
			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData==nil)
			{
				ClassMediator::iPanelCntrlDataPtr=ClassMediator::iPanelCntrlDataPtrTemp;//19.01
				break;
				//return;//19.01
			}				
			else//19/01
			{
				/* Storing IPanelControlData in Mediator */
				//CA("iPanelControlData");
				ClassMediator::iPanelCntrlDataPtr=iPanelControlData;//
				ClassMediator::iPanelCntrlDataPtrTemp=iPanelControlData;
			}
			
		}

		CTBActionComponent actionObsever(this);
		actionObsever.DoPallete();
		
		IControlView* TreeCtrlView = ClassMediator::iPanelCntrlDataPtr->FindWidget(kCTBClassTreeViewWidgetID);
		if(TreeCtrlView==nil)
		{
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::loadPaletteData::TreeCtrlView == nil");
			break;
		}
		
		ClassMediator::ClassTreeCntrlView=TreeCtrlView;

		IControlView* RefreshRollOverIconView = ClassMediator::iPanelCntrlDataPtr->FindWidget(kCTBRefreshButtonWidgetID);
		if(RefreshRollOverIconView==nil) 
		{
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::loadPaletteData::RefreshRollOverIconView == nil");
			break;
		}

		IControlView* SaveRollOverIconView = ClassMediator::iPanelCntrlDataPtr->FindWidget(kCTBSaveButtonWidgetID);
		if(SaveRollOverIconView==nil) 
		{
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::loadPaletteData::SaveRollOverIconView == nil");
			break;
		}
		IControlView* CloseRollOverIconView = ClassMediator::iPanelCntrlDataPtr->FindWidget(kCTBCloseButtonWidgetID);
		if(CloseRollOverIconView==nil) 
		{
			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::loadPaletteData::CloseRollOverIconView == nil");
			break;
		}
		if(!result)  
		{	
			ptrIAppFramework->LogError("Invalid selection.  Please log into PRINTsource before continuing.");

			TreeCtrlView->Disable();
			RefreshRollOverIconView->Disable();
			SaveRollOverIconView->Disable();
			CloseRollOverIconView->Disable();
			break;
		}			
		else
		{	
			//ptrIAppFramework->LogDebug("Else part");

			//isContentSprayerFlag = 2;

			TreeCtrlView->Enable();
			RefreshRollOverIconView->Enable();
			SaveRollOverIconView->Enable();
			CloseRollOverIconView->Enable();
		}
		
		if(result)
		{
			//ptrIAppFramework->LogError("***** CTBSelectionObserver::loadPaletteData::before loading catagory browsers tree");
			InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
			if(cop==nil){
				ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::loadPaletteData::cop == nil");
				break;
			}
	
			PMString LocaleName(kCTBNoTextStringKey);
			LocaleName.SetTranslatable(kFalse);
			double PubID = -1;
			PMString pubName("");
			pubName.SetTranslatable(kFalse);
			ClassMediator::languageID = -1;

			ClassMediator::languageID = ptrIAppFramework->getLocaleId();
			if(ClassMediator::languageID != -1){						
				ClassMediator::languageID = cop->getDefaultLocale(LocaleName);
				//ClassMediator::languageID = 1;
			}
			currSelLanguageID = ClassMediator::languageID ;	//--------
			//int32 PM_Product_Level =  ptrIAppFramework->getPM_Project_Levels();

			//if(Flag1 == 1 ) 
			//{															
				TreeCtrlView->ShowView(kTrue);
				
			//	Flag1 =0;

			/*	InterfacePtr<ITreeViewMgr> treeViewMgr(ClassMediator::ClassTreeCntrlView, UseDefaultIID());
				if(!treeViewMgr) 
				{
					ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::loadPaletteData::treeViewMgr == nil");
					return;
				}

			ptrIAppFramework->LogDebug("Loading tree in loadpallete data");

			ClassTreeModel pModel;
			PMString pfName("Master");
			pModel.setRoot(-2, pfName);
			treeViewMgr->ClearTree(kTrue);
		    pModel.GetRootUID();
			treeViewMgr->ChangeRoot(); */

			//ptrIAppFramework->LogError("***** CTBSelectionObserver::loadPaletteData::after loading catagory browsers tree");

			IControlView *	selectlanguageDropDownView = ClassMediator::iPanelCntrlDataPtr->FindWidget(kCTBSelectlanguageDropDownWidgetID);
			if(!selectlanguageDropDownView)
			{
				ptrIAppFramework->LogError("selectlanguageDropDownView is nil");
				break;
			}

			InterfacePtr<IStringListControlData> selectlanguageDropListData(selectlanguageDropDownView,UseDefaultIID());	
			if (selectlanguageDropListData==nil)
			{
				ptrIAppFramework->LogError("selectlanguageDropListData is nil");
				break;
			}

			selectlanguageDropListData->Clear(kFalse,kFalse);

			InterfacePtr<IDropDownListController> selectlanguageDropListController(selectlanguageDropDownView, UseDefaultIID());
			if (selectlanguageDropListController==nil)
			{
              ptrIAppFramework->LogError("selectlanguageDropListController is nil");
				break;
			}
			
			VectorLanguageModelPtr VectorLangNamePtr = ptrIAppFramework->StructureCache_getAllLanguages();
			if(VectorLangNamePtr == NULL)
			{
				ptrIAppFramework->LogError("VectorLangNamePtr == NULL");
				break;
			}
			
			VectorLanguageModel::iterator itr;
			int j = 0;
			int32 ind_count = 0;
			for(itr = VectorLangNamePtr->begin(); itr != VectorLangNamePtr->end(); itr++)
			{		
				//CA("itr->getLangugeName()  :  "+itr->getLangugeName());
				PMString localename(kCTBBlankSpaceStringKey);
				localename = itr->getLangugeName();
				//WideString localName(kCTBBlankSpaceStringKey); 
				//localName = (WideString)localename;
				localename.SetTranslatable(kFalse);
                localename.ParseForEmbeddedCharacters();
				selectlanguageDropListData->AddString(localename, j, kFalse, kFalse);
				
				if(LocaleName == "" )
				{
					if(j == 0)
					{
						currSelLanguageID = itr->getLanguageID();
						currSelLangName  =  localename;
						ind_count = 0;
					}
				}
				if(localename == LocaleName)
				{
					currSelLanguageID = itr->getLanguageID();
					currSelLangName  =  localename;
					ind_count = j;
				}
				j++;
				
			}
			selectlanguageDropListController->Select(ind_count/*0*/);
		
			IControlView* textView = ClassMediator::iPanelCntrlDataPtr->FindWidget( kCTBMultilineTextWidgetID);
			InterfacePtr<ITextControlData> textPtr( textView ,IID_ITEXTCONTROLDATA);
			
			if(PubID == -1)
			{

				PMString str("Please Select Langauge and Event/Category To Continue.");
				str.SetTranslatable(kFalse);
				textPtr->SetString(str);
				//PMString s("String : ");
				//s.Append(str);
				//CA(s);
			}
			/*if(VectorLangNamePtr)
			{
				delete VectorLangNamePtr;
				VectorLangNamePtr = NULL;
			}*/
						
		}
		else
		{				
			CTBActionComponent actionObsever(this);
			actionObsever.ClosePallete();
		}

	} while (kFalse);

}

void CTBSelectionObserver::AutoDetach()
{
//	CA("CTBSelectionObserver::AutoDetach");
	ClassMediator::shouldLoad = kFalse;
	ActiveSelectionObserver::AutoDetach();		
	do
	{

		DetachWidget(ClassMediator::iPanelCntrlDataPtr, kCTBStaticTextWidgetID, IID_ISTRINGLISTCONTROLDATA);
		DetachWidget(ClassMediator::iPanelCntrlDataPtr, kCTBRefreshButtonWidgetID, IID_ITRISTATECONTROLDATA);
	//	DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kTPLClassTreeIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);
	//	DetachWidget(TPLMediatorClass::iPanelCntrlDataPtr, kTPLRefreshIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(ClassMediator::iPanelCntrlDataPtr, kCTBSelectlanguageDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		DetachWidget(ClassMediator::iPanelCntrlDataPtr, kCTBSaveButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(ClassMediator::iPanelCntrlDataPtr, kCTBCloseButtonWidgetID, IID_ITRISTATECONTROLDATA);

	} while (kFalse);
	
}

//Here Subject can pass itself as a parameter so that observer knows which subject to examine
void CTBSelectionObserver::Update
(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{	
	//CA("CTBSelectionObserver::Update");	
	ActiveSelectionObserver::Update(theChange, theSubject, protocol, changedBy);
	do
	{	
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
			return;	

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			return;

		// added by avinash
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){	
			
			//CAlert::InformationAlert("AutoAttach panelControlData nil");
			break;
		}

		// upto here

		WidgetID theSelectedWidget = controlView->GetWidgetID();
		/*if(theSelectedWidget==kTPLDropDownWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			HandleDropDownListClick(theChange,theSubject,protocol,changedBy);
			break;
		}*/
///////////////////////////////////////////////////////////////////
//following code is added by vijay on 9-12-2006===========from here
		if(theSelectedWidget==kCTBRefreshButtonWidgetID &&  theChange == kTrueStateMessage)
		{

			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kCTBSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here

			//CA("kCTBRefreshButtonWidgetID");
			AcquireWaitCursor awc ;
			awc.Animate(); 

			//ptrIAppFramework->LogError("***** AP7_CategoryBrowser::CCategoryBrowser:before refreshing tree");
			

			bool16 result=ptrIAppFramework->getLoginStatus();
			if(!result)
				return;		

			ptrIAppFramework->StructureCache_clearInstance();
			ptrIAppFramework->EventCache_clearInstance();

			InterfacePtr<ITreeViewMgr> treeViewMgr(ClassMediator::ClassTreeCntrlView, UseDefaultIID());
			if(!treeViewMgr) 
				return;

			InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
			if(ptrLogInHelper == nil)
			{
				//CA("ptrLogInHelper == nil");
				ptrIAppFramework->LogDebug("AP7_CategoryBrowser::CCategoryBrowser::OpenCategoryBrowser::ptrLogInHelper == nil");
				return ;
			}
			LoginInfoValue cserverInfoValue;
			bool16 res = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
			ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
			isCheckCount = clientInfoObj.getShowObjectCountFlag();

			
			//clsCache.clearNodeMap();
			clsCache.clearMap();
			ClassTreeModel pModel;
			PMString pfName("Master");
			pModel.setRoot(-2, pfName);
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();

			//ptrIAppFramework->LogError("***** AP7_CategoryBrowser::CCategoryBrowser:after refreshing tree");


		}
///////////////////////////////////////////////===========upto here	

		if(theSelectedWidget == kCTBSelectlanguageDropDownWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{

			//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kCTBSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here

			//CA("Language List ");
			IControlView *	selectlanguageDropDownView = ClassMediator::iPanelCntrlDataPtr->FindWidget(kCTBSelectlanguageDropDownWidgetID);
			if(!selectlanguageDropDownView)
			{
				//CA("selectlanguageDropDownView is nil");
				break;
			}

			InterfacePtr<IStringListControlData> selectlanguageDropListData(selectlanguageDropDownView,UseDefaultIID());	
			if (selectlanguageDropListData==nil)
			{
				//CA("selectlanguageDropListData is nil");
				break;
			}

			InterfacePtr<IDropDownListController> selectlanguageDropListController(selectlanguageDropDownView, UseDefaultIID());
			if (selectlanguageDropListController==nil)
			{
			   // CA("selectlanguageDropListController is nil");
				break;
			}
			int32 index = selectlanguageDropListController->GetSelected();
			PMString currSelectedlangName = selectlanguageDropListData->GetString(index);

			VectorLanguageModelPtr VectorLangNamePtr = ptrIAppFramework->StructureCache_getAllLanguages();
			if(VectorLangNamePtr == NULL)
			{
				//CA("VectorLangNamePtr == NULL");
				break;
			}
			
			VectorLanguageModel::iterator itr;
			for(itr = VectorLangNamePtr->begin(); itr != VectorLangNamePtr->end(); itr++)
			{		
				PMString langName("");
				langName = itr->getLangugeName();
				if(langName == currSelectedlangName)
				{
					//CA("langName == currSelectedlangName");
					currSelLanguageID = itr->getLanguageID();
					currSelLangName  =  currSelectedlangName;					
				}
			}
			/*PMString ASD("currSelLangName11 : " + currSelLangName + "  currSelLanguageID11 : ");
				ASD.AppendNumber(currSelLanguageID);
				CA(ASD);*/

			InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
			if(cop == nil)
			{
				//CA("cop == nil");
				ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::Update::cop == nil");
				return;
			}
			cop->setLocale(currSelLangName , currSelLanguageID );

			ptrIAppFramework->setLocaleId(currSelLanguageID);

			/*if(VectorLangNamePtr)
				delete VectorLangNamePtr;*/

			InterfacePtr<ITreeViewMgr> treeViewMgr(ClassMediator::ClassTreeCntrlView, UseDefaultIID());
			if(!treeViewMgr) 
				return;
			ptrIAppFramework->LogDebug("Observer Language change");	
			//clsCache.clearNodeMap();
			clsCache.clearMap();
			ClassTreeModel pModel;
			PMString pfName("Master");
			pModel.setRoot(-2, pfName);
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();


		}
		
		
		if(theSelectedWidget==kCTBSaveButtonWidgetID &&  theChange == kTrueStateMessage)
		{
			//CA("Save Button");
			bool16 functionResult;
			bool16 isONESource = kFalse ;

			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				CA("Returning because of ptrIAppFramework == nil");
				return ;
			}

			InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
			if(cop == nil)
			{
				//CA("cop == nil");
				ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::Update::cop == nil");

				return;
			}

			InterfacePtr<ITreeViewController> treeViewCntrl(ClassMediator::ClassTreeCntrlView, UseDefaultIID());
			if(treeViewCntrl==nil) 
			{
				//CA("treeViewCntrl==nil");
				ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::Update::treeViewCntrl == nil");			
				return ;
			}

			NodeIDList selectedItem;
			
			treeViewCntrl->GetSelectedItems(selectedItem);
			if(selectedItem.size()<=0)
			{
				//CA("selectedItem.size()<=0");
				ptrIAppFramework->LogInfo("AP7_CategoryBrowser::CTBSelectionObserver::Update::selectedItem.size()<=0");			
				return ;
			}

			NodeID nid=selectedItem[0];
			TreeNodePtr<CTBTreeNodeID>  uidNodeIDTemp(nid);

			double uid= uidNodeIDTemp->GetNodeId()/*.Get()*/;

			ClassificationNode clasNode;
			ClassCache clsCache;

			bool16 retval = clsCache.isExist(uid, clasNode);
			if(retval == kTrue)
			{
				//CA("retval == kTrue");	
				ClassMediator::clasId = clasNode.getClassId();
				ClassMediator::clasName = clasNode.getClassName();

			}
			
			if(isContentSprayerFlag == 1 || isContentSprayerFlag == 3)
			{
				//CA("isContentSprayerFlag == 1 || isContentSprayerFlag == 3");
				InterfacePtr<IContentSprayer> ContentSprayerPtr((IContentSprayer*)::CreateObject(kSPContentSprayerBoss, IID_ICONTENTSPRAYER));
				if(!ContentSprayerPtr)
				{
					ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::Pointre to ContentSprayerPtr not found");
					return ;
				}
				InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
				if(cop == nil)
				{
					//CA("cop == nil");
					ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::Update::cop == nil");
					return ;
				}
		
				PMString name("");
				double ID = ClassMediator::clasId;
				name = ClassMediator::clasName;
				double sectionID = -1;
				PMString sectionName = "";

				currPublicationName = name;
				currPublicationID = ID;

				if(clasNode.getIsPUB() == kTrue)	
				{
					//CA("clasNode.getIsPUB() == kTrue");
					isONESource = kFalse ;
					ptrIAppFramework->set_isONEsourceMode(kFalse);

					if(clasNode.getLevel() > 3)	//-----
					{
						 sectionID = ClassMediator::clasId;
						 sectionName = clasNode.getClassName();
						
						CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(ClassMediator::clasId,currSelLanguageID);

						currPublicationID = pubModelRootValue.getRootID();
						currPublicationName = pubModelRootValue.getName();

					}
				}
				else
				{
					isONESource = kTrue ;
					ptrIAppFramework->set_isONEsourceMode(kTrue);
						
					ID = clasNode.getClassId();
					name = ClassMediator::clasName;

					currPublicationName = name;
					currPublicationID = ID;

				}
				cop->setPublication(currPublicationName , currPublicationID);
				cop->setLocale(currSelLangName , currSelLanguageID );
				cop->setSection(sectionName,sectionID );
				
				/*InterfacePtr<IContentSprayer> ContentSprayerPtr((IContentSprayer*)::CreateObject(kSPContentSprayerBoss, IID_ICONTENTSPRAYER));
				if(!ContentSprayerPtr)
				{
					ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::Pointre to ContentSprayerPtr not found");
					return ;
				}*/
				if(ContentSprayerPtr != nil)
				{
					
					functionResult = ContentSprayerPtr->setClassNameAndClassID(ID, name ,isONESource,NULL /*,isCheckCount ,itemProductSectionCountPtr*/);
					if(functionResult == kFalse)
					{
						InterfacePtr<IPanelControlData> panelControlData(QueryPanelControlData());
						if (panelControlData==nil)
						{
							ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::Pointer to panelControlData not found");
							return;
							
						}
						IControlView* iControlView = panelControlData->FindWidget(kCTBSettingsSavedStaticTextWidgetID);
						if(iControlView == nil){
							ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::Pointer to iControlView not found");
							return;
						}	

						InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
						if (textControlData == nil){
							ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::Point to textControlData not found");					
							return;
						}

						PMString setting("Your settings are saved successfully!");
						setting.SetTranslatable(kFalse);
						textControlData->SetString(setting);

						//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::Update::functionResult == kFalse");			
						return ;
					}
				}
			//	if(isContentSprayerFlag == 3)/*else*/
			//	{
			//		//CA("else");
			//		InterfacePtr<IOneSourceSearch> ContentSprayerPtr((IOneSourceSearch*)::CreateObject(kSEAOneSourceSearchBoss, IID_IONESOURCESEARCH));
			//		if(!ContentSprayerPtr)
			//		{
			//			//CA("if(!ContentSprayerPtr)");
			//			ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::Pointre to ContentSprayerPtr not found");
			//			return ;
			//		}
			//		int32 ID = ClassMediator::clasId;
			//	
			//		if(ContentSprayerPtr != nil)
			//		{
			//			functionResult = ContentSprayerPtr->setClassNameAndClassID(ID, name);
			//			if(functionResult == kFalse)
			//			{
			//				ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::functionResult == kFalse");			
			//				return ;
			//			}
			//			//CA("2----GETTING OUT setsetClassNameAndClassID");
			//		}
			//	}
			}

				//CA("Settings saved successfully!");
				//added by avinash

				InterfacePtr<IPanelControlData> panelControlData(QueryPanelControlData());
				if (panelControlData==nil)
				{
					ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::Pointer to panelControlData not found");
					return;
					
				}
				IControlView* iControlView = panelControlData->FindWidget(kCTBSettingsSavedStaticTextWidgetID);
				if(iControlView == nil){
					ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::Pointer to iControlView not found");
					return;
				}	

				InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
				if (textControlData == nil){
					ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBClassTreeObserver::Update::Point to textControlData not found");					
					return;
				}
				PMString setting("Your settings are saved successfully!");
				setting.SetTranslatable(kFalse);
				textControlData->SetString(setting);			
				// upto here
				//}
		}
		else if(theSelectedWidget== kCTBCloseButtonWidgetID &&  theChange == kTrueStateMessage)
		{

						//added by avinash
			IControlView* iControlView = panelControlData->FindWidget(kCTBSettingsSavedStaticTextWidgetID);
			if(iControlView == nil){
				//CAlert::InformationAlert("Fail to find publication static text widget");
				return;
			}	

			InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
			if (textControlData == nil){
				//CAlert::InformationAlert("ITextControlData nil");	
				return;
			}

			textControlData->SetString("");			
			// upto here

			CTBActionComponent actionComponetObj(this);
			actionComponetObj.ClosePallete(); 
		}
	} while (kFalse);
}

void CTBSelectionObserver::UpdatePanel()
{	
	do
	{
		/*InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData == nil)
			break;

		IControlView* iControlView = iPanelControlData->FindWidget(kTPLDropDownWidgetID);
		if (iControlView == nil)
			break;

		InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
		if (iDropDownListController == nil)
			break;
		InterfacePtr<IStringListControlData> iStringListControlData(iDropDownListController, UseDefaultIID());
		if (iStringListControlData == nil)
			break;*/

	}while(kFalse);
}

IPanelControlData* CTBSelectionObserver::QueryPanelControlData()
{
	IPanelControlData* iPanel = nil;
	do
	{
		InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID());
		if (iWidgetParent == nil)
			break;

		InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
		if (iPanelControlData == nil)
			break;
		iPanelControlData->AddRef();
		iPanel = iPanelControlData;
	}
	while (false); 
	return iPanel;
}

void CTBSelectionObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); 
}

void CTBSelectionObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}

#ifndef __ClassMediator_h__
#define __ClassMediator_h__

#include "VCPluginHeaders.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "NodeID.h"

class ClassMediator
{
public:
	static double clasId;
	static PMString clasName;
	static PMString hier;
	static int32 whichMode;
	static double languageID;
	static IControlView* ClassTreeCntrlView;
	static IPanelControlData* iPanelCntrlDataPtr;
	static IPanelControlData* iPanelCntrlDataPtrTemp;
	static IControlView* PrFamilyDropDownView;
	static IControlView* PrGroupDropDownView;
	static IControlView* ClassPathStringView;
	static PMString pathToSend ;
	static bool16 shouldLoad;

};
#endif
#include "VCPlugInHeaders.h"
#include "CommonFunctions.h"


#include "IBookManager.h"
#include "Utils.h"
#include "IBookUtils.h"
#include "IBook.h"
#include "IBookContentMgr.h"
#include "IDataLink.h"
#include "FileUtils.h"
#include "SDKLayoutHelper.h"
#include "ILayoutUIUtils.h" //Cs4
#include "IDocument.h"
#include "ISpreadList.h"
#include "ISpread.h"
#include "IHierarchy.h"
#include "IPageItemTypeUtils.h"
#include "IIDXMLElement.h"
#include "IProgressBarManager.h"
#include "ProgressBar.h"
#include "IGraphicFrameData.h"
#include "ITextFrameColumn.h"
#include "ITableModelList.h"
#include "ITableModel.h"
#include "IXMLReferenceData.h"
#include "TextIterator.h"
#include "MetaDataTypes.h"
#include "CTUnicodeTranslator.h"
#include "ITableTextContent.h"
#include "ITableCommands.h"
#include "Utils.h"
#include "IXMLUtils.h"


#include "ITextModelCmds.h"
#include "IAppFramework.h"
#include "ITagReader.h"
#include "TagStruct.h"
#include "CMediatorClass.h"
#include "ISpecialChar.h"
#include "ITableUtility.h"
#include "IXMLAttributeCommands.h"
#include "AcquireModalCursor.h"
#include "IOpenLayoutCmdData.h"




#include "CAlert.h"
#define CA(X) CAlert::InformationAlert(X);



vector<IDFile> BookFileList;///IDFile of all documents present inside book.
vector<double> attributeIDsList;
vector<double> elementIDsList;
vector<double> eventAttributeIDsList;

set<double> itemListIds;
set<double> itemGroupListIds;

extern K2Vector<bool16>  isSelected;

bool16 isListTableNameForProductPresent = kFalse;
bool16 isListTableNameForItemPresent = kFalse;

int32 progressBarMaxCount = 0;
static int32 TotalTagsInDocument ; 
static int32 TotalUnusedTagsRemoved;


//Method Purpose: remove the Special character from the Name. 
PMString keepOnlyAlphaNumeric(PMString name)
{
	PMString tagName("");

	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		bool isAlphaNumeric = false ;

		PlatformChar ch = name.GetChar(i);

		if(ch.IsAlpha() || ch.IsNumber())
			isAlphaNumeric = true ;

		if(ch.IsSpace())
		{
			isAlphaNumeric = true ;
			ch.Set('_') ;
		}

		if(ch == '_')
			isAlphaNumeric = true ;
	
		if(isAlphaNumeric) 
			tagName.Append(ch);
	}

	return tagName ;
}

PMString prepareTagName(PMString name)
{
	PMString tagName("");

	PlatformChar ch = name.GetChar(0);

	if(ch.IsNumber())
	{
		PMString temp = name;
		name.Clear();
		name = "A";
		name.Append(temp);
		temp.Clear();
	}

	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		if(name.GetChar(i) == '[' || name.GetChar(i) == ']' || name.GetChar(i) == '\n')
			continue;

		if(name.GetChar(i) ==' ' || name.GetChar(i) == '\\' || name.GetChar(i) == '/'  )
		{
			tagName.Append("_");
		}
		else tagName.Append(name.GetChar(i));
	}
	PMString FinalTagName("");
	FinalTagName = keepOnlyAlphaNumeric(tagName); // removes the Special characters from the Name.

	return FinalTagName;
}


void CommonFunctions::scanBook()
{
	//CA("CommonFunctions::scanBook");

	InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID());
	if (bookManager == nil) 
	{ 
		//CA("There is not book manager!");
		return; 
	}

	if (bookManager->GetBookCount() <= 0)
	{
		//CA("There is no book open. You must first open a book before running this snippet.");
		return;
	}

	int32 bookCount = bookManager->GetBookCount();

	PMString  ActiveBookName;
	//InterfacePtr<IFooUtils> fooUtils(Utils<IFooUtils>());
	bool16 result = Utils<IBookUtils>()->GetActiveBookName(ActiveBookName);


	//CA("ActiveBookName   =  "+ActiveBookName);
	BookFileList.clear();

	for (int32 i = 0 ; i < bookCount ; i++) 
	{
		IBook* book = bookManager->GetNthBook(i); //og
		//InterfacePtr<IBook> book(bookManager->GetNthBook(i)); // added by avinash
		if (book == nil) 
		{	//go onto the next book...
			continue;
		}				
		PMString BookTitalName = book->GetBookTitleName();
		//CA("BookTitalName   =  "+BookTitalName);
		if(BookTitalName == ActiveBookName)
		{	
			
			InterfacePtr<IBookContentMgr> bookContentMgr(book, UseDefaultIID());
			if (bookContentMgr == nil) 
			{
				//CA("This book doesn't have a book content manager!  Something is wrong.");
				return;
			}
			
			BookContentDocInfoVector* BookContentinfovector = this->GetBookContentDocInfo(bookContentMgr);
			if(BookContentinfovector== nil)
				return;

			if(BookContentinfovector->size()<=0)
			{
				//CA(" BookContentinfovector->size()<=0 ");
				return;
			}

			BookContentDocInfoVector::iterator it1;
			for(it1 = BookContentinfovector->begin(); it1 != BookContentinfovector->end(); it1++)
			{				
				IDFile fileName = it1->DocFile ;
				BookFileList.push_back(fileName);/*	*/			
			}

			if(BookContentinfovector)	//------
			{
				BookContentinfovector->clear();
				delete BookContentinfovector;
			}

		}
	}

	PMString strBookFileList = "";
	strBookFileList.AppendNumber(static_cast<int32>(BookFileList.size()));
	//CA("strBookFileList = " + strBookFileList);

	//CA("COMMENTING SCAN EACH DOCUMENT");
	scanEachDocument();


}

BookContentDocInfoVector* CommonFunctions::GetBookContentDocInfo(IBookContentMgr* bookContentMgr)
{
	bool16 flag=kFalse;
	BookContentDocInfoVector* BookContentDocInfovectorPtr = new BookContentDocInfoVector;
	BookContentDocInfovectorPtr->clear();
	do {
		if (bookContentMgr == nil) 
		{
			ASSERT(bookContentMgr);
			break;
		}

		// get the book's database (same for IBook)
		IDataBase* bookDB = ::GetDataBase(bookContentMgr);
		if (bookDB == nil) 
		{
			ASSERT_FAIL("bookDB is nil - wrong database?"); 
			break;
		}

		int32 contentCount = bookContentMgr->GetContentCount();
		for (int32 i = 0 ; i < contentCount ; i++) 
		{
			BookContentDocInfo contentDocInfo;
			UID contentUID = bookContentMgr->GetNthContent(i);
			if (contentUID == kInvalidUID) 
			{
				// somehow, we got a bad UID
				continue; // just goto the next one
			}
			// get the datalink that points to the book content
			InterfacePtr<IDataLink> bookLink(bookDB, contentUID, UseDefaultIID());
			if (bookLink == nil) 
			{
				ASSERT_FAIL(FORMAT_ARGS("IDataLink for book #%d is missing", i));
				break; // out of for loop
			}

			// get the book name and add it to the list
			PMString* baseName = bookLink->GetBaseName();
			//ASSERT(baseName && baseName->IsNull() == kFalse);
			
			IDFile CurrFile;
			bool16 IsMissingPluginFlag = kFalse;

			//IDocument* CurrDoc = Utils<IBookUtils>()->FindDocFromContentUID //og
			//	(	//og
			//		bookDB,		//og
			//		contentUID,	//og
			//		CurrFile,	//og
			//		IsMissingPluginFlag	//og
			//	);	//og

			//added by avinash
			InterfacePtr<IDocument> CurrDoc(Utils<IBookUtils>()->FindDocFromContentUID
				(
					bookDB,
					contentUID,
					CurrFile,
					IsMissingPluginFlag
				));
			//till here

			bool16 Flag1 = kFalse;
			Flag1 =  FileUtils::DoesFileExist(CurrFile);
			if(Flag1 == kFalse)
			{
				//CA("File dOES nOT Exists");
				continue;
			}
			else
			{				
				contentDocInfo.DocFile = CurrFile;	
				contentDocInfo.DocumentName = (*baseName);
				contentDocInfo.DocUID = contentUID;
				contentDocInfo.index = i;
				//contentDocInfo.documentPtr = CurrDoc;				
				BookContentDocInfovectorPtr->push_back(contentDocInfo);
				flag=kTrue;
			}			
		}

	} while (false);

	if(flag)	//------
		return BookContentDocInfovectorPtr;
	else
	{
		delete BookContentDocInfovectorPtr;
		return NULL;
	}

}


void CommonFunctions::scanEachDocument()
{
	//CA("CommonFunctions::scanEachDocument");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	InterfacePtr<ITagReader> tReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!tReader)
	{
		ptrIAppFramework->LogDebug("AP46_SquareInch::CDialogObserver::Update::itagReader==nil");
		return ;
	}

	attributeIDsList.clear();
	elementIDsList.clear();
	eventAttributeIDsList.clear();
	itemGroupListIds.clear();
	itemListIds.clear();

	for(int32 p=0; p< BookFileList.size(); p++) ////iterate through all documents of that book.
	{
		SDKLayoutHelper sdklhelp;

		UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[p]);
		ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
		
		IDocument* fntDoc =Utils<ILayoutUIUtils>()->GetFrontDocument(); //og
		//InterfacePtr<IDocument> fntDoc(Utils<ILayoutUIUtils>()->GetFrontDocument()); //added by avinash
		if(fntDoc==nil)
		{
			ptrIAppFramework->LogDebug("AP46_SquareInch::CDialogObserver::Update::ptr to IDocument==nil");
			return;
		}			
		
		InterfacePtr<ILayoutControlData> layout(/*::*/Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layout == nil)
		{
			ptrIAppFramework->LogDebug("AP46_SquareInch::CDialogObserver::Update::ptr to ILayoutControlData==nil");
			return;
		}
		
		IDataBase* database = ::GetDataBase(fntDoc);
		if(database==nil)
		{
			ptrIAppFramework->LogDebug("AP46_SquareInch::CDialogObserver::Update::ptr to IDataBase==nil");	
			return;
		}			
		
		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
		if (iSpreadList==nil)
		{
			ptrIAppFramework->LogDebug("AP46_SquareInch::CDialogObserver::Update::iSpreadList==nil");	
			return;
		}		       		

		UIDList allPageItems(database);

		PMString spreadCount("SpreadCount::");
		spreadCount.AppendNumber(iSpreadList->GetSpreadCount());
		//CA(spreadCount);

		for(int numSp=0; numSp < iSpreadList->GetSpreadCount(); numSp++)
		{	
			UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
			
			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
			if(!spread)
			{
				ptrIAppFramework->LogDebug("AP7_SquareInch::CDialogObserver::Update::There is no spread at all");	
				return;
			}				
			
			int numPages=spread->GetNumPages();
						
			for(int pageIndex=0;pageIndex<numPages; pageIndex++)	
			{
				UIDList tempList(database);
				UIDRef pageUIDRef(database, spread->GetNthPageUID(pageIndex));    // Per Page UID Ref
						
				spread->GetItemsOnPage(pageIndex, &tempList, kFalse); 

				progressBarMaxCount+= tempList.Length(); //added by avinash

				for(int frameIndex=0; frameIndex < tempList.Length(); frameIndex++)    // containing perPage Item List
				{	
					
					InterfacePtr<IHierarchy> iHier(tempList.GetRef(frameIndex), UseDefaultIID());
					if(!iHier)
						continue;

					UID kidUID;
					int32 numKids=iHier->GetChildCount();
			
					bool16 isGroupFrame = kFalse ;
					isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(tempList.GetRef(frameIndex));

					if(isGroupFrame == kTrue) 
					{
						//CA("isGroupFrame == kTrue");
						for(int j=0;j<numKids;j++)                     
						{	
							IIDXMLElement* ptr;
							kidUID=iHier->GetChildUID(j);
							UIDRef boxRef(tempList.GetDataBase(), kidUID);

							bool16 isGroupFrameInsideGroupFrame = kFalse ;
							isGroupFrameInsideGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(boxRef);
							
							if(isGroupFrameInsideGroupFrame)
							{
								//CA("isGroupFrameInsideGroupFrame");
								for(int j=0;j<numKids;j++)                     
								{	
									InterfacePtr<IHierarchy> iHier(boxRef, UseDefaultIID());
									if(!iHier){
										//CA("11111111111");
										continue;
									}
									IIDXMLElement* ptr;
									kidUID=iHier->GetChildUID(j);
									UIDRef boxRef(tempList.GetDataBase(), kidUID);

									TagList tList1;							
									
									tList1 = tReader->getTagsFromBox(boxRef, &ptr);
									if(tList1.size() == 0)
									{
										continue;
									}
													
									if(tList1.size() <= 0)   // check tag list whether it has somthing
									{
										continue;
									}
									

									if(tList1.size() > 0)
									{
										for(int32 tagIndex = 0; tagIndex < tList1.size(); tagIndex++)
										{
											TagStruct & tagInfo = tList1[tagIndex];
											
											if(tagInfo.isTablePresent)
											{
												//CA("Table Present");
												if(tagInfo.elementId == -115)
												{
													//objCSprayStencilInfo.isHyTable = kTrue;
													//objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
												}
												else
												{
													//objCSprayStencilInfo.isDBTable = kTrue;
													//objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);

													if(tagInfo.whichTab == 4 && tagInfo.elementId == -102)
													{
														TagList tList2;							
									
														tList2 = tReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
									
														/*PMString size("tList2.size = ");
														size.AppendNumber(tList2.size());
														CA(size);*/

														//IIDXMLElement * itemsTagXMLElementPtr = tagInfo.tagPtr;
														//if(itemsTagXMLElementPtr == nil)
														//{					
														//		//CA("itemsTagXMLElementPtr == nil");
														//		continue;
														//}	
														//int32 childTagCount = itemsTagXMLElementPtr->GetChildCount();
														//for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
														//{
														//	XMLReference childTagRef = itemsTagXMLElementPtr->GetNthChild(childTagIndex);
														//	//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
														//	InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
														//	if(childTagXMLElementPtr == nil)
														//		continue;
														//	
														//	PMString strID = childTagXMLElementPtr->GetAttributeValue("ID");
														//	int32 ID = strID.GetAsNumber();


														//	bool16 isPresent = kFalse;
														//	for(int32 i = 0;i < attributeIDsList.size(); i++)
														//	{
														//		if(ID == attributeIDsList[i])
														//		{
														//			isPresent = kTrue;
														//			break;
														//		}
														//	}
														//	if(!isPresent)
														//		attributeIDsList.push_back(tagInfo.elementId);
														//}



														// added by avinash
														for(int32 tagIndex = 0 ; tagIndex < tList2.size() ; tagIndex++)
														{
															tList2[tagIndex].tagPtr->Release();
														}
														//till here
													}																							

												}
												//CA("22222222222222222");
												continue;
											}

											if(tagInfo.imgFlag == 1)
											{
												//CA("image Present");
												//objCSprayStencilInfo.isAsset = kTrue;
												//objCSprayStencilInfo.AssetIds.push_back(tagInfo.typeId);
												continue;
											}
											
											//CA("copy Attribute");
											//objCSprayStencilInfo.isCopy = kTrue;


											if(tagInfo.whichTab == 3)
											{
												//CA("tagInfo.whichTab == 3");
												if( tagInfo.elementId == -121 || tagInfo.elementId == -983 || tagInfo.elementId == -976
														|| tagInfo.elementId == -977 || tagInfo.elementId == -978 || tagInfo.elementId == -979 || tagInfo.elementId == -980)
												{
													itemGroupListIds.insert(tagInfo.tableId);
												}

												bool16 isPresent = kFalse;
												for(int32 i = 0;i < elementIDsList.size(); i++)
												{
													
													if(tagInfo.elementId == elementIDsList[i])
													{
														isPresent = kTrue;
														break;
													}
												}
												if(!isPresent)
												{
													elementIDsList.push_back(tagInfo.elementId);
													CMediatorClass::oldAttributeLanguageID = tagInfo.languageID;
												}
											}

											if(tagInfo.whichTab == 4 && tagInfo.elementId == -101)
											{

												///CA("tagInfo.whichTab == 4 && tagInfo.elementId == -101");
												//IIDXMLElement * itemsTagXMLElementPtr = tagInfo.tagPtr;
												InterfacePtr<IIDXMLElement> itemsTagXMLElementPtr(tagInfo.tagPtr);
												if(itemsTagXMLElementPtr == nil)
												{					
														//CA("itemsTagXMLElementPtr == nil");
														//CA("22222222");
														continue;
												}	
												int32 childTagCount = itemsTagXMLElementPtr->GetChildCount();
												for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
												{
													XMLReference childTagRef = itemsTagXMLElementPtr->GetNthChild(childTagIndex);
													//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
													InterfacePtr<IIDXMLElement> childTagXMLElementPtr(childTagRef.Instantiate());
													if(childTagXMLElementPtr == nil)
														continue;
													
													PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));
													double ID = strID.GetAsDouble();

													if( ID == -121 || ID == -983 || ID == -976
														|| ID == -977 || ID == -978 || ID == -979 || ID == -980)
													{
														PMString strtableId = childTagXMLElementPtr->GetAttributeValue(WideString("tableId"));
														double tableId = strtableId.GetAsDouble();
														itemListIds.insert(tableId);
													}

													bool16 isPresent = kFalse;
													for(int32 i = 0;i < attributeIDsList.size(); i++)
													{
														
														if(ID == attributeIDsList[i])
														{
															isPresent = kTrue;
															break;
														}
													}
													if(!isPresent)
														attributeIDsList.push_back(ID);
												}
											}
											else if(tagInfo.whichTab == 4)
											{
												//CA("tagInfo.whichTab == 4");
												if( tagInfo.elementId == -121 || tagInfo.elementId == -983 || tagInfo.elementId == -976
														|| tagInfo.elementId == -977 || tagInfo.elementId == -978 || tagInfo.elementId == -979 || tagInfo.elementId == -980)
												{
													itemListIds.insert(tagInfo.tableId);
												}

												bool16 isPresent = kFalse;
												for(int32 i = 0;i < attributeIDsList.size(); i++)
												{
													if(tagInfo.elementId == attributeIDsList[i])
													{
														isPresent = kTrue;
														break;
													}
												}
												if(!isPresent)
													attributeIDsList.push_back(tagInfo.elementId);										
											}
											else if(tagInfo.whichTab == 5)
											{
												//CA("tagInfo.whichTab == 5");
												
												bool16 isPresent = kFalse;
												for(int32 i = 0;i < eventAttributeIDsList.size(); i++)
												{
													if(tagInfo.elementId == eventAttributeIDsList[i])
													{
														isPresent = kTrue;
														break;
													}
												}
												if(!isPresent)
												{
													eventAttributeIDsList.push_back(tagInfo.elementId);
													CMediatorClass::oldAttributeLanguageID = tagInfo.languageID;										
												}
											}
										}
										//------------ //og
										//for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++) //og
										//{		//og
											//tList1[tagIndex].tagPtr->Release(); //og
										//} //og
										
									}						
									for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
									{
										tList1[tagIndex].tagPtr->Release();
									}
								} 					
								//CA("33333333333333333");
								continue;
							}
							

							TagList tList1;							
							
							tList1 = tReader->getTagsFromBox(boxRef, &ptr);
							if(tList1.size() == 0)
							{
								continue;
							}
											
							if(tList1.size() <= 0)   // check tag list whether it has somthing
							{
								continue;
							}
							

							if(tList1.size() > 0)
							{
								for(int32 tagIndex = 0; tagIndex < tList1.size(); tagIndex++)
								{
									TagStruct & tagInfo = tList1[tagIndex];
									
									if(tagInfo.isTablePresent)
									{
										//CA("Table Present 2");
										if(tagInfo.elementId == -115)
										{
											//objCSprayStencilInfo.isHyTable = kTrue;
											//objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
										}
										else
										{
											//objCSprayStencilInfo.isDBTable = kTrue;
											//objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);

											if(tagInfo.whichTab == 4 && tagInfo.elementId == -102)
											{
												//CA("2222222-------tagInfo.whichTab == 4 && tagInfo.elementId == -102");
												TagList tList2;							
							
												tList2 = tReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);


												// added by avinash
												for(int32 tagIndex = 0 ; tagIndex < tList2.size() ; tagIndex++)
												{
													tList2[tagIndex].tagPtr->Release();
												}
												//till here
							
												/*PMString size("tList2.size = ");
												size.AppendNumber(tList2.size());
												CA(size);*/

												//IIDXMLElement * itemsTagXMLElementPtr = tagInfo.tagPtr;
												//if(itemsTagXMLElementPtr == nil)
												//{					
												//		//CA("itemsTagXMLElementPtr == nil");
												//		continue;
												//}	
												//int32 childTagCount = itemsTagXMLElementPtr->GetChildCount();
												//for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
												//{
												//	XMLReference childTagRef = itemsTagXMLElementPtr->GetNthChild(childTagIndex);
												//	//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
												//	InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
												//	if(childTagXMLElementPtr == nil)
												//		continue;
												//	
												//	PMString strID = childTagXMLElementPtr->GetAttributeValue("ID");
												//	int32 ID = strID.GetAsNumber();


												//	bool16 isPresent = kFalse;
												//	for(int32 i = 0;i < attributeIDsList.size(); i++)
												//	{
												//		if(ID == attributeIDsList[i])
												//		{
												//			isPresent = kTrue;
												//			break;
												//		}
												//	}
												//	if(!isPresent)
												//		attributeIDsList.push_back(tagInfo.elementId);
												//}
											}
										}
										continue;
									}

									if(tagInfo.imgFlag == 1)
									{
										//CA("image Present 2222222222222");
										//objCSprayStencilInfo.isAsset = kTrue;
										//objCSprayStencilInfo.AssetIds.push_back(tagInfo.typeId);
										continue;
									}
									
									//CA("copy Attribute");
									//objCSprayStencilInfo.isCopy = kTrue;

									if(tagInfo.whichTab == 3)
									{
										//CA("222222222222222222222");
										if( tagInfo.elementId == -121 || tagInfo.elementId == -983 || tagInfo.elementId == -976
														|| tagInfo.elementId == -977 || tagInfo.elementId == -978 || tagInfo.elementId == -979 || tagInfo.elementId == -980)
										{
											itemGroupListIds.insert(tagInfo.tableId);
										}

										bool16 isPresent = kFalse;
										for(int32 i = 0;i < elementIDsList.size(); i++)
										{
											if(tagInfo.elementId == elementIDsList[i])
											{
												isPresent = kTrue;
												break;
											}
										}
										if(!isPresent)
										{
											elementIDsList.push_back(tagInfo.elementId);
											CMediatorClass::oldAttributeLanguageID = tagInfo.languageID;
										}
									}

									if(tagInfo.whichTab == 4 && tagInfo.elementId == -101)
									{
										//CA("33333333333333333");
										//IIDXMLElement * itemsTagXMLElementPtr = tagInfo.tagPtr;
										InterfacePtr<IIDXMLElement> itemsTagXMLElementPtr(tagInfo.tagPtr); //added by avinash
										if(itemsTagXMLElementPtr == nil)
										{					
												//CA("itemsTagXMLElementPtr == nil");
												continue;
										}	
										int32 childTagCount = itemsTagXMLElementPtr->GetChildCount();
										for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
										{
											XMLReference childTagRef = itemsTagXMLElementPtr->GetNthChild(childTagIndex);
											//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
											InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate()); //added by avinash
											if(childTagXMLElementPtr == nil)
												continue;
											
											PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));
											double ID = strID.GetAsDouble();

											if( ID == -121 || ID == -983 || ID == -976
														|| ID == -977 || ID == -978 || ID == -979 || ID == -980)
											{
												PMString strtableId = childTagXMLElementPtr->GetAttributeValue(WideString("tableId"));
												double tableId = strtableId.GetAsDouble();
												itemListIds.insert(tableId);
											}

											bool16 isPresent = kFalse;
											for(int32 i = 0;i < attributeIDsList.size(); i++)
											{
												if(ID == attributeIDsList[i])
												{
													isPresent = kTrue;
													break;
												}
											}
											if(!isPresent)
												attributeIDsList.push_back(ID);//attributeIDsList.push_back(tagInfo.elementId);
										}
									}
									else if(tagInfo.whichTab == 4)
									{
										//CA("44444444444444444");
										if( tagInfo.elementId == -121 || tagInfo.elementId == -983 || tagInfo.elementId == -976
												|| tagInfo.elementId == -977 || tagInfo.elementId == -978 || tagInfo.elementId == -979 || tagInfo.elementId == -980)
										{
											itemListIds.insert(tagInfo.tableId);
										}

										bool16 isPresent = kFalse;
										for(int32 i = 0;i < attributeIDsList.size(); i++)
										{
											if(tagInfo.elementId == attributeIDsList[i])
											{
												isPresent = kTrue;
												break;
											}
										}
										if(!isPresent)
											attributeIDsList.push_back(tagInfo.elementId);										
									}
									else if(tagInfo.whichTab == 5)
									{
										//CA("555555555555555");
										
										bool16 isPresent = kFalse;
										for(int32 i = 0;i < eventAttributeIDsList.size(); i++)
										{
											if(tagInfo.elementId == eventAttributeIDsList[i])
											{
												isPresent = kTrue;
												break;
											}
										}
										if(!isPresent)
										{
											eventAttributeIDsList.push_back(tagInfo.elementId);
											CMediatorClass::oldAttributeLanguageID = tagInfo.languageID;										
										}
									}
								}
								//------------
								for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
								{
									tList1[tagIndex].tagPtr->Release();
								}
								
							}	


							//if(tList1.size() > 0)
							//{
							//for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
							//{
							//	tList1[tagIndex].tagPtr->Release();
							//}							
							//}
						} 					
					}
					else
					{
						//CA("isGroupFrame == kFalse");						
						IIDXMLElement* ptr;
						UIDRef boxRef = (tempList.GetRef(frameIndex));
						TagList tList1 = tReader->getTagsFromBox(boxRef, &ptr);

						if(tList1.size() == 0)
						{
							continue;
						}
										
						if(tList1.size() <= 0)   // check tag list whether it has somthing
						{
							continue;
						}
						

						if(tList1.size() > 0)
						{
							//CA("tList1.size() > 0");
							for(int32 tagIndex = 0; tagIndex < tList1.size(); tagIndex++)
							{
								TagStruct & tagInfo = tList1[tagIndex];
								
								if(tagInfo.isTablePresent)
								{
									//CA("Table Present");
									if(tagInfo.elementId == -115)
									{
										//objCSprayStencilInfo.isHyTable = kTrue;
										//objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
									}
									else
									{
										//objCSprayStencilInfo.isDBTable = kTrue;
										//objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);
										//CA("tagInfo.elementId != -115");
										if((tagInfo.whichTab == 4 && tagInfo.elementId == -102) || (tagInfo.whichTab == 3 && tagInfo.elementId == -102))//*******
										{
											//CA("tagInfo.whichTab == 4 && tagInfo.elementId == -102 || (tagInfo.whichTab == 3 && tagInfo.elementId == -102)**************");
											TagList tList2;
						
											tList2 = tReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
						
											for(int32 cellIndex = 0 ;cellIndex < tList2.size(); cellIndex++)
											{
												//IIDXMLElement * itemsTagXMLElementPtr = tList2[cellIndex].tagPtr; //og
												InterfacePtr<IIDXMLElement> itemsTagXMLElementPtr(tList2[cellIndex].tagPtr, UseDefaultIID()); //added by avinash
												if(itemsTagXMLElementPtr == nil)
												{					
													//CA("cellTagXMLElementPtr == nil");
													continue;
												}

												if(WideString(itemsTagXMLElementPtr->GetTagString()) == WideString("PSCell"))
													continue;

												PMString strID = itemsTagXMLElementPtr->GetAttributeValue(WideString("ID"));
												double ID = strID.GetAsDouble();
												TagStruct tagInfoIN =  tList2[cellIndex];//*******

												if(tagInfoIN.whichTab == 4 )
												{
													
													if( ID == -121 || ID == -983 || ID == -976
																|| ID == -977 || ID == -978 || ID == -979 || ID == -980)
													{
														PMString strtableId = itemsTagXMLElementPtr->GetAttributeValue(WideString("tableId"));
														double tableId = strtableId.GetAsDouble();
														itemListIds.insert(tableId);
													}
													bool16 isPresent = kFalse;
													for(int32 i = 0;i < attributeIDsList.size(); i++)
													{
														/*PMString v("ID  :  ");
														v.AppendNumber(ID);
														v.Append("\n attributeIDsList[i]  :  ");
														v.AppendNumber(attributeIDsList[i]);
														CA(v);*/
														if(ID == attributeIDsList[i])
														{
															//CA("ID == attributeIDsList[i]");
															isPresent = kTrue;
															break;
														}
													}
													if(!isPresent)
													{
														attributeIDsList.push_back(ID);
														CMediatorClass::oldAttributeLanguageID = tagInfoIN.languageID;
													}
												}
												else if(tagInfoIN.whichTab == 3 && tagInfoIN.elementId != -121 )//*******
												{
													//CA("OTHER THA -121");
													if( tagInfoIN.elementId == -983 || tagInfoIN.elementId == -976
														|| tagInfoIN.elementId == -977 || tagInfoIN.elementId == -978 || tagInfoIN.elementId == -979 || tagInfoIN.elementId == -980)
													{
														itemGroupListIds.insert(tagInfoIN.tableId);
													}
													bool16 isPresent = kFalse;
													for(int32 i = 0;i < elementIDsList.size(); i++)
													{

														if(ID == elementIDsList[i])
														{
															isPresent = kTrue;
															break;
														}
													}
													if(!isPresent){
														//CA("A");
														elementIDsList.push_back(ID);
														CMediatorClass::oldAttributeLanguageID = tagInfoIN.languageID;
													}
												}
												else if (tagInfoIN.whichTab == 3 && tagInfoIN.elementId == -121) {

													if( tagInfoIN.elementId == -121 )
													{
														itemGroupListIds.insert(tagInfoIN.tableId);
													}

													isListTableNameForProductPresent = kTrue;
													bool16 isPresent = kFalse;
													for(int32 i = 0;i < elementIDsList.size(); i++)
													{

														if(ID == elementIDsList[i])
														{
															isPresent = kTrue;
															break;
														}
													}
													if(!isPresent){
															elementIDsList.push_back(ID);
															CMediatorClass::oldAttributeLanguageID = tagInfoIN.languageID;
													}
													//CA("TESTING");
													//elementIDsList.push_back(ID); //added by avinash
												}												
												else if (tagInfoIN.whichTab == 4 && tagInfoIN.elementId == -121) {
													//CA("ITEM");
													isListTableNameForItemPresent = kTrue;
												}	
												  //***********												
											}											

											// added by avinash
											//CA("1111111111");

											for(int32 tagIndex = 0 ; tagIndex < tList2.size() ; tagIndex++)
											{
												tList2[tagIndex].tagPtr->Release();
											}

											//till here
										}										
										//----------
										else if(tagInfo.whichTab == 4 && (tagInfo.tableType == 1 || tagInfo.tableType == 3))
										{
											//CA("tagInfo.whichTab == 4 && tagInfo.tableType == 1");
											TagList tList2;
						
											tList2 = tReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
						
											for(int32 cellIndex = 0 ;cellIndex < tList2.size(); cellIndex++)
											{
												//IIDXMLElement * itemsTagXMLElementPtr = tList2[cellIndex].tagPtr; //og
												InterfacePtr<IIDXMLElement> itemsTagXMLElementPtr(tList2[cellIndex].tagPtr, UseDefaultIID()); //added by avinash
												if(itemsTagXMLElementPtr == nil) 
												{					
													//CA("cellTagXMLElementPtr == nil");
													continue;
												}

												if(WideString(itemsTagXMLElementPtr->GetTagString()) == WideString("PSCell"))
													continue;

												PMString strID = itemsTagXMLElementPtr->GetAttributeValue(WideString("ID"));
												double ID = strID.GetAsDouble();

												
												if( ID == -121 || ID == -983 || ID == -976
															|| ID == -977 || ID == -978 || ID == -979 || ID == -980)
												{
													PMString strtableId = itemsTagXMLElementPtr->GetAttributeValue(WideString("tableId"));
													double tableId = strtableId.GetAsDouble();
													itemListIds.insert(tableId);
												}

												bool16 isPresent = kFalse;
												for(int32 i = 0;i < attributeIDsList.size(); i++)
												{
													if(ID == attributeIDsList[i])
													{
														//CA("ID == attributeIDsList[i]");
														isPresent = kTrue;
														break;
													}
												}
												if(!isPresent)
													attributeIDsList.push_back(ID);
												
											}

											// added by avinash
											//CA("222222222222222222222");
											for(int32 tagIndex = 0 ; tagIndex < tList2.size() ; tagIndex++)
											{
												tList2[tagIndex].tagPtr->Release();
											}
											//till here
										}
										else if(tagInfo.whichTab == 3 && tagInfo.tableType == 1)
										{
											//CA("tagInfo.whichTab == 3 && tagInfo.tableType == 1");
											TagList tList2;
						
											tList2 = tReader->getTagsFromBox_ForRefresh_ByAttribute(boxRef, &ptr);
						
											for(int32 cellIndex = 0 ;cellIndex < tList2.size(); cellIndex++)
											{
												//IIDXMLElement * itemsTagXMLElementPtr = tList2[cellIndex].tagPtr; //og
												InterfacePtr<IIDXMLElement> itemsTagXMLElementPtr(tList2[cellIndex].tagPtr, UseDefaultIID()); //added by avinash
												if(itemsTagXMLElementPtr == nil)
												{					
													//CA("cellTagXMLElementPtr == nil");
													continue;
												}

												if(WideString(itemsTagXMLElementPtr->GetTagString()) == WideString("PSCell"))
													continue;

												PMString strID = itemsTagXMLElementPtr->GetAttributeValue(WideString("ID"));
												double ID = strID.GetAsDouble();

												if(itemsTagXMLElementPtr->GetAttributeValue(WideString("index")) == WideString("4")){

													
													if( ID == -121 || ID == -983 || ID == -976
																|| ID == -977 || ID == -978 || ID == -979 || ID == -980)
													{
														PMString strtableId = itemsTagXMLElementPtr->GetAttributeValue(WideString("tableId"));
														double tableId = strtableId.GetAsDouble();
														itemListIds.insert(tableId);
													}

													bool16 isPresent = kFalse;
													for(int32 i = 0;i < attributeIDsList.size(); i++)
													{
														if(ID == attributeIDsList[i])
														{
															//CA("ID == attributeIDsList[i]");
															isPresent = kTrue;
															break;
														}
													}
													if(!isPresent)
														attributeIDsList.push_back(ID);
												}
												
											}

											// added by avinash
											//CA("333333333");
											for(int32 tagIndex = 0 ; tagIndex < tList2.size() ; tagIndex++)
											{
												tList2[tagIndex].tagPtr->Release();
											}
											//till here
										}
									}
									continue;
								}

								if(tagInfo.imgFlag == 1)
								{
									//CA("image Present");
									//objCSprayStencilInfo.isAsset = kTrue;
									//objCSprayStencilInfo.AssetIds.push_back(tagInfo.typeId);
									continue;
								}
								
								
								//CA("copy Attribute");
								//objCSprayStencilInfo.isCopy = kTrue;
								if(tagInfo.whichTab == 3)
								{
									//CA("tagInfo.whichTab == 3");
									if( tagInfo.elementId == -121 || tagInfo.elementId == -983 || tagInfo.elementId == -976
										|| tagInfo.elementId == -977 || tagInfo.elementId == -978 || tagInfo.elementId == -979 || tagInfo.elementId == -980)
									{
										itemGroupListIds.insert(tagInfo.tableId);
									}

									bool16 isPresent = kFalse;
									for(int32 i = 0;i < elementIDsList.size(); i++)
									{
										if(tagInfo.elementId == elementIDsList[i])
										{
											isPresent = kTrue;
											break;
										}
									}
									if(!isPresent)
									{
										elementIDsList.push_back(tagInfo.elementId);
										CMediatorClass::oldAttributeLanguageID = tagInfo.languageID;
									}
								}
								if(tagInfo.whichTab == 4 && tagInfo.elementId == -101)
								{
									//CA("tagInfo.whichTab == 4 && tagInfo.elementId == -101");
									//IIDXMLElement * itemsTagXMLElementPtr = tagInfo.tagPtr; //og
									InterfacePtr<IIDXMLElement> itemsTagXMLElementPtr(tagInfo.tagPtr, UseDefaultIID()); //added by avinash
									if(itemsTagXMLElementPtr == nil)
									{					
											//CA("itemsTagXMLElementPtr == nil");
											continue;
									}	
									int32 childTagCount = itemsTagXMLElementPtr->GetChildCount();
									for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
									{
										XMLReference childTagRef = itemsTagXMLElementPtr->GetNthChild(childTagIndex);
										//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
										InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
										if(childTagXMLElementPtr == nil)
											continue;
										
										PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));
										double ID = strID.GetAsDouble();

										if( ID == -121 || ID == -983 || ID == -976
													|| ID == -977 || ID == -978 || ID == -979 || ID == -980)
										{
											PMString strtableId = childTagXMLElementPtr->GetAttributeValue(WideString("tableId"));
											double tableId = strtableId.GetAsDouble();
											itemListIds.insert(tableId);
										}

										bool16 isPresent = kFalse;
										for(int32 i = 0;i < attributeIDsList.size(); i++)
										{
									
											if(ID == attributeIDsList[i])
											{
												isPresent = kTrue;
												break;
											}
										}
										if(!isPresent)
											attributeIDsList.push_back(ID);
									}
								}
								else if(tagInfo.whichTab == 4)
								{
									//CA("tagInfo.whichTab == 4");
									if( tagInfo.elementId == -121 || tagInfo.elementId == -983 || tagInfo.elementId == -976
										|| tagInfo.elementId == -977 || tagInfo.elementId == -978 || tagInfo.elementId == -979 || tagInfo.elementId == -980)
									{
										itemListIds.insert(tagInfo.tableId);
									}

									bool16 isPresent = kFalse;
									for(int32 i = 0;i < attributeIDsList.size(); i++)
									{
										if(tagInfo.elementId == attributeIDsList[i])
										{
											//CA("tagInfo.elementId == attributeIDsList[i]");
											isPresent = kTrue;
											break;
										}
									}
									if(!isPresent)
									{
										attributeIDsList.push_back(tagInfo.elementId);
										CMediatorClass::oldAttributeLanguageID = tagInfo.languageID;
										
										//CA("before Calling");
										//VectorAttributeInfoPtr vec_ptr = ptrIAppFramework->AttributeCache_getAttributeByParentIdLanguageId(tagInfo.elementId,2/*tagInfo.languageID*/);
										//CA("after Calling"); 
										//if(vec_ptr == NULL)
										//{
										//	CA("vec_ptr == NULL");
										//	continue;

										//}
										//PMString sizeVectorAttributeInfoPtr("VectorAttributeInfoPtr size = ");
										//sizeVectorAttributeInfoPtr.AppendNumber(vec_ptr->size());
										//CA(sizeVectorAttributeInfoPtr);

									
										//VectorAttributeInfoValue::iterator itrr = vec_ptr->begin();
										//for(int32 k = 0;k < vec_ptr->size(); k++,itrr++)
										//{
										//	CAttributeModel objAttributeModel = *itrr;

										//	PMString temp("objAttributeModel.getAttribute_id = ");
										//	temp.AppendNumber(objAttributeModel.getAttribute_id());
										//	temp.Append("  , objAttributeModel.getLanguage_id = ");
										//	temp.AppendNumber(objAttributeModel.getLanguage_id());
										//	temp.Append("  , objAttributeModel.getDisplay_name = ");
										//	temp.Append(objAttributeModel.getDisplay_name());
										//	CA(temp);
										//}
									}
								}
								else if(tagInfo.whichTab == 5)
								{
									
									bool16 isPresent = kFalse;
									for(int32 i = 0;i < eventAttributeIDsList.size(); i++)
									{
										if(tagInfo.elementId == eventAttributeIDsList[i])
										{
											isPresent = kTrue;
											break;
										}
									}
									if(!isPresent)
									{
										eventAttributeIDsList.push_back(tagInfo.elementId);
										CMediatorClass::oldAttributeLanguageID = tagInfo.languageID;										
									}
								}
							}
							//------------		//og
							//CA("OG");
							for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++) // og
							{ // og
								tList1[tagIndex].tagPtr->Release(); //og
							} //og
						}					

						//CA("TESTING");
						//if(tList1.size() > 0)
						//{
						//	for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
						//	{
						//		//tList1[tagIndex].tagPtr->Release();
						//	}
						//}
						
					}

					
				}

			}
		}

		PMString numberOfFrames("Frames = ");
		numberOfFrames.AppendNumber(progressBarMaxCount);
		//CA(numberOfFrames);

		sdklhelp.SaveDocumentAs(CurrDocRef,BookFileList[p]);
		
		sdklhelp.CloseDocument(CurrDocRef, kFalse);	
	}
}


void CommonFunctions::startUpdate()
{
	//int32 framecount=0;
	//CA("Inside CommonFunctions::startUpdate");
	CMediatorClass::noOfItemFieldsUpdated = 0;
	CMediatorClass::noOfProductFieldsUpdated = 0;

	PMString title("Update in Progress...");
	title.SetTranslatable(kFalse);
	
	//RangeProgressBar progressBar(title, 0,static_cast<int32> (BookFileList.size()), kTrue); //og
RangeProgressBar* progressBar = new RangeProgressBar(title, 0,progressBarMaxCount, kTrue);

	for(int32 p=0; p< BookFileList.size(); p++) ////iterate through all documents of that book.
	{
		SDKLayoutHelper sdklhelp;

		UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[p]);
		ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);
        
		PMString FileName("");
        #if WINDOWS
            FileName= file.GetString();
        #else
            FileName = FileUtils::SysFileToPMString(BookFileList[p]);
        #endif
		//FileName.Append(FileName);
		//CA(FileName);
		FileName.SetTranslatable(kFalse);
		progressBar->SetTaskText(FileName);

		InterfacePtr<IDocument> currDoc(CurrDocRef,UseDefaultIID());

		//CA("Commenting THE UPDATE DOCUMENT FUCNTION");
		//this->UpdateDocument(currDoc,FileName); //og
		this->UpdateDocument(currDoc,FileName,progressBar);		

		sdklhelp.SaveDocumentAs(CurrDocRef,BookFileList[p]);
		
		sdklhelp.CloseDocument(CurrDocRef, kFalse);	


		//progressBar.SetPosition(p); 
		if(progressBar->WasCancelled())
		{
			//CA("Cancel of ProgressBar is clicked");
			progressBar->Abort();
			PMString lastIndex("Last Index is: ");
			lastIndex.AppendNumber(p);
			//CA(lastIndex);
			if(progressBar!= NULL)
			{
				//CA("delete");
				delete progressBar;
				progressBar=NULL;
			}
			
			progressBarMaxCount=0;			
			return;
		}
	}
	//if(progressBar.WasCancelled())
	
	//progressBar->Abort();
	if(progressBar!= NULL)
	{
		//CA("delete");
		delete progressBar;
		progressBar=NULL;
	}
	
	progressBarMaxCount=0;
}

//void CommonFunctions::UpdateDocument(InterfacePtr<IDocument> bgDoc,PMString FileName) //og
void CommonFunctions::UpdateDocument(InterfacePtr<IDocument> bgDoc,PMString FileName,RangeProgressBar* progressBar)
{
	//CA("Inside CommonFunctions::UpdateDocument");

	//int32 TotalPageCount=-1; //og
	int32 TotalPageCount= 0;
	static int32 countp=0;
	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)bgDoc,UseDefaultIID());
	if (iSpreadList==nil)
	{
		//CA("Error iSpreadList==nil");
		return;
	}
	//iSpreadList->AddRef();
	///////////////For loop for investigeting all spreads////////////////////////
	for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
	{
		UIDRef spreadUIDRef(::GetDataBase(bgDoc), iSpreadList->GetNthSpreadUID(numSp));

		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
		if(!spread)
		{
			//CA("Error !spread");
			return;
		}
		int numPages=spread->GetNumPages();
		//PMString strPages("PAGES = ");
		//strPages.AppendNumber(numPages);
		//CA(strPages);

		////////////FOR loop for investigeting all pages of Spread///////////
		for(int pageNo=0; pageNo<numPages; pageNo++)
		{
			//CA("pageeeee");
			//TotalPageCount++; og
			
			UIDList itemList(::GetDataBase(bgDoc));
			UID pageUIDs = spread->GetNthPageUID(pageNo);
			spread->GetItemsOnPage(pageNo, &itemList, kFalse);
			
			PMString listCount("Total List Element: ");
			listCount.AppendNumber(itemList.Length());
			//CA(listCount);

			/////FOR loop for investigeting all items of page//////////////

			for(int count=0;count<itemList.Length();count++)
			{
				countp++;
				if(progressBar!=NULL)
					progressBar->SetPosition(countp);

				//CA("page itemsssss");
				InterfacePtr<IHierarchy> iHier(itemList.GetRef(count), UseDefaultIID());
				if(!iHier)
				{
					//CA("Continue   ");					
					continue;
				}

				UID kidUID;
				int32 numKids=iHier->GetChildCount();
				bool16 isGroupFrame = kFalse ;
				isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(itemList.GetRef(count));

				if(isGroupFrame == kTrue)
				{
					PMString temp("numKids = ");
					temp.AppendNumber(numKids);
					//CA(temp);

					for(int j=0;j<numKids;j++)
					{
						//CA("1");
						kidUID=iHier->GetChildUID(j);
						UIDRef boxRef(itemList.GetDataBase(), kidUID);
						//IIDXMLElement* ptr;

						bool16 isGroupFrameInsideGroupFrame = kFalse ;
						isGroupFrameInsideGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(boxRef);
							
						if(isGroupFrameInsideGroupFrame)
						{
							//CA("isGroupFrameInsideGroupFrame");
							InterfacePtr<IHierarchy> iHier(boxRef, UseDefaultIID());
							if(!iHier)
							{
								//CA("Continue   ");					
								continue;
							}

							UID kidUID;
							int32 numKids=iHier->GetChildCount();
							
							for(int j=0;j<numKids;j++)
							{
								//CA("1");
								kidUID=iHier->GetChildUID(j);
								UIDRef boxRef(itemList.GetDataBase(), kidUID);
								//IIDXMLElement* ptr;

								UID frameUID1 = kInvalidUID;
								InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxRef, UseDefaultIID());
								if (graphicFrameDataOne) 
								{
									frameUID1 = graphicFrameDataOne->GetTextContentUID();
								}
								if(frameUID1==kInvalidUID)
								{
									//CA("Main Graphic Frame");
								}
								else
								{

									//InterfacePtr<ISelectionManager> iSelectionManager(Utils<ISelectionUtils>()->GetActiveSelection());
									//if(!iSelectionManager)
									//{
									//	CA("Source Selection Manager is null");
									//	break;
									//}
									//CA("Main Text Frame");
									//	InterfacePtr<ILayoutSelectionSuite>layoutSelSuite(Utils<ISelectionUtils>()->QueryLayoutSelectionSuite(iSelectionManager));
									//		if(!layoutSelSuite)
									//		{
									//		CA("Layout Selection Suite is null");
									//		//break;
									//		}
											//layoutSelSuite->Select(frameUID1, Selection::Action::kAddTo,Selection::ScrollChoice ::kScrollIntoView );
							//CS3  Change
									/*InterfacePtr<ITextFrame>textFrame(itemList.GetDataBase(),frameUID1,UseDefaultIID());
									if(!textFrame)
									{
										CA("Text Frame is null");
										continue;
									}*/

									InterfacePtr<IHierarchy>graphicFrameHierarchy(boxRef, UseDefaultIID());
									if (graphicFrameHierarchy == nil) 
									{
										//CA("graphicFrameHierarchy == nil");
										return;
									}
													
									InterfacePtr<IHierarchy>multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
									if (!multiColumnItemHierarchy) {
										//CA("multiColumnItemHierarchy == nil");
										return;
									}

									InterfacePtr<IMultiColumnTextFrame>multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
									if (!multiColumnItemTextFrame) {
										ASSERT(multiColumnItemTextFrame);
										//CA("multiColumnItemTextFrame == nil");
										return;
									}
									InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
									if (!frameItemHierarchy) {
										//CA("frameItemHierarchy == nil");
										continue;
									}

									InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
									if (!textFrame) {
										//CA("!!!ITextFrameColumn");
										continue;
									}
								//To this
									InterfacePtr<ITextModel>textModel(textFrame->QueryTextModel());
									if (textModel == nil)
									{
										//CA("textModel == nil");
										continue;
									}
									InterfacePtr<ITableModelList>tableList(textModel, UseDefaultIID());
									if(tableList==nil)
									{
										//CA("tableList==nil");
										continue;
									}
									int32	tableIndex = tableList->GetModelCount() ;

									bool16 customItemTableFound = kFalse;
									for(int32 index = 0;index < tableIndex ; index++)
									{
										//CA("for");
										InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(index));
										if(tableModel == nil) 
										{
											continue;
										}
										InterfacePtr<IXMLReferenceData>xmlRefData(tableModel,UseDefaultIID());
										if(xmlRefData == nil)
										{
											continue;
										}
										XMLReference tableXMLRef = xmlRefData->GetReference();
										IIDXMLElement * tableXMLElementPtr = tableXMLRef.Instantiate();
										//InterfacePtr<IIDXMLElement>tableXMLElementPtr(tableXMLRef.Instantiate());
										if(tableXMLElementPtr == nil)
										{
											continue;
										}
										PMString strIDValue = tableXMLElementPtr->GetAttributeValue(WideString("ID"));
										if(strIDValue == "-102")
										{
											customItemTableFound = kTrue;
											break;
										}
									}
								
									if(tableIndex<=0 || customItemTableFound) //This check is very important...  this ascertains if the table is in box or not.
									{
										//CA("tableIndex<=0 || customItemTableFound.......................");
										//UIDRef TextUIDRef(::GetUIDRef(textFrame)); //CS3 Change
									//	UIDRef TextUIDRef = itemList.GetRef(count);
										//UpdateTabbedData(TextUIDRef,FileName,TotalPageCount);
										//CA("COMMENTING");
										//CA("33333333333333333333");
										UpdateTabbedDataWithXML(boxRef,FileName,TotalPageCount);
											continue;
									}
									else
									{		
										//CA("tableIndex > 0 || ! customItemTableFound................");
										/*InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(p));
										if(tableModel == nil) 
										{
											continue;
										}*/
										//UIDRef textUIDRef(::GetUIDRef(textFrame)); //CS3Change
										UIDRef textUIDRef = itemList.GetRef(count);
										//UpdateTabelDataWithXML(tableList,textUIDRef,FileName,TotalPageCount);
										
									}

								}
								
							}


							continue;
						}

						//CA("2");

						UID frameUID1 = kInvalidUID;
						InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxRef, UseDefaultIID());
						if (graphicFrameDataOne) 
						{
							frameUID1 = graphicFrameDataOne->GetTextContentUID();
						}
						if(frameUID1==kInvalidUID)
						{
							//CA("Main Graphic Frame");
						}
						else
						{

							//InterfacePtr<ISelectionManager> iSelectionManager(Utils<ISelectionUtils>()->GetActiveSelection());
							//if(!iSelectionManager)
							//{
							//	CA("Source Selection Manager is null");
							//	break;
							//}
							//CA("Main Text Frame");
							//	InterfacePtr<ILayoutSelectionSuite>layoutSelSuite(Utils<ISelectionUtils>()->QueryLayoutSelectionSuite(iSelectionManager));
							//		if(!layoutSelSuite)
							//		{
							//		CA("Layout Selection Suite is null");
							//		//break;
							//		}
									//layoutSelSuite->Select(frameUID1, Selection::Action::kAddTo,Selection::ScrollChoice ::kScrollIntoView );
					//CS3  Change
							/*InterfacePtr<ITextFrame>textFrame(itemList.GetDataBase(),frameUID1,UseDefaultIID());
							if(!textFrame)
							{
								CA("Text Frame is null");
								continue;
							}*/

							InterfacePtr<IHierarchy>graphicFrameHierarchy(boxRef, UseDefaultIID());
							if (graphicFrameHierarchy == nil) 
							{
								//CA("graphicFrameHierarchy == nil");
								return;
							}
											
							InterfacePtr<IHierarchy>multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
							if (!multiColumnItemHierarchy) {
								//CA("multiColumnItemHierarchy == nil");
								return;
							}

							InterfacePtr<IMultiColumnTextFrame>multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
							if (!multiColumnItemTextFrame) {
								ASSERT(multiColumnItemTextFrame);
								//CA("multiColumnItemTextFrame == nil");
								return;
							}
							InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
							if (!frameItemHierarchy) {
								//CA("frameItemHierarchy == nil");
								continue;
							}

							InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
							if (!textFrame) {
								//CA("!!!ITextFrameColumn");
								continue;
							}
						//To this
							InterfacePtr<ITextModel>textModel(textFrame->QueryTextModel());
							if (textModel == nil)
							{
								//CA("textModel == nil");
								continue;
							}
							InterfacePtr<ITableModelList>tableList(textModel, UseDefaultIID());
							if(tableList==nil)
							{
								//CA("tableList==nil");
								continue;
							}
							int32	tableIndex = tableList->GetModelCount() ;

							bool16 customItemTableFound = kFalse;
							for(int32 index = 0;index < tableIndex ; index++)
							{
								InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(index));
								if(tableModel == nil) 
								{
									continue;
								}
								InterfacePtr<IXMLReferenceData>xmlRefData(tableModel,UseDefaultIID());
								if(xmlRefData == nil)
								{
									continue;
								}
								XMLReference tableXMLRef = xmlRefData->GetReference();
								//IIDXMLElement * tableXMLElementPtr = tableXMLRef.Instantiate();
								InterfacePtr<IIDXMLElement>tableXMLElementPtr(tableXMLRef.Instantiate()); //added by avinash
								if(tableXMLElementPtr == nil)
								{
									continue;
								}
								PMString strIDValue = tableXMLElementPtr->GetAttributeValue(WideString("ID"));
								if(strIDValue == "-102")
								{
									customItemTableFound = kTrue;
									break;
								}
							}
						
							if(tableIndex<=0 || customItemTableFound) //This check is very important...  this ascertains if the table is in box or not.
							{
								//CA("tableIndex<=0 || customItemTableFound");
								//UIDRef TextUIDRef(::GetUIDRef(textFrame)); //CS3 Change
							//	UIDRef TextUIDRef = itemList.GetRef(count);
								//UpdateTabbedData(TextUIDRef,FileName,TotalPageCount);
								//CA("222222222222222222222");
								UpdateTabbedDataWithXML(boxRef,FileName,TotalPageCount);
									continue;
							}
							else
							{		
								//CA("tableIndex > 0 || ! customItemTableFound");
								/*InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(p));
								if(tableModel == nil) 
								{
									continue;
								}*/
								//UIDRef textUIDRef(::GetUIDRef(textFrame)); //CS3Change
								UIDRef textUIDRef = itemList.GetRef(count);
								//UpdateTabelDataWithXML(tableList,textUIDRef,FileName,TotalPageCount);
								
							}

						}
						
					}

					//CA("IDHAR");
					continue; //og
				}

				//else{
					//CA("FALSE");
				
					
				/*InterfacePtr<IPMUnknown> Funknown(itemList.GetRef(count), IID_IUNKNOWN);
				UID frameUID1 = Utils<IFrameUtils>()->GetTextFrameUID(Funknown);*/
				UID frameUID1 = kInvalidUID;
				InterfacePtr<IGraphicFrameData> graphicFrameDataOne(itemList.GetRef(count), UseDefaultIID());
				if (graphicFrameDataOne) 
				{
					frameUID1 = graphicFrameDataOne->GetTextContentUID();
				}
				if(frameUID1==kInvalidUID)
				{
					//CA("Main Graphic Frame");
				}
				else
				{
					//CA("frameUID1!=kInvalidUID");
					//InterfacePtr<ISelectionManager> iSelectionManager(Utils<ISelectionUtils>()->GetActiveSelection());
					//if(!iSelectionManager)
					//{
					//	CA("Source Selection Manager is null");
					//	break;
					//}
					//CA("Main Text Frame");
					//	InterfacePtr<ILayoutSelectionSuite>layoutSelSuite(Utils<ISelectionUtils>()->QueryLayoutSelectionSuite(iSelectionManager));
					//		if(!layoutSelSuite)
					//		{
					//		CA("Layout Selection Suite is null");
					//		//break;
					//		}
							//layoutSelSuite->Select(frameUID1, Selection::Action::kAddTo,Selection::ScrollChoice ::kScrollIntoView );
			//CS3  Change
					/*InterfacePtr<ITextFrame>textFrame(itemList.GetDataBase(),frameUID1,UseDefaultIID());
					if(!textFrame)
					{
						CA("Text Frame is null");
						continue;
					}*/


						InterfacePtr<IHierarchy>graphicFrameHierarchy(itemList.GetRef(count), UseDefaultIID());
						if (graphicFrameHierarchy == nil) 
						{
							//CA("graphicFrameHierarchy == nil");
							return;
						}
										
						InterfacePtr<IHierarchy>multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
						if (!multiColumnItemHierarchy) {
							//CA("multiColumnItemHierarchy == nil");
							return;
						}

						InterfacePtr<IMultiColumnTextFrame>multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
						if (!multiColumnItemTextFrame) {
							ASSERT(multiColumnItemTextFrame);
							//CA("multiColumnItemTextFrame == nil");
							return;
						}
						InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
						if (!frameItemHierarchy) {
							//CA("frameItemHierarchy == nil");
							continue;
						}

						InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
						if (!textFrame) {
							//CA("!!!ITextFrameColumn");
							continue;
						}
					//To this
						InterfacePtr<ITextModel>textModel(textFrame->QueryTextModel());
						if (textModel == nil)
						{
							//CA("textModel == nil");
							continue;
						}
						InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
						if(tableList==nil)
						{
							//CA("tableList==nil");
							continue;
						}
						int32 tableIndex = tableList->GetModelCount() ;

						bool16 customItemTableFound = kFalse;
						for(int32 index = 0;index < tableIndex ; index++)
						{
							InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(index));
							if(tableModel == nil) 
							{
								continue;
							}
							InterfacePtr<IXMLReferenceData>xmlRefData(tableModel,UseDefaultIID());
							if(xmlRefData == nil)
							{
								continue;
							}
							XMLReference tableXMLRef = xmlRefData->GetReference();
							//IIDXMLElement * tableXMLElementPtr = tableXMLRef.Instantiate();
							InterfacePtr<IIDXMLElement>tableXMLElementPtr(tableXMLRef.Instantiate()); //added by avinash
							if(tableXMLElementPtr == nil)
							{
								continue;
							}
							PMString strIDValue = tableXMLElementPtr->GetAttributeValue(WideString("ID"));
							PMString strTableTypeValue = tableXMLElementPtr->GetAttributeValue(WideString("tableType"));
							if(strIDValue == "-102" || strTableTypeValue == "1" || strTableTypeValue == "3" )
							{
								//CA("strIDValue == 102");
								customItemTableFound = kTrue;
								break;
							}
						}
						
						if(tableIndex<=0 || customItemTableFound) //This check is very important...  this ascertains if the table is in box or not.
						{
							//CA("tableIndex<=0 || customItemTableFound");
							//UIDRef TextUIDRef(::GetUIDRef(textFrame)); //CS3 Change

							//CA("tableIndex<=0 || customItemTableFound");
							UIDRef TextUIDRef = itemList.GetRef(count);
							//UpdateTabbedData(TextUIDRef,FileName,TotalPageCount);

							//CA("11111111111111111");
							if(tableIndex<=0)
							{
								UpdateTabbedDataWithXML(TextUIDRef,FileName,TotalPageCount); //og
								//continue;
							}
							else if(customItemTableFound)
							{
								//CA("COMMENTED");
								UpdateTabelDataWithXML(tableList,TextUIDRef,FileName,pageNo/*TotalPageCount*/);
								//continue;
							}
						}
						else
						{		
							//CA("tableIndex > 0 || ! customItemTableFound");
							/*InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(p));
							if(tableModel == nil) 
							{
								continue;
							}*/
							//UIDRef textUIDRef(::GetUIDRef(textFrame)); //CS3Change
							UIDRef textUIDRef = itemList.GetRef(count);
							//UpdateTabelDataWithXML(tableList,textUIDRef,FileName,TotalPageCount); //og was commented
							
							
						}

					}
				//} //here added for testing
					TotalPageCount++; //testing
			}
			/////end  ofFOR loop for investigeting all items of page//////////////			
		}

		////////////End of FOR loop for investigeting all pages of Spread///////////
	}
	countp=0;
	///////////////end of FOR loop for investigeting all spreads//////////////////
}

void CommonFunctions::UpdateTabbedDataWithXML(const UIDRef& BoxUIDRef,PMString FileName,int32 pageNo)
{	
	/* We have to handle following conditions.
	   condition 1 : ItemTableInTabbedTextFormat
	   condition 2 : CustomItemTableInTabbedTextFormat..
				     Tabbed Text with item and non item copy attributes.
	   condition 3 : CustomItemTableInTable


	 Lets analyze each one by one
	 Condition 1: ItemTableInTabedTextFormat
	 The TagStruct attached to the frame is as below
	 [PRINTsource]__
				    |
				    |
				 [Items]
				 (ID = "-101" ,tableFlag = "-11",whichTab = "3")
					     |
					     |
						  ----[ItemCopyHeader]
						 |	 (typeID = "-2",tableFlag = "-12",whichTab = "3")
					     |
						  ----[ItemCopyValue]
							 (tableFlag = "-12",whichTab = "3")

	Condition 2: CustomItemTableInTabbedTextFormat
	The TagStruct attached to the frame is as below
	[PRINTsource]__
				   |
				   |
				    ---[ItemCopyHeader]
				   |  (typeID = "-2" ,tableFlag = "-12",whichTab = "4")
				   |
				    ---[ItemCopyValue]
					  (tableFlag = "-12",whichTab = "4")
	  Condition 3: CustomItemTableInTable
	  The tagStructure attached to the frame is as below
	  [PRINTsource]__
				     |
				     |
				  [PSTable0]
				 (ID = "-102" ,tableFlag = "1",whichTab = "3",typeId = "-3")
					     |
					     |
						  ----[ItemCopyHeader]
						 |	 (typeID = "-2",tableFlag = "1",whichTab = "4")
					     |
						  ----[ItemCopyValue]
							 (tableFlag = "1",whichTab = "4")


	  So in first and third condition , the TagStructure is of 3 level and in the second contdition
	  the TagStructure is of 2 level.
	  */

	//CA("Inside UpdateTabbedDataWithXML");
	TagList tList;

	InterfacePtr<ITagReader> itagReader
	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
		return ;
	//CA("1");
	tList=itagReader->getTagsFromBox(BoxUIDRef);
	if(tList.size()==0)
	{	
		tList=itagReader->getFrameTags(BoxUIDRef);
		if(tList.size()==0)
			return ;		
	}

	PMString temp="";
	temp.AppendNumber(static_cast<int32>(tList.size()));
	//CA("tList.size() = " + temp);

	
	InterfacePtr<IPMUnknown> unknown(BoxUIDRef, IID_IUNKNOWN);
	if(!unknown){
		//CA("unknown");
	}
	/*UID textFrameUID =  Utils<IFrameUtils>()->GetTextFrameUID(unknown);
	if (textFrameUID == kInvalidUID)
	{
		CA("textFrameUID == kInvalidUID");
		return;
	}*/
	//CS3 Change
	/*InterfacePtr<ITextFrame> textFrame(BoxUIDRef.GetDataBase(), textFrameUID, ITextFrameColumn::kDefaultIID);
	if (textFrame == nil)
		return;*/

	
	//Added by Amit
	InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxUIDRef, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		//CA("graphicFrameHierarchy  == nil");
		return;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy) {
		//CA("multiColumnItemHierarchy  == nil");
		return;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame) {
		//CA("multiColumnItemTextFrame  == nil");
		//CA("Its Not MultiColumn");
		return;
	}
	InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy) {
		//CA("frameItemHierarchy  == nil");
		return;
	}

	InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
	if (!textFrame) {
		//CA("!!!ITextFrameColumn");
		return;
	}

	//To this


	/*UID result = kInvalidUID;
	
	InterfacePtr<IGraphicFrameData> graphicFrameData(BoxUIDRef, UseDefaultIID());
	if (!graphicFrameData) {CA("!graphicFrameData");
		return;
	}
	result = graphicFrameData->GetTextContentUID();

	InterfacePtr<IMultiColumnTextFrame> mcf(BoxUIDRef.GetDataBase(), result, UseDefaultIID());
	if (!mcf) {
		CA("!mcf");
		return;
	}
	CA("mcf");*/

	TextIndex startIndex = textFrame->TextStart();
	TextIndex finishIndex = startIndex + textFrame->TextSpan()-1;

	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());

	/*TextIndex startIndex = mcf->TextStart();
	TextIndex finishIndex = startIndex + mcf->TextSpan()-1;

	InterfacePtr<ITextModel> textModel(mcf->QueryTextModel());*/
	if (textModel == nil)
		return;
	UIDRef txtMdlUIDRef =::GetUIDRef(textModel);

	InterfacePtr<ITextFocusManager> textFocusManager(textModel, UseDefaultIID());
	if (textFocusManager == nil)
		return;
	InterfacePtr<ITextFocus> frameTextFocus(textFocusManager->NewFocus(RangeData(startIndex, finishIndex, RangeData::kLeanForward)));
	if (frameTextFocus == nil)
		return;


	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return;	

	double currentTypeID =-1;

	for(int32 i=0; i<tList.size(); i++)
	{
								
		//Condition 1
		//ItemTableInTabbedTextFormat
		PMString strID = tList[i].tagPtr->GetAttributeValue(WideString("ID"));
		PMString strTableFlag = tList[i].tagPtr->GetAttributeValue(WideString("tableFlag"));
		
		PMString tagData;
		tagData.Append("ID : ");
		tagData.Append(strID);
		tagData.Append("\n");
		tagData.Append("whichTab : ");
		tagData.AppendNumber(tList[i].whichTab);
		tagData.Append("\n");
		tagData.Append("tableFlag : ");
		tagData.Append(strTableFlag);
		tagData.Append("\n");
		tagData.Append("-----------------\n");
		
		
		/*tagData.Append("GlobalData ::SelectedNewAttrID : ");
		tagData.AppendNumber(GlobalData ::SelectedNewAttrID);
		tagData.Append("\n-----------------\n");
		tagData.Append("GlobalData ::SelectedAttributeId : ");
		tagData.AppendNumber(GlobalData ::SelectedAttributeId);
		tagData.Append("\n-----------------\n");
		tagData.Append("GlobalData ::SelectedOldAttrID : ");
		tagData.AppendNumber(GlobalData ::SelectedOldAttrID);*/
		/*tagData.Append("\n tList[i].tableType  :  ");
		tagData.AppendNumber(tList[i].tableType);*/
		//CA(tagData)
		
		if((tList[i].whichTab == 3  ||  tList[i].whichTab == 4) &&  (tList[i].tableType == 1 || tList[i].tableType == 3))
		{
			//CA("AAAAAAAAAAAAAAAAAAAA");
			InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
			if(tableList==nil)
			{
				//CA("tableList==nil");
				continue;
			}
			UpdateTabelDataWithXML(tableList , BoxUIDRef , FileName , pageNo );

		}

		if((tList[i].whichTab == 3 || tList[i].whichTab == 4)&& strID == "-102" && tList[i].tableType == 2 /*&& tList[i].typeId == -3*/)//--------	
		{//customItemTableInsideTable
			//CA("BBBBBBBBBBBBBBBBBBBBB");
			InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
			if(tableList==nil)
			{
				//CA("tableList==nil");
				continue;
			}
			UpdateTabelDataWithXML(tableList,BoxUIDRef,FileName,pageNo);
		}		
		else if((tList[i].whichTab == 3 || tList[i].whichTab == 4) && strID == "-101")
		{	
			//CA("CCCCCCCCCCCCCCCCCCCCCCCCCCCC");
			//start if..1
			//ItemTableInTabbedTextFormat
			//Its a "Items" tag.
			//Get all the child tag of "Items" tag.
			IIDXMLElement * itemsTagXMLElementPtr = tList[i].tagPtr;
			if(itemsTagXMLElementPtr == nil)
			{					
				//CA("itemsTagXMLElementPtr == nil");
				continue;
			}	
			int32 childTagCount = itemsTagXMLElementPtr->GetChildCount();
			for(int32 childTagIndex = 0;childTagIndex < childTagCount;childTagIndex++)
			{
				XMLReference childTagRef = itemsTagXMLElementPtr->GetNthChild(childTagIndex);
				//IIDXMLElement * childTagXMLElementPtr = childTagRef.Instantiate();
				InterfacePtr<IIDXMLElement>childTagXMLElementPtr(childTagRef.Instantiate());
				if(childTagXMLElementPtr == nil)
					continue;
				
				
				PMString displayTagData;
				PMString strID = childTagXMLElementPtr->GetAttributeValue(WideString("ID"));
				double ID = strID.GetAsDouble();
				displayTagData.Append("elementID : " + strID + "\n");				
				
				PMString strTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("typeId"));
				double typeID = strTypeID.GetAsDouble();				
				displayTagData.Append("typeID : " + strTypeID + "\n");						
				
				PMString strIndex = childTagXMLElementPtr->GetAttributeValue(WideString("index"));
				int32 index = strIndex.GetAsNumber();
				displayTagData.Append("index : " + strIndex + "\n");		

				PMString strImgFlag = childTagXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
				int32 imgFlag = strImgFlag.GetAsNumber();
				 displayTagData.Append("imgFlag : " + strImgFlag + "\n");						

				PMString strParentID = childTagXMLElementPtr->GetAttributeValue(WideString("parentID"));
				double parentID = strParentID.GetAsDouble();
				displayTagData.Append("parentID : " + strParentID + "\n");
						
				PMString strParentTypeID = childTagXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
				double parentTypeID = strParentTypeID.GetAsDouble();
				displayTagData.Append("parentTypeID : " + strParentTypeID+ "\n");
						
				PMString strSectionID = childTagXMLElementPtr->GetAttributeValue(WideString("sectionID"));
				double sectionID = strSectionID.GetAsDouble();
				displayTagData.Append("sectionID : " + strSectionID+ "\n");
						
				PMString strTableFlag = childTagXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
				int32 tableFlag = strTableFlag.GetAsNumber();
				displayTagData.Append("tableFlag : " + strTableFlag+ "\n");

				PMString strLanguageID = childTagXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
				double languageID = strLanguageID.GetAsDouble();
				displayTagData.Append("languageID : " + strLanguageID+ "\n");

				PMString strIsAutoResize = childTagXMLElementPtr->GetAttributeValue(WideString("isAutoResize"));
				int32 isAutoResize = strIsAutoResize.GetAsNumber();
				displayTagData.Append("isAutoResize : " + strIsAutoResize + "\n");

				PMString strchildTag = childTagXMLElementPtr->GetAttributeValue(WideString("childTag"));
				int32 childTag = strchildTag.GetAsNumber();				
				displayTagData.Append("childTag : " + strchildTag + "\n");	

				PMString strchildId = childTagXMLElementPtr->GetAttributeValue(WideString("childId"));
				double childId = strchildId.GetAsDouble();				
				displayTagData.Append("childId : " + strchildId + "\n");	

				PMString strisEventField = childTagXMLElementPtr->GetAttributeValue(WideString("isEventField"));
				int32 isEventField = strisEventField.GetAsNumber();				
				displayTagData.Append("isEventField : " + strisEventField);

				//CA("displayTagData	:	"+displayTagData);	

				TagStruct tagInfo;
				tagInfo.elementId = ID;
				tagInfo.tagPtr = childTagXMLElementPtr;
				tagInfo.typeId = typeID;
				tagInfo.languageID = languageID;
				tagInfo.sectionID = sectionID;
				tagInfo.childTag = childTag;
				tagInfo.childId = childId;
				tagInfo.isEventField = isEventField;
						
				if(typeID == -2)
				{//if typeID == -2
//CA("header");							
					//if(IsChangePriceModeSelected == kFalse)
					//{//if(IsChangePriceModeSelected == kFalse)
					//							//CA(tList[i].tagPtr->GetTagString());
					//							//if(tList[i].id != GlobalData::Table_col) //commented bcoz have to use attribute ID
					//	if(ID != GlobalData::attributeID)
					//		continue;													
					//	if(!itagReader->GetUpdatedTag(tagInfo))
					//		return ;
					//	refreshTheItemHeader(textModel,currentTypeID,FileName,pageNo,tagInfo);

					//}//end if(IsChangePriceModeSelected == kFalse) 
					//else if(IsChangePriceModeSelected == kTrue)
					//{//else if (IsChangePriceModeSelected == kTrue)					
					//	if(ID != GlobalData::SelectedOldAttrID)
					//		continue;											
					//	if(!itagReader->GetUpdatedTag(tagInfo))
					//		return;
					//	replaceTheOldItemHeaderWithNewItemHeader(textModel,currentTypeID,FileName,pageNo,tagInfo);

					//}//end else if (IsChangePriceModeSelected == kTrue)
				}//end if typeID == -2
				else
				{//start else
					
					///////////////////////////////////////	Added to show total iems present in catlog					
					//bool16 itemIdFound = kFalse;
					//for(int32 i = 0 ; i < GlobalData::TotalItemPresentList.size() ; i++)
					//{
					//	if(GlobalData::TotalItemPresentList[i] == typeID)
					//		itemIdFound = kTrue;
					//}
					//if(itemIdFound == kFalse)
					//	GlobalData::TotalItemPresentList.push_back(typeID); 
					////////////////////////////////////////

					//if(IsChangePriceModeSelected == kFalse)
					//{
					////CA(tList[i].COLName);
					//	UpdateParentList(BoxUIDRef);

					////if(tList[i].id != GlobalData::Table_col) //commented bcoz attribute id funda comes into picture @vaibhav
					//	if(ID != GlobalData::attributeID)
					//		continue;
					//	if(!itagReader->GetUpdatedTag(tagInfo))
					//		return;
					//	
					//	refreshTheItemAttribute(textModel,currentTypeID,FileName,pageNo,tagInfo);
					//
					//}
					//else if(IsChangePriceModeSelected == kTrue)
					{				
					//CA(tList[i].tagPtr->GetTagString());
					//	UpdateParentList(BoxUIDRef);

					double SelectedNewAttrID = -1;
					bool16 updateThisTag = kFalse;
//CA("1");
					vec_listBoxData::iterator itr = CMediatorClass::vecPtr_listBoxData->begin();

					for(int j = 0; j < isSelected.size();j++)
					{
						if(isSelected[j]){
							//CA("True");
						}
						else{
							//CA("false");
						}
					}
//CA("2");					
					for(int32 index = 0 ;index < CMediatorClass::vecPtr_listBoxData->size();index++,itr++)
					{//CA("3");
						PMString tagData;
						tagData.Append("ID : ");
						tagData.Append(strID);
						tagData.Append("\n");
						tagData.Append("whichTab : ");
						tagData.AppendNumber(tList[i].whichTab);
						tagData.Append("\n");
						tagData.Append("tableFlag : ");
						tagData.Append(strTableFlag);
						tagData.Append("\n");
						tagData.Append("-----------------\n");
						tagData.Append("oldAttributeID : ");
						tagData.AppendNumber((*itr).old_AttributeId);
						tagData.Append("\n-----------------\n");
						tagData.Append("replaceWith_AttributeId : ");
						tagData.AppendNumber((*itr).replaceWith_AttributeId);
						tagData.Append("\n-----------------\n");
						tagData.Append("tList[i].elementId ");
						tagData.AppendNumber(tagInfo.elementId);
						//CA(tagData)



						if(tagInfo.elementId == (*itr).old_AttributeId && isSelected[index])
						{
//								CA("4");
							updateThisTag = kTrue;
							SelectedNewAttrID = (*itr).replaceWith_AttributeId;
							CMediatorClass::currentlySelectedAttributeReplaceName = (*itr).replaceWith_AttributeName;

							PMString SelectedNewAttrIDStr("SelectedNewAttrID = ");
							SelectedNewAttrIDStr.AppendNumber(SelectedNewAttrID);
							SelectedNewAttrIDStr.Append("  , (*itr).replaceWith_AttributeName = ");
							SelectedNewAttrIDStr.Append((*itr).replaceWith_AttributeName);
//								CA(SelectedNewAttrIDStr);

							break;
						}
						//CA("5");
					}
					//CA("outside for");

					if(!updateThisTag)
					{
			//			CA("!UpdateThisTag");
						continue;
					}

					if(!itagReader->GetUpdatedTag(tagInfo))
						return;
					//CA("6");
					replaceOldTagWithNewTag(textModel,currentTypeID,FileName,pageNo,tagInfo,SelectedNewAttrID); 
					//CA("7");


					//if(tList[i].id != GlobalData::OldTable_col) // commented @Vaibhav
					/*	if(ID != GlobalData::SelectedOldAttrID)
							continue;
						if(!itagReader->GetUpdatedTag(tagInfo))
							return;										
						replaceOldTagWithNewTag(textModel,currentTypeID,FileName,pageNo,tagInfo);*/
						
					
				}//end else
						 
				}
			}//end for
		}//end if..1
		//Condition 2
		//CustomItemTableInTabbedTextFormat
		else if(tList[i].whichTab == 4 &&  (strTableFlag == "-12" || strTableFlag == "-1001"))
		{ //ItemCopyAttributes

			//CA("DDDDDDDDDDDDDDDDDDDDDDDDDD");
			if(tList[i].typeId == -2)
			{
				/*if(IsChangePriceModeSelected == kFalse)
				{
					if(tList[i].elementId != GlobalData::attributeID)
						continue;
					if(!itagReader->GetUpdatedTag(tList[i]))
					return;
					refreshTheItemHeader(textModel,currentTypeID,FileName,pageNo,tList[i]);
	
				}
				else if(IsChangePriceModeSelected == kTrue)
				{
					if(tList[i].elementId != GlobalData::SelectedOldAttrID)
						continue;
					if(!itagReader->GetUpdatedTag(tList[i]))
						return;
					replaceTheOldItemHeaderWithNewItemHeader(textModel,currentTypeID,FileName,pageNo,tList[i]);
				
				}*/
			}	
			else
			{
			
				/////////////////////////////////////	Added to show total iems present in catlog					
				/*bool16 itemIdFound = kFalse;
				for(int32 k = 0 ; k < GlobalData::TotalItemPresentList.size() ; k++)
				{
					if(GlobalData::TotalItemPresentList[k] == tList[i].typeId)
						itemIdFound = kTrue;
				}
				if(itemIdFound == kFalse)
					GlobalData::TotalItemPresentList.push_back(tList[i].typeId); */
				//////////////////////////////////////

				//if(IsChangePriceModeSelected == kFalse)
				//{
				//	UpdateParentList(BoxUIDRef);
				//	//if(tList[i].id != GlobalData::Table_col) //commented bcoz attribute id funda comes into picture @vaibhav
				//	if(tList[i].elementId != GlobalData::attributeID)
				//		continue;					
				//	if(!itagReader->GetUpdatedTag(tList[i]))
				//		return;
				//	
				//	refreshTheItemAttribute(textModel,currentTypeID,FileName,pageNo,tList[i]);
	
				//}
				//else if(IsChangePriceModeSelected == kTrue)
				{					
				//	UpdateParentList(BoxUIDRef);
					//if(tList[i].elementId != GlobalData::SelectedOldAttrID)
					//	continue;
					
					double SelectedNewAttrID = -1;
					bool16 updateThisTag = kFalse;
//CA("1");
					vec_listBoxData::iterator itr = CMediatorClass::vecPtr_listBoxData->begin();
//CA("2");					
					for(int32 index = 0 ;index < CMediatorClass::vecPtr_listBoxData->size();index++,itr++)
					{//CA("3");
						PMString tagData;
						tagData.Append("ID : ");
						tagData.Append(strID);
						tagData.Append("\n");
						tagData.Append("whichTab : ");
						tagData.AppendNumber(tList[i].whichTab);
						tagData.Append("\n");
						tagData.Append("tableFlag : ");
						tagData.Append(strTableFlag);
						tagData.Append("\n");
						tagData.Append("-----------------\n");
						tagData.Append("oldAttributeID : ");
						tagData.AppendNumber((*itr).old_AttributeId);
						tagData.Append("\n-----------------\n");
						tagData.Append("replaceWith_AttributeId : ");
						tagData.AppendNumber((*itr).replaceWith_AttributeId);
						tagData.Append("\n-----------------\n");
						tagData.Append("tList[i].elementId ");
						tagData.AppendNumber(tList[i].elementId);
						//CA(tagData)



						if(tList[i].elementId == (*itr).old_AttributeId)
						{//CA("4");

							if(isSelected[index])
								updateThisTag = kTrue;
							else
								updateThisTag = kFalse;

							SelectedNewAttrID = (*itr).replaceWith_AttributeId;
							CMediatorClass::currentlySelectedAttributeReplaceName = (*itr).replaceWith_AttributeName;

							PMString SelectedNewAttrIDStr("SelectedNewAttrID = ");
							SelectedNewAttrIDStr.AppendNumber(SelectedNewAttrID);
							SelectedNewAttrIDStr.Append("  , (*itr).replaceWith_AttributeName = ");
							SelectedNewAttrIDStr.Append((*itr).replaceWith_AttributeName);
							//CA(SelectedNewAttrIDStr);

							break;
						}
						//CA("5");
					}
					//CA("outside for");

					if(!updateThisTag)
					{
//							CA("!UpdateThisTag");
						continue;
					}

					if(!itagReader->GetUpdatedTag(tList[i]))
						return;
//						CA("6");
					replaceOldTagWithNewTag(textModel,currentTypeID,FileName,pageNo,tList[i],SelectedNewAttrID); 
//						CA("7");

				}
			}

		}
		else if(tList[i].whichTab == 3)
		{
			//CA("Add code here for product copy attributes");
			//CA("EEEEEEEEEEEEEEEEEEEEEEEE");

/*testing*/	double SelectedNewAttrID = -1;
			bool16 updateThisTag = kFalse;

			vec_listBoxData::iterator itr = CMediatorClass::vecPtr_listBoxData->begin();

			//int32 noOfChild = tList[i].tagPtr->GetChildCount();
			//PMString asd("vecPtr_listBoxData->size = ");
			//asd.AppendNumber(CMediatorClass::vecPtr_listBoxData->size());
			//CA(asd);
		
			for(int32 index = 0 ;index < CMediatorClass::vecPtr_listBoxData->size();index++,itr++)
			{
				//CA("3");
				PMString tagData;
				tagData.Append("ID : ");
				tagData.Append(strID);
				tagData.Append("\n");
				tagData.Append("whichTab : ");
				tagData.AppendNumber(tList[i].whichTab);
				tagData.Append("\n");
				tagData.Append("tableFlag : ");
				tagData.Append(strTableFlag);
				tagData.Append("\n");
				tagData.Append("-----------------\n");
				tagData.Append("oldAttributeID : ");
				tagData.AppendNumber((*itr).old_AttributeId);
				tagData.Append("\n-----------------\n");
				tagData.Append("replaceWith_AttributeId : ");
				tagData.AppendNumber((*itr).replaceWith_AttributeId);
				tagData.Append("\n-----------------\n");
				tagData.Append("tList[i].elementId ");
				tagData.AppendNumber(tList[i].elementId);
				//CA(tagData)




				if(tList[i].elementId == (*itr).old_AttributeId && isSelected[index])
				{
					//CA("4");
					updateThisTag = kTrue;

					SelectedNewAttrID = (*itr).replaceWith_AttributeId;
					CMediatorClass::currentlySelectedAttributeReplaceName = (*itr).replaceWith_AttributeName;

					PMString SelectedNewAttrIDStr("SelectedNewAttrID = ");
					SelectedNewAttrIDStr.AppendNumber(SelectedNewAttrID);
					SelectedNewAttrIDStr.Append("  , (*itr).replaceWith_AttributeName = ");
					SelectedNewAttrIDStr.Append((*itr).replaceWith_AttributeName);
					//CA(SelectedNewAttrIDStr);

					break;
				}
			}

			if(!updateThisTag)
			{
				//CA("!UpdateThisTag");
				continue;
			}

			if(!itagReader->GetUpdatedTag(tList[i]))
			{
				//CA("returning");
				return;
			}
			
			//CA("CALLING");
			replaceOldProductTagWithNewTag(textModel,currentTypeID,FileName,pageNo,tList[i],SelectedNewAttrID); 
			
		}
		else if(tList[i].whichTab == 5)
		{
			//CA("For Section Attributes");
			//CA("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
			double SelectedNewAttrID = -1;
			bool16 updateThisTag = kFalse;

			vec_listBoxData::iterator itr = CMediatorClass::vecPtr_listBoxData->begin();
		
			for(int32 index = 0 ;index < CMediatorClass::vecPtr_listBoxData->size();index++,itr++)
			{
				//CA("3");
				PMString tagData;
				tagData.Append("ID : ");
				tagData.Append(strID);
				tagData.Append("\n");
				tagData.Append("whichTab : ");
				tagData.AppendNumber(tList[i].whichTab);
				tagData.Append("\n");
				tagData.Append("tableFlag : ");
				tagData.Append(strTableFlag);
				tagData.Append("\n");
				tagData.Append("-----------------\n");
				tagData.Append("oldAttributeID : ");
				tagData.AppendNumber((*itr).old_AttributeId);
				tagData.Append("\n-----------------\n");
				tagData.Append("replaceWith_AttributeId : ");
				tagData.AppendNumber((*itr).replaceWith_AttributeId);
				tagData.Append("\n-----------------\n");
				tagData.Append("tList[i].elementId ");
				tagData.AppendNumber(tList[i].elementId);
				//CA(tagData)



				if(tList[i].elementId == (*itr).old_AttributeId && isSelected[index])
				{
					//CA("4");
					updateThisTag = kTrue;
					SelectedNewAttrID = (*itr).replaceWith_AttributeId;
					CMediatorClass::currentlySelectedAttributeReplaceName = (*itr).replaceWith_AttributeName;

					PMString SelectedNewAttrIDStr("SelectedNewAttrID = ");
					SelectedNewAttrIDStr.AppendNumber(SelectedNewAttrID);
					SelectedNewAttrIDStr.Append("  , (*itr).replaceWith_AttributeName = ");
					SelectedNewAttrIDStr.Append((*itr).replaceWith_AttributeName);
					//CA(SelectedNewAttrIDStr);

					break;
				}
			}

			if(!updateThisTag)
			{
//				CA("!UpdateThisTag");
				continue;
			}

			if(!itagReader->GetUpdatedTag(tList[i]))
				return;
			
			replaceOldProductTagWithNewTag(textModel,currentTypeID,FileName,pageNo,tList[i],SelectedNewAttrID); 

		}

	}
	//------------------
	for(int32 tagIndex = 0 ; tagIndex < tList.size(); tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}	

}


void CommonFunctions::replaceOldTagWithNewTag(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo,double SelectedNewAttrID)
{
	////CA("replaceOldTagWithNewTag");	
	///*bool16 typeIdFound = kFalse;
	//for(int32 i=0;i<GlobalData::TotalItemList.size();i++)
	//{
	//	if(GlobalData::TotalItemList[i]==tagInfo.typeId)
	//	{
	//		typeIdFound = kTrue;
	//		break;
	//	}
	//}
	//if(typeIdFound == kFalse)
	//	GlobalData::TotalItemList.push_back(tagInfo.typeId);*/

	////GlobalData::TotalItemList.push_back(tagInfo.typeId);
	//					
	//PMString entireStory("");
	//bool16 result1 = kFalse;				
	//result1=GetTextstoryFromBox(textModel, tagInfo.startIndex+1, tagInfo.endIndex-1, entireStory);
	////CA("entireStory  =  "+entireStory);
	//bool16 refreshItemFlag = kFalse;
	//if(currentTypeID == tagInfo.typeId)
	//	refreshItemFlag = kFalse;
	//else
	//{
	//	//CA("refreshItemFlag = kTrue;");
	//	refreshItemFlag = kTrue;
	//	currentTypeID = tagInfo.typeId;
	//}
	//InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	//if(ptrIAppFramework == nil)
	//	return;	
	//
	////CA_NUM("tagInfo.languageID",tagInfo.languageID);
	////CA_NUM("SelectedNewAttrID",SelectedNewAttrID);
	//PMString temp("SelectedNewAttrID = " );
	//temp.AppendNumber(SelectedNewAttrID);
	//temp.Append("\n CMediatorClass::replaceWithAttributeLanguageID = ");
	//temp.AppendNumber(CMediatorClass::replaceWithAttributeLanguageID);
	//temp.Append("\n tagInfo.childId   :  ");
	//temp.AppendNumber(tagInfo.childId);
	//temp.Append("\n tagInfo.languageID   :  ");
	//temp.AppendNumber(tagInfo.languageID);
	//temp.Append("\n Section id	:	");
	//temp.AppendNumber(tagInfo.sectionID);
	//temp.Append("\n Type id	:	");
	//temp.AppendNumber(tagInfo.typeId);
	//temp.Append("\n child Tag	:	");
	//temp.AppendNumber(tagInfo.childTag);
	////tagInfo.childTag
	////CA(temp);

	////--------
	//PMString DBStory = "";
	//if(tagInfo.childTag != 1)
	//{
	//	//CA("tagInfo.childTag != 1");
	//	if(tagInfo.isEventField == 1)
	//	{
	//		//CA("tagInfo.isEventField");
	//		VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(tagInfo.typeId,SelectedNewAttrID,tagInfo.sectionID);
	//		if(vecPtr != NULL)
	//			if(vecPtr->size()> 0)
	//				DBStory = vecPtr->at(0).getObjectValue();	

	//		//CA("itemAttributeValue = " + itemAttributeValue);
	//	}
	//	else if(SelectedNewAttrID == -401 || SelectedNewAttrID == -402 || SelectedNewAttrID == -403){
	//		DBStory = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID);
	//	}
	//	else if(tagInfo.header == 1){
	//		//CA("ASD");
	//		DBStory=ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID);
	//	}
	//	else if(tagInfo.whichTab == 3)
	//	{
	//		//CA("tagInfo.whichTab == 3");
	//		//int32 objectId = strParentID.GetAsNumber();
	//		refreshItemFlag = kFalse;
	//		if(SelectedNewAttrID == -121)
	//		{
	//			VectorScreenTableInfoPtr tableInfo = NULL;
	//			CObjectTableValue oTableValue;
	//			PMStringVecPtr otherLanguageValue;

	//			// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
	//			//CA("Not ONEsource");
	//			do
	//			{
	//				//tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tagInfo.sectionID, tagInfo.typeId);
	//				//CA("Going for TABLEINFO");
	//				//PMString str2("PARAM PARENT ID = ");
	//				//str2.AppendNumber(tagInfo.parentId);
	//				//str2.Append("\n PATAM SECTION ID");
	//				//str2.AppendNumber(tagInfo.sectionID);
	//				//CA(str2);
	//				//str2.clear();
	//				tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tagInfo.sectionID, tagInfo.parentId, refreshItemFlag);
	//				if(!tableInfo)
	//				{
	//					//CA("!tableInfo");
	//					ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
	//					break;
	//				}
	//				if(tableInfo->size()==0)
	//				{
	//					//CA(" tableInfo->size()==0");
	//					ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
	//					break;
	//				}
	//				
	//				VectorScreenTableInfoValue::iterator it;
	//				PMStringVec::iterator strIt;
	//				bool16 typeidFound=kFalse;
	//				PMString asd("");
	//				//asd.AppendNumber((*it).getTableID());
	//				//CA(asd);
	//				int32 tableId = 0;
	//				
	//				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	//				{
	//					//CA("for Loop");
	//					oTableValue = *it;
	//					//asd.Append("Table ID = ");
	//					//asd.AppendNumber(oTableValue.getTableID());
	//					//asd.Append("\n tagInfo.typeId =  ");
	//					//asd.AppendNumber(tagInfo.typeId);
	//					//CA(asd);
	//					//asd.clear();
	//					if(oTableValue.getTableID() ==  tagInfo.typeId)
	//					{   
	//						//CA("typeidFound=kTrue;");
	//						typeidFound=kTrue;
	//						tableId = oTableValue.getTableID();
	//						break;
	//					}
	//				}
	//				
	//				if(typeidFound)
	//				{
	//					//CA("typeidFound");
	//					//DBStory = oTableValue.getTableName();
	//					//refreshItemFlag = kFalse;
	//					otherLanguageValue = ptrIAppFramework->GetProjectProducts_getListTableNameByLanguageId(tagInfo.sectionID, tagInfo.parentId , tableId, CMediatorClass::replaceWithAttributeLanguageID, refreshItemFlag);
	//					if(otherLanguageValue == NULL)
	//					{
	//						//CA("otherLanguageValue == NULL");
	//					}
	//					else
	//					{
	//						//CA("HHHHHHHHHHHHHHH");
	//						if(otherLanguageValue->size() == 0)
	//						{
	//							//CA("SIZE = 0");
	//						}
	//						for(strIt = otherLanguageValue->begin(); strIt != otherLanguageValue->end(); strIt++)
	//						{
	//							DBStory = *(strIt);
	//							//CA("----------> " + DBStory);
	//						}
	//					}
	//					

	//					
	//				}
	//				else
	//				{
	//					//CA("NOT FOUND");
	//					ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
	//				}

	//			}while(0);

	//			if(otherLanguageValue)
	//			{
	//				//CA("SSSS");
	//				//delete otherLanguageValue;
	//				otherLanguageValue = NULL;
	//			}
	//			if(tableInfo)
	//			{
	//				//CA("DELETIN TABLE INFO");
	//				delete tableInfo;
	//				tableInfo = NULL;
	//				ptrIAppFramework->clearAllStaticObjects();
	//			}
	//		}
	//		else
	//		{
	//			//CA("YYYYYYYYYYYYYYYYYYYYYY2222222222222222222222");
	//			DBStory = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tagInfo.parentId, SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID,refreshItemFlag);
	//		}
	//		
	//	}
	//	else{
	//		//CA("ZZZZZZZZZZZZZZZZZZZZZZ");
	//		DBStory =ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID,refreshItemFlag); //og
	//		
	//	}
	//}
	//else
	//{
	//	//CA("tagInfo.childTag == 1");
	//	if(tagInfo.isEventField == 1)
	//	{
	//		//CA("tagInfo.isEventField 2");
	//		VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(tagInfo.childId,SelectedNewAttrID,tagInfo.sectionID);
	//		if(vecPtr != NULL)
	//			if(vecPtr->size()> 0)
	//				DBStory = vecPtr->at(0).getObjectValue();			
	//	}
	//	else if(SelectedNewAttrID == -401 || SelectedNewAttrID == -402 || SelectedNewAttrID == -403){
	//		DBStory = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.childId,SelectedNewAttrID, CMediatorClass::replaceWithAttributeLanguageID);
	//	}
	//	else if(tagInfo.header == 1){
	//		DBStory=ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID);
	//	}
	//	else if(tagInfo.whichTab == 3)
	//	{
	//		//CA("ONE");
	//		//CA("tagInfo.whichTab == 3");
	//		//int32 objectId = strParentID.GetAsNumber();
	//		refreshItemFlag = kFalse;
	//		if(SelectedNewAttrID == -121)
	//		{
	//			//CA(":)");
	//			VectorScreenTableInfoPtr tableInfo = NULL;
	//			CObjectTableValue oTableValue;
	//			PMStringVecPtr otherLanguageValue;

	//			// For Publication to get all information about Table such as typeid,tablename,tableid,istranspose,printheader,tableheader etc..
	//			//CA("Not ONEsource");
	//			do
	//			{
	//				//tableInfo= ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tagInfo.sectionID, tagInfo.typeId);
	//				//CA("Going for TABLEINFO");
	//				//PMString str2("PARAM PARENT ID = ");
	//				//str2.AppendNumber(tagInfo.parentId);
	//				//str2.Append("\n PATAM SECTION ID");
	//				//str2.AppendNumber(tagInfo.sectionID);
	//				//CA(str2);
	//				//str2.clear();
	//				tableInfo = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(tagInfo.sectionID, tagInfo.parentId, refreshItemFlag);
	//				if(!tableInfo)
	//				{
	//					//CA("!tableInfo");
	//					ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!tableInfo");
	//					break;
	//				}
	//				if(tableInfo->size()==0)
	//				{
	//					//CA(" tableInfo->size()==0");
	//					ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::tableInfo->size()==0");
	//					break;
	//				}
	//				
	//				VectorScreenTableInfoValue::iterator it;
	//				PMStringVec::iterator strIt;
	//				bool16 typeidFound=kFalse;
	//				PMString asd("");
	//				//asd.AppendNumber((*it).getTableID());
	//				//CA(asd);
	//				int32 tableId = 0;
	//				
	//				for(it = tableInfo->begin(); it!=tableInfo->end(); it++)
	//				{
	//					//CA("for Loop");
	//					oTableValue = *it;
	//					//asd.Append("Table ID = ");
	//					//asd.AppendNumber(oTableValue.getTableID());
	//					//asd.Append("\n tagInfo.typeId =  ");
	//					//asd.AppendNumber(tagInfo.typeId);
	//					//CA(asd);
	//					//asd.clear();
	//					if(oTableValue.getTableID() ==  tagInfo.childId)
	//					{   
	//						//CA("typeidFound=kTrue;");
	//						typeidFound=kTrue;
	//						tableId = oTableValue.getTableID();
	//						break;
	//					}
	//				}
	//				
	//				if(typeidFound)
	//				{
	//					//CA("typeidFound");
	//					//DBStory = oTableValue.getTableName();
	//					refreshItemFlag = kFalse;
	//					otherLanguageValue = ptrIAppFramework->GetProjectProducts_getListTableNameByLanguageId(tagInfo.sectionID, tagInfo.parentId , tableId, CMediatorClass::replaceWithAttributeLanguageID, refreshItemFlag);
	//					if(otherLanguageValue == NULL)
	//					{
	//						//CA("otherLanguageValue == NULL");
	//					}
	//					else
	//					{
	//						//CA("HHHHHHHHHHHHHHH");
	//						if(otherLanguageValue->size() == 0)
	//						{
	//							//CA("SIZE = 0");
	//						}
	//						for(strIt = otherLanguageValue->begin(); strIt != otherLanguageValue->end(); strIt++)
	//						{
	//							DBStory = *(strIt);
	//							//CA("----------> " + DBStory);
	//						}
	//					}
	//					
	//				}
	//				else
	//				{
	//					//CA("NOT FOUND");
	//					ptrIAppFramework->LogError("AP46_DataSprayer::CDataSprayer::sprayForThisBox::!typeidFound");
	//					
	//				}

	//			}while(0);

	//			if(otherLanguageValue)
	//			{
	//				//CA("SSSS");
	//				//delete otherLanguageValue;
	//				otherLanguageValue = NULL;
	//			}
	//			if(tableInfo)
	//			{
	//				//CA("DELETIN TABLE INFO");
	//				delete tableInfo;
	//				tableInfo = NULL;
	//				ptrIAppFramework->clearAllStaticObjects();
	//			}
	//		}
	//		else
	//		{
	//			//CA("YYYYYYYYYYYYYYYYYYYYYY11111111111111111111");
	//			DBStory = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tagInfo.parentId, SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID,kTrue);
	//		}
	//		
	//	}
	//	else{
	//		DBStory =ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.childId,SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID,refreshItemFlag);
	//	}
	//}
	////CA("DBStory = " + DBStory);
	////PMString DBStory =ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID,refreshItemFlag);
	//
	////CA("OldStory = " + entireStory + ", NewStory =  " + DBStory);

	////**Aded By sachin Sharma
	////CAttributeModel cAttributeModel = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(1,tagInfo.languageID);
	////PMString baseNumber = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,cAttributeModel.getAttribute_id(),tagInfo.languageID,refreshItemFlag);

	//CMediatorClass::noOfItemFieldsUpdated++;
	//if(DBStory.IsEqual(entireStory)==kFalse)
	//{
	//	
	//	//SKUData tempData;
	//	//metadata::Clock a;
	//	//metadata::DateTime dt;
	//	//a.timestamp(dt);

	//	///*PMString Date;
	//	//Date.AppendNumber(dt.mday);
	//	//Date.Append("-");
	//	//Date.AppendNumber(dt.month);
	//	//Date.Append("-");
	//	//Date.AppendNumber(dt.year);*/
	//	//
	//	//PMString Date;
	//	//Date.AppendNumber(dt.month);
	//	//Date.Append("-");
	//	//Date.AppendNumber(dt.mday);
	//	//Date.Append("-");
	//	//Date.AppendNumber(dt.year);

	//	//tempData.OriginalData.Append(entireStory);
	//	//tempData.ChangedData.Append(DBStory);
	//	//tempData.DocPath.Append(FileName);
	//	//tempData.Date.Append(Date);
	//	//tempData.PageNo=pageNo;
	//	//tempData.ColumName.Append(GlobalData::OldTable_col);
	//	//tempData.ColumName.Append(" >> ");
	//	//tempData.ColumName.Append(GlobalData::NewTable_col);
	//	//tempData.ItemID = tagInfo.typeId;
	//	//
	//	////**** Now Getting the AttriBute Name from AttributeID		
	//	//PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tagInfo.elementId,tagInfo.languageID);
	//	//tempData.attributeName = dispname;		
	//	//tempData.baseNumber =baseNumber;
	//	////*******Up To Here	

	//	//GlobalData::skuDataList.push_back(tempData);
	//	VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
	//	VectorHtmlTrackerValue vectorObj ;
	//	vectorHtmlTrackerSPTBPtr = &vectorObj;
	//	
	//	
	//	int32 NoofChar1 = DBStory.NumUTF16TextChars();
	//	//WideString* myText=new WideString(DBStory.GrabCString());
	//	int32 tStart = tagInfo.startIndex+1;;
	//	int32 tEnd = tagInfo.endIndex-tStart;

	//	PMString temp("tStart = ");
	//	temp.AppendNumber(tStart);
	//	temp.Append( ", tEnd = ");
	//	temp.AppendNumber(tEnd);
	//	temp.Append( ", tagInfo.startIndex = ");
	//	temp.AppendNumber(tagInfo.startIndex);
	//	temp.Append( ", tagInfo.endIndex = ");
	//	temp.AppendNumber(tagInfo.endIndex);
	//	//CA(temp);


	//	PMString textToInsert("");
	//	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	//	if(!iConverter)
	//	{
	//		textToInsert=DBStory;					
	//	}
	//	else
	//	{
	//		//textToInsert=iConverter->translateString(dispName);
	//		vectorHtmlTrackerSPTBPtr->clear();
	//		textToInsert=iConverter->translateStringNew(DBStory, vectorHtmlTrackerSPTBPtr);
	//		//CA("textToInsert = " + textToInsert);
	//	}

	//	/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
	//	ReplaceText(textModel, tStart, tEnd+1, insertText);
	//	//ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos + 1, insertText);
	//	


	//	//if(iConverter)
	//	//{	
	//	//	//CA("iConverter");	
	//	//	iConverter->ChangeQutationMarkONOFFState(kFalse);
	//	//	//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
	//	//	ErrorCode Err = textModel->Replace(tStart, tEnd, myText);				
	//	//	iConverter->ChangeQutationMarkONOFFState(kTrue);
	//	//}	
	//	//else
	//	//{	
	//	//	CA("!!!!!!!!!iConverter");	
	//	//	//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
	//	//	ErrorCode Err = textModel->Replace(tStart, tEnd, myText);
	//	//}

	//	//CA("before CommonFunctions::updateTheTagWithModifiedAttributeValues");
	//	updateTheTagWithModifiedAttributeValues(tagInfo.tagPtr,SelectedNewAttrID);
	//	//CA("After CommonFunctions::updateTheTagWithModifiedAttributeValues");
	//}
	//else
	//{
	//	updateTheTagWithModifiedAttributeValues(tagInfo.tagPtr,SelectedNewAttrID);
	//}

	////CA("outside if");
}

bool16 CommonFunctions::GetTextstoryFromBox(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story)
{	
	TextIterator begin(iModel, startIndex);
	TextIterator end(iModel, finishIndex);
	
	for (TextIterator iter = begin; iter <= end; iter++)
	{	
		const textchar characterCode = (*iter).GetValue();
		char buf;
		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
		story.Append(buf);
	}	
	return kTrue;
}

void CommonFunctions::updateTheTagWithModifiedAttributeValues(IIDXMLElement * &XMLElementTagPtr,double newAttrID)
{
	//CA("inside CommonFunctions::updateTheTagWithModifiedAttributeValues");
					
	PMString ID;
	ID.AppendNumber(PMReal(newAttrID));
	PMString tagName;
	tagName = prepareTagName(CMediatorClass::currentlySelectedAttributeReplaceName);
//CA(tagName + "   ,  " + CMediatorClass::currentlySelectedAttributeReplaceName);
	XMLElementTagPtr->SetTag(/*tagName*/XMLElementTagPtr->GetTagUID()); //cS3/cS4
	XMLElementTagPtr->SetAttributeValue(WideString("ID"),WideString(ID));

	PMString LanguageID;
	LanguageID.AppendNumber(PMReal(CMediatorClass::replaceWithAttributeLanguageID));
	XMLElementTagPtr->SetAttributeValue(WideString("LanguageID"),WideString(LanguageID));

//CA("2");

}

void CommonFunctions::UpdateTabelDataWithXML(InterfacePtr<ITableModelList>& tableList,const UIDRef& TextUIDRef,PMString FileName,int32 pageNo)
{	
	//CA("Inside CommonFunctions::UpdateTabelDataWithXML");
			
do{
	int32	tableIndex = tableList->GetModelCount() ;

	for(int32 p=0; p<tableIndex; p++)
	{
		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(p));
		if(tableModel == nil) 
		{
			continue;
		}

		//start 17July
		InterfacePtr<ITableTextContent> tableTextContent(tableModel,UseDefaultIID());
		if(tableTextContent == nil)
		{
			//CA("tableTextContent == nil");
			continue;
		}
		InterfacePtr<ITextModel> tableTextModel(tableTextContent->QueryTextModel());
		if(tableTextModel == nil)
		{
			//CA("tableTextModel == nil");
			continue;
		}
		//end 17July

		UIDRef tableUIDRef(::GetUIDRef(tableModel));
		
		ColRange col = tableModel->GetTotalCols();
		RowRange row = tableModel->GetTotalRows();

		int32 rows = row.count;
		int32 cols = col.count;
		PMString b("rows  :  ");
		b.AppendNumber(rows);
		b.Append("\n cols  :  ");
		b.AppendNumber(cols);
		//CA(b);

		/*
		InterfacePtr<ISelectionManager> iSelectionManager(Utils<ISelectionUtils>()->QueryActiveSelection());
		if(!iSelectionManager)
		{
			CA("Source Selection Manager is null");
			return;
		}

		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(!pTextSel)
		{
			CA("Source IConcrete Selection is null");
			return;
		}

		InterfacePtr<ITableTextSelection>tblTxtSel(pTextSel,UseDefaultIID());
		if(!tblTxtSel)
		{
			CA(" Source Table Text Selection is null");
			return;
		}
		*/

		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==nil)
		{
			//CA("Err: invalid interface pointer ITableCommands");
			break;
		}

		InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader)
			return ;

		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj){// CA("!iTableUtlObj");
			return ;
		}
		
		TagList tList=itagReader->getTagsFromBox(TextUIDRef);	
		IIDXMLElement* XMLElementPtr =  NULL; 
		TagStruct tStruct;
		if(tList.size()==0)
			return;

		for(int32 r=0; r<tList.size(); r++)
		{
			XMLElementPtr = tList[r].tagPtr;
			tStruct = tList[r];
			XMLContentReference contentRef = XMLElementPtr->GetContentReference();
			if(contentRef.IsTable()) {
				//CA("Table Content");
			} else if(contentRef.IsTableCell()) {
				//CA("Table Cell");
			} 

			UIDRef ContentRef = contentRef.GetUIDRef();
			if( ContentRef != tableUIDRef)
			{
				//CA("ContentRef != tableUIDRef");	
				continue;
			}
			else
			{
				break;
			}
		}
		/*if(tStruct.isTablePresent == kFalse)
		{
			CA("Table Tag not Found");
			return;
		}*/
			
		//UIDRef tableRef(::GetUIDRef(tableModel));
		//IIDXMLElement* XMLElementPtr =  tStruct.tagPtr; 		
		//PMString Maintag = XMLElementPtr->GetTagString();
		//CA(Maintag);
		//int MainelementCount=XMLElementPtr->GetChildCount();
		//if(MainelementCount > 0)
		//{
		//	XMLReference MainelementXMLref=XMLElementPtr->GetNthChild(0);
		//	IIDXMLElement * TableElement=MainelementXMLref.Instantiate();
		//	if(TableElement==nil)
		//		break;
		//	//CA("22");
		//	PMString TabletagName=TableElement->GetTagString();
		//	CA(TabletagName);	

		
		
		
		IIDXMLElement * TableElement = XMLElementPtr; //og
		//InterfacePtr<IIDXMLElement> TableElement(XMLElementPtr); //added by avinash
		XMLContentReference contentRef = TableElement->GetContentReference();
		if(contentRef.IsTable()) {
			//CA("Table Content");
		} else if(contentRef.IsTableCell()) {
			//CA("Table Cell");
		} 
		UIDRef ContentRef = contentRef.GetUIDRef();
		if( ContentRef != tableUIDRef)
		{
			CA("ContentRef != tableUIDRef");	
			return;
		}
		
		//IXMLReferenceData* xmlRefData = contentRef.Instantiate(); //og
		InterfacePtr<IXMLReferenceData> xmlRefData(contentRef.Instantiate()); 
		XMLReference xmlRef=xmlRefData->GetReference();
		//IIDXMLElement *xmlElement = xmlRef.Instantiate(); //og
		InterfacePtr<IIDXMLElement> xmlElement(xmlRef.Instantiate());
		int32 elementCount=xmlElement->GetChildCount();
		
		PMString ORgTagString = xmlElement->GetTagString();
		//CA("Content Tag");
		//CA("Content Tag++++++++++++++++++++=s = " + ORgTagString);		
			
		for(int32 i1=0; i1<elementCount; i1++)
		{
			//CA("21");		
			XMLReference elementXMLref=xmlElement->GetNthChild(i1);
			//IIDXMLElement * childElement=elementXMLref.Instantiate();
			InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
			if(childElement==nil)
				continue;
			//CA("22");
			PMString ChildtagName=childElement->GetTagString();
			//CA("** ---> " + ChildtagName);
			//if(tagName == ChildtagName){  //CA("tagName == ChildtagName");
			//	existingTagUID = childElement->GetTagUID();
			//	break;
			//}
			int32 elementCount1=childElement->GetChildCount();
			/*PMString ASD("elementCount1 for Cell : ");
			ASD.AppendNumber(elementCount1);
			CA(ASD);*/
			for(int32 i=0; i<elementCount1; i++)
			{
				XMLReference elementXMLref1=childElement->GetNthChild(i);
				//IIDXMLElement * childElement1=elementXMLref1.Instantiate();
				InterfacePtr<IIDXMLElement> childElement1(elementXMLref1.Instantiate());
				if(childElement1==nil)
					continue;
				//check for the tableFlag
				PMString strTableFlag =childElement1->GetAttributeValue(WideString("tableFlag"));
				if(strTableFlag != "0" && strTableFlag != "-1001")
				{
					//CA("strTableFlag != 0 && strTableFlag != -1001");
					continue;
				}			
				PMString ChileEleName = childElement1->GetTagString();
				TagStruct tInfo;
				int32 attribCount=childElement1->GetAttributeCount();
				TextIndex sIndex=0, eIndex=0;
				Utils<IXMLUtils>()->GetElementIndices(childElement1, &sIndex, &eIndex);
				
				tInfo. startIndex=sIndex;
				tInfo.endIndex=eIndex;
				tInfo.tagPtr=childElement1;
					
					
				for(int32 j=0; j<attribCount; j++)
				{
					PMString attribName=childElement1->GetAttributeNameAt(j);
					PMString attribVal=childElement1->GetAttributeValue(WideString(attribName));
					itagReader->getCorrespondingTagAttributes(attribName, attribVal, tInfo);
					
				}

				tInfo.curBoxUIDRef=TextUIDRef;
				double currentTypeID = -1;

				//mainPriceUpdate(tableTextModel,currentTypeID,FileName,pageNo,tInfo);
				
				if(tInfo.typeId == -2)
				{//CA("tInfo.typeId == -2");
					//CA(GlobalData::Table_col);
					//CA(tInfo.COLName);
					//if(IsChangePriceModeSelected == kFalse)
					//{
					//	//if(tInfo.id != GlobalData::Table_col)  // commented @Vaibhav
					//	if(tInfo.elementId != GlobalData::attributeID)
					//		continue;
					//	refreshTheItemHeader(tableTextModel,currentTypeID,FileName,pageNo,tInfo);


					//}
					//else
					//{
					//	//if(tInfo.id != GlobalData::OldTable_col) // commented @vaibhav
					//	if(tInfo.elementId != GlobalData::SelectedOldAttrID)
					//		continue;
					//	replaceTheOldItemHeaderWithNewItemHeader(tableTextModel,currentTypeID,FileName,pageNo,tInfo);

					//}

					

				}
				//cell is not a header cell
				else
				{
					//CA("tInfo.typeId != -2");
					///////////////////////////////////////	Added to show total iems present in catlog					
					//bool16 itemIdFound = kFalse;
					//for(int32 i = 0 ; i < GlobalData::TotalItemPresentList.size() ; i++)
					//{
					//	if(GlobalData::TotalItemPresentList[i] == tInfo.typeId)
					//		itemIdFound = kTrue;
					//}
					//if(itemIdFound == kFalse)
					//	GlobalData::TotalItemPresentList.push_back(tInfo.typeId); 
					////////////////////////////////////////

					//if(IsChangePriceModeSelected == kFalse)
					//{
					//	
					//	UpdateParentList(TextUIDRef);
					//	//if(tInfo.id != GlobalData::Table_col) // commented @ Vaibhav
					//	if(tInfo.elementId != GlobalData::attributeID)
					//		continue;
					//	
					//	refreshTheItemAttribute(tableTextModel,currentTypeID,FileName,pageNo,tInfo);
					//	
					//}
					//else if(IsChangePriceModeSelected == kTrue)
					//{
						
					//	UpdateParentList(TextUIDRef);

						//if(tInfo.id != GlobalData::OldTable_col) // commented @Vaibhav

					double SelectedNewAttrID = -1;
					bool16 updateThisTag = kFalse;
//CA("1");
					vec_listBoxData::iterator itr = CMediatorClass::vecPtr_listBoxData->begin();
//CA("2");					
					for(int32 index = 0 ;index < CMediatorClass::vecPtr_listBoxData->size();index++,itr++)
					{//CA("3");
						PMString tagData;
						tagData.Append("ID : ");
						tagData.AppendNumber(tInfo.elementId);
						tagData.Append("\n");
						tagData.Append("whichTab : ");
						tagData.AppendNumber(tList[i].whichTab);
						tagData.Append("\n");
						tagData.Append("tableFlag : ");
						tagData.Append(strTableFlag);
						tagData.Append("\n");
						tagData.Append("-----------------\n");
						tagData.Append("oldAttributeID : ");
						tagData.AppendNumber((*itr).old_AttributeId);
						tagData.Append("\n-----------------\n");
						tagData.Append("replaceWith_AttributeId : ");
						tagData.AppendNumber((*itr).replaceWith_AttributeId);
						tagData.Append("\n-----------------\n");
						tagData.Append("tList[i].elementId ");
						tagData.AppendNumber(tInfo.elementId);
						//CA(tagData)

						if(tInfo.elementId == (*itr).old_AttributeId  && isSelected[index])
						{
							//CA("4");
							updateThisTag = kTrue;
							SelectedNewAttrID = (*itr).replaceWith_AttributeId;
							CMediatorClass::currentlySelectedAttributeReplaceName = (*itr).replaceWith_AttributeName;

							PMString SelectedNewAttrIDStr("SelectedNewAttrID = ");
							SelectedNewAttrIDStr.AppendNumber(SelectedNewAttrID);
							SelectedNewAttrIDStr.Append("  , (*itr).replaceWith_AttributeName = ");
							SelectedNewAttrIDStr.Append((*itr).replaceWith_AttributeName);
							//CA(SelectedNewAttrIDStr);

							break;
						}
						//CA("5");
					}
					//CA("outside for");

					if(!updateThisTag)
					{
//						CA("!UpdateThisTag");
						continue;
					}

					//if(!itagReader->GetUpdatedTag(tList[i])) //og
					if(!itagReader->GetUpdatedTag(tInfo))
					{
						return;	//og
						//CA("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&7");
						//continue; 
					}
					//CA("6666666666666666666666666");
					replaceOldTagWithNewTag(tableTextModel,currentTypeID,FileName,pageNo,tInfo,SelectedNewAttrID); //og	
					//CA("777777777777777777777777");
						
				}					
			}		
		}			
		
		//CA("EXITINGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG");
		//------------------
		for(int32 tagIndex = 0 ; tagIndex < tList.size(); tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	}
 }while(kFalse);
}

void CommonFunctions::replaceOldProductTagWithNewTag(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo,double SelectedNewAttrID)
{
//	//CA("replaceOldProductTagWithNewTag");	
//	/*bool16 typeIdFound = kFalse;
//	for(int32 i=0;i<GlobalData::TotalItemList.size();i++)
//	{
//		if(GlobalData::TotalItemList[i]==tagInfo.typeId)
//		{
//			typeIdFound = kTrue;
//			break;
//		}
//	}
//	if(typeIdFound == kFalse)
//		GlobalData::TotalItemList.push_back(tagInfo.typeId);*/
//
//	//GlobalData::TotalItemList.push_back(tagInfo.typeId);
//						
//	PMString entireStory("");
//	bool16 result1 = kFalse;				
//	result1=GetTextstoryFromBox(textModel, tagInfo.startIndex+1, tagInfo.endIndex-1, entireStory);
////	CA(entireStory);
//
//	bool16 refreshItemFlag = kFalse;
//	if(currentTypeID == tagInfo.typeId)
//		refreshItemFlag = kFalse;
//	else
//	{
//		refreshItemFlag = kTrue;
//		currentTypeID = tagInfo.typeId;
//	}
//	
//	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
//	if(ptrIAppFramework == nil)
//		return;	
//	
//	//CA_NUM("tagInfo.languageID",tagInfo.languageID);
//	//CA_NUM("typeID",tagInfo.typeId);
//	//PMString DBStory = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,GlobalData::SelectedNewAttrID,tagInfo.languageID,refreshItemFlag);
//	//PMString DBStory = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID,refreshItemFlag);
//	
//	PMString DBStory("");
//	if(tagInfo.whichTab == 5)
//	{
//		//CA("tagInfo.whichTab == 5");
//		if(tagInfo.colno != -1)
//		{	
//			//CA("tagInfo.colno != -1 ");
//			int32 ProductTypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PRODUCT_LEVEL");
//			int32 ItemTypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("ITEM_LEVEL");
//							
//			if(tagInfo.parentTypeID == ProductTypeId)
//			{
//				//CA("tagInfo.typeId == ProductTypeId");
// 				DBStory = ptrIAppFramework->getCategoryCopyAttributeValue(tagInfo.parentId, tagInfo.colno, SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID, 1 );
//			}
//			else
//			{
//				//CA("!! tagInfo.typeId == ProductTypeId");
//				DBStory = ptrIAppFramework->getCategoryCopyAttributeValue(tagInfo.parentId, tagInfo.colno, SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID, 0 );
//			}
//		}
//		else
//		{
//			//CA("slugInfo.colno == -1 ");
//			
//			CPubModel CPubObjectModel = ptrIAppFramework->getpubModelByPubID(tagInfo.sectionID,CMediatorClass::replaceWithAttributeLanguageID);
//			if(SelectedNewAttrID < 0) // for static attribute Spray
//			{
//				//CA("attributeID < 0 ");
//				int32 result = ptrIAppFramework->StaticAttributeIDs_CheckPublicationID(SelectedNewAttrID); // for getting whether it is PR,SEC,SUBSEC
//				int32 InputID = -1;			
//
//				if(result == 1){
//			
//					InputID = CPubObjectModel.getRootID();
//
//				}
//				else if(result == 2)
//					InputID = tagInfo.sectionID;
//				else if(result == 3)
//					InputID = tagInfo.sectionID;
//
//				if(InputID != 0)					
//					DBStory = ptrIAppFramework->PUBModel_getAttributeValueByLanguageID(InputID,SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID,kTrue);
//				
//				//CA(" DBStory  : "+DBStory);
//			}
//			else  // for spraying Publication, Section & Subsection level copy attributes.
//			{
//				//CA("for spraying Publication, Section & Subsection level copy attributes");
//				//CA("attributeID > 0");
//				int32 PubtypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PUB_PUBLICATION_TYPE");
//				int32 SecTypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PUB_SECTION_TYPE");
//				int32 SubSecTypeId = ptrIAppFramework->TYPEMngr_getObjectTypeID("PUB_SUBSECTION_TYPE");
//				
//				int32 InputID = -1;
//				
//				PMString asd("INPUT ID =  ");
//				if(tagInfo.typeId == PubtypeId)
//				{
//					//CA("111111111111111");
//					InputID = CPubObjectModel.getRootID();/*CurrentPublicationID;*/
//				}
//				else if(tagInfo.typeId == SecTypeId)
//				{
//					//CA("22222222222222");
//					InputID = tagInfo.sectionID;/*CurrentSectionID;*/ //og
//					//InputID = tagInfo.parentId;		//added by avinash
//				}
//				else if(tagInfo.typeId == SubSecTypeId)
//				{
//					//CA("333333333333333333");
//					InputID = tagInfo.sectionID;//CurrentSubSectionID;
//				}
//				//CA_NUM(" InputID : " , InputID );
//				//CA_NUM(" SelectedNewAttrID = " , SelectedNewAttrID);
//				//CA_NUM("CMediatorClass::replaceWithAttributeLanguageID = " , CMediatorClass::replaceWithAttributeLanguageID);
//				
//				asd.AppendNumber(InputID);
//				asd.Append("\nSelectedNewAttrID = ");
//				asd.AppendNumber(SelectedNewAttrID);
//				asd.Append("\nlanguageID = ");
//				asd.AppendNumber(CMediatorClass::replaceWithAttributeLanguageID);
//				//CA(asd);
//
//				//CA("-----------> getPubSecSubsecCopyAttributeValue");
//				DBStory = ptrIAppFramework->getPubSecSubsecCopyAttributeValue(InputID, SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID);
//				//CA(" DBStory  :: "+DBStory);
//			}
//		}
//		//CA(" DBStory  :: "+DBStory);
//
//	}
//	//if(tagInfo.typeId == -2)
//	else if(tagInfo.header == 1){
//		//CA("tagInfo.header == 1")
//		CElementModel cElementModelObj;
//		bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(SelectedNewAttrID,cElementModelObj);
//		//CA_NUM("result ",result);
//		
//		if(result)
//			DBStory = cElementModelObj.getDisplayName();
//	}	
//	else
//	{
//		//CA("ELSE")
//		DBStory = ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(tagInfo.parentId,SelectedNewAttrID,CMediatorClass::replaceWithAttributeLanguageID,kFalse);
//		//CA("DBStory = " +DBStory);
//	}
//	//CA("DBStory = " + DBStory);
////	CA("OldStory = " + entireStory + ", NewStory =  " + DBStory);
//
//	//**Aded By sachin Sharma
//	//CAttributeModel cAttributeModel = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(1,tagInfo.languageID);
//	//PMString baseNumber = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,cAttributeModel.getAttribute_id(),tagInfo.languageID,refreshItemFlag);
//	
//	CMediatorClass::noOfProductFieldsUpdated++;
//	if(DBStory.IsEqual(entireStory)==kFalse)
//	{
//		//CA("DBStory.IsEqual(entireStory)==kFalse");
//		//CA("inside if");
//		//SKUData tempData;
//		//metadata::Clock a;
//		//metadata::DateTime dt;
//		//a.timestamp(dt);
//
//		///*PMString Date;
//		//Date.AppendNumber(dt.mday);
//		//Date.Append("-");
//		//Date.AppendNumber(dt.month);
//		//Date.Append("-");
//		//Date.AppendNumber(dt.year);*/
//		//
//		//PMString Date;
//		//Date.AppendNumber(dt.month);
//		//Date.Append("-");
//		//Date.AppendNumber(dt.mday);
//		//Date.Append("-");
//		//Date.AppendNumber(dt.year);
//
//		//tempData.OriginalData.Append(entireStory);
//		//tempData.ChangedData.Append(DBStory);
//		//tempData.DocPath.Append(FileName);
//		//tempData.Date.Append(Date);
//		//tempData.PageNo=pageNo;
//		//tempData.ColumName.Append(GlobalData::OldTable_col);
//		//tempData.ColumName.Append(" >> ");
//		//tempData.ColumName.Append(GlobalData::NewTable_col);
//		//tempData.ItemID = tagInfo.typeId;
//		//
//		////**** Now Getting the AttriBute Name from AttributeID		
//		//PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tagInfo.elementId,tagInfo.languageID);
//		//tempData.attributeName = dispname;		
//		//tempData.baseNumber =baseNumber;
//		////*******Up To Here	
//
//		//GlobalData::skuDataList.push_back(tempData);
//
//		//CA("DBStory.IsEqual(entireStory)==kFalse");
//		VectorHtmlTrackerPtr vectorHtmlTrackerSPTBPtr = NULL;
//		VectorHtmlTrackerValue vectorObj ;
//		vectorHtmlTrackerSPTBPtr = &vectorObj;
//		
//		
//		int32 NoofChar1 = DBStory.NumUTF16TextChars();
//		//WideString* myText=new WideString(DBStory.GrabCString());
//		
//		int32 tStart;
//		int32 tEnd;
//
//		if(tagInfo.startIndex + 1 == tagInfo.endIndex)
//		{
//			tStart=tagInfo.startIndex+1;
//			tEnd=tagInfo.endIndex;
//		}
//		else if(tagInfo.startIndex != tagInfo.endIndex)
//		{
//			tStart = tagInfo.startIndex+1;
//			tEnd = tagInfo.endIndex-tStart;
//		}
//
//		PMString textToInsert("");
//		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//		if(!iConverter)
//		{
//			textToInsert=DBStory;					
//		}
//		else
//		{
//			//textToInsert=iConverter->translateString(dispName);
//			vectorHtmlTrackerSPTBPtr->clear();
//			textToInsert=iConverter->translateStringNew(DBStory, vectorHtmlTrackerSPTBPtr);
//		}
//
//		//CA("Str to insert = " + textToInsert);
//		/*K2*/boost::shared_ptr<WideString> insertText(new WideString(textToInsert));
//		
//		ReplaceText(textModel, tStart, tEnd, insertText);
//
//
//		//CA("Value = " + textToInsert)
//		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//		//if(iConverter)
//		//{	//CA("5");	
//		//	iConverter->ChangeQutationMarkONOFFState(kFalse);
//		//	//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
//		//	ErrorCode Err = textModel->Replace(tStart, tEnd, myText);				
//		//	iConverter->ChangeQutationMarkONOFFState(kTrue);
//		//}	
//		//else
//		//{	
//		//	//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
//		//	ErrorCode Err = textModel->Replace(tStart, tEnd, myText);
//		//}
//
//		//CA("before CommonFunctions::updateTheTagWithModifiedAttributeValues");
//		updateTheTagWithModifiedAttributeValues(tagInfo.tagPtr,SelectedNewAttrID);
//		//CA("After CommonFunctions::updateTheTagWithModifiedAttributeValues");
//	}
//	else
//	{
//		//CA("TRUE");
//		updateTheTagWithModifiedAttributeValues(tagInfo.tagPtr,SelectedNewAttrID);
//	}
//
//	//CA("outside if");
}

ErrorCode CommonFunctions::ReplaceText(ITextModel* textModel, const TextIndex position, const int32 length, const /*K2*/boost::shared_ptr<WideString>& text)
{
	ErrorCode status = kFailure;
	do {
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{
			//CA("iConverter");
			iConverter->ChangeQutationMarkONOFFState(kFalse);			
		}

		ASSERT(textModel);
		if (!textModel) {
			//CA("!textModel");
			break;
		}
		if (position < 0 || position >= textModel->TotalLength()) {
			//CA("position invalid");
			break;
		}
		if (length < 0 || length >= textModel->TotalLength()) {
			//CA("length invalid");
			break;
		}
    	InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    	ASSERT(textModelCmds);
    	if (!textModelCmds) {
			//CA("!textModelCmds");
			break;
		}

		if(length == 0)
		{
			InterfacePtr<ICommand> insertTextCmd(textModelCmds->InsertCmd(position, text));
			ASSERT(insertTextCmd);		
			if (!insertTextCmd) {
				//CA("!replaceCmd");
				break;
			}
			//CA("before Replace ");
			status = CmdUtils::ProcessCommand(insertTextCmd);
			if(status != kSuccess)
			{
				//CA("status != kSuccess");
				break;
			}

		}
		else
		{
			InterfacePtr<ICommand> replaceCmd(textModelCmds->ReplaceCmd(position, length, text));
			ASSERT(replaceCmd);		
			if (!replaceCmd) {
				//CA("!replaceCmd");
				break;
			}
			//CA("before Replace ");
			status = CmdUtils::ProcessCommand(replaceCmd);
			if(status != kSuccess)
			{
				//CA("status != kSuccess");
				break;
			}
		}
		//CA("After Replace");
		if(iConverter)
		{
			//CA("true iConverter");
			iConverter->ChangeQutationMarkONOFFState(kTrue);			
		}
		
	} while(false);
	//CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
	return status;
}


void CommonFunctions::AddKeysWithoutSectionIds(IIDXMLElement* xmlElement, set<PMString>& resultIds)
{
	WideString parentTypeIDValue("");
	WideString parentIDValue("");
	WideString childIdValue("");
	WideString imgFlagValue("");
	PMString childHelperKey("");
	PMString parentHelperKey("");
	WideString underscore("_");
	WideString negativeOne("-1");
	WideString zero("0");
	WideString one("1");
	//WideString itemTypeForAssets("6882"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForValues("6936"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForAssets("2192"); // JAMIE this needs to be dynamic!!
	//WideString itemTypeForValues("2246"); // JAMIE this needs to be dynamic!!

	WideString parentID("parentID");
	WideString parentTypeID("parentTypeID");
	WideString childId("childId");
	WideString imgFlag("imgFlag");

	if (xmlElement->HasAttribute(parentID)) {
		parentIDValue.Append(xmlElement->GetAttributeValue(parentID));
		if (parentIDValue != negativeOne) {
			if (xmlElement->HasAttribute(parentTypeID)) {
				parentTypeIDValue.Append(xmlElement->GetAttributeValue(parentTypeID));
				if (parentTypeIDValue != negativeOne) {
					if (xmlElement->HasAttribute(imgFlag)) {
						imgFlagValue.Append(xmlElement->GetAttributeValue(imgFlag));
						if (imgFlagValue != one) {
							if (xmlElement->HasAttribute(childId)) {
								// add the child details
								childIdValue.Append(xmlElement->GetAttributeValue(childId));
								if (childIdValue != negativeOne) {
									childHelperKey.Append(zero);
									childHelperKey.Append(underscore);
									childHelperKey.Append(childIdValue);
									childHelperKey.Append(underscore);
									childHelperKey.Append(one);
									resultIds.insert(set<PMString> ::value_type(childHelperKey));
								}

								// add the parent details
								parentHelperKey.Append(parentTypeIDValue);
								parentHelperKey.Append(underscore);
								parentHelperKey.Append(parentIDValue);
								parentHelperKey.Append(underscore);
								parentHelperKey.Append(zero);
								parentHelperKey.SetTranslatable(kFalse);
								resultIds.insert(set<PMString> ::value_type(parentHelperKey));
							}
						}
					}
				}
			}
		}
	}

	for(int32 j = 0; j < xmlElement->GetChildCount(); j++)
	{
		XMLReference xmlReference = xmlElement->GetNthChild(j);
		IIDXMLElement* childElement = xmlReference.Instantiate();
		AddKeysWithoutSectionIds(childElement, resultIds);
		childElement->Release();
	}
}

void CommonFunctions::StartUpdateJL()
{

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
        return;
	}

	AcquireWaitCursor awc ;
	awc.Animate(); 

	int32 TotalUnusedTagsRemoved=0;
	int32 Unusedtags=0;

	InterfacePtr<IBookManager> bookManager(GetExecutionContextSession(), UseDefaultIID());
	IBook* currentActiveBook = bookManager->GetCurrentActiveBook();

	IPMUnknown* ipmUnknown = (IPMUnknown*) currentActiveBook;
	//IDataBase* bookDatabase = ::GetDataBase(ipmUnknown);
	InterfacePtr<IBookContentMgr> iBookContentMgr(ipmUnknown, UseDefaultIID());

	//for (int32 docIndex = 0; docIndex < iBookContentMgr->GetContentCount(); docIndex++)
	for (int32 docIndex = 0; docIndex < BookFileList.size(); docIndex++)
	{
		//UIDRef docRef;
		//OriginallyCloseDocInfo docInfo;

		//UID currentDocUID = iBookContentMgr->GetNthContent(docIndex);
		//ErrorCode openStatus = Utils<IBookUtils>()->OpenOneDocument(bookDatabase, currentDocUID, docRef, docInfo);

		//InterfacePtr<ICommand> cmd(CmdUtils::CreateCommand(kOpenLayoutCmdBoss));
		//cmd->SetItemList(UIDList(docRef));
		//InterfacePtr<IOpenLayoutPresentationCmdData> winData(cmd, IID_IOPENLAYOUTCMDDATA);
		//CmdUtils::ProcessCommand(cmd);
		//InterfacePtr<IWindow> window(winData->GetResultingPresentation(), UseDefaultIID());
		//if (window == nil) {
		//	// handle if document doesn't open
		//}

		//InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		//if(docIndex > 0)
        //    CmdUtils::ProcessScheduledCmds( ICommand::kLowestPriority);

		SDKLayoutHelper sdklhelp;

		UIDRef CurrDocRef = sdklhelp.OpenDocument(BookFileList[docIndex]);
		ErrorCode err = sdklhelp.OpenLayoutWindow(CurrDocRef);

		IDocument* doc = Utils<ILayoutUIUtils>()->GetFrontDocument();;
		IDataBase* docDatabase = ::GetDataBase((IPMUnknown*) doc);
		//const IDFile* idFile = docDatabase->GetSysFile();

		IIDXMLElement* rootElem = Utils<IXMLUtils>()->QueryRootElement(docDatabase);

		int32 count1 = rootElem->GetChildCount();
		int32 badTagsCnt = 0;

		set<PMString> uniqueResultIds;

		do
		{
			PMString str11("Unused Frame Removal");
			str11.SetTranslatable(kFalse);
			PMString str12("Unused frame removal is in progress ...");
			str12.SetTranslatable(kFalse);
			RangeProgressBar progressBar(str11, 0, count1, kTrue);
			progressBar.SetTaskText(str12);
				
			for(int32 i = count1 - 1; i >= 0; --i)
			{
				XMLReference xmlRef = rootElem->GetNthChild(i);
				IIDXMLElement* childElem = xmlRef.Instantiate();

				const XMLContentReference& contentRef = childElem->GetContentReference();
				const UIDRef & uidref = contentRef.GetUIDRef();
				if(uidref  == UIDRef::gNull)
				{
					ErrorCode status = Utils<IXMLElementCommands>()->DeleteElement(xmlRef,kTrue);
					badTagsCnt++;
				}
				else
				{
					AddKeysWithoutSectionIds(childElem, uniqueResultIds);  // this will have to move into the new logic
				}

				childElem->Release();

				progressBar.SetPosition(count1 - i);
			}
		} while (kFalse);

		do
		{
			vector<IndexReference> indexReferences;

			PMString indexingDoc("Refreshing Book");
			PMString taskText("");
			indexingDoc.SetTranslatable(kFalse);

			int32 taskCount = rootElem->GetChildCount() + 2;

			RangeProgressBar progressBar2(indexingDoc, 0, taskCount, kTrue);
			taskText.Append("Retrieving updated values from server");
			taskText.SetTranslatable(kFalse);
			progressBar2.SetTaskText(taskText);

			/////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////
					
			// DON
			//PMString itemFieldIds("11002997");
			//PMString itemGroupFieldIds("379,380");
			//PMString sectionFieldIds("");
			//WideString oldLanguageId("85");
			//WideString newLanguageId("85");

			//WideString itemFieldId1("11002997");
			//WideString itemGroupFieldId1("379");
			//WideString itemGroupFieldId2("380");

			//set<WideString> itemFieldIdsSet;
			//set<WideString> itemGroupFieldIdsSet;
			//set<WideString> sectionFieldIdsSet;
			//itemFieldIdsSet.insert(set<WideString> ::value_type(itemFieldId1));
			//itemGroupFieldIdsSet.insert(set<WideString> ::value_type(itemGroupFieldId1));
			//itemGroupFieldIdsSet.insert(set<WideString> ::value_type(itemGroupFieldId2));


			// 244
			//PMString itemFieldIds("11002700,11002975,11002974,11002997,11002969,11002972,11002970");
			//PMString itemGroupFieldIds("379,380,381,376,370,382");
			//PMString sectionFieldIds("");
			////WideString oldLanguageId("1000");
			////WideString newLanguageId("91");
			//WideString oldLanguageId("85");
			//WideString newLanguageId("85");

			//WideString itemFieldId1("11002700");
			//WideString itemFieldId2("11002975");
			//WideString itemFieldId3("11002974");
			//WideString itemFieldId4("11002997");
			//WideString itemFieldId5("11002969");
			//WideString itemFieldId6("11002972");
			//WideString itemFieldId7("11002970");
			//WideString itemGroupFieldId1("379");
			//WideString itemGroupFieldId2("380");
			//WideString itemGroupFieldId3("381");
			//WideString itemGroupFieldId4("376");
			//WideString itemGroupFieldId5("370");
			//WideString itemGroupFieldId6("382");
						
			/*itemFieldIdsSet.insert(set<WideString> ::value_type(itemFieldId1));
			itemFieldIdsSet.insert(set<WideString> ::value_type(itemFieldId2));
			itemFieldIdsSet.insert(set<WideString> ::value_type(itemFieldId3));
			itemFieldIdsSet.insert(set<WideString> ::value_type(itemFieldId4));
			itemFieldIdsSet.insert(set<WideString> ::value_type(itemFieldId5));
			itemFieldIdsSet.insert(set<WideString> ::value_type(itemFieldId6));
			itemFieldIdsSet.insert(set<WideString> ::value_type(itemFieldId7));

			set<WideString> itemGroupFieldIdsSet;
			itemGroupFieldIdsSet.insert(set<WideString> ::value_type(itemGroupFieldId1));
			itemGroupFieldIdsSet.insert(set<WideString> ::value_type(itemGroupFieldId2));
			itemGroupFieldIdsSet.insert(set<WideString> ::value_type(itemGroupFieldId3));
			itemGroupFieldIdsSet.insert(set<WideString> ::value_type(itemGroupFieldId4));
			itemGroupFieldIdsSet.insert(set<WideString> ::value_type(itemGroupFieldId5));
			itemGroupFieldIdsSet.insert(set<WideString> ::value_type(itemGroupFieldId6));

			set<WideString> sectionFieldIdsSet;

			*/


			set<WideString> itemFieldIdsSet;
			PMString itemFieldIds("");
			PMString strItemListIds("");
			if(attributeIDsList.size() > 0)
			{
				int32 j=0;
				for(int32 i =0; i < attributeIDsList.size(); i++ )
				{
					PMString itemFieldId("");
					itemFieldId.AppendNumber(PMReal(attributeIDsList.at(i)));
					WideString itemFieldWS(itemFieldId);
					itemFieldIdsSet.insert(set<WideString> ::value_type(itemFieldWS));
					


					if(i >0)
						itemFieldIds.Append(",");
					itemFieldIds.AppendNumber(PMReal(attributeIDsList.at(i)));
				}
			}

			if(itemListIds.size() > 0)
			{
				int32 j=0;
				for (std::set<double>::iterator it=itemListIds.begin(); it!=itemListIds.end(); ++it)
				{
					//std::cout << ' ' << *it;
					if(j >0)
							strItemListIds.Append(",");

					strItemListIds.AppendNumber(PMReal(*it));
					j++;
				}
			}

			set<WideString> itemGroupFieldIdsSet;
			PMString itemGroupFieldIds("");
			PMString strItemGroupListIds("");
			if(elementIDsList.size() > 0)
			{
				for(int32 i =0; i < elementIDsList.size(); i++ )
				{
					PMString itemGroupFieldId("");
					itemGroupFieldId.AppendNumber(PMReal(elementIDsList.at(i)));
					WideString itemGroupFieldWS(itemGroupFieldId);
					itemGroupFieldIdsSet.insert(set<WideString> ::value_type(itemGroupFieldWS));

					if(i >0)
						itemGroupFieldIds.Append(",");
					itemGroupFieldIds.AppendNumber(PMReal(elementIDsList.at(i)));
				}				
			}

			if(itemGroupListIds.size() > 0)
			{
				int32 k=0;
				for (std::set<double>::iterator it=itemGroupListIds.begin(); it!=itemGroupListIds.end(); ++it)
				{
					if(k >0)
							strItemGroupListIds.Append(",");

					strItemGroupListIds.AppendNumber(PMReal(*it));
					k++;
				}
			}
			


			set<WideString> sectionFieldIdsSet;
			PMString sectionFieldIds("");
			if(eventAttributeIDsList.size() > 0)
			{
				for(int32 i =0; i < eventAttributeIDsList.size(); i++ )
				{
					PMString eventFieldId("");
					eventFieldId.AppendNumber(PMReal(eventAttributeIDsList.at(i)));
					WideString eventFieldWS(eventFieldId);
					sectionFieldIdsSet.insert(set<WideString> ::value_type(eventFieldWS));

					if(i >0)
						sectionFieldIds.Append(",");
					sectionFieldIds.AppendNumber(PMReal(eventAttributeIDsList.at(i)));
				}
			}
			/////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////

			//ptrIAppFramework->getFieldSwapValues(uniqueResultIds, indexReferences, resultType, oldFieldId, newFieldId);
			PMString newLangId("");
			newLangId.AppendNumber(PMReal(CMediatorClass::replaceWithAttributeLanguageID));
			WideString newLanguageId(newLangId);

			PMString oldLangId("");
			oldLangId.AppendNumber(PMReal(CMediatorClass::oldAttributeLanguageID));
			WideString oldLanguageId(oldLangId);

			

			ptrIAppFramework->getRefreshData(uniqueResultIds, indexReferences, newLanguageId, itemFieldIds, itemGroupFieldIds, sectionFieldIds, strItemGroupListIds, strItemListIds );
			progressBar2.SetPosition(1);

			TotalTagsInDocument = count1;
			TotalUnusedTagsRemoved = badTagsCnt; 

			taskText.Clear();
			taskText.Append("");
			taskText.SetTranslatable(kFalse);
			progressBar2.SetTaskText(taskText);
			//DeleteAllTopics(doc);
			progressBar2.SetPosition(2);

			taskText.Clear();
			taskText.Append("Refreshing values in DocNameGoesHere");
			taskText.SetTranslatable(kFalse);
			progressBar2.SetTaskText(taskText);

			//UIDRef documentTopicListRef;
			//InterfacePtr <IIndexTopicListList> pIndexTopicListList(doc->GetDocWorkSpace(), UseDefaultIID());

			//int32 topicListCount = pIndexTopicListList->GetNumTopicLists();
			//if (topicListCount == 0)
			//{
			//	InterfacePtr<ICommand> piCreateTopicListCmd(CmdUtils::CreateCommand(kCreateTopicListCmdBoss));
			//	InterfacePtr<ICreateTopicListCmdData> piCreateTopicListCmdData(piCreateTopicListCmd, UseDefaultIID());
			//	piCreateTopicListCmdData->SetTargetItem(doc->GetDocWorkSpace());
			//	piCreateTopicListCmdData->SetDoNotifyFlag(kTrue);
			//	if (CmdUtils::ProcessCommand(piCreateTopicListCmd) != kSuccess)
			//		break;
			//}

			//// Indesign only supports upto one topic list currently
			//UID uid = pIndexTopicListList->GetNthTopicList(0);
			//documentTopicListRef = UIDRef(docDatabase, uid);

			set<WideString> childIds;
			for(int32 i=0; i < rootElem->GetChildCount(); i++)
			{
				XMLReference xmlRef = rootElem->GetNthChild(i);
				IIDXMLElement* childElem = xmlRef.Instantiate();
						
				const XMLContentReference& contentRef = childElem->GetContentReference();
				if (contentRef.GetContentType() == XMLContentReference::kContentType_PageItem) 
				{
					RefreshJL(childElem, indexReferences, false, childIds, false, oldLanguageId, newLanguageId, itemFieldIdsSet, itemGroupFieldIdsSet, sectionFieldIdsSet);
					progressBar2.SetPosition(i + 3);
				}

				childElem->Release();
			}
		} while (kFalse);

		rootElem->Release();

		sdklhelp.SaveDocumentAs(CurrDocRef,BookFileList[docIndex]);
		
		sdklhelp.CloseDocument(CurrDocRef, kFalse);	
	}



}



void CommonFunctions::RefreshJL(IIDXMLElement* xmlElement,
									   vector<IndexReference> &indexReferences,
									   bool isTable, set<WideString> &childIds, bool isChild, WideString oldLanguageId, WideString newLanguageId,
									   set<WideString> itemFieldIdsSet, set<WideString> itemGroupFieldIdsSet, set<WideString> sectionFieldIdsSet)
{
	WideString parentTypeIDValue("");
	WideString parentIDValue("");
	WideString childIdValue("");
	WideString imgFlagValue("");
	WideString fieldIdValue("");
	WideString languageIdValue("");
	PMString helperKey("");
	WideString minusOne("_-1");
	WideString zero("0");
	WideString one("1");
	WideString underscore("_");
	WideString negativeOne("-1");
	WideString three("3");
	WideString four("4");

	WideString parentID("parentID");
	WideString parentTypeID("parentTypeID");
	WideString childId("childId");
	WideString imgFlag("imgFlag");
	WideString attributeID("ID");
	WideString languageId("LanguageID");
	WideString tableId("tableId");
	WideString index("index");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
        return;
	}
	
	// currently, this is only checking if the fieldId matches up, but we will probably have to match up if it's group vs item
	if (xmlElement->HasAttribute(attributeID))
	{
		fieldIdValue.Append(xmlElement->GetAttributeValue(attributeID));

		set<WideString>::iterator itemFieldsIt;
		itemFieldsIt = itemFieldIdsSet.find(fieldIdValue);
		set<WideString>::iterator itemGroupFieldsIt;
		itemGroupFieldsIt = itemGroupFieldIdsSet.find(fieldIdValue);
		set<WideString>::iterator sectionFieldsIt;
		sectionFieldsIt = sectionFieldIdsSet.find(fieldIdValue);
		bool16 isRefreshedItemField = itemFieldsIt != itemFieldIdsSet.end();
		bool16 isRefreshedItemGroupField = itemGroupFieldsIt != itemGroupFieldIdsSet.end();
		bool16 isRefreshedSectionField = sectionFieldsIt != sectionFieldIdsSet.end();

		if (isRefreshedItemField || isRefreshedItemGroupField || isRefreshedSectionField)
		{
			if (xmlElement->HasAttribute(languageId))
			{
				languageIdValue.Append(xmlElement->GetAttributeValue(languageId));
				if (languageIdValue == oldLanguageId)
				{
					if (xmlElement->HasAttribute(parentID))
					{
						parentIDValue.Append(xmlElement->GetAttributeValue(parentID));
						if (parentIDValue != negativeOne)
						{
							if (xmlElement->HasAttribute(parentTypeID))
							{
								parentTypeIDValue.Append(xmlElement->GetAttributeValue(parentTypeID));
								if (parentTypeIDValue != negativeOne)
								{
									if (xmlElement->HasAttribute(imgFlag)) {
										imgFlagValue.Append(xmlElement->GetAttributeValue(imgFlag));
										if (imgFlagValue != one) 
										{
											if (xmlElement->HasAttribute(childId)) {
												childIdValue.Append(xmlElement->GetAttributeValue(childId));

												bool16 foundValue = false;

												for(int32 i=0; i < indexReferences.size(); i++) 
												{
													IndexReference indexReference = indexReferences.at(i);

													PMString irParentTypeId("");
													PMString irParentId("");
													bool16 irIsItem;
													bool16 irIsItemGroup;
													bool16 irIsSection;
													PMString irNewFieldId("");
													PMString irTableId("");
													PMString TableId("");
													irParentTypeId.AppendNumber(PMReal(indexReference.getParentTypeID()));
													irParentId.AppendNumber(PMReal(indexReference.getParentID()));
													irIsItem = indexReference.getIsItem();
													irIsItemGroup = indexReference.getIsItemGroup();
													irIsSection = indexReference.getIsSection();
													irTableId.AppendNumber(PMReal(indexReference.getTableId()));

													irNewFieldId.AppendNumber(PMReal(indexReference.getNewFieldID()));

													// if the indexReference is found, add to index
													if ((irParentTypeId == parentTypeIDValue && irParentId == parentIDValue && irNewFieldId == fieldIdValue) 
														|| (irIsItem && irParentId == childIdValue && irNewFieldId == fieldIdValue))
													{
														if ((irIsItem && isRefreshedItemField)
															|| (irIsItemGroup && isRefreshedItemGroupField)
															|| (irIsSection && isRefreshedSectionField))
														{
															PMString textToInsert("");
															if(irNewFieldId=="-980" || irNewFieldId=="-979" || irNewFieldId=="-978" || irNewFieldId=="-977" || irNewFieldId=="-976" || irNewFieldId=="-983" || irNewFieldId=="-982" || irNewFieldId=="-121")
															{
																TableId.Append(xmlElement->GetAttributeValue(tableId));
																//CA("irTableId : " + irTableId);
																//CA("TableId : " + TableId);
																if(TableId  == irTableId)
																{
																	 textToInsert =indexReference.getNewValue();
																	foundValue = true;
																}
																/*else
																{
																	continue;
																}*/
															}
															else
															{
																// change the text for the tag
																textToInsert =indexReference.getNewValue();
																foundValue = true;
															}

															if(foundValue)
															{
																textToInsert.ParseForEmbeddedCharacters();
																boost::shared_ptr<WideString> newText(new WideString(textToInsert));

																int32 tagStartPos = -1;
																int32 tagEndPos = -1;
																Utils<IXMLUtils>()->GetElementIndices(xmlElement, &tagStartPos, &tagEndPos);
																tagStartPos = tagStartPos + 1;
																tagEndPos = tagEndPos -1;

																ITextModel* textModel = Utils<IXMLUtils>()->QueryTextModel(xmlElement);

																if(textModel != NULL)
																	ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos +1, newText);

																//change the languageId in the XML
																Utils<IXMLAttributeCommands> xmlCmd;
																xmlCmd->SetAttributeValue(xmlElement->GetXMLReference(), languageId, WideString(newLanguageId));

																textModel->Release();

															

																if(irIsItem)
																	CMediatorClass::noOfItemFieldsUpdated++;
															
																if(irIsItemGroup)
																	CMediatorClass::noOfProductFieldsUpdated++;

																break;
															}																
														}
													}
												}

												if (!foundValue)
												{
													boost::shared_ptr<WideString> newText(new WideString(""));
													
													int32 tagStartPos = -1;
													int32 tagEndPos = -1;
													Utils<IXMLUtils>()->GetElementIndices(xmlElement, &tagStartPos, &tagEndPos);
													tagStartPos = tagStartPos + 1;
													tagEndPos = tagEndPos -1;

													ITextModel* textModel = Utils<IXMLUtils>()->QueryTextModel(xmlElement);

													if(textModel != NULL)
														ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos +1, newText);

													Utils<IXMLAttributeCommands> xmlCmd;
													xmlCmd->SetAttributeValue(xmlElement->GetXMLReference(), languageId, WideString(newLanguageId));

													textModel->Release();

													/*if(irIsItem)
														CMediatorClass::noOfItemFieldsUpdated++;
													else if(irIsItemGroup)
														CMediatorClass::noOfProductFieldsUpdated++;*/
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	WideString sectionTag("Section_");
	WideString customTabbedTextTag("CustomTabbedText");
	WideString psTableTag("PSTable");
	WideString header("header");

	for(int32 j = 0; j < xmlElement->GetChildCount(); j++)
	{
		XMLReference xmlReference = xmlElement->GetNthChild(j);
		IIDXMLElement* childElement = xmlReference.Instantiate();

		WideString tagName = childElement->GetTagString();
		bool isSectionTag = tagName.Contains(sectionTag);
		bool isCustomTabbedTextTag = tagName == customTabbedTextTag;
		bool isPsTableTag = tagName == psTableTag;

		if (isCustomTabbedTextTag || isPsTableTag)
			isTable = true;

		if (!isTable && !isSectionTag)
		{
			childIds.clear();
			RefreshJL(childElement, indexReferences, isTable, childIds, true, oldLanguageId, newLanguageId, itemFieldIdsSet, itemGroupFieldIdsSet, sectionFieldIdsSet);
		}
		else if (isTable && !isSectionTag) 
		{
			bool isHeaderRowTag = false;
			if (childElement->HasAttribute(header))
			{
				WideString headerValue("");
				headerValue.Append(childElement->GetAttributeValue(header));
				if (headerValue == one)
					isHeaderRowTag = true;
			}

			if (isCustomTabbedTextTag || isPsTableTag || !isHeaderRowTag)
			{
				if (childElement->HasAttribute(childId))
				{
					WideString childIdValue("");
					childIdValue.Append(childElement->GetAttributeValue(childId));
					bool isChild = childIdValue != negativeOne;
					if (isChild)
						childIds.insert(set<WideString> ::value_type(childIdValue));

					RefreshJL(childElement, indexReferences, isTable, childIds, isChild, oldLanguageId, newLanguageId, itemFieldIdsSet, itemGroupFieldIdsSet, sectionFieldIdsSet);
				}
				else
				{
					RefreshJL(childElement, indexReferences, isTable, childIds, false, oldLanguageId, newLanguageId, itemFieldIdsSet, itemGroupFieldIdsSet, sectionFieldIdsSet);
				}
			}
			else if(isHeaderRowTag)
			{
				WideString indexValue("");
				indexValue.Append(childElement->GetAttributeValue(index));
				PMString textToInsert("");

				PMString fieldIdValue1(childElement->GetAttributeValue(attributeID));
				//fieldIdValue1.Append(childElement->GetAttributeValue(attributeID));
				PMString newLanguageId1(newLanguageId);

				if (childElement->HasAttribute(languageId))
				{
					{
						languageIdValue.clear();
						languageIdValue.Append(childElement->GetAttributeValue(languageId));
						//if (languageIdValue == oldLanguageId)
						{
							if(indexValue == three)
							{
								CElementModel cElementModelObj;
								bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(fieldIdValue1.GetAsDouble(),cElementModelObj, newLanguageId1.GetAsDouble());
								if(result)
									textToInsert = cElementModelObj.getDisplayNameByLanguageId(newLanguageId1.GetAsDouble());

								CMediatorClass::noOfProductFieldsUpdated++;
							}
							else if(indexValue == four)
							{
								textToInsert=ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(fieldIdValue1.GetAsDouble(),newLanguageId1.GetAsDouble() );
								CMediatorClass::noOfItemFieldsUpdated++;
							}
						}
					}
				}



				textToInsert.ParseForEmbeddedCharacters();
				boost::shared_ptr<WideString> newText(new WideString(textToInsert));

				int32 tagStartPos = -1;
				int32 tagEndPos = -1;
				Utils<IXMLUtils>()->GetElementIndices(childElement, &tagStartPos, &tagEndPos);
				tagStartPos = tagStartPos + 1;
				tagEndPos = tagEndPos -1;

				ITextModel* textModel = Utils<IXMLUtils>()->QueryTextModel(childElement);

				if(textModel != NULL)
					ReplaceText(textModel, tagStartPos, tagEndPos-tagStartPos +1, newText);

				//change the languageId in the XML
				Utils<IXMLAttributeCommands> xmlCmd;
				xmlCmd->SetAttributeValue(childElement->GetXMLReference(), languageId, WideString(newLanguageId));

				textModel->Release();

			}
		}
		
		childElement->Release();
	}
}






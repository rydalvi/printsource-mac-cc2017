//========================================================================================
//  
//  $File: //depot/indesign_5.0/gm/source/sdksamples/writefishprice/WFPDialogController.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:37:33 $
//  
//  $Revision: #1 $
//  
//  $Change: 505969 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
#include "ITextEditSuite.h"  			// STEP 7.1
#include "ISelectionManager.h"			// STEP 7.1
#include "IPanelControlData.h" 			// STEP 9.1
#include "IDropDownListController.h" 	// STEP 9.1

// General includes:
#include "CDialogController.h"

// Project includes:
#include "RLID.h"
#include "SDKListBoxHelper.h"

#include "IAppFramework.h"
#include "CMediatorClass.h"
#include <vector>


#include "CAlert.h"
#define CA(x) CAlert::InformationAlert(x);

using namespace std;
//extern K2Vector<PMString> bookContentNames;  
extern vector<double> attributeIDsList;
extern vector<double> elementIDsList;
extern vector<double> eventAttributeIDsList;
IControlView* SelectAllControlView = nil;

double classID;


//****
K2Vector<bool16>  isSelected;  //**This Attribute Stores the Value of current Status of ListBox for checked(kTrue) and for Unchecked(kFalse) 

/** WFPDialogController

	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.

	
	@ingroup writefishprice
*/
class RLSelectAttribDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		RLSelectAttribDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~RLSelectAttribDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	    virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
};

CREATE_PMINTERFACE(RLSelectAttribDialogController, kSelectAttribDialogControllerImpl)


void RLSelectAttribDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	//CA("RLSelectAttribDialogController::InitializeDialogFields : Select Attribute Dlg - 1 Open ");
	CDialogController::InitializeDialogFields(dlgContext);

	// STEP 9.1  - BEGIN
	do {
		// Get current panel control data.

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			return;



		InterfacePtr<IPanelControlData> pPanelData(QueryIfNilElseAddRef(nil));
		if (pPanelData == nil)
		{
			//CA("CPDDialogController::InitializeFields: PanelControlData is nil!");
			break;
		}
		else
		{			
			IControlView *iListBoxControlView  = pPanelData->FindWidget(kRLListBoxWidgetID);
			if(iListBoxControlView ==  nil)
			{
				//CA("iListBoxControlView ====== nil");
				return;
			}						
			SDKListBoxHelper listHelper(this,kRLPluginID,0,0);
			listHelper.EmptyCurrentListBox(iListBoxControlView);
									
			vector<double>::iterator  itr; 
			int32 index=0;
			
			isSelected.clear();

		//testing
		//PMString attrid("");
		//for(itr = attributeIDsList.begin(); itr != attributeIDsList.end(); itr++)
		//{
		//	attrid.Append("\n");
		//	attrid.AppendNumber(*itr);
		//	
		//}
		//CA(attrid);
		// till here
			for(itr = attributeIDsList.begin(); itr != attributeIDsList.end(); itr++)
			{	
				if(*itr == -804)
					continue;

				listBoxData objListBoxData;
				objListBoxData.index = 4;
				objListBoxData.old_AttributeId = *itr;

				
				/*PMString replaceWithAttributeLanguageID("CMediatorClass::replaceWithAttributeLanguageID = ");
				replaceWithAttributeLanguageID.AppendNumber(CMediatorClass::replaceWithAttributeLanguageID);
				CA(replaceWithAttributeLanguageID);*/

				PMString oldAttributeValue = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(*itr,CMediatorClass::oldAttributeLanguageID);
				
				if(*itr == -401 || *itr == -402 || *itr == -403){
					if(*itr == -401)
						oldAttributeValue = "Make";	
					else if(*itr == -402)
						oldAttributeValue = "Model";
					else if(*itr == -403)
						oldAttributeValue = "Year";
				}
				oldAttributeValue.SetTranslatable(kFalse);
				//VectorAttributeInfoPtr vec_ptr = ptrIAppFramework->AttributeCache_getAttributeByParentIdLanguageId(*itr,CMediatorClass::replaceWithAttributeLanguageID);
				//if(vec_ptr == NULL)
				{
					//CA("vec_ptr == NULL");
					objListBoxData.replaceWith_AttributeId = *itr;
					
					//PMString replaceWith_AttributeName1 = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(*itr,CMediatorClass::replaceWithAttributeLanguageID);
					//if(replaceWith_AttributeName1 == "")
						objListBoxData.replaceWith_AttributeName = oldAttributeValue;
					//else
					//	objListBoxData.replaceWith_AttributeName = replaceWith_AttributeName1;

					//objListBoxData.replaceWith_AttributeName = oldAttributeValue;
					CMediatorClass::vecPtr_listBoxData->push_back(objListBoxData);
					
					listHelper.AddElement(iListBoxControlView, oldAttributeValue,kRLAttributeNameTextWidgetID,oldAttributeValue,kRLReplaceWithAttributeNameTextWidgetID, index);
					listHelper.CheckUncheckRow(iListBoxControlView,index,kTrue);
					isSelected.push_back(kTrue);
					//isSelected.insert(itr,kFalse);
					index++;
					continue;
				}

				//PMString sizeVectorAttributeInfoPtr("VectorAttributeInfoPtr size = ");
				//sizeVectorAttributeInfoPtr.AppendNumber(static_cast<int32>(vec_ptr->size()));
				//CA(sizeVectorAttributeInfoPtr);

				//VectorAttributeInfoValue::iterator itrr = vec_ptr->begin();
				//for(int32 k = 0;k < vec_ptr->size(); k++,itrr++)
				//{
				//	Attribute objAttributeModel = *itrr;

				//	//PMString temp("objAttributeModel.getAttribute_id = ");
				//	//temp.AppendNumber(objAttributeModel.getAttribute_id());
				//	//temp.Append("  , objAttributeModel.getLanguage_id = ");
				//	//temp.AppendNumber(objAttributeModel.getLanguage_id());
				//	//temp.Append("  , objAttributeModel.getDisplay_name = ");
				//	//temp.Append(objAttributeModel.getDisplay_name());
				//	//CA(temp);
				//	classID = objAttributeModel.getClass_id();

				//	objListBoxData.replaceWith_AttributeId = objAttributeModel.getAttribute_id();
			
				//	PMString newAttribute(objAttributeModel.getDisplayName());
				//	objListBoxData.replaceWith_AttributeName = newAttribute;
				//	newAttribute.SetTranslatable(kFalse);
	
				//	CMediatorClass::vecPtr_listBoxData->push_back(objListBoxData);
				//	
				//	listHelper.AddElement(iListBoxControlView, oldAttributeValue,kRLAttributeNameTextWidgetID,newAttribute,kRLReplaceWithAttributeNameTextWidgetID, index/*, kTrue*/);
				//	listHelper.CheckUncheckRow(iListBoxControlView,index,kTrue);
				//	isSelected.push_back(kTrue);
				//	//isSelected.insert(itr,kFalse);
				//	index++;
				//}
			}	


			//testing
			//PMString id("");
			//for(itr = elementIDsList.begin(); itr != elementIDsList.end(); itr++)
			//{
			//	id.Append("\n");
			//	id.AppendNumber(*itr);
			//	
			//}
			//CA(id);
			//till here

			for(itr = elementIDsList.begin(); itr != elementIDsList.end(); itr++)
			{	
				//CA("1");
				if(*itr == -804 )
					continue;						
					
				listBoxData objListBoxData;
				objListBoxData.index = 3;
				objListBoxData.old_AttributeId = *itr;
								
				//PMString replaceWithAttributeLanguageID("CMediatorClass::replaceWithAttributeLanguageID = ");
				//replaceWithAttributeLanguageID.AppendNumber(CMediatorClass::replaceWithAttributeLanguageID);
				//CA(replaceWithAttributeLanguageID);
				
				CElementModel cElementModelObj;
				bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(*itr,cElementModelObj,CMediatorClass::oldAttributeLanguageID);
				
				PMString oldAttributeValue("");
				oldAttributeValue.SetTranslatable(kFalse);
				if(result)
				{
					//CA("2");
					oldAttributeValue = cElementModelObj.getDisplayName();
					if(oldAttributeValue == "" && *itr == -1)
						oldAttributeValue = "Item Group Number";
					//CA("oldAttributeValue = " + oldAttributeValue);
				}
				 
				//CA("HERE");
				CElementModel cElementModelObj2;
				//bool16 result2 = ptrIAppFramework->ElementCache_GetElementModelForLanguageIdByElementId(*itr,CMediatorClass::replaceWithAttributeLanguageID, cElementModelObj2,1);
				//if(!result2)
				{
					///CA("!result2");					
					objListBoxData.replaceWith_AttributeId = *itr;
					objListBoxData.replaceWith_AttributeName = oldAttributeValue;
					CMediatorClass::vecPtr_listBoxData->push_back(objListBoxData);
					
					listHelper.AddElement(iListBoxControlView, oldAttributeValue,kRLAttributeNameTextWidgetID,oldAttributeValue,kRLReplaceWithAttributeNameTextWidgetID, index/*, kTrue*/);
					listHelper.CheckUncheckRow(iListBoxControlView,index,kTrue);
					isSelected.push_back(kTrue);
					//isSelected.insert(itr,kFalse);
					index++;
				}
				//else
				//{
				//	//CA("result2");
				//	classID = cElementModelObj2.getClassId();
				//	objListBoxData.replaceWith_AttributeId = cElementModelObj2.getElement_id();
		

	
				//	PMString newAttribute(cElementModelObj2.getDisplayName());
				//	//CA("newAttribute = " +newAttribute);
				//	if(newAttribute == "" && (objListBoxData.replaceWith_AttributeId == -1 || objListBoxData.replaceWith_AttributeId == -2)){
				//		//CA("oldAttributeValue = "+oldAttributeValue);
				//		objListBoxData.replaceWith_AttributeName = oldAttributeValue;
				//	}
				//	else{
				//		//CA("NOT NULL");
				//		objListBoxData.replaceWith_AttributeName = newAttribute;
				//	}
				//	CMediatorClass::vecPtr_listBoxData->push_back(objListBoxData);

				//	newAttribute.SetTranslatable(kFalse);
				//	
				//	listHelper.AddElement(iListBoxControlView, oldAttributeValue,kRLAttributeNameTextWidgetID,objListBoxData.replaceWith_AttributeName,kRLReplaceWithAttributeNameTextWidgetID, index/*, kTrue*/);
				//	listHelper.CheckUncheckRow(iListBoxControlView,index,kTrue);
				//	isSelected.push_back(kTrue);
				//	//isSelected.insert(itr,kFalse);
				//	index++;

				//}				
			}
			for(itr = eventAttributeIDsList.begin(); itr != eventAttributeIDsList.end(); itr++)
			{	
				listBoxData objListBoxData;
				objListBoxData.index = 5;
				objListBoxData.old_AttributeId = *itr;

				
				/*PMString replaceWithAttributeLanguageID("CMediatorClass::replaceWithAttributeLanguageID = ");
				replaceWithAttributeLanguageID.AppendNumber(CMediatorClass::replaceWithAttributeLanguageID);
				CA(replaceWithAttributeLanguageID);*/

				CElementModel cElementModelObj;
				bool16 result = ptrIAppFramework->ElementCache_GetElementModelByElementID(*itr,cElementModelObj,CMediatorClass::oldAttributeLanguageID);
				
				PMString oldAttributeValue("");
				oldAttributeValue.SetTranslatable(kFalse);
				if(result)
				{
					oldAttributeValue = cElementModelObj.getDisplayName();
					//CA("oldAttributeValue = " + oldAttributeValue);
				}
							
				/*PMString s("ElementId	:");
				s.AppendNumber(*itr);
				CA(s);*/
				
				CElementModel cElementModelObj2;
				//bool16 result2 = ptrIAppFramework->ElementCache_GetElementModelForLanguageIdByElementId(*itr,CMediatorClass::replaceWithAttributeLanguageID, cElementModelObj2,0);
				//if(!result2)
				{
					//CA("!result2");
					objListBoxData.replaceWith_AttributeId = *itr;
					objListBoxData.replaceWith_AttributeName = oldAttributeValue;
					//CA("replaceWith_AttributeName	:	"+oldAttributeValue);
					CMediatorClass::vecPtr_listBoxData->push_back(objListBoxData);
					
					listHelper.AddElement(iListBoxControlView, oldAttributeValue,kRLAttributeNameTextWidgetID,oldAttributeValue,kRLReplaceWithAttributeNameTextWidgetID, index/*, kTrue*/);
					listHelper.CheckUncheckRow(iListBoxControlView,index,kTrue);
					isSelected.push_back(kTrue);
					//isSelected.insert(itr,kFalse);
					index++;
				}
				//else
				//{
				//	//CA("result2");
				//	classID = cElementModelObj2.getClassId();
				//	objListBoxData.replaceWith_AttributeId = cElementModelObj2.getElement_id();

				//	PMString newAttribute(cElementModelObj2.getDisplayName());
				//	objListBoxData.replaceWith_AttributeName = newAttribute;
				//	//CA("replaceWith_AttributeName	:	"+newAttribute);
				//	CMediatorClass::vecPtr_listBoxData->push_back(objListBoxData);

				//	newAttribute.SetTranslatable(kFalse);
				//	
				//	listHelper.AddElement(iListBoxControlView, oldAttributeValue,kRLAttributeNameTextWidgetID,newAttribute,kRLReplaceWithAttributeNameTextWidgetID, index/*, kTrue*/);
				//	listHelper.CheckUncheckRow(iListBoxControlView,index,kTrue);
				//	isSelected.push_back(kTrue);
				//	//isSelected.insert(itr,kFalse);
				//	index++;

				//}	

			}
			//****Initialising SelectAll CkeckBox
			IControlView * selectAllSectionCheckBoxControlView = pPanelData->FindWidget(kSelectAllCheckBoxWidgetID);
			if(selectAllSectionCheckBoxControlView == nil)
			{
				//CA("selectAllSectionCheckBoxControlView == nil");
				break;
			}
			InterfacePtr<ITriStateControlData>selectAllSectionCheckBoxTriState(selectAllSectionCheckBoxControlView,UseDefaultIID());
			if(selectAllSectionCheckBoxTriState == nil)
			{
				//CA("selectAllSectionCheckBoxTriState == nil");
				break;
			}
			SelectAllControlView =  selectAllSectionCheckBoxControlView;
	
			this->SetTriStateControlData(kSelectAllCheckBoxWidgetID,kTrue);



		}
	} while (kFalse);
	// STEP 9.1  - END
}

/* ValidateFields
*/
WidgetID RLSelectAttribDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	//CA("RLSelectAttribDialogController::ValidateDialogFields");
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	return result;
}

/* ApplyFields
*/
void RLSelectAttribDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	// STEP 5.1 - BEGIN
	// DropDownList result string.
	//CA("CPDFSelectDocsDialogController::ApplyDialogFields");
	PMString resultString;
	
	//Get Selected text of DropDownList.
	//resultString = this->GetTextControlData(kWFPDropDownListWidgetID);
	//resultString.Translate(); // Look up our string and replace.
	//// STEP 5.1 - END

	//// STEP 6.1 - BEGIN
	//// Get the editbox list widget string.
	//PMString editBoxString = this->GetTextControlData(kWFPTextEditBoxWidgetID);
	//// STEP 6.1 - END

	//// STEP 6.2 - BEGIN
	//PMString moneySign(kWFPStaticTextKey);
	//moneySign.Translate(); // Look up our string and replace.
	//
	//resultString.Append('\t'); // Append tab code.
	//resultString.Append(moneySign);
	//resultString.Append(editBoxString);
	//resultString.Append('\r'); // Append return code.
	//// STEP 6.2 - END

	//// STEP 7.1 - BEGIN
 //   if (myContext == nil)
	//{
	//	ASSERT(myContext);
	//	return;
	//}
	//// Insert resultString to TextFrame.
 //   InterfacePtr<ITextEditSuite> textEditSuite(myContext->GetContextSelection(), UseDefaultIID());

	//// STEP 7.2 - BEGIN
 //   if (textEditSuite && textEditSuite->CanEditText())
 //   {
	//	// STEP 7.3 - BEGIN
 //       ErrorCode status = textEditSuite->InsertText(WideString(resultString));
 //       ASSERT_MSG(status == kSuccess, "CPDDialogController::ApplyDialogFields: can't insert text"); 
	//	// STEP 7.3 - END
 //   }
	// STEP 7.2 - END
}

/* ApplyFields
*/
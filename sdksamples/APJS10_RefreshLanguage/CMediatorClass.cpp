#include "VCPluginHeaders.h"
#include "CMediatorClass.h"

double CMediatorClass::oldAttributeLanguageID = -1;
double CMediatorClass::replaceWithAttributeLanguageID = -1;

int32 CMediatorClass::noOfItemFieldsUpdated = 0;
int32 CMediatorClass::noOfProductFieldsUpdated = 0;

vecPTR_ListBoxData CMediatorClass::vecPtr_listBoxData = nil;

PMString CMediatorClass::currentlySelectedAttributeReplaceName = "";


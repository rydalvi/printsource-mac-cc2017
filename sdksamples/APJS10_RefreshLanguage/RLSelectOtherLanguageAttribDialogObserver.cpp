//========================================================================================
//  
//  $File: //depot/indesign_5.0/gm/source/sdksamples/writefishprice/WFPDialogObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:37:33 $
//  
//  $Revision: #1 $
//  
//  $Change: 505969 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
// General includes:
#include "CDialogObserver.h"
#include "CDialogController.h"
// Project includes:
#include "RLID.h"

#include "SDKListBoxHelper.h"
#include "IListBoxController.h"

#include <vector>

#include "IDialog.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"

#include "RLActionComponent.h"
#include "CommonFunctions.h"
#include "CMediatorClass.h"


#include "CAlert.h"
#define CA(x) CAlert::InformationAlert(x)

using namespace std;

//extern K2Vector<PMString> bookContentNames;
extern vector<int32> attributeIDsList;
extern vector<int32> elementIDsList;
extern K2Vector<bool16>  isSelected;

extern vector<IDFile> BookFileList;///IDFile of all documents present inside book.

extern IDialog* firstDialog;
extern IDialog* secondDialog;

PMString selectedAttributeName;


/** Implements IObserver based on the partial implementation CDialogObserver.

	
	@ingroup writefishprice
*/
class RLSelectOtherLanguageAttribDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		RLSelectOtherLanguageAttribDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~RLSelectOtherLanguageAttribDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);

		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);

		void GetSelectedLocaleName(IControlView* LocaleControlView, PMString&);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(RLSelectOtherLanguageAttribDialogObserver, kSelectOtherLanguageAttribDialogObserverImpl)


void RLSelectOtherLanguageAttribDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Attach to other widgets you want to handle dynamically here.
		AttachWidget(panelControlData, kRLAttributeDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		//AttachToWidget(kRLOKButtonWidgetID , IID_IBOOLEANCONTROLDATA ,panelControlData);
		//AttachToWidget(kRLCancelButtonWidgetID, IID_IBOOLEANCONTROLDATA ,panelControlData);
		AttachToWidget(kRLOKDialogIconWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kRLCancel2DialogIconWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		
				
	} while (kFalse);
}

/* AutoDetach
*/
void RLSelectOtherLanguageAttribDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Detach from other widgets you handle dynamically here.
		DetachWidget(panelControlData, kRLAttributeDropDownWidgetID,IID_ISTRINGLISTCONTROLDATA);	
//		DetachFromWidget(kRLOKButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);		
//		DetachFromWidget(kRLCancelButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);	
		DetachFromWidget(kRLOKDialogIconWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);	
		DetachFromWidget(kRLCancel2DialogIconWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		
	} while (kFalse);
}

/* Update
*/
void RLSelectOtherLanguageAttribDialogObserver::Update
(
	const ClassID& theChange,
	ISubject* theSubject,
	const PMIID& protocol,
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	//CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	do
	{
		//CA("RLSelectOtherLanguageAttribDialogObserver::Update");
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		ASSERT(controlView);
		if(!controlView) {
			break;
		}
		
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		// TODO: process this


		InterfacePtr<IPanelControlData> pPanelData(this, UseDefaultIID());
		
		if(theSelectedWidget ==kRLAttributeDropDownWidgetID)
		{
			//CA("theSelectedWidget ==kLocaleDropDownWidgetID");		
			
			GetSelectedLocaleName(controlView, selectedAttributeName);
			//CA("selectedAttributeName : " + selectedAttributeName);
		}
		else if((theSelectedWidget == kRLOKDialogIconWidgetID  ||  theSelectedWidget == kOKButtonWidgetID /*kRLOKButtonWidgetID*/)&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRLCancelButton1WidgetID");
			CDialogObserver::CloseDialog();
		}
		else if((theSelectedWidget == kRLCancel2DialogIconWidgetID  ||  theSelectedWidget == kCancelButton_WidgetID/*kRLCancelButtonWidgetID*/)&& (theChange == kTrueStateMessage))
		{
			//CA("theSelectedWidget == kRLCancelButton1WidgetID");
			CDialogObserver::CloseDialog();
		}		
	} while (kFalse);
}

void RLSelectOtherLanguageAttribDialogObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{	
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil){
			//CAlert::InformationAlert("AutoAttach panelControlData nil");
			break;
		}

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil){
			//ASSERT_FAIL("widget has no ISubject... Ouch!");
			break;
		}
		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); // only do once
}

/*	::DetachWidget
*/
void RLSelectOtherLanguageAttribDialogObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
		{
			//ASSERT_FAIL("iControlView invalid. Where the widget are you?");
			break;
		}

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
		{
			//ASSERT_FAIL("PstLst Panel widget has no ISubject... Ouch!");
			break;
		}
		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}

void RLSelectOtherLanguageAttribDialogObserver::GetSelectedLocaleName(IControlView* LocaleControlView, PMString & attributeSelected)
{	
	//CA("RLSelectOtherLanguageAttribDialogObserver::GetSelectedLocaleName");
	
	do{
		InterfacePtr<IStringListControlData> LocaleDropListData(LocaleControlView,UseDefaultIID());	
		if(LocaleDropListData == nil){
			CAlert::InformationAlert("LocaleDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> LocaleDropListController(LocaleDropListData, UseDefaultIID());
		if(LocaleDropListController == nil){
			CAlert::InformationAlert("LocaleDropListController nil");
			break;
		}
		int32 selectedRow = LocaleDropListController->GetSelected();
		if(selectedRow <=0){
			//CAlert::InformationAlert("invalid row");
			break;
		}
		attributeSelected = LocaleDropListData->GetString(selectedRow);
		if(selectedRow>0){
			InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
			if (panelControlData == nil){
				CAlert::InformationAlert("panelControlData nil");
				break;
			}
			//IControlView* controlView = panelControlData->FindWidget(kRLAttributeDropDownWidgetID);
			//if (controlView == nil){
			//	//CAlert::InformationAlert("Publication Browse Button ControlView nil");
			//	break;
			//}
			//IControlView* controlView1 = panelControlData->FindWidget(kRLNextButtonWidgetID);
			//if (controlView1 == nil){
			//	CAlert::InformationAlert("Ok Button ControlView nil");
			//	break;
			//}
			//if(!controlView->GetEnableState()){
			//	controlView->Enable(kTrue);
			//	//controlView1->Enable(kTrue);
			//}
		}
	}while(0);	
	return;
} 


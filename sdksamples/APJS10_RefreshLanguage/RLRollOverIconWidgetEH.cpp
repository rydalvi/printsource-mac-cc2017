//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/pictureicon/RLCustomIconWidgetEH.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interfaces
#include "IControlView.h"
#include "IBooleanControlData.h"
#include "IUIFontSpec.h"
// General includes:
#include "CEventHandler.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "IDialogController.h"

// Project includes:
#include "RLID.h"
#include "IAppFramework.h"
#include "PublicationNode.h"
#include "CMediatorClass.h"
#include "ITriStateControlData.h"
#include "ISubject.h"

#include "IApplication.h"
#include "LocaleSetting.h"
#include "IDialogMgr.h"
#include "CoreResTypes.h"
#include "RsrcSpec.h"


#include "SDKListBoxHelper.h"
#define CA(X) CAlert::InformationAlert(X)

//extern int32 CurrentSelectedSubSection;
//extern 	int no_of_lstboxElements;
extern K2Vector<bool16>  isSelected;
extern IControlView* SelectAllControlView;

extern PMString selectedAttributeName;
extern int32 classID;

int32 selectedAttributeIndex;
bool16 isParametric;

extern vector<int32> eventAttributeIDsList;
extern vector<int32> elementIDsList;
/** 
	Traps LButtonDn events.

	The only method that is overridden by this class is
	LButtonDn; this is to create a picture-based button which can use
	Mac PICT or Windows BITMAP resources rather than just icons as in the
	other buttons in the widget set.
	The implementation is quite sparse in that there are only two different
	pictures for the selected and uselected states. There is
	none of the sophistication of the real iconic buttons such as
	being able to display a highlighted state when the end-user is clicking on the
	button.

	@ingroup pictureicon
	
*/

class RLRollOverIconWidgetEH : public CEventHandler
{
public:
	
	/**
		ctor. 
		@param boss interface ptr on the boss object to which the interface implemented here belongs.
	*/	
	RLRollOverIconWidgetEH(IPMUnknown *boss);
	
	/**
		Destructor
	*/	
	virtual ~RLRollOverIconWidgetEH(){}

	/**
		Handle left button down events.
		Change the data model, and also change the picture displayed.
		This is because there is no platform peer that knows how to change its visual representation
		so we have to do it ourselves.

		@param e event of interest.
	*/
	virtual bool16 LButtonDn(IEvent* e);
	void DoSelectOtherLangugeAttributeDialog();
};

CREATE_PMINTERFACE( RLRollOverIconWidgetEH, kRLRollOverIconWidgetEHImpl)

	
RLRollOverIconWidgetEH::RLRollOverIconWidgetEH(IPMUnknown *boss) :
	CEventHandler(boss)
{
	
	
}

bool16 RLRollOverIconWidgetEH::LButtonDn(IEvent* e)
{		
	do {
		//CA("...my  New LButtonDn...");
		int32 rsrcID = 0;
		
		IAppFramework* ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CA("AP46CreateMedia::Update::ptrIAppFramework == NULL");		
			break;
		}
		
		InterfacePtr<IWidgetParent> findParentOfRollOverIconButtonWidget(this,UseDefaultIID());
		if(findParentOfRollOverIconButtonWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::Update::findParentOfRollOverIconButtonWidget == nil");
		//	break;
		}
		IPMUnknown *  primaryResourcePanelWidgetUnKnown = findParentOfRollOverIconButtonWidget->GetParent();
		if(primaryResourcePanelWidgetUnKnown == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::Update::primaryResourcePanelWidgetUnKnown == nil");
			break;
		}
		InterfacePtr<IControlView>primaryResourcePanelWidgetControlView(primaryResourcePanelWidgetUnKnown,UseDefaultIID());
		if(primaryResourcePanelWidgetControlView == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::Update::primaryResourcePanelWidgetControlView == nil");
			break;
		}
		InterfacePtr<IPanelControlData>primaryResourcePanelWidgetPanelControlData(primaryResourcePanelWidgetControlView,UseDefaultIID());
		if(primaryResourcePanelWidgetPanelControlData == nil)			
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::Update::primaryResourcePanelWidgetPanelControlData == nil");
			break;
		}

		InterfacePtr<IWidgetParent> findParentOfPrimaryResourcePanelWidget(primaryResourcePanelWidgetPanelControlData,UseDefaultIID());
		if(findParentOfPrimaryResourcePanelWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::Update::findParentOfPrimaryResourcePanelWidget == nil");
			break;
		}
		IPMUnknown *  cellPanelWidgetUnKnown = findParentOfPrimaryResourcePanelWidget->GetParent();
		if(cellPanelWidgetUnKnown == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::Update::cellPanelWidgetUnKnown == nil");
			break;
		}
		InterfacePtr<IPanelControlData>cellPanelWidgetPanelControlData(cellPanelWidgetUnKnown,UseDefaultIID());
		if(cellPanelWidgetPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::Update::cellPanelWidgetPanelControlData == nil");
			break;
		}
		
		int32 listBoxRowIndex = cellPanelWidgetPanelControlData->GetIndex(primaryResourcePanelWidgetControlView);
		
		listBoxData objListBoxData = CMediatorClass::vecPtr_listBoxData->at(listBoxRowIndex);
		selectedAttributeName = objListBoxData.replaceWith_AttributeName;

		//CA("selectedAttributeName = " + selectedAttributeName);
		selectedAttributeIndex = objListBoxData.index;

		vec_listBoxData::iterator listItr1 = CMediatorClass::vecPtr_listBoxData->begin();
		for(int i = 0; i < listBoxRowIndex;i++)
		{
			listItr1++;
		}
		
	//	PMString temp1("listItr1->old_AttributeId = ");
	//	temp1.AppendNumber(listItr1->old_AttributeId);
		//CA(temp1);


		VectorAttributeInfoPtr vec_attributesPtr1 = nil; // //ptrIAppFramework->AttributeCache_getAttributeByParentIdLanguageId(listItr1->old_AttributeId, CMediatorClass::replaceWithAttributeLanguageID);
		if(vec_attributesPtr1 != nil)
		{
			VectorAttributeInfoValue::iterator itr2 = vec_attributesPtr1->begin();

			for(;itr2 != vec_attributesPtr1->end(); itr2++)
			{
				Attribute objAttributeModel = *itr2;		

				if(objAttributeModel.getAttributeId() == listItr1->old_AttributeId)
				{
					//CA("objAttributeModel.getAttribute_id() == listItr1->old_AttributeId");
					//classID = objAttributeModel.getClassId();
					//PMString temp("classID = ");
					//temp.AppendNumber(classID);
					//CA(temp);
					isParametric = objAttributeModel.getisParametric();
					break;
				}		
			}
		}		


		DoSelectOtherLangugeAttributeDialog();
		//CA("selectedAttributeName = " + selectedAttributeName);


		SDKListBoxHelper listHelper(nil, 0, 0, 0);
		listHelper.UpdateStaticTextWidget(cellPanelWidgetPanelControlData,kRLReplaceWithAttributeNameTextWidgetID,selectedAttributeName,listBoxRowIndex);	
	
		int32 isEventAttribute = 2;
		for(int32 indx = 0 ; indx < eventAttributeIDsList.size() ; indx++)
		{
			if(objListBoxData.old_AttributeId == eventAttributeIDsList[indx])
				isEventAttribute = 1;
		}
		for(int32 indx = 0 ; indx < eventAttributeIDsList.size() ; indx++)
		{
			if(objListBoxData.old_AttributeId == elementIDsList[indx])
				isEventAttribute = 0;
		}
		if(isEventAttribute == 1)	//for section/Event 
		{
			//CA("event Attribute");
			//VectorElementInfoPtr vec_ElementsPtr = ptrIAppFramework->ElementCache_getEventSectionAttributesByLangaugeId(CMediatorClass::replaceWithAttributeLanguageID,isEventAttribute);
			//if(vec_ElementsPtr == nil)
			//{
			//	CAlert::InformationAlert("Pointer to vec_ElementsPtr is nil.");
			//	break;
			//}

			//VectorElementInfoValue::iterator itr = vec_ElementsPtr->begin();

			//for(;itr != vec_ElementsPtr->end(); itr++)
			//{
			//	CElementModel objElementModel = *itr;		

			//	if(objElementModel.getDisplayName() == selectedAttributeName)
			//	{
			//		listBoxData objListBoxData = CMediatorClass::vecPtr_listBoxData->at(listBoxRowIndex);

			//		vec_listBoxData::iterator listItr = CMediatorClass::vecPtr_listBoxData->begin();
			//		for(int i = 0; i < listBoxRowIndex;i++)
			//		{
			//			listItr++;
			//		}

			//		listItr->replaceWith_AttributeId = objElementModel.getElement_id();
			//		listItr->replaceWith_AttributeName = selectedAttributeName;
			//	
			//		/*PMString replaceWithAttributeLanguageID("CMediatorClass::replaceWithAttributeLanguageID = ");
			//		replaceWithAttributeLanguageID.AppendNumber(CMediatorClass::replaceWithAttributeLanguageID);
			//		CA(replaceWithAttributeLanguageID);*/

			//		return kTrue;
			//	}		
			//}
		}
		else if(isEventAttribute == 0)	//for Product
		{
			//VectorElementInfoPtr vec_ElementsPtr = ptrIAppFramework->ElementCache_getEventSectionAttributesByLangaugeId(CMediatorClass::replaceWithAttributeLanguageID,isEventAttribute);
			//if(vec_ElementsPtr == nil)
			//{
			//	CAlert::InformationAlert("Pointer to vec_ElementsPtr is nil.");
			//	break;
			//}

			//VectorElementInfoValue::iterator itr = vec_ElementsPtr->begin();

			//for(;itr != vec_ElementsPtr->end(); itr++)
			//{
			//	CElementModel objElementModel = *itr;		

			//	if(objElementModel.getDisplayName() == selectedAttributeName)
			//	{
			//		listBoxData objListBoxData = CMediatorClass::vecPtr_listBoxData->at(listBoxRowIndex);

			//		vec_listBoxData::iterator listItr = CMediatorClass::vecPtr_listBoxData->begin();
			//		for(int i = 0; i < listBoxRowIndex;i++)
			//		{
			//			listItr++;
			//		}

			//		listItr->replaceWith_AttributeId = objElementModel.getElement_id();
			//		listItr->replaceWith_AttributeName = selectedAttributeName;
			//	
			//		/*PMString replaceWithAttributeLanguageID("CMediatorClass::replaceWithAttributeLanguageID = ");
			//		replaceWithAttributeLanguageID.AppendNumber(CMediatorClass::replaceWithAttributeLanguageID);
			//		CA(replaceWithAttributeLanguageID);*/

			//		return kTrue;
			//	}		
			//}
		}
		else
		{
			VectorAttributeInfoPtr vec_attributesPtr = ptrIAppFramework->/*AttributeCache_getItemAttributes*/StructureCache_getAllItemAttributes(CMediatorClass::replaceWithAttributeLanguageID);
			if(vec_attributesPtr == nil)
			{
				CAlert::InformationAlert("Pointer to vec_attributesPtr is nil.");
				break;
			}

			VectorAttributeInfoValue::iterator itr = vec_attributesPtr->begin();

			for(;itr != vec_attributesPtr->end(); itr++)
			{
				Attribute objAttributeModel = *itr;		

				if(objAttributeModel.getDisplayName() == selectedAttributeName)
				{
					listBoxData objListBoxData = CMediatorClass::vecPtr_listBoxData->at(listBoxRowIndex);

					vec_listBoxData::iterator listItr = CMediatorClass::vecPtr_listBoxData->begin();
					for(int i = 0; i < listBoxRowIndex;i++)
					{
						listItr++;
					}

					listItr->replaceWith_AttributeId = objAttributeModel.getAttributeId();
					listItr->replaceWith_AttributeName = selectedAttributeName;
				
					/*PMString replaceWithAttributeLanguageID("CMediatorClass::replaceWithAttributeLanguageID = ");
					replaceWithAttributeLanguageID.AppendNumber(CMediatorClass::replaceWithAttributeLanguageID);
					CA(replaceWithAttributeLanguageID);*/

					return kTrue;
				}		
			}
		}
		return kTrue;
		// early return as here we have already
		// handled the event and do not need to
		// delegate any processing to the base class.
	} while(0);


	return CEventHandler::LButtonDn(e);
}

void RLRollOverIconWidgetEH::DoSelectOtherLangugeAttributeDialog()
{
	InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication());
	ASSERT(application);
	if (application == nil) {	
		return;
	}
	InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
	ASSERT(dialogMgr);
	if (dialogMgr == nil) {
		return;
	}
	
	PMLocaleId nLocale = LocaleSetting::GetLocale();
	RsrcSpec dialogSpec
	(
		nLocale,					// Locale index from PMLocaleIDs.h. 
		kRLPluginID,			// Our Plug-in ID  
		kViewRsrcType,				// This is the kViewRsrcType.
		kRLSelectAttributeDialogResourceID,	// Resource ID for our dialog.
		kTrue						// Initially visible.
	);

	// CreateNewDialog takes the dialogSpec created above, and also
	// the type of dialog being created (kMovableModal).
	IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
	if (dialog == nil) {
		return;
	}	

	dialog->Open(); 
	dialog->WaitForDialog();	
}





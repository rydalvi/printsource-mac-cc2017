//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/wlistboxcomposite/WLBCmpListBoxObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: jbond $
//  
//  $DateTime: 2005/03/16 15:42:00 $
//  
//  $Revision: #2 $
//  
//  $Change: 326153 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"

// Interface includes
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
// Implem includes
#include "CAlert.h"
#include "CObserver.h"
#include "RLID.h"
#include "SDKListBoxHelper.h"
#include "SDKFileHelper.h"

#include "IWidgetParent.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "ITriStateControlData.h"

#include "IEventUtils.h"
#include "Utils.h"
#include "IAppFramework.h"
//#include "MediatorClass.h"
#include "RLActionComponent.h"

//for template file validation
#include "SDKUtilities.h"
#include "FileUtils.h"

void CAMessage(const PMString & fileName,const PMString & functionName,const PMString & message,int32 line);
#define FILENAME			PMString("CMMCmpTemplateFileListBoxObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA(X) CAlert::InformationAlert(X); 
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
/**

	Observes the listbox via IID_ILISTCONTROLDATA.

	@ingroup wlistboxcomposite
	
*/

//extern 	int no_of_lstboxElements;

//forward declaration
void DoSelectOtherLanguageAttributeDialog();

class RLSelectOtherLangAttributeObserver : public CObserver
{
public:
	
	/**
		Constructor for WLBListBoxObserver class.
		@param interface ptr from boss object on which this interface is aggregated.
	*/
	RLSelectOtherLangAttributeObserver(IPMUnknown *boss);

	/**
		Destructor for WLBCmpListBoxObserver class
		
	*/	
	~RLSelectOtherLangAttributeObserver();

	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	
	*/	
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when widget hidden.
	*/	
	virtual void AutoDetach();

	/**
		This class is interested in changes along IID_ILISTCONTROLDATA protocol with classID of
		kListSelectionChangedByUserMessage. This message is sent when a user clicks on an element
		in the list-box.

	
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/	
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

private:
	/**
		Helper method to change state of list-box, dependent on whether
		it is being shown or hidden.
		@param isAttaching specifies whether attaching (shown) or detaching (hidden)
	*/
	 void updateListBox(bool16 isAttaching);

	
};

CREATE_PMINTERFACE(RLSelectOtherLangAttributeObserver, kSelectOtherLanguageAttributeListBoxObserverImpl)


RLSelectOtherLangAttributeObserver::RLSelectOtherLangAttributeObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}


RLSelectOtherLangAttributeObserver::~RLSelectOtherLangAttributeObserver()
{
	
}


void RLSelectOtherLangAttributeObserver::AutoAttach()
{
	
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ITRISTATECONTROLDATA);
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
		
	}	
	//updateListBox(kTrue);
}


void RLSelectOtherLangAttributeObserver::AutoDetach()
{
	
	//updateListBox(kFalse);
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_ITRISTATECONTROLDATA);
		subject->DetachObserver(this,IID_ILISTCONTROLDATA);
		
	}
}


void RLSelectOtherLangAttributeObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	//CA("RLSelectOtherLangAttributeObserver::update");
	InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
	if(!controlView) 
		{
			CAlert::InformationAlert("controlView == nil");
			return;
		}
	IAppFramework* ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		//CA("AP46CreateMedia::Update::ptrIAppFramework == NULL");		
		return;
	}
	// Get the button ID from the view.
	WidgetID theSelectedWidget = controlView->GetWidgetID();
	///////////////////////////////////////
	if ((theSelectedWidget == kSelectOtherLanguageAttributeRollOverButtonWidgetID)			
		&& 
		(protocol == IID_ITRISTATECONTROLDATA) 
		&& 
		(theChange == kTrueStateMessage)) 
	{
		
		do
		{
			//CA("theSelectedWidget == kSelectOtherLanguageAttributeRollOverButtonWidgetID");
		
			InterfacePtr<IWidgetParent> findParentOfRollOverIconButtonWidget(this,UseDefaultIID());
			if(findParentOfRollOverIconButtonWidget == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::findParentOfRollOverIconButtonWidget == nil");
				break;
			}
			IPMUnknown *  primaryResourcePanelWidgetUnKnown = findParentOfRollOverIconButtonWidget->GetParent();
			if(primaryResourcePanelWidgetUnKnown == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::primaryResourcePanelWidgetUnKnown == nil");
				break;
			}
			InterfacePtr<IControlView>primaryResourcePanelWidgetControlView(primaryResourcePanelWidgetUnKnown,UseDefaultIID());
			if(primaryResourcePanelWidgetControlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::primaryResourcePanelWidgetControlView == nil");
				break;
			}
			InterfacePtr<IPanelControlData>primaryResourcePanelWidgetPanelControlData(primaryResourcePanelWidgetControlView,UseDefaultIID());
			if(primaryResourcePanelWidgetPanelControlData == nil)			
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::primaryResourcePanelWidgetPanelControlData == nil");
				break;
			}

//			MediatorClass :: iPrimaryPanelCntrlDataPtr = primaryResourcePanelWidgetPanelControlData;

			PMString pathOfFile("");
			//find parent of primaryResourcePanelWidgetPanel which is a cellpanelwidget
			InterfacePtr<IWidgetParent> findParentOfPrimaryResourcePanelWidget(primaryResourcePanelWidgetPanelControlData,UseDefaultIID());
			if(findParentOfPrimaryResourcePanelWidget == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::findParentOfPrimaryResourcePanelWidget == nil");
				break;
			}
			IPMUnknown *  cellPanelWidgetUnKnown = findParentOfPrimaryResourcePanelWidget->GetParent();
			if(cellPanelWidgetUnKnown == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::cellPanelWidgetUnKnown == nil");
				break;
			}
			InterfacePtr<IPanelControlData>cellPanelWidgetPanelControlData(cellPanelWidgetUnKnown,UseDefaultIID());
			if(cellPanelWidgetPanelControlData == nil)
			{
				ptrIAppFramework->LogDebug("AP46CreateMedia::Update::cellPanelWidgetPanelControlData == nil");
				break;
			}
//			MediatorClass :: iPanelCntrlDataPtr = cellPanelWidgetPanelControlData;
			int32 listBoxRowIndex = cellPanelWidgetPanelControlData->GetIndex(primaryResourcePanelWidgetControlView);
			
			//PMString ss;
			//ss.AppendNumber(listBoxRowIndex);
			//CA(ss);
			//PMString indexx;
			//indexx.AppendNumber(listBoxRowIndex);
			//CA(indexx);

			{	
				//PMString indexx;
				//indexx.AppendNumber(index);
				//CA(indexx);
//				MediatorClass :: selectedIndexOfListBoxElement = listBoxRowIndex;
				PMString num;
//				num.AppendNumber(MediatorClass :: selectedIndexOfListBoxElement);
			//	CA(num);
				//DoCMMSpraySettingsDialog();
				//CMMActionComponent cmmActn(MediatorClass :: CMMActionComponentIPMUnknown);
				//cmmActn.
//				DoCMMSpraySettingsDialog();
			}
		}while(kFalse);
	}		
			
}

 
void RLSelectOtherLangAttributeObserver::updateListBox(bool16 isAttaching)
{
	//CAlert::InformationAlert("RLSelectOtherLangAttributeObserver::updateListBox");
//	SDKListBoxHelper listHelper(this, kRLPluginID, kRLListBoxWidgetID, kRLDialog1WidgetID);
//	listHelper.EmptyCurrentListBox();
	// See if we can find the  listbox
	// If we can then populate the listbox in brain-dead way
	if(isAttaching) {
		do {
				const int targetDisplayWidgetId = kRLReplaceWithAttributeNameTextWidgetID;
				const int kNumberItems = 5;
				for(int i=0; i < kNumberItems; i++) {
					//CAlert::InformationAlert("hi");
					PMString name("Filexxxxxxxxxxxxxxxxxxxxxxxxxxxxx : ");
					name.AppendNumber(i+1);
//					listHelper.AddElement(name, targetDisplayWidgetId);
				}
		   } while(0);
	}
}


//========================================================================================
//  
//  $File: //depot/indesign_4.0/gm/source/sdksamples/pictureicon/RLCustomIconWidgetEH.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:31:35 $
//  
//  $Revision: #1 $
//  
//  $Change: 323509 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interfaces
#include "IControlView.h"
#include "IBooleanControlData.h"
#include "IUIFontSpec.h"
// General includes:
#include "CEventHandler.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "IDialogController.h"

// Project includes:
#include "RLID.h"
#include "IAppFramework.h"
#include "PublicationNode.h"
#include "CMediatorClass.h"
#include "ITriStateControlData.h"
#include "ISubject.h"

#define CA(X) CAlert::InformationAlert(X)

//extern int32 CurrentSelectedSubSection;
//extern 	int no_of_lstboxElements;
extern K2Vector<bool16>  isSelected;
extern IControlView* SelectAllControlView;
/** 
	Traps LButtonDn events.

	The only method that is overridden by this class is
	LButtonDn; this is to create a picture-based button which can use
	Mac PICT or Windows BITMAP resources rather than just icons as in the
	other buttons in the widget set.
	The implementation is quite sparse in that there are only two different
	pictures for the selected and uselected states. There is
	none of the sophistication of the real iconic buttons such as
	being able to display a highlighted state when the end-user is clicking on the
	button.

	@ingroup pictureicon
	
*/

class RLCustomIconWidgetEH : public CEventHandler
{
public:
	
	/**
		ctor. 
		@param boss interface ptr on the boss object to which the interface implemented here belongs.
	*/	
	RLCustomIconWidgetEH(IPMUnknown *boss);
	
	/**
		Destructor
	*/	
	virtual ~RLCustomIconWidgetEH(){}

	/**
		Handle left button down events.
		Change the data model, and also change the picture displayed.
		This is because there is no platform peer that knows how to change its visual representation
		so we have to do it ourselves.

		@param e event of interest.
	*/
	virtual bool16 LButtonDn(IEvent* e);
};

CREATE_PMINTERFACE( RLCustomIconWidgetEH, kRLCustomIconSuitWidgetEHImpl)

	
RLCustomIconWidgetEH::RLCustomIconWidgetEH(IPMUnknown *boss) :
	CEventHandler(boss)
{
	
	
}

bool16 RLCustomIconWidgetEH::LButtonDn(IEvent* e)
{

		
	do {
		//CA("...LButtonDn...");
		int32 rsrcID = 0;
	//break;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA(" ptrIAppFramework nil ");
			return 1;
		}
		//CA("115");
		InterfacePtr<IBooleanControlData> controlData(this, IID_IBOOLEANCONTROLDATA); // no kDefaultIID
		if(controlData==nil)
		{
			//CA("RLCustomIconWidgetEH::LButtonDn - No IBooleanControlData found on this boss.");
			break;
		}
		//CA("122");
		InterfacePtr<IUIFontSpec> fontSpec(this, IID_IUIFONTSPEC); // no kDefaultIID
		if(fontSpec==nil) 
		{
			//CA("RLCustomIconWidgetEH::LButtonDn - No IUIFontSpec found on this boss.");
			break;
		}
		//CA("129");

		InterfacePtr<IWidgetParent> findParentOfRollOverIconButtonWidget(this,UseDefaultIID());
		if(findParentOfRollOverIconButtonWidget == nil)
		{
			
			/*ptrIAppFramework->LogDebug*///CA("AP46CreateMedia::Update::findParentOfRollOverIconButtonWidget == nil");
			break;
		}
		//CA("138");
		IPMUnknown *  primaryResourcePanelWidgetUnKnown = findParentOfRollOverIconButtonWidget->GetParent();
		if(primaryResourcePanelWidgetUnKnown == nil)
		{
			/*ptrIAppFramework->LogDebug*///CA("AP46CreateMedia::Update::primaryResourcePanelWidgetUnKnown == nil");
			break;
		}
		//CA("145");
		InterfacePtr<IControlView>primaryResourcePanelWidgetControlView(primaryResourcePanelWidgetUnKnown,UseDefaultIID());
		if(primaryResourcePanelWidgetControlView == nil)
		{
			/*ptrIAppFramework->LogDebug*///CA("AP46CreateMedia::Update::primaryResourcePanelWidgetControlView == nil");
			break;
		}
		//CA("152");
		InterfacePtr<IPanelControlData>primaryResourcePanelWidgetPanelControlData(primaryResourcePanelWidgetControlView,UseDefaultIID());
		if(primaryResourcePanelWidgetPanelControlData == nil)			
		{
			/*ptrIAppFramework->LogDebug*///CA("AP46CreateMedia::Update::primaryResourcePanelWidgetPanelControlData == nil");
			break;
		}
		//CA("159");
		//find parent of primaryResourcePanelWidgetPanel which is a cellpanelwidget
		InterfacePtr<IWidgetParent> findParentOfPrimaryResourcePanelWidget(primaryResourcePanelWidgetPanelControlData,UseDefaultIID());
		if(findParentOfPrimaryResourcePanelWidget == nil)
		{
			/*ptrIAppFramework->LogDebug*///CA("AP46CreateMedia::Update::findParentOfPrimaryResourcePanelWidget == nil");
			break;
		}
		//CA("167");
		IPMUnknown *  cellPanelWidgetUnKnown = findParentOfPrimaryResourcePanelWidget->GetParent();
		if(cellPanelWidgetUnKnown == nil)
		{
			/*ptrIAppFramework->LogDebug*///CA("AP46CreateMedia::Update::cellPanelWidgetUnKnown == nil");
			break;
		}
		//CA("174");
		InterfacePtr<IPanelControlData>cellPanelWidgetPanelControlData(cellPanelWidgetUnKnown,UseDefaultIID());
		if(cellPanelWidgetPanelControlData == nil)
		{
			/*ptrIAppFramework->LogDebug*///CA("AP46CreateMedia::Update::cellPanelWidgetPanelControlData == nil");
			break;
		}
		//MediatorClass :: iPanelCntrlDataPtr = cellPanelWidgetPanelControlData;/*19-jan*/
		int32 listBoxRowIndex = cellPanelWidgetPanelControlData->GetIndex(primaryResourcePanelWidgetControlView);
		PMString ss("listBoxRowIndex :");
		ss.AppendNumber(listBoxRowIndex);
		//CA(ss);
		
		if(isSelected[listBoxRowIndex])
		{
			//CA("Selected kTrue;");
			isSelected[listBoxRowIndex] = kFalse;
		
		}
		else
		{
			//CA("Selected False");
			isSelected[listBoxRowIndex] =  kTrue;
		}
		
		bool16 isPressedIn = controlData->IsSelected();
		
		//added
	//	if(isPressedIn)
	//	{
	//		//CA("isPressedIn ==kTrue");
	//	}
	//	else
	//	{
	//
	//		//CA("isPressedIn ==kFalse");
	//	}	

		if(isSelected[listBoxRowIndex])  // Selectecd Star
		{//// Showing Check Icon			
			//CA("controlData->select..line226");
			controlData->Select();  			
			rsrcID = fontSpec->GetHiliteFontID();
		}else  // Unselected Star
		{// Showing Unchecked Icon			
			//CA("controlData->Deselect");
			controlData->Deselect();			
			rsrcID =fontSpec->GetFontID(); 			
		}	

		InterfacePtr<IControlView> myView(this, UseDefaultIID());
		if(myView == nil)
		{
			//CA("RLCustomIconWidgetEH::LButtonDn - No IControlView found on this boss.");
			break;
		}
		
	//	if(MediatorClass ::IsCommonMasterForAllPublicationRadioWidgetSelected)
		{
			//CA("here in IsCommonMasterForAllPublicationRadioWidgetSelected");

			InterfacePtr<ITriStateControlData>selectAllSectionCheckBoxTriState(SelectAllControlView,UseDefaultIID());
			if(selectAllSectionCheckBoxTriState == nil)
			{
				//CA("selectAllSectionCheckBoxTriState == nil");
				break;
			}
			//CA("Deselect it everytime ");
			//MediatorClass ::IsEventSourceInListBox = kTrue;
			selectAllSectionCheckBoxTriState->Deselect(kTrue,kFalse);
			//MediatorClass ::IsEventSourceInListBox = kFalse;
		}

		//CA(" Chetan's check.....");
		bool16 flag = kTrue;
		for(int index=0;index < isSelected.size();index++)
		{
			if(isSelected[index]);
			else flag = kFalse;
		}		
		if(flag)
		{
			InterfacePtr<ITriStateControlData>selectAllSectionCheckBoxTriState(SelectAllControlView ,UseDefaultIID());
			if(selectAllSectionCheckBoxTriState == nil)
			{
				//CA("selectAllSectionCheckBoxTriState == nil");
				return kTrue;//break;
			}
			//CA("kSelectAllCheckBoxWidgetID selecting kTrue in chetean's code");
		//	MediatorClass ::IsEventSourceInListBox = kTrue;
			selectAllSectionCheckBoxTriState->Select(kTrue);
		//	MediatorClass ::IsEventSourceInListBox = kFalse;
		}


		if(rsrcID != 0) {
			//CA("rsrcID != 0..234 event handler..");
			myView->SetRsrcID(rsrcID);	
			myView->Invalidate();

		}

		return kTrue;
		// early return as here we have already
		// handled the event and do not need to
		// delegate any processing to the base class.
	} while(0);


	return CEventHandler::LButtonDn(e);
}




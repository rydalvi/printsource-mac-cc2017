# ifndef _ISubSectionSprayer__h_
# define _ISubSectionSprayer__h_

#include "IPMUnknown.h"
#include "SSSID.h"

class ISubSectionSprayer : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_ISUBSECTIONSPRAYER };

	virtual void setLeftMargin(PMReal)=0;
	virtual void setRightMargin(PMReal)=0;
	virtual void setTopMargin(PMReal)=0;
	virtual void setBottomMargin(PMReal)=0;
	
	virtual void setHorizontalBoxSpacing(PMReal)=0;
	virtual void setVerticalBoxSpacing(PMReal)=0;

	virtual void setLeftToRightVal(bool16)=0;
	virtual void setAlternatingVal(bool16)=0;
	virtual void setTopToBottomVal(bool16)=0;
	virtual void setHorizFlowType(bool16)=0;
		
	virtual bool16 getLeftToRightVal()=0;
	virtual bool16 getAlternatingVal()=0;
	virtual bool16 getTopToBottomVal()=0;
	virtual bool16 getHorizFlowType()=0;

	virtual PMReal getTopMargin()=0;
	virtual PMReal getBottomMargin()=0;
	virtual PMReal getLeftMargin()=0;
	virtual PMReal getRightMargin()=0;
	
	virtual PMReal getHorizontalBoxSpacing()=0;
	virtual PMReal getVerticalBoxSpacing()=0;

	virtual bool16 callDialog(bool16)=0;

	virtual void setSprayAllSectionsFlag(bool16)=0;
	virtual bool16 getSprayAllSectionsFlag()=0;
	//added by dattatray
	virtual bool16 getCurrentPage(UIDRef&, UIDRef&)=0;
	virtual bool16 getSelectedBoxIds(UIDList& selectUIDList)=0;
	virtual bool16 getProductStencilsList(UIDList&)=0;
	virtual bool16 getItemStencilsList(UIDList&)=0;
	virtual bool16 getHybridStencilsList(UIDList&)=0;
	//added on 8Nov..
	virtual void setSprayCustomProductOrItemListFlag(bool16 kFlag)=0; 
	virtual bool16 getSprayCustomProductOrItemListFlag()=0; 
	//ended on 8Nov..

	virtual void setAltVerticalFlow(bool16)=0;
	virtual void setVerticalFlow(bool16)=0;
	virtual void setAltHorizontalFlow(bool16)=0;
	virtual void setHorizontalFlow(bool16)=0;

	virtual bool16 getAltVerticalFlow()=0;
	virtual bool16 getVerticalFlow()=0;
    virtual bool16 getAltHorizontalFlow()=0;
	virtual bool16 getHorizontalFlow()=0;

	virtual void setSprayItemPerFrameFlag(bool16)=0;
	virtual bool16 getSprayItemPerFrameFlag()=0;
	virtual void setHorizontalFlowForAllImageSprayFlag(bool16)=0;
	virtual bool16 getHorizontalFlowForAllImageSprayFlag()=0;
	virtual bool16 getSectionStencilsList(UIDList&)=0;
	virtual bool16 getIsWithoutPageBreakFlag() = 0;
	virtual bool16 getIsAddSectionStencilFlag() = 0;
	virtual bool16 getIsAtStartOfSectionFlag() = 0;
	virtual bool16 getIsAtStartOfEachPageFlag() = 0;
	virtual bool16 getIsAtStartOfFirstPageFlag() = 0;

	virtual void setIsWithoutPageBreakFlag(bool16 IsAtStartFlag) = 0;
	virtual void setIsAddSectionStencilFlag(bool16 ISSectionStencil)= 0;
	virtual void setIsAtStartOfSectionFlag(bool16 ISSectionStencil)=0;
	virtual void setIsAtStartOfEachPageFlag(bool16 IsAtStartOfEachPage)=0;
	virtual void setIsAtStartOfFirstPageFlag(bool16 IsAtStartOfFirstPag)=0;
	
	virtual void setProductStencilsList(UIDList pUidList)=0;
	virtual void setItemStencilsList(UIDList pUidList)=0;
	virtual void setHybridStencilsList(UIDList pUidList)=0;
	virtual void setSectionStencilsList(UIDList pUidList)=0;
	
	virtual bool16 getIsSectionSprayFlag() = 0;
	virtual void setIsSectionSprayFlag(bool16)=0;


};

# endif
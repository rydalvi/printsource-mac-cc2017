#include "VCPlugInHeaders.h"
#include "HelperInterface.h"
#include "ISubSectionSprayer.h"
#include "CPMUnknown.h"
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SSSID.h"
#include "Mediator.h"
#include "ILayoutControlData.h"
#include "IDataBase.h"
#include "IGeometry.h"
#include "IHierarchy.h"
#include "ILayoutUIUtils.h" //Cs4
#include "UIDRef.h"
#include "CAlert.h"
#include "ISelectionManager.h"
#include "ITextMiscellanySuite.h"
#include "ILayoutControlData.h"
#include "IDocument.h"
#include "IHierarchy.h"
#include "ITagReader.h"
#include "ISelectionUtils.h"
#include "IDataSprayer.h"
#include "SDKListBoxHelper.h"
#include "TagStruct.h"
#include "iterator"
#include "IIDXMLElement.h"
#include "ISpread.h"
#include "PMString.h"
#include "UIDList.h"
#include <vector>
#include "IPanelControlData.h"
#include "IPageItemTypeUtils.h"
#include "ILoginHelper.h"
#include "ITextControlData.h"
#include"IDialogController.h"


#define CA(x) CAlert::InformationAlert(x)
using namespace std;
vector<PMString> dataString;
typedef vector<UID> PageUIDList;
PageUIDList pageUidList;
int32 PageCount=0;
extern bool16 IsWithoutPageBreakFlag;
extern bool16 IsAddSectionStencilFlag;
extern bool16 IsAtStartOfSectionFlag;
extern bool16 IsAtStartOfEachPageFlag;
extern bool16 IsAtStartOfFirstPageFlag;

UIDList getselectUIDList; //It is used in getProductStencilsList and getItemStencilsList,getHybridStencilsList,getSectionStencilsList .

extern UIDList productSelUIDList;
extern UIDList itemSelUIDList;
extern UIDList hybridSelUIDList;
extern UIDList sectionSelUIDList;
//by amarjit patil
extern PMReal Vspace;
extern PMReal Hspace;

bool16 IsSectionSprayFlag = kFalse;

bool16 doesExist(TagList &tagList,const UIDList &selectUIDList);
bool16 isSingleSprayFlag = kFalse;

class SubSectionSprayer : public CPMUnknown<ISubSectionSprayer>
{
private:
	static PMReal horizontalBoxSpacing;
	static PMReal verticalBoxSpacing;
	static PMReal leftMargin;
	static PMReal rightMargin;
	static PMReal topMargin;
	static PMReal bottomMargin;
	static bool16 leftToRight;
	static bool16 topToBottom;
	static bool16 alternating;
	static bool16 firstHorizFlow;
	static bool16 SprayAllSectionsFlag;

	//added on 8Nov..
	static bool16 isSprayProductOrItemListFlag;
	//ended on 8Nov..

	static bool16 AltVerticalFlow;
	static bool16 VerticaleFlow;
	static bool16 AltHorizontalFlow;
	static bool16 HorizontalFlow;
	static bool16 SprayItemPerFrameFlag;
	static bool16 HorizontalFlowForAllImageSprayFlag;		
public:
	SubSectionSprayer(IPMUnknown* boss);
	virtual ~SubSectionSprayer()
	{
		
	}
	void setAlternatingVal(bool16);
	void setTopToBottomVal(bool16);
	void setLeftToRightVal(bool16);
	void setBottomMargin(PMReal);
	void setTopMargin(PMReal);
	void setRightMargin(PMReal);
	void setLeftMargin(PMReal);
	void setVerticalBoxSpacing(PMReal);
	void setHorizontalBoxSpacing(PMReal);
	void setHorizFlowType(bool16);
	void setSprayAllSectionsFlag(bool16);
	bool16 getAlternatingVal();
	bool16 getTopToBottomVal();
	bool16 getLeftToRightVal();
	PMReal getBottomMargin();
	PMReal getTopMargin();
	PMReal getRightMargin();
	PMReal getLeftMargin();
	PMReal getVerticalBoxSpacing();
	PMReal getHorizontalBoxSpacing();
	bool16 getHorizFlowType();
	bool16 getSprayAllSectionsFlag();
	bool16 callDialog(bool16);
	//Added By Dattatray on 26/07
	bool16 getCurrentPage(UIDRef&, UIDRef&);
	bool16 getSelectedBoxIds(UIDList& selectUIDList);
	bool16 getProductStencilsList(UIDList&);
	bool16 getItemStencilsList(UIDList&);
	bool16 getHybridStencilsList(UIDList&);
	bool16 getSectionStencilsList(UIDList&);
    //added on 8Nov..
	void setSprayCustomProductOrItemListFlag(bool16 kFlag) ;
	bool16 getSprayCustomProductOrItemListFlag() ;
	//ended on 8Nov..

	void setAltVerticalFlow(bool16);
	void setVerticalFlow(bool16);
	void setAltHorizontalFlow(bool16);
	void setHorizontalFlow(bool16);

	bool16 getAltVerticalFlow();
	bool16 getVerticalFlow();
    bool16 getAltHorizontalFlow();
	bool16 getHorizontalFlow();

	void setSprayItemPerFrameFlag(bool16);
	bool16 getSprayItemPerFrameFlag();
	void setHorizontalFlowForAllImageSprayFlag(bool16);
	bool16 getHorizontalFlowForAllImageSprayFlag();
	bool16 getIsWithoutPageBreakFlag();
	bool16 getIsAddSectionStencilFlag();
	bool16 getIsAtStartOfSectionFlag();
	bool16 getIsAtStartOfEachPageFlag();
	bool16 getIsAtStartOfFirstPageFlag();

	void setIsWithoutPageBreakFlag(bool16 IsAtStartFlag);
	void setIsAddSectionStencilFlag(bool16 ISSectionStencil);
	void setIsAtStartOfSectionFlag(bool16 ISSectionStencil);
	void setIsAtStartOfEachPageFlag(bool16 IsAtStartOfEachPage);
	void setIsAtStartOfFirstPageFlag(bool16 IsAtStartOfFirstPag);

	void setProductStencilsList(UIDList pUidList);
	void setItemStencilsList(UIDList pUidList);
	void setHybridStencilsList(UIDList pUidList);
	void setSectionStencilsList(UIDList pUidList);
	
	bool16 getIsSectionSprayFlag();
	void setIsSectionSprayFlag(bool16);
};

CREATE_PMINTERFACE(SubSectionSprayer, kSubSectionSprayerImpl)

SubSectionSprayer::SubSectionSprayer(IPMUnknown* boss): 
	CPMUnknown<ISubSectionSprayer>(boss)
{
	/*horizontalBoxSpacing = 0.0;
	verticalBoxSpacing = 0.0;
	leftMargin= 0.0;
	rightMargin= 0.0;
	topMargin= 0.0;
	bottomMargin= 0.0;
	
	leftToRight=kTrue;
	topToBottom=kTrue;
	alternating=kFalse;
	firstHorizFlow=kTrue;*/
}

PMReal SubSectionSprayer::horizontalBoxSpacing=5.0;
PMReal SubSectionSprayer::verticalBoxSpacing=5.0;
PMReal SubSectionSprayer::leftMargin=0.0;
PMReal SubSectionSprayer::rightMargin=0.0;
PMReal SubSectionSprayer::topMargin=0.0;
PMReal SubSectionSprayer::bottomMargin=0.0;
bool16 SubSectionSprayer::leftToRight=kTrue;
bool16 SubSectionSprayer::topToBottom=kTrue;
bool16 SubSectionSprayer::alternating=kFalse;
bool16 SubSectionSprayer::firstHorizFlow=kTrue;
bool16 SubSectionSprayer::SprayAllSectionsFlag= kFalse;

bool16 SubSectionSprayer::AltVerticalFlow = kFalse;
bool16 SubSectionSprayer::VerticaleFlow = kTrue;

bool16 SubSectionSprayer::AltHorizontalFlow = kFalse;
bool16 SubSectionSprayer::HorizontalFlow = kFalse;

//added on 8Nov..
bool16 SubSectionSprayer::isSprayProductOrItemListFlag = kFalse;
//ended on 8Nov..
bool16  SubSectionSprayer::SprayItemPerFrameFlag = kFalse;
bool16  SubSectionSprayer::HorizontalFlowForAllImageSprayFlag = kFalse;

PMReal SubSectionSprayer::getHorizontalBoxSpacing()
{
	//CA("inside getHorizontalBoxSpacing");
    /*
	InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
	if(ptrLogInHelper == nil)
	{
		CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
		return  horizontalBoxSpacing;
	}

	LoginInfoValue cserverInfoValue;	
	bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
	if(result)
	{
		ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();

		// Added by Prabhat Singh
		//int32 horizontalspace1 = ToInt32(Hspace);  
		//clientInfoObj.setHorizontalSpacing(horizontalspace1);
		// till here
		horizontalBoxSpacing = clientInfoObj.getHorizontalSpacing();
		if(horizontalBoxSpacing == 0)
		{
			//CA("if(horizontalBoxSpacing == 0)");
			horizontalBoxSpacing = Hspace;//5;

		}
	}
     */
	return horizontalBoxSpacing;
}

PMReal SubSectionSprayer::getVerticalBoxSpacing()
{
	//CA("inside getVerticalBoxSpacing");
    /*
	InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
	if(ptrLogInHelper == nil)
	{
		CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
		return verticalBoxSpacing;
	}
	PMString str="";
	str.AppendNumber(Vspace);
	//CA("Vertical Space in  getVertcalBoxSpacing: " + str);

	LoginInfoValue cserverInfoValue;	
	bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
	if(result)
	{
		ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
	
		// Added by Prabhat Singh
		//int32 verticalspace1 = ToInt32(Vspace);  
		//cserverInfoValue.setVerticalSpacing(verticalspace1);
		// till here
		verticalBoxSpacing = clientInfoObj.getVerticalSpacing();
		//PMString str="";
		//str.AppendNumber(verticalBoxSpacing);
		//CA("verticalBoxSpacing 259 : " + str);

		if(verticalBoxSpacing == 0)
		{
			//CA("if(verticalBoxSpacing == 0)");
			//by amarjit patil
			verticalBoxSpacing = Vspace;//5;
			
			
		}
	}
    */
	return verticalBoxSpacing;
}

PMReal SubSectionSprayer::getLeftMargin()
{
	return leftMargin;
}

PMReal SubSectionSprayer::getRightMargin()
{
	return rightMargin;
}

PMReal SubSectionSprayer::getTopMargin()
{
	return topMargin;
}

PMReal SubSectionSprayer::getBottomMargin()
{
	return bottomMargin;
}

bool16 SubSectionSprayer::getLeftToRightVal()
{
	return leftToRight;
}

bool16 SubSectionSprayer::getTopToBottomVal()
{
	return topToBottom;
}

bool16 SubSectionSprayer::getAlternatingVal()
{
	return alternating;
}

bool16 SubSectionSprayer::getHorizFlowType()
{
	return firstHorizFlow;
}

bool16 SubSectionSprayer::getSprayAllSectionsFlag()
{
	return SprayAllSectionsFlag;
}

void SubSectionSprayer::setHorizontalBoxSpacing(PMReal hbs)
{
	horizontalBoxSpacing=hbs;
}

void SubSectionSprayer::setVerticalBoxSpacing(PMReal vbs)
{
	verticalBoxSpacing=vbs;
}

void SubSectionSprayer::setLeftMargin(PMReal lmargin)
{
	leftMargin=lmargin;
}

void SubSectionSprayer::setRightMargin(PMReal rmargin)
{
	rightMargin=rmargin;
}

void SubSectionSprayer::setTopMargin(PMReal tmargin)
{
	topMargin=tmargin;
}

void SubSectionSprayer::setBottomMargin(PMReal bmargin)
{
	bottomMargin=bmargin;
}

void SubSectionSprayer::setLeftToRightVal(bool16 ltor)
{
	leftToRight=ltor;
}

void SubSectionSprayer::setTopToBottomVal(bool16 ttob)
{
	topToBottom=ttob;
}

void SubSectionSprayer::setAlternatingVal(bool16 isAlt)
{
	alternating=isAlt;
}

void SubSectionSprayer::setHorizFlowType(bool16 flow)
{
	firstHorizFlow = flow;
}

void SubSectionSprayer::setSprayAllSectionsFlag(bool16 flow)
{
	SprayAllSectionsFlag = flow;
}

bool16 SubSectionSprayer::callDialog(bool16 isFlagSingleSpray)
{
	bool16 result = kFalse;
	do
	{
		isSingleSprayFlag = isFlagSingleSpray;
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if (application == nil)
		{
			ASSERT_FAIL("SSSActionComponent::DoAction: application invalid"); 
			break;
		}

		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("SSSActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kSSSPluginID,			// Our Plug-in ID from SSSID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal/*kModeless*/);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("SSSActionComponent::DoAction: can't create dialog"); 
			break;
		}

		// Open the dialog.
		dialog->Open(); 
	
		//dialog->WaitForDialog();

		if(Mediator::hasUserCancelled)
			break;

		result = kTrue;
	}while (false);
	return result;
}

//added by dattatay on 26/07
bool16 SubSectionSprayer::getCurrentPage(UIDRef& pageUIDRef, UIDRef& spreadUIDRef)
{
	bool16 result = kFalse;
	do
	{
		InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()/*::QueryFrontLayoutData()*/); //Cs4
		if (layoutData == nil)
			break;
		IDocument* document = layoutData->GetDocument();
		if (document == nil)
			break;
		IDataBase* database = ::GetDataBase(document);
		if(!database)
			break;
		UID pageUID = layoutData->GetPage();
		if(pageUID == kInvalidUID)
			break;
		int32/*int64*/ CurrentPageIndex= static_cast<int32>(pageUidList.size());
		if(CurrentPageIndex== 0 )  //CurrentPageIndex== PageCount)
		{
			UIDRef pageRef(database, pageUID);
			pageUIDRef = pageRef;
			//Commented by Nitin
			//IGeometry* spreadGeomPtr = layoutData->GetSpread();
			//if(spreadGeomPtr == nil)
			//	break;
			//InterfacePtr<IHierarchy> hierarchyPtr(spreadGeomPtr, IID_IHIERARCHY);
			//if(hierarchyPtr == nil)
			//	break;
			//UID spreadUID = hierarchyPtr->GetSpreadUID();
			//UIDRef spreadRef(database, spreadUID);
			//spreadUIDRef = spreadRef;
			//Added by Nitin---from here to-----
			//Get current spread UIDRef, the spread this view is currently viewing
			const UIDRef spreadRef=layoutData->GetSpreadRef();
			spreadUIDRef = spreadRef;
			//upto here--------------
			result = kTrue;		
			break;
		}
		else if(CurrentPageIndex!=PageCount)
		{
			if(pageUidList[CurrentPageIndex-1]!= pageUID)
			{
				UIDRef pageRef(database, pageUID);
				pageUIDRef = pageRef;
				//Commented by nitin-----
				//IGeometry* spreadGeomPtr = layoutData->GetSpread();
				//if(spreadGeomPtr == nil)
				//	break;
				//InterfacePtr<IHierarchy> hierarchyPtr(spreadGeomPtr, IID_IHIERARCHY);
				//if(hierarchyPtr == nil)
				//	break;
				//UID spreadUID = hierarchyPtr->GetSpreadUID();
				//UIDRef spreadRef(database, spreadUID);
				//spreadUIDRef = spreadRef;
				//Following Code Added by Nitin -- from here to---
				//Get current spread UIDRef, the spread this view is currently viewing
				const UIDRef spreadRef=layoutData->GetSpreadRef();
				spreadUIDRef = spreadRef;
				//Upto here-----
				result = kTrue;				
				break;		
			}
			else
			{
				//Commented by nitin
				//IGeometry* spreadItem= layoutData->GetSpread();
				//if(spreadItem == nil)
				//	return kFalse;
				
				//InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
				
				//Added by nitin from here to-----
				UIDRef spreadRef=layoutData->GetSpreadRef();
				InterfacePtr<ISpread> iSpread(spreadRef, UseDefaultIID());
				//upto here------
				if (iSpread == nil)
					return kFalse;

				int numPages=iSpread->GetNumPages();
				int OldPageIndex= iSpread->GetPageIndex(pageUID);
				if(numPages >= OldPageIndex+1 )
				 pageUID= iSpread->GetNthPageUID(OldPageIndex+1);

				UIDRef pageRef(database, pageUID);
				pageUIDRef = pageRef;
			//Commented ny nitin
				//IGeometry* spreadGeomPtr = layoutData->GetSpread();
				//if(spreadGeomPtr == nil)
				//	break;
				//InterfacePtr<IHierarchy> hierarchyPtr(spreadGeomPtr, IID_IHIERARCHY);
				//if(hierarchyPtr == nil)
				//	break;
				//UID spreadUID = hierarchyPtr->GetSpreadUID();
				//UIDRef spreadRef(database, spreadUID);
				//spreadUIDRef = spreadRef;
				//Added by Nitin From Here to ----				
				spreadUIDRef = spreadRef;
				//Upto Here--------
				result = kTrue;				
				break;		
			}
		}

	}
	while(kFalse);
   return result;
}

bool16 SubSectionSprayer::getSelectedBoxIds(UIDList& selectUIDList)
{	
	InterfacePtr<ITagReader> itagReader
		   ((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
		return kFalse;

	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
		return kFalse;
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(!txtMisSuite)
		return kFalse; 
	txtMisSuite->GetUidList(selectUIDList);
	const int32 listLength=selectUIDList.Length();
	if(listLength==0)
		return kFalse;
	///////////////////////for passing database pointer////////////////
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()/*::QueryFrontLayoutData()*/); //Cs4
	if (layoutData == nil)
		return kFalse;
	IDocument* document = layoutData->GetDocument();
	if (document == nil)
		return kFalse;
	IDataBase* database = ::GetDataBase(document);
	if(!database)
		return kFalse;
	UIDList TempUIDList(database);
	TagList NewList;
	
	UIDList tempList(selectUIDList.GetDataBase());
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		if(!iHier)
		{
			//CA(" !iHier >> Continue ");
			continue;
		}
		UID kidUID;
		
		int32 numKids=iHier->GetChildCount();
		//PMString ASD("numKids : ");
		//ASD.AppendNumber(numKids);
		//CA(ASD);

		bool16 isGroupFrame = kFalse ;
		isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(i));

		if(isGroupFrame == kTrue) 
		{
			IIDXMLElement* ptr = NULL;
			for(int j=0;j<numKids;j++)
			{
				kidUID=iHier->GetChildUID(j);
				UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);	


				InterfacePtr<IHierarchy> iHierarchy(boxRef, UseDefaultIID());
				if(!iHierarchy)
				{
					//CA(" !iHier >> Continue ");
					continue;
				}
				UID newkidUID;
				
				int32 numNewKids=iHierarchy->GetChildCount();
				/*PMString ASD("numKids ................: ");
				ASD.AppendNumber(numKids);
				CA(ASD);*/

				bool16 isGroupFrameAgain = kFalse ;
				isGroupFrameAgain = Utils<IPageItemTypeUtils>()->IsGroup(boxRef);

				if(isGroupFrameAgain == kTrue) 
				{
					IIDXMLElement* newPtr = NULL;
					for(int k=0;k<numNewKids;k++)
					{
						//CA("Inside For Loop");
						newkidUID=iHierarchy->GetChildUID(k);
						UIDRef childBoxRef(selectUIDList.GetDataBase(), newkidUID);	

					
						//CA("isGroupFrame == kTrue");
						TagList NewList = itagReader->getTagsFromBox(childBoxRef, &newPtr);
						
						/*PMString s("NewList.size() : ");
						s.AppendNumber(NewList.size());
						CA(s);*/
					
					

						if(!doesExist(NewList,tempList))
						{
							tempList.Append(newkidUID);				
							//CA("After Appending to tempList");
						}
						//------------
						for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
						{
							NewList[tagIndex].tagPtr->Release();
						}
					}
				}
				else
				{
					TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
					
					/*PMString s("NewList.size() : ");
					s.AppendNumber(NewList.size());
					CA(s);*/
				
				

					if(!doesExist(NewList,tempList))
					{
						tempList.Append(kidUID);				
					}
					for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
					{
						NewList[tagIndex].tagPtr->Release();
					}
				}			
			}
		}
		else
		{
			//CA("isGroupFrame == kFalse");
			tempList.Append(selectUIDList.GetRef(i).GetUID());
		}
	}

	UIDList OriginalFramesUIDList(selectUIDList.GetDataBase());
	OriginalFramesUIDList = selectUIDList;
	selectUIDList = tempList;

	for(int i=0; i<selectUIDList.Length(); i++)
	{
		//InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		//if(!iHier)
		//	continue;

		//UID kidUID;
		//int32 numKids=iHier->GetChildCount();
		/*PMString temp = "";
		temp.AppendNumber(numKids);
		CA("childCount = " + temp);*/

		IIDXMLElement* ptr = NULL;

//		for(int j=0;j<numKids;j++)
//	   {
//		   kidUID=iHier->GetChildUID(j);
			
		 /*  PMString uid ="kidUID = ";
		   uid.AppendNumber((uint32)kidUID.Get());
		   uid.Append(" , ");
		   UID temp = selectUIDList.GetRef(i).GetUID();
		   uid.AppendNumber((uint32)temp.Get());
		   CA(uid);*/
		
//		   UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
		   
			
		    NewList=itagReader->getTagsFromBox(selectUIDList.GetRef(i), &ptr);
			vector<TagStruct>::iterator itertr;
	//Push the text and image tag which we got from getTagsFromBox in the NewList into vector.	
			for(itertr= NewList.begin();itertr!=NewList.end();itertr++)
			{
				PMString story;
				int32 index=0;
				IIDXMLElement* tempXml = itertr->tagPtr;//to get tag From Vector in PMString Format it is required.
				if(tempXml)
				story=tempXml->GetTagString();
				//CA(story);
				PMString tagName("");
				//To remove "_" which present in between single tag name and repace it with " "
				for(int i=0;i<story.NumUTF16TextChars();i++)
				{
					if(story.GetChar(i) =='_')
						tagName.Append(" ");
					else
						tagName.Append(story.GetChar(i));
				}
				dataString.push_back(tagName);
			}
		//This is get called when there is/are Selected Boxes.		
			if(NewList.size()<=0)//This can be a Tagged Frame
			 {	
				bool16 flaG = kFalse;
				//InterfacePtr<ITagReader> itagReader
				//((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
				/*if(!itagReader)
					return 0;*/
				NewList.clear();
				NewList=itagReader->getFrameTags(selectUIDList.GetRef(i));
				//insert box tag into Vector.
				vector<TagStruct>::iterator itertr;
				for( itertr= NewList.begin();itertr!=NewList.end();itertr++)
				{
					PMString story;
					int32 index=0;
					IIDXMLElement* tempXml = itertr->tagPtr;
					if(tempXml)
					story=tempXml->GetTagString();
					//CA(story);
					PMString tagName("");
					//To remove "_" which present in tag name and replace it with " "
					for(int i=0;i<story.NumUTF16TextChars();i++)
					{
						if(story.GetChar(i) =='_')
							tagName.Append(" ");
						else
							tagName.Append(story.GetChar(i));
					}
					dataString.push_back(tagName);
					if(NewList.size()==0)
						return 0;
			}
			//	}
		 }

			for(int32 tagIndex = 0 ; tagIndex < NewList.size(); tagIndex++)
			{
				NewList[tagIndex].tagPtr->Release();
			}
	}

	//for(int32 i= selectUIDList.Length()-1; i>=0; i--)
	//	TempUIDList.Append(selectUIDList.GetRef(i).GetUID());
	//selectUIDList = TempUIDList;
	// CA("Inside For 4");
	//getselectUIDList = TempUIDList;//getselectUIDList is declared as a global and used in getProductStencilsList and getItemStencilsList.
	getselectUIDList= OriginalFramesUIDList;
	selectUIDList = OriginalFramesUIDList;
	//-----------
	

	return kTrue;
}

////////Added By Dattatray on 28/07 
bool16 SubSectionSprayer::getProductStencilsList(UIDList& pUidList){
	bool16 result=kFalse;
	int32 size=productSelUIDList.Length();
	if(size==0)
		result=kFalse;
	else
	{
		pUidList.Clear();
		pUidList = productSelUIDList;
		result=kTrue;//If UIDList is there that is Product are in ListBox return kTrue
	}
	return result;
}


bool16 SubSectionSprayer::getItemStencilsList(UIDList& pUidList){
	bool16 result=kFalse;
	int32 size=itemSelUIDList.Length();
	if(size==0)
		result=kFalse;
	else
	{
		pUidList.Clear();
		pUidList = itemSelUIDList;//If UIDList is there i.e Items are in the ListBox return kTrue
		result=kTrue;
	}
	return result;
}
///////////////	Added By Amit on 1/9/07
bool16 SubSectionSprayer::getHybridStencilsList(UIDList& pUidList){
	bool16 result=kFalse;
	int32 size=hybridSelUIDList.Length();
	if(size==0)
		result=kFalse;
	else
	{
		pUidList.Clear();
		pUidList = hybridSelUIDList;
		result=kTrue;//If UIDList is there that is Product are in ListBox return kTrue
	}
	return result;
}
bool16 SubSectionSprayer::getSectionStencilsList(UIDList& pUidList){
	bool16 result = kFalse;
	int32 size = sectionSelUIDList.Length();
	if(size == 0)
		result=kFalse;
	else
	{
		pUidList.Clear();
		pUidList = sectionSelUIDList;
		result = kTrue;//If UIDList is there that is Product are in ListBox return kTrue
	}
	return result;
}
///////////////	End

//added on 8Nov..
void SubSectionSprayer::setSprayCustomProductOrItemListFlag(bool16 kFlag) 
{
	isSprayProductOrItemListFlag = kFlag;
}

bool16 SubSectionSprayer::getSprayCustomProductOrItemListFlag() 
{
	return isSprayProductOrItemListFlag;
}
//ended on 8Nov..

void SubSectionSprayer::setAltVerticalFlow(bool16 FlowFlag)
{
	AltVerticalFlow = FlowFlag;
}

void SubSectionSprayer::setVerticalFlow(bool16 FlowFlag)
{
	VerticaleFlow = FlowFlag;
}

void SubSectionSprayer::setAltHorizontalFlow(bool16 FlowFlag)
{
	AltHorizontalFlow = FlowFlag;
}

void SubSectionSprayer::setHorizontalFlow(bool16 FlowFlag)
{
	HorizontalFlow = FlowFlag;
}

bool16 SubSectionSprayer::getAltVerticalFlow()
{	
	return AltVerticalFlow;
}

bool16 SubSectionSprayer::getVerticalFlow()
{	
	return VerticaleFlow;
}

bool16 SubSectionSprayer::getAltHorizontalFlow()
{
	return AltHorizontalFlow;
}

bool16 SubSectionSprayer::getHorizontalFlow()
{
	return HorizontalFlow;
}

void SubSectionSprayer::setSprayItemPerFrameFlag(bool16 Flag)
{
	SprayItemPerFrameFlag=Flag;
}

bool16 SubSectionSprayer::getSprayItemPerFrameFlag()
{
	return SprayItemPerFrameFlag;
}

void SubSectionSprayer::setHorizontalFlowForAllImageSprayFlag(bool16 Flag)
{
	HorizontalFlowForAllImageSprayFlag=Flag;
}

bool16 SubSectionSprayer::getHorizontalFlowForAllImageSprayFlag()
{
	return HorizontalFlowForAllImageSprayFlag;
}

bool16 SubSectionSprayer::getIsWithoutPageBreakFlag()
{
	return IsWithoutPageBreakFlag;
}

bool16 SubSectionSprayer::getIsAddSectionStencilFlag()
{
	return IsAddSectionStencilFlag;
}

bool16 SubSectionSprayer::getIsAtStartOfSectionFlag()
{
	return IsAtStartOfSectionFlag;
}

bool16 SubSectionSprayer::getIsAtStartOfEachPageFlag()
{
	return IsAtStartOfEachPageFlag;
}

bool16 SubSectionSprayer::getIsAtStartOfFirstPageFlag()
{
	return IsAtStartOfFirstPageFlag;
}

bool16 doesExist(TagList &tagList,const UIDList &selectUIDList)
{
	bool16 result = kFalse;
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		return kFalse;
	}
	TagList tList;
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		tList = itagReader->getTagsFromBox(selectUIDList.GetRef(i));

		if(tList.size()==0||tagList.size()==0 || !tList[0].tagPtr || !tagList[0].tagPtr )
			continue;

		if(tagList[0].tagPtr == tList[0].tagPtr )
		{
			//CA("return kTrue");
			result = kTrue;
		}

		for(int32 tagIndex = 0 ; tagIndex < tList.size(); tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		if(result)
			break;
	}
	//CA("return kFalse");
	return result;
}


void SubSectionSprayer::setIsWithoutPageBreakFlag(bool16 IsAtStartFlag)
{
	IsWithoutPageBreakFlag = IsAtStartFlag;
}

void SubSectionSprayer::setIsAddSectionStencilFlag(bool16 ISSectionStencil)
{
	IsAddSectionStencilFlag = ISSectionStencil;
}

void SubSectionSprayer::setIsAtStartOfSectionFlag(bool16 ISSectionStencil)
{
	IsAtStartOfSectionFlag = ISSectionStencil;
}

void SubSectionSprayer::setIsAtStartOfEachPageFlag(bool16 IsAtStartOfEachPage)
{
	IsAtStartOfEachPageFlag = IsAtStartOfEachPage;
}

void SubSectionSprayer::setIsAtStartOfFirstPageFlag(bool16 IsAtStartOfFirstPag)
{
	IsAtStartOfFirstPageFlag = IsAtStartOfFirstPag;
}


void SubSectionSprayer::setProductStencilsList(UIDList pUidList)
{	
	productSelUIDList = pUidList;
}

void SubSectionSprayer::setItemStencilsList(UIDList pUidList)
{
	itemSelUIDList = pUidList;
	
}
void SubSectionSprayer::setHybridStencilsList(UIDList pUidList)
{	
	hybridSelUIDList = pUidList;
	
}

void SubSectionSprayer::setSectionStencilsList(UIDList pUidList)
{
	sectionSelUIDList = pUidList;
}


bool16 SubSectionSprayer::getIsSectionSprayFlag()
{
	return IsSectionSprayFlag;
}

void SubSectionSprayer::setIsSectionSprayFlag(bool16 sectionSpray)
{
	IsSectionSprayFlag=sectionSpray;
}
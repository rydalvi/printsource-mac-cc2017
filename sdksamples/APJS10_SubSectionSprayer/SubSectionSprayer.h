# ifndef _ISubSectionSprayer__h_
# define _ISubSectionSprayer__h_

#include "IPMUnknown.h"
#include "SSSID.h"

class ISubSectionSprayer : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_ISUBSECTIONSPRAYER };

	virtual void setLeftMargin(PMReal)=0;
	virtual void setRightMargin(PMReal)=0;
	virtual void setTopMargin(PMReal)=0;
	virtual void setBottomMargin(PMReal)=0;
	
	virtual void setHorizontalBoxSpacing(PMReal)=0;
	virtual void setVerticalBoxSpacing(PMReal)=0;

	virtual void setLeftToRightVal(bool16)=0;
	virtual void setAlternatingVal(bool16)=0;
	virtual void setTopToBottomVal(bool16)=0;
	virtual void setHorizFlowType(bool16)=0;
		
	virtual bool16 getLeftToRightVal()=0;
	virtual bool16 getAlternatingVal()=0;
	virtual bool16 getTopToBottomVal()=0;
	virtual bool16 getHorizFlowType()=0;

	virtual PMReal getTopMargin()=0;
	virtual PMReal getBottomMargin()=0;
	virtual PMReal getLeftMargin()=0;
	virtual PMReal getRightMargin()=0;
	
	virtual PMReal getHorizontalBoxSpacing()=0;
	virtual PMReal getVerticalBoxSpacing()=0;

	virtual bool16 callDialog(void)=0;

	virtual void setSprayAllSectionsFlag(bool16)=0;
	virtual bool16 getSprayAllSectionsFlag()=0;

};

# endif
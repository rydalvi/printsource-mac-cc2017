//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
// General includes:
#include "CDialogObserver.h"
// Project includes:
#include "SSSID.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "ISubSectionSprayer.h"
#include "SubSectionSprayer.h"
#include "UIDRef.h"
#include "UIDList.h"
// General includes:
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"

// Project includes:

#include "CAlert.h"
#include "SDKListBoxHelper.h"
#include "HelperInterface.h"

#include "IStringListControlData.h"
#include "ITextControlData.h"
#include "IDialogController.h"
#include "Mediator.h"

#include <vector>
#include "ISelectionManager.h"
#include "IGroupItemSuite.h"
#include "Utils.h"
#include "ISelectionUtils.h"
#include "ILoginHelper.h"

#include "ISubSectionSprayer.h"
#include "IClientOptions.h"

#define CA(x) CAlert::InformationAlert(x)
using namespace std;
extern vector<PMString> dataString;
typedef vector<UID> PageUIDList;
extern PageUIDList pageUidList;
extern int32 PageCount;
//by amarjit patil
PMReal Vspace=5.0;
PMReal Hspace=5.0;

UIDList productSelUIDList;
UIDList itemSelUIDList;
UIDList hybridSelUIDList;
UIDList sectionSelUIDList;

bool16 IsWithoutPageBreakFlag = kFalse;
bool16 IsAddSectionStencilFlag = kFalse;
bool16 IsAtStartOfSectionFlag = kFalse;
bool16 IsAtStartOfEachPageFlag = kFalse;
bool16 IsAtStartOfFirstPageFlag = kFalse;

/** SSSDialogObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author Anurag
*/
class SSSDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		SSSDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~SSSDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(SSSDialogObserver, kSSSDialogObserverImpl)

/* AutoAttach
*/
void SSSDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();
	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("SSSDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		// Now attach to SSSprayer's info button widget.
		/*AttachToWidget(kOptionsDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		AttachToWidget(kTopMarginWidgetID, IID_ITEXTCONTROLDATA, panelControlData);
		AttachToWidget(kBottomMarginWidgetID, IID_ITEXTCONTROLDATA, panelControlData);
		AttachToWidget(kLeftMarginWidgetID, IID_ITEXTCONTROLDATA, panelControlData);
		AttachToWidget(kRightMarginWidgetID, IID_ITEXTCONTROLDATA, panelControlData);
		AttachToWidget(kHorizontalSpacingWidgetID, IID_ITEXTCONTROLDATA, panelControlData);
		AttachToWidget(kVerticalSpacingWidgetID, IID_ITEXTCONTROLDATA, panelControlData);*/
		//AttachToWidget(kLeftToRightWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//AttachToWidget(kRightToLeftWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//AttachToWidget(kTopToBottomWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//AttachToWidget(kBottomToTopWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//AttachToWidget(kHorizontalFlowWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//AttachToWidget(kVerticalFlowWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kProductButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kItemButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kHybridButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kSSSListBoxWidgetID, IID_ILISTCONTROLDATA, panelControlData);
		AttachToWidget(kSSSListBoxTwoWidgetID, IID_ILISTCONTROLDATA, panelControlData);
		AttachToWidget(kSSSListBoxThreeWidgetID, IID_ILISTCONTROLDATA, panelControlData);
		AttachToWidget(kSectionButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kSSSListBoxFourWidgetID, IID_ILISTCONTROLDATA, panelControlData);

		AttachToWidget(kIsAllSectionSprayWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		AttachToWidget(kIsAddSectionStencilWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kIsAtStartOfEachPageWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kIsAtStartOfFirstPageWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kSectionSprayOKButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kSectionSprayCancelButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		AttachToWidget(kIsAllSectionSpraySecondLineWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		
		// Attach to other widgets you want to handle dynamically here.
		
		InterfacePtr<IPanelControlData> iPanelControlData(this,UseDefaultIID());
		if(!iPanelControlData)
			return;
		
		//--------
		//IControlView* pRedioButton =iPanelControlData->FindWidget(kSSSVerticalFlowRadioButtonWidgetID);
		//// Get ITriStateControlData interface pointer.
		//InterfacePtr<ITriStateControlData>
		//pRedioButtonController(pRedioButton, UseDefaultIID());
		//// Select the first Redio Button.
		//pRedioButtonController->Select(0);


	} while (false);
}

/* AutoDetach
*/
void SSSDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("SSSDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}
		
		// Now we detach from SSSprayer's info button widget.
		/*DetachFromWidget(kOptionsDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		DetachFromWidget(kTopMarginWidgetID, IID_ITEXTCONTROLDATA, panelControlData);
		DetachFromWidget(kBottomMarginWidgetID, IID_ITEXTCONTROLDATA, panelControlData);
		DetachFromWidget(kLeftMarginWidgetID, IID_ITEXTCONTROLDATA, panelControlData);
		DetachFromWidget(kRightMarginWidgetID, IID_ITEXTCONTROLDATA, panelControlData);
		DetachFromWidget(kHorizontalSpacingWidgetID, IID_ITEXTCONTROLDATA, panelControlData);
		DetachFromWidget(kVerticalSpacingWidgetID, IID_ITEXTCONTROLDATA, panelControlData);*/
	
		//DetachFromWidget(kLeftToRightWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//DetachFromWidget(kRightToLeftWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//DetachFromWidget(kTopToBottomWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//DetachFromWidget(kBottomToTopWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//DetachFromWidget(kHorizontalFlowWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//DetachFromWidget(kVerticalFlowWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kProductButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kSSSListBoxWidgetID, IID_ILISTCONTROLDATA, panelControlData);
		DetachFromWidget(kSSSListBoxTwoWidgetID, IID_ILISTCONTROLDATA, panelControlData);
		DetachFromWidget(kSSSListBoxThreeWidgetID, IID_ILISTCONTROLDATA, panelControlData);
		DetachFromWidget(kItemButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kHybridButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kSectionButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kSSSListBoxFourWidgetID, IID_ILISTCONTROLDATA, panelControlData);
		
		DetachFromWidget(kIsAllSectionSprayWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		
		DetachFromWidget(kIsAddSectionStencilWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kIsAtStartOfEachPageWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kIsAtStartOfFirstPageWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kSectionSprayOKButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kSectionSprayCancelButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		
		DetachFromWidget(kIsAllSectionSpraySecondLineWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		
		// Detach from other widgets you handle dynamically here.
		
	} while (false);
}

/* Update
*/
void SSSDialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("SSSDialogObserver::Update() controlView invalid");
			break;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
	
	/*  Commited by amit	
		if(theSelectedWidget == kOptionsDropDownWidgetID)
		{

			CA("Hello World");
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			if (!panelControlData)
				return;

			IControlView* marginsPanelCtrlView=panelControlData->FindWidget(kMarginsPanelWidgetID);
			if(!marginsPanelCtrlView)
				return;

			IControlView* flowPanelCtrlView=panelControlData->FindWidget(kFlowPanelWidgetID);
			if(!flowPanelCtrlView)
				return;
			
	//		IControlView* repeatingDataPanelCtrlView=panelControlData->FindWidget(kRepeatingDataPanelWidgetID);
	//		if(!repeatingDataPanelCtrlView)
	//			return;
			
			InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
			if(dlgController==nil)
				return;
			
			PMString optionSelected= dlgController->GetTextControlData(theSelectedWidget, panelControlData);

			if(optionSelected == "Margins & Spacings")
			{
				marginsPanelCtrlView->ShowView();
				flowPanelCtrlView->HideView();
			//	repeatingDataPanelCtrlView->HideView();
			}
			else if(optionSelected == "Flow")
			{
				marginsPanelCtrlView->HideView();
				flowPanelCtrlView->ShowView();
			//	repeatingDataPanelCtrlView->HideView();
			}
		//	else if(optionSelected == "Repeating Data")
		//	{
		//		marginsPanelCtrlView->HideView();
		//		flowPanelCtrlView->HideView();
		//		repeatingDataPanelCtrlView->ShowView();
		//	}
		}

		*/
/*		if(theSelectedWidget == kRightToLeftWidgetID)
		{
			InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
			if(dlgController==nil)
				return;

			dlgController->SetTriStateControlData(theSelectedWidget, kTrue, nil, kTrue, kFalse);
			dlgController->SetTriStateControlData(kLeftToRightWidgetID, kFalse, nil, kTrue, kFalse);
		}
*/
	/*	Commited by amit
		if(theSelectedWidget == kRightToLeftWidgetID)
		{
			CA("theSelectedWidget == kRightToLeftWidgetID");
			InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
			if(dlgController==nil)
				return;

			dlgController->SetTriStateControlData(theSelectedWidget, kTrue, nil, kTrue, kFalse);
			dlgController->SetTriStateControlData(kLeftToRightWidgetID, kFalse, nil, kTrue, kFalse);
		}
	
		if(theSelectedWidget == kLeftToRightWidgetID)
		{
			CA("theSelectedWidget == kLeftToRightWidgetID");
			InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
			if(dlgController==nil)
				return;

			dlgController->SetTriStateControlData(theSelectedWidget, kTrue, nil, kTrue, kFalse);
			dlgController->SetTriStateControlData(kRightToLeftWidgetID, kFalse, nil, kTrue, kFalse);
		}
		
		if(theSelectedWidget == kTopToBottomWidgetID)
		{
			CA("theSelectedWidget == kTopToBottomWidgetID");
			InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
			if(dlgController==nil)
				return;

			dlgController->SetTriStateControlData(theSelectedWidget, kTrue, nil, kTrue, kFalse);
			dlgController->SetTriStateControlData(kBottomToTopWidgetID, kFalse, nil, kTrue, kFalse);
		}
		if(theSelectedWidget == kBottomToTopWidgetID)
		{
			CA("theSelectedWidget == kBottomToTopWidgetID");
			InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
			if(dlgController==nil)
				return;

			dlgController->SetTriStateControlData(theSelectedWidget, kTrue, nil, kTrue, kFalse);
			dlgController->SetTriStateControlData(kTopToBottomWidgetID, kFalse, nil, kTrue, kFalse);
		}
		
		if(theSelectedWidget == kHorizontalFlowWidgetID)
		{
			CA("theSelectedWidget == kHorizontalFlowWidgetID");
			InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
			if(dlgController==nil)
				return;
			dlgController->SetTriStateControlData(theSelectedWidget, kTrue, nil, kTrue, kFalse);
			dlgController->SetTriStateControlData(kVerticalFlowWidgetID, kFalse, nil, kTrue, kFalse);
		}
		if(theSelectedWidget == kVerticalFlowWidgetID)
		{
			CA("theSelectedWidget == kVerticalFlowWidgetID");
			InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
			if(dlgController==nil)
				return;

			dlgController->SetTriStateControlData(theSelectedWidget, kTrue, nil, kTrue, kFalse);
			dlgController->SetTriStateControlData(kHorizontalFlowWidgetID, kFalse, nil, kTrue, kFalse);
		}
*/
		/////////added by dattatray on 28/07 to handle Product Button Click and Item Button Click..
		if(theSelectedWidget == kProductButtonWidgetID && theChange == kTrueStateMessage)
		{
			bool16 result=kFalse;
			do
			{
				UIDRef originalPageUIDRef=UIDRef::gNull;
				UIDRef originalSpreadUIDRef = UIDRef::gNull;
				pageUidList.clear();
				InterfacePtr<ISubSectionSprayer> iSubSectSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
				if(!iSubSectSprayer)
				{
					//CA("No SubSectSprayer");
					break;
				}			

				result=iSubSectSprayer->getCurrentPage(originalPageUIDRef,originalSpreadUIDRef);
				if(result == kFalse) 
					break;
				////***Added
				//InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
				//if(!iSelectionManager)
				//	break;
				//InterfacePtr<IGroupItemSuite> igpItem(iSelectionManager,UseDefaultIID());
				//if(!igpItem){
				//	//CA("Not Found");	
				//	break;
				//}
				//if(igpItem->CanItemsBeUngrouped()){
				//	ErrorCode  sucess = igpItem->UngroupItems();					
				//}
				////**** Up To Here
				PageCount=1;
				UID originalPageUID = originalPageUIDRef.GetUID();
				pageUidList.push_back(originalPageUID);
					//get the list of selected box UIDRef's
				productSelUIDList.Clear();
				result = iSubSectSprayer->getSelectedBoxIds(productSelUIDList);
				
				//SDKListBoxHelper lstHelper(this,kSSSPluginID,kSSSListBoxWidgetID,kSSSDialogWidgetID);			
				//lstHelper.EmptyCurrentListBox();

				if(result)  
				{//All the selected Items are populated in Items ListBox
			//		SDKListBoxHelper lstHelper(this,kSSSPluginID,kSSSListBoxWidgetID,kSSSDialogWidgetID);			
			//		lstHelper.EmptyCurrentListBox();

					//for(int i =static_cast<int>(dataString.size()-1);i>=0;i--)
					//	lstHelper.AddElement(dataString[i],kSSSTextWidgetID,0);

					dataString.clear();
				}
				
			}while(kFalse);
		}
		
			//added by dattatray to handle  ItemButton click Action
		else if(theSelectedWidget == kItemButtonWidgetID && theChange == kTrueStateMessage)
		{
			bool16 result=kFalse;
			do
			{
				UIDRef originalPageUIDRef=UIDRef::gNull;
				UIDRef originalSpreadUIDRef = UIDRef::gNull;
				pageUidList.clear();
				InterfacePtr<ISubSectionSprayer> iSubSectSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
				if(!iSubSectSprayer)
				{
				//CA("No SubSectSprayer");
					break;
				}
				////***Added
				//InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
				//if(!iSelectionManager)
				//	break;
				//InterfacePtr<IGroupItemSuite> igpItem(iSelectionManager,UseDefaultIID());
				//if(!igpItem){
				//	//CA("Not Found");	
				//	break;
				//}
				//if(igpItem->CanItemsBeUngrouped()){
				//	ErrorCode  sucess = igpItem->UngroupItems();					
				//}
				////**** Up To Here
				result=iSubSectSprayer->getCurrentPage(originalPageUIDRef,originalSpreadUIDRef);
				if(result == kFalse) 
					break;
				PageCount=1;
				UID originalPageUID = originalPageUIDRef.GetUID();
				pageUidList.push_back(originalPageUID);

				//get the list of selected box UIDRef's
				itemSelUIDList.Clear();
				result = iSubSectSprayer->getSelectedBoxIds(itemSelUIDList);
				
				//SDKListBoxHelper lstHelper(this,kSSSPluginID,kSSSListBoxTwoWidgetID,kSSSDialogWidgetID);			
				//lstHelper.EmptyCurrentListBox();

				if(result)  
				{
					//All the Selected tags are populated in Item's ListBox.
		//			SDKListBoxHelper lstHelper(this,kSSSPluginID,kSSSListBoxTwoWidgetID,kSSSDialogWidgetID);			
		//			lstHelper.EmptyCurrentListBox();
					
					//for(int i =static_cast<int>(dataString.size()-1);i>=0;i--)
					//	lstHelper.AddElement(dataString[i],kSSSTextWidgetID,0);
					dataString.clear();
				}
			}while(kFalse);
		}
///////////////////		Added By Amit 
		else if(theSelectedWidget == kHybridButtonWidgetID && theChange == kTrueStateMessage)
		{
			bool16 result=kFalse;
			do
			{
				UIDRef originalPageUIDRef=UIDRef::gNull;
				UIDRef originalSpreadUIDRef = UIDRef::gNull;
				pageUidList.clear();
				InterfacePtr<ISubSectionSprayer> iSubSectSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
				if(!iSubSectSprayer)
				{
				//CA("No SubSectSprayer");
					break;
				}
				result=iSubSectSprayer->getCurrentPage(originalPageUIDRef,originalSpreadUIDRef);
				if(result == kFalse) 
					break;
				////***Added
				//InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
				//if(!iSelectionManager)
				//	break;
				//InterfacePtr<IGroupItemSuite> igpItem(iSelectionManager,UseDefaultIID());
				//if(!igpItem){
				//	//CA("Not Found");	
				//	break;
				//}
				//if(igpItem->CanItemsBeUngrouped()){
				//	ErrorCode  sucess = igpItem->UngroupItems();					
				//}
				////**** Up To Here
				PageCount=1;
				UID originalPageUID = originalPageUIDRef.GetUID();
				pageUidList.push_back(originalPageUID);

				//get the list of selected box UIDRef's
				hybridSelUIDList.Clear();
				result = iSubSectSprayer->getSelectedBoxIds(hybridSelUIDList);
			
				//SDKListBoxHelper lstHelper(this,kSSSPluginID,kSSSListBoxThreeWidgetID,kSSSDialogWidgetID);			
				//lstHelper.EmptyCurrentListBox();
			
				if(result)  
				{
					//All the Selected tags are populated in Item's ListBox.
					//for(int i =static_cast<int>(dataString.size()-1);i>=0;i--)
					//{
						//lstHelper.AddElement(dataString[i],kSSSTextWidgetID,0);
					//}
					dataString.clear();
				}
			}while(kFalse);
		}
		else if(theSelectedWidget == kSectionButtonWidgetID && theChange == kTrueStateMessage)
		{
			bool16 result=kFalse;
			do
			{
				UIDRef originalPageUIDRef=UIDRef::gNull;
				UIDRef originalSpreadUIDRef = UIDRef::gNull;
				pageUidList.clear();
				InterfacePtr<ISubSectionSprayer> iSubSectSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
				if(!iSubSectSprayer)
				{
				//CA("No SubSectSprayer");
					break;
				}
				////***Added
				//InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
				//if(!iSelectionManager)
				//	break;
				//InterfacePtr<IGroupItemSuite> igpItem(iSelectionManager,UseDefaultIID());
				//if(!igpItem){
				//	//CA("Not Found");	
				//	break;
				//}
				//if(igpItem->CanItemsBeUngrouped()){
				//	ErrorCode  sucess = igpItem->UngroupItems();					
				//}
				////**** Up To Here
				result=iSubSectSprayer->getCurrentPage(originalPageUIDRef,originalSpreadUIDRef);
				if(result == kFalse) 
					break;
				PageCount=1;
				UID originalPageUID = originalPageUIDRef.GetUID();
				pageUidList.push_back(originalPageUID);

				//get the list of selected box UIDRef's
				sectionSelUIDList.Clear();
				result = iSubSectSprayer->getSelectedBoxIds(sectionSelUIDList);
			
				//SDKListBoxHelper lstHelper(this,kSSSPluginID,kSSSListBoxFourWidgetID,kSSSDialogWidgetID);			
				//lstHelper.EmptyCurrentListBox();
			
				if(result)  
				{
					//All the Selected tags are populated in Item's ListBox.
					//for(int i =static_cast<int>(dataString.size()-1);i>=0;i--)
					//{
					//	lstHelper.AddElement(dataString[i],kSSSTextWidgetID,0);
					//}
					dataString.clear();
				}
			}while(kFalse);
		}

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (!panelControlData)
			break;

		IControlView * IsWithoutPageBreakCheckBoxControlView = panelControlData->FindWidget(kIsWithoutPageBreakWidgetID);
		IControlView * IsAtStartOfSectionCheckBoxControlView = panelControlData->FindWidget(kIsAtStartOfSectionWidgetID);
		IControlView * IsAtStartOfEachPageCheckBoxControlView = panelControlData->FindWidget(kIsAtStartOfEachPageWidgetID);	
		IControlView * IsAtStartOfFirstPageCheckBoxControlView = panelControlData->FindWidget(kIsAtStartOfFirstPageWidgetID);	
		
		InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
		if(dlgController==nil)
			break;
		
		if(theSelectedWidget == kIsAllSectionSprayWidgetID && theChange == kTrueStateMessage)
		{
			IsWithoutPageBreakCheckBoxControlView->Enable();
		}
		else if(theSelectedWidget == kIsAllSectionSprayWidgetID && theChange != kTrueStateMessage)
		{
			dlgController->SetTriStateControlData(kIsWithoutPageBreakWidgetID,kFalse,panelControlData);
			IsWithoutPageBreakCheckBoxControlView->Disable();
		}
		else if(theSelectedWidget == kIsAddSectionStencilWidgetID && theChange == kTrueStateMessage)
		{
			dlgController->SetTriStateControlData(kIsAtStartOfSectionWidgetID,kTrue,panelControlData);
			IsAtStartOfSectionCheckBoxControlView->Enable();
			IsAtStartOfEachPageCheckBoxControlView->Enable();
			IsAtStartOfFirstPageCheckBoxControlView->Enable();
		}
		else if(theSelectedWidget == kIsAddSectionStencilWidgetID && theChange != kTrueStateMessage)
		{
			dlgController->SetTriStateControlData(kIsAtStartOfSectionWidgetID,kFalse,panelControlData);
			dlgController->SetTriStateControlData(kIsAtStartOfEachPageWidgetID,kFalse,panelControlData);
			dlgController->SetTriStateControlData(kIsAtStartOfFirstPageWidgetID,kFalse,panelControlData);
			IsAtStartOfSectionCheckBoxControlView->Disable();
			IsAtStartOfEachPageCheckBoxControlView->Disable();
			IsAtStartOfFirstPageCheckBoxControlView->Disable();
		}

		else if(theSelectedWidget == kIsAtStartOfEachPageWidgetID && theChange == kTrueStateMessage)
		{
			dlgController->SetTriStateControlData(kIsAtStartOfFirstPageWidgetID,kFalse,panelControlData);
		}
	
		else if(theSelectedWidget == kIsAtStartOfFirstPageWidgetID && theChange == kTrueStateMessage)
		{
			dlgController->SetTriStateControlData(kIsAtStartOfEachPageWidgetID,kFalse,panelControlData);
		}

		else if((theSelectedWidget == kSectionSprayCancelButtonWidgetID || theSelectedWidget == kCancelButton_WidgetID )&& theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kSectionSprayCancelButtonWidgetID");
			Mediator::hasUserCancelled = kTrue;
			if(theSelectedWidget == kSectionSprayCancelButtonWidgetID)
			{
				CDialogObserver::CloseDialog();
			}
			//break;
		}

/////////////////////// End
        else if((theSelectedWidget == kSectionSprayOKButtonWidgetID || theSelectedWidget ==kOKButtonWidgetID) && theChange == kTrueStateMessage)
		{
			//CA("inside section Spray");
			InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
			if(dlgController==nil)
				break;
			bool16 result=dlgController->GetTriStateControlData (kIsSprayItemPerFrameWidgetID);
			InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
			if(iSSSprayer==nil)
			{
				CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
				return;
			}
			iSSSprayer->setSprayItemPerFrameFlag(result);
			
			bool16 result1 = dlgController->GetTriStateControlData(kIsHorizontalFlowForAllImageSprayWidgetID);
			iSSSprayer->setHorizontalFlowForAllImageSprayFlag(result1);
			
			bool16 state = dlgController->GetTriStateControlData(kIsWithoutPageBreakWidgetID);
			IsWithoutPageBreakFlag = state;

			state = dlgController->GetTriStateControlData(kIsAddSectionStencilWidgetID);
			IsAddSectionStencilFlag = state;

			state = dlgController->GetTriStateControlData(kIsAtStartOfSectionWidgetID);
			IsAtStartOfSectionFlag = state;

			state = dlgController->GetTriStateControlData(kIsAtStartOfEachPageWidgetID);
			IsAtStartOfEachPageFlag = state;

			state = dlgController->GetTriStateControlData(kIsAtStartOfFirstPageWidgetID);
			IsAtStartOfFirstPageFlag = state;

			///// Code from SSSDialogController::ApplyDialogFields  as code is not going in SSSDialogController::ApplyDialogFields
			
			PMString value;			
			value = dlgController->GetTextControlData(kHorizontalSpacingWidgetID, panelControlData);
			if(value != "")
			{
				PMReal horizontalSpacingReal = dlgController->GetTextValue(kHorizontalSpacingWidgetID, panelControlData);
				iSSSprayer->setHorizontalBoxSpacing(horizontalSpacingReal);
				//by amarjit patil
				Hspace = horizontalSpacingReal;
				
			}
					
			value = dlgController->GetTextControlData(kVerticalSpacingWidgetID, panelControlData);
			if(value != "")
			{
				PMReal verticalSpacingReal = dlgController->GetTextValue(kVerticalSpacingWidgetID, panelControlData);
				iSSSprayer->setVerticalBoxSpacing(verticalSpacingReal);
				
				//by amarjit patil;
				PMString str1="";
				str1.AppendNumber(verticalSpacingReal);
				//CA("756  Vertical Space: " + str1);
				//CA("742 Vertical Space: " + verticalSpacingReal);
				Vspace = verticalSpacingReal;

			}
            
            InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
            if(cop==nil)
            {
                //CA("cop==nil");
                return;
            }
            cop->set_HorizontalAndVerticleSpacing(ToInt32(Hspace), ToInt32(Vspace));
            
			
			ITriStateControlData::TriState theSprayAllSectionsFlafgState= dlgController->GetTriStateControlData(kIsAllSectionSprayWidgetID, panelControlData);
			if(theSprayAllSectionsFlafgState == ITriStateControlData::kSelected)
				iSSSprayer->setSprayAllSectionsFlag(kTrue);
			else
				iSSSprayer->setSprayAllSectionsFlag(kFalse);

			bool16 AltVerticalFlowFlag = kFalse;
			bool16 VerticalFlowFlag = kFalse;
			bool16 AltHorizontalFlowFlag = kFalse;
			bool16 HorizontalFLowFlag = kFalse;

			ITriStateControlData::TriState AltVerticalFlowState = dlgController->GetTriStateControlData(kSSSAltVerticalFLowRadioButtonWidgetID, panelControlData);
			if(AltVerticalFlowState == ITriStateControlData::kSelected)
			{
				AltVerticalFlowFlag = kTrue;
				iSSSprayer->setAltVerticalFlow(kTrue);
			}
			else
			{
				AltVerticalFlowFlag = kFalse;
				iSSSprayer->setAltVerticalFlow(kFalse);
			}

			ITriStateControlData::TriState VerticalFlowState = dlgController->GetTriStateControlData(kSSSVerticalFlowRadioButtonWidgetID, panelControlData);
			if(VerticalFlowState == ITriStateControlData::kSelected)
			{
				VerticalFlowFlag = kTrue;
				iSSSprayer->setVerticalFlow(kTrue);
			}
			else
			{
				VerticalFlowFlag = kFalse;
				iSSSprayer->setVerticalFlow(kFalse);
			}

			ITriStateControlData::TriState AltHorizontalFlowState = dlgController->GetTriStateControlData(kSSSAltHorzintalFlowRadioButtonWidgetID, panelControlData);
			if(AltHorizontalFlowState == ITriStateControlData::kSelected)
			{
				AltHorizontalFlowFlag = kTrue;
				iSSSprayer->setAltHorizontalFlow(kTrue);
			}
			else
			{
				AltHorizontalFlowFlag = kFalse;
				iSSSprayer->setAltHorizontalFlow(kFalse);
			}

			ITriStateControlData::TriState HorizontalFlowState = dlgController->GetTriStateControlData(kSSSHorzintalFlowRadioButtonWidgetID, panelControlData);
			if(HorizontalFlowState == ITriStateControlData::kSelected)
			{
				HorizontalFLowFlag = kTrue;
				iSSSprayer->setHorizontalFlow(kTrue);
			}
			else
			{
				HorizontalFLowFlag = kFalse;
				iSSSprayer->setHorizontalFlow(kFalse);
			}

			if(AltVerticalFlowFlag)
			{
				iSSSprayer->setLeftToRightVal(kTrue);    
				iSSSprayer->setAlternatingVal(kTrue);    
				iSSSprayer->setHorizFlowType(kFalse);    
				iSSSprayer->setTopToBottomVal(kTrue);
			}
			else if(VerticalFlowFlag)
			{
				iSSSprayer->setLeftToRightVal(kTrue);    
				iSSSprayer->setAlternatingVal(kFalse);    
				iSSSprayer->setHorizFlowType(kFalse);    
				iSSSprayer->setTopToBottomVal(kTrue);
			}
			else if(AltHorizontalFlowFlag)
			{
				iSSSprayer->setLeftToRightVal(kTrue);    
				iSSSprayer->setAlternatingVal(kTrue);    
				iSSSprayer->setHorizFlowType(kTrue);    
				iSSSprayer->setTopToBottomVal(kTrue);
			}
			else if(HorizontalFLowFlag)
			{
				iSSSprayer->setLeftToRightVal(kTrue);    
				iSSSprayer->setAlternatingVal(kFalse);    
				iSSSprayer->setHorizFlowType(kTrue);    
				iSSSprayer->setTopToBottomVal(kTrue);
			}
			/////End
			if(theSelectedWidget == kSectionSprayOKButtonWidgetID)
			{
				CDialogObserver::CloseDialog();
			}
//CA("222");
		}		
  }while(kFalse);
}
//  Generated by Dolly build 17: template "Dialog".

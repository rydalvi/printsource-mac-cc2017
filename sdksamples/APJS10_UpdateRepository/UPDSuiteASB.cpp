#include "VCPlugInHeaders.h"								
// Interface includes:						
#include "ISelectionManager.h"
// General includes:
#include "CmdUtils.h"		// this is necessary for the selectionasbtemplates.tpp
#include "SelectionASBTemplates.tpp"
#include "CPMUnknown.h"
#include "K2Vector.h"

// Project includes:
#include "UPDID.h"
#include "IUPDSuit.h"	

class UPDSuiteASB : public CPMUnknown<IUPDSuite>
{
public:
	UPDSuiteASB  (IPMUnknown *iBoss);
	virtual ~UPDSuiteASB (void);
	virtual bool16 CanGetTextInsets(void);
	virtual void GetTextInsets(TextInsets& textInsets);
	virtual bool16	CanApplyTextInset(void);
	virtual ErrorCode ApplyTextInset(const PMReal& insetValue);
	virtual ErrorCode GetTextFocus(ITextFocus * & Ifocus);
};

/* CREATE_PMINTERFACE
 	Binds the C++ implementation class onto its ImplementationID making the C++ code callable by the application.
*/
CREATE_PMINTERFACE (UPDSuiteASB , kUPDSuiteASBImpl)



/* RfhSuiteASB Constructor
*/
UPDSuiteASB::UPDSuiteASB(IPMUnknown* boss) :
	CPMUnknown<IUPDSuite>(boss)
{
}

/* RfhSuiteASB Destructor
*/
UPDSuiteASB::~UPDSuiteASB(void)
{
}

/* 
*/
bool16 UPDSuiteASB::CanGetTextInsets(void)
{
	return (AnyCSBSupports (make_functor(&IUPDSuite::CanGetTextInsets), this));
}

/* 
*/
void UPDSuiteASB::GetTextInsets(TextInsets& textInsets)
{
	CallEach(make_functor(&IUPDSuite::GetTextInsets, textInsets), this);
}

/* 
*/
bool16 UPDSuiteASB::CanApplyTextInset(void)
{
	return (AnyCSBSupports (make_functor(&IUPDSuite::CanApplyTextInset), this));
}

/* 
*/
ErrorCode UPDSuiteASB::ApplyTextInset(const PMReal& insetValue)
{
	return (Process (make_functor(&IUPDSuite::ApplyTextInset, insetValue), this, IUPDSuite::kDefaultIID));
}
ErrorCode UPDSuiteASB::GetTextFocus(ITextFocus * & Ifocus)
{
	return (Process (make_functor(&IUPDSuite::GetTextFocus, Ifocus), this, IUPDSuite::kDefaultIID));
}

// End, UPDSuiteASB.cpp.




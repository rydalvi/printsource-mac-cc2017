#include "VCPlugInHeaders.h"
#include "IHierarchy.h"	
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
#ifdef	 DEBUG
#include "IPlugInList.h"
#include "IObjectModel.h"
#endif
#include "ILayoutUtils.h" //Cs4
#include "UPDID.h"
#include "UPDTreeModel.h"
#include "CAlert.h"
#include "RefreshData.h"
//#include "TPLMediatorClass.h"


#include "UPDDataNode.h"
//#include "IClientOptions.h"
//#include "IAppFrameWork.h"
#include "UPDTreeDataCache.h"
//#include "ISpecialChar.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("UPDTreeModel.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

UPDTreeDataCache dc;

int32 UPDTreeModel::root=0;
PMString UPDTreeModel::FieldName;
double UPDTreeModel::FieldId=0;
extern RefreshDataList UniqueDataList;
extern RefreshDataList rDataList;
int32 RowCountForTree = -1;



UPDTreeModel::UPDTreeModel()
{
	//root=-1;
	//classId=11002;
}

UPDTreeModel::~UPDTreeModel() 
{
}


int32 UPDTreeModel::GetRootUID() const
{	
	if(FieldId<=0)
		return 0;// kInvalidUID;
	
	UPDDataNode pNode;
	if(dc.isExist(root, pNode))
	{
		return root;
	}
	
	dc.clearMap();
	

	
	RefreshDataList CurrentNodeList;
	CurrentNodeList.clear();



	if((/*UniqueDataList*/rDataList.size() <= 0))
		return 0;

	CurrentNodeList = rDataList /*UniqueDataList*/;


	RowCountForTree = 0;
	PMString name("Root");
	pNode.setFieldId(root);
	pNode.setChildCount(static_cast<int32>(CurrentNodeList.size()));
	/*pNode.setLevel(0);*/
	pNode.setParentId(-2);
	pNode.setFieldName(name);
	pNode.setSequence(0);
	pNode.setIconDispayHint(0);
	dc.add(pNode);

	int32 count=0;
	for(int i =0 ; i<CurrentNodeList.size(); i++)
	{ //CAlert::InformationAlert(" 123");
			/*pNode.setLevel(1);*/
			
				pNode.setParentId(root);
				pNode.setSequence(i);
				count++;
				pNode.setFieldId(count/*UniqueDataList[i].elementID*/);
				PMString ASD(CurrentNodeList[i].name);
				pNode.setFieldName(ASD);
				//CA(ASD);
				pNode.setChildCount(0);
                pNode.setHitCount(0);
				pNode.setIsSelected(CurrentNodeList[i].isSelected);
				pNode.setIconDispayHint(CurrentNodeList[i].iconDispayHint);
			dc.add(pNode);
		}
	return root;
}


int32 UPDTreeModel::GetRootCount() const
{
	int32 retval=0;
	UPDDataNode pNode;
	if(dc.isExist(root, pNode))
	{
		return pNode.getChildCount();
	}
	return 0;
}

int32 UPDTreeModel::GetChildCount(const int32& uid) const
{
	UPDDataNode pNode;

	if(dc.isExist(uid, pNode))
	{
		return pNode.getChildCount();
	}
	return -1;
}

int32 UPDTreeModel::GetParentUID(const int32& uid) const
{
	UPDDataNode pNode;

	if(dc.isExist(uid, pNode))
	{
		if(pNode.getParentId()==-2)
			return 0; //kInvalidUID;
		return pNode.getParentId();
	}
	return 0; //kInvalidUID;
}


int32 UPDTreeModel::GetChildIndexFor(const int32& parentUID, const int32& childUID) const
{
	UPDDataNode pNode;
	int32 retval=-1;
	
	if(dc.isExist(childUID, pNode))
		return pNode.getSequence();
	return -1;
}


int32 UPDTreeModel::GetNthChildUID(const int32& parentUID, const int32& index) const
{
	UPDDataNode pNode;
	int32 retval=-1;

	if(dc.isExist(parentUID, index, pNode))
		return pNode.getFieldId();
	return 0; // kInvalidUID;
}


int32 UPDTreeModel::GetNthRootChild(const int32& index) const
{
	UPDDataNode pNode;
	if(dc.isExist(root, index, pNode))
	{
		return pNode.getFieldId();
	}
	return 0; //kInvalidUID;
}

int32 UPDTreeModel::GetIndexForRootChild(const int32& uid) const
{
	UPDDataNode pNode;
	if(dc.isExist(uid, pNode))
		return pNode.getSequence();
	return -1;
}

PMString UPDTreeModel::ToString(const int32& uid, int32 *Rowno) const
{
	UPDDataNode pNode;
	PMString name("");

	if(dc.isExist(uid, pNode))
	{	
		*Rowno= pNode.getSequence();
		//*Rowno = pNode.getHitCount();
		/*PMString ASD("getHitCount in ToolString : ");
		ASD.AppendNumber(pNode.getHitCount());
		CA(ASD);*/
		return pNode.getFieldName();
	}
	return name;
}


int32 UPDTreeModel::GetNodePathLengthFromRoot(const int32& uid) 
{
	UPDDataNode pNode;
	if(uid==root)
		return 0;
	if(dc.isExist(uid, pNode))
		return (1);
	return -1;
}

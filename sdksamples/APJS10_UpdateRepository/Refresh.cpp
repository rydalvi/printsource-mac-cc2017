#include "VCPluginHeaders.h"
#include "Refresh.h"
#include "RefreshData.h"
#include "BoxReader.h"

#include "ILayoutUtils.h" //cs4
//#include "ISelection.h"
#include "CAlert.h"

#include "TagReader.h"
//#include "ISelection.h"
#include "ILayoutUIUtils.h"
#include "UIDList.h"
#include "CAlert.h"
#include "IClientOptions.h"
#include "IPageList.h"
#include "IDocument.h"
#include "ISelectUtils.h"
#include "ITextAttrUID.h"
#include "ICommand.h"
#include "ITextAttrUtils.h"
#include "ISwatchList.h"
#include "IWorkspace.h"
//#include "ITextFrame.h"
#include "ITextModel.h"
#include "ITextFocusManager.h"
#include "ITextFocus.h"
#include "IFrameList.h"
#include "IFrameUtils.h" //FrameUtils.h"
#include "IAppFramework.h"
#include "ISpecialChar.h"
#include "ILayoutControlData.h"
#include "ISpread.h"
#include "ISpreadList.h"
#include "MediatorClass.h"
#include "IHierarchy.h"
#include "ITextMiscellanySuite.h"
//#include "IImportFileCmdData.h"
#include "IOpenFileDialog.h"
#include "IImportProvider.h"
#include "IGraphicFrameData.h"
#include "IPlaceGun.h"
#include "IPlaceBehavior.h"
#include "IReplaceCmdData.h"
#include "SDKUtilities.h"
#include "TransformUtils.h"
#include "TableUtility.h"
#include "ITextTarget.h"
#include "ISelectionUtils.h"
#include "TextEditorID.h"
#include "TextIterator.h"
#include "PMString.h"
#include "ITriStateControlData.h"
#include "UPDSelectionObserver.h"
#include "PageData.h"
//#include "LayoutUIUtils.h"
#include "ILayoutUIUtils.h"
#include <string.h>
#include "ITextFrameColumn.h"
#include "CTUnicodeTranslator.h"
#include "ITagReader.h"

#include "URI.h"
#include "IURIUtils.h"
#include "IImportResourceCmdData.h"
#include <string>

#define CA(z) CAlert::InformationAlert(z)

#define CAI(y,x)	{\
						PMString tempString(y);\
						tempString.Append(" : ");\
						tempString.AppendNumber(x);\
						CAlert::InformationAlert(tempString);\
					}
					
extern RefreshDataList rDataList;
extern bool16 HiliteFlag;
extern RefreshDataList OriginalrDataList;

/// Global Pointers
extern ITagReader* itagReader;
extern IAppFramework* ptrIAppFramework;
extern ISpecialChar* iConverter;
///////////

RefreshDataList ColorDataList;

TextVectorList OriginalTextList;
TextVectorList NewTextList;
TextVectorList ChangedTextList;

int16 pgno,cursel,seltext,FlagParentID;

using namespace std;

void Refresh::appendToGlobalList(const PageData& pData)
{
	//CA(__FUNCTION__);
	RefreshData rData;
	for(int i=0; i<pData.objectDataList.size(); i++)
	{

		if(pData.objectDataList[i].elementList.size()==0 && pData.objectDataList[i].isTableFlag!=kTrue)
			continue;
		pgno=1;
		cursel=1;
		rData.StartIndex= 0;
		rData.EndIndex= 0;
		rData.BoxUIDRef=pData.BoxUIDRef;
		rData.boxID=pData.boxID;
		rData.elementID=-1;
		rData.isObject=kTrue;
		rData.isSelected=kFalse;
		rData.name=pData.objectDataList[i].objectName;
		rData.objectID=pData.objectDataList[i].objectID;
		rData.publicationID=pData.objectDataList[i].publicationID;
		rData.isTableFlag=pData.objectDataList[i].isTableFlag;
		//Awasthi
		rData.isImageFlag=pData.objectDataList[i].isImageFlag;
		rData.whichTab = pData.objectDataList[i].whichTab;
		//rDataList.push_back(rData);
		
		//OriginalrDataList.push_back(rData);
		
		for(int j=0; j<pData.objectDataList[i].elementList.size(); j++)
		{	
			rData.TypeID=pData.objectDataList[i].elementList[j].typeID;
			rData.childId = pData.objectDataList[i].elementList[j].childId;
			rData.childTag = pData.objectDataList[i].elementList[j].childTag;
			if(j == 0)
			{
				rDataList.push_back(rData);
				OriginalrDataList.push_back(rData);
			}

			//CA("inside inner for");
			rData.StartIndex= pData.objectDataList[i].elementList[j].StartIndex;
			rData.EndIndex= pData.objectDataList[i].elementList[j].EndIndex;
			rData.BoxUIDRef=pData.BoxUIDRef;
			rData.boxID=pData.boxID;
			rData.elementID=pData.objectDataList[i].elementList[j].elementID;
			rData.isTaggedFrame=pData.objectDataList[i].elementList[j].isTaggedFrame;
			rData.isObject=kFalse;
			rData.isSelected=kFalse;
			rData.name=pData.objectDataList[i].elementList[j].elementName;
			rData.objectID=pData.objectDataList[i].objectID;
			rData.publicationID=pData.objectDataList[i].publicationID;
			//rData.TypeID=pData.objectDataList[i].elementList[j].typeID;
			rData.whichTab =pData.objectDataList[i].elementList[j].whichTab;
			rData.LanguageID = pData.objectDataList[i].elementList[j].languageid;
			rDataList.push_back(rData);	

			OriginalrDataList.push_back(rData);
		}		
	}
}

bool16 Refresh::getAllPageItemsFromPage(int32 pageNumber)
{

	//CA(__FUNCTION__);
	UID pageUID;
/*	
	TagReader tReader;
	if(!this->isValidPageNumber(pageNumber, pageUID))
		return kFalse;
		IDocument* document = ::GetFrontDocument(); 
	IDataBase* database= ::GetDataBase(document); 
	IDocument* document = layoutData->GetDocument();
	if (document == NULL)
		return kFalse;
	IDataBase* database = ::GetDataBase(document);
	if(!database)
		return kFalse;*/
	
/*	InterfacePtr<ISpreadList> spreadList(document, IID_ISPREADLIST); 
	int32 spreadNo = -1; 

	for(int32 i=0; i<spreadList->GetSpreadCount(); ++i) 
	{ 
		UIDRef spreadRef(database, spreadList->GetNthSpreadUID(i)); 
		InterfacePtr<ISpread> spread(spreadRef, IID_ISPREAD); 
		if(spread->GetPageIndex(pageUID)!= -1) 
		{			
			spreadNo = i; 
			break; 
		}
	}

	if(spreadNo==-1)
		return kFalse;

	InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
	if (layoutData == NULL)
		return kFalse;
//	layoutData->SetSpread(spreadList->QueryNthSpread(spreadNo),kFalse);
	
	InterfacePtr<ILayoutControlData> layoutData1(::QueryFrontLayoutData());
	if (layoutData1 == NULL)
		return kFalse;

	IGeometry* spreadItem = layoutData1->GetSpread();
	if (spreadItem == NULL)
		return kFalse;
	InterfacePtr<ISpread> spread((IPMUnknown *)spreadItem, UseDefaultIID());
	if(spread == NULL)
		return kFalse;
	UIDList tempList(database);

	spread->GetItemsOnPage(pageNumber, &tempList, kFalse);

	selectUIDList=tempList;

	for(int i=0; i<selectUIDList.Length(); i++)
	{	
		InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		if(!iHier)
			continue;

		UID kidUID;
		int32 numKids=iHier->GetChildCount();
		for(int j=0;j<numKids;j++)
		{
			IIDXMLElement* ptr;
			kidUID=iHier->GetChildUID(j);
			UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
			itagReader->getTagsFromBox(boxRef, &ptr);
			if(!doesExist(ptr))
				selectUIDList.Append(kidUID);
		}
	}
	return kTrue;
*/
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*>(CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	return kFalse;
		//TagReader tReader;
		InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
		if(!itagReader){
			ptrIAppFramework->LogError("AP46_UpdateRepository::Refresh::getAllPageItemsFromPage::!itagReader");	
			return kFalse;
		}
		if(!this->isValidPageNumber(pageNumber, pageUID))
		return kFalse;
	
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		IDataBase *db = ::GetDataBase(document); 
		ASSERT(db); 

		InterfacePtr<ISpreadList> spreadList(document, UseDefaultIID()); 
		ASSERT(spreadList); 

		int32 spreadNum = 0; 
		int32 spreadCount = spreadList->GetSpreadCount(); 
		int32 currentPageNumber = 0; 
		
		
		while(spreadNum < spreadCount) 
		{ 
			bool16 FirstWhilebreak= kFalse;
			InterfacePtr<ISpread> spread(db, spreadList->GetNthSpreadUID(spreadNum), IID_ISPREAD); 
			ASSERT(spread); 

			int32 pagesOnSpread = spread->GetNumPages(); 
			
			int32 PageNO=0;
			while(pagesOnSpread--) 
			{	
				bool16 SecondWhileBreak= kFalse;
				
				if(currentPageNumber == pageNumber) 
				{ 
					
					UIDList itemsList(db); 
					spread->GetItemsOnPage(/*pageNumber*/PageNO, &itemsList, kFalse); 
					selectUIDList = itemsList;
					
					SecondWhileBreak= kTrue;
					FirstWhilebreak= kTrue;
					break;//return itemsList; 
				} 
				PageNO++;
				currentPageNumber++; 
				if(SecondWhileBreak)
					break;
				
			} 
			++spreadNum; 
			if(FirstWhilebreak)
				break;
			
		} 

		for(int i=0; i<selectUIDList.Length(); i++)
		{	
			//InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
			//if(!iHier)
			//	continue;

			//UID kidUID;
			//int32 numKids=iHier->GetChildCount();
			//for(int j=0;j<numKids;j++)
			//{
				IIDXMLElement* ptr;
			//	kidUID=iHier->GetChildUID(j);
			//	UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
				itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(i), &ptr);
			//	if(!doesExist(ptr))
			//		selectUIDList.Append(kidUID);
			//}
		}
	return kTrue;

}

bool16 Refresh::isValidPageNumber(int32 pageNumber, UID& myPageUID)
{	
	//CA(__FUNCTION__);
	IDocument * myDocPtr = Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
	IDataBase* db =::GetDataBase(myDocPtr);
	
	InterfacePtr <IDocument> myDoc(myDocPtr, UseDefaultIID());
	
	if(myDoc!=NULL)
	{	
		InterfacePtr<IPageList> myPageList(myDoc, UseDefaultIID());
		if(myPageList==NULL)
			return kFalse;
		
		int32 PageCount = myPageList->GetPageCount();
		PageCount--;
		if(pageNumber> PageCount)
		{	myPageUID= kInvalidUID;
			return kFalse;
		}
		myPageUID = myPageList->GetNthPageUID(pageNumber);
		if(myPageUID != kInvalidUID)
		return kTrue;
	}
	
	return kFalse;
}

bool16 Refresh::isValidPageNumber(int32 pageNumber)
{
	//CA(__FUNCTION__);
	UID myPageUID;
	IDocument * myDocPtr = Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/; //Cs4
	IDataBase* db =::GetDataBase(myDocPtr);
	
	InterfacePtr <IDocument> myDoc(myDocPtr, UseDefaultIID());

	if(myDoc!=NULL)
	{
		InterfacePtr<IPageList> myPageList(myDoc, UseDefaultIID());
		if(myPageList==NULL)
			return kFalse;
		
		myPageUID = myPageList->GetNthPageUID(pageNumber);
		if(myPageUID != kInvalidUID)
			return kTrue;
	}
	return kFalse;
}


bool16 Refresh::GetPageDataInfo(int whichSelection)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*>(CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return kFalse;
	BoxReader bReader;
	UID pageUID=kInvalidUID;
	int i,k=0;
	 
	rDataList.clear();

	OriginalrDataList.clear();
	
	pgno=0;
	cursel=0;
	seltext=0;
	FlagParentID = 0;
	this->selectUIDList.Clear();

	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		ptrIAppFramework->LogError("AP46_UpdateRepository::Refresh::GetPageDataInfo::!itagReader");	
		return kFalse;
	}
	
	switch(whichSelection)
	{
		case 1:
		{
			//CA("case 1"); // Current Page option selected
			UID pageUID;
			//TagReader tReader;
			int32 pageNumber= 0;
			if(!this->isValidPageNumber(pageNumber, pageUID))
				return kFalse;
			
				IDocument* document = /*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();  //Cs4
			IDataBase* database= ::GetDataBase(document); 

			/*IDocument* document = layoutData->GetDocument();
			if (document == NULL)
				return kFalse;
			IDataBase* database = ::GetDataBase(document);
			if(!database)
				return kFalse;*/
			
			InterfacePtr<ISpreadList> spreadList(document, IID_ISPREADLIST); 
			int32 spreadNo = -1; 

			for(int32 p=0; p<spreadList->GetSpreadCount(); ++p) 
			{ 
				UIDRef spreadRef(database, spreadList->GetNthSpreadUID(p)); 
				InterfacePtr<ISpread> spread(spreadRef, IID_ISPREAD); 
				if(spread->GetPageIndex(pageUID)!= -1) 
				{			
					spreadNo = p; 
					break; 
				}
			}

			if(spreadNo==-1)
				return kFalse;

			InterfacePtr<ILayoutControlData>layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layoutData == NULL)
				return kFalse;
			
			 pageUID = layoutData->GetPage();
			if(pageUID == kInvalidUID)
			return kFalse;

			//layoutData->SetSpread(spreadList->QueryNthSpread(spreadNo),kFalse);
			
		//	InterfacePtr<ILayoutControlData> layoutData1(::QueryFrontLayoutData());
		//	if (layoutData1 == NULL)
		//		return kFalse;	
			
		/////Added by Amit
			UIDRef spreadUIDRef=layoutData->GetSpreadRef();
			InterfacePtr<ISpread>iSpread(spreadUIDRef, UseDefaultIID());
			if (iSpread == NULL)
				return kFalse;

		//////	end
			
/*	///////CS3 Change 
			IGeometry* spreadItem = layoutData->GetSpread();
			if (spreadItem == NULL)
				return kFalse;
*/
			/*InterfacePtr<ISpread> spread(spreadItem, UseDefaultIID());
			if(spread == NULL)
				return kFalse;*/

/*		///CS3 Change
			InterfacePtr<ISpread> iSpread((IPMUnknown *)spreadItem, UseDefaultIID());
			if (iSpread == NULL)
				return kFalse;
*/			
			
			UIDList tempList(database);
			pageNumber=iSpread->GetPageIndex(pageUID);


			iSpread->GetItemsOnPage(pageNumber, &tempList, kFalse);

			selectUIDList=tempList;

			for( int p=0; p<selectUIDList.Length(); p++)
			{	
				//CA("GetPageDataInfo :: first for "); 
			//	InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(p), UseDefaultIID());
			//	if(!iHier)
			//		continue;

			//	UID kidUID;
			//	int32 numKids=iHier->GetChildCount();

			//	for(int j=0;j<numKids;j++)
			//	{
					//CA("GetPageDataInfo :: first first for "); 
				IIDXMLElement* ptr;

			//		kidUID=iHier->GetChildUID(j);

			//		UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);

				itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(p), &ptr);
			//		if(!doesExist(ptr))
			//			selectUIDList.Append(kidUID);

			//	}
			}

			/*PMString temp1(" 1 selectUIDList.size = ");
			temp1.AppendNumber(selectUIDList.size());
			CA(temp1);*/

			for(int p=0; p<selectUIDList.Length(); p++)
			{	
				//CA("GetPageDataInfo :: second for "); 
				PageData pData;
				bReader.getBoxInformation(selectUIDList.GetRef(p), pData);
				appendToGlobalList(pData);

				// Added By Awasthi
				UIDRef boxID=selectUIDList.GetRef(p);
				TagList tList;

				//TagReader tReader;
				tList=itagReader->getTagsFromBox_ForRefresh(boxID);
				if(tList.size()==0)
				{	
					tList=itagReader->getFrameTags(boxID);
					if(tList.size()==0)
					{
						//FlagParentID = 1;
						continue;
					}
				}

				for(int j=0; j<tList.size(); j++)
				{		
					if(tList[j].parentId==-1)
						continue;				
					k=1;
				}
				if(k==0)
					FlagParentID = 1;	
			
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
			}		
			break;
		}

	case 2://Page number   // Page no option selected 
		if(!this->getAllPageItemsFromPage(Mediator::refreshPageNumber))
		{
			//CA("getAllPageItemsFromPage");
			
			return kFalse;
		}
		for(i=0; i<selectUIDList.Length(); i++)
		{	
			PageData pData;
			bReader.getBoxInformation(selectUIDList.GetRef(i), pData);
			appendToGlobalList(pData);
			// Added By Awasthi
			UIDRef boxID=selectUIDList.GetRef(i);
			TagList tList;
			//TagReader tReader;
			tList=itagReader->getTagsFromBox_ForRefresh(boxID);
			if(tList.size()==0)
			{	
				tList=itagReader->getFrameTags(boxID);
				if(tList.size()==0)
				{
					//FlagParentID = 1;
					continue;
				}
			}
			for(int j=0; j<tList.size(); j++)
			{				
				if(tList[j].parentId==-1)
					continue;
				k=1;
			}
			if(k==0)
				FlagParentID = 1;	

			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}
		break;

	case 3://Current Selection
		//CA("case 3");
		if(!this->getAllBoxIds())	
		{
			//CA("case 3 : return");
			return kFalse;
		}
	
		for(i=0; i<selectUIDList.Length(); i++)
		{	
			PageData pData;
			bReader.getBoxInformation(selectUIDList.GetRef(i), pData);

			/*PMString size = "";
			size.Append("pData.objectDataList.size() = ");
			size.AppendNumber(pData.objectDataList.size());
			CA(size);*/
			
			appendToGlobalList(pData);

			/*PMString temp;
			for(int j=0; j<OriginalrDataList.size(); j++)
			{
				temp.AppendNumber(j);
				temp.Append(" OriginalrDataList[j].TypeID = ");
				temp.AppendNumber(OriginalrDataList[j].TypeID);
				temp.Append("\n");			
			}
			CA(temp);*/
			// Added By Awasthi
			UIDRef boxID=selectUIDList.GetRef(i);
			TagList tList;
			//TagReader tReader;		
			tList=itagReader->getTagsFromBox_ForUpdate(boxID);
			if(tList.size()==0)
			{
				tList=itagReader->getFrameTags(boxID);
				if(tList.size()==0)
				{
					//FlagParentID = 1;
					continue;
				}
			}
			for(int j=0; j<tList.size(); j++)
			{				
				if(tList[j].parentId==-1)
					continue;
				k=1;
			}
			if(k==0)
				FlagParentID = 1;

			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}
		//CA("case 3 : outside  for ");


		break;
	case 4:
		do
		{  // Entire Document option selected
/*			ITextFocus * iFocus=Utils<ISelectUtils>()->QueryTextFocus();*/
/*			InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());

			if(!iSelectionManager)
			{	
				//CA("null selection");
				return kFalse;
			}
			// Get text selection 
			InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
			InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID()); 

			InterfacePtr<ITextFocus> iFocus(pTextTarget->QueryTextFocus()); 

			if(!iFocus)
				return kFalse;

			InterfacePtr<ITextModel> textModel(iFocus->QueryModel());
			if (textModel==NULL)
				return kFalse;
			
			IFrameList* frameList=textModel->QueryFrameList();
			if(!frameList)
				return kFalse;
			UID frameUID=frameList->GetNthFrameUID(0);

			IDocument *docPtr=::GetFrontDocument();
			IDataBase* db =::GetDataBase(docPtr);
			if(db==NULL)
				return kFalse;

			UIDRef boxUIDRef(db, frameUID);

			TagReader tReader;
			TagList tList=itagReader->getTagsFromBox_ForRefresh(boxUIDRef);
			if(tList.size()==0)
			{
				tList=itagReader->getFrameTags(boxUIDRef);
				if(tList.size()==0)
					return kFalse;
				PageData pData;

				bReader.getBoxInformation(boxUIDRef, pData);
				appendToGlobalList(pData);
				break;
			}
			

			RangeData iRangeData=iFocus->GetCurrentRange();

			int32 textEnd=-1, textStart=-1;
				
			textEnd=iRangeData.End();
			textStart=iRangeData.Start(NULL);
			if(textEnd==textStart)
				return kFalse;
			
			PageData pData;

			bReader.getBoxInformation(boxUIDRef, pData, textStart, textEnd);
			appendToGlobalList(pData);
			seltext=1;
			//CA("seltext");
*/
			///////////// Code to get Elementlist from Document/////////////////
			if(!getDocumentSelectedBoxIds())
			{
				//CA("getDocumentSelectedBoxIds False");
				
				return kFalse;
			}
			for(i=0; i<selectUIDList.Length(); i++)
			{
				PageData pData;
				bReader.getBoxInformation(selectUIDList.GetRef(i), pData);
				appendToGlobalList(pData);
				// Added By Awasthi
				UIDRef boxID=selectUIDList.GetRef(i);
				TagList tList;
				//TagReader tReader;
				tList=itagReader->getTagsFromBox_ForRefresh(boxID);
				if(tList.size()==0)
				{
					tList=itagReader->getFrameTags(boxID);
					if(tList.size()==0)
					{
						//FlagParentID = 1;
						continue;
					}
				}
				for(int j=0; j<tList.size(); j++)
				{				
					if(tList[j].parentId==-1)
						continue;
					k=1;
				}
				if(k==0)
					FlagParentID = 1;
				
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
			}
		}while(kFalse);
		break;
	}
	return kTrue;
}


bool16 Refresh::refreshThisBox(UIDRef& boxID, PMString& imagePath)
{
	//CA(__FUNCTION__);
	TagList tList;
	//TagReader tReader;
//	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return kFalse;
	}
	tList=itagReader->getTagsFromBox_ForRefresh(boxID);

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}

	
	//CA("11");
	if(tList.size()==0)
	{	
		//CA("tList.size()==0");
		tList=itagReader->getFrameTags(boxID);
		if(tList.size()==0)
			return kTrue;
		//CA("11.5");
		refreshTaggedBox(boxID, imagePath);
		return kTrue;
	}

	if(tList[0].isTablePresent == 1)
	{	//CA("Going fro refresh Tagged box");
		refreshTaggedBox(boxID, imagePath);
		return kTrue;
	}
	
	/*if(IsDeleteFlagSelected == kTrue)
	{
		bool16 ISProdDeleted = kFalse;
		ISProdDeleted  = ptrIAppFramework->IsProductDeleted(tList[0].sectionID, tList[0].parentId);
		if(ISProdDeleted)
		{
			this->deleteThisBox(boxID);
			return kFalse;
		}
	}*/

	for(int j=0; j<tList.size(); j++)
	{	
		if(!shouldRefresh(tList[j], boxID.GetUID()))
			continue;

		if(tList[j].parentId==-1)
			continue;

		if(tList[j].imgFlag==1)
			fillImageInBox(boxID, tList[j], tList[j].parentId, imagePath);
		else if(tList[j].imgFlag==0)
		{	
			//PMString QWE("tList[j] tList[j].parentId : ");
			//QWE.AppendNumber(tList[j].parentId);
			////CA(QWE);
			fillDataInBox(boxID, tList[j], tList[j].parentId);
		}
	}

	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}

	return kTrue;
}

bool16 Refresh::refreshTaggedBox(UIDRef& boxID, PMString& imagePath)
{
	// CA("Refresh::refreshTaggedBox");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
		return kFalse;
	}


	//TagReader tReader;
//	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return kFalse;
	}

	TagList tList;
	TableUtility tUtility;
	tList=itagReader->getFrameTags(boxID);
	UIDRef tableUIDRef;
	if(tList.size()==0 /*|| tList.size()>1*/)
		return kFalse;
	
	if(tList[0].parentId==-1)
		return kFalse;
 
	if(!shouldRefresh(tList[0], boxID.GetUID(), kTrue))
		return kFalse;

	PMString theData;

	if(tList[0].whichTab!=4)//For copy items
	{
		/*** Following is Ritesh's code for Table refresh ***
		getDataFromDB(theData, tList[0], tList[0].parentId); // parentid contains objectid
		if(tUtility.isTablePresent(boxID, tableUIDRef))
		{
			vector< vector<PMString> > tableText;

			int numCols=parseTheText(theData, tableText);

			tUtility.resizeTable(tableUIDRef, tableText.size(), numCols);
3
			for(int j=0; j<tableText.size(); j++)
			{
				for(int k=0; k<numCols; k++)
				{
					if(tableText[j].size()<=k)
						tUtility.setTableRowColData(tableUIDRef, PMString(" "), j, k);
					else
						tUtility.setTableRowColData(tableUIDRef, tableText[j][k], j, k);
				}
			}
		}
		else
			setTextInBox(boxID, theData, textAction::OverWrite);//Later changes here
		
		*********** My Changes for Table refresh start from here ****************/
		/* Check if table exists */
		if(tUtility.isTablePresent(boxID, tableUIDRef))
		{

			double tableId = tList[0].tableId;//0;

			// CA("Table exists");
			// resize table to original nos of columns and rows
		//	tUtility.fillDataInTable(tableUIDRef, tList[0].parentId, tList[0].typeId, tableId, tList[0].imgFlag);
			tUtility.fillDataInTable(tableUIDRef, tList[0].parentId, tList[0].typeId, tableId, tList[0].imgFlag, tList[0], boxID);
		}
		/******************************** My Changes end here **********************************/
	}
	/*
	else
	{
		vector<int32> itemIDList;
		int numRows=getAllItemIDs(tList[0].parentId, tList[0].imgFlag, itemIDList);
		if(numRows==0)
		{
			setTextInBox(boxID, PMString(""), textAction::OverWrite);
			return kTrue;
		}

		vector<PMString> rowString;
		for(int i=0; i<numRows; i++)
		{
			PMString tempStr;
			if(!getDataFromDB(tempStr, tList[0], itemIDList[i]))
				return kFalse;
			rowString.push_back(tempStr);
		}
			
		if(tUtility.isTablePresent(boxID, tableUIDRef))
		{
			CA("Else...table present");
			PMString tempStr;
			tUtility.resizeTable(tableUIDRef, 1, 1);
			for(int j=0; j<numRows; j++)
			{
				if(j!=numRows-1)
					rowString[j].Append("\r");
				tempStr.Append(rowString[j]);
			}
			tUtility.setTableRowColData(tableUIDRef, tempStr, 0, 0);
		}
		else
		{
			for(int j=0; j<numRows; j++)
			{
				if(j!=numRows-1)
					rowString[j].Append("\r");
				if(!j)
					setTextInBox(boxID, rowString[j], textAction::OverWrite);
				else
					setTextInBox(boxID, rowString[j], textAction::AtEnd);
			}
		}
	}*/

	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}

	return kTrue;
}


bool16 Refresh::setTextInBox(UIDRef& boxID, PMString& textToInsert, enum textAction action)
{
	//CA(__FUNCTION__);	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return kFalse;

	/*InterfacePtr<IPMUnknown> unknown(boxID, IID_IUNKNOWN);
	UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxID, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::setTextInBox::textFrameUID == kInvalidUID");	
		return kFalse;
	}
/*		//CS3 Change
	InterfacePtr<ITextFrame> textFrame(boxID.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	if (textFrame == NULL)
		return kFalse;
*/	
//////Added by Amit
	
	InterfacePtr<IHierarchy> graphicFrameHierarchy(boxID, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::setTextInBox::graphicFrameHierarchy == NULL");
		return kFalse;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy) 
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::setTextInBox::graphicFrameHierarchy == NULL");
		return kFalse;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame) {
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::setTextInBox::multiColumnItemTextFrame == NULL");
		//CA("Its Not MultiColumn");
		return kFalse;
	}
	InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy) {
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::setTextInBox::frameItemHierarchy == NULL");
		return kFalse;
	}

	InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
	if (!textFrame) {
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::setTextInBox::textFrame == NULL");
		return kFalse;
	}
////end
	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	if (textModel == NULL)
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::setTextInBox::textModel == NULL");	
		return kFalse;
	}
	TextIndex startIndex = textFrame->TextStart();
	TextIndex finishIndex = startIndex + textModel->GetPrimaryStoryThreadSpan()-1;


	InterfacePtr<ITextFocusManager> textFocusManager(textModel, UseDefaultIID());
	if (textFocusManager == NULL)
		return kFalse;

	InterfacePtr<ITextFocus> frameTextFocus(textFocusManager->NewFocus(RangeData(startIndex, finishIndex, RangeData::kLeanForward)));
	if (frameTextFocus == NULL)
		return kFalse;
    
	textToInsert.ParseForEmbeddedCharacters();
	WideString* myText=new WideString(textToInsert);
	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	
	if(action==Refresh::OverWrite)
	{
		//ErrorCode Err = textModel->Replace(kTrue,startIndex, finishIndex, myText); ///CS3 Change
		if(iConverter)
		{
			iConverter->ChangeQutationMarkONOFFState(kFalse);			
		}
		ErrorCode Err = textModel->Replace(startIndex, finishIndex, myText);
		if(iConverter)
		{
			iConverter->ChangeQutationMarkONOFFState(kTrue);			
		}
		/*
		InterfacePtr<ICommand> pInsertTextCommand(textModel->ReplaceCmd(startIndex, finishIndex, myText, kFalse, NULL));
		if (pInsertTextCommand ==NULL )
			return kFalse;

		if (CmdUtils::ProcessCommand(pInsertTextCommand)!=kSuccess )
			return kFalse;
		*/
	}
	else if(action==Refresh::AtEnd)
	{
//		textModel->Insert(kTrue,finishIndex, myText); //CS3 Change
		if(iConverter)
		{
			iConverter->ChangeQutationMarkONOFFState(kFalse);			
		}
		textModel->Insert(finishIndex, myText);
		if(iConverter)
		{
			iConverter->ChangeQutationMarkONOFFState(kTrue);			
		}
/*		InterfacePtr<ICommand> pInsertTextCommand(textModel->InsertCmd(finishIndex, myText, kFalse, NULL));
		if (pInsertTextCommand ==NULL )
			return kFalse;

		if (CmdUtils::ProcessCommand(pInsertTextCommand)!=kSuccess )
			return kFalse;*/	
	}
	else
		return kFalse;//Will implement later if required

	if(myText)
		delete myText;

	return kTrue;
}
////////////////////////////////[ From old refresh ]//////////////////////////////////////

bool16 fileExists(PMString& path, PMString& name)
{
	//CA(__FUNCTION__);
	PMString theName("");
	theName.Append(path);
	theName.Append(name);

	std::string tempStr = theName.GetPlatformString();
	const char *file=tempStr.c_str();

	FILE *fp=NULL;

	fp=std::fopen(file, "r");
	if(fp!=NULL)
	{
		std::fclose(fp);
		return kTrue;
	}
	return kFalse;
}

bool16 Refresh::doesExist(IIDXMLElement * ptr)
{
	//CA(__FUNCTION__);
	//TagReader tReader;
//	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return kFalse;
	}
	IIDXMLElement *xmlPtr;
	
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(i), &xmlPtr);
		if(ptr==xmlPtr)
			return kTrue;
	}
	return kFalse;
}


bool16 Refresh::getAllBoxIds(void)
{
	//CA(__FUNCTION__);
	//TagReader tReader;
//	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return kFalse;
	}
/*	InterfacePtr<ISelection> selection(::QuerySelection());
	if(selection == NULL)
		return kFalse;

	scoped_ptr<UIDList> _selectUIDList(selection->CreateUIDList());
	if(_selectUIDList==NULL)
		return kFalse;

	const int32 listLength=_selectUIDList->Length();
*/	
	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());

	if(!iSelectionManager)
	{	
		return 0;
	}
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(!txtMisSuite)
	{
	  return 0; 
	}

//	UIDList	selectUIDList;
	txtMisSuite->GetUidList(selectUIDList);

	const int32 listLength=selectUIDList.Length();
	PMString len;
	len.Append("Length=");
	len.AppendNumber(listLength);
		if(listLength==0)
	{
       	return kFalse;
	}
	
//	selectUIDList=*(_selectUIDList);

	for(int i=0; i<selectUIDList.Length(); i++)
	{
		//InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		//if(!iHier)
		//	continue;
        //UID kidUID;
		//int32 numKids=iHier->GetChildCount();
		//for(int j=0;j<numKids;j++)
		//{
		IIDXMLElement* ptr;
	//	kidUID=iHier->GetChildUID(j);
	//	UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
		TagList tList = itagReader->getTagsFromBox_ForUpdate(selectUIDList.GetRef(i), &ptr);
	//	if(!doesExist(ptr))
	//	{
    //   	selectUIDList.Append(kidUID);
	//	}
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	//}
	}
	return kTrue;
}

void Refresh::showTagInfo(UIDRef boxRef)
{
	//CA(__FUNCTION__);
	TagList tList;
	//TagReader tReader;
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return ;
	}
	tList=itagReader->getTagsFromBox_ForRefresh(boxRef);
	PMString allInfo;
	for(int numTags=0; numTags<tList.size(); numTags++)
	{
		allInfo.Clear();
		allInfo.AppendNumber(PMReal(tList[numTags].elementId));
		allInfo+="\n";
		allInfo.AppendNumber(PMReal(tList[numTags].parentId));
		allInfo+="\n";
		allInfo.AppendNumber(PMReal(tList[numTags].typeId));
		allInfo+="\n";
		allInfo.AppendNumber(tList[numTags].imgFlag);
		allInfo+="\n";
		allInfo+="Start and end index :";
		allInfo.AppendNumber(tList[numTags].startIndex);
		allInfo+=" / ";
		allInfo.AppendNumber(tList[numTags].endIndex);
		CAlert::InformationAlert(allInfo);
	}
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
}

bool16 Refresh::shouldRefresh(const TagStruct& tInfo, const UID& boxID, bool16 isTaggedFrame)
{
	CA(__FUNCTION__);
	for(int i=0; i<rDataList.size(); i++)
	{
		if(/*boxID==rDataList[i].boxID &&*/ rDataList[i].isSelected && (!rDataList[i].isObject || rDataList[i].isTableFlag==kTrue))
		{
			if(!isTaggedFrame)
			{
				if(rDataList[i].elementID==tInfo.elementId && rDataList[i].objectID==tInfo.parentId)// && rDataList[i].publicationID==tInfo.sectionID)//Later make changes here
				{
					return kTrue;
				}
			}
			else{
				if(rDataList[i].objectID==tInfo.parentId && rDataList[i].TypeID ==tInfo.typeId )// && rDataList[i].publicationID==tInfo.sectionID)//Later make changes here
				{
					return kTrue;
				}
			}
		}
	}
	
	return kFalse;
}

void Refresh::doRefresh(int option)
{	
	//CA(__FUNCTION__);
	PMString imagePath;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*>(CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	return;
	InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
	if(ptrIClientOptions==NULL)
	{
		ptrIAppFramework->LogError("AP46_UpdateRepository::Refresh::doRefresh::Interface for IClientOptions not found.");
		return;
	}
	imagePath=ptrIClientOptions->getImageDownloadPath();
	if(imagePath!="")
	{
		std::string tempStr = imagePath.GetPlatformString();
		const char *imageP=tempStr.c_str();
		if(imageP[std::strlen(imageP)-1]!='\\' || imageP[std::strlen(imageP)-1]!=':')
			#ifdef MACINTOSH
				imagePath+=":";
			#else
				imagePath+="\\";
			#endif
	}
	selectUIDList.Clear();
	int i;
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		ptrIAppFramework->LogError("AP46_UpdateRepository::Refresh::doRefresh::Interface for TagReader not found.");
		return ;
	}

	switch(option)
	{
	case 1:
	case 2:
        			
		if(!getAllPageItemsFromPage(Mediator::refreshPageNumber))
			return;
		
		for(i=0; i<selectUIDList.Length(); i++)
		{	
			UIDRef boxID=selectUIDList.GetRef(i);
			this->refreshThisBox(boxID, imagePath);
		}
		break;
	case 3:
		if(!getAllBoxIds())
			return;
		for(i=0; i<selectUIDList.Length(); i++)
		{
			UIDRef boxID=selectUIDList.GetRef(i);
			this->refreshThisBox(boxID, imagePath);
		}
		break;
	case 4:
	/*		InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());

			if(!iSelectionManager)
			{	
			//	CA("null selection");
				return;
			}
			// Get text selection 
			InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
			InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID()); 

			InterfacePtr<ITextFocus> iFocus(pTextTarget->QueryTextFocus()); 

	ITextFocus * iFocus=Utils<ISelectUtils>()->QueryTextFocus();
*/
/*		if(!iFocus)
			return;

		InterfacePtr<ITextModel> textModel(iFocus->QueryModel());
		if (textModel==NULL)
			return;
		
		IFrameList* frameList=textModel->QueryFrameList();
		if(!frameList)
			return;
		UID frameUID=frameList->GetNthFrameUID(0);
				
		IDocument *docPtr=::GetFrontDocument();
		IDataBase* db =::GetDataBase(docPtr);
		if(db==NULL)
			return;

		UIDRef boxUIDRef(db, frameUID);

		this->refreshThisBox(boxUIDRef, imagePath);
*/
		if(!getDocumentSelectedBoxIds())
			{	//CA("getDocumentSelectedBoxIds False");				
				return ;
			}

		for(i=0; i<selectUIDList.Length(); i++)
		{
			UIDRef boxID=selectUIDList.GetRef(i);
			this->refreshThisBox(boxID, imagePath);
		}
		break;
	}
}


void Refresh::fillDataInBox(const UIDRef& curBox, TagStruct& slugInfo, double objectId)
{	
	//CA(__FUNCTION__);
	//int32 count[50];
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return;

	/*InterfacePtr<IPMUnknown> unknown(curBox, IID_IUNKNOWN);
	UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(curBox, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}
	if (textFrameUID == kInvalidUID)
		return;
	/*	////CS3 Change
	InterfacePtr<ITextFrame> textFrame(curBox.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	if (textFrame == NULL)
		return;
	*/	
	///////Added By Amit

	InterfacePtr<IHierarchy> graphicFrameHierarchy(curBox, UseDefaultIID());
	if (graphicFrameHierarchy == nil) 
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::fillDataInBox::graphicFrameHierarchy == NULL");	
		return;
	}
					
	InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
	if (!multiColumnItemHierarchy) {
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::fillDataInBox::multiColumnItemHierarchy == NULL");
		return;
	}

	InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
	if (!multiColumnItemTextFrame) {
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::fillDataInBox::multiColumnItemTextFrame == NULL");
		//CA("Its Not MultiColumn");
		return;
	}
	InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
	if (!frameItemHierarchy)
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::fillDataInBox::frameItemHierarchy == NULL");
		return;
	}

	InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
	if (!textFrame) {
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::fillDataInBox::textFrame == NULL");
		//CA("!!!ITextFrameColumn");
		return;
	}

	///end
	TextIndex startIndex = textFrame->TextStart();
	TextIndex finishIndex = startIndex + textFrame->TextSpan()-1;

	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	if (textModel == NULL)
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::fillDataInBox::textModel == NULL");
		return;
	}
	InterfacePtr<ITextFocusManager> textFocusManager(textModel, UseDefaultIID());
	if (textFocusManager == NULL)
		return;
	InterfacePtr<ITextFocus> frameTextFocus(textFocusManager->NewFocus(RangeData(startIndex, finishIndex, RangeData::kLeanForward)));
	if (frameTextFocus == NULL)
		return;
	int32 tStart1=0;
	int32 tEnd1=0;
	//TagReader tReader1;
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return ;
	}

	TagList tList1;
	tList1=itagReader->getTagsFromBox_ForRefresh(curBox);
	if(!tList1.size())//The box contains no tags..But it can be the tagged frame!!!!!
	{
		tList1=itagReader->getFrameTags(curBox);
		if(tList1.size()<=0)//It really is not our box
			return ;			
	}
	
	for(int j=0; j<tList1.size(); j++)
	{	
		if((slugInfo.elementId == tList1[j].elementId) && (slugInfo.whichTab==tList1[j].whichTab))	//if(slugInfo.elementId == tList1[j].elementId)
		{	
			tStart1 = tList1[j].startIndex+1;
			tEnd1 = tList1[j].endIndex-1;
			break;
		}
	}
	//int32 tStart1=slugInfo.startIndex;
	//int32 tEnd1=slugInfo.endIndex-1;
	PMString ZXC("tStart1 : ");
	ZXC.AppendNumber(tStart1);
	ZXC.Append("  tEnd1 : ");
	ZXC.AppendNumber(tEnd1);
	//CA(ZXC);
	
	char *originalString="";
	char *changedString="";
	PMString entireStory("");
	bool16 result1 = kFalse;
	
	result1=this->GetTextstoryFromBox(textModel, tStart1, tEnd1, entireStory);

	std::string tempStr = entireStory.GetPlatformString();
	changedString	= const_cast<char*>(tempStr.c_str());
	int32 tStart, tEnd;

	//TagReader tReader;
	
	if(!itagReader->GetUpdatedTag(slugInfo))
		return;
	tStart=slugInfo.startIndex+1;
	tEnd=slugInfo.endIndex-tStart;
	
	PMString textToInsert("");
	getDataFromDB(textToInsert, slugInfo, objectId);

	std::string tempStr1 = textToInsert.GetPlatformString();
	originalString	= const_cast<char*>(tempStr1.c_str()); 
	int32 flag=0;
	
	///////////////////////////rahul
    textToInsert.ParseForEmbeddedCharacters();
	WideString* myText=new WideString(textToInsert);	
//	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	if(!iConverter)
	{	
		return;
	}
	else
		iConverter->ChangeQutationMarkONOFFState(kFalse);

	//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText); /////CS3 Change
	ErrorCode Err = textModel->Replace(tStart, tEnd, myText);
	if(iConverter)
		iConverter->ChangeQutationMarkONOFFState(kTrue);
	int32 result2 =0;
	result2= textToInsert.Compare(kTrue, entireStory );
	

	TagList tList2;
	tList2=itagReader->getTagsFromBox_ForRefresh(curBox);
	if(!tList2.size())//The box contains no tags..But it can be the tagged frame!!!!!
	{
		tList2=itagReader->getFrameTags(curBox);
		if(tList2.size()<=0)//It really is not our box
			return ;			
	}
	for(int i=0; i<tList2.size(); i++)
	{	
		if((slugInfo.elementId == tList2[i].elementId) && (slugInfo.whichTab==tList2[i].whichTab))//slugInfo.elementId == tList[i].elementId)
		{
			tStart = tList2[i].startIndex+1;
			tEnd = tList2[i].endIndex;
			break;
		}
	}
	PMString GreenColour ("C=75 M=5 Y=100 K=0");
	char * OriginalString= NULL;
	char * NewString= NULL;
	std::string tempStr2 = entireStory.GetPlatformString();
	OriginalString = const_cast<char*>(tempStr2.c_str());
	std::string tempStr3 = textToInsert.GetPlatformString();
	NewString= const_cast<char*>(tempStr3.c_str()); 
	int32 k1=0;
	PMString ABC("");
	NewTextList.clear();
	int32 Q=tStart;
	int32 Flagg=0;
	for(int32 p=tStart; p<=tEnd; p++)
	{ 			
		TextVector Textvector1;
		if(NewString[k1]== ' '|| p==tEnd)
		{ 
			if(Flagg==0)
			{
				Textvector1.Word.Clear();
				Textvector1.Word.Append(ABC);
				Textvector1.StartIndex=Q;
				Textvector1.EndIndex=Textvector1.StartIndex + k1;
				Q=Textvector1.EndIndex+1;
				NewTextList.push_back(Textvector1);
				ABC.Clear();
				Flagg=1;
			}
			else
			{
				Textvector1.Word.Clear();
				Textvector1.Word.Append(ABC);
				Textvector1.StartIndex=Q;
				Textvector1.EndIndex=tStart+k1;
				Q=Textvector1.EndIndex+1;
				NewTextList.push_back(Textvector1);
				ABC.Clear();
			}
		}
		else
		{
			ABC.Append(NewString[k1]);
		}
		k1++;
	}
	Flagg=0;
	ABC.Clear();
	k1=0;
	OriginalTextList.clear();
	Q=tStart1;
	for( int32 p=tStart1; p<=tEnd1+1; p++)
	{ 
		TextVector Textvector1;
		if(OriginalString[k1]== ' ' || p==tEnd1+1)
		{ 
			if(Flagg==0)
			{
				Textvector1.Word.Clear();
				Textvector1.Word.Append(ABC);
				Textvector1.StartIndex=Q;
				Textvector1.EndIndex=Textvector1.StartIndex + k1;
				Q=Textvector1.EndIndex+1;
				OriginalTextList.push_back(Textvector1);
				ABC.Clear();
				Flagg=1;
			}
			else
			{
				Textvector1.Word.Clear();
				Textvector1.Word.Append(ABC);
				Textvector1.StartIndex=Q;
				Textvector1.EndIndex=tStart+k1;
				Q=Textvector1.EndIndex+1;
				OriginalTextList.push_back(Textvector1);
				ABC.Clear();
			}
		}
		else
		{
			ABC.Append(OriginalString[k1]);
		}
		k1++;
	}
	
	if(result2!=0 && HiliteFlag==kTrue)
	{	
		RefreshData ColorData;
		ColorData.BoxUIDRef= curBox;
		//TagReader tReader;
		TagList tList;
		tList=itagReader->getTagsFromBox_ForRefresh(curBox);
		if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
		{
			tList=itagReader->getFrameTags(curBox);
			if(tList.size()<=0)//It really is not our box
				return ;			
		}
		for(int i=0; i<tList.size(); i++)
		{	
			if((slugInfo.elementId == tList1[i].elementId) && (slugInfo.whichTab==tList1[i].whichTab))//slugInfo.elementId == tList[i].elementId)
			{
				tStart = tList[i].startIndex+1;
				tEnd = tList[i].endIndex;
				break;
			}
		}
		ChangedTextList.clear();
///////////////////////////////rahul
	
/*	
			
	int32 srt,en,flag2=0,k=0,a,b=0,c=0,tot1,tot2;//srt1,en1,
	char Odoubl[100][500],Cdoubl[100][500];
	srt = tStart1;
	en = tEnd1;
	b=0;
	
	for(a=0;originalString[a];a++)
	{
		if(originalString[a] == ' ' && originalString[a+1] == ' ')
			continue;
		if(originalString[a] == ' ')
		{
			Odoubl[b][c] = '\0';
			b++;
			c=0;
		}
		else
		{
			Odoubl[b][c] = originalString[a];
			c++;
		}
	}
	Odoubl[b][c] = '\0';
	tot1=b;
	b=0;
	c=0;
	for(a=0;changedString[a];a++)
	{
		if(changedString[a] == ' ' && changedString[a+1] == ' ')
			continue;
		if(changedString[a] == ' ')
		{
			Cdoubl[b][c] = '\0';
			b++;
			c=0;
		}
		else
		{
			Cdoubl[b][c] = changedString[a];
			c++;
		}
	}
	Cdoubl[b][c] = '\0';
	tot2=b;
	/////////////////////////////
	for(a=0;a<=tot2;a++)
	{
		PMString ab("");
		ab.Append(Cdoubl[a]);
		//CA(ab);
	}
	int32 m=0,n=0,ind=0;
	int32 Counter=0; 
	if(tot1==tot2)
	{
		for(a=0;a<=tot2;a++)
		{
			if(strcmp(Odoubl[a],Cdoubl[a])!=0)
				{
					PMString abc("");
					abc.Append(Odoubl[a]);
					TextVector Textvector1;
					Textvector1.Word.Clear();
					Textvector1.Word.Append(abc);
					ChangedTextList.push_back(Textvector1);
                    CA(abc);
				}
				
		}
	}
	else
	{
		for(a=0;a<=tot2;a++)
		{
			b=m;
			Counter = 0;
			for(;b<=tot1;b++)
			{
				
				if(strcmp(Odoubl[b],Cdoubl[a])==0)
					{
						m=b+1;
						break;
					}
				else
					{
						flag2 = 0;
						for(c=0;c<=tot2;c++)
						{
							if(strcmp(Odoubl[b],Cdoubl[c])==0)
							{
								flag2 = 1;
							}
						}
						if(flag2 == 0)
						{
							PMString abc("");
							abc.Append(Odoubl[b]);
							TextVector Textvector1;
							Textvector1.Word.Clear();
							Textvector1.Word.Append(abc);
							ChangedTextList.push_back(Textvector1);
							CA(abc);
						}
											
					}
					Counter++;

			}
			
			
		}
	}
	

*/
	// Awasthi Added
		
		int32/*int64*/ tot1=static_cast<int32>(NewTextList.size());
		int32/*int64*/ tot2=static_cast<int32>(OriginalTextList.size());
		
		int32 m=0, flag2=0;
		if(tot1==tot2)
		{	
			for(int32 a=0; a<tot2; a++)
			{	
				if((NewTextList[a].Word.Compare(kTrue, OriginalTextList[a].Word))!=0)
				{							
					PMString abc("");
					abc.Append(NewTextList[a].Word);
					TextVector Textvector1;
					Textvector1.Word.Clear();
					Textvector1.Word.Append(abc);
					Textvector1.StartIndex= NewTextList[a].StartIndex;
					Textvector1.EndIndex= NewTextList[a].EndIndex;
					ChangedTextList.push_back(Textvector1);		
				}					
			}
		}
		else
		{
			for(int32 a=0;a<tot2;a++)
			{
				int32 b=m;
				
				for(;b<tot1;b++)
				{					
					if((NewTextList[b].Word.Compare(kTrue, OriginalTextList[a].Word)==0))//(strcmp(Odoubl[b],Cdoubl[a])==0)
					{
						m=b+1;
						break;
					}
					else
					{
						flag2 = 0;
						for(int32 c=0;c<tot2;c++)
						{
							if(((NewTextList[b].Word.Compare(kTrue, OriginalTextList[c].Word))==0))//(strcmp(Odoubl[b],Cdoubl[c])==0)
							{
								flag2 = 1;
							}
						}
						if(flag2 == 0)
						{									
							PMString abc("");
							abc.Append(NewTextList[b].Word);
							TextVector Textvector1;
							Textvector1.Word.Clear();
							Textvector1.Word.Append(abc);
							Textvector1.StartIndex= NewTextList[b].StartIndex;
							Textvector1.EndIndex= NewTextList[b].EndIndex;
							ChangedTextList.push_back(Textvector1);
						}												
					}
				}				
			}
		}
			
		for(int32 s=0; s<ChangedTextList.size();s++)
		{	
			ColorData.elementID=slugInfo.elementId;
			ColorData.StartIndex=ChangedTextList[s].StartIndex;
			ColorData.EndIndex=ChangedTextList[s].EndIndex; 
			this->ChangeColorOfText(textModel, ChangedTextList[s].StartIndex, ChangedTextList[s].EndIndex, GreenColour);
			ColorDataList.push_back(ColorData);
		}
		
		for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
	}
	
	for(int32 tagIndex = 0 ; tagIndex < tList2.size() ; tagIndex++)
	{
		tList2[tagIndex].tagPtr->Release();
	}
	for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
	{
		tList1[tagIndex].tagPtr->Release();
	}
	
	if(myText)
		delete myText;
}



bool16 Refresh::getDataFromDB(PMString& textToInsert, TagStruct& slugInfo, double objectId)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return kFalse;

	char* tempText=NULL;

	do
	{
		CObjectValue oVal;
		//if(slugInfo.elementId==-1 && slugInfo.typeId==-1)//NAME eg pfname/pgname/prname etc.
		//{
		//	PMString myTemp;
		//	//temp_to_test oVal=ptrIAppFramework->FAMMngr_getObjectElementValues(objectId);
		//	if(oVal.getRef_id()==-1)
		//		break;
		//	myTemp=oVal.getName();
		//	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		//	if(!iConverter)
		//	{
		//		textToInsert=myTemp;
		//		break;
		//	}
		//	textToInsert=iConverter->translateString(myTemp);
		//	break;
		//}

		//if(slugInfo.elementId==-2 && slugInfo.typeId==-1)//PR Number
		//{
		//	PMString myTemp;
		//	//temp_to_test oVal=ptrIAppFramework->FAMMngr_getObjectElementValues(objectId);
		//	if(oVal.getRef_id()==-1)
		//		break;
		//	myTemp=oVal.get_number();
		//	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		//	if(!iConverter)
		//	{
		//		textToInsert=myTemp;
		//		break;
		//	}
		//	textToInsert=iConverter->translateString(myTemp);
		//	break;
		//}

		if(slugInfo.whichTab==1 || slugInfo.whichTab==2 || slugInfo.whichTab==3 )
		{
			PMString tempBuffer;
			tempBuffer=ptrIAppFramework->GETProduct_getAttributeValueByLanguageID(objectId ,slugInfo.elementId, slugInfo.languageID, slugInfo.sectionID, kFalse);
			if(tempBuffer == "")
				tempBuffer.Append("N/A");
//			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
			if(!iConverter)
			{
				textToInsert=tempBuffer;
				break;
			}
			textToInsert=iConverter->translateString(tempBuffer);
			break;
		}
			
		//temp_to_test oVal=ptrIAppFramework->FAMMngr_getObjectElementValues(objectId);
		
		//if(oVal.getRef_id()==-1)// This object may have been deleted
		//	break;
	
		//tempText;//temp_to_test =ptrIAppFramework->PRODMngr_getProductColData(oVal.getRef_id(), slugInfo.elementId, slugInfo.sectionID);
		//textToInsert.Append(tempText);
		if(tempText)
			delete []tempText;
	}while(0);   
	return kTrue;
}

void Refresh::fillImageInBox(const UIDRef& boxUIDRef, TagStruct& slugInfo, double objectId, PMString imagePath)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return;

	if(imagePath=="")
	{
		ptrIAppFramework->LogError("AP46_UpdateRepository::Refresh::fillImageInBox::imagepath is blank");	
		return;
	}

	PMString imagePathWithSubdir=imagePath;
	PMString fileName("");
	int32 assetID;

	if(slugInfo.whichTab!=4)
	{
		//temp_to_test 	fileName=ptrIAppFramework->getObjectAssetName(objectId, slugInfo.parentTypeID, slugInfo.typeId);
		VectorAssetValuePtr AssetValuePtrObj = ptrIAppFramework->GETAssets_GetAssetByParentAndType(objectId,slugInfo.parentTypeID, slugInfo.typeId);
		if(AssetValuePtrObj == NULL)
		{
			ptrIAppFramework->LogError("AP46_UpdateRepository::Refresh::fillImageInBox::GETAssets_GetAssetByParentAndType's AssetValuePtrObj == NULL");		
			return ;
		}

		if(AssetValuePtrObj->size() ==0)
			return ;

		VectorAssetValue::iterator it; // iterator of Asset value

		CAssetValue objCAssetvalue;
		for(it = AssetValuePtrObj->begin();it!=AssetValuePtrObj->end();it++)
		{
			objCAssetvalue = *it;
			fileName = objCAssetvalue.geturl();
			assetID = objCAssetvalue.getAsset_id();

		}
	}
	else
	{
		//CObjectValue oVal;

		//temp_to_test oVal=ptrIAppFramework->FAMMngr_getObjectElementValues(objectId);
		
		//if(oVal.getRef_id()==-1)//This object may have been deleted
		//	return;

		//temp_to_test fileName=ptrIAppFramework->getObjectAssetName(oVal.getRef_id(), slugInfo.parentTypeID, slugInfo.typeId);
	}
	
	if(fileName=="")
		return;

	//if(1)//temp_to_test ptrIAppFramework->getSubDirectoryFlag())
	//{
	//	PMString typeCodeString;
	//	//temp_to_test typeCodeString=ptrIAppFramework->TYPECACHE_getTypeCode(slugInfo.typeId);

	//	if(typeCodeString.NumUTF16TextChars()==0)
	//		return;

	//	imagePathWithSubdir.Append(typeCodeString);
	//	
	//	#ifdef WINDOWS
	//		imagePathWithSubdir.Append("\\");
	//	#endif

	//}

	#ifdef WINDOWS
			imagePathWithSubdir.Append("\\");
	#endif
	
	if(!fileExists(imagePathWithSubdir,fileName))
	{
		/*int answer=1;
		PMString message("\"");
		message.Append(fileName);
		message.Append("\" was not found in the local drive. Press OK to download the file.");
		answer=CAlert::ModalAlert(message, "OK", "Cancel", "", 1, CAlert::eQuestionIcon);
		if(answer==2)
			return;*/
		//if(!ptrIAppFramework->GETAsset_downLoadProductAsset(assetID,imagePathWithSubdir))
		//	return;
	}

	/*if(!fileExists(imagePathWithSubdir,fileName))
		return;*/

	PMString total=imagePathWithSubdir+fileName;

	if(!ImportFileInFrame(boxUIDRef, total))
		CAlert::InformationAlert("Unable to place picture in the box");
	else
		fitImageInBox(boxUIDRef);
}


bool16 Refresh::ImportFileInFrame(const UIDRef& imageBox, const PMString& fromPath)
{
	//CA(__FUNCTION__);
	bool16 fileExists = SDKUtilities::FileExistsForRead(fromPath) == kSuccess;
	if(!fileExists) 
		return kFalse;
	
	IDocument *docPtr=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
	IDataBase* db = ::GetDataBase(docPtr);
	if (db == NULL)
		return kFalse;

	IDFile sysFile = SDKUtilities::PMStringToSysFile(const_cast<PMString* >(&fromPath));	
	InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
	if(!importCmd) 
		return kFalse;
		
	//InterfacePtr<IImportFileCmdData> importFileCmdData(importCmd, IID_IIMPORTFILECMDDATA); // no DefaultIID for this
	//if(!importFileCmdData)
	//	return kFalse;
	//
	//importFileCmdData->Set(db, sysFile, kMinimalUI);
	
	//-----CS5---
	URI tmpURI;
	Utils<IURIUtils>()->IDFileToURI(sysFile, tmpURI);
	InterfacePtr<IImportResourceCmdData> importFileCmdData(importCmd, IID_IIMPORTRESOURCECMDDATA); // no kDefaultIID	
	if (importFileCmdData == nil){
		CA(" importResourceCmdData == nil ");
		return kFalse;
	}	
	importFileCmdData->Set(db,tmpURI,kMinimalUI);

	ErrorCode err = CmdUtils::ProcessCommand(importCmd);
	if(err != kSuccess) 
		return kFalse;

	InterfacePtr<IPlaceGun> placeGun(db, db->GetRootUID(), UseDefaultIID());
	if(!placeGun)
		return kFalse;
	
	//UIDRef placedItem(db, placeGun->GetItemUID()); ////CS3 Change
	UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());
	InterfacePtr<ICommand> replaceCmd(CmdUtils::CreateCommand(kReplaceCmdBoss));
	if (replaceCmd == NULL)
		return kFalse;

	InterfacePtr<IReplaceCmdData>iRepData(replaceCmd, IID_IREPLACECMDDATA);
	if(!iRepData)
		return kFalse;
	
	iRepData->Set(db, imageBox.GetUID(), placedItem.GetUID(), kFalse);

	ErrorCode status = CmdUtils::ProcessCommand(replaceCmd);
	if(status==kFailure)
		return kFalse;
	return kTrue;
}



int32 Refresh::getAllItemIDs(double parentID, double pubID, vector<double>& itemIDList)
{
	//CA(__FUNCTION__);
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return kFalse;
	//temp_to_test if(!ptrIAppFramework->PUBCntrller_getChildrensByObjectId(parentID, pubID, itemIDList))
		return 0;
	return (int32)itemIDList.size();
}


int32 Refresh::parseTheText(PMString& rowString, vector< vector<PMString> >& dataPtr)
{
	//CA(__FUNCTION__);
	int maxCount=0;
	int tempCount=0;

	dataPtr.clear();
	vector<PMString> stringList;

	stringList.clear();
	tempCount=0;
	PMString hayStack=rowString;

	PMString tempString;
	for(int j=0; j<hayStack.NumUTF16TextChars(); j++)
	{
		if(hayStack[j]=='\t')
		{
			stringList.push_back(tempString);
			tempString.Clear();
			tempCount++;
		}
		else if(hayStack[j]=='\r')
		{
			if(tempString.NumUTF16TextChars())
			{
				stringList.push_back(tempString);
				tempString.Clear();
				tempCount++;
			}
			dataPtr.push_back(stringList);
			if(tempCount>maxCount)
				maxCount=tempCount;
			tempString.Clear();
			stringList.clear();
			tempCount=0;
		}
		else
			tempString.Append(hayStack[j]);
	}

	if(tempString.NumUTF16TextChars())
	{
		stringList.push_back(tempString);
		tempString.Clear();
		tempCount++;
	}

	if(tempCount>maxCount)
		maxCount=tempCount;
	dataPtr.push_back(stringList);

	if(maxCount==0)
		maxCount=1;

	return maxCount;
}


void Refresh::fitImageInBox(const UIDRef& boxUIDRef)
{
	//CA(__FUNCTION__);
	do 
	{
		InterfacePtr<ICommand> iAlignCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
		if(!iAlignCmd)
			return;

		InterfacePtr<IHierarchy> iHier(boxUIDRef, IID_IHIERARCHY);
		if(!iHier)
			return;

		if(iHier->GetChildCount()==0)//There may not be any image at all????
			return;

		UID childUID=iHier->GetChildUID(0);
		UIDRef newChildUIDRef(boxUIDRef.GetDataBase(), childUID);
		iAlignCmd->SetItemList(UIDList(newChildUIDRef));
		CmdUtils::ProcessCommand(iAlignCmd);
	} while (false);
}

bool16 Refresh::GetTextstoryFromBox(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story)
{	
	//CA(__FUNCTION__);
	TextIterator begin(iModel, startIndex);
	TextIterator end(iModel, finishIndex);
	
	for (TextIterator iter = begin; iter <= end; iter++)
	{	
		/*const textchar characterCode = (*iter).GetTextChar();
		char buf;
		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
		story.Append(buf);*/
		//const textchar characterCode = (*iter).GetTextChar();
		char buf;
		//::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
		 UTF16TextChar utfChar[2];
		 int32 len=2;
		 (*iter).ToUTF16(utfChar,&len);
		::CTUnicodeTranslator::Instance()->TextCharToChar(utfChar, 1, &buf, 1); 
		story.Append(buf);
	}	
	return kTrue;
}

void Refresh::ChangeColorOfText(InterfacePtr<ITextModel> textModel1 ,int32 StartText1, int32 EndText1, PMString  altColorSwatch)
{
	//CA(__FUNCTION__);
	do
	{
		/*UIDRef	BoxRefUID1 = rDataList[c].BoxUIDRef; 
		int32 LObjectID1 = rDataList[c].objectID;
		int32 LElementID1 = rDataList[c].elementID;
		int32 StartText1 = rDataList[c].StartIndex;
		int32 EndText1 = rDataList[c].EndIndex;
		
		InterfacePtr<IPMUnknown> unknown(BoxRefUID1, IID_IUNKNOWN);
		UID textFrameUID1 = FrameUtils::GetTextFrameUID(unknown);
		if (textFrameUID1 == kInvalidUID)
			break;

		InterfacePtr<ITextFrame> textFrame1(BoxRefUID1.GetDataBase(), textFrameUID1, ITextFrame::kDefaultIID);
		if (textFrame1 == NULL)
			break;


		TextIndex startIndex2 = textFrame1->TextStart();
		
		InterfacePtr<ITextModel> textModel1(textFrame1->QueryTextModel());
		if (textModel1 == NULL)
			break;
		*/
		
		//InterfacePtr<IWorkspace> workSpace(Utils<ILayoutUtils>()->QueryActiveWorkspace());
		InterfacePtr<IWorkspace> workSpace(Utils<ILayoutUIUtils>()->QueryActiveWorkspace());
		InterfacePtr<ISwatchList> swatchList(workSpace,UseDefaultIID());
		if (swatchList == NULL)
		{	//CA("swatchList == NULL");
			ASSERT_FAIL("Unable to get swatch list from workspace!");
			break;
		}
		
		InterfacePtr<ITextAttrUID>
		textAttrUID(::CreateObject2<ITextAttrUID>(kTextAttrColorBoss));
		UID colorUID;
		UID altColorUID = kInvalidUID;
		
		UIDRef altColorRef = swatchList->FindSwatch (altColorSwatch);
		if (altColorRef.GetUID() == kInvalidUID)
		{
			ASSERT_FAIL("Unable to get alternate swatch in DeleteColor!");
			break;
		}
		else
		{
			altColorUID = altColorRef.GetUID();
		}
				
		textAttrUID->Set(altColorUID);
		int32 charCount = EndText1 -StartText1;
		
		InterfacePtr<ICommand> applyCmd(
						Utils<ITextAttrUtils>()->
						BuildApplyTextAttrCmd(textModel1,
						StartText1,
						charCount,
						textAttrUID,
						kCharAttrStrandBoss) );
		ErrorCode err = CmdUtils::ProcessCommand(applyCmd);
		
	}while(0);
}

bool16 Refresh::doesExist(IIDXMLElement * ptr, UIDRef BoxRef)
{
	//CA(__FUNCTION__);
	//TagReader tReader;
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return kFalse;
	}
	IIDXMLElement *xmlPtr;
	TagList tList1;
	tList1=itagReader->getTagsFromBox_ForRefresh(BoxRef, &xmlPtr);
	if(ptr==xmlPtr)
	{ 
		for(int32 j=0; j<tList1.size(); j++)
		{//CA("Inside check for Parent id");
			if(tList1[j].parentId==-1)
			{
				for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
				{
					tList1[tagIndex].tagPtr->Release();
				}
				return kFalse;
			}
		}
		for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
		{
			tList1[tagIndex].tagPtr->Release();
		}
		return kTrue;
	}
	for(int32 tagIndex = 0 ; tagIndex < tList1.size() ; tagIndex++)
	{
		tList1[tagIndex].tagPtr->Release();
	}
	return kFalse;
}

/// Function to get elements from Whole Document
bool16 Refresh::getDocumentSelectedBoxIds()
{
	//CA(__FUNCTION__);
   do
   {
	//TagReader tReader;
//	  InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return kFalse;
	}
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*>(CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
	return kFalse;
	IDocument* fntDoc = /*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument(); //Cs4
	if(fntDoc==NULL)
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::getDocumentSelectedBoxIds::!fntDoc");	
		return kFalse;
	}
	InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
	if (layout == NULL)
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::getDocumentSelectedBoxIds::!layout");		
		return kFalse;
	}
	IDataBase* database = ::GetDataBase(fntDoc);
	if(database==NULL)
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::getDocumentSelectedBoxIds::!database");			
		return kFalse;
	}
	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
	if (iSpreadList==NULL)
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::getDocumentSelectedBoxIds::!iSpreadList");				
		return kFalse;
	}
	bool16 collisionFlag=kFalse;
	UIDList allPageItems(database);

	for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
	{
		UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));

		InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
		if(!spread)
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::Refresh::getDocumentSelectedBoxIds::!iSpread");						
			return -1;
		}
		int numPages=spread->GetNumPages();

		for(int i=0; i<numPages; i++)
		{
			UIDList tempList(database);
			spread->GetItemsOnPage(i, &tempList, kFalse);
			allPageItems.Append(tempList);
		}
	}
	selectUIDList= allPageItems;
/*	for(int i=0; i<allPageItems.Length(); i++)
	{
		InterfacePtr<IHierarchy> iHier(allPageItems.GetRef(i), UseDefaultIID());
		if(!iHier)
			continue;

		UID kidUID;
		
		int32 numKids=iHier->GetChildCount();
		CA("9");
		for(int j=0;j<numKids;j++)
		{
			IIDXMLElement* ptr;

			kidUID=iHier->GetChildUID(j);
			UIDRef boxRef(allPageItems.GetDataBase(), kidUID);
			itagReader->getTagsFromBox_ForRefresh(boxRef, &ptr);
			CA("10");
			if(this->doesExist(ptr,boxRef))
			{	CA("11");
				selectUIDList.Append(kidUID);	
			//	this->showTagInfo(boxRef);
			}
			if(!this->doesExist(ptr,boxRef))
			{
			//	CA("XML tags not found ");				
			}
		}
	}
*/
		for(int i=0; i<selectUIDList.Length(); i++)
		{
			//InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
			//if(!iHier)
			//	continue;

			//UID kidUID;
			//int32 numKids=iHier->GetChildCount();
			//for(int j=0;j<numKids;j++)
			//{
				IIDXMLElement* ptr;
			//	kidUID=iHier->GetChildUID(j);
			//	UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
				itagReader->getTagsFromBox_ForRefresh(selectUIDList.GetRef(i), &ptr);
			//	if(!doesExist(ptr))
			//		selectUIDList.Append(kidUID);
			//}
		}

    }while(0);
	//if(selectUIDList.Length()==0)
	//	{
	//		return kFalse;
	//	}
	return kTrue;
  }


#include "VCPluginHeaders.h"
//#include "ITextFrame.h"
#include "ITextModel.h"
#include "ITableModelList.h"
#include "ITableCommands.h"
#include "TableUtility.h"
#include "IFrameUtils.h" //FrameUtils.h"
#include "IAppFramework.h"
#include "IDataSprayer.h"
#include "ITableUtility.h"
#include "CAlert.h"
#include "ITextFrameColumn.h"
/// Global Pointers
extern IAppFramework* ptrIAppFramework;
///////////

#define CA(x) CAlert::InformationAlert(x)


bool16 TableUtility::isTablePresent(const UIDRef& boxUIDRef, UIDRef& tableUIDRef, int32 tableNumber)
{
	bool16 returnValue=kFalse;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return kFalse;
	do
	{
		/*InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
		UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);*/
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
			break;
/*		////CS3 Change
		InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if (textFrame == NULL)
			break;
*/		

		////Added By Amit

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TableUtility::isTablePresent::graphicFrameHierarchy == nil");
			break;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TableUtility::isTablePresent::multiColumnItemHierarchy == NULL");
			break;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TableUtility::isTablePresent::multiColumnItemTextFrame == NULL");
			//CA("Its Not MultiColumn");
			break;
		}
		InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy)
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TableUtility::isTablePresent::frameItemHierarchy == NULL");
			break;
		}

		InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame)
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TableUtility::isTablePresent::ITextFrameColumn == NULL");
			//CA("!!!ITextFrameColumn");
			break;
		}

		/////end
		InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
		if (textModel == NULL)
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TableUtility::isTablePresent::textModel == NULL");		
			break;
		}

		InterfacePtr<ITableModelList> tableList(textModel, UseDefaultIID());
		if(tableList==NULL)
			break;

		int32	tableIndex = tableList->GetModelCount() - 1;
		if(tableIndex<0)
			break;

		tableIndex=tableNumber-1;

		InterfacePtr<ITableModel> tableModel(tableList->QueryNthModel(tableIndex));
		if(tableModel == NULL) 
			break;
		
		UIDRef tableRef(::GetUIDRef(tableModel));
		
		tableUIDRef=tableRef;

		returnValue=kTrue;
	}
	while(kFalse);

	return returnValue;
}

/*void TableUtility::resizeTable
(const UIDRef& tableUIDRef, const int32& numRows, const int32& numCols, bool16 isTranspose)
{
	do
	{
		ErrorCode result;
		//CA("Resize");
		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
		//	CA("Err: invalid interface pointer ITableFrame");
			break;
		}

		int32 presentRows = tableModel->GetTotalCols().count;
		int32 presentCols = tableModel->GetTotalRows().count;

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
			break;

		if(isTranspose)
		{
			result = tableCommands->InsertColumns(ColRange(0,1), Tables::eBefore, PMReal(100.0));
		
			if(presentRows<numRows)
			{
				result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows), Tables::eAfter, PMReal(30.0));
			}
			else if(presentRows>numRows)
			{
				result = tableCommands->DeleteRows(RowRange(0, presentRows-numRows));
			}
			
			if(presentCols<numCols)
			{
				result = tableCommands->InsertColumns(ColRange(presentCols-1, numCols-presentCols), Tables::eAfter, PMReal(30.0));
			}
			else if(presentCols>numCols)
			{
				result = tableCommands->DeleteColumns(ColRange(0, presentCols-numCols));
			}
		}
		else
		{
			if(tableCommands==NULL)
				break;
			else
			{
				result = tableCommands->InsertRows(RowRange(1, 2), Tables::eAfter, PMReal(0.0));
			}
			
			if(presentRows<numRows)
			{
				result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows-1), Tables::eAfter, PMReal(30.0));
			}
			else if(presentRows>numRows)
			{
				result = tableCommands->DeleteRows(RowRange(0, presentRows-numRows+1));
			}			
			
			if(presentCols<numCols)
			{
				result = tableCommands->InsertColumns(ColRange(presentCols-1, numCols-presentCols), Tables::eAfter, PMReal(30.0));
			}
			else if(presentCols>numCols)
			{
				result = tableCommands->DeleteColumns(ColRange(0, presentCols-numCols));
			}
		}
	}
	while(kFalse);
}*/

void TableUtility::resizeTable
(const UIDRef& tableUIDRef, const int32& numRows, const int32& numCols, bool16 isTranspose)
{
	
	do
	{
		ErrorCode result;

		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
		{
			//CA("Err: invalid interface pointer ITableFrame");
			break;
		}

		int32 presentRows = tableModel->GetTotalRows().count;
		int32 presentCols = tableModel->GetTotalCols().count;
		
		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
		{
			break;
		}

		if(!isTranspose)
		{	
			if(presentRows<(numRows+1))
			{
				// Awasthi
				
				//result = tableCommands->InsertRows(RowRange(presentRows-1, (numRows+1)-presentRows), Tables::eAfter, PMReal(0.0));
				result = tableCommands->InsertRows(RowRange(presentRows-1, (numRows+1)-presentRows), Tables::eAfter, PMReal(0.0));

			}
			else if(presentRows>(numRows+1))
			{
				
				//result = tableCommands->DeleteRows(RowRange(0, presentRows-(numRows+1)));
				result = tableCommands->DeleteRows(RowRange((numRows+1)-1, presentRows-(numRows+1)));

			}
			
			if(presentCols<numCols)
			{
				//  Awasthi
				
				result = tableCommands->InsertColumns(ColRange(presentCols - 1, numCols-presentCols), Tables::eAfter, PMReal(0.0));
			}
			else if(presentCols>numCols)
			{
				//result = tableCommands->DeleteColumns(ColRange(0, presentCols-numCols));
				result = tableCommands->DeleteColumns(ColRange(numCols - 1, presentCols-numCols));
			}

			// Disabled by Awasthi ( Not required insertion)
			//for table header
			//if(isTranspose)
			//{
				
			//	result = tableCommands->InsertColumns(ColRange(0,1), Tables::eBefore, PMReal(30.0));
			//}
			//else
			//{
				
			//	result = tableCommands->InsertRows(RowRange(0, 1), Tables::eBefore, PMReal(30.0));
			//}
		}
		else
		{
			if(presentRows<numRows)
			{
				// Awasthi
				
				//result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows), Tables::eAfter, PMReal(0.0));
				result = tableCommands->InsertRows(RowRange(presentRows-1, numRows-presentRows), Tables::eAfter, PMReal(0.0));

			}
			else if(presentRows>numRows)
			{
				
				//result = tableCommands->DeleteRows(RowRange(0, presentRows-numRows));
				//result = tableCommands->DeleteRows(RowRange(presentRows-numRows-1, presentRows-numRows));
				result = tableCommands->DeleteRows(RowRange(numRows-1, presentRows-numRows));


			}
			
			if(presentCols<(numCols+1))
			{
				//  Awasthi
				
				result = tableCommands->InsertColumns(ColRange(presentCols-1, (numCols+1)-presentCols), Tables::eAfter, PMReal(0.0));
			}
			else if(presentCols>(numCols+1))
			{
				//result = tableCommands->DeleteColumns(ColRange(0, presentCols-(numCols+1)));
				result = tableCommands->DeleteColumns(ColRange((numCols+1)-1, presentCols-(numCols+1)));
			}

		}

	}
	while(kFalse);
	
}



void TableUtility::setTableRowColData
(const UIDRef& tableUIDRef, const PMString& data, const int32& rowIndex, const int32& colIndex)
{
	do
	{
		InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
		if (tableModel == NULL)
			break;

		InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
		if(tableCommands==NULL)
			break;
        std::string tempStr = data.GetPlatformString();

		WideString* wStr=new WideString(tempStr.c_str());

		tableCommands->SetCellText(*wStr, GridAddress(rowIndex, colIndex));

		if(wStr)
			delete wStr;
	}
	while(kFalse);
}

void TableUtility::fillDataInTable
(const UIDRef& tableUIDRef, double objectId, double tableTypeId,double& tableId ,double CurrSectionid, TagStruct tagStruct, const UIDRef& BoxUIDRef)
{
	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		//CA("Pointre to DataSprayerPtr not found");//
		return;
	}


	VectorScreenTableInfoPtr result=NULL;
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}
		PublicationNode refpNode;
		PMString PubName("PubName");
		//refpNode.setAll(PubName, tagStruct.parentId, tagStruct.sectionID,3,1,1,1,tagStruct.typeId,0,kFalse);
		if(tagStruct.whichTab == 3)
			refpNode.setAll(PubName, tagStruct.parentId, tagStruct.sectionID,3,1,1,1,tagStruct.typeId,0,kFalse,kTrue);
		//else if(tagStruct.whichTab == 4)
		//refpNode.setAll(PubName, tagStruct.parentId, tagStruct.sectionID,3,1,1,1,tagStruct.typeId,0,kFalse,kFalse);
		DataSprayerPtr->FillPnodeStruct(refpNode, tagStruct.sectionID, 1, tagStruct.sectionID);

		//InterfacePtr<ITableUtility> iTableUtlObj
		//((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		//if(!iTableUtlObj){ CA("!iTableUtlObj");
		//	return ;
		//}

		//IIDXMLElement* XMLElementPtr =  tagStruct.tagPtr; 		
		//PMString Maintag = XMLElementPtr->GetTagString();
		////CA(Maintag);
		//int MainelementCount=XMLElementPtr->GetChildCount();
		//if(MainelementCount > 0)
		//{
		//	XMLReference MainelementXMLref=XMLElementPtr->GetNthChild(0);
		//	IIDXMLElement * TableElement=MainelementXMLref.Instantiate();
		//	if(TableElement==NULL)
		//		break;
		//	//CA("22");
		//	PMString TabletagName=TableElement->GetTagString();
		//	//CA(TabletagName);			
		//	ErrorCode errCode=Utils<IXMLElementCommands>()->DeleteElement(MainelementXMLref, kFalse);
		//}
		//
		//iTableUtlObj->SetDBTableStatus(kTrue);		
		//iTableUtlObj->fillDataInTable(tableUIDRef, refpNode, tagStruct, tableId,BoxUIDRef);
		//iTableUtlObj->SetDBTableStatus(kFalse);
		
		//result = ptrIAppFramework->OBJTABLECntrller_getAllScreenTables(objectId);
		PublicationNode pNode;
		if(pNode.getIsONEsource())
		{
			// For ONEsource mode
			//result =ptrIAppFramework->GetProduct_getObjectTableByObjectId(objectId);
		}else
		{
			//For publication
			result = ptrIAppFramework->GETProjectProduct_getAllScreenTablesBySectionidObjectid(CurrSectionid, objectId, tagStruct.languageID, kTrue);			
		}
		if(!result)
		{
			ptrIAppFramework->LogError("AP46_UpdateRepository::TableUtility::fillDataInTable::ONEsource/else !result");		
			return;
		}

		bool16 IsAutoResize = kFalse;
		if(tagStruct.isAutoResize)
			IsAutoResize= kTrue;
		
		bool16 AddHeaderToTable = kTrue;
		int32/*int64*/ numRows=0;
		int32/*int64*/ numCols=0;

		CItemTableValue oTableValue;
		VectorScreenTableInfoValue::iterator it;		

		bool16 typeidFound=kFalse;
		for(it = result->begin(); it!=result->end(); it++)
		{
			oTableValue = *it;
			if(oTableValue.getTableTypeID() == tableTypeId)
			{
				typeidFound=kTrue;
				break;
			}
		}
		if(!typeidFound)
			break;

		numRows =static_cast<int32> (oTableValue.getTableData().size());
		if(numRows<=0)
			break;
		numCols =static_cast<int32>(oTableValue.getTableHeader().size());
		if(numCols<=0)
			break;
		
		bool8 isTranspose = oTableValue.getTranspose();
		
//isTranspose = kTrue;

		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj){
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TableUtility::fillDataInTable::!iTableUtlObj");
			return ;
		}

		if(isTranspose)
		{
			//CA("is transpose");
			int32 i=1, j=0;
			//Awasthi
			resizeTable(tableUIDRef, numCols, numRows, isTranspose);
			//End		
			vector<double> vec_tableheaders;
			vector<double> vec_items;
			vec_items = oTableValue.getItemIds();
			vec_tableheaders = oTableValue.getTableHeader();

			putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose,tagStruct, BoxUIDRef );

			vector<vector<PMString> > vec_tablerows;

			vec_tablerows = oTableValue.getTableData();
			//PMString temp1("Size in sprayer: ");
			//temp1.AppendNumber(vec_tablerows.size());
			//CA(temp1);

			vector<vector<PMString> >::iterator it1;

			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				//CA("row iterator");
				vector<PMString>::iterator it2;
				//PMString temp2("Size of cols in sprayer: ");
				//temp2.AppendNumber((*it1).size());
				//CA(temp2);

				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{
					//CA("col iterator");
					if(j==0)
					{
						j++;
						continue;
					}
					if((j-1)>=numCols)
						break;

					setTableRowColData(tableUIDRef, (*it2), j-1, i);
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[j-1];
					//temp_to_test CAttributeValue oAttribVal;
					//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
					PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j-1],tagStruct.languageID );// changes is required language ID req.
					stencileInfo.elementName = dispname;
					PMString TableColName("");//temp_to_test  = oAttribVal.getTable_col();
					//if(TableColName == "")   /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
					//{
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(vec_tableheaders[j-1]));
					//}
	//CA(TableColName);
					stencileInfo.TagName = TableColName;
					if(AddHeaderToTable)
						stencileInfo.childId  = vec_items[i-1];//stencileInfo.typeId  = vec_items[i-1];
					else
						stencileInfo.childId  = vec_items[i];//stencileInfo.typeId  = vec_items[i];

					stencileInfo.parentId = tagStruct.parentId;
					stencileInfo.parentTypeId = tagStruct.parentTypeID;
					stencileInfo.whichTab = 4; // Item Attributes
					stencileInfo.imgFlag = tagStruct.sectionID; 
					stencileInfo.isAutoResize = tagStruct.isAutoResize;
					stencileInfo.LanguageID = tagStruct.languageID ;
					stencileInfo.pbObjectId = tagStruct.pbObjectId;
					stencileInfo.tableType = tagStruct.tableType;
					stencileInfo.tableId = tagStruct.tableId;
					stencileInfo.childTag = 1;

					XMLReference txmlref;
	//CA(tStruct.tagPtr->GetTagString());	


					iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, j-1, i,tableId );
					j++;
				}
				i++;
				j=0;
				//break;
			}
		}
		else
		{
			//CA("is not transpose");
			int32 i=1, j=0;
			// Awasthi
			resizeTable(tableUIDRef, numRows, numCols, isTranspose);
			// End
			vector<double> vec_tableheaders;
			vector<double> vec_items;
			vec_items = oTableValue.getItemIds();

			vec_tableheaders = oTableValue.getTableHeader();
			putHeaderDataInTable(tableUIDRef, vec_tableheaders, isTranspose,tagStruct, BoxUIDRef);

			vector<vector<PMString> > vec_tablerows;

			vec_tablerows = oTableValue.getTableData();
			//PMString temp1("Size in sprayer: ");
			//temp1.AppendNumber(vec_tablerows.size());
			//CA(temp1);

			vector<vector<PMString> >::iterator it1;

			for(it1=vec_tablerows.begin(); it1!=vec_tablerows.end(); it1++)
			{
				//CA("row iterator");
				vector<PMString>::iterator it2;
				//PMString temp2("Size of cols in sprayer: ");
				//temp2.AppendNumber((*it1).size());
				//CA(temp2);

				for(it2=(*it1).begin();it2!=(*it1).end();it2++)
				{
					//CA("col iterator");
					if(j==0)
					{
						j++;
						continue;
					}
					if((j-1)>=numCols)
						break;

					setTableRowColData(tableUIDRef, (*it2), i, j-1);
					SlugStruct stencileInfo;
					stencileInfo.elementId = vec_tableheaders[j-1];
					//temp_to_test CAttributeValue oAttribVal;
					//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[j-1]);
					PMString dispname= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[j-1],tagStruct.languageID );//temp_to_test  = oAttribVal.getDisplay_name();
					stencileInfo.elementName = dispname;
					PMString TableColName("");//temp_to_test  = oAttribVal.getTable_col();
					//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
					//{
						TableColName.Append("ATTRID_");
						TableColName.AppendNumber(PMReal(vec_tableheaders[j-1]));
					//}
					stencileInfo.TagName = TableColName;
					if(AddHeaderToTable)
						stencileInfo.childId  = vec_items[i-1];//stencileInfo.typeId  = vec_items[i-1];
					else
						stencileInfo.childId  = vec_items[i];//stencileInfo.typeId  = vec_items[i];
					stencileInfo.parentId = tagStruct.parentId;
					stencileInfo.parentTypeId = tagStruct.parentTypeID;
					stencileInfo.whichTab = 4; // Item Attributes
					stencileInfo.imgFlag = tagStruct.sectionID; 
					stencileInfo.isAutoResize = tagStruct.isAutoResize;
					stencileInfo.LanguageID = tagStruct.languageID ;
					stencileInfo.pbObjectId = tagStruct.pbObjectId;
					stencileInfo.tableType = tagStruct.tableType;
					stencileInfo.tableId = tagStruct.tableId;
					stencileInfo.childTag = 1;

					XMLReference txmlref;
//	CA("Before Tag Table");
					iTableUtlObj->TagTable(tableUIDRef,BoxUIDRef,tagStruct.tagPtr->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, i, j-1, tableId );
//	CA("After Tag Table");
					j++;
				}
				i++;
				j=0;
				//break;
			}
			
		}
		
	}
	while(kFalse);
	if(result)
		delete result;
}

void TableUtility::putHeaderDataInTable
(const UIDRef& tableUIDRef, vector<double> vec_tableheaders, bool16 isTranspose, TagStruct& tStruct, UIDRef boxUIDRef)
{
//temp_to_test 	CAttributeValue oAttribVal;

	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is NULL.");
			break;
		}

		InterfacePtr<ITableUtility> iTableUtlObj
		((static_cast<ITableUtility*> (CreateObject(kTableUtilityBoss,IID_ITABLEUTILITY))));
		if(!iTableUtlObj){ 
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TableUtility::putHeaderDataInTable::!iTableUtlObj");
			return ;
		}


		for(int i=0;i<vec_tableheaders.size();i++)
		{
			//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
			PMString dispname= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID );;//temp_to_test  = oAttribVal.getDisplay_name();
			//CA(dispname);
			InterfacePtr<ITableModel> tableModel(tableUIDRef, UseDefaultIID());
			if (tableModel == NULL)
			{
				ptrIAppFramework->LogDebug("AP46_UpdateRepository::TableUtility::putHeaderDataInTable::!tableModel");
				break;
			}

			InterfacePtr<ITableCommands> tableCommands(tableModel, UseDefaultIID());
			if(tableCommands==NULL)
			{
				ptrIAppFramework->LogDebug("AP46_UpdateRepository::TableUtility::putHeaderDataInTable::!tableCommands");
				break;
			}
			//CA(data.GrabCString());
			WideString* wStr=new WideString(dispname);
			if(isTranspose)
			{
				tableCommands->SetCellText(*wStr, GridAddress(i, 0));
				SlugStruct stencileInfo;
				stencileInfo.elementId = vec_tableheaders[i];
				//temp_to_test CAttributeValue oAttribVal;
				//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
				PMString dispname;//temp_to_test  = oAttribVal.getDisplay_name();
				//CA("Inside PutHeaderDataIN Table");	CA(dispname);
				stencileInfo.elementName = dispname;
				PMString TableColName("");//temp_to_test  = oAttribVal.getTable_col();
				//if(TableColName == "")  /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
				//{
					TableColName.Append("ATTRID_");
					TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
				//}
				//	CA(TableColName);
				stencileInfo.TagName = TableColName;
				stencileInfo.typeId  = -1;//-2;  // TypeId is hardcodded to -2 for Header Elements.
				stencileInfo.header  = 1;
				stencileInfo.parentId = tStruct.parentId;  // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.imgFlag = tStruct.sectionID; 
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.LanguageID = tStruct.languageID ;
				stencileInfo.pbObjectId = tStruct.pbObjectId;
				stencileInfo.tableType = tStruct.tableType;
				stencileInfo.tableId = tStruct.tableId;
				XMLReference txmlref;				
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, i, 0, 0 );

			}
			else
			{
				tableCommands->SetCellText(*wStr, GridAddress(0, i));
				SlugStruct stencileInfo;
				stencileInfo.elementId =vec_tableheaders[i];
				//temp_to_test CAttributeValue oAttribVal;
				//temp_to_test oAttribVal = ptrIAppFramework->ATTRIBMngr_getAttribute(vec_tableheaders[i]);
				PMString dispname= ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(vec_tableheaders[i],tStruct.languageID ); //temp_to_test  = oAttribVal.getDisplay_name();
				stencileInfo.elementName = dispname;
				//CA("Inside PutHeaderDataIN Table");	CA(dispname);
				PMString TableColName("");//temp_to_test  = oAttribVal.getTable_col();
				//if(TableColName == "") /// Adjustment For Parametric Attributes as Table_Col Field in DB is NULL 
				//{
					TableColName.Append("ATTRID_");
					TableColName.AppendNumber(PMReal(vec_tableheaders[i]));
				//}
				stencileInfo.TagName = TableColName;
		//CA(TableColName);
				stencileInfo.typeId  = -1; //vec_items[i];
				stencileInfo.header  = 1;
				stencileInfo.parentId = tStruct.parentId;   // -2 for Header Element
				stencileInfo.parentTypeId = tStruct.parentTypeID;
				stencileInfo.whichTab = 4; // Item Attributes
				stencileInfo.imgFlag = tStruct.sectionID; 
				stencileInfo.isAutoResize = tStruct.isAutoResize;
				stencileInfo.LanguageID = tStruct.languageID ;
				stencileInfo.pbObjectId = tStruct.pbObjectId;
				stencileInfo.tableType = tStruct.tableType;
				stencileInfo.tableId = tStruct.tableId;
				XMLReference txmlref;

				//CA(tStruct.tagPtr->GetTagString());
				//CA(stencileInfo.TagName);
				iTableUtlObj->TagTable(tableUIDRef,boxUIDRef,tStruct.tagPtr->GetTagString(), stencileInfo.TagName, txmlref, stencileInfo, 0, i, 0 );


			}
			if(wStr)
				delete wStr;
		}
	}
	while(kFalse);
}

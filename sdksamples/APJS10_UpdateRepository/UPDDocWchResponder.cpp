//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/docwatch/UPDDocWchResponder.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IClassIDData.h"
#include "ISignalMgr.h"
#include "IDocumentSignalData.h"
#include "IUIDData.h"

// Implementation includes:
#include "CreateObject.h"
#include "CResponder.h"
#include "UPDID.h"
#include "CAlert.h"
#include "DocWchUtils.h"
#include "UPDSelectionObserver.h"
#include "ILayoutUtils.h"
#include "UPDActionComponent.h"

//#include "ILayoutUIUtils.h"
#include "ILayoutUIUtils.h"

#define CA(X) CAlert::InformationAlert(X)
extern bool16 FlgToCheckMenuAction;

/** RfhDocWchResponder
	Handles signals related to document file actions.  The file action 
	signals it receives are dictated by the DocWchServiceProvider class.

	RfhDocWchResponder implements IResponder based on
	the partial implementation CResponder.


	@ingroup docwatch
	@author John Hake
*/
class UPDDocWchResponder : public CResponder
{
	public:
	
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		UPDDocWchResponder(IPMUnknown* boss);

		/**
			Respond() handles the file action signals when they
			are dispatched by the signal manager.  This implementation
			simply creates alerts to display each signal.

			@param signalMgr Pointer back to the signal manager to get
			additional information about the signal.
		*/
		virtual void Respond(ISignalMgr* signalMgr);

};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(UPDDocWchResponder, kUPDDocWchResponderImpl)

/* DocWchActionComponent Constructor
*/
UPDDocWchResponder::UPDDocWchResponder(IPMUnknown* boss) :
	CResponder(boss)
{
}

/* Respond
*/
void UPDDocWchResponder::Respond(ISignalMgr* signalMgr)
{
	// Exit if the responder should do nothing
	if (DocWchUtils::QueryDocResponderMode() != kTrue)
		return;

	// Get the service ID from the signal manager
	ServiceID serviceTrigger = signalMgr->GetServiceID();

	/*PMString ASD(" ServiceID : ");
	ASD.AppendNumber(serviceTrigger.Get());
	CA(ASD);*/
	// Get a UIDRef for the document.  It will be an invalid UIDRef
	// for BeforeNewDoc, BeforeOpenDoc, AfterSaveACopy, and AfterCloseDoc because
	// the document doesn't exist at that point.
	/*InterfacePtr<IDocumentSignalData> docData(signalMgr, UseDefaultIID());
	if (docData == NULL)
	{
		CA("Invalid IDocumentSignalData* - UPDDocWchResponder::Respond");
		return;
	}
	UIDRef doc = docData->GetDocument();*/
	UPDSelectionObserver SELObj(this);


	// Take action based on the service ID
	switch (serviceTrigger.Get())
	{
	//	case kBeforeNewDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kBeforeNewDocSignalStringKey);
	//		break;
	//	}
	//	case kDuringNewDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kDuringNewDocSignalStringKey);
	//		break;
	//	}
		//case kAfterNewDocSignalResponderService:
		//{	//CA("New doc created");
		//	FlgToCheckMenuAction= kTrue;
		//	SELObj.loadPaletteData();
		//	
		//	//CA("New doc created 1");
		//	IDocument *iDoc=::GetFrontDocument();
		//	if(iDoc== NULL)
		//	{ 
		//		//CA("New doc created  iDoc== NULL");
		//		SELObj.EnableAll();
		//	}
		////	CA("after New doc created");

		//	//DocWchUtils::DwAlert(doc, kAfterNewDocSignalStringKey);
		//	break;
		//}
	//	case kBeforeOpenDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kBeforeOpenDocSignalStringKey);
	//		break;
	//	}
	//	case kDuringOpenDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kDuringOpenDocSignalStringKey);
	//		break;
	//	}
		//case kAfterOpenDocSignalResponderService:
		//{	CA("After document open");
		//	FlgToCheckMenuAction= kTrue;
		//	SELObj.loadPaletteData();

		//	IDocument *iDoc=::GetFrontDocument();
		//	if(iDoc== NULL)
		//	{ 
		//		//CA("After document open iDoc== NULL");
		//		SELObj.EnableAll();

		//	}
		//	//DocWchUtils::DwAlert(doc, kAfterOpenDocSignalStringKey);
		//	break;
		//}
	//	case kBeforeSaveDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kBeforeSaveDocSignalStringKey);
	//		break;
	//	}
	//	case kAfterSaveDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kAfterSaveDocSignalStringKey);
	//		break;
	//	}
	//	case kBeforeSaveAsDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kBeforeSaveAsDocSignalStringKey);
	//		break;
	//	}
	//	case kAfterSaveAsDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kAfterSaveAsDocSignalStringKey);
	//		break;
	//	}
	//	case kBeforeSaveACopyDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kBeforeSaveACopyDocSignalStringKey);
	//		break;
	//	}
	//	case kDuringSaveACopyDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kDuringSaveACopyDocSignalStringKey);
	//		break;
	//	}
	//	case kAfterSaveACopyDocSignalResponderService:
	//	{
	//		DocWchUtils::DwAlert(doc, kAfterSaveACopyDocSignalStringKey);
	//		break;
	//	}
	//	case kBeforeRevertDocSignalResponderService:
	//	{	CA("Before Revert Doc signal");
	//		//DocWchUtils::DwAlert(doc, kBeforeRevertDocSignalStringKey);
	//		break;
	//	}
		//case kAfterRevertDocSignalResponderService:
		//{	//CA("After revert doc signal");
		//	FlgToCheckMenuAction= kTrue;
		//	SELObj.loadPaletteData();
		//	
		//	//DocWchUtils::DwAlert(doc, kAfterRevertDocSignalStringKey);
		//	break;
		//}
		 case kBeforeCloseDocSignalResponderService:
		{	//CA("Before close Doc ");
			
			FlgToCheckMenuAction= kTrue;
			SELObj.loadPaletteData();	
			////
			////IDocument *iDoc=::GetFrontDocument();
			////if(iDoc== NULL)
			////{ 
			////	//CA("Before close Doc signal iDoc== NULL");
			////	SELObj.DisableAll();
			////}
			//////DocWchUtils:wAlert(doc, kBeforeCloseDocSignalStringKey);
			/*UPDActionComponent actionObj(this);
			actionObj.CloseUpdateRepositoryPalette();*/
			break;
		}
	//	case kAfterCloseDocSignalResponderService:
	//	{	FlgToCheckMenuAction= kTrue;
	//		SELObj.loadPaletteData();
	//		//DocWchUtils::DwAlert(doc, kAfterCloseDocSignalStringKey);
	//		break;
	//	}

		case kOpenLayoutSignalServiceID:
		{	
			//CA("kOpenLayoutSignalServiceID ... ");			
			FlgToCheckMenuAction= kTrue;
			SELObj.loadPaletteData();	
			//
			//IDocument *iDoc=::GetFrontDocument();
			//if(iDoc!= NULL)
			//{ 
			//	//CA("New doc created  iDoc== NULL");
			//	SELObj.EnableAll();
			//}

		

			
			break;
		}
		
		default:
			break;
	}
}

// End, RfhDocWchResponder.cpp.




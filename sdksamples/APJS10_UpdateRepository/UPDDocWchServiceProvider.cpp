//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/docwatch/RfhDocWchServiceProvider.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// Implementation includes:
#include "CServiceProvider.h"
#include "K2Vector.h"
#include "DocumentID.h"
#include "DocWchUtils.h"
#include "CAlert.h"
#include "UPDID.h"
#include "LayoutUIID.h"

#define CA(X) CAlert::InformationAlert(X)

/** RfhDocWchServiceProvider
	registers as providing the service of responding to a group of document
	file action signals.  See the constructor code for a list of the
	signals this service responds to.

	RfhDocWchServiceProvider implements IK2ServiceProvider based on
	the partial implementation CServiceProvider.


	@ingroup docwatch
	@author John Hake
*/
class UPDDocWchServiceProvider : public CServiceProvider
{
	public:

		/**
			Constructor initializes a list of service IDs, one for each file action 
			signal that DocWchResponder will handle.
			@param boss interface ptr from boss object on which interface is aggregated.
		*/
		UPDDocWchServiceProvider(IPMUnknown* boss);
		
		/**
			Destructor.  
		*/
		virtual	~UPDDocWchServiceProvider();

		/**
			GetName initializes the name of the service.
			@param pName Ptr to PMString to receive the name.
		*/
		virtual void GetName(PMString* pName);

		/**
			GetServiceID returns a single service ID.  This is required, even though
			GetServiceIDs() will return the complete list initialized in the constructor.
			This method just returns the first service ID in the list.
		*/
		virtual ServiceID GetServiceID();

		/**
			IsDefaultServiceProvider tells application this service is not the default service.
		*/
		virtual bool16 IsDefaultServiceProvider();
		
		/**
			GetInstantiationPolicy returns a InstancePerX value to indicate that only
			one instance per session is needed.
		*/
		virtual InstancePerX GetInstantiationPolicy();

		/**
			HasMultipleIDs returns kTrue in order to force a call to GetServiceIDs().
		*/
		virtual bool16 HasMultipleIDs() const;

		/**
			GetServiceIDs returns a list of services provided.
			@param serviceIDs List of IDs describing the services that 
			RfhDocWchServiceProvider registers to handle.
		*/
		virtual void GetServiceIDs(K2Vector<ServiceID>& serviceIDs);
	
	private:

		K2Vector<ServiceID> fSupportedServiceIDs;
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(UPDDocWchServiceProvider, kUPDDocWchServiceProviderImpl)


/* DocWchActionComponent Constructor
*/
UPDDocWchServiceProvider::UPDDocWchServiceProvider(IPMUnknown* boss)
	: CServiceProvider(boss)
{	
	// Add the serviceIDs we want the associated responder to handle.
	// (See DocumentID.h)
	fSupportedServiceIDs.clear();
	
	//	NewDoc
	fSupportedServiceIDs.push_back(kBeforeNewDocSignalResponderService);// Append(kBeforeNewDocSignalResponderService);
	fSupportedServiceIDs.push_back(kDuringNewDocSignalResponderService);//Append(kDuringNewDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterNewDocSignalResponderService);//Append(kAfterNewDocSignalResponderService);

	//	OpenDoc
	fSupportedServiceIDs.push_back(kBeforeOpenDocSignalResponderService);//Append(kBeforeOpenDocSignalResponderService);
	fSupportedServiceIDs.push_back(kDuringOpenDocSignalResponderService);//Append(kDuringOpenDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterOpenDocSignalResponderService);//Append(kAfterOpenDocSignalResponderService);

	//	SaveDoc
	fSupportedServiceIDs.push_back(kBeforeSaveDocSignalResponderService);//Append(kBeforeSaveDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterSaveDocSignalResponderService);//Append(kAfterSaveDocSignalResponderService);

	//	SaveAsDoc
	fSupportedServiceIDs.push_back(kBeforeSaveAsDocSignalResponderService);//Append(kBeforeSaveAsDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterSaveAsDocSignalResponderService);//Append(kAfterSaveAsDocSignalResponderService);

	//	SaveACopyDoc
	fSupportedServiceIDs.push_back(kBeforeSaveACopyDocSignalResponderService);//Append(kBeforeSaveACopyDocSignalResponderService);
	fSupportedServiceIDs.push_back(kDuringSaveACopyDocSignalResponderService);//Append(kDuringSaveACopyDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterSaveACopyDocSignalResponderService);//Append(kAfterSaveACopyDocSignalResponderService);

	//	RevertDoc
	fSupportedServiceIDs.push_back(kBeforeRevertDocSignalResponderService);//Append(kBeforeRevertDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterRevertDocSignalResponderService);//Append(kAfterRevertDocSignalResponderService);

	//	CloseDoc
	fSupportedServiceIDs.push_back(kBeforeCloseDocSignalResponderService);//Append(kBeforeCloseDocSignalResponderService);
	fSupportedServiceIDs.push_back(kAfterCloseDocSignalResponderService);//Append(kAfterCloseDocSignalResponderService);

	fSupportedServiceIDs.push_back(kOpenLayoutSignalServiceID);//Append(kOpenLayoutSignalServiceID);

	if (fSupportedServiceIDs.size()<=0)
	{	
		ASSERT_FAIL("RfhRfhDocWchServiceProvider must support at least 1 service ID");
		fSupportedServiceIDs.push_back(kInvalidService);//Append(kInvalidService);
	}

}

/* DocWchActionComponent Dtor
*/
UPDDocWchServiceProvider::~UPDDocWchServiceProvider()
{
}

/* GetName
*/
void UPDDocWchServiceProvider::GetName(PMString* pName)
{
	pName->SetCString("DocWatch Responder Service");
}

/* GetServiceID
*/
ServiceID UPDDocWchServiceProvider::GetServiceID() 
{
	// Should never be called given that HasMultipleIDs() returns kTrue.
	return fSupportedServiceIDs[0];
}

/* IsDefaultServiceProvider
*/
bool16 UPDDocWchServiceProvider::IsDefaultServiceProvider()
{
	return kFalse;
}

/* GetInstantiationPolicy
*/
IK2ServiceProvider::InstancePerX UPDDocWchServiceProvider::GetInstantiationPolicy()
{
	return IK2ServiceProvider::kInstancePerSession;
}

/* HasMultipleIDs
*/
bool16 UPDDocWchServiceProvider::HasMultipleIDs() const
{
	return kTrue;
}

/* GetServiceIDs
*/
void UPDDocWchServiceProvider::GetServiceIDs(K2Vector<ServiceID>& serviceIDs)
{
	// Append a service IDs for each service provided. 
	for (int32 i = 0; i<fSupportedServiceIDs.size(); i++)
		serviceIDs.push_back(fSupportedServiceIDs[i]);//Append(fSupportedServiceIDs[i]);

}


// End, RfhDocWchServiceProvider.cpp.




//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __UPDID_h__
#define __UPDID_h__

#include "SDKDef.h"

// Company:
#define kUPDCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kUPDCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kUPDPluginName	"Update Apsiva"			// Name of this plug-in.
#define kUPDPrefixNumber	0xABA150B 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kUPDVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kUPDAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kUPDPrefixNumber above to modify the prefix.)
#define kUPDPrefix		RezLong(kUPDPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kUPDStringPrefix	SDK_DEF_STRINGIZE(kUPDPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kUPDMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kUPDMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kUPDPluginID, kUPDPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kUPDActionComponentBoss, kUPDPrefix + 0)
DECLARE_PMID(kClassIDSpace, kUPDPanelWidgetBoss, kUPDPrefix + 1)
///((((()))))))///
DECLARE_PMID(kClassIDSpace, kUPDPanelListBoxWidgetBoss,	kUPDPrefix + 3)
DECLARE_PMID(kClassIDSpace, kUPDDocWchResponderServiceBoss,	kUPDPrefix + 4)
DECLARE_PMID(kClassIDSpace, kUPDLoginEventsHandler,		kUPDPrefix + 5)
DECLARE_PMID(kClassIDSpace, kUPDPanelActionFilterBoss,	kUPDPrefix + 6)
DECLARE_PMID(kClassIDSpace, kUPDTreeViewWidgetBoss, kUPDPrefix + 7)
DECLARE_PMID(kClassIDSpace, kUPDTreeNodeWidgetBoss, kUPDPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kUPDBoss, kUPDPrefix + 25)


// InterfaceIDs:
///(((((()))))))////
DECLARE_PMID(kInterfaceIDSpace, IID_IUPDSUITE,						kUPDPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IUPDContentSELECTIONOBSERVER,	kUPDPrefix + 10)
DECLARE_PMID(kInterfaceIDSpace, IID_IUPDWIDGETOBSERVER,				kUPDPrefix + 12)

///(((((())))))))////

DECLARE_PMID(kInterfaceIDSpace, IID_LISTCONTROLDATA, kUPDPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IUPDINTERFACE, kUPDPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kUPDActionComponentImpl , kUPDPrefix + 0 )
////(((((())))))////
DECLARE_PMID(kImplementationIDSpace, kUPDContentSelectionObserverImpl,	kUPDPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kUPDPanelListBoxObserverImpl,		kUPDPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kUPDSuiteTextCSBImpl,				kUPDPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kUPDSuiteASBImpl,					kUPDPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kUPDWidgetObserverImpl,			kUPDPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kUPDDocWchServiceProviderImpl,		kUPDPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kUPDDocWchResponderImpl,			kUPDPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kUPDLoginEventsHandlerImpl,		kUPDPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kUPDActionFilterImpl,				kUPDPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kUPDUpdateControlViewImpl ,		kUPDPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kUPDTreeViewHierarchyAdapterImpl,	kUPDPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kUPDTreeViewWidgetMgrImpl,			kUPDPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kUPDTreeObserverImpl,				kUPDPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kUPDImpl, kUPDPrefix + 25)


// ActionIDs:
//DECLARE_PMID(kActionIDSpace, kUPDAboutActionID,				kUPDPrefix + 0)
DECLARE_PMID(kActionIDSpace, kUPDContentPanelWidgetActionID,kUPDPrefix + 0)
DECLARE_PMID(kActionIDSpace, kUPDPanelPSMenuActionID,		kUPDPrefix + 1)
//DECLARE_PMID(kActionIDSpace, kUPDPopupAboutThisActionID,	kUPDPrefix + 3)
DECLARE_PMID(kActionIDSpace, kUPDMenuItem4ActionID,		kUPDPrefix + 4)

DECLARE_PMID(kActionIDSpace, kUPDMenuItem1ActionID,			kUPDPrefix + (10 + 1))
////((((()))))))///
DECLARE_PMID(kActionIDSpace, kUPDMenuItem2ActionID,			kUPDPrefix + (10 + 2))
DECLARE_PMID(kActionIDSpace, kUPDMenuItem3ActionID,			kUPDPrefix + (10 + 3))
////((((()))))))///
DECLARE_PMID(kActionIDSpace, kUPDPanelWidgetActionID, kUPDPrefix + (10 + 4))


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kUPDPanelWidgetID, kUPDPrefix + 0)
/////((((()))))))////
DECLARE_PMID(kWidgetIDSpace, kUPDOptionsDropDownWidgetID,	kUPDPrefix + 1)
//DECLARE_PMID(kWidgetIDSpace, kUPDPageNumWidgetID,			kUPDPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kUPDPanelLstboxWidgetID,		kUPDPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kUPDRefreshButtonWidgetID,		kUPDPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kUPDPanelListParentWidgetId,	kUPDPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kUPDUnCheckIconWidgetID,		kUPDPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kUPDCheckIconWidgetID,			kUPDPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kUPDPanelTextWidgetID,			kUPDPrefix + 8)


DECLARE_PMID(kWidgetIDSpace, kUPDLocateButtonWidgetID,			kUPDPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kUPDFailIconWidgetID,				kUPDPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kUPDSucessIconWidgetID,			kUPDPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kUPDDeactiveIconWidgetID,			kUPDPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kUPDReloadIconWidgetID,			kUPDPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kUPDSelectionBoxtWidgetID,			kUPDPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kUPDPanelTreeViewWidgetID,			kUPDPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kUPDTreePanelNodeWidgetID,			kUPDPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kUPDTreeNodeExpanderWidgetID,		kUPDPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, kUPDCheckBoxWidgetID,				kUPDPrefix + 18)


/////((((()))))))////


// "About Plug-ins" sub-menu:
#define kUPDAboutMenuKey			kUPDStringPrefix "kUPDAboutMenuKey"
#define kUPDAboutMenuPath		kSDKDefStandardAboutMenuPath kUPDCompanyKey

// "Plug-ins" sub-menu:
#define kUPDPluginsMenuKey 		kUPDStringPrefix "kUPDPluginsMenuKey"
#define kUPDPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kUPDCompanyKey kSDKDefDelimitMenuPath kUPDPluginsMenuKey

// Menu item keys:
#define kUPDMenuItem1MenuItemKey		"kUPDMenuItem1MenuItemKey"
//////(((()))))////
#define kUPDMenuItem2MenuItemKey		kUPDStringPrefix "kUPDMenuItem2MenuItemKey"
#define kUPDMenuItem3MenuItemKey		kUPDStringPrefix "kUPDMenuItem3MenuItemKey"
#define kUPDUpateApsivaStringKey		kUPDStringPrefix "kUPDUpateApsivaStringKey"	
#define kUPDUniqueAttributesStringKey	kUPDStringPrefix "kUPDUniqueAttributesStringKey"
#define kUPDSelectStringKey kUPDStringPrefix	 "kUPDSelectStringKey"
#define kUPDSelect2StringKey kUPDStringPrefix "kUPDSelect2StringKey"
#define kUPDSelectedFramesStringKey kUPDStringPrefix "kUPDSelectedFramesStringKey"
//////(((()))))////

// Other StringKeys:
#define kUPDAboutBoxStringKey	kUPDStringPrefix "kUPDAboutBoxStringKey"
#define kUPDMenuItem1StringKey				kUPDStringPrefix "kUPDMenuItem1StringKey"
#define kUPDPanelTitleKey					kUPDStringPrefix	"kUPDPanelTitleKey"
#define kUPDStaticTextKey kUPDStringPrefix	"kUPDStaticTextKey"
#define kUPDInternalPopupMenuNameKey kUPDStringPrefix	"kUPDInternalPopupMenuNameKey"
#define kUPDTargetMenuPath kUPDInternalPopupMenuNameKey

// Menu item positions:
#define kUPDContentPanelMenuItemPosition			 7
#define kUPDMenuItem1MenuItemPosition				 1
#define	kUPDSeparator1MenuItemPosition		10.0
#define kUPDAboutThisMenuItemPosition		11.0

#define kUPDMenuItem2MenuItemPosition				2
#define kUPDMenuItem3MenuItemPosition			    3

#define kUPDPanelLstBoxRsrcID				10900
#define kUPDRefreshIconID					11000
#define kUPDUnChkIconID						11001
#define kUPDChkIconID						11002

//#define kUPDlocatIconID			20002

#define kUPDFailIconID			20003
#define kUPDSucessIconID		20004
#define kUPDDeactiveIconID		20005	
#define kUPDReloadIconID		20006	
//#define kUPDlocatIconID						11003

#define kUPDTreePanelNodeRsrcID 20007

// Initial data format version numbers
#define kUPDFirstMajorFormatNumber  RezLong(1)
#define kUPDFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kUPDCurrentMajorFormatNumber kUPDFirstMajorFormatNumber
#define kUPDCurrentMinorFormatNumber kUPDFirstMinorFormatNumber

//PNGR ID
#define kPNGRUPDUpdateIconRsrcID                           1000
#define kPNGRUPDUpdateIconRollRsrcID                       1000

#define kPNGRUPDReloadIconRsrcID                           1001
#define kPNGRUPDReloadIconRollRsrcID                       1001

#endif // __UPDID_h__

//  Code generated by DollyXs code generator

#ifndef _IUPDSuite_
#define _IUPDSuite_

#include "K2Vector.h"

class IPMUnknown;
class PMRect;
//class K2Vector;

class ITextFocus;

class IUPDSuite : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IUPDSUITE };

	typedef PMRect TextInset;

	typedef K2Vector<TextInset> TextInsets;

public:
	virtual bool16 CanGetTextInsets(void) = 0;

	virtual void GetTextInsets(TextInsets& textInsets) = 0;

	virtual bool16 CanApplyTextInset(void) = 0;

	virtual ErrorCode ApplyTextInset(const PMReal& insetValue) = 0;
	virtual ErrorCode GetTextFocus(ITextFocus * & Ifocus)=0;

};	

#endif // _IUPDSuite_





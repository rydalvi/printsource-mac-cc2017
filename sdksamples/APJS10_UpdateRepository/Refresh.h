#ifndef __REFRESH_H__
#define __REFRESH_H__

#include "VCPluginHeaders.h"
#include "PageData.h"
#include "IIDXMLElement.h"
#include "UIDList.h"
#include "vector"
#include "ITextModel.h"

#include "TagStruct.h"
using namespace std;

class Refresh
{
public:
	bool16 refreshThisBox(UIDRef&, PMString&);
	bool16 GetPageDataInfo(int);
	void doRefresh(int);
	bool16 isValidPageNumber(int32);
	bool16 isValidPageNumber(int32, UID&);
	enum textAction{AtEnd, AtBegin, OverWrite};
	bool16 GetTextstoryFromBox(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story);
	void ChangeColorOfText(InterfacePtr<ITextModel> textModel1 ,int32 StartText1, int32 EndText1, PMString  altColorSwatch);
	bool16 getDocumentSelectedBoxIds();

private:
	void fitImageInBox(const UIDRef& boxUIDRef);
	int32 getAllItemIDs(double, double, vector<double>&);
	int32 parseTheText(PMString&, vector< vector<PMString> >&);
	bool16 setTextInBox(UIDRef& boxID, PMString& textToInsert, enum textAction);
	bool16 refreshTaggedBox(UIDRef&, PMString&);
	bool16 getDataFromDB(PMString&, TagStruct&, double);
	void appendToGlobalList(const PageData&);
	bool16 getAllBoxIds(void);
	void showTagInfo(UIDRef);
	void fillDataInBox(const UIDRef&, TagStruct&, double);
	void fillImageInBox(const UIDRef&, TagStruct&, double, PMString);
	bool16 ImportFileInFrame(const UIDRef&, const PMString&);
	bool16 shouldRefresh(const TagStruct&, const UID&, bool16 isTaggedFrame=kFalse);
	bool16 getAllPageItemsFromPage(int32);
	bool16 doesExist(IIDXMLElement*);
	bool16 doesExist(IIDXMLElement * ptr, UIDRef BoxRef);

	UIDList selectUIDList;

};

#endif
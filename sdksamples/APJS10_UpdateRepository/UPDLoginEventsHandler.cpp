#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "UPDID.h"
#include "ILoginEvent.h"
#include "CAlert.h"
#include "UPDLoginEventsHandler.h"
#include "UPDSelectionObserver.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IWindow.h"
#include "IApplication.h"
#include "UPDActionComponent.h"
#include "MediatorClass.h"
#include "DocWchUtils.h"

#define CA(z) CAlert::InformationAlert(z)
extern bool16 FlgToCheckMenuAction;

CREATE_PMINTERFACE(UPDLoginEventsHandler,kUPDLoginEventsHandlerImpl)

UPDLoginEventsHandler::UPDLoginEventsHandler
(IPMUnknown* boss):CPMUnknown<ILoginEvent>(boss)
{
}

UPDLoginEventsHandler::~UPDLoginEventsHandler()
{}

bool16 UPDLoginEventsHandler::userLoggedIn(void)
{
	do
	{
		/* Check if the Template palette is on screen */
/*		InterfacePtr<IApplication> iApplication(gSession->QueryApplication());
		if(!iApplication)
		{
			CA("1");
			return kFalse;
		}

		InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPaletteManager());
		if(!iPaletteMgr)
		{
			CA("2");
			return kFalse;
		}
		
		InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		if(!iPanelMgr)
		{
			CA("3");
			return kFalse;
		}
		
		PMLocaleId nLocale = LocaleSetting::GetLocale();	

		//iPaletteMgr->Startup(nLocale);
		IWindow* wndPtr=iPaletteMgr->GetPalette(0);
		if(!wndPtr)
		{
			CA("4");
			return kFalse;
		}
CA("No Probs at all");
		 Template Palette is present in the screen */
		//CA("Before calling loadPaletteData");
		UPDSelectionObserver selObserver(this);
		selObserver.loadPaletteData();
	}while(kFalse);			
	return kTrue;	
}

bool16 UPDLoginEventsHandler::userLoggedOut(void)
{	
	// Awasthi
	//CA("Inside User Logged Out");
	UPDActionComponent actionComponetObj(this);
	actionComponetObj.CloseUpdateRepositoryPalette();
	return kTrue;	
}

bool16 UPDLoginEventsHandler::resetPlugIn(void)
{
	//CA("UPDLoginEventsHandler::resetPlugIn");

	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil){ 
			//CA("No iApplication");
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDLoginEventsHandler::resetPlugIn::iApplication==nil");
			break;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()/*, UseDefaultIID()*/); 
		if(iPanelMgr == nil){ 
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDLoginEventsHandler::resetPlugIn::iPanelMgr == nil");
			break;
		}

		bool16 isPanelOpen = iPanelMgr->IsPanelWithMenuIDMostlyVisible(kUPDPanelWidgetActionID);
		if(!isPanelOpen){
			break;
		}

		UPDActionComponent actionComponetObj(this);
		actionComponetObj.CloseUpdateRepositoryPalette();

	
		Mediator::listControlView=NULL;
		//Mediator::editBoxView=NULL;
		Mediator::ChkHighlightView=NULL;
		Mediator::iPanelCntrlDataPtr=NULL;
		Mediator::iPanelCntrlDataPtrTemp=NULL;
		Mediator::dropdownCtrlView=NULL;
		Mediator::UPDRefreshButtonView=NULL;
		//Mediator::EditBoxTextControlData=NULL;
		//Mediator::UPDLocateButtonView = NULL;
		Mediator::UPDReloadButtonView = NULL;


		FlgToCheckMenuAction= kTrue;

		
		PMLocaleId nLocale=LocaleSetting::GetLocale() ;
		iPanelMgr->ShowPanelByMenuID(kUPDPanelWidgetActionID);

		
	//	UPDActionComponent::palettePanelPtr = iPaletteMgr ;
		DocWchUtils::StartDocResponderMode();

		if(FlgToCheckMenuAction)
		{
			IControlView* icontrol = iPanelMgr->GetVisiblePanel(kUPDPanelWidgetID);
			if(!icontrol)
			{
				//CA("!control");
				return kFalse;
			}
			icontrol->Resize(PMPoint(PMReal(207),PMReal(291)));			
		}
	}while(kFalse);
	return kTrue;
}
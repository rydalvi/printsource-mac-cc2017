//#include "VCPluginHeaders.h"
//#include "BoxReader.h"
//#include "PageData.h"
//#include "TagReader.h"
//#include "IAppFrameWork.h"
//#include "ISpecialChar.h"
//#include "CAlert.h"
//
//
//
///*
//
//  This is how the hierarchy goes
//								
//								   BOXID
//									/ \
//								   /   \
//							ObjectID   ObjectID
//								/\		 \
//							   /  \		Elem1
//							 Elem1 Elem2
//				   
//For each box in the page, there will be a corresponding object of PageData.
//Each box will contain the ObjectIDs of the sprayed data. (We have to consider the 
//different publication IDs too. For example object id 11001 with pubID 100 is not
//the same as object id 11001 for pubID 102
//*/
//
//#define CA(x)	CAlert::InformationAlert(x)
//
//bool16 BoxReader::getBoxInformation(const UIDRef& boxUIDRef, PageData& pData)
//{
//	TagReader tReader;
//	TagList tList;
//
//	bool16 taggedFrameFlag=kFalse;
////	pData.boxID=boxUIDRef.GetUID();
//	pData.BoxUIDRef=boxUIDRef;
//	
//	tList=itagReader->getTagsFromBox(boxUIDRef);
//
//	if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
//	{
//		tList=itagReader->getFrameTags(boxUIDRef);
//		if(tList.size()<=0)//It really is not our box
//			return kFalse;
//		taggedFrameFlag=kTrue;
//	}
//	int32 index;
//
//	for(int i=0; i<tList.size(); i++)
//	{
//		TagStruct tInfo=tList[i];
//		//added by Yogesh
//		if((tInfo.isTablePresent != kTrue) && (tInfo.imgFlag== 0))
//		{
//		//end Yogesh
//			if(taggedFrameFlag)
//			{	pData.boxID=boxUIDRef.GetUID();
//				this->AppendObjectInfo(tInfo, pData, kTrue);
//				continue;
//			}
//			if(this->objectIDExists(pData, tInfo, &index))//Search for this Object ID in the pData. If it exists add as an Element or add as a new Object
//			{	pData.boxID=boxUIDRef.GetUID();
//				this->AppendElementInfo(tInfo, pData, index);
//			}
//			else
//			{	pData.boxID=boxUIDRef.GetUID();
//				this->AppendObjectInfo(tInfo, pData);
//			}
//		//end by Yogesh
//		}
//	}
//	return kTrue;
//}
//
//bool16 BoxReader::elementIDExists(const ElementInfoList& eList, int32 elementID, PMString& elementName)
//{
//	for(int i=0; i<eList.size(); i++)
//	{
//		if(eList[i].elementID==elementID)		
//		{
//			if(elementName.NumUTF16TextChars())
//				elementName=eList[i].elementName;
//			return kTrue;
//		}
//	}
//	return kFalse;
//}
//
//bool16 BoxReader::AppendObjectInfo(const TagStruct& tInfo, PageData& pData, bool16 isTaggedFrame)
//{
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == NULL)
//		return kFalse;
//
//	CObjectValue oVal;
//	oVal=ptrIAppFramework->FAMMngr_getObjectElementValues(tInfo.parentId);
//
//	PMString objName=oVal.getName();
//	
//	if(objName.NumUTF16TextChars()==0)
//		return kFalse;
//
//	ObjectData oData;
//	oData.objectID=tInfo.parentId;
//
//	if(isTaggedFrame && tInfo.whichTab==4)
//		oData.objectName="Item Data";
//	else
//		oData.objectName=oVal.getName();
//
//	oData.objectTypeID=oVal.getObject_type_id();
//	oData.publicationID=tInfo.sectionID;
//	oData.whichTab = tInfo.whichTab;
//
//	if(tInfo.tableFlag) // Added for table
//		oData.isTableFlag=kTrue;
//	else
//		oData.isTableFlag=kFalse;
//
//	if(tInfo.imgFlag) // Added for Image
//		oData.isImageFlag=kTrue;
//	else
//		oData.isImageFlag=kFalse;
//		
//	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//	if(iConverter)
//	{
//		PMString temp=iConverter->translateString(oData.objectName);
//		oData.objectName=temp;
//	}
//
//	pData.objectDataList.push_back(oData);
//
//	this->AppendElementInfo(tInfo, pData, pData.objectDataList.size()-1, isTaggedFrame);
//	return kTrue;
//}
//
//bool16 getAttributeName(int32 elementID, PMString& name)
//{
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == NULL)
//		return kFalse;
//
//	VectorAttributeInfoPtr objPtr;
//
//	objPtr=ptrIAppFramework->ATTRIBMngr_getCoreAttributes();
//
//	if(!objPtr)
//		return kFalse;
//
//	VectorAttributeInfoValue::iterator it;
//
//	for(it=objPtr->begin(); it!=objPtr->end(); it++)
//	{
//		if(it->getAttributeId()==elementID)
//		{
//			name=it-> getDisplayName();
//			if(name.NumUTF16TextChars()>0)
//			{
//				delete objPtr;
//				return kTrue;
//			}
//			else
//			{
//				delete objPtr;
//				return kFalse;
//			}
//		}
//	}
//
//	delete objPtr;
//
//	objPtr=ptrIAppFramework->ATTRIBMngr_getParametricAttributes();
//
//	if(!objPtr)
//		return kFalse;
//
//	for(it=objPtr->begin(); it!=objPtr->end(); it++)
//	{
//		if(it->getAttributeId()==elementID)
//		{
//			name=it-> getDisplayName();
//			if(name.NumUTF16TextChars())
//			{
//				delete objPtr;
//				return kTrue;
//			}
//			else
//			{
//				delete objPtr;
//				return kFalse;
//			}
//		}
//	}
//	delete objPtr;
//	return kFalse;
//}
//
//
//bool16 BoxReader::AppendElementInfo(const TagStruct& tStruct, PageData& pData, int32 index, bool16 isTaggedFrame)
//{
//	TagInfo tInfo;
//	tInfo.elementID=tStruct.elementId;
//	tInfo.typeID=tStruct.typeId;
//	tInfo.isTaggedFrame=isTaggedFrame;
//	tInfo.tableFlag = tStruct.tableFlag;
//	tInfo.imageFlag = tStruct.imgFlag;
//	tInfo.whichTab = tStruct.whichTab;
//	ObjectData oData=pData.objectDataList[index];
//	ElementInfoList eList=oData.elementList;
//
//	PMString eName;
//
//	if(this->elementIDExists(eList, tInfo.elementID, eName))//An element can be sprayed multiple times
//		return kFalse;
//	tInfo.StartIndex =tStruct.startIndex;
//	tInfo.EndIndex= tStruct.endIndex;
//
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == NULL)
//		return kFalse;
//
//	if(tInfo.typeID<=0 && tInfo.elementID<=0)//Hardcoded Name
//	{
//		
//		switch(tStruct.whichTab)
//		{
//		case 1: tInfo.elementName="PF Name"; break;
//		case 2: tInfo.elementName="PG Name"; break;
//		case 3:
//			if(tInfo.elementID == -2)
//			tInfo.elementName="PR Number"; 
//			else
//			tInfo.elementName="PR Name"; 
//		break;
//		case 4: tInfo.elementName="Item Name"; break;
//		default:return kFalse;
//		}
//		pData.objectDataList[index].elementList.push_back(tInfo);
//		return kTrue;
//	}
//
//		
//	if(tInfo.typeID<=0)//Attribute Name
//	{
//		PMString tname;
//		tname.AppendNumber(tInfo.elementID);
//		if(!getAttributeName(tInfo.elementID, eName))
//			return kFalse;
//		tInfo.elementName=eName;
//		pData.objectDataList[index].elementList.push_back(tInfo);
//		return kTrue;
//	}
//
//	else if(tInfo.typeID>0 && tInfo.elementID>0)//Element Name
//	{
//		VectorElementInfoPtr eleValObj = ptrIAppFramework->EleMngr_getElementsByTypeId(tStruct.typeId);
//		if(eleValObj==NULL)
//			return kFalse;
//		VectorElementInfoValue::iterator it;
//		
//		for(it=eleValObj->begin();it!=eleValObj->end();it++)
//		{
//			if(it->getElementId()==tStruct.elementId)
//			{
//				eName=it->getName();
//				break;
//			}
//		}
//		delete eleValObj;
//		if(eName.NumUTF16TextChars()==0)
//			return kFalse;
//
//		tInfo.elementName=eName;
//		pData.objectDataList[index].elementList.push_back(tInfo);
//		return kTrue;
//	}
//	else if(tInfo.typeID>0 && tInfo.elementID<=0)//Images
//	{
//		
//		VectorTypeInfoPtr vinfoPtr=NULL;
//		switch(tStruct.whichTab)
//		{
//		case 1:
//			vinfoPtr=ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PF_ASSETS");
//			
//			break;
//		case 2:
//			vinfoPtr=ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PG_ASSETS");
//			
//			break;
//		case 3:
//			if(tInfo.tableFlag == 1)
//			{
//			vinfoPtr=ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PR_TABLES");
//			
//			break;
//			}
//			vinfoPtr=ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PR_ASSETS");
//			
//			
//			break;
//		case 4:
//			vinfoPtr=ptrIAppFramework->TYPEMngr_getAllTypesOfTypeGroupByCode("PRD_IMAGES");
//			
//			break;
//		}
//
//		VectorTypeInfoValue::iterator it;
//		for(it=vinfoPtr->begin(); it!=vinfoPtr->end(); it++)
//		{
//			if(it->getTypeId()==tInfo.typeID)
//			{
//				// Awasthi
//				eName=it->getName();
//				break;
//			}
//		}
//
//		if(vinfoPtr)
//			delete vinfoPtr;
//
//		if(eName.NumUTF16TextChars())
//		{
//			
//			tInfo.elementName=eName;
//			pData.objectDataList[index].elementList.push_back(tInfo);
//			return kTrue;
//		}
//	}
//	return kFalse;
//}
//
//
//bool16 BoxReader::objectIDExists(const PageData& pData, const TagStruct& tInfo, int32* index)
//{
//	for(int i=0; i<pData.objectDataList.size(); i++)
//	{
//		ObjectData oData=pData.objectDataList[i];
//		if(oData.objectID==tInfo.parentId && oData.publicationID==tInfo.sectionID)
//		{
//			*index=i;
//			return kTrue;
//		}
//	}
//	return kFalse;
//}
//
//
//bool16 BoxReader::getBoxInformation(const UIDRef& boxUIDRef, PageData& pData, int32 start, int32 end)
//{
//	pData.boxID=boxUIDRef.GetUID();
//	pData.BoxUIDRef =boxUIDRef;
//	TagReader tReader;
//	TagList tList;
//	tList=itagReader->getTagsFromBox(boxUIDRef);
//
//	if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
//	{
//		tList=itagReader->getFrameTags(boxUIDRef);
//		if(tList.size()<=0)//It really is not our box
//			return kFalse;
//	}
//
//	int32 index;
//
//	for(int i=0; i<tList.size(); i++)
//	{
//		TagStruct tInfo=tList[i];
//		if(tInfo.endIndex<=end && tInfo.startIndex>=start)
//		{
//			if(this->objectIDExists(pData, tInfo, &index))
//				this->AppendElementInfo(tInfo, pData, index);
//			else
//				this->AppendObjectInfo(tInfo, pData);
//		}
//	}
//	return kTrue;
//}


#include "VCPluginHeaders.h"
#include "BoxReader.h"
#include "PageData.h"
//#include "TagReader.h"
#include "IAppFrameWork.h"
#include "ISpecialChar.h"
#include "CAlert.h"
#include "ITagReader.h"

/// Global Pointers
extern ITagReader* itagReader;
extern IAppFramework* ptrIAppFramework;
extern ISpecialChar* iConverter;
///////////

/*

  This is how the hierarchy goes
								
								   BOXID
									/ \
								   /   \
							ObjectID   ObjectID
								/\		 \
							   /  \		Elem1
							 Elem1 Elem2
				   
For each box in the page, there will be a corresponding object of PageData.
Each box will contain the ObjectIDs of the sprayed data. (We have to consider the 
different publication IDs too. For example object id 11001 with pubID 100 is not
the same as object id 11001 for pubID 102
*/

#define CA(x)	CAlert::InformationAlert(x)
#define CAI(x) {PMString str;\
			str.AppendNumber(x);\
			CAlert::InformationAlert(str);}\

//This is the function which is used.
bool16 BoxReader::getBoxInformation(const UIDRef& boxUIDRef, PageData& pData)
{
	
	//CA(__FUNCTION__);
	//TagReader tReader;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return kFalse;

	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{
		ptrIAppFramework->LogDebug("AP46_UpdateRepository::BoxReader::getBoxInformation::!itagReader");
		return kFalse;
	}
	
	TagList tList;

	bool16 taggedFrameFlag=kFalse;
//	pData.boxID=boxUIDRef.GetUID();
	pData.BoxUIDRef=boxUIDRef;	
	tList=itagReader->getTagsFromBox_ForUpdate(boxUIDRef);

	/*PMString sizeOftList = "";
	sizeOftList.AppendNumber(tList.size());
	CA("sizeOftList = " + sizeOftList);*/
	
	if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
	{
		//CA("it can be the tagged frame");
		tList=itagReader->getFrameTags(boxUIDRef);
		if(tList.size()<=0)
		{
			//CA("It really is not our box");
			//It really is not our box
			//ptrIAppFramework->LogDebug("AP46_UpdateRepository::BoxReader::getBoxInformation::tList.size()<=0");
			return kFalse;
		}
		taggedFrameFlag=kTrue;
	
		/*sizeOftList.Clear();
		sizeOftList.AppendNumber(tList.size());
		CA("sizeOftList = " + sizeOftList);*/

	}
	int32 index;
	
	/*PMString a("tList.size()	:	");
	a.AppendNumber(static_cast<int32>(tList.size()));
	CA(a);*/

	for(int i=0; i<tList.size(); i++)
	{
		TagStruct tInfo=tList[i];
		if(taggedFrameFlag)
		{	
			if(tInfo.isTablePresent == kTrue)
			{
				//CA("tInfo.isTablePresent == kTrue");
				continue;
			}
			if(tInfo.imgFlag == 1) // check for Image Flag
				continue;

			pData.boxID=boxUIDRef.GetUID();			
		
			this->AppendObjectInfo(tInfo, pData, kTrue);
			continue;
		}
		if(this->objectIDExists(pData, tInfo, &index))//Search for this Object ID in the pData. If it exists add as an Element or add as a new Object
		{
			//CA("objectIDExists");
			/*if(tInfo.isTablePresent == kTrue || (tInfo.tagPtr->GetAttributeValue("tableFlag") == "-11"))
			{
				CA("tInfo.isTablePresent == kTrue || tInfo.tagPtr->GetAttributeValue tableFlag == -11");
				continue;
			}*/
			if(tInfo.imgFlag == 1) // check for Image Flag
				continue;
			pData.boxID=boxUIDRef.GetUID();
			
			this->AppendElementInfo(tInfo, pData, index);
		}
		else
		{	
			//CA("objectIDExists does not exist ");
			/*if(tInfo.isTablePresent == kTrue)
			{
				CA("inside else tInfo.isTablePresent == kTrue");
				continue;
			}*/
			if(tInfo.imgFlag == 1) // check for Image Flag
				continue;
			pData.boxID=boxUIDRef.GetUID();
			
			this->AppendObjectInfo(tInfo, pData);
		}
	}
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
	return kTrue;
}

bool16 BoxReader::elementIDExists(const ElementInfoList& eList, double elementID, PMString& elementName)
{
	//CA(__FUNCTION__);
	for(int i=0; i<eList.size(); i++)
	{
		if(eList[i].elementID==elementID)		
		{
			if(elementName.NumUTF16TextChars())
				elementName=eList[i].elementName;
			return kTrue;
		}
	}
	return kFalse;
}

bool16 BoxReader::AppendObjectInfo(const TagStruct& tInfo, PageData& pData, bool16 isTaggedFrame)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return kFalse;

	CObjectValue oVal;
	CItemModel iModel;
	//temp_to_test oVal=ptrIAppFramework->FAMMngr_getObjectElementValues(tInfo.parentId);
	if(tInfo.parentId == -1 || tInfo.whichTab == 5 )
		return kFalse;
	
//EventPrice related attributes Uodate handling is break here. on 25/01/07
	if(tInfo.elementId == -701 || tInfo.elementId == -702 || tInfo.elementId == -703 || tInfo.elementId == -704)
		return kFalse;

	//Item was sprayed for Product Tag.
	//IIDXMLElement * xmlPtr = tInfo.tagPtr;
	//if(xmlPtr == NULL)
	//	return kFalse;
	PMString tableFlagValue = tInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));
	if(tableFlagValue == "-15")
			return kFalse;

	//only filter products had items so that only their attributes get in ListBox on 18/01/07 By Dattatray.
	if((tInfo.whichTab == 5) || (tInfo.whichTab == 1)|| (tInfo.whichTab == 2)  )// || tInfo.elementId == -102)
		return kFalse;
//CA("1");
	// Filter Header attributes
	//if((tInfo.whichTab == 4 ) && (tInfo.typeId == -2))
	if(tInfo.header == 1)//if((tInfo.whichTab == 4 ) && (tInfo.header == 1))
		return kFalse;
//CA("2");
	//CAI(tInfo.elementId);
	
	if(tInfo.whichTab == 3)
	{
		if(ptrIAppFramework->StructureCache_isElementMPV(tInfo.elementId)){
			//CA("StructureCache_isElementMPV returns True");
			return kFalse;
		}
	}

	if(tInfo.whichTab == 4)
	{
		if(ptrIAppFramework->StructureCache_isAttributeMPV(tInfo.elementId)){
			//CA("StructureCache_isAttributeMPV returns True");
			return kFalse;
		}
	}
	
	//CAI(tInfo.parentId);
	//CAI(tInfo.languageID);
	PMString objName("");
	if(tInfo.whichTab == 3)
	{
		//oVal=ptrIAppFramework->GETProduct_getObjectElementValue(tInfo.parentId, tInfo.languageID);//FAMMngr_getObjectElementValues(tInfo.parentId);
		//objName=oVal.getName();
		//objName=oVal.get_number();
		//CA("objName	:	"+objName);

		PMString id("");
		id.AppendNumber(PMReal(tInfo.parentId));
		objName = id;
	}
	if(tInfo.whichTab == 4)
	{
		//CA("tInfo.whichTab == 4");
		//This Function is used to get Item Number on 18/01/07
		//PMString a("tInfo.childTag	:	");
		//a.AppendNumber(tInfo.childTag);
		//a.Append("\ntInfo.childId	:	");
		//a.AppendNumber(tInfo.childId);
		//a.Append("\ntInfo.typeId	:	");
		//a.AppendNumber(tInfo.typeId);
		//a.Append("\rtInfo.languageID	:	");
		//a.AppendNumber(tInfo.languageID);
		//CA(a);
		if(tInfo.childTag == 1)
		{
			//iModel= ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tInfo.childId,tInfo.languageID);
			/*CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(tInfo.sectionID , tInfo.childId, 0, tInfo.languageID);
			iModel =  pbObjValue.getItemModel();*/
			PMString id("");
			id.AppendNumber(PMReal(tInfo.childId));
			objName = id;
		}
		else
		{
			//iModel= ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tInfo.typeId,tInfo.languageID);
			//CPbObjectValue pbObjValue =  ptrIAppFramework->getPbObjectValueBySectionIdObjectId(tInfo.sectionID , tInfo.typeId, 0, tInfo.languageID);
			//iModel =  pbObjValue.getItemModel();
			PMString id("");
			id.AppendNumber(PMReal(tInfo.parentId));
			objName = id;
		}
		//objName = iModel.getItemNo();		
	}
	//CA("item no  = " + objName);
//	PMString objName=oVal.getName();
	
	if(objName.NumUTF16TextChars()==0)
		return kFalse;

	ObjectData oData;
	oData.objectID=tInfo.parentId;	

	if(isTaggedFrame && tInfo.whichTab==4)
		oData.objectName="Item Data";
	else
		oData.objectName=objName;

	if(tInfo.whichTab == 3)
		oData.objectTypeID=tInfo.parentTypeID;  //oVal.getObject_type_id();
	//if(tInfo.whichTab == 4) //For Item TypeId == -1
	//	oData.objectTypeID= tInfo.typeId;

	if(tInfo.whichTab == 4 ) //For Item TypeId == -1
	{
		if(tInfo.childTag == 1)
			oData.objectTypeID = tInfo.childId;
		else
			oData.objectTypeID = tInfo.typeId;
	}
	oData.publicationID=tInfo.sectionID;
	oData.whichTab = tInfo.whichTab;

	if(tInfo.isTablePresent) // Added for table
		oData.isTableFlag=kTrue;
	else
	{
		PMString tempTableFlagValue = tInfo.tagPtr->GetAttributeValue(WideString("tableFlag"));
		
		if(tInfo.whichTab == 4 && tempTableFlagValue == "-1001")
		{
			oData.isTableFlag=kFalse;
		}
		else
		{
            oData.isTableFlag=kFalse;
			//CA("oData.isTableFlag=kFalse");
		}
	}
	if(tInfo.imgFlag) // Added for Image
		oData.isImageFlag=kTrue;
	else
		oData.isImageFlag=kFalse;
		
	//CA("oData.objectName Before spcl conversion : " + oData.objectName);
	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	if(iConverter)
	{
		PMString temp=iConverter->translateString(oData.objectName);
		oData.objectName=temp;
	}
	//CA("oData.objectName After spcl conversion : " + oData.objectName);
	pData.objectDataList.push_back(oData);

	this->AppendElementInfo(tInfo, pData, (int32)pData.objectDataList.size()-1, isTaggedFrame);
	return kTrue;
}

bool16 getAttributeName(double elementID, PMString& name)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return kFalse;

	VectorAttributeInfoPtr objPtr;

	objPtr=NULL;//temp_to_test ptrIAppFramework->ATTRIBMngr_getCoreAttributes();

	if(!objPtr)
		return kFalse;

	VectorAttributeInfoValue::iterator it;

	for(it=objPtr->begin(); it!=objPtr->end(); it++)
	{
		if(it->getAttributeId()==elementID)
		{
			name=it-> getDisplayName();
			if(name.NumUTF16TextChars()>0)
			{
				delete objPtr;
				return kTrue;
			}
			else
			{
				delete objPtr;
				return kFalse;
			}
		}
	}

	delete objPtr;

	objPtr=NULL;//temp_to_test ptrIAppFramework->ATTRIBMngr_getParametricAttributes();

	if(!objPtr)
		return kFalse;

	for(it=objPtr->begin(); it!=objPtr->end(); it++)
	{
		if(it->getAttributeId()==elementID)
		{
			name=it-> getDisplayName();
			if(name.NumUTF16TextChars())
			{
				delete objPtr;
				return kTrue;
			}
			else
			{
				delete objPtr;
				return kFalse;
			}
		}
	}
	delete objPtr;
	return kFalse;
}


bool16 BoxReader::AppendElementInfo(const TagStruct& tStruct, PageData& pData, int32 index, bool16 isTaggedFrame)
{
	//CA(__FUNCTION__);
	TagInfo tInfo;
	tInfo.elementID=tStruct.elementId;
	tInfo.typeID=tStruct.typeId;
	tInfo.isTaggedFrame=isTaggedFrame;
	tInfo.tableFlag = tStruct.isTablePresent;
	tInfo.imageFlag = tStruct.imgFlag;
	tInfo.whichTab = tStruct.whichTab;
	tInfo.languageid = tStruct.languageID;
	tInfo.childId = tStruct.childId;
	tInfo.childTag = tStruct.childTag;
	tInfo.dataType = tStruct.dataType;
	tInfo.header = tStruct.header;
	
	ObjectData oData=pData.objectDataList[index];
	ElementInfoList eList=oData.elementList;

	/*PMString ASD(" tInfo.elementID : ");
	ASD.AppendNumber(tInfo.elementID);
	ASD.Append("  tInfo.typeID : ");
	ASD.AppendNumber(tInfo.typeID);
	ASD.Append("tStruct.parentId");
	ASD.AppendNumber(tStruct.parentId);
	CA(ASD);*/

	PMString eName;
	if(this->elementIDExists(eList, tInfo.elementID, eName))//An element can be sprayed multiple times
		return kFalse;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return kFalse;

	//CAI(tInfo.elementID);
	if(tInfo.whichTab == 3)
	{
		if(ptrIAppFramework->StructureCache_isElementMPV(tInfo.elementID)){
			//CA("StructureCache_isElementMPV returns True");
			return kFalse;
		}
	}

	if(tInfo.whichTab == 4)
	{
		if(ptrIAppFramework->StructureCache_isAttributeMPV(tInfo.elementID)){
			//CA("StructureCache_isAttributeMPV returns True");
			return kFalse;
		}
	}

	tInfo.StartIndex =tStruct.startIndex;
	tInfo.EndIndex= tStruct.endIndex;
	
	if(tInfo.imageFlag == 0 && tInfo.tableFlag == 0 )
	{
		if(tInfo.whichTab != 4)
		{
			//CA("tInfo.whichTab != 4");
			VectorElementInfoPtr eleValObj = NULL;
			switch(tStruct.whichTab)
			{
				case 1:
					//eleValObj = ptrIAppFramework->ElementCache_getCopyAttributesByIndex(1, tInfo.languageid);
					break;
				case 2: 
					//eleValObj = ptrIAppFramework->ElementCache_getCopyAttributesByIndex(2, tInfo.languageid);
					break;
				case 3:
					eleValObj = ptrIAppFramework->StructureCache_getItemGroupCopyAttributesByLanguageId(tInfo.languageid);
					break;		
			}
			if(eleValObj==NULL)
				return kFalse;
			VectorElementInfoValue::iterator it;
			
			bool16 gotName = kFalse;
			for(it=eleValObj->begin();it!=eleValObj->end();it++)
			{
				/*PMString Temp = "";
				Temp.Append("it->getElementId() = ");
				Temp.AppendNumber(it->getElementId());
				Temp.Append(" , tStruct.elementId = ");
				Temp.AppendNumber(tStruct.elementId);
				CA(Temp);*/

				if(it->getElementId()==tStruct.elementId)
				{
					//CA("eName = " + it-> getDisplayName());
					eName=it-> getDisplayName();
					gotName = kTrue;
					break;
				}
			}
			if(eleValObj)
				delete eleValObj;

			//following if is added for Standard Table 
			if(tInfo.elementID != -1 && tInfo.elementID != -2 && tInfo.elementID != 1 && tInfo.elementID != 2 && gotName==kFalse)
			{
				eName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tInfo.elementID , tInfo.languageid);
			}
			if(eName.NumUTF16TextChars()==0)
				return kFalse;
			tInfo.elementName=eName;
		}
		else if(tInfo.whichTab == 4)
		{
			//CA("else if(tInfo.whichTab == 4)");
			//This Function will return Item Name.
			tInfo.elementName = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tInfo.elementID , tInfo.languageid);
		}

		pData.objectDataList[index].elementList.push_back(tInfo);
		return kTrue;
	}

	//if(tInfo.typeID<=0 && tInfo.elementID<=0)//Hardcoded Name
	//{  
	//	switch(tStruct.whichTab)
	//	{
	//	case 1: tInfo.elementName="PF Name"; break;
	//	case 2: tInfo.elementName="PG Name"; break;
	//	case 3: if(tInfo.elementID == -2 && tStruct.typeId == -1)
	//				tInfo.elementName="Product Number";
	//			else if(tInfo.elementID == -1 && tStruct.typeId == -1)
	//			{									
	//				tInfo.elementName="Product Name"; 
	//			}
	//			else if(tStruct.typeId == -3)
	//			{
	//				tInfo.elementName = tStruct.tagPtr->GetTagString();
	//			}
	//			break;
	//	case 4: tInfo.elementName="Item Name"; break;
	//	default:return kFalse;
	//	}
	//	pData.objectDataList[index].elementList.push_back(tInfo);
	//	return kTrue;
	//}

	//if(tInfo.typeID<=0)//Attribute Name
	//{	
	//	PMString tname;
	//	tname.AppendNumber(tInfo.elementID);

	//	if(!getAttributeName(tInfo.elementID, eName))
	//		return kFalse;

	//	tInfo.elementName=eName;
	//	pData.objectDataList[index].elementList.push_back(tInfo);

	//	return kTrue;
	//}

	//else if(tInfo.typeID>0 && tInfo.elementID>0)//Element Name
	//{
	//	VectorElementInfoPtr eleValObj = NULL;//temp_to_test ptrIAppFramework->EleMngr_getElementsByTypeId(tStruct.typeId);
	//	if(eleValObj==NULL)
	//		return kFalse;
	//	VectorElementInfoValue::iterator it;
	//	
	//	for(it=eleValObj->begin();it!=eleValObj->end();it++)
	//	{
	//		if(it->getElementId()==tStruct.elementId)
	//		{
	//			//temp_to_test eName=it->getName();
	//			break;
	//		}
	//	}
	//	delete eleValObj;
	//	if(eName.NumUTF16TextChars()==0)
	//		return kFalse;

	//	tInfo.elementName=eName;
	//	pData.objectDataList[index].elementList.push_back(tInfo);
	//	return kTrue;
	//}
	else if(tInfo.imageFlag == 1 || tInfo.tableFlag == 1 && tInfo.tableType != 2/*tInfo.typeID != -3*//*tInfo.typeID>0 && tInfo.elementID<=0*/)//Images
	{
		//CA("tInfo.imageFlag == 1 || tInfo.tableFlag == 1 && tInfo.typeID != -3");
		VectorTypeInfoPtr vinfoPtr=NULL;
		
		switch(tStruct.whichTab)
		{
		case 1:
			//vinfoPtr=ptrIAppFramework->ElementCache_getImageAttributesByIndex(1);
			break;
		case 2:
			//vinfoPtr=ptrIAppFramework->ElementCache_getImageAttributesByIndex(1);
			break;
		case 3:
			if(tInfo.tableFlag == 1)
			{//CA("!!!!!!! tInfo.tableFlag == 1");
				vinfoPtr=ptrIAppFramework->StructureCache_getListTableTypes();
				break;
			}
			//vinfoPtr=ptrIAppFramework->StructureCache_getItemGroupImageAttributes();
			eName  = ptrIAppFramework->StructureCache_TYPECACHE_getTypeNameById(tInfo.typeID);
			break;
		case 4:
			//vinfoPtr=ptrIAppFramework->AttributeCache_getItemImages();
			eName  = ptrIAppFramework->StructureCache_TYPECACHE_getTypeNameById(tInfo.typeID);
			break;
		}

		VectorTypeInfoValue::iterator it;
		for(it=vinfoPtr->begin(); it!=vinfoPtr->end(); it++)
		{
			/*CA("inside vinfoPtr");
			PMString Temp = "";
			Temp.Append("it->getTypeId() = ");
			Temp.AppendNumber(it->getTypeId());
			Temp.Append(" , tInfo.typeID = ");
			Temp.AppendNumber(tInfo.typeID);
			CA(Temp);*/

			if(it->getTypeId()==tInfo.typeID)
			{
				// Awasthi
				eName=it->getName();
				//CA("it->getName() = " + eName);
				break;
			}
		}

		if(vinfoPtr)
			delete vinfoPtr;

		if(eName.NumUTF16TextChars())
		{	
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
	}
	else if(tInfo.imageFlag == 1 || tInfo.tableFlag == 1 && tInfo.tableType == 2/*tInfo.typeID == -3*//*tInfo.typeID>0 && tInfo.elementID<=0*/)//Images
	{	
		eName = tStruct.tagPtr->GetTagString();
		//CA(eName);
		if(eName.NumUTF16TextChars())
		{		
			tInfo.elementName=eName;
			pData.objectDataList[index].elementList.push_back(tInfo);
			return kTrue;
		}
	}
	return kFalse;
}


bool16 BoxReader::objectIDExists(const PageData& pData, const TagStruct& tInfo, int32* index)
{
	//CA(__FUNCTION__);
	for(int i=0; i<pData.objectDataList.size(); i++)
	{
		ObjectData oData=pData.objectDataList[i];
		if(tInfo.whichTab == 4 && tInfo.isTablePresent == 0)
		{
			//if(oData.objectTypeID == tInfo.typeId)
			if(tInfo.childTag == 1)
			{
				if(oData.objectTypeID == tInfo.childId)
				{
					*index=i;
					return kTrue;
				}
				else
					continue;
			}
			else
			{
				if(oData.objectTypeID == tInfo.typeId)
				{
					*index=i;
					return kTrue;
				}
				else
					continue;
			}
		}
		if(tInfo.whichTab == 4 && tInfo.imgFlag == 1)
		{
			if(oData.objectID==tInfo.parentId && oData.publicationID==tInfo.sectionID)
			{
				*index=i;
				return kTrue;
			}
			else
				continue;
		}
	}
	return kFalse;
}


bool16 BoxReader::getBoxInformation(const UIDRef& boxUIDRef, PageData& pData, int32 start, int32 end)
{
	//CA(__FUNCTION__);
	pData.boxID=boxUIDRef.GetUID();
	pData.BoxUIDRef =boxUIDRef;
	//TagReader tReader;
//	InterfacePtr<ITagReader> itagReader	((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader){ 
		return kFalse;
	}

	TagList tList;
	tList=itagReader->getTagsFromBox(boxUIDRef);

	if(!tList.size())//The box contains no tags..But it can be the tagged frame!!!!!
	{
		tList=itagReader->getFrameTags(boxUIDRef);
		if(tList.size()<=0)//It really is not our box
			return kFalse;
	}
	int32 index;

	for(int i=0; i<tList.size(); i++)
	{
		TagStruct tInfo=tList[i];
		if(tInfo.endIndex<=end && tInfo.startIndex>=start)
		{
			if(this->objectIDExists(pData, tInfo, &index))
				this->AppendElementInfo(tInfo, pData, index);
			else
				this->AppendObjectInfo(tInfo, pData);
		}
	}
	for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
	{
		tList[tagIndex].tagPtr->Release();
	}
	return kTrue;
}

bool16 BoxReader::itemIDelementIDExists(const ElementInfoList& eList, double elementID,double itemID, PMString& elementName)
{
	
	//CA(__FUNCTION__);
	//CA_NUM("eList.size() :",eList.size());
	for(int i=0; i<eList.size(); i++)
	{
		//CA_NUM("elementID : ",elementID);
		if(eList[i].elementID==elementID )//&& eList[i].typeID == itemID)		
		{
			
			if(elementName.NumUTF16TextChars())
				elementName=eList[i].elementName;
			return kTrue;
		}
	}
	return kFalse;
}
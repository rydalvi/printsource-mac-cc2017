#include "VCPluginHeaders.h"

#include "IPMUnknown.h"
#include "UIDRef.h"
//#include "ITextFrame.h"
#include "IXMLUtils.h"
#include "IXMLTagList.h"
//#include "ISelection.h"
#include "IDocument.h"
#include "IStoryList.h"
#include "IFrameList.h"
#include "ITextModel.h"
#include "IIDXMLElement.h"
#include "IXMLReferenceData.h"
#include "IGraphicFrameData.h"
#include "UIDList.h"
#include "XMLReference.h"
#include "IFrameUtils.h"
#include "ILayoutUtils.h" //Cs4
#include "k2smartptr.h"
#include "CAlert.h"
#include "IXMLUtils.h"
#include "ITextFrameColumn.h"
#include "IHierarchy.h"
#include "TagReader.h"
#include "IAppFramework.h"
#include "CAlert.h"
#define CA(x) CAlert::InformationAlert(x)

void TagReader::getTextFrameTags(void)
{
	//CA(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*>(CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;
	do{	
/*		//CS3 Change		
		InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		
		if (textFrame == nil)
		{
            ptrIAppFramework->LogDebug("AP46_UpdateRepository::TagReader::getTextFrameTags::!textFrame");
			break;
		}
*/
		///////Added By Amit

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TagReader::getTextFrameTags::graphicFrameHierarchy == nil");
			return;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TagReader::getTextFrameTags::multiColumnItemHierarchy == nil");
			return;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TagReader::getTextFrameTags::multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn");
			return;
		}
		InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TagReader::getTextFrameTags::frameItemHierarchy == nil");
			return;
		}

		InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TagReader::getTextFrameTags::textFrame == nil");
			//CA("!!!ITextFrameColumn");
			return;
		}

		///////end
		InterfacePtr<ITextModel> objTxtMdl (textFrame->QueryTextModel());
		if (objTxtMdl == nil)
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TagReader::getTextFrameTags::!objTxtMdl");
           	break;
		}

		IXMLReferenceData* objXMLRefDat=(IXMLReferenceData*) objTxtMdl->QueryInterface(IID_IXMLREFERENCEDATA);
		if (objXMLRefDat==nil)
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::TagReader::getTextFrameTags::!objXMLRefDat");
			break;
		}

		XMLReference xmlRef=objXMLRefDat->GetReference();

		UIDRef refUID=xmlRef.GetUIDRef();		
       	//IIDXMLElement *xmlElement=xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement==nil)
		{
          	ptrIAppFramework->LogDebug("AP46_UpdateRepository::TagReader::getTextFrameTags::!xmlElement");
			break;
		}
       	xmlPtr=xmlElement;

		int elementCount=xmlElement->GetChildCount();    		
			
		PMString tagName=xmlElement->GetTagString();
		TagStruct tInfo;
		int32 attribCount=xmlElement->GetAttributeCount();
		TextIndex sIndex=0, eIndex=0;
		Utils<IXMLUtils>()->GetElementIndices(xmlElement, &sIndex, &eIndex);
		tInfo.startIndex=sIndex;
		tInfo.endIndex=eIndex;
		xmlElement->AddRef();
		tInfo.tagPtr=xmlElement;
		

		for(int j=0; j<attribCount; j++)
		{
			PMString attribName=xmlElement->GetAttributeNameAt(j);
			PMString attribVal=xmlElement->GetAttributeValue(WideString(attribName));
			getCorrespondingTagAttributes(WideString(attribName),  WideString(attribVal), tInfo); //Cs4
		}
		//tList.push_back(tInfo);
		if(tInfo.isTablePresent == 1)
			getGraphicFrameTags();
		else
		{

			for(int i=0; i<elementCount; i++)
			{
           		XMLReference elementXMLref=xmlElement->GetNthChild(i);
				//IIDXMLElement * childElement=elementXMLref.Instantiate();
				InterfacePtr<IIDXMLElement>childElement(elementXMLref.Instantiate());
				if(childElement==NULL)
				{
	               
					continue;
				}
				PMString tagName=childElement->GetTagString();
				TagStruct tInfo;
				int32 attribCount=childElement->GetAttributeCount();
				TextIndex sIndex=0, eIndex=0;
				Utils<IXMLUtils>()->GetElementIndices(childElement, &sIndex, &eIndex);
				tInfo.startIndex=sIndex;
				tInfo.endIndex=eIndex;
				childElement->AddRef();
				tInfo.tagPtr=childElement;

				for(int j=0; j<attribCount; j++)
				{

					PMString attribName=childElement->GetAttributeNameAt(j);

					PMString attribVal=childElement->GetAttributeValue(WideString(attribName));//Cs4

					getCorrespondingTagAttributes(WideString(attribName),WideString(attribVal), tInfo);
				}
				tList.push_back(tInfo);				

			}
		}

	}while(0);
}

void TagReader::getGraphicFrameTags(void)
{
	//CA(__FUNCTION__);
	do{	
		
		InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);	
		InterfacePtr<IXMLReferenceData> objXMLRefDat(Utils<IXMLUtils>()->QueryXMLReferenceData(unknown));
		if (objXMLRefDat == nil)
			break;

		XMLReference xmlRef = objXMLRefDat->GetReference();
		UIDRef refUID = xmlRef.GetUIDRef();
		//IIDXMLElement *xmlElement = xmlRef.Instantiate();
		InterfacePtr<IIDXMLElement>xmlElement(xmlRef.Instantiate());
		if(xmlElement == nil)
			break;

		xmlPtr=xmlElement;
       
		PMString tagName = xmlElement->GetTagString();
		
		TagStruct tInfo;
		xmlElement->AddRef();
		tInfo.tagPtr=xmlElement;

		int32 attribCount = xmlElement->GetAttributeCount();

		for(int j=0; j<attribCount; j++)
		{
			PMString attribName = xmlElement->GetAttributeNameAt(j);
			PMString attribVal  = xmlElement->GetAttributeValue(WideString(attribName));
			getCorrespondingTagAttributes(attribName, attribVal, tInfo);
			
		}
		tInfo.endIndex=-1;
		tInfo.startIndex=-1;
		tList.push_back(tInfo);
        
	}while(0);
}

TagList TagReader::getTagsFromBox(UIDRef boxId, IIDXMLElement** xmlPtr)
{
	//CA(__FUNCTION__);
	tList.clear();

	/*InterfacePtr<IPMUnknown> unknown(boxId, IID_IUNKNOWN);
	UID frameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);	*/
	UID textFrameUID = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxId, UseDefaultIID());
	if (graphicFrameDataOne) 
	{
		textFrameUID = graphicFrameDataOne->GetTextContentUID();
	}

	boxUIDRef=boxId;	

	if(textFrameUID == kInvalidUID)
	{
		getGraphicFrameTags();	
	}
	else 
	{

		getTextFrameTags();
	}
	

	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;
	if(xmlPtr)
	{
		*xmlPtr=this->xmlPtr;
	}

	return tList;
}

bool16 TagReader::getCorrespondingTagAttributes(const PMString& attribName, const PMString& attribVal, TagStruct& tStruct)
{
	//CA(__FUNCTION__);
	if(attribName.IsEqual("ID", kFalse))
	{
		tStruct.elementId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	
	if(attribName.IsEqual("imgFlag", kFalse))
	{
		tStruct.imgFlag=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("index", kFalse))
	{
		tStruct.whichTab=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("typeId", kFalse))
	{
		tStruct.typeId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("parentId", kFalse))
	{
		tStruct.parentId=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("reserved", kFalse))
	{
		tStruct.sectionID=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("parentTypeID", kFalse))
	{
		tStruct.parentTypeID=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	if(attribName.IsEqual("LanguageID", kFalse))
	{
		tStruct.languageID=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("tableFlag", kFalse))
	{
		int32 isTable = attribVal.GetAsNumber();
		if(isTable == 1)
		{
		//	CA("has table");
			tStruct.isTablePresent = kTrue;
		}
		else
		{
			//CA("NO table");
			tStruct.isTablePresent = kFalse;
		}

		tStruct.numValidFields++;
		 
		return kTrue;
	}

	if(attribName.IsEqual("isAutoResize", kFalse))
	{
		tStruct.isAutoResize=attribVal.GetAsNumber();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("rowno", kFalse))
	{
		tStruct.rowno=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}

	if(attribName.IsEqual("colno", kFalse))
	{
		tStruct.colno=attribVal.GetAsDouble();
		tStruct.numValidFields++;
		return kTrue;
	}
	
	return kFalse;
}


bool16 TagReader::GetUpdatedTag(TagStruct& tInfo)
{
	//CA(__FUNCTION__);
	if(!tInfo.tagPtr)
		return kFalse;
	
	TextIndex sIndex=0, eIndex=0;
	Utils<IXMLUtils>()->GetElementIndices(tInfo.tagPtr, &sIndex, &eIndex);
	
	tInfo.startIndex=sIndex;
	tInfo.endIndex=eIndex;
	
	return kTrue;
}

TagList TagReader::getFrameTags(UIDRef frameUIDRef)
{
	tList.clear();
	boxUIDRef=frameUIDRef;
	// Get the graphic frame tags as in case of graphic frames tag is attached with the frame itself
	getGraphicFrameTags();			

	for(int i=0; i<tList.size(); i++)
		tList[i].isProcessed=kFalse;

	return tList;
}
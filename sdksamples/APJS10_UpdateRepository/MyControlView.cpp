#include "VCPlugInHeaders.h"

#include "ErasablePanelView.h"
//#include "ImageReorgniser.h"
//#include "GlobalFunctions.h"

// Plug-in includes

#include "UPDID.h"
#include "CAlert.h"

#define CA(X) CAlert::InformationAlert(X)

int32 num_of_images = 1 ;


/** Overrides the ConstrainDimensions to control the maximum and minimum width 
	and height of panel when it is resized.

	@ingroup snippetrunner
	@author Seoras Ashby
*/
class  UpdateControlView : public ErasablePanelView
{
	public:
		/** 
			Constructor.
		*/
		UpdateControlView(IPMUnknown* boss) 
			: ErasablePanelView(boss) { fowner = boss ;}

		/** 
			Destructor.
		*/
		virtual ~UpdateControlView() {}

		/** Allows the panel size to be constrained.  
			@param dimensions OUT specifies the maximum and minimum width and height of the panel
				when it is resized.
		*/
		virtual PMPoint	ConstrainDimensions(const PMPoint& dimensions) const;

		/**	Clear the SnippetRunner framework log when resizing. 
			The multi line widget log gives some odd behaviour if we don't. 
			Means you lose the log contents when you resize.
			@param newDimensions
			@param invalidate
		*/
		virtual  void Resize(const PMPoint& newDimensions, bool16 invalidate);

	private:

		static const int kMinimumPanelWidth;
		static const int kMaximumPanelWidth;
		static const int kMinimumPanelHeight;
		static const int kMaximumPanelHeight;
		IPMUnknown *fowner ; 
};

// define the max/min width and height for the panel
const int	UpdateControlView::kMinimumPanelWidth	=	207 ;
const int 	UpdateControlView::kMaximumPanelWidth	=	300 ;
const int 	UpdateControlView::kMinimumPanelHeight	=	291 ;
const int 	UpdateControlView::kMaximumPanelHeight	=	1500;


/* Make the implementation available to the application.	
*/
CREATE_PERSIST_PMINTERFACE(UpdateControlView , kUPDUpdateControlViewImpl)


/* Allows the panel size to be constrained.  
*/
PMPoint UpdateControlView::ConstrainDimensions(const PMPoint& dimensions) const
{
	PMPoint constrainedDim = dimensions;

	// Width can vary if not above maximum or below minimum
	if(constrainedDim.X() > kMaximumPanelWidth)
	{
		constrainedDim.X(kMaximumPanelWidth);
	}
	else if(constrainedDim.X() < kMinimumPanelWidth)
	{
		constrainedDim.X(kMinimumPanelWidth);
	}

	// Height can vary if not above maximum or below minimum
	if(constrainedDim.Y() > kMaximumPanelHeight)
	{
		constrainedDim.Y(kMaximumPanelHeight);
	}
	else if(constrainedDim.Y() < kMinimumPanelHeight)
	{
		constrainedDim.Y(kMinimumPanelHeight);
	}

	return constrainedDim;
}

/*
*/
void UpdateControlView::Resize(const PMPoint& newDimensions, bool16 invalidate)
{
	/*CA("UpdateControlView::Resize");
	PMString  ASD("newDimensions.X : ");
	ASD.AppendNumber(newDimensions.X());
	ASD.Append("  newDimensions.Y : ");
	ASD.AppendNumber(newDimensions.Y());
	CA(ASD);*/
	PMPoint constrainedDim = newDimensions;
	if(constrainedDim.X() < kMinimumPanelWidth)
	{
		constrainedDim.X(kMinimumPanelWidth);
	}
	
	/*ASD.Append("--constrainedDim.X : ");
	ASD.AppendNumber(constrainedDim.X());
	ASD.Append("  --constrainedDim.Y : ");
	ASD.AppendNumber(constrainedDim.Y());
	CA(ASD);*/

	//----------This code is added to avoid situation where two indesign windows are opened in Indesign CS3 on 10/07/07
	//------------one of Indesign and other showing hwndpaletteBasedUID.One of the palette is having 0,0 co-ordinates which that is cant traced so..
	PMReal x=newDimensions.X();
	PMReal y=newDimensions.Y();

	if(x == 0 && y == 0 )
	{
		//CA("Its zero zero");
		PMPoint newDimension(PMReal(207),PMReal(291));
		 ErasablePanelView::Resize(newDimension, invalidate) ;
	}
	//---------------upto here..
	else
	{
		ErasablePanelView::Resize(/*newDimensions*/constrainedDim, invalidate);
	}
}




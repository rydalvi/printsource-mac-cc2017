#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "ISelectableDialogSwitcher.h"
#include "IAppFramework.h"
#include "ListBoxHelper.h"
#include "IListBoxController.h"
#include "IListControlData.h"
#include "ITextControlData.h"
#include "MediatorClass.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
#include "IHierarchy.h"
#include "IListBoxController.h"
#include "ILayoutUtils.h"  //Cs4
#include "MediatorClass.h"
#include "Refresh.h"
#include "RefreshData.h"
#include "UPDID.h"
#include "IFrameUtils.h" 
#include "IWorkspace.h"
#include "ISwatchList.h"
#include "ITextAttrUID.h"
#include "ICommand.h"
#include "ILayoutControlData.h"
#include "ISpread.h"
#include "IGeometry.h"
#include "UPDActionComponent.h"
#include "ISpreadList.h"

#include "SystemUtils.h"
#include "ILayoutUtils.h" //cs4
#include "ILayoutUIUtils.h"
#include "ILayoutUIUtils.h"
#include "ITextAttrUtils.h"
#include "IPanelControlData.h"
#include "DocWchUtils.h"
#include "CheckTag.h"
#include "ISpecialChar.h"
#include "ISelectionManager.h"
#include "ILayoutSelectionSuite.h"
#include "ITextSelectionSuite.h"
#include "IConcreteSelection.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ITextTarget.h"
#include "ITextModel.h"
#include "ITagReader.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "ITextWalker.h"
#include "ITextWalkerSelectionUtils.h"
#include "AcquireModalCursor.h" 
#include "TagReader.h"
#include "AcquireModalCursor.h"
#include "PageData.h"
#include "TagStruct.h"
#include "UPDSelectionObserver.h"
#include "ITextFrameColumn.h"
#include "IGraphicFrameData.h"
#include "ITreeViewMgr.h"
#include "UPDTreeDataCache.h"
#include "UPDTreeModel.h"
#include "IntNodeID.h"

#define CA(z) CAlert::InformationAlert(z)
extern int32 Flag1;
extern RefreshDataList rDataList;
extern int16 pgno,cursel,seltext,FlagParentID;
extern RefreshDataList ColorDataList;
extern int32 CurrentSelectedRowno;
extern bool16 FlgToCheckMenuAction;
bool16 isElementoptSelected = kFalse;

extern int32 GroupFlag;
extern RefreshDataList OriginalrDataList;

//extern bool16 GroupFlag;
/// Global Pointers
extern ITagReader* itagReader;
extern IAppFramework* ptrIAppFramework;
extern ISpecialChar* iConverter;
//extern bool16 SelectBoxFlage;
///////////
int32 refreshStatus;

RefreshDataList ElementGroupList;
RefreshDataList UniqueDataList;


bool16 allElementSelectedFlag = kFalse ; //Added

InterfacePtr<IPanelControlData> PanelControlDataforList;

extern set<NodeID> UniqueNodeIds;

CREATE_PMINTERFACE(UPDSelectionObserver, kUPDContentSelectionObserverImpl)

UPDSelectionObserver::UPDSelectionObserver(IPMUnknown *boss) :
	ActiveSelectionObserver(boss/*, IID_IUPDContentSELECTIONOBSERVER*/) {}

UPDSelectionObserver::~UPDSelectionObserver() {}

void UPDSelectionObserver::HandleSelectionChanged(const ISelectionMessage* message)
{
	UpdatePanel();
}

void UPDSelectionObserver::AutoAttach()
{   
	ActiveSelectionObserver::AutoAttach();	
	Mediator::loadData=kTrue;
	
	/*if(Mediator::iPanelCntrlDataPtr==NULL)
	{*/	
		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData==NULL)
		{
			//CA("iPanelControlData is NULL");
			return;
		}
		/* Storing IPanelControlData in Mediator */
		Mediator::iPanelCntrlDataPtr=iPanelControlData;
		
		AttachWidget(iPanelControlData, kUPDOptionsDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		//AttachWidget(Mediator::iPanelCntrlDataPtr, kTPLClassTreeIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kUPDRefreshButtonWidgetID, IID_ITRISTATECONTROLDATA);
		//AttachWidget(Mediator::iPanelCntrlDataPtr, kUPDRefreshButtonWidgetID, IID_IBOOLEANCONTROLDATA);  // new added 25 Feb vaibhav
		//AttachWidget(iPanelControlData, kUPDPageNumWidgetID, IID_ITEXTCONTROLDATA );

		AttachWidget(iPanelControlData, kUPDReloadIconWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kUPDSelectionBoxtWidgetID, IID_ITRISTATECONTROLDATA);
	/*}*/
	
	loadPaletteData();	

}

void UPDSelectionObserver::loadPaletteData(void)
{	
	//CA("Inside LoadPalleteData");
	//this->DisableAll();    
	//Mediator md;
	static int i;
	
	if(Mediator::loadData==kFalse)
	Flag1 = 1;
	if(Mediator::loadData==kTrue)
	{
		//CA("1");
		do
		{		
					
			/* Check if the user has LoggedIn successfully */
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == NULL)
				return;
		
			bool16 result=ptrIAppFramework->getLoginStatus();	
			//CA("2");
						
			if(Mediator::iPanelCntrlDataPtr==NULL)
			{	
				InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
				if (iPanelControlData==NULL)
				{
					//CA("iPanelControlData is NULL");
					Mediator::iPanelCntrlDataPtr=Mediator::iPanelCntrlDataPtrTemp;
					//return;					
				}
				
				else
				{
					/* Storing IPanelControlData in Mediator */
					Mediator::iPanelCntrlDataPtr=iPanelControlData;//
					Mediator::iPanelCntrlDataPtrTemp=iPanelControlData;
				}
				i++;
			}
			//CA("3");
			UPDActionComponent actionObsever(this);
			actionObsever.DoPalette();
		
			Mediator::dropdownCtrlView=NULL;
			Mediator::UPDRefreshButtonView=NULL;
			Mediator::UPDReloadButtonView=NULL;
			//Yogesh
			//Mediator :: UPDLocateButtonView = NULL;
			IControlView* dropdownCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDOptionsDropDownWidgetID);
			if(!dropdownCtrlView)
				break;

			IControlView* listControlView1=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDPanelTreeViewWidgetID);
			if(!listControlView1){ 	//CA("!listControlView1");
				break;
			}

			IControlView* refreshBtnCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDRefreshButtonWidgetID);
			if(!refreshBtnCtrlView)
				break;
//////////// from here 

				IControlView* UPDReloadButtonView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDReloadIconWidgetID);
			if(!UPDReloadButtonView)
				break;
////////////// till here

			//Start Yogesh
			/*IControlView* locateBtnCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDLocateButtonWidgetID);
			if(!locateBtnCtrlView)
				break;*/
			//End Yogesh

			/*IControlView* EditBoxCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDPageNumWidgetID);
			if(!EditBoxCtrlView)
				break;

			InterfacePtr<ITextControlData> obj(EditBoxCtrlView, UseDefaultIID());
			////24/01
			//if(!obj)
			//	break;
			////end 24/01


			Mediator::EditBoxTextControlData=obj;
			*/
			// Added By Awasthi
			Mediator::dropdownCtrlView=dropdownCtrlView;
			Mediator::UPDRefreshButtonView=refreshBtnCtrlView;
			Mediator::listControlView=listControlView1;	
			//Mediator::editBoxView=EditBoxCtrlView;
			Mediator::UPDReloadButtonView=UPDReloadButtonView;
			//yogesh
			//Mediator::UPDLocateButtonView = locateBtnCtrlView;

			IDocument *iDoc=Utils<ILayoutUIUtils>()->GetFrontDocument()/*::GetFrontDocument()*/;			
			if(iDoc== NULL) { //CA("iDoc== NULL");
			//return;
			}

			if(!(result && iDoc))
			{	
				if(!result && !iDoc)
				{
					//CA("Invalid selection.  Please log into PRINTsource and open a document before continuing.");
					//this->DisableAll();
				}
				else 
				{
					if(!result){
						//CA("Invalid selection.  Please log into PRINTsource before continuing.");
						//this->DisableAll();
					}
					else if(result && !iDoc){
						//CA("Invalid selection.  Please open a document before continuing.");
						//this->DisableAll();
					}
				}
							
								
				InterfacePtr<IDropDownListController> RefreshDropListCntrler(dropdownCtrlView, UseDefaultIID());
				if(RefreshDropListCntrler==NULL)
				{
					//CA("RefreshDropListCntrler==NULL");
					break;
				}
				RefreshDropListCntrler->Select(1); //0
				
				
				/*if (iPanelControlData != NULL)
				{	
					CA("1234");
					SDKListBoxHelper listBox(this, kUPDPluginID);				
					listBox.EmptyCurrentListBox(iPanelControlData, 1);
					rDataList.clear();
					OriginalrDataList.clear();
				}*/
	
			//	dropdownCtrlView->Disable();
				//refreshBtnCtrlView->Disable();
				////EditBoxCtrlView->Disable();
				//UPDReloadButtonView->Disable();
				////Yogesh
				////locateBtnCtrlView->Disable();
				//listControlView1->Disable();
				/*UPDActionComponent actionObsever(this);
				actionObsever.CloseUpdateRepositoryPalette();*/
				Flag1 = 1;
				break;
			}
			
			else
			{	//CA("6");
				dropdownCtrlView->Enable();
				refreshBtnCtrlView->Enable();
				UPDReloadButtonView->Enable();
				//Yogesh
				//locateBtnCtrlView->Enable();
				listControlView1->Enable();
				/*if(Mediator::selectedRadioButton==2)
                	EditBoxCtrlView->Enable();
				else
					EditBoxCtrlView->Disable();*/


				//24/01
				if(rDataList.size()>0)
						{
							//refreshBtnCtrlView->Enable();
							//locateBtnCtrlView->Enable();
							Mediator::UPDRefreshButtonView->Enable();
							//Mediator::UPDLocateButtonView->Enable();

						}
						else
						{
							//refreshBtnCtrlView->Disable();
							//locateBtnCtrlView->Disable();
							Mediator::UPDRefreshButtonView->Disable();
							//Mediator::UPDLocateButtonView->Disable();
						}

				//end 24/01


				if(FlgToCheckMenuAction)
				{	
					InterfacePtr<IDropDownListController> RefreshDropListCntrler(dropdownCtrlView, UseDefaultIID());
					if(RefreshDropListCntrler==NULL)
					{
						//CA("subSectionDropListCntrler");
						break;
					}
					RefreshDropListCntrler->Select(1); //0
					FlgToCheckMenuAction= kFalse;
					//EditBoxCtrlView->Disable();
					InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
					if (iPanelControlData==NULL)
					{
						//CA("iPanelControlData is NULL");
						break;
					}
					
					//Apsiva 9
					//SDKListBoxHelper listBox(this, kUPDPluginID);
					//listBox.EmptyCurrentListBox(iPanelControlData, 1);	// this is a False one.... 
					//UPDTreeDataCache dc;
					//dc.clearMap();
					//CA("Inside loadpalettedata");
				}

			}
			
/***************************************************************/
			
			if(result && iDoc)
			{  			
				//AttachWidget(Mediator::iPanelCntrlDataPtr, kUPDOptionsDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
				////	AttachWidget(Mediator::iPanelCntrlDataPtr, kTPLClassTreeIconSuiteWidgetID, IID_ITRISTATECONTROLDATA);
				//AttachWidget(Mediator::iPanelCntrlDataPtr, kUPDRefreshButtonWidgetID, IID_ITRISTATECONTROLDATA);
				////Added by Yogesh
				//AttachWidget(Mediator::iPanelCntrlDataPtr, kUPDLocateButtonWidgetID, IID_ITRISTATECONTROLDATA);

				//AttachWidget(Mediator::iPanelCntrlDataPtr, kUPDPageNumWidgetID, IID_ITEXTCONTROLDATA );

				IControlView* iControlView = Mediator::iPanelCntrlDataPtr->FindWidget(kUPDOptionsDropDownWidgetID);
				if (iControlView == NULL)
					break;
				
				InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
				if (iDropDownListController == NULL)
					break;

				if(Flag1 == 1)
				{					
					iDropDownListController->Select(1); //0
					Flag1 =0;					
				}
				else if(Flag1 == 0)
				{						
					InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
					if (iPanelControlData == NULL)
						break;					
				}

			}
			else
			{				
		//CA("10");
				/*UPDActionComponent actionObsever(this);
				actionObsever.CloseUpdateRepositoryPalette();*/
			}
		
		} while (kFalse);
	}
}

void UPDSelectionObserver::AutoDetach()
{
	Mediator::loadData=kFalse;
	//CA("UPDSelectionObserver::AutoDetach 1");
	ActiveSelectionObserver::AutoDetach();
	//CA("UPDSelectionObserver::AutoDetach 2");
	
	//CA("UPDSelectionObserver::AutoDetach 3");
	do
	{
		//InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		//if (iPanelControlData==NULL)
		//{
		//	//CA("iPanelControlData is NULL");
		//	return;
		//}
		DetachWidget(Mediator::iPanelCntrlDataPtr, kUPDOptionsDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		DetachWidget(Mediator::iPanelCntrlDataPtr, kUPDRefreshButtonWidgetID, IID_ITRISTATECONTROLDATA);
		//DetachWidget(Mediator::iPanelCntrlDataPtr, kUPDRefreshButtonWidgetID, IID_IBOOLEANCONTROLDATA);  // updated by vaibhav 25 Feb
	
		
		DetachWidget(Mediator::iPanelCntrlDataPtr, kUPDReloadIconWidgetID, IID_ITRISTATECONTROLDATA);
		//DetachWidget(Mediator::iPanelCntrlDataPtr, kUPDPageNumWidgetID, IID_ITEXTCONTROLDATA );
		DetachWidget(Mediator::iPanelCntrlDataPtr, kUPDSelectionBoxtWidgetID, IID_ITRISTATECONTROLDATA );

	} while (kFalse);
	
	//CA(" DocWchUtils::StopDocResponderMode ");
	DocWchUtils::StopDocResponderMode();

	
}

void UPDSelectionObserver::setTriState
(const WidgetID&  widgetID, ITriStateControlData::TriState state)
{
	do 
	{
		/*
		InterfacePtr<IPanelControlData> iPanelControlData(this, UseDefaultIID());
		if(iPanelControlData==NULL) 
			break;

		//Mediator::iPanelCntrlDataPtr=iPanelControlData;
		*/
		IControlView * iControlView=Mediator::iPanelCntrlDataPtr->FindWidget(widgetID);
		if(iControlView==NULL) 
			break;
		
		InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
		if(itristatecontroldata==NULL) 
			break;

		itristatecontroldata->SetState(state);
	} while(kFalse);	
}

void UPDSelectionObserver::Update
(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{
	ActiveSelectionObserver::Update(theChange, theSubject, protocol, changedBy);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*>(CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == NULL)
		return;
	
	do
	{	
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == NULL)
			return;	

		WidgetID theSelectedWidget = controlView->GetWidgetID();
		
		if(theSelectedWidget==kUPDOptionsDropDownWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			refreshStatus = -1;
			HandleDropDownListClick(theChange,theSubject,protocol,changedBy);
			break;
		}
		
	//	if(theSelectedWidget==kUPDPageNumWidgetID && protocol==IID_ITEXTCONTROLDATA )
	//	{
	//		bool16 result1 = ptrIAppFramework->getLoginStatus();
	//		if(!result1)
	//			return;
	//		
	//		IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();		 //Cs4
	//		if(iDoc== NULL) { 
	//			//CA("iDoc== NULL in kUPDPageNumWidgetID");
	//		return;
	//		}
	//		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
	//		if (iPanelControlData == NULL)
	//			break;

	//		IControlView * editBox =iPanelControlData->FindWidget(kUPDPageNumWidgetID);

	//		SDKListBoxHelper listBox(this, kUPDPluginID);//24/01
	//		InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());

	//		Mediator::EditBoxTextControlData=obj;
	//		PMString pageNumber;
	//		pageNumber=obj->GetString();
	//		if(!pageNumber.NumUTF16TextChars())
	//		{
	//			
	//			break;
	//		}
	//		
	//		Mediator::refreshPageNumber=pageNumber.GetAsNumber();
	//		Mediator::refreshPageNumber--;
	//		
	//		UID pageUID;
	//		Refresh refresh;
	//		if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
	//		{					
	//			//24/01
	//			SDKListBoxHelper listBox(this, kUPDPluginID);
	//			listBox.EmptyCurrentListBox(iPanelControlData, 1);
	//			
	//			Mediator::UPDRefreshButtonView->Disable();
	//			//Mediator::UPDLocateButtonView->Disable();
	//			//flagforinvalidpage = kTrue;

	//			rDataList.clear();
	//			OriginalrDataList.clear();
	//			//end 24/01
	//			CAlert::InformationAlert("Please enter a valid page number");
	//			break;
	//		} 
	//		
	//		Mediator::selectedRadioButton=2;
	//		refresh.GetPageDataInfo(Mediator::selectedRadioButton);

	//		if(GroupFlag == 3)	//Group by Element //if(GroupFlag)
	//		{
	////CA("GroupFlag == 3...543..UPDSelectionObserver.cpp");
	//			reSortTheListForElem();
	//			fillDataInListBox(/*kTrue*/3);
	//			Mediator::isGroupByObj=kFalse;
	//			isObjectGroup=kFalse;
	//			isElementoptSelected= kTrue;
	//		}
	//		else if(GroupFlag == 2) // Group By Object..
	//		{	//CA("isGroupByObj ==  kTrue...543..UPDSelectionObserver.cpp");
	//			reSortTheListForObject();
	//			fillDataInListBox(/*kFalse*/2);
	//			Mediator::isGroupByObj=kTrue;
	//			isObjectGroup = kTrue;
	//			isElementoptSelected = kFalse;
	//		}
	//		else if(GroupFlag == 1) //Added..//Unique Attributes in Doc
	//		{
	//			//CA("isGroupByObj ==  kTrue...543..UPDSelectionObserver.cpp");
	//			reSortTheListForUniqueAttr();
	//			fillDataInListBox(1);
	//			//Mediator::isGroupByObj=kTrue;
	//			//isObjectGroup=kFalse;
	//			//isElementoptSelected= kFalse;
	//		}
	//		break;
	//	}
			
		if(theSelectedWidget == kUPDRefreshButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kUPDRefreshButtonWidgetID");
			bool16 result1 = ptrIAppFramework->getLoginStatus();
			if(!result1)
				return;
			
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*>(CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
				return;
			IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();		 //Cs4
			if(iDoc== NULL) { 
				//CA("iDoc== NULL in kUPDRefreshButtonWidgetID");
			return;
			}
			AcquireWaitCursor awc;
			awc.Animate();
					
			//24/01
			//bool16 retVal=0;
			//retVal=ptrIAppFramework->TYPECACHE_clearInstance();
			//if(retVal==0)
			//{
			//	//CA("retVal==0");
			//	break;
			//}
			//end 24/01

			//isselected = kTrue; //24/01
			//24/01
		
			if(rDataList.size()<=0){
				//Mediator::UPDLocateButtonView->Disable();
				Mediator::UPDRefreshButtonView->Disable();
				return;
			}
			
			int32 dataListIndex =0;  //Addded....


			// Code to set check box selected in unique & rdatalist.

			if(UniqueNodeIds.size() > 0)
			{
			
				set<NodeID>::iterator it;
				for(it=UniqueNodeIds.begin(); it!=UniqueNodeIds.end(); it++ )
				{
					NodeID nid= (NodeID)*it;

					InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::listControlView, UseDefaultIID());
					if(!treeViewMgr)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::!treeViewMgr");					
						continue ;
					}
					//QueryWidgetFromNode 

					InterfacePtr<IPanelControlData> panelControlData(treeViewMgr->QueryWidgetFromNode(nid), UseDefaultIID());
					ASSERT(panelControlData);
					if(panelControlData==nil)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::panelControlData is nil");		
						continue ;
					}

					IControlView* checkBoxCntrlView=panelControlData->FindWidget(kUPDCheckBoxWidgetID);
					if(checkBoxCntrlView==nil) 
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::checkBoxCntrlView == nil");
						continue ;
					}
		
					InterfacePtr<ITriStateControlData> itristatecontroldata(checkBoxCntrlView, UseDefaultIID());
					if(itristatecontroldata==nil)
					{
						ptrIAppFramework->LogDebug("APJS9_RefreshContent::RfhSelectionObserver::HandleDropDownListClick::itristatecontroldata is nil");		
						continue ;
					}

					TreeNodePtr<IntNodeID>  uidNodeIDTemp(nid);

					int32 uid= uidNodeIDTemp->Get();
					int32 TextFrameuid = uidNodeIDTemp->Get();
					UPDDataNode rNode;
					UPDTreeDataCache dc1;

					dc1.isExist(uid, rNode);

					PMString fieldName = rNode.getFieldName();

					for(int32 k =0; k < UniqueDataList.size(); k ++)
					{
						if(UniqueDataList[k].name == fieldName)
						{
							if(itristatecontroldata->IsSelected())
								UniqueDataList[k].isSelected = kTrue;
							else
								UniqueDataList[k].isSelected = kFalse;

							for(int j=0; j<rDataList.size(); j++)
							{				
								if(UniqueDataList[k].elementID == rDataList[j].elementID /*&& UniqueDataList[k].TypeID ==  rDataList[j].TypeID*/ && UniqueDataList[k].whichTab == rDataList[j].whichTab)
								{	
									if((UniqueDataList[k].whichTab != 4 || UniqueDataList[k].isImageFlag == kTrue || UniqueDataList[k].isTableFlag == kTrue) && !(UniqueDataList[k].elementID == - 121 && UniqueDataList[k].isTableFlag == kFalse))
									{						
										if( UniqueDataList[k].TypeID !=  rDataList[j].TypeID)
											continue;							
									}
									if(UniqueDataList[k].whichTab == 4 && UniqueDataList[k].elementID == -101 ) // for Item Table in Tabbed text
									{						
										if( UniqueDataList[k].TypeID !=  rDataList[j].TypeID)
											continue;							
									}					
									rDataList[j].isSelected = UniqueDataList[k].isSelected;				
								}
							}
						}
					}
					
				}
			}
			
			
			//SDKListBoxHelper sList2(this, kUPDPluginID);

			for(int r=0 ; r< rDataList.size(); r++)
			{
				if(rDataList[r].isSelected == kTrue)
				{
					//CA("rDataList[r].isSelected == kTrue");
					if(rDataList[r].EndIndex == 0) //means current element isObject 
					{
						dataListIndex = r ; //Added....
						if(allElementSelectedFlag == kTrue  && GroupFlag != 2){  //Added...
							//CA("calling UpdateSignalRow	1");
							//sList2.UpdateSignalRow(Mediator::listControlView, dataListIndex , 1 );
							rDataList[r].iconDispayHint = 1;
						}
						else if(allElementSelectedFlag == kFalse && GroupFlag != 2){
							//sList2.UpdateSignalRow(Mediator::listControlView, dataListIndex,2 );	
							rDataList[r].iconDispayHint = 2;
						}	
						continue;
					}//end if..if

					
					BoxRefUID  = rDataList[r].BoxUIDRef; 
					LObjectID  = rDataList[r].objectID;
					LElementID = rDataList[r].elementID;
					//Added on 20/01/07 By Dattatray whih is used in UpdatetheCurrentSelection .
					getWhichTab = rDataList[r].whichTab;
					if(rDataList[r].childTag == 1)
						itemTypeId	= rDataList[r].childId;
					else
						itemTypeId	= rDataList[r].TypeID;
					
					TagList tList;
					//TagReader tReader;
					InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
					if(!itagReader){ 
							ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::Update::!itagReader");
						return ;
					}
					tList=itagReader->getTagsFromBoxForUpdate(BoxRefUID);		


					if(tList.size()==0){	
						tList=itagReader->getFrameTags(BoxRefUID);
						
						if(tList.size()==0){
							continue;
						}
					}
		
					int32 StartText = 0;	
					int32 EndText = 0;
					double ElemetsLangID = 0;
					double ElemetsSectionID = 0;
					bool16 isElementPresent = kFalse;


					/*PMString asd1("LElementID: ");
					asd1.AppendNumber(LElementID);
					CA(asd1);*/

					for(int i=0 ; i <  tList.size() ;i++){
						if((tList[i].elementId == LElementID)/* && (tList[i].typeId == itemTypeId)*/)
						{	
							PMString asd("tList[i].elementId: ");
							asd.AppendNumber(tList[i].elementId);
							//CA(asd);

							if(tList[i].childTag == 1)
							{
								if(tList[i].childId != itemTypeId)
									continue;
							}
							else if(tList[i].typeId != itemTypeId)
								continue;
							else if(tList[i].header == 1)
								continue;

							isElementPresent = kTrue;
							StartText = tList[i].startIndex/*+1*/;
							
							EndText = tList[i].endIndex/*-1*/;
							ElemetsLangID = tList[i].languageID;
							
							ElemetsSectionID = tList[i].sectionID;
							break;
						}
					}
		
					if(isElementPresent == kFalse)
					{
						//CA("Calling UpdateSignalRow	3");
						//SDKListBoxHelper sList1(this, kUPDPluginID);
						//sList1.UpdateSignalRow(Mediator::listControlView, r, 2 );
						rDataList[r].iconDispayHint = 2;
						continue;
					}
		
					InterfacePtr<IPMUnknown> unknown(BoxRefUID,IID_IUNKNOWN);

					if(!unknown)
					{
						continue ;
					}
		
		
					//InterfacePtr<IPMUnknown> unknown(BoxRefUID, IID_IUNKNOWN);					
					//if(!unknown)
     //               {
					//	//CA("No unknown prt available");
					//	continue;
					//}
					////UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
					////InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
					//UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
					UID textFrameUID = kInvalidUID;
					InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxRefUID, UseDefaultIID());
					if (graphicFrameDataOne) 
					{
						textFrameUID = graphicFrameDataOne->GetTextContentUID();
					}
					if (textFrameUID == kInvalidUID)
						break;
/*			//////CS3 Change
					InterfacePtr<ITextFrame> textFrame(BoxRefUID.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
					if (textFrame == NULL)
					{
						//CA("textFrame == NULL");					
						break;
					}
*/          
					//////added by Amit
					InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxRefUID, UseDefaultIID());
					if (graphicFrameHierarchy == nil) 
					{
						ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::Update::graphicFrameHierarchy == nil");
						return;
					}
									
					InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
					if (!multiColumnItemHierarchy) {
						ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::Update::multiColumnItemHierarchy == nil");
						return;
					}

					InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
					if (!multiColumnItemTextFrame) {
						ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::Update::multiColumnItemTextFrame == nil");
						//CA("Its Not MultiColumn");
						return;
					}
					InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
					if (!frameItemHierarchy) {
						ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::Update::frameItemHierarchy == nil");
						return;
					}

					InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
					if (!textFrame) {
						ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::Update::textFrame == nil");
						//CA("!!!ITextFrameColumn");
						return;
					}

					//// End
					TextIndex startIndex1 = textFrame->TextStart();
					
					InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
					if (textModel == NULL)
					{
						ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::Update::textModel == nil");
						//CA("textModel == NULL");
						return;
					}

					TextIndex finishIndex = startIndex1 + textModel->GetPrimaryStoryThreadSpan()-1;

					int32 tStart=0, tEnd=0;
					PMString entireStory("");

					CheckTag tagstory;
					//CA(StartText);
					//CA(EndText);

					if(!tagstory.GetTextstoryFromBox(textModel, StartText, EndText, entireStory))
					{
						CA("break....");
						break;
					}
					
					
					//CA(entireStory);
					/*char *storyLimits = entireStory.GrabCString();

					PMString FinalStory("");
					
					for(int32 j=StartText; j<=EndText; j++)
					{
						FinalStory.Append(storyLimits[j]);
					}

					LatestStory = FinalStory;*/
					
					LatestStory = entireStory;
					bool16 Updatereult = kFalse;				
					//CA("798 entireStory  :  "+entireStory);	
					Updatereult = this->UpdatetheCurrentSelection(LatestStory, r ,ElemetsLangID , ElemetsSectionID);
					if(Updatereult == kTrue)
					{
						//CA("Updatereult == kTrue");
						allElementSelectedFlag = kTrue;
						PMString GreenColour("C=75 M=5 Y=100 K=0");
						
						//SDKListBoxHelper sList1(this, kUPDPluginID);
						//sList1.UpdateSignalRow(Mediator::listControlView, r , 1);
						rDataList[r].iconDispayHint = 1;
								
					}
					else
					{
						//CA("Updatereult == kFalse");
						allElementSelectedFlag = kFalse;
					}
					
					for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
					{
						tList[tagIndex].tagPtr->Release();
					}

				}//end of if

			}//end of for
			//End Yogesh

			if(GroupFlag == 2) //added ... 09/09/07
			{
				//CA("GroupFlag == 2");
				bool16 GreenUpdateFlag = kFalse;

				for( int32 i= 0 ; i <  rDataList.size() ; i++)
				{
					GreenUpdateFlag = kFalse;
					if(rDataList[i].isSelected)
					{
						if(rDataList[i].EndIndex == 0)
						{
							for(int32 j = i+1; j < rDataList.size(); j++)
							{
								if(rDataList[j].EndIndex == 0){
									break;
								}
								if(rDataList[j].isElementUpdated){
									GreenUpdateFlag = kTrue;
									continue;
								}
								else{
									GreenUpdateFlag = kFalse;
									break;
								}
						
							}//End of inner for Loop
							if(GreenUpdateFlag){
								//CA("Updated ");
								//sList2.UpdateSignalRow(Mediator::listControlView, i , 1); //making Green i.e. Update Success
								rDataList[i].iconDispayHint = 1;
							}
							else{
								//CA("update fail");
								//sList2.UpdateSignalRow(Mediator::listControlView, i , 2);//Making Red i.e.Update Failure
								rDataList[i].iconDispayHint = 2;
							}
						
						}//End of if(rDataList[i].EndIndex == 0)

					
					}//end of if(rDataList[i].isSelected)
					else if( ! (rDataList[i].isSelected)) //When CheckBox is Not  Selected
					{
						//sList2.UpdateSignalRow(Mediator::listControlView, i , 0);//Making Radio Button White i.e.Normal
						rDataList[i].iconDispayHint = 0;
					}
				
				}
			}	

			InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::listControlView, UseDefaultIID());
			if(!treeViewMgr)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
				return ;
			}
			UPDTreeDataCache dc;
			dc.clearMap();

			UPDTreeModel pModel;
			PMString pfName("Root");
			pModel.setRoot(-1, pfName, 1);
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();

//++++++++
		}///End of if();;;


	
		
	//if (theSelectedWidget == kUPDLocateButtonWidgetID
	//			&& theChange == kTrueStateMessage)
	//{
	//	AcquireWaitCursor awc;
	//	awc.Animate();
	//			//24/01
	//			if(rDataList.size()<=0)
	//					{
	//						Mediator::UPDLocateButtonView->Disable();
	//						Mediator::UPDRefreshButtonView->Disable();
	//						return;
	//					}
	//				//end 24/01
	//		if(!GroupFlag)
	//			{
	//				for(int i=0 ; i<rDataList.size() ; i++)
	//					{								
	//						PMString Colourrr ("");
	//						if(1) 
	//						{									
	//								if(rDataList[i].EndIndex != 0)
	//									{										
	//										if(i== CurrentSelectedRowno)
	//											{
	//												this->ChangeColorOfText(i , Colourrr);
	//												break;// added 14/01
	//											}
	//									 }
	//						}
	//					

	//					}//for
	//			}
	//				else
	//				{
	//				//start added 17/01
	//					for(int i=0 ; i<ElementGroupList.size() ; i++)
	//						{	
	//							
	//							PMString Colourrr ("");
	//							
	//							if(1)     
	//								{	
	//									//CA("1"); 
	//									if(ElementGroupList[i].EndIndex != 0)
	//										{	
	//											
	//											if(i== CurrentSelectedRowno)
	//											{
	//												this->ChangeColorOfText(i , Colourrr);
	//												break;// added 14/01
	//											}
	//										}
	//								}
	//						}//for loop ended
	//			}//else ended
	//	
	//}//if ended
		if (theSelectedWidget == kUPDReloadIconWidgetID	&& theChange == kTrueStateMessage)
		{
	
			bool16 result1 = ptrIAppFramework->getLoginStatus();
			if(!result1)
				return;	
			
			IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();		 //Cs4
			if(iDoc== NULL) { 
				//CA("iDoc== NULL in kUPDReloadIconWidgetID");
			return;
			}
			if(refreshStatus == -1)
				return;

			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData == NULL)
					return;

			Refresh refresh;
			//SDKListBoxHelper listBox( this , kUPDPluginID );

			switch(refreshStatus)
			{
				// Awasthi swap 0 and 3
				case 0:	
					{	
						//Mediator::editBoxView->Disable();
						//Mediator::EditBoxTextControlData->SetString("");
						
						//24/01
						//listBox.EmptyCurrentListBox(iPanelControlData, 1);
						UPDTreeDataCache dc;
						dc.clearMap();
						rDataList.clear();
						OriginalrDataList.clear();
						Mediator::UPDRefreshButtonView->Disable();
						//Mediator::UPDLocateButtonView->Disable();
						//end 24/01
						//isElementoptSelected= kFalse;//C 24/01
						
					}
					break;
				case 1:
					{	
						
						Mediator::selectedRadioButton = 3;
						//Mediator::editBoxView->Disable();
						//Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						//fillDataInListBox(iPanelControlData);
						//Mediator::isGroupByObj=kTrue;
						//isObjectGroup=kTrue;
						
						if(GroupFlag ==3)                 //if(GroupFlag)
						{
							//CA("group flag == 3 Group by Element");
							reSortTheListForElem();
							fillDataInListBox(/*kTrue*/3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;
							isElementoptSelected= kTrue;							
						}
						else if(GroupFlag ==2)
						{	
							//CA("GroupFlag  ==2  Group By Object..");
							reSortTheListForObject();
							fillDataInListBox(/*kFalse*/2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag==1)  //Added  New From Here..
						{
							//CA("GroupFlag==1 Unique Attributes in Doc");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						}
					}
					break;
				case 2:
					{	
						//CA("In Case:2 ..UPDSelectionObserver.cpp..900");
						Mediator::selectedRadioButton = 1;
						//Mediator::editBoxView->Disable();
						//Mediator::EditBoxTextControlData->SetString("");
						/*InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
						if (layoutData == NULL)
							break;
						UID pageUID = layoutData->GetPage();
						if(pageUID == kInvalidUID)
							break;
						IGeometry* spreadItem = layoutData->GetSpread();
						if(spreadItem == NULL)
							break;
						InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
						if (iSpread == NULL)
							break;
						int pageNum=iSpread->GetPageIndex(pageUID);
						Mediator::refreshPageNumber=pageNum;	*/				
						
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
	
						/*fillDataInListBox(iPanelControlData);
						Mediator::isGroupByObj=kTrue;
						isObjectGroup=kTrue;

						isElementoptSelected= kFalse;
						//CA("case 2 selected");*/

						if(GroupFlag  == 3)		//Group by Element							// if(GroupFlag) //Commented By Sachin..
						{
							//CA("GroupFlag ==3 Group by Element");
							reSortTheListForElem();
							fillDataInListBox(/*kTrue*/3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;

							isElementoptSelected= kTrue;
						
						}
						else if(GroupFlag  == 2)	/// Group By Object..
						{	
							//CA("GroupFlag ==2 Group By Object..");
							reSortTheListForObject();
							fillDataInListBox(/*kFalse*/2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag == 1)  //Added..For,Unique Attributes in Doc
						{
							//CA("GroupFlag==1 Unique Attributes in Doc");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						}
					}
					break;

				//case 3:
				//	{	
				//		//CA("case 3: UPDSElectionObserver.cpp .958");	
				//		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
				//		if (iPanelControlData == NULL)
				//			break;

				//		IControlView * editBox =iPanelControlData->FindWidget(kUPDPageNumWidgetID);
				//		SDKListBoxHelper listBox(this, kUPDPluginID);
				//		InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());							

				//		Mediator::EditBoxTextControlData=obj;
				//		PMString pageNumber;
				//		pageNumber=obj->GetString();
				//		if(!pageNumber.NumUTF16TextChars())
				//		{
				//			CAlert::InformationAlert("Please enter the Page Number you want to Update");
				//			break;
				//		}
				//		
				//		Mediator::refreshPageNumber=pageNumber.GetAsNumber();
				//		Mediator::refreshPageNumber--;
			
				//		UID pageUID;
				//		Refresh refresh;
				//		if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
				//		{					
				//			SDKListBoxHelper listBox(this, kUPDPluginID);
				//			listBox.EmptyCurrentListBox(iPanelControlData, 1);
				//			
				//			Mediator::UPDRefreshButtonView->Disable();

				//			rDataList.clear();
				//			OriginalrDataList.clear();
				//			CAlert::InformationAlert("Please enter a valid page number");
				//			break;
				//		} 
				//		
				//		Mediator::selectedRadioButton=2;
				//		refresh.GetPageDataInfo(Mediator::selectedRadioButton);

				//			if(GroupFlag == 3)	//Group by Element					//if(GroupFlag)
				//			{
				//				//CA("GroupFlag ==3 Group by Element");
				//				reSortTheListForElem();
				//				fillDataInListBox(/*kTrue*/3);
				//				Mediator::isGroupByObj=kFalse;
				//				isObjectGroup=kFalse;
				//				isElementoptSelected= kTrue;
				//			}
				//			else if(GroupFlag ==2) // Group By Object..
				//			{	
				//				//CA("GroupFlag ==2 Group by Object..1015");
				//				reSortTheListForObject();
				//				fillDataInListBox(/*kFalse*/2);
				//				Mediator::isGroupByObj=kTrue;
				//				isObjectGroup=kTrue;
				//				isElementoptSelected= kFalse;
				//			}
				//			else if(GroupFlag == 1)//Unique Attributes in Doc
				//			{
				//				//CA("GroupFlag==1 Unique Attributes in Doc..1025");
				//				reSortTheListForUniqueAttr();
				//				fillDataInListBox(1);
				//			}

				//			//Mediator::selectedRadioButton=2;
				//			//listBox.EmptyCurrentListBox(iPanelControlData, 1);
				//			//IControlView * editBox =iPanelControlData->FindWidget(kUPDPageNumWidgetID);

				//			//editBox->Enable();
				//			//
				//			//InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());

				//			//obj->SetString("");
				//			//
				//			//rDataList.clear();
				//			//OriginalrDataList.clear();

				//			//iPanelControlData->SetKeyboardFocus(kUPDPageNumWidgetID);
				//			//
				//			//Mediator::UPDRefreshButtonView->Disable();
				//			//Mediator::isGroupByObj=kTrue;
				//			//isObjectGroup=kTrue;
				//			//isElementoptSelected= kFalse;
				//			//
				//		}					
				//		break;

				case 3:	//case 4:
					{
						Mediator::selectedRadioButton = 4;
						//Mediator::editBoxView->Disable();
						//Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						/*fillDataInListBox(iPanelControlData);
						Mediator::isGroupByObj=kTrue;
						isObjectGroup=kTrue;
						isElementoptSelected= kFalse;*/
						//CA("case 4 selected");

							if(GroupFlag == 3)	//Group by Element				//if(GroupFlag)
							{
								//CA("groupflag == 3..1069 ");
								reSortTheListForElem();
								fillDataInListBox(/*kTrue*/3);
								Mediator::isGroupByObj=kFalse;
								isObjectGroup=kFalse;

								isElementoptSelected= kTrue;
							}
							else if(GroupFlag == 2)// Group By Object..
							{
								//CA("GroupFlag ==2..1079");
								reSortTheListForObject();
								fillDataInListBox(/*kFalse*/2);
								Mediator::isGroupByObj=kTrue;
								isObjectGroup=kTrue;

								isElementoptSelected= kFalse;

							}
							else if(GroupFlag == 1)  //Added..For,Unique Attributes in Doc
							{
								//CA("GroupFlag == 1..1090");
								reSortTheListForUniqueAttr();
								fillDataInListBox(1);
							
							}
					}
					break;
				// End Awasthi Swap
			}
		}



/////////////////////////////////////////////////////////////////////////upto here

//Added by Nitin for select and deselect all (CheckBox) on 01/09/07 From here to///////////
		if(theSelectedWidget == kUPDSelectionBoxtWidgetID && ((theChange ==kFalseStateMessage )||(theChange == kTrueStateMessage)) && protocol == IID_ITRISTATECONTROLDATA)
		{			
			//if(SelectBoxFlage==kTrue)
			//{
			//	//CA("SelectBoxFlage==kTrue");
			//	SelectBoxFlage=kFalse;
			//	break;
			//}
			//SelectBoxFlage=kFalse;
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				//CA("ptrIAppFramework == nil");
				break;
			}
			//SDKListBoxHelper sList(this, kUPDPluginID);			
			/*InterfacePtr<IListBoxController> listCntl(Mediator::listControlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
			{
				ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::Update::IListBoxController is nil");
				break;
			}			
			listCntl->Select(0,kTrue,kTrue);*/
			//K2Vector<int32> curSelection ;			
			//listCntl->GetSelected(curSelection ) ;			
			//const int kSelectionLength =  curSelection.Length();			
			//if(kSelectionLength<=0)	
			//{
			//	//CA("kSelectionLength<=0");
			//	break;
			//}
			IControlView * iControlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDSelectionBoxtWidgetID);
			if(iControlView==nil) 
			{
				ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::Update::iControlView is nil");		
				break;
			}
			InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
			if(itristatecontroldata==nil)
			{
				ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::Update::itristatecontroldata is nil");				
				break;
			}
			/*InterfacePtr<IListControlData> listControlData(Mediator::listControlView, UseDefaultIID());
			if(listControlData==nil) 
			{
				ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::Update::listControlData is nil");
				break;
			}*/
			if(theChange==kTrueStateMessage)
			{		
				if(GroupFlag != 1)
				{
					int32 i=0;
					while(i<rDataList.size())
					{
						rDataList[i].isSelected=kTrue;
						//sList.CheckUncheckRow(Mediator::listControlView, i, kTrue);
						i++;
					}				
				}
				else
				{
					//CA("GroupFlag == 1");
					int32 i=0;
					while(i<UniqueDataList.size())
					{
						rDataList[i].isSelected=kTrue;
						UniqueDataList[i].isSelected=kTrue;
						//sList.CheckUncheckRow(Mediator::listControlView, i, kTrue);
						i++;
					}
				}
			}
			else
			{
				//for thechange is false	/////////			
				if(GroupFlag != 1)
				{						
					int32 i=0;
					while(i<rDataList.size())
					{
						rDataList[i].isSelected=kFalse;
						//sList.CheckUncheckRow(Mediator::listControlView, i, kFalse);
						i++;
					}
				}
				else
				{
					int32 i=0;
					while(i<UniqueDataList.size())
					{
						rDataList[i].isSelected=kFalse;
						UniqueDataList[i].isSelected=kFalse;
						//sList.CheckUncheckRow(Mediator::listControlView, i, kFalse);
						i++;
					}
				}
			}

			InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::listControlView, UseDefaultIID());
			if(!treeViewMgr)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
				return ;
			}
			UPDTreeDataCache dc;
			dc.clearMap();

			UPDTreeModel pModel;
			PMString pfName("Root");
			pModel.setRoot(-1, pfName, 1);
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();

		}
///////////////////////upto here////////////////////////
	} while (kFalse);
}

void UPDSelectionObserver::UpdatePanel()
{
	//CA("updatepanel");
	do
	{
		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData == NULL)
			break;

		IControlView* iControlView = iPanelControlData->FindWidget(kUPDOptionsDropDownWidgetID);
		if (iControlView == NULL)
			break;

		InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
		if (iDropDownListController == NULL)
			break;
		InterfacePtr<IStringListControlData> iStringListControlData(iDropDownListController, UseDefaultIID());
		if (iStringListControlData == NULL)
			break;
	}while(kFalse);
}

IPanelControlData* UPDSelectionObserver::QueryPanelControlData()
{
	IPanelControlData* iPanel = NULL;
	do
	{
		InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID());
		if (iWidgetParent == NULL)
			break;

		InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
		if (iPanelControlData == NULL)
			break;
		iPanelControlData->AddRef();
		iPanel = iPanelControlData;
	}
	while (false); 
	return iPanel;
}

void UPDSelectionObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == NULL)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == NULL)
			break;
		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); 
}

void UPDSelectionObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == NULL)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == NULL)
			break;
		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}

void UPDSelectionObserver::HandleDropDownListClick
(const ClassID& theChange, 
ISubject* theSubject, 
const PMIID& protocol, 
void* changedBy)
{
	do
	{	
		
		AcquireWaitCursor awc;
		awc.Animate();
		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*>(CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		return;

		InterfacePtr<IControlView> iControlView(theSubject, UseDefaultIID());
		if (iControlView == NULL)
			break;
		WidgetID widgetID = iControlView->GetWidgetID();

		if (widgetID.Get() == kUPDOptionsDropDownWidgetID)
		{	

			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData == NULL)
				break;
			IControlView* iControlView = iPanelControlData->FindWidget(kUPDOptionsDropDownWidgetID);
			if (iControlView == NULL)
				break;
			InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
			if (iDropDownListController == NULL)
				break;

			int32 selectedRowIndex=0; 
			selectedRowIndex = iDropDownListController->GetSelected();
			refreshStatus = selectedRowIndex;
			Refresh refresh;
			//SDKListBoxHelper listBox(this, kUPDPluginID);


			switch(selectedRowIndex)
			{
				// Awasthi swap 0 and 3
				case 0:	
					{	//CA("case 0");
						Mediator::UPDReloadButtonView->Disable();
						//Mediator::editBoxView->Disable();
						//Mediator::EditBoxTextControlData->SetString("");
						
						//24/01
						//listBox.EmptyCurrentListBox(iPanelControlData, 1);
						UPDTreeDataCache dc;
						dc.clearMap();
						rDataList.clear();
						OriginalrDataList.clear();
						Mediator::UPDRefreshButtonView->Disable();

						InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::listControlView, UseDefaultIID());
						if(!treeViewMgr)
						{
							ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
							return ;
						}
					
						UPDTreeModel pModel;
						PMString pfName("Root");
						pModel.setRoot(-1, pfName, 1);
						treeViewMgr->ClearTree(kTrue);
						pModel.GetRootUID();
						treeViewMgr->ChangeRoot();

						
					}
					break;
				case 1:
					{	
						bool16 result1 = ptrIAppFramework->getLoginStatus();
						if(!result1)
							return;
						IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();		 //Cs4
						if(iDoc== NULL) { 
							//CA("iDoc== NULL in Handle Drop Down ---1");
						return;
						}
						//CA("case 1");
						Mediator::UPDReloadButtonView->Enable();
						Mediator::selectedRadioButton = 3;
						//Mediator::editBoxView->Disable();
						//Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);

						
			            if(GroupFlag == 3)						                //if(GroupFlag)
						{
							//CA("GroupFlag == 3..1257");
							reSortTheListForElem();
							//CA("after reSortTheListForElem");
							fillDataInListBox(/*kTrue*/3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;

							isElementoptSelected= kTrue;
						
						}
						else if(GroupFlag == 2)
						{	
							//CA("GroupFlag == 2..1265");
							reSortTheListForObject();
							//CA("after reSortTheListForElem");
							fillDataInListBox(/*kFalse*/2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;

							isElementoptSelected = kFalse;
						}
						else if(GroupFlag == 1)
						{
							
							reSortTheListForUniqueAttr();
							
							fillDataInListBox(1);
						}
						
					}
					break;
				case 2:
					{	
						//CA("case 2:");
						bool16 result1 = ptrIAppFramework->getLoginStatus();
						if(!result1)
							return;

						IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument(); //cs4		
						if(iDoc== NULL) { 
							//CA("iDoc== NULL in Handle Drop Down --- 2");
						return;
						}

						Mediator::UPDReloadButtonView->Enable();
						Mediator::selectedRadioButton = 1;
						//Mediator::editBoxView->Disable();
						//Mediator::EditBoxTextControlData->SetString("");
						/*InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
						if (layoutData == NULL)
							break;
						UID pageUID = layoutData->GetPage();
						if(pageUID == kInvalidUID)
							break;
						IGeometry* spreadItem = layoutData->GetSpread();
						if(spreadItem == NULL)
							break;
						InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
						if (iSpread == NULL)
							break;
						int pageNum=iSpread->GetPageIndex(pageUID);
						Mediator::refreshPageNumber=pageNum;	*/				
						
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
	
						/*fillDataInListBox(iPanelControlData);
						Mediator::isGroupByObj=kTrue;
						isObjectGroup=kTrue;

						isElementoptSelected= kFalse;
						//CA("case 2 selected");*/

						if(GroupFlag  == 3)                         //if(GroupFlag)
						{
							//CA("GroupFlag  == 3..1327");
							reSortTheListForElem();
							fillDataInListBox(/*kTrue*/3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;

							isElementoptSelected= kTrue;
						
						}
						else if(GroupFlag  == 2)
						{	//CA("GroupFlag  == 2..1337");
							reSortTheListForObject();
							fillDataInListBox(/*kFalse*/2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;

							isElementoptSelected= kFalse;


						}
						else if(GroupFlag == 1)//Added...
						{
							//CA("GroupFlag  == 1..1349");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						}

					}
					break;


				//case 3:
				//	{	
				//		bool16 result1 = ptrIAppFramework->getLoginStatus();
				//		if(!result1)
				//			return;

				//		IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();		 //Cs4
				//		if(iDoc== NULL) { 
				//			//CA("iDoc== NULL in Handle Drop Down --- 3");
				//		return;
				//		}

				//		Mediator::UPDReloadButtonView->Enable();
				//		Mediator::selectedRadioButton=2;
				//		listBox.EmptyCurrentListBox(iPanelControlData, 1);
				//		IControlView * editBox =iPanelControlData->FindWidget(kUPDPageNumWidgetID);

				//		//24.01
				//		/*if(!editBox)
				//		{
				//			CA("EditBox is Null");
				//			break;
				//		}*/
				//		//end 24.01

				//		editBox->Enable();
				//		//editBox->Hilite ();//C 24/01
				//		
				//		InterfacePtr<ITextControlData> obj(editBox, UseDefaultIID());

				//		//24.01
				//		/*if(!obj)
				//		{
				//			CA("editbox data is NU+++++++++++++++++++++++++++++++++++65LL");
				//				break;
				//		}*/
				//		//end 24.01


				//		obj->SetString("");
				//		
				//		rDataList.clear();
				//		OriginalrDataList.clear();

				//		iPanelControlData->SetKeyboardFocus(kUPDPageNumWidgetID);
				//		
				//		//24/01
				//		//Mediator::UPDLocateButtonView->Disable();
				//		Mediator::UPDRefreshButtonView->Disable();
				//		//end 24/01
				//	//	PMString pageNumber;
				//	//	pageNumber=obj->GetString();
				//	//	if(!pageNumber.NumUTF16TextChars())
				//	//	{
				//	//	//	CAlert::InformationAlert("Please enter the Page Number you want to refresh");
				//	//		break;
				//	//	}
				//	//	
				//	//	Mediator::refreshPageNumber=pageNumber.GetAsNumber();
				//	//	Mediator::refreshPageNumber--;
				//	//	
				//	//	UID pageUID;
				//	//	
				//	//	if(!refresh.isValidPageNumber(Mediator::refreshPageNumber, pageUID))
				//	//	{	CA("1");
				//	//		CAlert::InformationAlert("Please enter a valid page number");
				//	//		break;
				//	//	} 
				//	//	
				//	//	//Try to set the spread as the current spread

				//	//	if(!setSpreadFromPage(pageUID))
				//	//	{
				//	//		CAlert::InformationAlert("Some unknown error occured");
				//	//		break;
				//	//	}
				//	//	
				//	//	InterfacePtr<ILayoutControlData> layoutData(::QueryFrontLayoutData());
				//	//	if (layoutData == NULL)
				//	//		break;
				//	//	
				//	//	if(pageUID == kInvalidUID)
				//	//		break;
				//	//	
				//	//	IGeometry* spreadItem = layoutData->GetSpread();
				//	//	if(spreadItem == NULL)
				//	//		break;
				//	////	CA("Step1Valid12");
				//	//	InterfacePtr<ISpread> iSpread(spreadItem, UseDefaultIID());
				//	//	if (iSpread == NULL)
				//	//		break;
				//	////	CA("Step1Valid13");
				//	//	int pageNum=iSpread->GetPageIndex(pageUID);
				//	//	
				//	//	Mediator::refreshPageNumber=pageNum;//This is the actual page position
				//	//							
				//	//	refresh.GetPageDataInfo(Mediator::selectedRadioButton);
				//		
				//	//	fillDataInListBox(iPanelControlData);
				//		Mediator::isGroupByObj=kTrue;
				//		isObjectGroup=kTrue;
				//		isElementoptSelected= kFalse;
				//		
				//	}					
				//	break;

				case 3:  //case 4:
					{
						bool16 result1 = ptrIAppFramework->getLoginStatus();
						if(!result1)
							return;

						IDocument *iDoc=/*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();		//Cs4
						if(iDoc== NULL) { 
							//CA("iDoc== NULL in Handle Drop Down ----- 4");
						return;
						}
						Mediator::UPDReloadButtonView->Enable();
						Mediator::selectedRadioButton = 4;
						//Mediator::editBoxView->Disable();
						//Mediator::EditBoxTextControlData->SetString("");
						refresh.GetPageDataInfo(Mediator::selectedRadioButton);
						
						/*fillDataInListBox(iPanelControlData);
						Mediator::isGroupByObj=kTrue;
						isObjectGroup=kTrue;
						isElementoptSelected= kFalse;*/
						//CA("case 4 selected");

						if(GroupFlag  == 3)					//if(GroupFlag)
						{
							//CA("GroupFlag  == 3..1489");
							reSortTheListForElem();
							fillDataInListBox(/*kTrue*/3);
							Mediator::isGroupByObj=kFalse;
							isObjectGroup=kFalse;

							isElementoptSelected= kTrue;
						}
						else if(GroupFlag ==2)
						{
							//CA("GroupFlag  == 2..1504");
							reSortTheListForObject();
							fillDataInListBox(/*kFalse*/2);
							Mediator::isGroupByObj=kTrue;
							isObjectGroup=kTrue;
							isElementoptSelected= kFalse;
						}
						else if(GroupFlag == 1 )
						{
							//CA("GroupFlag  == 1..1508");
							reSortTheListForUniqueAttr();
							fillDataInListBox(1);
						}
					}
					break;
				// End Awasthi Swap
			}
		}

		/*if (widgetID.Get() == kUPDRefreshButtonWidgetID)
		{	
			if(!isObjectGroup)
				reSortTheListForObject();
			Refresh refresh;
			refresh.doRefresh(Mediator::selectedRadioButton);
		}*/

		//Yogesh
		//if (widgetID.Get() == kUPDLocateButtonWidgetID)
		//{	
		//	/*if(!isObjectGroup)
		//		reSortTheListForObject();
		//	Refresh refresh;
		//	refresh.doRefresh(Mediator::selectedRadioButton);
		//	CA("Locate");*/
		//}

	}while(false); 
	allElementSelectedFlag =kFalse;//Added
}

//delete it afterwards
//bool16 UPDSelectionObserver::shouldRefresh(const TagStruct& tInfo, const UID& boxID, bool16 isTaggedFrame)
//{
//	for(int i=0; i<rDataList.size(); i++)
//	{
//		if(/*boxID==rDataList[i].boxID &&*/ rDataList[i].isSelected && (!rDataList[i].isObject || rDataList[i].isTableFlag==kTrue))
//		{
//			if(!isTaggedFrame)
//			{
//				if(rDataList[i].elementID==tInfo.elementId && rDataList[i].objectID==tInfo.parentId)// && rDataList[i].publicationID==tInfo.sectionID)//Later make changes here
//				{
//					return kTrue;
//				}
//			}
//			else
//				if(rDataList[i].objectID==tInfo.parentId && rDataList[i].TypeID ==tInfo.typeId )// && rDataList[i].publicationID==tInfo.sectionID)//Later make changes here
//				{
//					return kTrue;
//				}
//				
//		}
//	}
//	
//	return kFalse;
//}
//delete it afterwards
bool16 UPDSelectionObserver::fillDataInListBox(InterfacePtr<IPanelControlData> panelControlData, bool16 isGroupByElem)
{	
	//CA("Inside fillDataInListBox");
	if(!Mediator::listControlView)
		return kFalse;
	
	//SDKListBoxHelper listBox(this, kUPDPluginID);
	//listBox.EmptyCurrentListBox(panelControlData, 1);

	if(rDataList.size()>0)
	{
		Mediator::UPDRefreshButtonView->Enable();
		//Mediator::UPDLocateButtonView->Enable();

	}
	else
	{
		Mediator::UPDRefreshButtonView->Disable();
		//Mediator::UPDLocateButtonView->Disable();

	}
		

	for(int i=0; i<rDataList.size(); i++)
	{

		PMString objectName;
		if(!isGroupByElem){

			if(rDataList[i].isObject){	
				objectName= rDataList[i].name;				
			}
			else{	
				objectName="        " + rDataList[i].name;				
			}
			
		}
		else{
			if(rDataList[i].isObject){
				objectName="        " + rDataList[i].name;				
			}
			else{
				objectName= rDataList[i].name;		
			}
		}

		/*objectName+=" , ";
		objectName.AppendNumber(rDataList[i].boxID.Get());
		objectName+=" , ";
		objectName.AppendNumber(rDataList[i].elementID);
		objectName+=" , ";
		objectName.AppendNumber(rDataList[i].objectID);
		objectName+=" , ";
		objectName.AppendNumber(rDataList[i].publicationID);
		*/
		//listBox.AddElement(Mediator::listControlView, objectName, kUPDPanelTextWidgetID, i, kTrue);
		//listBox.CheckUncheckRow(Mediator::listControlView, i, kTrue);
		rDataList[i].isSelected=kTrue;
		rDataList[i].isProcessed=kFalse;
	}
	return kTrue;
}

bool16 UPDSelectionObserver::setSpreadFromPage(const UID& pageUID)
{
	IDocument* doc = /*::GetFrontDocument()*/Utils<ILayoutUIUtils>()->GetFrontDocument();  //Cs4
		//24.01
		if(!doc)
		{
			//CA("No front document open");
			return kFalse;
		}
		//end 24.01

	IDataBase* db= ::GetDataBase(doc); 

	//24.01
		if(!db)
		{
			//CA("No database available");
			return kFalse;
		}
		//end 24.01

	InterfacePtr<ISpreadList> spreadList(doc, IID_ISPREADLIST); 

		//24.01
	if(!spreadList)
	{
		//CA("No spreadList available");
		return kFalse;
	}
	//end 24.01



	int32 spreadNo = -1; 

	for(int32 i=0; i<spreadList->GetSpreadCount(); ++i) 
	{ 
		UIDRef spreadRef(db, spreadList->GetNthSpreadUID(i)); 
		InterfacePtr<ISpread> spread(spreadRef, IID_ISPREAD);
		//24.01
		if(!spread)
		{
			//CA("No spread available");
			continue;
		}
		//end 24.01
		if(spread->GetPageIndex(pageUID)!= -1) 
		{	
			spreadNo = i; 
			break; 
		}
	}

	if(spreadNo==-1)
		return kFalse;
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); //Cs4
	if (layoutData == NULL)
		return kFalse;
//	layoutData->SetSpread(spreadList->QueryNthSpread(spreadNo),kFalse);
	return kTrue;
}

bool16 UPDSelectionObserver::reSortTheListForElem(void)//We want to group by element
{
	//CA("Inside the reSortTheListForElem");
	//if(isElementoptSelected!= kFalse)
	//		return kTrue;

	//RefreshDataList theDataList=rDataList;
	//rDataList.clear();
	///*int32 length = theDataList.size();
	//PMString len("");
	//len.AppendNumber(length);
	//CA(len);*/
	//
	//for(int i=0; i<theDataList.size(); i++)
	//{
	//	if(theDataList[i].isObject || theDataList[i].isProcessed)
	//		continue;

	//	theDataList[i].isProcessed=kTrue;

	//	rDataList.push_back(theDataList[i]);//Added the element

	//	RefreshData nodeToAdd;

	//	for(int j=0; j<theDataList.size(); j++)//Now adding the objects which have these elements
	//	{	
	//		if(theDataList[j].isObject)
	//		{
	//			nodeToAdd=theDataList[j];
	//			continue;
	//		}
	//		if((theDataList[i].isTableFlag!=1 && theDataList[i].isImageFlag!=1 ) && theDataList[i].whichTab==theDataList[j].whichTab)
	//		{
	//			
	//			if(theDataList[i].elementID==theDataList[j].elementID && theDataList[j].objectID==nodeToAdd.objectID && theDataList[i].whichTab==theDataList[j].whichTab && theDataList[i].TypeID==theDataList[j].TypeID)
	//			{
	//				
	//				rDataList.push_back(nodeToAdd);//Added the Object
	//				theDataList[j].isProcessed=kTrue;
	//			}
	//		}
	//		else
	//		{
	//			
	//			if(theDataList[i].TypeID==theDataList[j].TypeID && theDataList[j].objectID==nodeToAdd.objectID && theDataList[i].whichTab==theDataList[j].whichTab)
	//			{
	//				rDataList.push_back(nodeToAdd);//Added the Object
	//				theDataList[j].isProcessed=kTrue;
	//			}
	//		}
	//	}
	//}
	//isElementoptSelected = kTrue;
	//return kTrue;
	rDataList = OriginalrDataList;
	RefreshDataList theDataList=rDataList;
	rDataList.clear();
	ElementGroupList.clear();



	for(int i=0; i< theDataList.size(); i++)
	{
		if(theDataList[i].isObject || theDataList[i].isProcessed){
			continue;
		}

		theDataList[i].isProcessed=kTrue;
//***Added...1/09/07
		theDataList[i].isSelected = kTrue;
//***Up To Here
		rDataList.push_back(theDataList[i]);//Added the element
// Changed by Rahul		
		RefreshData ElementToAdd;
		ElementToAdd =theDataList[i];
//***Added...1/09/07
		ElementToAdd.isSelected = kTrue;
//***Up To Here
		//ElementToAdd.isObject= kTrue;
		ElementToAdd.EndIndex=0;
		ElementGroupList.push_back(ElementToAdd);
		
		
		PMString ObjectName("");
		RefreshData nodeToAdd;

		for(int j=0; j < theDataList.size(); j++)//Now adding the objects which have these elements
		{	
			if(theDataList[j].isObject)
			{					
				ObjectName.Clear();
				nodeToAdd=theDataList[j];
//***Added From Here 
				nodeToAdd.isSelected = kTrue;
//Up To Here
				ObjectName= theDataList[j].name;
				continue;
			}
			if((theDataList[i].isTableFlag!=1 && theDataList[i].isImageFlag!=1 ) && theDataList[i].whichTab==theDataList[j].whichTab)
			{

				if(theDataList[i].elementID==theDataList[j].elementID && theDataList[j].objectID==nodeToAdd.objectID && theDataList[i].whichTab==theDataList[j].whichTab /*&& theDataList[i].TypeID==theDataList[j].TypeID //Commented On 1/09/07*/)
				{
	//Ok...		
				//////////	rDataList.push_back(nodeToAdd);//Added the Object
				//////////	theDataList[j].isProcessed=kTrue;

				//////////// changed by Rahul
				//////////	RefreshData ElementToAdd1;
				//////////	ElementToAdd1 = theDataList[j];
				//////////	ElementToAdd1.name = ObjectName;

				//////////	ElementGroupList.push_back(ElementToAdd1);

//****Added.From Here					
					if(theDataList[i].whichTab != 4 )
					{
						//CA("If not Item");	
						if( theDataList[i].TypeID!=theDataList[j].TypeID)
							continue;
					}
					if(theDataList[i].whichTab == 4 && theDataList[i].elementID == -101 ) // for Item Table in Tabbed text
					{
						//CA("If Object");
						if( theDataList[i].TypeID != theDataList[j].TypeID)
							continue;
					}

					
					//rDataList.push_back(nodeToAdd);//Added the Object
					theDataList[j].isProcessed=kTrue;

				// changed by Rahul
					RefreshData ElementToAdd1;
					ElementToAdd1 = theDataList[j];
					ElementToAdd1.isSelected = kTrue;
					ElementToAdd1.name = ObjectName;
					
					ElementGroupList.push_back(ElementToAdd1);
					nodeToAdd.elementID = ElementToAdd1.elementID;
					rDataList.push_back(nodeToAdd);//Added the Object
//****Up to Here.

				
				}
			}
			else
			{	

				if(theDataList[i].TypeID==theDataList[j].TypeID && theDataList[j].objectID==nodeToAdd.objectID && theDataList[i].whichTab==theDataList[j].whichTab)
				{

					//rDataList.push_back(nodeToAdd);//Added the Object  //Commeneted by Sachin sharma 1/09/07
					theDataList[j].isProcessed=kTrue;

				// Changed by Rahul
					RefreshData ElementToAdd1;
					ElementToAdd1 = theDataList[j];
//** Added..
					ElementToAdd1.isSelected = kTrue;
//Up To Here
					ElementToAdd1.name = ObjectName;
//******Added
					nodeToAdd.elementID = ElementToAdd1.elementID;
					rDataList.push_back(nodeToAdd);//Added the Object
//***** Up To Here
					ElementGroupList.push_back(ElementToAdd1);
				//

				}
			}
		}
	}
	isElementoptSelected = kTrue;
	Mediator::isGroupByObj=kFalse;

	return kTrue;
}


bool16 UPDSelectionObserver::reSortTheListForObject(void)//Only to be called when the list has been resorted as Element
{
	//CA("Inside the reSortTheListForObject");
	//if(isElementoptSelected== kFalse)
	//	return kTrue;
	//RefreshDataList theDataList=rDataList;
	//rDataList.clear();
	///*int32 length = theDataList.size();
	//PMString len("");
	//len.AppendNumber(length);
	//CA(len);*/
	//for(int i=0; i<theDataList.size(); i++)
	//{
	//	if(!theDataList[i].isObject || theDataList[i].isProcessed)
	//		continue;
	//	
	//	theDataList[i].isProcessed=kTrue;

	//	RefreshData referenceNode=theDataList[i];

	//	rDataList.push_back(theDataList[i]);//Added the Object

	//	RefreshData nodeToAdd;

	//	for(int j=0; j<theDataList.size();j++)//Now adding the elements which have these elements
	//	{	
	//		if(!theDataList[j].isObject)
	//		{
	//			nodeToAdd=theDataList[j];
	//			continue;
	//		}
	//		
	//		if(theDataList[i].objectID==theDataList[j].objectID)
	//		{
	//			nodeToAdd.objectID=referenceNode.objectID;
	//			nodeToAdd.isSelected=theDataList[j].isSelected;

	//			rDataList.push_back(nodeToAdd);//Added the Element
	//			theDataList[j].isProcessed=kTrue;
	//		}
	//	}
	//}
	//isElementoptSelected = kFalse;
	//return kTrue;
//CA(__FUNCTION__);
//******Added..
	
	//allElementSelectedFlag = kFalse;
//*****
		rDataList = OriginalrDataList;
		RefreshDataList theDataList=rDataList;

		rDataList.clear();
		RefreshDataList CurrentObjectDataList ;

		for(int i=0; i < theDataList.size(); i++)
		{
			if((!theDataList[i].isObject) || (theDataList[i].isProcessed))
			{
				//CA("(!theDataList[i].isObject) || (theDataList[i].isProcessed)");
				continue;
			}
			//CA("theDataList  name ==" +theDataList[i].name);			
			theDataList[i].isProcessed= kTrue;
			RefreshData referenceNode= theDataList[i];
			bool16 ReturnFlag = kFalse;


			for(int k=0; k < CurrentObjectDataList.size(); k++)
			{
		
				if(theDataList[i].whichTab == 4 && theDataList[i].isTableFlag == kFalse)
				{	

					if(CurrentObjectDataList[k].TypeID == theDataList[i].TypeID)
					{
				
						ReturnFlag= kTrue;
					}
				}
				else if(CurrentObjectDataList[k].objectID ==theDataList[i].objectID)
				{
					
					ReturnFlag= kTrue;
				}				

			}
			if(ReturnFlag)
			{
				//CA("ReturnFlag= kTrue");
				continue;
			}

	
			rDataList.push_back(theDataList[i]);//Added the Object
			RefreshData nodeToAdd;
			CurrentObjectDataList.push_back(referenceNode);
			
			for(int j=0; j< theDataList.size();j++)//Now adding the elements which have these elements
			{	
				//CA("inside for 111 ");
				if(!theDataList[j].isObject)
				{
					//CA("!theDataList[j].isObject");
					nodeToAdd=theDataList[j];
					//continue;
					if((theDataList[j].whichTab == 4 && theDataList[j].isTableFlag == kFalse /*&& theDataList.size()> 2*/))
					{
						//CA("theDataList[j].whichTab == 4 && theDataList[j].isTableFlag == kFalse...1986");
						/*PMString temp1 = "";
						temp1.Append("theDataList[i].TypeID = ");
						temp1.AppendNumber(theDataList[i].TypeID);
						temp1.Append(" ,theDataList[j].TypeID = ");
						temp1.AppendNumber(theDataList[j].TypeID);
						CA(temp1);*/

						if(theDataList[i].TypeID == theDataList[j].TypeID)
						{
							//CA("referenceNode.TypeID == theDataList[j].TypeID...1997");
							if(!theDataList[j].isProcessed)
							{
								
								nodeToAdd.isSelected=theDataList[j].isSelected;

								rDataList.push_back(nodeToAdd);//Added the Element
								theDataList[j].isProcessed=kTrue;
							}
						}
						continue;
					}

					
					if(referenceNode.objectID==theDataList[j].objectID)
					{
						//nodeToAdd.objectID=referenceNode.objectID;
						if(!theDataList[j].isProcessed)
						{
							nodeToAdd.isSelected=theDataList[j].isSelected;
							//CA("2017...nodeToAdd.name ="+nodeToAdd.name);
							rDataList.push_back(nodeToAdd);//Added the Element
							theDataList[j].isProcessed=kTrue;
						}
					}
			    }
			}
		}

		isElementoptSelected = kFalse;
		Mediator::isGroupByObj=kTrue;

		return kTrue;
}
//**************************Added..........
bool16 UPDSelectionObserver::reSortTheListForUniqueAttr(void)//We want to group by element
{
	//allElementSelectedFlag = kFalse;

	rDataList = OriginalrDataList;
	RefreshDataList theDataList=rDataList;
	rDataList.clear();
	UniqueDataList.clear();
	RefreshData nodeToAdd;
	RefreshDataList tempDataList;

	for(int j = 0; j < theDataList.size(); j++)
	{	
		if(theDataList[j].isObject){
			continue;
		}
		RefreshData nodeToAdd;
		if(UniqueDataList.size()==0)
		{	
			//CA("when UniqueDataList.Size == 0");	
			nodeToAdd = theDataList[j]; 
			nodeToAdd.isSelected = kTrue;
			//CA("nodeToAdd.Name ==" +nodeToAdd.name);
			UniqueDataList.push_back(nodeToAdd);
			rDataList.push_back(nodeToAdd);			
		}
		else 
		{		
			bool16 CheckFlag = kFalse;
			for(int p=0; p < UniqueDataList.size(); p++)
			{	

				if(UniqueDataList[p].elementID == theDataList[j].elementID && UniqueDataList[p].whichTab == theDataList[j].whichTab /*&& UniqueDataList[p].TypeID == theDataList[j].TypeID*/ )
				{	

					if(theDataList[j].whichTab != 4 || theDataList[j].isImageFlag == kTrue || theDataList[j].isTableFlag == kTrue)
					{

						if( UniqueDataList[p].TypeID != theDataList[j].TypeID){
							continue;
						}
					}
					if(theDataList[j].whichTab == 4 && UniqueDataList[p].elementID == -101 ) // for Item Table in Tabbed text
					{

						if( UniqueDataList[p].TypeID != theDataList[j].TypeID){
							continue;
						}
					}
					nodeToAdd = theDataList[j];
					nodeToAdd.isSelected =kTrue;
					//CA("nodeToAdd.Name =="+ nodeToAdd.name);
					rDataList.push_back(nodeToAdd);	
					CheckFlag = kTrue;
					break;
                }
			}

			if(!CheckFlag)
			{	
				nodeToAdd = theDataList[j];
				nodeToAdd.isSelected =kTrue;
				//CA("Adding element in UniqueDataList..2062");
				UniqueDataList.push_back(nodeToAdd);
				rDataList.push_back(nodeToAdd);
			}
		}
	}


	tempDataList.clear();
	for(int32 j=0; j< UniqueDataList.size(); j++)
	{
		if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kFalse && UniqueDataList[j].elementID == -2)
		{
			
			tempDataList.push_back(UniqueDataList[j]);
		}
	}

	for(int32 j=0; j<UniqueDataList.size(); j++){
		if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kFalse && UniqueDataList[j].elementID == -1 && UniqueDataList[j].elementID != -2)
		{
			tempDataList.push_back(UniqueDataList[j]);
		}
	}

	for(int32 j=0; j< UniqueDataList.size(); j++){
		if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kFalse && UniqueDataList[j].elementID != -1 && UniqueDataList[j].elementID != -2)
		{
				tempDataList.push_back(UniqueDataList[j]);
		}
	}

	for(int32 j=0; j< UniqueDataList.size(); j++){
		if(UniqueDataList[j].isTableFlag == kTrue ){
			tempDataList.push_back(UniqueDataList[j]);
		}
	}

	for(int32 j=0; j< UniqueDataList.size(); j++){
		if(UniqueDataList[j].isTableFlag == kFalse && UniqueDataList[j].isImageFlag == kTrue ){
				tempDataList.push_back(UniqueDataList[j]);
		}
	}
	UniqueDataList = tempDataList;
	return kTrue;
}
//**************************


void UPDSelectionObserver::ChangeColorOfAllText(int c , PMString  altColorSwatch)
{
	do
	{	
		//CA("Inside ChangeColorOfAllText");
		UIDRef	BoxRefUID1 = ColorDataList[c].BoxUIDRef; 
	//	int32 LObjectID1 = ColorDataList[c].objectID;
		double LElementID1 = ColorDataList[c].elementID;
		int32 StartText1 = ColorDataList[c].StartIndex;
		int32 EndText1 = ColorDataList[c].EndIndex;
		
		//InterfacePtr<IPMUnknown> unknown(BoxRefUID1, IID_IUNKNOWN);
		////InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
		//UID textFrameUID1 = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		UID textFrameUID1 = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(BoxRefUID1, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID1 = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID1 == kInvalidUID)
			break;
		/* ////CS3 Change
		InterfacePtr<ITextFrame> textFrame1(BoxRefUID1.GetDataBase(), textFrameUID1, ITextFrame::kDefaultIID);
		if (textFrame1 == NULL)
			break;
		*/
		/////Added by Amit
		InterfacePtr<IHierarchy> graphicFrameHierarchy(BoxRefUID1, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::ChangeColorOfAllText::graphicFrameHierarchy == nil");
			return;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::ChangeColorOfAllText::multiColumnItemHierarchy == nil");
			return;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::ChangeColorOfAllText::multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn");
			return;
		}
		InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::ChangeColorOfAllText::frameItemHierarchy == nil");
			return;
		}

		InterfacePtr<ITextFrameColumn>textFrame1(frameItemHierarchy, UseDefaultIID());
		if (!textFrame1) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::ChangeColorOfAllText::textFrame1 == nil");
			//CA("!!!ITextFrameColumn");
			return;
		}
		
		////End

		TextIndex startIndex2 = textFrame1->TextStart();
		
		InterfacePtr<ITextModel> textModel1(textFrame1->QueryTextModel());
		if (textModel1 == NULL)
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::ChangeColorOfAllText::textModel1 == nil");		
			break;
		}
		InterfacePtr<IWorkspace> workSpace(Utils<ILayoutUIUtils>()->QueryActiveWorkspace());
		InterfacePtr<ISwatchList> swatchList(workSpace,UseDefaultIID());
		if (swatchList == NULL)
		{	
			ASSERT_FAIL("Unable to get swatch list from workspace!");
			break;
		}
		
		InterfacePtr<ITextAttrUID>
		textAttrUID(::CreateObject2<ITextAttrUID>(kTextAttrColorBoss));
		UID colorUID;
		UID altColorUID = kInvalidUID;
		
		UIDRef altColorRef = swatchList->FindSwatch (altColorSwatch);
		if (altColorRef.GetUID() == kInvalidUID)
		{
			ASSERT_FAIL("Unable to get alternate swatch in DeleteColor!");
			break;
		}
		else
		{
			altColorUID = altColorRef.GetUID();
		}
		

		textAttrUID->Set(altColorUID);
		int32 charCount = EndText1 -StartText1;
		
		InterfacePtr<ICommand> applyCmd(
						Utils<ITextAttrUtils>()->
						BuildApplyTextAttrCmd(textModel1,
						StartText1,
						charCount,
						textAttrUID,
						kCharAttrStrandBoss) );
		
		ErrorCode err = CmdUtils::ProcessCommand(applyCmd);
		
	}while(0);
}

//Yogesh
void UPDSelectionObserver::ChangeColorOfText(int c , PMString  altColorSwatch)
{
	do
	{
		//CA("Inside Change Colour of text");
		UIDRef	BoxRefUID1;// = rDataList[c].BoxUIDRef;
		int32 StartText1 = 0;	
		int32 EndText1 = 0;
		if(!GroupFlag)
		{
			BoxRefUID1 = rDataList[c].BoxUIDRef;
			TagList tList;
			//TagReader tReader;
//			InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReader){ 
				return ;
			}
			tList=itagReader->getTagsFromBox(BoxRefUID1);
			
			if(tList.size()==0)
			{	

					tList=itagReader->getFrameTags(BoxRefUID1);
					if(tList.size()==0)
					{
						
						return;
					}
			}
		
	
			double LObjectID1 = rDataList[c].objectID;
			double LElementID1 = rDataList[c].elementID;
			
			bool16 isElementPresent = kFalse;
			for(int i=0; i<tList.size(); i++)
			{
				if(tList[i].elementId == LElementID1)
				{	
					//CA("isElementPresent = kTrue");
					isElementPresent = kTrue;
					StartText1 = tList[i].startIndex;
					EndText1 = tList[i].endIndex;
					
					/*PMString eidString("The StartText1 is ");
					eidString.AppendNumber(StartText1);
					eidString.Append(" \nThe EndText1 is ");
					eidString.AppendNumber(EndText1);
					CA(eidString);*/
					break;
				}
			}
			if(isElementPresent==kFalse)
			{
				//CA("Element not Present");
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
				return;
			}
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}
		else
		{
			//CA("Inside Change Colour of text");
			BoxRefUID1 = ElementGroupList[c].BoxUIDRef;
			//start Yogesh 10/01/05
			TagList tList;
			//TagReader tReader;
//			InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
			if(!itagReader){ 
				return ;
			}
			tList=itagReader->getTagsFromBox(BoxRefUID1);
							
			//start yogesh
			/*	PMString num("The taglist size is ");
			num.AppendNumber(tList.size());
			CA(num);*/
			//end yogesh
			if(tList.size()==0)
			{	
				
				tList=itagReader->getFrameTags(BoxRefUID1);
				if(tList.size()==0)
				{
					return;
				}
			}
			
			//start yogesh
		/*	int32 eid = tList[c].elementId;
			PMString eidString("The element id is ");
			eidString.AppendNumber(eid);
			CA(eidString);*/
			
			//end yogesh

			//end Yogesh 10/01/05
			
			double LObjectID1 = ElementGroupList[c].objectID;
		
			double LElementID1 = ElementGroupList[c].elementID;
			/*int32 StartText1 = 0;	
			int32 EndText1 = 0;*/
			//start Yogesh
			bool16 isElementPresent = kFalse;
			for(int i=0; i<tList.size(); i++)
			{
				if(tList[i].elementId == LElementID1)
				{	
					//CA("isElementPresent = kTrue");
					isElementPresent = kTrue;
					StartText1 = tList[i].startIndex;
					EndText1 = tList[i].endIndex;
					
					/*PMString eidString("The StartText1 is ");
					eidString.AppendNumber(StartText1);
					eidString.Append(" \nThe EndText1 is ");
					eidString.AppendNumber(EndText1);
					CA(eidString);*/
					break;
				}
			}
			if(isElementPresent==kFalse)
			{
				//CA("Element not Present");
				for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
				{
					tList[tagIndex].tagPtr->Release();
				}
				return;
			}
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
				
		}
		SetFocusForText(BoxRefUID1 ,StartText1,EndText1);		
		
		return;
	}while(0);
}
//end Yogesh

bool16 UPDSelectionObserver::fillDataInListBox(/*bool16*/int32  GroupFlag /*isGroupByElem*/)
{	
	
	//from here to----------

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		//return;
	}
	IControlView * iControlView=
		Mediator::iPanelCntrlDataPtr->FindWidget(kUPDSelectionBoxtWidgetID);
	if(iControlView==nil) 
	{
		//CA("iControlView==nil");
		ptrIAppFramework->LogDebug("AP46_RefreshContent::RfhSelectionObserver::fillDataInListBox::iControlView is nil");		
		//break;
	}
	InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
	if(itristatecontroldata==nil)
	{
		//CA("itristatecontroldata==nil");
		ptrIAppFramework->LogDebug("AP46_RefreshContent::RfhSelectionObserver::fillDataInListBox::itristatecontroldata is nil");				
		//break;
	}
	//itristatecontroldata->Select();
	//itristatecontroldata->Deselect ();
	//upto here---------
	
	//CA("Inside fill data in list box");
	InterfacePtr<IPanelControlData> panelControlData(QueryPanelControlData());
			/*if (panelControlData == NULL)
				return kFalse;*/
	if(!Mediator::listControlView)
		return kFalse;
	
	//SDKListBoxHelper listBox(this, kUPDPluginID);
	//listBox.EmptyCurrentListBox(panelControlData, 1);

	//24/01
	if(rDataList.size()>0)
	{
		itristatecontroldata->Select();
		Mediator::UPDRefreshButtonView->Enable();
		//Mediator::UPDLocateButtonView->Enable();

	}
	else
	{
		itristatecontroldata->Deselect ();
		Mediator::UPDRefreshButtonView->Disable();
		//Mediator::UPDLocateButtonView->Disable();

	}
	//end 24/01

	if( GroupFlag != 1 )
	{
		if(rDataList.size()==0)
		{
			//CA("RfhSelectionObserver::fillDataInListBox->rDataLsit.size==0");
		}
		if(rDataList.size()>0)
		{
			itristatecontroldata->Select();
			Mediator::UPDRefreshButtonView->Enable();
		}
		else
		{
			itristatecontroldata->Deselect();
			Mediator::UPDRefreshButtonView->Disable();
		}
		for(int i=0; i<rDataList.size(); i++)
		{
			PMString objectName;
			
			if(GroupFlag == 2){
				if(rDataList[i].isObject){
						objectName= rDataList[i].name;
				}
				else{
					objectName="        " + rDataList[i].name;			
				}
			}
			else if(GroupFlag == 3){

				if(rDataList[i].isObject){
					objectName="        " + rDataList[i].name;
				}
				else{
					objectName= rDataList[i].name;		
				}
			}
			
			//listBox.AddElement(Mediator::listControlView, objectName, kUPDPanelTextWidgetID, i, kTrue);			
			//listBox.CheckUncheckRow(Mediator::listControlView, i, kTrue);
			rDataList[i].isSelected=kTrue;
			rDataList[i].isProcessed=kFalse;
		}
	}    
	else if(GroupFlag == 1)
	{	
		
		if(UniqueDataList.size()>0)
		{
			itristatecontroldata->Select();
			Mediator::UPDRefreshButtonView->Enable();
		}	
		else
		{
			itristatecontroldata->Deselect();
			Mediator::UPDRefreshButtonView->Disable();
		}	

		for(int i=0; i<UniqueDataList.size(); i++)
		{	
			PMString objectName;
			if(GroupFlag == 1)
			{	
				if(UniqueDataList[i].isObject){
					objectName= UniqueDataList[i].name;
				}
				else{
					objectName="        " + UniqueDataList[i].name;			
				}
			}
			
			//listBox.AddElement(Mediator::listControlView,objectName,kUPDPanelTextWidgetID,i,kTrue);		
			//listBox.CheckUncheckRow(Mediator::listControlView, i, kTrue);
			UniqueDataList[i].isSelected=kTrue;
			UniqueDataList[i].isProcessed=kFalse;
		}
		for(int i=0; i<UniqueDataList.size(); i++)
		{
			for(int j=0; j<rDataList.size(); j++)
			{
				if(rDataList[j].isProcessed == kTrue)
					continue;
				if(UniqueDataList[i].elementID == rDataList[j].elementID /*&& UniqueDataList[i].TypeID ==  rDataList[j].TypeID */&& UniqueDataList[i].whichTab == rDataList[j].whichTab)
				{	
					if(UniqueDataList[i].whichTab != 4 || UniqueDataList[i].isImageFlag == kTrue || UniqueDataList[i].isTableFlag == kTrue)
					{
						if( UniqueDataList[i].TypeID !=  rDataList[j].TypeID)
							continue;
					}
					if(UniqueDataList[i].whichTab == 4 && UniqueDataList[i].elementID == -101 ) // for Item Table in Tabbed text
					{
						if( UniqueDataList[i].TypeID !=  rDataList[j].TypeID)
							continue;
					}
				
					rDataList[j].isSelected = UniqueDataList[i].isSelected;
					rDataList[j].isProcessed = kTrue;
					
				}
			}
		}
	}
	
	InterfacePtr<ITreeViewMgr> treeViewMgr(Mediator::listControlView, UseDefaultIID());
	if(!treeViewMgr)
	{
		ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");					
		return kFalse;
	}
	UPDTreeDataCache dc;
	dc.clearMap();

	UPDTreeModel pModel;
	PMString pfName("Root");
	pModel.setRoot(-1, pfName, 1);
	treeViewMgr->ClearTree(kTrue);
	pModel.GetRootUID();
	treeViewMgr->ChangeRoot();


	return kTrue;
}//End Of FillDataInListBox()



void UPDSelectionObserver::DisableAll()
{	//CA("Disable All");
	if(Mediator::loadData == kFalse)
		return;


	if(Mediator::iPanelCntrlDataPtr != NULL && FlgToCheckMenuAction== kTrue)
	{	
		IControlView* dropdownCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDOptionsDropDownWidgetID);
			if(!dropdownCtrlView)
				return;
		
			IControlView* listControlView1=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDPanelTreeViewWidgetID);
			if(!listControlView1){ //CA("!listControlView1");
				return;
			}
		
			IControlView* refreshBtnCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDRefreshButtonWidgetID);
			if(!refreshBtnCtrlView)
				return;

			//Yogesh
			/*IControlView* locateBtnCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDLocateButtonWidgetID);
			if(!locateBtnCtrlView)
				return;*/
		
			/*IControlView* EditBoxCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDPageNumWidgetID);
			if(!EditBoxCtrlView)
				return;*/

			IControlView* ReloadButtonCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDReloadIconWidgetID);
			if(!ReloadButtonCtrlView)
				return;

		
			//dropdownCtrlView->Disable();
			listControlView1->Disable();
			refreshBtnCtrlView->Disable();
			//Yogesh
			//locateBtnCtrlView->Disable();
			//EditBoxCtrlView->Disable();
			ReloadButtonCtrlView->Disable();
		
	}
}

//Yogesh
bool16 UPDSelectionObserver::UpdatetheCurrentSelection(PMString STORY, int r , double LanguageID, double SectionID)
{
	//CA("UPDSelectionObserver::UpdatetheCurrentSelection");
	do
	{
		//CA("UpdatetheCurrentSelection");
		//char* LStory = NULL;
		//CA("Before Conversion"+ STORY);
		isselected = kFalse;
		// Converts the given String to DataBase Formate 

/////------------
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{
			//CA("Inside iConverter");
			//CA(STORY);
			PMString temp1=iConverter->translateToDatabaseCode(STORY);
			STORY=temp1;
	
		}

		//LStory= STORY.GrabCString();


		//CA("Story: "+STORY);
//////-----------			
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == NULL)
		{
			CAlert::InformationAlert("Err ptrIAppFramework is NULL");
			return kFalse;
		}

		bool16 result=kFalse;
		//CA(" STORY Before Insert to DB : "+ STORY);
		//This Function will update the product or item attributes in the Databse on 20/01/07
		if(getWhichTab == 3)
		{
			//CA("getWhichTab == 3");
			//PMString objectId("objectId::");
			//objectId.AppendNumber(LObjectID);
		
			//PMString elementId("elementId::");
			//elementId.AppendNumber(LElementID);
			//CA(objectId);
			//CA(elementId);
			//CA("before call");
			//result = ptrIAppFramework->GETProduct_setObjectElementValue(LObjectID, LElementID, STORY/*LStory*/, LanguageID, SectionID);
			result = ptrIAppFramework->callReverseUpdateJson(3, LObjectID, LElementID, LanguageID, SectionID, STORY );
			//CA("After call");

		}

		if(getWhichTab == 4)
		{
			//CA("getWhichTab == 4");
			//PMString objectId("objectId::	");
			//objectId.AppendNumber(itemTypeId);
			//objectId.Append(" \nelementId::	");
			//objectId.AppendNumber( LElementID );
			//objectId.Append(" \nStory:	" + STORY);
			//CA(objectId);

			//result = ptrIAppFramework->GetItem_updateItemAttributeValue(itemTypeId,LElementID,STORY);
			result = ptrIAppFramework->callReverseUpdateJson(4, itemTypeId, LElementID, LanguageID, SectionID, STORY );
		}

		if(result == kFalse)
		{
			//CA("result == kFalse");
			//SDKListBoxHelper sList3(this,kUPDPluginID);
			//sList3.UpdateSignalRow(Mediator::listControlView,r, 2 ); // for failure signal widget on list 
			
			rDataList[r].isElementUpdated   = kFalse ;         //Added
			allElementSelectedFlag          = kFalse ;        ///Added	
			rDataList[r].iconDispayHint = 1; // Falied red Icon
			return kFalse;
		}
		

	}while(0);

	allElementSelectedFlag        =   kTrue  ;
	rDataList[r].isElementUpdated =   kTrue  ;
	rDataList[r].iconDispayHint = 2 ; // Sussess Green Icon
	return kTrue;
}


void UPDSelectionObserver::EnableAll()
{	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == NULL)
				return;
		
			bool16 result=ptrIAppFramework->getLoginStatus();					
			if(!result)
				return;

	if(Mediator::iPanelCntrlDataPtr != NULL && FlgToCheckMenuAction== kTrue)
	{	
		IControlView* dropdownCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDOptionsDropDownWidgetID);
			if(!dropdownCtrlView)
				return;
		
			IControlView* listControlView1=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDPanelTreeViewWidgetID);
			if(!listControlView1){ //CA("!listControlView1");  
				return;
			}
		
			IControlView* refreshBtnCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDRefreshButtonWidgetID);
			if(!refreshBtnCtrlView)
				return;
		
			/*IControlView* EditBoxCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDPageNumWidgetID);
			if(!EditBoxCtrlView)
				return;*/

			
			IControlView* ReloadButtonCtrlView=
				Mediator::iPanelCntrlDataPtr->FindWidget(kUPDReloadIconWidgetID);
			if(!ReloadButtonCtrlView)
				return;
		
			dropdownCtrlView->Enable();
			listControlView1->Enable();
			refreshBtnCtrlView->Enable();
			//EditBoxCtrlView->Disable();
			ReloadButtonCtrlView->Enable();

			InterfacePtr<IDropDownListController> RefreshDropListCntrler(dropdownCtrlView, UseDefaultIID());
			if(RefreshDropListCntrler==NULL)
			{
			//	CA("subSectionDropListCntrler");
				return;
			}
			RefreshDropListCntrler->Select(1);  //0
			FlgToCheckMenuAction= kFalse;

			InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
			if (iPanelControlData==NULL)
			{
				//CA("iPanelControlData is NULL");
				//	return;
			}
			//SDKListBoxHelper listBox(this, kUPDPluginID);
			//listBox.EmptyCurrentListBox(iPanelControlData, 1);
			UPDTreeDataCache dc;
			dc.clearMap();

		
	}
}

//End Yogesh

//start 14/01/05
void UPDSelectionObserver::SetFocusForText(UIDRef &boxUIDRef ,TextIndex StartText1, TextIndex EndText1)
{

//	RangeData rData( StartText1, EndText1);	
//	
//	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils>()->QueryActiveSelection());
//	InterfacePtr<ILayoutSelectionSuite>layoutSelectionSuite(iSelectionManager, UseDefaultIID());
//			if (!layoutSelectionSuite) 
//			{	CA("!layoutSelectionSuite");
//				return;
//			}
//
//	IDataBase * idatabase = boxUIDRef.GetDataBase();
//		UID uid = boxUIDRef.GetUID();
//		UIDList itemList(idatabase,uid);	
////
////		
//	layoutSelectionSuite->Select(itemList,Selection::kReplace,Selection::kDontScrollSelection);
////		
//	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
//	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
//	if(!txtMisSuite)
//	{
//		//CA("retun");
//		
//		return; 
//	}
////
//	UIDList	newUIDList;
//	txtMisSuite->GetUidList(newUIDList);
//
//	InterfacePtr<ITextSelectionSuite>txtSelSuite(Utils<ISelectionUtils>()->QueryTextSelectionSuite(iSelectionManager),UseDefaultIID());
//	if(!txtSelSuite)
//	{ 
//		CA("Text Selectin suite is null"); 
//		return;
//	}
//
//	InterfacePtr<IConcreteSelection>pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss));
//	// deprecated but universal (CS/2.0.2)
//
//	InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());

		//InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
		////UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		////InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
		//UID textFrameUID = Utils<IFrameUtils>()->GetTextFrameUID(unknown);
		UID textFrameUID = kInvalidUID;
		InterfacePtr<IGraphicFrameData> graphicFrameDataOne(boxUIDRef, UseDefaultIID());
		if (graphicFrameDataOne) 
		{
			textFrameUID = graphicFrameDataOne->GetTextContentUID();
		}
		if (textFrameUID == kInvalidUID)
			return;
//
//	InterfacePtr<ITextFrame>textFrame(/*boxUIDRef*/newUIDList.GetRef(0),UseDefaultIID());
/*		//CS3 Change
		InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
		if(!textFrame)
		{	//CA("!textFrame");
			return;
		}
*/
		/////Added by Amit

		InterfacePtr<IHierarchy> graphicFrameHierarchy(boxUIDRef, UseDefaultIID());
		if (graphicFrameHierarchy == nil) 
		{
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::SetFocusForText::graphicFrameHierarchy == nil");
			return;
		}
						
		InterfacePtr<IHierarchy> multiColumnItemHierarchy(graphicFrameHierarchy->QueryChild(0));
		if (!multiColumnItemHierarchy) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::SetFocusForText::multiColumnItemHierarchy == nil");
			return;
		}

		InterfacePtr<IMultiColumnTextFrame> multiColumnItemTextFrame(multiColumnItemHierarchy, UseDefaultIID());
		if (!multiColumnItemTextFrame) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::SetFocusForText::multiColumnItemTextFrame == nil");
			//CA("Its Not MultiColumn");
			return;
		}
		InterfacePtr<IHierarchy>frameItemHierarchy(multiColumnItemHierarchy->QueryChild(0));
		if (!frameItemHierarchy) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::SetFocusForText::frameItemHierarchy == nil");
			return;
		}

		InterfacePtr<ITextFrameColumn>textFrame(frameItemHierarchy, UseDefaultIID());
		if (!textFrame) {
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::SetFocusForText::textFrame == nil");
			//CA("!!!ITextFrameColumn");
			return;
		}
	
		////End 
		InterfacePtr<ITextModel>textModel(textFrame->QueryTextModel());
		if(!textModel)
		{   //CA("!textModel");
			ptrIAppFramework->LogDebug("AP46_UpdateRepository::UPDSelectionObserver::SetFocusForText::textModel == nil");
			return;
		}	
//	
//	txtSelSuite->SetTextSelection(::GetUIDRef(textModel),rData,Selection::kScrollIntoView,NULL);
//
//	pTextTarget->SetTextFocus(pTextTarget->GetTextModel(),rData);
//CA("1");
InterfacePtr<IK2ServiceRegistry> pServiceRegistry(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
if(!pServiceRegistry)
{
	//CA("IK2ServiceRegistry is NULL");
	return;
}
//CA("1.1");		
InterfacePtr<IK2ServiceProvider> pServiceProvider(pServiceRegistry->QueryServiceProviderByClassID(kTextWalkerService, kTextWalkerServiceProviderBoss));
if(!pServiceProvider)
{
	//CA("IK2ServiceProvider is NULL");
	return;
}
//CA("2");
		
InterfacePtr<ITextWalker> pWalker(pServiceProvider, UseDefaultIID());
if(!pWalker)
{
	//CA("ITextWalker is NULL");
	return;
}
//CA("3");
InterfacePtr<ITextWalkerSelectionUtils>pUtils(pWalker,UseDefaultIID());
		
		const TextWalkerSelections_CriticalSection criticalSection(pUtils);

		
if(!pUtils)
{
	//CA("ITextWalkerSelectionUtils is NULL");
	return;
}
//CA("4");

InterfacePtr<ITextWalkerSelectionUtils> pTextWalkerSelectUtils(pWalker, UseDefaultIID());

if(!pTextWalkerSelectUtils)
{
	//CA("ITextWalkerSelectionUtils is NULL");
	return;
}
//CA("5");

    UIDRef uidRef = ::GetUIDRef(textModel);
pTextWalkerSelectUtils->SelectText(uidRef, StartText1, EndText1 - StartText1);

/*if(!SelectText)
{
	CA("pTextWalkerSelectUtils is NULL");
	return;
}*/
//CA("6");

}
//end   14/01/05
//End Yogesh




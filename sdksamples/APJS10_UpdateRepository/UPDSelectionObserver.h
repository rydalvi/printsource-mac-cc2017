#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "IAppFramework.h"
#include "ListBoxHelper.h"
#include "IListBoxController.h"
#include "ITextControlData.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
//#include "CObserver.h"

#include "UPDID.h"

class UPDSelectionObserver : public ActiveSelectionObserver
{
	public:
		UPDSelectionObserver(IPMUnknown *boss);
		virtual ~UPDSelectionObserver();
		void AutoAttach();
		void AutoDetach();
		void Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		
		void setTriState(const WidgetID&  widgetID, ITriStateControlData::TriState state);
		void loadPaletteData();	
		bool16 fillDataInListBox(InterfacePtr<IPanelControlData>, bool16 isGroupByElem=kFalse);
		bool16 setSpreadFromPage(const UID& pageUID);
		bool16 reSortTheListForElem(void);
		bool16 reSortTheListForObject(void);
		bool16 reSortTheListForUniqueAttr(void); //Added.
		void ChangeColorOfAllText(int c , PMString  altColorSwatch);
		//Yogesh
		void ChangeColorOfText(int c , PMString);
		bool16 UpdatetheCurrentSelection(PMString STORY, int r, double LanguageID, double SectionID);
		bool16 isObjectGroup;
		IPanelControlData*	QueryPanelControlData();
		//bool16 fillDataInListBox(bool16 isGroupByElem=kFalse);
		bool16 fillDataInListBox(int32 GroupFlag);//Added
		void DisableAll();
		void EnableAll();

	protected:
		
		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void UpdatePanel();
		virtual void HandleSelectionChanged(const ISelectionMessage* message);
		void HandleDropDownListClick(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		void SetFocusForText(UIDRef &boxUIDRef ,TextIndex StartText1, TextIndex EndText1);
	private:
		 //Yogesh
		PMString ElementName;
		UIDRef BoxRefUID;
		int32 SelectNo;
		PMString LatestStory;
		double LObjectID ;
		double LElementID ;
		bool16 isselected;
		//end Yogesh
		//Addition By Dattatray on 20/01/07
		int32 getWhichTab;//It is Added for Updating item and product attributes in UpdatetheCurrentSelection where which tab is required.
		double itemTypeId; //----//---- It is used in updating item's attributes where item id is required 
		

};

//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/wlistboxcomposite/WLBCmpPenObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"

// Interface includes
#include "ISubject.h"
#include "IBooleanControlData.h"
// Implem includes
#include "CAlert.h"
#include "CObserver.h"
#include "UPDID.h"


#define CA(X) CAlert::InformationAlert(X)


/**

	Observes the "pen" buttons in the widget list.

	@ingroup wlistboxcomposite
	@author Ian Paterson
*/
class UPDIconSuiteWidgetObserver : public CObserver
{
public:
	
	/**
		Constructor for WLBListBoxObserver class.
		@param interface ptr from boss object on which this interface is aggregated.
	*/
	UPDIconSuiteWidgetObserver(IPMUnknown *boss);

	/**
		Destructor.
	*/	
	~UPDIconSuiteWidgetObserver();

	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	
	*/	
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when widget hidden.
	*/	
	virtual void AutoDetach();

	/**
		Update when there's been a change on the pen widget.
	
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/	
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

private:
	/**
		Helper method to change state of list-box, dependent on whether
		it is being shown or hidden.
		@param isAttaching specifies whether attaching (shown) or detaching (hidden)
	*/
	//void updateListBox(bool16 isAttaching);

	
};

CREATE_PMINTERFACE(UPDIconSuiteWidgetObserver, kUPDIconSuiteWidgetObserverImpl)


UPDIconSuiteWidgetObserver::UPDIconSuiteWidgetObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}


UPDIconSuiteWidgetObserver::~UPDIconSuiteWidgetObserver()
{
	
}


void	UPDIconSuiteWidgetObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_IBOOLEANCONTROLDATA);
	}
	
}


void UPDIconSuiteWidgetObserver::AutoDetach()
{
		InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_IBOOLEANCONTROLDATA);
	}
}


void UPDIconSuiteWidgetObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	/*if(theChange == kTrueStateMessage) {
		// Then the button has been activated.
		do {
				PMString dbgInfoString("WLBCmpPenObserver::Update() ");
				dbgInfoString.SetTranslatable(kFalse);	// only for debug- not real code
				CAlert::InformationAlert(dbgInfoString);
		
		} while(0);
	}*/

	CA("Inside Update of IconSuiteWidgetObserver");
}





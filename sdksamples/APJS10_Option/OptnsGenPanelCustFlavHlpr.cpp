
#include "VCPlugInHeaders.h"

//Interface includes:
#include "CDragDropTargetFlavorHelper.h"
#include "IControlView.h"
#include "ILayoutUtils.h"
#include "ILayoutControlData.h"
#include "ICommand.h"
#include "IDataExchangeHandler.h"
#include "IGeometry.h"
#include "IPageItemLayerData.h"
#include "IPageItemScrapUtils.h"
#include "IPasteboardUtils.h"
#include "IDocument.h"
#include "IPageItemScrapData.h"
#include "ISpread.h"
#include "IBoundsUtils.h"
#include "IMoveRelativeCmdData.h"
#include "ILayoutCmdData.h"
#include "IBoundsUtils.h"

// General includes:
#include "PersistUtils.h"
#include "ErrorUtils.h"
#include "Utils.h"
#include "DataObjectIterator.h"
#include "CmdUtils.h"
#include "LayerID.h"

// Project includes:
#include "OptnsDlgID.h"
#include "CAlert.h"
#include "OptnsDragEventFinder.h"
#include "CommonFunctions.h"
#include "ISelection.h"
#include "LayoutUtils.h"
#include "SystemUtils.h"
#include "InterfacePtr.h"
#include "ISpread.h"
#include "ISpreadList.h"
#include "UIDList.h"
#include "K2SmartPtr.h"
#include "SDKListBoxHelper.h"
#include "IControlView.h"
#include "listData.h"


#define CA(Z) CAlert::InformationAlert(Z)

class OptnsGenPanelCustFlavHlpr : public CDragDropTargetFlavorHelper
{
public:
	/**
		Constructor.
		@param boss IN the boss class the interface is aggregated onto.
	*/
	OptnsGenPanelCustFlavHlpr(IPMUnknown *boss);
	/**
		dtor.
	*/
	virtual	~OptnsGenPanelCustFlavHlpr();

	/**
		Determines whether we can handle the flavors in the drag.
		@param target IN the target the mouse is currently over.
		@param dataIter IN iterator providing access to the data objects within the drag.
		@param fromSource IN the source of the drag.
		@param controller IN the drag drop controller mediating the drag.
		@return a target response (either won't accept or drop will copy).
	*/
	virtual DragDrop::TargetResponse	
						CouldAcceptTypes(const IDragDropTarget*, DataObjectIterator*, const IDragDropSource*, const IDragDropController*) const;

	/**
		performs the actual drag. Because we know that our custom flavor is masquerading and is 
		really a page item we must take a copy of the page item, then move this copy to the drop zone.
		We know we have been dropped on a widget (because we are called as part of that widget responding to
		a drop of the custom flavor).
		@param target IN the target for this drop.
		@param controller IN the drag drop controller that is mediating the drag.
		@param action IN what the drop means (i.e. copy, move etc)
		@return kSuccess if the drop is executed without error, kFailure, otherwise.
	*/
	virtual ErrorCode		ProcessDragDropCommand(IDragDropTarget*, IDragDropController*, DragDrop::eCommandType);

	DECLARE_HELPER_METHODS()
};


//--------------------------------------------------------------------------------------
// Class LayoutDDTargetFileFlavorHelper
//--------------------------------------------------------------------------------------
CREATE_PMINTERFACE(OptnsGenPanelCustFlavHlpr, kOptnsGenPanelCustFlavHlprImpl)
DEFINE_HELPER_METHODS(OptnsGenPanelCustFlavHlpr)

OptnsGenPanelCustFlavHlpr::OptnsGenPanelCustFlavHlpr(IPMUnknown *boss) :
	CDragDropTargetFlavorHelper(boss), HELPER_METHODS_INIT(boss)
{
}

OptnsGenPanelCustFlavHlpr::~OptnsGenPanelCustFlavHlpr()
{
}

/* Determine if we can handle the drop type */
DragDrop::TargetResponse 
OptnsGenPanelCustFlavHlpr::CouldAcceptTypes(const IDragDropTarget* target, DataObjectIterator* dataIter, const IDragDropSource* fromSource, const IDragDropController* controller) const
{
	// Check the available external flavors to see if we can handle any of them
	if (dataIter != nil)
	{
		//CAlert::InformationAlert("CouldAcceptTypes");
		// Test for swatches in the drag
		DataExchangeResponse response;
		
		response = dataIter->FlavorExistsWithPriorityInAllObjects(ourFlavor1);
		if (response.CanDo())
			return DragDrop::TargetResponse(response, DragDrop::kDropWillCopy);
	}
	return DragDrop::kWontAcceptTargetResponse;
}

/* process the drop, this method called if CouldAcceptTypes returns valid response */
ErrorCode	
OptnsGenPanelCustFlavHlpr::ProcessDragDropCommand
	(
	IDragDropTarget* 			target, 
	IDragDropController* 		controller,
	DragDrop::eCommandType		action
	)
{	
	ErrorCode stat = kFailure;
	do
	{		

		if (action != DragDrop::kDropCommand) {
			CAlert::InformationAlert("DragDrop::kDropCommand");
			break;
		}

		// Find the target view for the drop and the mouse position.
		InterfacePtr<IControlView> layoutView(target, UseDefaultIID());
		ASSERT(layoutView);
		if (!layoutView) {
			CAlert::InformationAlert("IControlView layoutView nil");
			break;
		}
		InterfacePtr<ILayoutControlData> layoutData(target, UseDefaultIID());
		ASSERT(layoutData);
		if (!layoutData) {
			CAlert::InformationAlert("ILayoutControlData layoutData nil");
			break;
		}
		GSysPoint where = controller->GetDragMouseLocation();
		PMPoint currentPoint = Utils<ILayoutUtils>()->GlobalToPasteboard(layoutView, where);

		// Determine the target spread to drop the items into.
		InterfacePtr<ISpread> targetSpread(Utils<IPasteboardUtils>()->QueryNearestSpread(layoutView, currentPoint));
		ASSERT(targetSpread);
		if (!targetSpread) {
			CAlert::InformationAlert(" targetSpread nil");
			break;
		}
		UIDRef targetSpreadUIDRef = ::GetUIDRef(targetSpread);

		// Change spread if necessary.
		if (targetSpreadUIDRef != ::GetUIDRef(layoutData->GetSpread())) {
			InterfacePtr<ICommand> setSpreadCmd(CmdUtils::CreateCommand(kSetSpreadCmdBoss));
			ASSERT(setSpreadCmd);
			if (!setSpreadCmd) {
				CAlert::InformationAlert(" setSpreadCmd nil");
				break;
			}
			InterfacePtr<ILayoutCmdData> setSpreadLayoutCmdData(setSpreadCmd, UseDefaultIID());
			ASSERT(setSpreadLayoutCmdData);
			if (!setSpreadLayoutCmdData) {
				CAlert::InformationAlert(" setSpreadLayoutCmdData nil");
				break;
			}
			setSpreadLayoutCmdData->Set(::GetUIDRef(layoutData->GetDocument()), layoutData);
			setSpreadCmd->SetItemList(UIDList(targetSpreadUIDRef));
			stat = CmdUtils::ProcessCommand(setSpreadCmd);
			ASSERT_MSG(stat == kSuccess, "Failed to change spread");
		}

		// Get a list of the page items in the drag.
		stat = controller->InternalizeDrag(kNoExternalFlavor, ourFlavor1);
		ASSERT_MSG(stat == kSuccess, "Cannot internalize the dragged item?\n");
		if (stat != kSuccess) {
			break;
		}
		InterfacePtr<IDataExchangeHandler> handler(controller->QueryTargetHandler());
		ASSERT(handler);
		if (!handler) {
			break;
		}
		InterfacePtr<IPageItemScrapData> pageItemData(handler, UseDefaultIID());
		ASSERT(pageItemData);
		if (!pageItemData) {
			break;
		}
		InterfacePtr<IPageItemLayerData> layerData(handler, IID_IPAGEITEMLAYERDATA);
		ASSERT(layerData);
		if (!layerData) {
			break;
		}
		UIDList* draggedItemList = pageItemData->CreateUIDList();		

		// Duplicate the drag content in the target spread.
		// Parent the duplicated page item in the active spread layer.
		UIDRef parent(targetSpreadUIDRef.GetDataBase(), layoutData->GetActiveLayerUID());
		// See IPageItemLayerData if you want to maintain layer information across the drop.
		K2Vector<PMString>* layerNameList = nil;
		K2Vector<int32>* layerIndexList = nil;
		InterfacePtr<ICommand>	duplicateCmd(Utils<IPageItemScrapUtils>()->CreateDuplicateCommand(draggedItemList, layerNameList, layerIndexList, kFalse, parent));	
		ASSERT(duplicateCmd);
		if (!duplicateCmd) {
			delete draggedItemList;
			break;
		}
		stat = CmdUtils::ProcessCommand(duplicateCmd);
		const UIDList* copiedItemList = duplicateCmd->GetItemList();
		ASSERT_MSG(stat == kSuccess && copiedItemList != nil, "Failed to duplicate dropped item");
		if (stat != kSuccess) {
			break;
		}

		// Position the dropped items on the spread at the mouse position.
		// Calculate the bounds of the items so we can center them on the cursor.
		// You might want to extend the code below to constrain the dropped items
		// position to intersect the bounds of the spread.
		PMRect itemsBoundingBox = Utils<IBoundsUtils>()->GetStrokeBounds(*copiedItemList);
		PMPoint itemsBoundsCenter = itemsBoundingBox.GetCenter();
		InterfacePtr<ICommand> moveRelativeCmd(CmdUtils::CreateCommand(kMoveRelativeCmdBoss));
		ASSERT(moveRelativeCmd);
		if (!moveRelativeCmd) {
			break;
		}
		InterfacePtr<IMoveRelativeCmdData> moveRelativeCmdData(moveRelativeCmd, UseDefaultIID());
		ASSERT(moveRelativeCmdData);
		if (!moveRelativeCmdData) {
			break;
		}
		moveRelativeCmdData->Set(currentPoint - itemsBoundsCenter);
		moveRelativeCmd->SetItemList(UIDList(*copiedItemList));
		stat = CmdUtils::ProcessCommand(moveRelativeCmd);
		ASSERT_MSG(stat == kSuccess, "Failed to position dropped items");
		if (stat != kSuccess) {
			break;
		}

		stat = kSuccess;
		InterfacePtr<ISelection> isel(::QuerySelection());

		isel->Select(*(moveRelativeCmd->GetItemList()),ISelection::kReplace, kTrue);
		changeMode(DRAGMODE);

		listData lstdta;

		lstdta.setUID(OptnsDragEventFinder::listboxType, OptnsDragEventFinder::selectedRowIndex,
			 (moveRelativeCmd->GetItemList())->GetRef(0).GetUID());

		/* Marking the dragged flag to true */
		lstdta.resetDraggedBox
			(OptnsDragEventFinder::currentTab, 
			 OptnsDragEventFinder::selectedRowIndex, 
			 kTrue);

		/* Changing the radio btn selection if Image box */
		if(OptnsDragEventFinder::isImgflag==kTrue)
		{
			OptnsDragEventFinder::whichRadBtnSelected=2;
			CAlert::InformationAlert("Image");
		}
		insertOrAppend
			(OptnsDragEventFinder::selectedRowIndex, 
			 OptnsDragEventFinder::currentSelectionRowString,
			 OptnsDragEventFinder::whichRadBtnSelected,
			 OptnsDragEventFinder::currentTab);

		//CAlert::InformationAlert("found the drag end Event");
	}
	while(false);
	return stat;
}


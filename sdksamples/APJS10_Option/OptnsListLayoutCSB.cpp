
/*
//	File:	PstLstLayoutCSB.cpp
//
//	Date:	6/19/2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

//	INCLUDES
#include "VCPlugInHeaders.h"								// For MSVC

// Interface headers
#include "ILayoutTarget.h"
#include "ILayoutSelContentChangedMsg.h"
#include "IUIDData.h"
#include "IIntData.h"
#include "IOptnsListData.h"
#include "IOptnsListSuite.h"										// Superclass declaration
#include "IOptnsListUID.h"										// Superclass declaration
#include "CAlert.h"
// Other headers
#include "HelperInterface.h"
#include "K2Vector.tpp"
#include "CmdUtils.h"
#include "UIDList.h"
#include "GenericID.h"										// ID definition header
#include "K2Vector.tpp"
#include "SelectionExtTemplates.tpp"
#include "listData.h"
#include "OptnsDragEventFinder.h"

// Project headers

//	CLASS DECLARATIONS
/** 
	Layout concrete selection boss (CSB) \Ref{IPstLstSuite} implementation.  The suite implementations on the CSBs are 
	responsible for doing all of the model specific work.  The implementation of a CSB suite deals with the 
	model format of its owning boss. The only information that the suite should need is from sibling interfaces 
	on the CSB, primarily the target interface. This interface specifies which items to act upon. For the 
	LayoutCSB, the target interface is ILayoutTarget which wraps a UIDList. So usually you see the methods in
	this CSB will first get an InterfacePtr to a ILayoutTarget, then it gets the UIDList of the selected items
	from the pointer.
	
    Note that the client does not interact with the CSB (which has all the real implementation of the suite) 
    directly, the client interacts with the ASB only.  Also note that the Suite API shouldn't contain model 
    data that is specific to a selection format (layout uidLists, text model/range, etc) so that the client 
    code can be completely decoupled from the underlying CSB.

	This suite is an advanced suite which uses selection extension.  The selection extension is a bridge 
	between a suite implementation with an unknown interface (IPstLstData) and the selection architecture. The
	main reason this extension is needed is so we can be notified of current selection's model change (thru
	SelectionAttributeChanged).
	 
	@author Lee Huang
*/
class PstLstLayoutCSB : public CPMUnknown<IPstLstSuite>
{
public:
	/**
		Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.
	*/
	PstLstLayoutCSB(IPMUnknown* iBoss);
	
	/** Destructor. */
	virtual					~PstLstLayoutCSB(void);

//	Suite Member functions
public:
	/**
		@return true if this CSB suite allow adding new IPstLstData, false otherwise.
	*/
	virtual bool16			CanCreateNewPstLstData(void);

	/**
		Process commands kPstLstNewDataCmdBoss, which will add a new IPstLstData to all selected page items
		@param theData the IPstLstData (a PMString) to be set onto the selected item.
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode		CreateNewPstLstData(const PMString& theData);
	
	/**
		@return true if the user can modify the current selected IPstLstData with the current selection, false otherwise.
	*/
	virtual bool16			CanModifyPstLstData(void);
	
	/**
		Process command kPstLstModifyDataCmdBoss, which will modify the current selected IPstLstData of all selected page items with the input data (theData)
		@param theData the IPstLstData (a PMString) to be modified onto the selected item.
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode		ModifyPstLstData(const PMString& theData);
	
	/**
		@return true if the user can delete current selected IPstLstData of the current selections, false otherwise.
	*/
	virtual bool16			CanDeletePstLstData(void);
	
	/**
		Process command kPstLstDeleteDataCmdBoss, which will delete the current selected IPstLstData of all selected page items.
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode		DeletePstLstData(void);

	/**
		Retrive the data associated with one of the current selected items. Note there might be multiple selections, 
		we will just return the first one we encounter since we only display one page item's info.
		@param pstlstDataList list of the PstLstData of the current selected items. The PstLstData
		is a structure that contains the UID of the IPstLstDat and its data string.
	*/
	virtual void			GetPstLstDataDescription(PstLstDataVector& pstlstDataVector);

	/**
		Get the current selected UID's position index as appeared in the drop down list.  Note there might be multiple selections, 
		we will just return the first one we encounter since we only display one page item's info.
		@param uidListIndex the current selected UID's position index as represented in the drop down list.
	*/
	virtual void			GetSelectedItemIndex(int32& uidListIndex);

	/** 
		Tell PstLstUIDList that a new UID has been selected from the drop down list
		@param uidListIndex the newly selected UID's position index to be modified onto the selected item.
		@return kSuccess if the command is processed successfully, kFailure otherwise.
	*/
	virtual ErrorCode 		SetSelectedItemIndex(int32 uidListIndex);

	//________________________________________________________________________________
	//	Selection Extention functions 
	//________________________________________________________________________________
	/**
		This function is called when the selection sub-system starts up. Any suite initialization
		code can be placed in this funciton. Called for suites on the ASB and CSBs.
	*/
	virtual void				Startup			 					(void) {}

	/**
		This function is called when the selection sub-system shuts down. Any suite termination 
		code can be placed in this funciton. Called for suites on the ASB and CSBs.
	*/
	virtual void				Shutdown			 				(void) {}

	/**
		This function is called when an item is added to or removed from the selection.  Suites on the 
		changing CSB and the ASB are called.	
	*/
	virtual void				SelectionChanged 					(void*, SuiteBroadcastData*) {}

	/**
		This function is called when an attribute of the selection has changed. It is only called on the CSB whose
		selection attribute has changed. For the layout CSB an ProtocolCollection must be supplied first.
	*/
	virtual void				SelectionAttributeChanged 			(SuiteBroadcastData*, void*);

	/**
		This is only supported on the ASB and is rarely used.
	*/
	virtual void				HandleIntegratorSuiteMessage		(void*, const ClassID&, ISubject*, const PMIID&, void*) {}

	/**
		Each suite must define the protocols that contain the attributes that they care about. The Layout CSB's selection 
		attribute observer asks each suite with a selection extension for a list of protocols to observer on the document.  
		Thereby, allowing all suites to share a single observer. When a document broadcast occurs with the specified 
		protocol, the suite must first determine if the command affects the selection by comparing the commands target to 
		the LayoutTarget (This is necessary because all commands don't use the UIDList for page items.).	
	*/
	virtual ProtocolCollection*	CreateObserverProtocolCollection(void);


private:
	/**
		Check if a drawable page item is selected, a drawable page item will have a IPstLstUIDList interface when the plug-in is loaded.
		So we will check if we can get a valid IPstLstUIDList InterfacePtr thru the selected items as the test to see if it is a
		drawable page item.
	*/
	bool16					DrawablePageItemSelected();					
	/**
		Check if any drawable page item(s) is (are) selected, if yes, then it will further check if any of the selected items contains
		PstLst data, if yes, then return true, if all selected items PstLst are all empty, then return false.
	*/
	bool16					AllSelectedPstLstAreEmpty();
};

CREATE_PMINTERFACE(PstLstLayoutCSB, kPstLstLayoutCSBImpl)

/*
	Instatiate the selection extension template and create an InDesign interface.
	The parameter to the template is the suite implementation class.
*/
template class SelectionExt<PstLstLayoutCSB>;
CREATE_PMINTERFACE(SelectionExt<PstLstLayoutCSB>, kPstLstSuiteLayoutSelectionExtImpl)

/*	Constructor
*/
PstLstLayoutCSB::PstLstLayoutCSB(IPMUnknown* iBoss) :
	CPMUnknown<IPstLstSuite>(iBoss)
{
}

#pragma mark -

/*	Destructor
*/
PstLstLayoutCSB::~PstLstLayoutCSB(void)
{
}

/*	PstLstLayoutCSB::CanCreateNewPstLstData
*/
bool16 PstLstLayoutCSB::CanCreateNewPstLstData(void)
{
	// if the current selection has at least one drawable page item (which will have an
	// IPstLstUIDList interface), then we will return true so the client code can add the data object
	if (DrawablePageItemSelected())
	{
		return (true);
	}
	else
	{
		return (false);
	}
}

/*	PstLstLayoutCSB::CreateNewPstLstData
*/
ErrorCode PstLstLayoutCSB::CreateNewPstLstData(const PMString& theData)
{
	do
	{
		InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
		if (iLayoutTarget == nil)
		{
			ASSERT_FAIL("iLayoutTarget invalid");
			break;
		}
		UIDList					selectUIDList(iLayoutTarget->GetUIDList(kStripStandoffs));

		if (selectUIDList.Length() < 1)
		{
			// We should not be here since CanCreateNewPstLstData should be called to make sure there
			// is at least one page item who allows adding an IPstLstData, we are adding data in this routine.
			ASSERT_FAIL("UIDList invalid");
			break;
		}

		int32 item = selectUIDList.Length();
		// remove those items that does not have IPstLstUIDList, IPstLstUIDList is only aggregated onto
		// kDrawablePageItemBoss
		while (item-- > 0)
		{
			InterfacePtr<IPstLstUIDList> tempData(selectUIDList.GetRef(item), UseDefaultIID());
			if (tempData == nil)
			{
				selectUIDList.Remove(item);
			}
		}
		// If there are no items left in the list, then don't do anything.
		if (selectUIDList.Length() == 0)
		{
			ASSERT_FAIL("No valid page item to apply PstLstNewDataCmd" );
			break;
		}
		
		// item list and settings information are ok, create the command.
		InterfacePtr<ICommand> pstlstCommand(CmdUtils::CreateCommand(kPstLstNewDataCmdBoss));
		if (pstlstCommand == nil)
		{
			// For some reason we couldn't get our PstLstNewDataCmd command pointer, so assert and
			// issue a user warning and leave:
			ASSERT_FAIL("PstLstNewDataCmd invalid");
			break;
		}

		pstlstCommand->SetItemList(selectUIDList);
		InterfacePtr<IPstLstData> cmdData(pstlstCommand, UseDefaultIID());		
		if (cmdData == nil)
		{
			// For some reason we couldn't get our IPstLstData pointer, so assert and
			// issue a user warning and leave:
			ASSERT_FAIL("IPstLstData invalid");
			break;
		}
		
		// updating the attributes of the label
		cmdData->SetName(theData);

		// Finally, the command is processed:
		ErrorCode error = CmdUtils::ProcessCommand(pstlstCommand);
		// Check for errors, issue warning if so:
		if (error != kSuccess)
		{
			ASSERT_FAIL("PstLstNewDataCmd failed");
		}
		
		return (error);
		
	} while (false);
	
	return (kFailure);
}

/*	PstLstLayoutCSB::CanModifyPstLstData
*/
bool16 PstLstLayoutCSB::CanModifyPstLstData(void)
{
	// if the current selection has at least one drawable page item that has an IPstLstData stored in
	// its IPstLstUIDList, then we will return true so the client code can modify the data object
	if (AllSelectedPstLstAreEmpty())
	{
		return (false);
	}
	else
	{
		return (true);
	}
}

/*	PstLstLayoutCSB::ModifyPstLstData
*/
ErrorCode PstLstLayoutCSB::ModifyPstLstData(const PMString& theData)
{
	/*
	do
	{
		InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
		if (iLayoutTarget == nil)
		{
			ASSERT_FAIL("iLayoutTarget invalid");
			break;
		}
		UIDList		selectUIDList(iLayoutTarget->GetUIDList(kStripStandoffs));

		if (selectUIDList.Length() < 1)
		{
			// We should not be here since CanModifyPstLstData should be called to make sure there
			// is at least one page item who has an IPstLstData, we are modifying that data in this routine.
			ASSERT_FAIL("UIDList invalid");
			break;
		}

		int32 item = selectUIDList.Length();
		// remove those items that does not have IPstLstUIDList, IPstLstUIDList is only aggregated onto
		// kDrawablePageItemBoss
		while (item-- > 0)
		{
			InterfacePtr<IPstLstUIDList> tempData(selectUIDList.GetRef(item), UseDefaultIID());
			if (tempData == nil)
			{
				selectUIDList.Remove(item);
			}
		}
		// If there are no items left in the list, then don't do anything.
		if (selectUIDList.Length() == 0)
		{
			ASSERT_FAIL("no valid page item to apply PstLstModifyDataCmd");
			break;
		}
		
		// item list and settings information are ok, create the command.
		InterfacePtr<ICommand> pstlstCommand(CmdUtils::CreateCommand(kPstLstModifyDataCmdBoss));
		if (pstlstCommand == nil)
		{
			// For some reason we couldn't get our PstLstModifyDataCmd command pointer, so assert and
			// issue a user warning and leave:
			ASSERT_FAIL("PstLstModifyDataCmd invalid");
			break;
		}

		pstlstCommand->SetItemList(selectUIDList);
		
		InterfacePtr<IPstLstData> cmdData(pstlstCommand, UseDefaultIID());		
		if (cmdData == nil)
		{
			// For some reason we couldn't get our IPstLstData pointer, so assert and
			// issue a user warning and leave:
			ASSERT_FAIL("IPstLstData invalid");
			break;
		}
		
		// updating the attributes of the label
		cmdData->SetName(theData);

		// Finally, the command is processed:
		ErrorCode error = CmdUtils::ProcessCommand(pstlstCommand);
		// Check for errors, issue warning if so:
		if (error != kSuccess)
		{
			ASSERT_FAIL("PstLstModifyDataCmd failed");
		}
		
		return (error);
		
	} while (false);
	*/
	return (kFailure);
}

/*	PstLstLayoutCSB::CanDeletePstLstData
*/
bool16 PstLstLayoutCSB::CanDeletePstLstData(void)
{
	// if the current selection has at least one drawable page item that has an IPstLstData stored in
	// its IPstLstUIDList, then we will return true so the client code can delete the data object
	if (AllSelectedPstLstAreEmpty())
	{
		return (false);
	}
	else
	{
		return (true);
	}
}

/*	PstLstLayoutCSB::DeletePstLstData
*/
ErrorCode PstLstLayoutCSB::DeletePstLstData(void)
{
	// no need to handle delete perticular UID(and text) stored with the page item
	/*
	do
	{
		InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
		if (iLayoutTarget == nil)
		{
			ASSERT_FAIL("iLayoutTarget invalid");
			break;
		}
		UIDList					selectUIDList(iLayoutTarget->GetUIDList(kStripStandoffs));

		if (selectUIDList.Length() < 1)
		{
			// We should not be here since CanDeletePstLstData should be called to make sure there
			// is at least one page item who has an IPstLstData, we are deleting that data in this routine.
			ASSERT_FAIL("UIDList invalid");
			break;
		}

		int32 item = selectUIDList.Length();
		// remove those items that does not have IPstLstUIDList, IPstLstUIDList is only aggregated onto
		// kDrawablePageItemBoss
		while (item-- > 0)
		{
			InterfacePtr<IPstLstUIDList> tempData(selectUIDList.GetRef(item), UseDefaultIID());
			if (tempData == nil)
			{
				selectUIDList.Remove(item);
			}
		}
		// If there are no items left in the list, then don't do anything.
		if (selectUIDList.Length() == 0)
		{
			ASSERT_FAIL("No valid page item to apply PstLstDeleteDataCmd");
			break;
		}
		
		// item list and settings information are ok, create the command.
		InterfacePtr<ICommand> pstlstCommand(CmdUtils::CreateCommand(kPstLstDeleteDataCmdBoss));
		if (pstlstCommand == nil)
		{
			// For some reason we couldn't get our PstLstDeleteDataCmd command pointer, so assert and
			// issue a user warning and leave:
			ASSERT_FAIL("PstLstDeleteDataCmd invalid");
			break;
		}

		pstlstCommand->SetItemList(selectUIDList);
		
		// Finally, the command is processed:
		ErrorCode error = CmdUtils::ProcessCommand(pstlstCommand);
		// Check for errors, issue warning if so:
		if (error != kSuccess)
		{
			ASSERT_FAIL("PstLstDeleteDataCmd failed");
		}
		
		return (error);
		
	} while (false);
	*/
	return (kFailure);
}


/*	PstLstLayoutCSB::GetPstLstDataDescription
*/
void PstLstLayoutCSB::GetPstLstDataDescription(PstLstDataVector& pstlstDataVector)
{
	do
	{
		InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
		if (iLayoutTarget == nil)
		{
			ASSERT_FAIL("ILayoutTarget ptr nil, can't get PstLstData");
			break;
		}
		UIDList		selectUIDList(iLayoutTarget->GetUIDList(kStripStandoffs));
		PMString	pstlstDataString = "";
		int32 eleId;
		int32 typid;
		PstLstDataDescription	pstlstData;
		
		const int32 listLength = selectUIDList.Length();
		
		InterfacePtr<IPstLstUIDList> iPstLstUIDList(selectUIDList.GetRef(0), UseDefaultIID());
		if (iPstLstUIDList == nil)
		{
			ASSERT_FAIL("IPstLstUIDList ptr nil, can't get PstLstData");
			break;
		}
		const	UIDList*	uidList = iPstLstUIDList->GetUIDList();
		if (uidList->Length() > 0)
		{
			for (int32 item = 0; item <= uidList->Length()-1; item++)
			{
				UIDRef	uidRef = uidList->GetRef(item);
				if (uidRef.GetDataBase() != nil && uidRef.GetUID() != kInvalidUID)
				{
					
					InterfacePtr<IPstLstData>	iPstLstData(uidRef, UseDefaultIID());
					if (iPstLstData == nil)
					{
						ASSERT_FAIL("Can't get valid PstLst data!");
						pstlstDataString = "";
					}
					else
					{
						pstlstDataString = iPstLstData->GetName();
						iPstLstData->GetOtherInfo(&eleId, &typid);
					}
				}
				/*
				char* buf = new char[11];
				sprintf(buf, "0x%x", uidRef.GetUID());
				PMString	uidStr(buf);
				*/
				PMString temp("");
				temp.AppendNumber(uidRef.GetUID().Get());
				pstlstData.fUIDStr = temp;
				pstlstData.fName = pstlstDataString;
				pstlstData.elementId=eleId;
				pstlstData.typeId=typid;

/* Some probs in setting right slug data 

				listData listdta;
				listInfo listinf;
				int16 retVal;
				int16 lstboxType;
				int16 curRowIndex;

				retVal = listdta.getLstTypeAndCurRowIndex
				(uidRef.GetUID(), &lstboxType, &curRowIndex);
				PMString curBoxUID("Box UID in setting slug ");
				curBoxUID.AppendNumber(uidRef.GetUID().Get());
				CAlert::InformationAlert(curBoxUID);

				if(retVal==1)
				{
					listinf = listdta.getData
						(lstboxType,
						 curRowIndex);
					
					pstlstData.elementId =listinf.id;
					pstlstData.typeId = listinf.typeId;*/
					pstlstDataVector.push_back(pstlstData);
				/*}
				else
				{
					CAlert::InformationAlert("Nope");
				}*/
				//delete [] buf;				
			}
		}
	} while (kFalse);
}

/* PstLstLayoutCSB::GetSelectedItemIndex
*/
void PstLstLayoutCSB::GetSelectedItemIndex(int32& uidListIndex)
{
	do
	{
		InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
		if (iLayoutTarget == nil)
		{
			ASSERT_FAIL("ILayoutTarget ptr nil, can't get selected item index");
			break;
		}
		UIDList		selectUIDList(iLayoutTarget->GetUIDList(kStripStandoffs));
		PMString	pstlstData = "";

		const int32 listLength = selectUIDList.Length();
		
		InterfacePtr<IPstLstUIDList> iPstLstUIDList(selectUIDList.GetRef(0), UseDefaultIID());
		if (iPstLstUIDList == nil)
		{
			ASSERT_FAIL("IPstLstUIDList ptr nil, can't get selected item index");
			break;
		}
		uidListIndex = iPstLstUIDList->GetSelectedIndex();
	} while (kFalse);
}

/* PstLstLayoutCSB::SetSelectedItemIndex
*/
ErrorCode PstLstLayoutCSB::SetSelectedItemIndex(int32 uidListIndex)
{
	/*
	do
	{
		InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
		if (iLayoutTarget == nil)
		{
			ASSERT_FAIL("ILayoutTarget invalid");
			break;
		}
		UIDList					selectUIDList(iLayoutTarget->GetUIDList(kStripStandoffs));
		PMString	pstlstData = "";

		if (selectUIDList.Length() < 1)
		{
			ASSERT_FAIL("UIDList invalid");
			break;
		}

		// The selection list can have items in it that will not have an IPstLstUIDList interface
		// so those will be removed from the selection list before the PstLstSelectDataCmd is sent.
		// Walking backwards through the list makes removing items easier.
		int32 item = selectUIDList.Length();
		// remove those items that does not have IPstLstUIDList, IPstLstUIDList is only aggregated onto
		// kDrawablePageItemBoss
		while (item-- > 0)
		{
			InterfacePtr<IPstLstUIDList> tempData(selectUIDList.GetRef(item), UseDefaultIID());
			if (tempData == nil)
			{
				selectUIDList.Remove(item);
			}
		}

		// We will only allow single selection for this operation, since this is for user to select
		// a different data object from the drop down list, it should be aimed at only its
		// corresponding page item.
		if (selectUIDList.Length() != 1)
		{
			break;
		}
		
		// item list and settings information are ok, create the command.
		InterfacePtr<ICommand> pstlstCommand(CmdUtils::CreateCommand(kPstLstSelectDataCmdBoss));
		if (pstlstCommand == nil)
		{
			// For some reason we couldn't get our PstLstSelectDataCmd command pointer, so assert and
			// issue a user warning and leave:
			ASSERT_FAIL("PstLstSelectDataCmd invalid");
			break;
		}

		InterfacePtr<IIntData> cmdData(pstlstCommand, UseDefaultIID());		
		if (cmdData == nil)
		{
			// For some reason we couldn't get our IIntData pointer, so assert and
			// issue a user warning and leave:
			ASSERT_FAIL("IIntData invalid");
			break;
		}
		
		// updating the attributes of the label
		cmdData->Set(uidListIndex);
		pstlstCommand->SetItemList(selectUIDList);

		// Finally, the command is processed:
		ErrorCode error = CmdUtils::ProcessCommand(pstlstCommand);
		// Check for errors, issue warning if so:
		if (error != kSuccess)
		{
			ASSERT_FAIL("PstLstSelectDataCmd failed");
		}
		
		return (error);
	} while (false);
	*/	
	return (kFailure);
}

/*	PstLstLayoutCSB::DrawablePageItemSelected
*/
bool16 PstLstLayoutCSB::DrawablePageItemSelected()
{
	bool16							drawablePageItemFound = false;
	do
	{
		InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
		if (iLayoutTarget == nil)
		{
			ASSERT_FAIL("ILayoutTarget invalid");
			break;
		}
		const UIDList					selectedItems(iLayoutTarget->GetUIDList(kStripStandoffs));

		// loop thru all the selected items to see if any of it is a drawable page item.
		// we only want to apply the IPstLst data to drawable page item, since the IPstLstUIDList interface
		// is only aggregated to kDrawablePageItemBoss.  
		// so the way to check if it is a drawable page item is to see if it has IPstLstUIDList interface
		for (int32 i = 0; i < selectedItems.Length(); i++)
		{
			InterfacePtr<IPstLstUIDList> tempData(selectedItems.GetRef(i), UseDefaultIID());
			if (tempData != nil)
			{
				drawablePageItemFound = true;
				break;
			}
		}
	} while (kFalse);
	return (drawablePageItemFound);
}

/*	PstLstLayoutCSB::AllSelectedPstLstAreEmpty
	this returns true if all the selected items have empty IPstLstUIDList, in such case, Modify and Delete menu
	will be disabled.
*/
bool16 PstLstLayoutCSB::AllSelectedPstLstAreEmpty()
{
	do
	{
		if (DrawablePageItemSelected())
		{
			InterfacePtr<ILayoutTarget>		iLayoutTarget(this, UseDefaultIID());
			if (iLayoutTarget == nil)
			{
				ASSERT_FAIL("ILayoutTarget invalid");
				break;
			}
			UIDList					selectUIDList(iLayoutTarget->GetUIDList(kStripStandoffs));
			PMString	pstlstData = "";

			const int32 listLength = selectUIDList.Length();
			
			for (int32 i = 0; i < listLength; i++)
			{
				InterfacePtr<IPstLstUIDList> iPstLstUIDList(selectUIDList.GetRef(i), UseDefaultIID());
				if (iPstLstUIDList != nil)
				{
					if (iPstLstUIDList->GetUIDList()->Length() != 0)
						return (false);
				}
			}
			return (true);
		}
		else	// no page item is selected
		{
			return (true);
		}
	} while (kFalse);
	
	return (true);
}

/*	PstLstLayoutCSB::CreateObserverProtocolCollection
*/
ProtocolCollection*	PstLstLayoutCSB::CreateObserverProtocolCollection(void)
{
	ProtocolCollection*	protocolList = new ProtocolCollection();
	protocolList->Insert(IID_IPSTLSTDATA);

	return protocolList;
}

/*	PstLstLayoutCSB::SelectionAttributeChanged
*/
void PstLstLayoutCSB::SelectionAttributeChanged(SuiteBroadcastData* broadcastData, void* message)
{
	ILayoutSelectionContentChangedMessage*	layoutMessage = static_cast<ILayoutSelectionContentChangedMessage*>(message);
	do {
		if (layoutMessage->GetPMIID() == IID_IPSTLSTDATA)
		{
			// check to see if we've been called before and have already signalled
			// that we want client code to be messaged.
			PMIID key(IPstLstSuite::kDefaultIID);
			if (FindLocationInSorted(*broadcastData, key) >= 0)
				break;
			InsertKeyValueInSorted(*broadcastData, static_cast<PMIID>(IID_IPSTLST_ISUITE), K2::shared_ptr<SelectionSuiteData>(nil));
		}
		else	
			ASSERT_FAIL("Selection Attribute changed called for an unregistered protocol.");
	} while(false);
}

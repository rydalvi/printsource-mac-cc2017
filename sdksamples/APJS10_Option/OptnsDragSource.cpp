#include "VCPlugInHeaders.h"

// Interface includes:
#include "IPageItemScrapData.h"
#include "IPageItemLayerData.h"
#include "IPathUtils.h"
#include "IPMDataObject.h"
#include "IDragDropController.h"
#include "IGraphicAttributeUtils.h"
#include "IGraphicAttributeSuite.h"
#include "ISwatchUtils.h"
#include "ISwatchList.h"
#include "ILayoutUtils.h"
#include "IGeometry.h"

// General includes:
#include "SDKUtilities.h"
#include "CAlert.h"
#include "CDragDropSource.h"
#include "UIDList.h"
#include "CmdUtils.h"
#include "UIDRef.h"
#include "K2Vector.h"
#include "CmdUtils.h"
#include "SplineID.h"
#include "PMFlavorTypes.h"
#include "PMRect.h"
#include "SystemUtils.h"
#include "OptnsDragEventFinder.h"
#include "listData.h"
#include "OptnsDlgID.h"
#include "OptnsDragEventFinder.h"
#include "CAlert.h"

#define CA(x)	CAlert::InformationAlert(x)

class OptnsDragSource : public CDragDropSource
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		OptnsDragSource(IPMUnknown* boss);
		/**
			we will only allow the drag if there is an existing document. We faux the creation of a page item,
			this simplifies the database requirements.
			@param e IN the event signifying the start of the drag.
			@return kTrue if we will drag from this location, otherwise kFalse.
		*/
		bool16 			WillDrag(IEvent* e) const;
		/**
			we override this method to add the content we want to drag from this widget.
			@param DNDController IN the controller in charge of this widget's DND capabilities.
			@return kTrue if valid content has been added to the drag
		*/
		bool16			DoAddDragContent(IDragDropController* DNDController);

		/** In our case we have a custom drag item, so the target does not know how to draw feedback for this item.
			Therefore we assume responsibility for this.
			@return the SysRgn representing the feedback we want to convey.
		*/
		SysRgn 			DoMakeDragOutlineRegion() const;

	private:
		/** 
			this method just creates a spline
			@param newPageItem OUT the UIDRef for the created page item.
			@param parent IN the UIDRef for the parent that the new item should be attached to.
			@param points IN the pair of points that define the spline.
			@param strokeWeight IN the initial stroke applied to the path for the new page item.
			@return kSuccess if the page item is created without error, kFailure otherwise.
		*/
		ErrorCode CreatePageItem(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);
		ErrorCode CreatePicturebox(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);
		ErrorCode CreateTextbox(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);

		/** Cache the page item */
		UIDRef fPageItem;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(OptnsDragSource, kOptnsDragSourceImpl)

/* OptnsDragSource Constructor
*/
OptnsDragSource::OptnsDragSource(IPMUnknown* boss)
: CDragDropSource(boss)
{
}

/*
	We indicate we are only interested in dragging if there is a document open. This is making the assumption that we
	want to drag the item onto the layout widget, but also allows us to create the page item within the context of that document.
*/
bool16
OptnsDragSource::WillDrag(IEvent* e) const
{
	IDocument * theFrontDoc = Utils<ILayoutUtils>()->GetFrontDocument();
	if (theFrontDoc == nil)
		return kFalse;

	listData genPanelListData;

	bool16 boxDoesExistOnPage  = genPanelListData.checkIfBoxIsDragged
	(OptnsDragEventFinder::listboxType, 
	 OptnsDragEventFinder::selectedRowIndex);
	if(boxDoesExistOnPage==kTrue)
	{
	//	CA("Item already exists on Page");
		return kFalse;
	}
	
	return kTrue;
}

/*
	We override the DoAddDragContent method to define the content for the drag, 
	in this case a simple spline item is added. Note, this is just a simple 
	implementation for demonstration purposes. 
*/
bool16
OptnsDragSource::DoAddDragContent(IDragDropController* DNDController)
{
	//CA("coming here before drag starts");
	bool16 result = kFalse;
	// do while(false) loop for error control
	do 
	{

		// Get the dataexchangehandler for the flavor we want to add 
		// The method QueryHandler is on the dataexchangecontroller, which adds extra functionality
		// to the dragdropcontroller. I am assuming we drag from our custom source to custom target
		// via the layout widget, if we attempt to go directly, the handler will be nil.
		InterfacePtr<IDataExchangeHandler> ourHandler(DNDController->QueryHandler(ourFlavor1));
		// if we cannot get the handler for our flavor, we are dead in the water.
		// The handler gives us the scrap to store our content on...
		if (ourHandler==nil)
		{
			ASSERT_FAIL("Handler nil for our flavor?");
			break;
		}	
		// we need to place our content onto the scrap
		InterfacePtr<IPageItemScrapData> scrapData(ourHandler, UseDefaultIID());
		if (scrapData == nil)
		{
			ASSERT_FAIL("No scrap data for DEHandler?");
			break;
		}
		
		// get the root of the scrap	
		UIDRef 	scrapRoot =	scrapData->GetRootNode();
		// and its associated database
		IDataBase * scrapDB = scrapRoot.GetDataBase();

		// new page item
		UIDRef newPageItem;
		// set up a list of points for the page item
		PMPointList points(2);
		// define the text bounding box using two pmpoints
		PMPoint startPoint(0,0);
		PMPoint endPoint(100,100);
		points.Append( startPoint );
		points.Append( endPoint );
		PMReal strokeWeight(0.0);
		if(OptnsDragEventFinder::isImgflag==kFalse)
		{
			if(CreateTextbox(newPageItem,scrapRoot,points,strokeWeight)==kFailure)
			{
				ASSERT_FAIL("Failed to create page item?");
				break;
			}
		}
		if(OptnsDragEventFinder::isImgflag==kTrue)
		{
			if(CreatePicturebox(newPageItem,scrapRoot,points,strokeWeight)==kFailure)
			{
				ASSERT_FAIL("Failed to create page item?");
				break;
			}
		}
	//	OptnsDragEventFinder::currentSelectedItemUIDRef=newPageItem;
		// Must manually clear the handler before adding any data...
		ourHandler->Clear();
		
		// we call replace to define the DB the item exists in. We add our new item to the scrap
		scrapData->Replace(UIDList(newPageItem));
		// Update the cached variable
		fPageItem = newPageItem;
/*		InterfacePtr<IPageItemLayerData> layerData(scrapData,IID_IPAGEITEMLAYERDATA);
		// I faux the layer the item is on.
		K2Vector<int32> layerIndexList(1);
		layerIndexList.Append(0);

		// Make a list of the layer names.
		K2Vector<PMString> layerNameList;
		layerNameList.Preallocate(1);
		PMString layerName("Untitled");
		layerNameList.Append(layerName);

		layerData->SetPageItemList(UIDList(newPageItem));
		layerData->SetLayerNameList(layerNameList);
		layerData->SetLayerIndexList(layerIndexList);
		layerData->SetIsGuideLayer(kFalse);
*/
		
		// we point the controller at the pageitem handler 
		DNDController->SetSourceHandler(ourHandler);
			
		// we get the data object that represents the drag
		InterfacePtr<IPMDataObject> item(DNDController->AddDragItem(1));

		// no flavor flags	
		PMFlavorFlags flavorFlags = 0;
		
		// we set the type (flavour) in the drag object 
		item->PromiseFlavor(ourFlavor1, flavorFlags);
		
 		result = kTrue;
	} while (false);
/*
	if(result==kTrue)
	{
		if(DNDController->IsTargetSameAsSource())
			OptnsDragEventFinder::didUserDragBox=kFalse;
		else
			OptnsDragEventFinder::didUserDragBox=kTrue;
	}
	else
		OptnsDragEventFinder::didUserDragBox=kFalse;
*/
	return result; 
}

/* As we have a custom flavour, the drag target is unlikely to know how to draw feedback, therfore we draw it in the source */
SysRgn
OptnsDragSource::DoMakeDragOutlineRegion() const
{
	// do while(false) loop for error control
	do 
	{
		// get the geometry of the page item
		InterfacePtr<IGeometry> iGeometry(fPageItem,UseDefaultIID());
		if (iGeometry == nil)
		{
			ASSERT_FAIL("No geometry on scrap item!");
			break;
		}
		
		// we will draw a rectangle around the bounding box
		PMRect thePIGeo = iGeometry->GetStrokeBoundingBox();
	
		// we need to draw the bounding box at the current mouse location
		SysPoint currentMouse = GetMousePosition();
		PMPoint start(currentMouse);
		
		PMPoint lTop(thePIGeo.LeftTop());
		PMPoint rBottom(thePIGeo.RightBottom());

		lTop+=start;
		rBottom+=start;
		// create a new pmrect based on the page item, offset from the mouse location
		// On windows this feedback resorts to the default, on the mac it would be 
		PMRect offsetRect(lTop,rBottom);

		SysRect windowRect = ::ToSys(offsetRect);
		SysRgn origRgn = ::CreateRectSysRgn(windowRect);

		// Get the window region for the rectangle.
		::InsetSysRect(windowRect,7,7);
		
		SysRgn windowRgn = ::CreateRectSysRgn(windowRect);

		DiffSysRgn(origRgn,windowRgn,origRgn);
		return origRgn;
	}
	while (kFalse);
	return nil;
}

/* this method takes in a points list, stroke weight and parent UIDRef and creates a spline with those points, the stroke weight
	and attaches it to the parent.
*/
ErrorCode 
OptnsDragSource::CreatePageItem(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{
	ErrorCode returnValue = kFailure;
	do 
	{
		// wrap the commands in a sequence
		ICommandSequence *sequence = CmdUtils::BeginCommandSequence();
		if (sequence == nil)
		{
			ASSERT_FAIL("Cannot create command sequence?");
			break;
		}

		// create the page item
		UIDRef newSplineItem = Utils<IPathUtils>()->CreateLineSpline(parent, points, INewPageItemCmdData::kDefaultGraphicAttributes);
		
		// put the new item into a splinelist
		const UIDList splineItemList(newSplineItem);

		// we apply the stroke to our item
		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==nil)
		{
			ASSERT_FAIL("Cannot create the command to stroke the spline?");
			break;
		}

		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
		{
			ASSERT_FAIL("Failed to stroke the spline?");
			break;
		}

		// we want to apply the stroke, color it black. We need to get the black swatch...
		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		if(iSwatchList==nil)
		{
			ASSERT_FAIL("Cannot get swatch list?");
			break;
		}
		UID blackUID = iSwatchList->GetBlackSwatchUID();
		
		// Now get the command to apply the stroke...
		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
		if(applyStrokeCmd==nil)
		{
			ASSERT_FAIL("Cannot create the command to render the stroke?");
			break;
		}

		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		{
			ASSERT_FAIL("Failed to render the stroke?");
			break;
		}
		
		// now we can end the command sequence, we are done.
		CmdUtils::EndCommandSequence(sequence);
		newPageItem = newSplineItem;
		returnValue=kSuccess;
	} while(false);
	return returnValue;
}

ErrorCode 
OptnsDragSource::CreatePicturebox(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{
	ErrorCode returnValue = kFailure;
	do 
	{
		ICommandSequence *sequence = CmdUtils::BeginCommandSequence();
		if (sequence == nil)
		{
			ASSERT_FAIL("Cannot create command sequence?");
			CAlert::InformationAlert("ICommandSequence nil");
			break;
		}

		PMPoint topLeft(5,5);
		PMPoint bottomRight(100,100);
		PMRect rect(topLeft, bottomRight);
		UIDRef newSplineItem = Utils<IPathUtils>()->CreateRectangleSpline
		(parent, rect, INewPageItemCmdData::kGraphicFrameAttributes);

		const UIDList splineItemList(newSplineItem);

		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==nil)
		{
			ASSERT_FAIL("Cannot create the command to stroke the spline?");
			CAlert::InformationAlert("ICommand nil");
			break;
		}
				
		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
		{
			ASSERT_FAIL("Failed to stroke the spline?");
			CAlert::InformationAlert("ProcessCommand failure");
			break;
		}

		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		if(iSwatchList==nil)
		{
			ASSERT_FAIL("Cannot get swatch list?");
			CAlert::InformationAlert("ISwatchList nil");
			break;
		}
		UID blackUID = iSwatchList->GetBlackSwatchUID();
		
		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
		if(applyStrokeCmd==nil)
		{
			ASSERT_FAIL("Cannot create the command to render the stroke?");
			CAlert::InformationAlert("ICommand nil");
			break;
		}

		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		{
			ASSERT_FAIL("Failed to render the stroke?");
			CAlert::InformationAlert("ProcessCommand failre");
			break;
		}
		
		CmdUtils::EndCommandSequence(sequence);
		newPageItem = newSplineItem;
		returnValue=kSuccess;
	} while(false);
	return returnValue;
}


ErrorCode 
OptnsDragSource::CreateTextbox(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{
	ErrorCode returnValue=kFailure;
	do 
	{
		
		ICommandSequence *sequence=CmdUtils::BeginCommandSequence();
		if (sequence==nil)
		{
			ASSERT_FAIL("Cannot create command sequence?");
			CAlert::InformationAlert("ICommandSequence nil");
			break;
		}

		PMPoint topLeft(5,5);
		PMPoint bottomRight(100,100);
		PMRect rect(topLeft, bottomRight);
		UIDRef newSplineItem = Utils<IPathUtils>()->CreateRectangleSpline
		(parent, rect, INewPageItemCmdData::kTextFrameAttributes, 0x263);

		const UIDList splineItemList(newSplineItem);

		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==nil)
		{
			ASSERT_FAIL("Cannot create the command to stroke the spline?");
			CAlert::InformationAlert("ICommand nil");
			break;
		}

		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
		{
			ASSERT_FAIL("Failed to stroke the spline?");
			CAlert::InformationAlert("ProcessCommand failure");
			break;
		}

		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		if(iSwatchList==nil)
		{
			ASSERT_FAIL("Cannot get swatch list?");
			CAlert::InformationAlert("ISwatchList nil");
			break;
		}
		UID blackUID = iSwatchList->GetBlackSwatchUID();
		
		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
		if(applyStrokeCmd==nil)
		{
			ASSERT_FAIL("Cannot create the command to render the stroke?");
			CAlert::InformationAlert("ICommand nil");
			break;
		}

		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		{
			ASSERT_FAIL("Failed to render the stroke?");
			CAlert::InformationAlert("ProcessCommand failre");
			break;
		}


		CmdUtils::EndCommandSequence(sequence);
		newPageItem=newSplineItem;
		returnValue=kSuccess;
	} while(false);
	return returnValue;
}
// End, OptnsDragSource.cpp.






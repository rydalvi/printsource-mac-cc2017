

#ifndef __PubBrowseDlgCreator__
#define __PubBrowseDlgCreator__

// Interface includes:
#include "LocaleSetting.h"
#include "IDialogMgr.h"
#include "IApplication.h"
#include "ISession.h"
#include "CAlert.h"

// Implementation includes:
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"

// Project includes:
#include "OptnsDlgID.h"

/** PubBrowseDlgCreator
	provides management and control over the dialog. 
  
	PubBrowseDlgCreator implements IDialogCreator based on
	the partial implementation CDialogCreator. 
	@author Rodney Cook
*/
class PubBrowseDlgCreator : public CDialogCreator
{
	public:
		/**
			Constructor.

			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		PubBrowseDlgCreator(IPMUnknown *boss) : CDialogCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~PubBrowseDlgCreator() {}

		/** 
			Creates a dialog from the resource that is cached by 
			the dialog manager until invoked.
		*/
		virtual IDialog* CreateDialog();	
};


#endif
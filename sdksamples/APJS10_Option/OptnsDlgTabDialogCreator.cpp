/*
//	File:	OptnsDlgTabDialogCreator.cpp
//
//	Date:	22-Feb-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/
#include "VCPlugInHeaders.h"

// Interface includes:
#include "LocaleSetting.h"
#include "IDialogMgr.h"
#include "IApplication.h"
#include "ISession.h"
#include "CAlert.h"
// Implementation includes:
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"

// Project includes:
#include "OptnsDlgID.h"

/** OptnsDlgTabDialogCreator
	provides management and control over the dialog. 

	SelDlgDialogCreator implements IDialogCreator based on
	the partial implementation CDialogCreator. 

	@author Rodney Cook
*/

class OptnsDlgTabDialogCreator : public CDialogCreator
{
	public:
		/**
			Constructor.

			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		OptnsDlgTabDialogCreator(IPMUnknown* boss) : CDialogCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~OptnsDlgTabDialogCreator() {}

		/** 
			Creates a dialog from the resource that is cached by 
			the dialog manager until invoked.
		*/
		virtual IDialog* CreateDialog();

		/** 
			Returns the resource ID of the resource containing an
			ordered list of panel IDs.

			@param classIDs an ordered list of class IDs of selectable dialogs that are to be installed in this dialog
		*/
		virtual void GetOrderedPanelIDs(K2Vector<ClassID>& classIDs);

		/** 
			Returns an ordered list of class IDs of selectable dialogs
			that are to be installed in this dialog.
		*/
		virtual RsrcID GetOrderedPanelsRsrcID() const;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(OptnsDlgTabDialogCreator, kOptnsDlgTabDialogCreatorImpl)

/* CreateDialog
*/
IDialog* OptnsDlgTabDialogCreator::CreateDialog()
{
	IDialog* dialog = nil;

	//CAlert::InformationAlert("SelDlgTabDialog Creator");

	// Use a do-while(false) so we can break on bad pointers:
	do
	{
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());//CS4
		InterfacePtr<IDialogMgr> dialogMgr(app, UseDefaultIID());
		if (dialogMgr == nil)
		{
			ASSERT_FAIL("OptnsDlgTabDialogCreator::CreateDialog: dialogMgr invalid");
			break;
		}

		// We need to load the plug-ins resource:
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,								// Locale index from PMLocaleIDs.h. 
			kOptnsDlgPluginID,			// Our Plug-in ID from SelectableDialog.h. 
			kViewRsrcType,						// This is the kViewRsrcType.
			kOptnsDlgTabDialogResourceID,	// Resource ID for our dialog.
			kTrue									// Initially visible.
		);

		// CreateDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).	
		// The dialog manager caches the dialog for us:
		dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);

		// We want initial focus to be in the selection list.
		dialog->SetDialogFocusingAlgorithm(IDialog::kNoAutoFocus);
	} while (false); // Only do once.

	return dialog;
}

/*	GetOrderedPanelIDs
*/
void OptnsDlgTabDialogCreator::GetOrderedPanelIDs(K2Vector<ClassID>& classIDs)
{
	ResourceEnabler rsrcEnable;
	CDialogCreator::GetOrderedPanelIDs(classIDs);
}

/* GetOrderedPanelsRsrcID
*/
RsrcID OptnsDlgTabDialogCreator::GetOrderedPanelsRsrcID() const
{
	return kOptnsDlgPanelOrderingResourceID;
}

// End, OptnsDlgTabDialogCreator.cpp

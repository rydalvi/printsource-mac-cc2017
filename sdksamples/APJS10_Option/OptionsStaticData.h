
#ifndef PERSISTDAT_H
#define PERSISTDAT_H

#include"PMString.h"
#include"IPanelControlData.h"
#include"OptionsValue.h"
#include "k2Vector.tpp"
#include "vector"
using namespace std;

class PersistData
{	
	//static InterfacePtr<IPanelControlData> generalPanelControlData;
	static IPanelControlData *generalPanelControlData;
	static OptionsValue curOptions;
	static double PubID;
	static PMString PubName;
	static PMString LocaleName;
	static double LocaleID;
	
	// 1 showing with options dialog, 2 stand alone, 0 invalid
	static bool16 PublicationDialogType;
	static bool16 PathBrowseDialogType;
	static bool8  isPubBrowseCancelPress;
	
public:

	static double getPubId(void);
	static void  setPubId(double);

	static PMString getPubName(void);
	static void setPubName(PMString);
	static PMString getLocaleName(void);
	static void setLocaleName(PMString);
	static OptionsValue& getOptions(void);
	static void  setOptions(OptionsValue&);
	//static InterfacePtr<IPanelControlData> getGeneralPanelControlData(void);
	//static void setGeneralPanelControlData(InterfacePtr<IPanelControlData> );
	static IPanelControlData *getGeneralPanelControlData(void);
	static void setGeneralPanelControlData(IPanelControlData *);

	static bool16 getPublicationDialogType(void);
	static void   setPublicationDialogType(bool16);

	static bool8  getPubBrowseDlgCancelPressFlag(void);
	static void   setPubBrowseDlgCancelPressFlag(bool8);

	static bool16 getPathBrowseDialogType(void);
	static void   setPathBrowseDialogType(bool16);

	static double getLocaleID(void);
	static void setLocaleID(double);
};


class PubBrowseDropDownListData
{
public:
	PMString name;
	double ID;

};

typedef vector <PubBrowseDropDownListData> VectorPubBrowseDropDownListData;


#endif
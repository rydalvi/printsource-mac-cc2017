/*
//	File:	PubBrowseDlgController.cpp
//
//	Date:	5-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"
// Interface includes:
// none.
// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "CAlert.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "IPanelControlData.h"
#include "IControlView.h"
// Project includes:
#include "OptnsDlgID.h"
#include "OptionsUtils.h"
#include "OptionsStaticData.h"
//#include "IAppFramework.h"
//#include "ApplicationFrameHeader.h"
#include "IAppFramework.h"
#include "ISpecialChar.h"

#include "ILoginHelper.h"


#define CA(X) CAlert::InformationAlert(X);

/** PubBrowseDlgController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author raghu
*/
//long SelectedLocaleID = -1;
double SelectedLocaleID = -1;
extern bool16 doRefreshFlag;

double clientId = 0;
VectorPubBrowseDropDownListData LanguageDropDwonList;
VectorPubBrowseDropDownListData CatalogDropDownList;

class PubBrowseDlgController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		PubBrowseDlgController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~PubBrowseDlgController() {}


		/**
			Initializes each widget in the dialog with its default value.
			Called when the dialog is opened.
			@param dlgContext
		*/
		virtual void InitializeDialogFields( IActiveContext*);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			this method to be called. When all widgets are valid, 
			ApplyFields will be called.		
			@param myContext
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields( IActiveContext*);

		/**
			Retrieve the values from the widgets and act on them.
			@param myContext
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields( IActiveContext* , const WidgetID& );
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(PubBrowseDlgController, kPublicationDialogControllerImpl)

/* ApplyFields
*/
void PubBrowseDlgController::InitializeDialogFields( IActiveContext* dlgContext)
{
	//CA("PubBrowseDlgController::InitializeDialogFields");
	int i=0;
	int j=0;
	
	CDialogController::InitializeDialogFields(dlgContext);

	doRefreshFlag = kTrue;
	
	InterfacePtr<IStringListControlData> LocaleDropListData(
		this->QueryListControlDataInterface(kLocaleDropDownWidgetID));
	if(LocaleDropListData == nil){
		CAlert::InformationAlert("LocaleDropListData nil");
		return;
	}

	LocaleDropListData->Clear(kFalse,kFalse);		
	InterfacePtr<IDropDownListController> LocaleDropListControllerx(LocaleDropListData,UseDefaultIID());
	if (LocaleDropListControllerx == nil){
		CAlert::InformationAlert("LocaleDropListControllerx nil");
		return;
	}

	//AppFramework* ptrIAppFramework = new AppFramework(); commented by vaibhav
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return ;
	}
	
	//VectorLocaleValuePtr VectorLocValPtr = ptrIAppFramework->getAllLocales(); @vaibhav
	/*bool16 clearFlag =*/ ptrIAppFramework->StructureCache_clearInstance();
	LanguageDropDwonList.clear();
	/*if(clearFlag)
		CA("Language Cache Clear.");*/

	//********** Get all languages

	VectorLanguageModelPtr VectorLocValPtr = ptrIAppFramework->StructureCache_getAllLanguages();
	if(VectorLocValPtr == NULL)
	{
	//	CAlert::InformationAlert("VectorLocValPtr == NULL");
		return;
	}
	
	//VectorLocaleValue::iterator it; @vaibhav
	VectorLanguageModel::iterator it;
	
	for(it = VectorLocValPtr->begin(); it != VectorLocValPtr->end(); it++)
	{		
		//CA(it->getLangugeName());    // just for testing only 27 Jan
		PMString localename(it->getLangugeName());
		LocaleDropListData->AddString(localename, j, kFalse, kFalse);

		PubBrowseDropDownListData LanguageRowObject;
		LanguageRowObject.name = localename;
		LanguageRowObject.ID = it->getLanguageID();
		LanguageDropDwonList.push_back(LanguageRowObject);
		j++;

	}
	
	if(VectorLocValPtr->size() == 1)
		LocaleDropListControllerx->Select(0);
	else
		LocaleDropListControllerx->Select(-1);
	//LocaleDropListControllerx->Select(0);
	// *********** Set selected language in the drop down list

	ClientOptionsManip cops;
	OptionsValue optnsObj = cops.ReadClientOptions();
	
	j=0;

	PMString curLocaleName = optnsObj.getDefaultLocale();
	//CAlert::InformationAlert("curLocaleName : " + curLocaleName);
//////////////////////////////////////	25MAR09
	//CUserInfoValue usrProfile = ptrIAppFramework->LOGINMngr_getUserProfile();
	//int32 clientID = -1;
	//LoginInfoValue cserverInfoValue ;
	
	//PMString s("clientId	:");	
	//s.AppendNumber(clientId);
	//s.Append("\nusrProfile.getClientID()	:	");
	//s.AppendNumber(usrProfile.getClientId());
	//CA(s);
	if(clientId != ptrIAppFramework->getClientID())
	{
		//CA("clientID != usrProfile.getClientId()");
		curLocaleName = VectorLocValPtr->at(0).getLangugeName();
	//	PMString w("curLocaleName	:	");
	//	w.Append(curLocaleName);
	//	w.Append("\nVectorLocValPtr->at(0).getLanguageID()	:	");
	//	w.AppendNumber(VectorLocValPtr->at(0).getLanguageID());
		//CA(w);
		optnsObj.setDefaultLocale(curLocaleName);
		clientId = ptrIAppFramework->getClientID();
		//CA("localename == localename");
	}
	
///////////////////////////////////////
	for(it = VectorLocValPtr->begin(); it != VectorLocValPtr->end(); it++)
	{			
		PMString localename(it->getLangugeName());			
		//CAlert::InformationAlert("localename	:	"+localename);
		if(localename == curLocaleName){
			//CA("localename == localename");
		
			LocaleDropListControllerx->Select(j);
			SelectedLocaleID = it->getLanguageID();
			ptrIAppFramework->setLocaleId(SelectedLocaleID);
			break;
		}
		j++;
	}	
//PMString s1("SelectedLocaleID	:");	
//s1.AppendNumber(SelectedLocaleID);
//CA(s1);
		//}while(0);
	InterfacePtr<IStringListControlData> PubDropListData(
		this->QueryListControlDataInterface(kPubDropDownWidgetID));
	if(PubDropListData == nil){
		//CAlert::InformationAlert("PubDropListData nil");
		return;
	}

	//****** clearing Catalog list
	PubDropListData->Clear(kFalse,kFalse);		
	InterfacePtr<IDropDownListController> PubDropListController(PubDropListData,UseDefaultIID());
	if (PubDropListController == nil){
		//CAlert::InformationAlert("PubDropListController nil");
		return;
	}

	int32 selectedRow1 = LocaleDropListControllerx->GetSelected();
	InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
	if (panelControlData == nil){
		//CAlert::InformationAlert("panelControlData nil");
		return;
	}
	IControlView* controlView = panelControlData->FindWidget(kPubDropDownWidgetID);
	if (controlView == nil){
		//CAlert::InformationAlert("Publication Browse Button ControlView nil");
		return;
	}
	IControlView* controlView1 = panelControlData->FindWidget(kOKButtonWidgetID);
	if (controlView1 == nil){
		CAlert::InformationAlert("Publication Browse Button ControlView nil");
		return;
	}
	if(selectedRow1<0){	//CA("selectedRow1<0");	 ///*** if no language is selected then disable catalog list & ok button
		controlView1->Disable(kTrue); 
		controlView->Disable(kTrue);
		return;
	}
	else
	{   //CA("selectedRow1<0  else");
		controlView->Enable();
		controlView1->Enable();
	}
    
	do{
		//********** Now goint to fill Catalog list(second drop down )
		// Put code to initialize widget values here.
		/*InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Interface for IAppFramework not found.");
			break;
		}	*/
		if(SelectedLocaleID == -1)
			break;
		//VectorCatalogValuePtr VectorCataloglInfoValuePtr = ptrIAppFramework->getAllCatalogsByLocaleID(SelectedLocaleID);		 @vaibhav

		/*bool16 ClarFlag1 =*/ ptrIAppFramework->EventCache_clearInstance();
		/*if(ClarFlag1)
			CA("ClarFlag1 ProjectCache Cleared");*/

//added by Tushar on 20/09 START////////////////////////////
		VectorClassInfoPtr vectClassInfoValuePtr = 
			ptrIAppFramework->ClassificationTree_getRoot(1); // 1 default for English
	
		if(vectClassInfoValuePtr==nil)
		{		
			//CA("vectClassInfoValuePtr==nil");
			break;
		}

		VectorClassInfoValue::iterator it1;
		i=0;
	
		CatalogDropDownList.clear();

		//*** Adding ONEsource classes to Catalog list
		//***Added by mane*************************
		for(it1=vectClassInfoValuePtr->begin(); it1!=vectClassInfoValuePtr->end(); it1++)
		{
			InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));

			PMString tempclassificationName; //mane
			//int32 tempclassificationId;
			if(!iConverter)
			{
				//CA("tempclassificationName");
				tempclassificationName = it1->getClassification_name(); //mane
				//tempclassificationId = it1->getClass_id();
			}
			else
			{
				//CA("!tempclassificationName");
				tempclassificationName = iConverter->translateString(it1->getClassification_name());//mane
			}
		
			PubDropListData->AddString(tempclassificationName, i, kFalse, kFalse);
			PubBrowseDropDownListData CatalogRowObject;
			CatalogRowObject.name = tempclassificationName;
			CatalogRowObject.ID = it1->getClass_id();
			CatalogDropDownList.push_back(CatalogRowObject);
			i++;
		}
		
////added by Tushar on 20/09 END///////////////////////////////////		
		//**** getting  all projects (Publications)
		VectorPubModelPtr VectorPublInfoValuePtr = ptrIAppFramework->ProjectCache_getAllProjects(SelectedLocaleID);
		//VectorPubInfoPtr VectorPublInfoValuePtr = ptrIAppFramework->PUBMngr_findSectionById(101,TRUE);
		//VectorPubInfoPtr VectorPublInfoValuePtr = ptrIAppFramework->PUBMngr_findByLevel(120);
	
		VectorPubModel::iterator it;
		if(VectorPublInfoValuePtr != nil){
	
			//VectorCatalogValue::iterator it;	 @vaibhav			
			for(it = VectorPublInfoValuePtr->begin(); it != VectorPublInfoValuePtr->end(); it++)
			{	
				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				
				PMString CatalogName("");
				PMString TempBuffer("");

				//if(ptrIAppFramework->CONFIGCACHE_getCmShowEventNumber())
				//{
				//	TempBuffer = 	it->getNumber()+"-"+it->getName();					 					
				//}
				//else					
					TempBuffer = it->getName();

				if(iConverter){ 
					CatalogName=iConverter->translateString(TempBuffer);
				}
				else{ 				
					CatalogName = TempBuffer;
				}
				//PMString CatalogName(it->getName()); /* it->getcatalog_name() */			
				PubDropListData->AddString(CatalogName, i, kFalse, kFalse);

				PubBrowseDropDownListData CatalogRowObject;
				CatalogRowObject.name = CatalogName;
				CatalogRowObject.ID = it->getEventId();
				CatalogDropDownList.push_back(CatalogRowObject);

				i++;
			}
			//PubDropListController->Select(-1);	
			PubDropListController->Select(0);
		}
		
		ClientOptionsManip cops;
		OptionsValue optnsObj = cops.ReadClientOptions();
		i=0;

//****************************************************ADDED BY MANE****************************		
		double pID = PersistData::getPubId();
		
		if(vectClassInfoValuePtr != nil)
		{
			
			for(it1=vectClassInfoValuePtr->begin(); it1!=vectClassInfoValuePtr->end(); it1++)
			{
				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				double tempclassificationId;
				if(!iConverter)
				{
					tempclassificationId = it1->getClass_id();
				}
				else
				{
					tempclassificationId = it1->getClass_id();
				}
				if(tempclassificationId ==  pID){
					PubDropListController->Select(i);
					ptrIAppFramework->setCatalogId(it1->getClass_id());
					controlView->Enable();
					break;
				}
				i++;
			}
		}	

		/*if(VectorPublInfoValuePtr == nil)
		{
			break;
		}*/

		if(VectorPublInfoValuePtr != nil)
		{  
			for(it = VectorPublInfoValuePtr->begin(); it != VectorPublInfoValuePtr->end(); it++)
			{			
				InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				double curPubid = it->getEventId();
				
				if(pID == curPubid)	{
					PubDropListController->Select(i);
					ptrIAppFramework->setCatalogId(it->getEventId());
					controlView->Enable();
					break;
				}
				i++;
			}	

		}
/////////////////	26MAR09
		if(optnsObj.getDefaultLocale().NumUTF16TextChars() < 1)
		{	
			optnsObj.setDefaultLocale(curLocaleName);
			PMString locID("");
			locID.AppendNumber(PMReal(SelectedLocaleID));
			
			optnsObj.setLocaleID(locID);
			cops.WriteClientOptions(optnsObj);
			PersistData::setLocaleName(curLocaleName);
			PersistData::setLocaleID(SelectedLocaleID);
		}
		if(optnsObj.getPublicationName().NumUTF16TextChars() < 1)
		{
            PMString asd = PubDropListData->GetString(0);
			optnsObj.setPublicationName(asd);
			double pubID = 0;
			if(vectClassInfoValuePtr != nil)
				pubID = vectClassInfoValuePtr->at(0).getClass_id();
			else
				if(VectorPublInfoValuePtr != nil)
					pubID = VectorPublInfoValuePtr->at(0).getEventId();
			PMString pubIDStr("");
			pubIDStr.AppendNumber(PMReal(pubID));
			
			optnsObj.setProjectID(pubIDStr);
			cops.WriteClientOptions(optnsObj);
			PersistData::setPubName(PubDropListData->GetString(0));
			PersistData::setPubId(pubID);
		}

////////////////	END

		if(VectorPublInfoValuePtr !=nil)
			delete VectorPublInfoValuePtr;
	}while(0);

	//int32 selectedRow = PubDropListController->GetSelected();		
	//if(selectedRow<0){	// OKButton View	
	//	controlView->Disable(kTrue);
	//}
	//else
	//	controlView->Enable();

	PersistData::setPubBrowseDlgCancelPressFlag(FALSE);
	//if(ptrIAppFramework !=nil)  commented by vaibhav
			//delete ptrIAppFramework;
}

/* ValidateFields
*/
WidgetID PubBrowseDlgController::ValidateDialogFields( IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);

	// Put code to validate widget values here.

	return result;
}

/* ApplyFields
*/
void PubBrowseDlgController::ApplyDialogFields( IActiveContext* myContext, const WidgetID& widgetId)
{
	// Replace with code that gathers widget values and applies them.
	//SystemBeep();
}
//  Generated by Dolly build 17: template "Dialog".
// End, PubBrowseDlgController.cpp.


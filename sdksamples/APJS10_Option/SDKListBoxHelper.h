#ifndef __SDKListBoxHelper_h__
#define __SDKListBoxHelper_h__

class IPMUnknown;
class IControlView;
class IPanelControlData;

class SDKListBoxHelper
{
public:
	SDKListBoxHelper(IPMUnknown * fOwner, int32 pluginId);
	virtual ~SDKListBoxHelper();
	void AddElement(IControlView* lstboxControlView, const PMString & displayName,   WidgetID updateWidgetId, int atIndex = -2 /* kEnd */, int x=1);
	void RemoveElementAt(int indexRemove, int x);
	void RemoveLastElement( int x);
	IControlView * FindCurrentListBox(InterfacePtr<IPanelControlData> panel, int x);
	void EmptyCurrentListBox(InterfacePtr<IPanelControlData> panel, int x);
	int GetElementCount(int x);
	void CheckUncheckRow(IControlView* listboxCntrlView, int32, bool);
private:
	bool16 verifyState() { return (fOwner!=nil) ? kTrue : kFalse; }
	void addListElementWidget(IControlView* lstboxControlView, InterfacePtr<IControlView> & elView, const PMString & displayName, WidgetID updateWidgetId, int atIndex, int x);
	void removeCellWidget(IControlView * listBox, int removeIndex);
	IPMUnknown * fOwner;
	int32 fOwnerPluginID;
};

#endif 




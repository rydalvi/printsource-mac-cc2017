/*
//	File:	GeneralPanelCreator.cpp
//
//	Date:	07-Jun-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

//Implementation includes:
#include "CPanelCreator.h"
#include "CAlert.h"

// Project includes:
#include "OptnsDlgID.h"

/** GeneralPanelCreator
	implements IPanelCreator based on the partial implementation CPanelCreator.
	We override CPanelCreator's GetPanelRsrcID().
	@author Rodney Cook
*/
class GeneralPanelCreator : public CPanelCreator
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		GeneralPanelCreator(IPMUnknown *boss) : CPanelCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~GeneralPanelCreator() {}

		/** 
			Returns the local resource ID of the ordered panels.
		*/
		virtual RsrcID GetPanelRsrcID() const;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(GeneralPanelCreator, kGeneralPanelCreatorImpl)

/* GetPanelRsrcID
*/
RsrcID GeneralPanelCreator::GetPanelRsrcID() const
{
	//CAlert::InformationAlert("YinPanel Creator");
	return kGeneralPanelCreatorResourceID;
}

// End, GeneralPanelCreator.cpp



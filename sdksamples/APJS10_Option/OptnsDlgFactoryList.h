//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================
REGISTER_PMINTERFACE(OptnsDlgActionComponent, kOptnsDlgActionComponentImpl)
REGISTER_PMINTERFACE(OptnsDlgDialogController,kOptnsDlgDialogControllerImpl)
REGISTER_PMINTERFACE(OptnsDlgDialogObserver, kOptnsDlgDialogObserverImpl)

REGISTER_PMINTERFACE(OptnsDlgDialogCreator,   kOptnsDlgDialogCreatorImpl)
REGISTER_PMINTERFACE(SelDlgTabDialogCreator,  kOptnsDlgTabDialogCreatorImpl)

REGISTER_PMINTERFACE(GeneralPanelCreator,	kGeneralPanelCreatorImpl)
REGISTER_PMINTERFACE(GeneralPanelObserver,	kGeneralPanelObserverImpl)
//REGISTER_PMINTERFACE(OkButtonObserver,		kOptnsTabOKBtnObserverImpl)
REGISTER_PMINTERFACE(TasksPanelCreator,		kTasksPanelCreatorImpl)

REGISTER_PMINTERFACE(ClientOptions,			kClientOptionsImpl)
//REGISTER_PMINTERFACE(ClientOptionsManip,	kClientOptionsManipImpl)

REGISTER_PMINTERFACE(PubBrowseDlgCreator,	kPublicationDialogCreatorImpl)
REGISTER_PMINTERFACE(PubBrowseDlgController,kPublicationDialogControllerImpl)
REGISTER_PMINTERFACE(PubBrowseDlgObserver,	kPublicationDialogObserverImpl)

REGISTER_PMINTERFACE(PathBrowseDlgCreator,	 kPathBrowseDlgCreatorImpl)
REGISTER_PMINTERFACE(PathBrowseDlgController,kPathBrowseDlgControllerImpl)
REGISTER_PMINTERFACE(PathBrowseDlgObserver,	 kPathBrowseDlgObserverImpl)

// End, OptnsDlgFactoryList.h.


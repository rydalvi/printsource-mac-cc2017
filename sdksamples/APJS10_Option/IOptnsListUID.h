#ifndef __IOptnsListUID_h__
#define __IOptnsListUID_h__

#include "IPMUnknown.h"
#include "IDataBase.h"
#include "UIDList.h"
#include "OptnsDlgID.h"

class IPstLstUIDList : public IPMUnknown
{
public:	
	enum	{kDefaultIID = IID_IPSTLSTUIDLIST};

	/**
		The selected UID of the list is set with the parameter being passed in.  
		
		@param uid the UID of the object to be set as the selected one
	*/
	virtual void SetSelectedUID(const UID& uid) = 0;
		
	/**
		This function returns the current selected UID.
		@return UID of the current selected item in the list.
	*/
	virtual const UID& GetSelectedUID() = 0;

	/**
		Make the item index of the list as the current selected item in the list.
		
		@param index the new current selection index.
	*/
	virtual void SetSelectedIndex(const int32& index) = 0;
	
	/**
		This function returns the index of its current selection as represented in the UID list.
		@return index of the current selection.
	*/
	virtual int32 GetSelectedIndex() = 0;
	
	/**
		@return UID list this class maintains.
	*/
	virtual const UIDList* GetUIDList() = 0;
	
	/**
		@return the UIDRef of the current selected object.
	*/
	virtual UIDRef GetSelectedUIDRef() = 0;
	
	/**
		Given an index, this routine will returns its corresponding UIDRef of the object as referenced by the
		index.
		@param index the index of the object in the UID list that the user is interested in.
		@return the UIDRef of the object that the user is inquiring about.
	*/
	virtual UIDRef GetRef(int32 index) = 0;
		
	/** 
		@return the database that stores the UID list 
	*/
	virtual IDataBase* GetDataBase() = 0;
		
	/**
		This function appends an uid of an object onto the UID list that it maintains.
		@param uid the UID of the object to be appened to the UID list.
	*/
	virtual void Append(const UID& uid) = 0;
		
	/**
		This function removes an uid of an object from the UID list that it maintains.
		@param uid the UID of the object to be removed from the UID list.
	*/
	virtual void Remove(const UID& uid) = 0;
};

#endif // __IPstLstUIDList_h__

// End, IPstLstUIDList.h.



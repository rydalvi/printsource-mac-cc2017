

#ifndef __PathBrowseDlgCreator__
#define __PathBrowseDlgCreator__

// Interface includes:
#include "LocaleSetting.h"
#include "IDialogMgr.h"
#include "IApplication.h"
#include "ISession.h"
#include "CAlert.h"

// Implementation includes:
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"

// Project includes:
#include "OptnsDlgID.h"

/** PathBrowseDlgCreator
	provides management and control over the dialog. 
  
	PathBrowseDlgCreator implements IDialogCreator based on
	the partial implementation CDialogCreator. 
	@author Rodney Cook
*/
class PathBrowseDlgCreator : public CDialogCreator
{
	public:
		/**
			Constructor.

			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		PathBrowseDlgCreator(IPMUnknown *boss) : CDialogCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~PathBrowseDlgCreator() {}

		/** 
			Creates a dialog from the resource that is cached by 
			the dialog manager until invoked.
		*/
		virtual IDialog* CreateDialog();	
};


#endif
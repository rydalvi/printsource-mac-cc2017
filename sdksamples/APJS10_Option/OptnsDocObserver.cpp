#include "VCPlugInHeaders.h"
#include "K2Vector.tpp"

// Interface includes
#include "ITextControlData.h"
#include "IDocument.h"
#include "ISubject.h"
#include "ISelection.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "IDropdownListController.h"
#include "IStringListControlData.h"
#include "IHierarchy.h"
#include "ISelectionUtils.h"

// API includes
#include "LayoutUtils.h"
#include "PageItemScrapID.h"
#include "StandoffID.h"
#include "CObserver.h"
#include "CmdUtils.h"

// PstLst includes
#include "OptnsDlgID.h"
#include "CAlert.h"
#include "OptnsDragEventFinder.h"
#include "IControlView.h"
#include "SDKListBoxHelper.h"
#include "listData.h"
#include "CommonFunctions.h"

listData templistData;

#define CA(Z) CAlert::InformationAlert(Z)

class OptnsDocObserver : public CObserver
{
	public:
		OptnsDocObserver(IPMUnknown* boss);
		virtual ~OptnsDocObserver();
		void AutoAttach();
		void AutoDetach();
		virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
	protected:
		void AttachDocument(IDocument*	iDocument);
		void DetachDocument(IDocument*	iDocument);
		void HandlePageItemDeleted(void* changedBy);
};

CREATE_PMINTERFACE(OptnsDocObserver, kOptnsDocObserverImpl)

OptnsDocObserver::OptnsDocObserver(IPMUnknown* boss) :
	CObserver(boss, IID_IPSTLSTOBSERVER)
{
}

OptnsDocObserver::~OptnsDocObserver()
{
}

void OptnsDocObserver::AutoAttach()
{
	CObserver::AutoAttach();

	InterfacePtr<IDocument>	iDocument(this, UseDefaultIID());
	if (iDocument != nil)
		AttachDocument(iDocument);
}


void OptnsDocObserver::AutoDetach()
{
	CObserver::AutoDetach();

	InterfacePtr<IDocument>	iDocument(this, UseDefaultIID());
	if (iDocument != nil)
		DetachDocument(iDocument);
}

void OptnsDocObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{
	do
	{
		if ((protocol == IID_IHIERARCHY_DOCUMENT) || (protocol == IID_IHIERARCHY))
		{
			if ((protocol == IID_IHIERARCHY)
			&&  (theChange == kAddToHierarchyCmdBoss || theChange == kRemoveFromHierarchyCmdBoss))
			{
				//	These commands double broadcast on both IID_IHIERARCHY & IID_IHIERARCHY_DOCUMENT
				//	Only react to the document broadcasts
				return;
			}
			
			if ((theChange == kDeletePageItemCmdBoss)
		/*	||  (theChange == kRemoveFromHierarchyCmdBoss)
			||  (theChange == kDeleteStandOffItemCmdBoss)
			||  (theChange == kDeleteInsetPageItemCmdBoss)*/)
			{
				ICommand* 				iCommand = (ICommand*)changedBy;
				const UIDList 			itemList = iCommand->GetItemListReference();
				ICommand::CommandState	cmdState = iCommand->GetCommandState();

				if (itemList.Length() && (cmdState == ICommand::kNotDone || cmdState == ICommand::kNotRedone))
				{
					ICommand*	cmd = (ICommand*)changedBy;
					const UIDList	*selectUIDList =cmd->GetItemList();
					Dirty();
					refreshLstbox(selectUIDList->GetRef(0).GetUID());

					/*
						listData genPanelListData;
						genPanelListData.setUID
						(OptnsDragEventFinder::listboxType, 
						 OptnsDragEventFinder::selectedRowIndex,
						 selectUIDList->GetRef(0).GetUID());
				/*	}
					else
					{
						if(OptnsDragEventFinder::whichRadBtnSelected==2)
						{
							listData genPanelListData;
							genPanelListData.setUID
							(OptnsDragEventFinder::listboxType, 
							 OptnsDragEventFinder::selectedRowIndex,
							 selectUIDList->GetRef(0).GetUID());
						}
						else
						{
							// Sure delete the box
							HandlePageItemDeleted(changedBy);	
						}
					}*/
					//HandlePageItemDeleted(changedBy);	
				}
			}
		}
	} while (kFalse);
}

void OptnsDocObserver::HandlePageItemDeleted(void* changedBy)
{
	do
	{		
		ICommand*	cmd = (ICommand*)changedBy;
		const UIDList	*selectUIDList =cmd->GetItemList();

		if (selectUIDList->Length() < 1)
		{
			ASSERT_FAIL("invalid page item");
			break;
		}
/* Getting UID of the box deleted currently */
		int32 item = selectUIDList->Length();
		UIDList		myUIDList = *selectUIDList;
		PMString temp("UID of deleted box ");
		temp.AppendNumber(selectUIDList->GetRef(0).GetUID().Get());
		//CA(temp);

		listData genPanelListData;
/* now iterate thru all the entries in selected lstbox and match both UIDs */

		int16 rowIndex = genPanelListData.getRowForIndex
		(OptnsDragEventFinder::listboxType,
		 selectUIDList->GetRef(0).GetUID());

/* Resetting the Draggedbox flag to kFalse */
		genPanelListData.resetDraggedBox
			(OptnsDragEventFinder::listboxType,rowIndex, kFalse);
		// item list and settings information are ok, create the command.*/
	} while (false);
}

/*	OptnsDocObserver::AttachDocument
*/
void OptnsDocObserver::AttachDocument(IDocument*	iDocument)
{
	do
	{
		if (iDocument == nil)
		{
			ASSERT_FAIL("no document to attach to");
			break;
		}
		
		InterfacePtr<ISubject> 		iDocSubject(iDocument, UseDefaultIID());
		if (iDocSubject == nil)
		{
			ASSERT_FAIL("no document subject to attach the observer");
			break;
		}
		
		//	Protocols used for page item model changes
		if (!iDocSubject->IsAttached(this, IID_IHIERARCHY_DOCUMENT, IID_IPSTLSTOBSERVER))
		{
			iDocSubject->AttachObserver(this, IID_IHIERARCHY_DOCUMENT, IID_IPSTLSTOBSERVER);
		}

		if (!iDocSubject->IsAttached(this, IID_IHIERARCHY, IID_IPSTLSTOBSERVER))
		{
			iDocSubject->AttachObserver(this, IID_IHIERARCHY, IID_IPSTLSTOBSERVER);
		}
	} while (kFalse);
}


/*	OptnsDocObserver::DetachDocument
*/
void OptnsDocObserver::DetachDocument(IDocument*	iDocument)
{
	do
	{
		if (iDocument == nil)
		{
			ASSERT_FAIL("no document to attach to");
			break;
		}
		
		InterfacePtr<ISubject> 		iDocSubject(iDocument, UseDefaultIID());
		if (iDocSubject == nil)
		{
			ASSERT_FAIL("no document subject to attach the observer");
			break;
		}

		if (iDocSubject->IsAttached(this, IID_IHIERARCHY_DOCUMENT, IID_IPSTLSTOBSERVER))
		{
			iDocSubject->DetachObserver(this, IID_IHIERARCHY_DOCUMENT, IID_IPSTLSTOBSERVER);
		}

		if (iDocSubject->IsAttached(this, IID_IHIERARCHY, IID_IPSTLSTOBSERVER))
		{
			iDocSubject->DetachObserver(this, IID_IHIERARCHY, IID_IPSTLSTOBSERVER);
		}
	} while (kFalse);
}

// End, OptnsDocObserver.cpp.

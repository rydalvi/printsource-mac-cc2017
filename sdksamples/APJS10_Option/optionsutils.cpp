/*
//	File:	OptnsDlgDialogCreator.cpp
//
//	Date:	07-Jun-2001
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ICoreFilename.h"
#include "ISelectionUtils.h"

#include "HelperInterface.h"
#include "SDKUtilities.h"
#include "CPMUnknown.h"
#include "CAlert.h"
#include "PMString.h"
#include "OptionsStaticData.h"

// Project includes:
#include "OptionsUtils.h"
#include "IAppFramework.h"
//#include "ApplicationFrameHeader.h"
#include "LNGID.h"
#include "ILoginHelper.h"
#include "ISpecialChar.h"
#include "OptionsValue.h"
#include "OptnsDlgID.h"


#include <fstream>
#include <string>

#define CA(x)	CAlert::InformationAlert(x)
#define CA_NUM(i,Message,NUM) PMString K##i(Message);K##i.AppendNumber(NUM);CA(K##i)

extern bool16 doRefreshFlag;

ClientOptionsManip::ClientOptionsManip(){}

ClientOptionsManip::~ClientOptionsManip(){}

bool8 ClientOptionsManip::IsClientOptionsFileExiats(void)
{
	ErrorCode status=kFailure;
	PMString path = GetOptionsFilePath();
#ifdef MACINTOSH
	SDKUtilities::convertToMacPath(path);
#endif
	status = SDKUtilities::FileExistsForRead(path);
	if(status==kFailure)
		return FALSE;		
	return TRUE;
}

bool8 ClientOptionsManip::CreateOptionsFile(PMString& filePath)
{
	FILE *fp=NULL;
#ifdef MACINTOSH
	SDKUtilities::convertToMacPath(filePath);
#endif
	fp=std::fopen(filePath.GetPlatformString().c_str(), "w+");
	if(!fp)
		return FALSE;

	PMString str("deflocale=\ndefcatalog=\nimagespath=\nindesigndocpath=\n");
	const char *data=str.GetPlatformString().c_str(); //Cs4
	std::fprintf(fp, "%s", data);
	std::fclose(fp);
	return TRUE;
}

// Getting application(indesign.exe) path later appending plugin path
PMString ClientOptionsManip::GetOptionsFilePath(void)
{	
	SDKUtilities::GetApplicationFolder(OptionsFilepath);
	
	PMString pluginFolderStr("");
	// appending '\'
#ifdef MACINTOSH
	pluginFolderStr+=":";
#else
	pluginFolderStr+="\\";
#endif
	// appending folder name
	pluginFolderStr+="Plug-ins";

	// appending '\'
#ifdef MACINTOSH
	pluginFolderStr+=":";
#else
	pluginFolderStr+="\\";
#endif

#ifdef MACINTOSH
	pluginFolderStr+=":";
#else
	pluginFolderStr+="\\";
#endif
	// appending folder name
	pluginFolderStr+="PRINTsource7";

	// appending '\'
#ifdef MACINTOSH
	pluginFolderStr+=":";
#else
	pluginFolderStr+="\\";
#endif
	// appending file name
	pluginFolderStr+="ClientOptions.properties";

	OptionsFilepath+=pluginFolderStr;

	return OptionsFilepath;
}

OptionsValue& ClientOptionsManip::ReadClientOptions(void)
{
	std::ifstream inFile;		
	PMString x("");
	OptionsObj.setDefaultLocale(x);
	OptionsObj.setPublicationName(x);
	OptionsObj.setImagePath(x);
	OptionsObj.setIndesignDocPath(x);
//21-feb
	OptionsObj.setAssetStatus(1);
	OptionsObj.setLogLevel(3);
	OptionsObj.setMissingFlag(0);//added by nitin by default is 0
//21-feb

	PMString filePath = GetOptionsFilePath();

	//CA("ClientOptionsManip::ReadClientOptions");
	//----------------------------------------------------------------------------------------------------//
	try
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return OptionsObj;
		}	

		InterfacePtr<ILoginHelper> ptrLogInHelper(static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss,ILoginHelper::kDefaultIID)));
		if(ptrLogInHelper == nil)
		{
			//CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
			return OptionsObj ;
		}
		
		PMString name("");
		LoginInfoValue cserverInfoValue;

		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				
		bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue);
		if(result)
		{
			double clientID = ptrIAppFramework->getClientID();
			PMString ClientNo("");
			ClientNo.AppendNumber(PMReal(clientID));
			ClientInfoValue currentClientInfoValueObj =  cserverInfoValue.getClientInfoByClientNo(ClientNo);
	

			//CA("Inside Result");
            PMString dummyLang = currentClientInfoValueObj.getLanguage();
			OptionsObj.setLocaleID/*.setDefaultLocale*/(dummyLang);

			PMString temp1 = currentClientInfoValueObj.getProject();
			//CA("OptionsObj.setProjectID = " + temp1);
			if(!iConverter)
            {
                PMString dummyPrjId = currentClientInfoValueObj.getProject();
				OptionsObj.setProjectID/*.setPublicationName*/(dummyPrjId);
            }
			if(iConverter)
            {
                PMString dummyPrjId = iConverter->translateString(currentClientInfoValueObj.getProject());
				OptionsObj.setProjectID/*.setPublicationName*/(dummyPrjId);
            }

            PMString dummyImagePath = currentClientInfoValueObj.getAssetserverpath();
			OptionsObj.setImagePath(dummyImagePath);
            PMString dummyIndDocPath = currentClientInfoValueObj.getDocumentPath();
			OptionsObj.setIndesignDocPath(dummyIndDocPath);
//21-feb
			OptionsObj.setAssetStatus(currentClientInfoValueObj.getAssetStatus());
			OptionsObj.setLogLevel(currentClientInfoValueObj.getLogLevel());
			OptionsObj.setMissingFlag(currentClientInfoValueObj.getMissingFlag());//nitin
//21-feb
            PMString dummySectId = currentClientInfoValueObj.getSectionID();
			OptionsObj.setSectionID(dummySectId);
            PMString dummyOS = currentClientInfoValueObj.getIsONESource();
			OptionsObj.setIsOneSource(dummyOS);

			OptionsObj.setDisplayPartnerImgs(currentClientInfoValueObj.getDisplayPartnerImages());
			OptionsObj.setDisplayPickListImgs(currentClientInfoValueObj.getDisplayPickListImages());
			OptionsObj.setShowObjCountFlag(currentClientInfoValueObj.getShowObjectCountFlag());

			OptionsObj.setHorizontalSpacing(currentClientInfoValueObj.getHorizontalSpacing());
			OptionsObj.setVerticalSpacing(currentClientInfoValueObj.getVerticalSpacing());
			OptionsObj.setByPassForSingleSelectionSprayFlag(currentClientInfoValueObj.getByPassForSingleSelectionSprayFlag());

			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil){
				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
				return OptionsObj;
			}
			OptionsObj.setclientID(ptrIAppFramework->getClientID());
			//CA("OptionsObj.getLocaleID() : " + OptionsObj.getLocaleID());
			if((OptionsObj.getLocaleID()).NumUTF16TextChars() != 0)
			{
				VectorLanguageModelPtr VectorLocValPtr = ptrIAppFramework->StructureCache_getAllLanguages();
				if(VectorLocValPtr == NULL)
				{
					//CAlert::InformationAlert("VectorLocValPtr == NULL");
					return OptionsObj;
				}
						
				/*PMString str("VectorLocValPtr-> size  = ");
				str.AppendNumber(static_cast<int32>(VectorLocValPtr->size()));
				CA(str);*/

				VectorLanguageModel::iterator it;				
				for(it = VectorLocValPtr->begin(); it != VectorLocValPtr->end(); it++)
				{		
					//CA(it->getLangugeName()); 
					double LocID = it->getLanguageID();
					double CurrentLocID = (OptionsObj.getLocaleID()).GetAsDouble();
					if(LocID == CurrentLocID )
					{
						PMString localename(it->getLangugeName());
						OptionsObj.setDefaultLocale(localename);
						//CA("setDefaultLocale as : " + localename);
						break;
					}
				}
//12-april
				//if(VectorLocValPtr)
					//delete VectorLocValPtr;
//12-april
			}

			/*CA("OptionsObj.getProjectID() =:  " + OptionsObj.getProjectID());
			CA("OptionsObj.getPublicationName()   : "+OptionsObj.getPublicationName());*/
			if((OptionsObj.getProjectID()).NumUTF16TextChars() != 0)
			{
				//CA("OptionsObj.getProjectID()).NumUTF16TextChars() != 0");	
//added by Tushar on 20/09 START////////////////////////////////////////////////	
				int flag=0;

//				VectorClassInfoPtr vectClsValuePtr = ptrIAppFramework->ClassificationTree_getRoot((OptionsObj.getLocaleID()).GetAsNumber()); // 1 default for English
//				if(vectClsValuePtr==nil)
//				{		
//					//CA("vectClsValuePtr==nil");
//					//return OptionsObj;
//				}
//				else
//				{
//					//----------
//					VectorClassInfoValue::iterator itr;
//					itr = vectClsValuePtr->begin();
//
//					VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getAllChildren(itr->getClass_id(),(OptionsObj.getLocaleID()).GetAsNumber());
//					if(vectClassInfoValuePtr == nil)
//					{
//						//CA("vectClassInfoValuePtr == nil ");
//					}
//					else
//					{//CA("check 0112");
//						VectorClassInfoValue::iterator it1;
//						
//						for(it1=vectClassInfoValuePtr->begin(); it1!=vectClassInfoValuePtr->end(); it1++)
//						{
//							//CA("inside class for");
//							InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//							int32 ProjID = it1->getClass_id();
//							
//							//PMString temp = OptionsObj.getProjectID();
//							//CA("temp = " + temp);
//							PMString projectIdString("");
//							projectIdString = OptionsObj.getProjectID();
//							int32 CurrProjID = projectIdString.GetAsNumber();
//							if(ProjID == CurrProjID )
//							{
//								PMString CatalogName("");
//								PMString TempBuffer = it1->getClassification_name();
//
//								if(iConverter){ 
//									CatalogName=iConverter->translateString(TempBuffer);
//								}
//								else				
//									CatalogName = TempBuffer;
//							//	CA("CatalogName : " + CatalogName);
//								OptionsObj.setPublicationName(CatalogName);
//								OptionsObj.setProjectID(projectIdString);//added by mane
//								flag = 1;
//								ptrIAppFramework->set_isONEsourceMode(kTrue);
//							}
//						}
//
//					}
//					if(vectClassInfoValuePtr)
//						delete vectClassInfoValuePtr;
//
//				}
////12-april
//				if(vectClsValuePtr)
//					delete vectClsValuePtr;
				
//12-april
//added by Tushar on 20/09 END/////////////////////////////////////////////////

				PMString projectIdString("");
				projectIdString = OptionsObj.getProjectID();
				double CurrProjID = projectIdString.GetAsDouble();
				double lang_ID = (OptionsObj.getLocaleID()).GetAsDouble();

				//if(OptionsObj.getIsOneSource() == "true")
				//{
				//	//CA("true");
				//	ptrIAppFramework->set_isONEsourceMode(kTrue);
				//	CClassificationValue classValueObj = ptrIAppFramework->ClassificationTree_getCClassificationValueByClassId(CurrProjID , lang_ID );
				//	int32 class_id = classValueObj.getClass_id();
			
				//	if(class_id == CurrProjID)
				//	{
				//		//CA("class_id == defPubId");
				//		PMString CatalogName = "";
				//		flag = 1;

				//		PMString TempBuffer = classValueObj.getClassification_name();
				//		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
				//		if(iConverter)
				//		{
				//			CatalogName=iConverter->translateString(TempBuffer);
				//		}
				//		else
				//			CatalogName = TempBuffer;
				//		/*CA("CatalogName   :  "+CatalogName);
				//		CA("projectIdString   :  "+projectIdString);*/
				//		OptionsObj.setPublicationName(CatalogName);
				//		OptionsObj.setProjectID(projectIdString);
				//		

				//	}
				//}
				//else
				{
					//CA("false");
					ptrIAppFramework->set_isONEsourceMode(kFalse);

					//VectorPubModelPtr VectorPublInfoValuePtr = ptrIAppFramework->PUBMgr_getAllProjects(lang_ID);
					//if(VectorPublInfoValuePtr == nil){
					//	//CAlert::InformationAlert("There is no Publications...333");
					//	return OptionsObj;
					//}
					
					//--------------

					CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(CurrProjID,lang_ID);
					double pubParentID = pubModelRootValue.getParentId();
					double rootPubID = pubModelRootValue.getRootID();

					VectorPubModelPtr VectorPublInfoValuePtr = NULL;
					if(CurrProjID == rootPubID)
					{
						CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(CurrProjID,lang_ID);
						PMString pubStrID("");
						double pubID = pubModelValue.getEventId();
						pubStrID.AppendNumber(PMReal(pubID));

						PMString PubName = "";

						PMString TempBuff = pubModelValue.getName();
						if(iConverter){ 
							PubName=iConverter->translateString(TempBuff);
						}
						else				
							PubName = TempBuff;

						OptionsObj.setPublicationName(PubName);
						OptionsObj.setProjectID(pubStrID);//added by mane
						
						return OptionsObj;
						
					}
					else
					{
						//CA("not equal");
						VectorPublInfoValuePtr = ptrIAppFramework->ProjectCache_getAllChildren(pubParentID,lang_ID);
						if(VectorPublInfoValuePtr == nil){
							//CAlert::InformationAlert("There is no Publications...333");
							return OptionsObj;
						}

					}
					//-----------------

					VectorPubModel::iterator it;
					for(it = VectorPublInfoValuePtr->begin(); it != VectorPublInfoValuePtr->end(); it++)
					{
						//CA("Options Utils inside publication for");
						InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
						/*PMString temp;*/
						
						
						double ProjID = it->getEventId();

						//CA("it->getName()  : "+it->getName());
						if(ProjID == CurrProjID )
						{
							//CA("ProjID == CurrProjID");
							PMString CatalogName("");
							
							PMString TempBuffer = it->getName();
							if(iConverter){ 
								CatalogName=iConverter->translateString(TempBuffer);
							}
							else				
								CatalogName = TempBuffer;
							//CA("CatalogName : " + CatalogName);
							OptionsObj.setPublicationName(CatalogName);
							OptionsObj.setProjectID(projectIdString);//added by mane
							
							break;
						}
					}
					
//12-april			
					if(VectorPublInfoValuePtr)
						delete VectorPublInfoValuePtr;
	//12-april
				}
			}
			else
			{
				PMString pubStr_ID("");
				pubStr_ID.Append("-1");
				OptionsObj.setProjectID(pubStr_ID);
			}
		}
		else {
			//CA("--getCurrentServerInfo failed--");
		}
	}
	catch(...)
	{
		//CA("--UNKNOWN EXCEPTION -- ");
	}
	//----------------------------------------------------------------------------------------------------//
//-----COMMENTED ON 15 FEB 2006-----------------//
//#ifdef MACINTOSH
//	SDKUtilities::getMacPath(filePath);
//#endif
//	char* path=filePath.GrabCString();
//
//	FILE* fp=NULL;
//	fp=std::fopen(path, "r");
//	if(!fp)
//		return OptionsObj;
//
//	char str[512];
//	PMString defLocale("deflocale");
//	PMString defPub("defcatalog");
//	PMString imgPath("imagespath");
//	PMString docPath("indesigndocpath");
//
//	//----------------------------------------------------------------------------------------------------//
//	while(std::fgets(str, 512, fp))
//	{
//		do
//		{
//			if(str[std::strlen(str)-1]=='\r' || str[std::strlen(str)-1]=='\n')
//				str[std::strlen(str)-1]='\0';
//			PMString tempString(str);
//
//			if(std::strstr(str, "deflocale"))
//			{
//				PMString lName;
//				lName=getStringByKey(tempString, defLocale);
//				OptionsObj.setDefaultLocale(lName);
//				break;
//			}
//			if(std::strstr(str, "defcatalog"))
//			{
//				PMString pName;
//				pName=getStringByKey(tempString, defPub);
//				OptionsObj.setPublicationName(pName);
//				break;
//			}
//			if(std::strstr(str, "imagespath"))
//			{
//				PMString pName;
//				pName=getStringByKey(tempString, imgPath);
//				OptionsObj.setImagePath(pName);
//				break;
//			}
//			if(std::strstr(str, "indesigndocpath"))
//			{
//				PMString pName;
//				pName=getStringByKey(tempString, docPath);
//				OptionsObj.setIndesignDocPath(pName);
//				break;
//			}
//		}while(0);
//		//--------------------------------------------------------------------------------------//
//	}
//	std::fclose(fp);
	return OptionsObj;
}

//std::ifstream* fileptr,
// return 1 for publicationname, 2 for Imagepath, 3 document path
PMString ClientOptionsManip::getStringByKey(PMString& line,PMString& searchString)
{
	//char* str = line.GrabCString();
	//int stringLength = strlen(str);
	int stringLength = line.NumUTF16TextChars();
	int searchStringLength = searchString.NumUTF16TextChars();

	if((searchStringLength+1)==stringLength)
	{
		///CAlert::InformationAlert("there is no def values for "+searchString);
		return PMString("");
	}
	CharCounter pos = line.IndexOfCharacter('=');
	if(pos == 0 || pos == -1){
		//CAlert::InformationAlert("getStringByKey position 0 or -1");
		return PMString("");
	}	

	if(pos == searchStringLength && pos > stringLength)
		return PMString("");


	PMString *tmp = line.Substring(pos+1);
	PMString x(*tmp);
	if(tmp)
		delete tmp;
	return x;
	
}

void ClientOptionsManip::WriteClientOptions(OptionsValue& optnsObj)
{
	//CA("ClientOptionsManip::WriteClientOptions");
	OptionsObj = optnsObj;
//22-feb
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return;
	}

	ptrIAppFramework->setAssetServerOption (optnsObj.getAssetStatus());
	ptrIAppFramework->setLoggerFileFlag (optnsObj.getLogLevel());
	ptrIAppFramework->setMissingFlag(optnsObj.getMissingFlag());
//22-feb
	//----------------------------------------------------------------------------------------------//
	InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
	if(ptrLogInHelper == nil)
	{
		//CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
		return ;
	}
	//CA("OptionsObj.getPublicationName()  :  "+OptionsObj.getPublicationName());
	//PMString name("") ;
	LoginInfoValue cserverInfoValue ;

	bool16 result = ptrLogInHelper->getCurrentServerInfo(/*name ,*/ cserverInfoValue) ;
	if(result)
	{
		//CA(" UPDATING SERVER INFO -- WRITING TO SERVER ");
		double clientID = ptrIAppFramework->getClientID();
		PMString ClientNo("");
		ClientNo.AppendNumber(PMReal(clientID));
		ClientInfoValue currentClientInfoValueObj =  cserverInfoValue.getClientInfoByClientNo(ClientNo);


		currentClientInfoValueObj.setDocumentPath(OptionsObj.getIndesignDocPath()) ;
		currentClientInfoValueObj.setAssetserverpath(OptionsObj.getImagePath()) ;
		currentClientInfoValueObj.setLanguage(OptionsObj.getLocaleID()/*getDefaultLocale()*/) ;
//21-feb
		currentClientInfoValueObj.setAssetStatus(OptionsObj.getAssetStatus());
		currentClientInfoValueObj.setLogLevel(OptionsObj.getLogLevel());
		currentClientInfoValueObj.setMissingFlag(OptionsObj.getMissingFlag());//nitin
//21-feb
		//InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		//if(!iConverter)
		//{
		//	CA("!iConverter");
		//	//CA("set project1  :  "+OptionsObj.getPublicationName());
		//	cserverInfoValue.setProject(OptionsObj.getProjectID()) ;
		//
		//}
		//else
		//{	
			//CA("set project2  :   "+OptionsObj.getProjectID());
			currentClientInfoValueObj.setProject(OptionsObj.getProjectID()) ;
		//}

		currentClientInfoValueObj.setSectionID(OptionsObj.getSectionID());
		currentClientInfoValueObj.setIsONESource(OptionsObj.getIsOneSource());

		currentClientInfoValueObj.setDisplayPartnerImages(OptionsObj.getDisplayPartnerImgs());
		currentClientInfoValueObj.setDisplayPickListImages(OptionsObj.getDisplayPickListImgs());

		currentClientInfoValueObj.setShowObjectCountFlag(OptionsObj.getShowObjCountFlag());

		currentClientInfoValueObj.setByPassForSingleSelectionSprayFlag(OptionsObj.getByPassForSingleSelectionSprayFlag());
        
		currentClientInfoValueObj.setHorizontalSpacing(OptionsObj.getHorizontalSpacing());
		currentClientInfoValueObj.setVerticalSpacing(OptionsObj.getVerticalSpacing());

		cserverInfoValue.setClientInfoValue(currentClientInfoValueObj);

		ptrLogInHelper->setEditedServerInfo(cserverInfoValue);
		//CA("--image path--"+ OptionsObj.getImagePath());
		//CA("--doc path -- "+ OptionsObj.getIndesignDocPath());
		//CA_NUM(111 , " CONNECTION MOD  " , cserverInfoValue.getdbConnectionMode());
		//CUserInfoValue usrProfile = ptrIAppFramework->LOGINMngr_getUserProfile();
		//int32 clientID = OptionsObj.getclientID();
		//int32 clientID = ptrIAppFramework->getClientID();
		result = ptrLogInHelper->editServerInfo(cserverInfoValue,clientID);	
	}

	//----------------------------------------------------------------------------------------------//
	/*PMString filePath = GetOptionsFilePath(); //--COMMENTED ON 15 FEB 2006 

#ifdef MACINTOSH
	SDKUtilities::getMacPath(filePath);
#endif

	FILE *fp=NULL;
	fp=std::fopen(filePath.GrabCString(), "w+");
	if(!fp)
		return;*/

	//PMString str("deflocale");
	//str += "=";
	//str += OptionsObj.getDefaultLocale();
	//str += "\n";
	//str += "defcatalog";
	//str += "=";
	//str += OptionsObj.getPublicationName();
	//str += "\n";
	//str += "imagespath";
	//str += "=";
	//str += OptionsObj.getImagePath();
	//str += "\n";	
	//str += "indesigndocpath";
	//str += "=";
	//str += OptionsObj.getIndesignDocPath();
	//str += "\n";

	//std::fprintf(fp, "%s", str.GrabCString());
	//std::fclose(fp);
	return;
}

double ClientOptionsManip::checkforPublicationExistance(PMString& pName, double& pID)
{	
	//CA("ClientOptionsManip::checkforPublicationExistance");
	if(pName == "" || pID == -1)
	{
		//CA("pName ==  || pID == -1");
		return -1;
	}

	static VectorClassInfoPtr vectClassInfoValuePtr = NULL;  //// static pointer is used ti cache the ClassVector & ProjectVector . to avoid round trips to server
	static VectorPubModelPtr VectorPublInfoValuePtr = NULL;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));

		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return -1;
		}
		//VectorCatalogValuePtr VectorPublInfoValuePtr = ptrIAppFramework->getAllCatalogsByLocaleID(PersistData::getLocaleID()); // changes @vaibhav
		
		//VectorPublicationPtr AppFramework::PUBMgr_getAllProjects()
		double localeID = PersistData::getLocaleID();
		
		if(doRefreshFlag == kTrue)
		{
			if(vectClassInfoValuePtr != NULL)
			{
				delete vectClassInfoValuePtr;
				vectClassInfoValuePtr = NULL;
			}
			if(VectorPublInfoValuePtr != NULL)
			{
				delete VectorPublInfoValuePtr;
				VectorPublInfoValuePtr = NULL;
			}
		}

		if(doRefreshFlag == kTrue || vectClassInfoValuePtr == NULL )
		{
			//CA("doRefreshFlag == kTrue in ONEsource");
			vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(1); // 1 default for English	
			doRefreshFlag = kFalse;
		}
		if(vectClassInfoValuePtr==nil)
		{		
			//CA("vectClassInfoValuePtr==nil");
			return -1;
		}
		
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		

	do{	
		//added by Tushar on 20/09 START////////////////////////////
		VectorClassInfoValue::iterator it1;
		int flag = 0;
		for(it1=vectClassInfoValuePtr->begin(); it1!=vectClassInfoValuePtr->end(); it1++)
		{
			double tempclassificationId;
			if(!iConverter)
				tempclassificationId = it1->getClass_id();
			else
			{
				tempclassificationId = it1->getClass_id();
			}
			
				if(pID == tempclassificationId) 
				{
					//CA("pID == tempclassificationId");
				    pID =it1->getClass_id();	
					flag = 1;
					ptrIAppFramework->set_isONEsourceMode(kTrue);
					break;
				}
				
		}
		if(flag == 1) 
		{
			break;
		}
		//added by Tushar on 20/09 END///////////////////////////////////	
		
		if(doRefreshFlag == kTrue || VectorPublInfoValuePtr == NULL)
		{
			//CA("doRefreshFlag == kTrue in Project");
			VectorPublInfoValuePtr = ptrIAppFramework->ProjectCache_getAllProjects(localeID);
			doRefreshFlag = kFalse;
		}
		if(VectorPublInfoValuePtr == nil){
			//CAlert::InformationAlert("There is no Publications 123...");
			return -1;
		}

		//VectorCatalogValue::iterator it;   // changes @ vaibhav
		VectorPubModel::iterator it;
  		for(it = VectorPublInfoValuePtr->begin(); it != VectorPublInfoValuePtr->end(); it++)
		{//CA("23");		
			double nameid;
			nameid = it->getEventId();
			if(pID == nameid)
			{
				//CA("pID == nameid");
				pID =it->getEventId();
 				ptrIAppFramework->set_isONEsourceMode(kFalse);
				break;
			}
		}
		//CA("inside while");
	}while(0);
	
	//if(ptrIAppFramework !=nil)  commented by vaibhav
	//		delete ptrIAppFramework;
//CA("27");	
//	return pubID; //commented by mane
	return pID;
}
////================UP TO HERE=========================

double ClientOptionsManip::checkforLocaleExistance(PMString& pName,double& languageID)
{
	//CA("ClientOptionsManip::checkforLocaleExistance");
	VectorLanguageModelPtr VectorLocValPtr = NULL;
	
	double Localeid = -1;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return Localeid;
	}
//CA("2");
	//VectorLocaleValuePtr VectorLocValPtr = ptrIAppFramework->getAllLocales(); changes here done by vaibhav
	if(VectorLocValPtr == NULL)
	{
		//CA("VectorLocValPtr == NULL ... doRefresh is true" );
		VectorLocValPtr = ptrIAppFramework->StructureCache_getAllLanguages();
	}
	if(VectorLocValPtr == NULL)
	{
		//CAlert::InformationAlert("VectorLocValPtr == NULL");
		return -1;
	}
 //CA("3");
	do
	{
		VectorLanguageModel::iterator it;
		for(it = VectorLocValPtr->begin(); it != VectorLocValPtr->end(); it++)
		{  			
			double lanID(it->getLanguageID());
			if(lanID == languageID){
				//CA("pName == name : " + it->getLangugeName());
				Localeid = it->getLanguageID();	
				PersistData::setLocaleID(Localeid);
				break;
			}
		}

	}while(0);

	//if(ptrIAppFramework !=nil) commented by vaibhav
	//		delete ptrIAppFramework;
	
	return Localeid;
}


bool8 ClientOptionsManip::checkforFolderExistance(PMString& path)
{
	//bool8 flag= kFalse;	
	//InterfacePtr<IFileUtility> tableSuite(static_cast<IFileUtility*>(Utils<ISelectionUtils>()->QuerySuite(IFileUtility::kDefaultIID)));

	//AppFramework* ptrIAppFramework = new AppFramework(); commented by vaibhav

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));

	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return FALSE;
	}

#ifdef MACINTOSH
	SDKUtilities::convertToMacPath(path);		
#endif
	string tempString(path.GetPlatformString());
	
	const char* cPath = const_cast<char *> (tempString.c_str());
	//flag= false;//ptrIAppFramework->isDirectory(cPath);	 
	/*if(!flag){
		PMString a("isDirectory returns false");
		a.Append("\npath =");
		a.Append(path);
		CA(a);
		a.Clear();
		a.Append("*cPath =");
		a.Append(*cPath);
		CA(a);
		a.Clear();
		a.Append("*cPath =");
		a.Append(cPath);
		CA(a);
	}*/
	//return flag;
	
	bool8 isFolderExists = TRUE;
	//CAlert::InformationAlert(path);
	InterfacePtr<ICoreFilename> cfn((ICoreFilename *)::CreateObject(kCoreFilenameBoss, IID_ICOREFILENAME));

	cfn->Initialize(&path);

	bool16 flag = cfn->DirectoryExists();
	//PMString x("flag:");
	//x.AppendNumber(flag);
	///CAlert::InformationAlert(x);

	if(flag == 0){		
		isFolderExists =FALSE;
		if(path.NumUTF16TextChars() > 0)
			isFolderExists = TRUE;
	}
	else{		
		isFolderExists = TRUE;		
	}

	return isFolderExists;
	
}

void ClientOptionsManip::SetCatalogIDInAppFramework(double catalogID)
{
	//AppFramework* ptrIAppFramework = new AppFramework();  commented by vaibhav

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));

	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return ;
	}
	ptrIAppFramework->setCatalogId(catalogID);

	//if(ptrIAppFramework !=nil)
	//		delete ptrIAppFramework;
}

void ClientOptionsManip::SetLocaleIDInAppFramework(double localeID)
{
// 	AppFramework* ptrIAppFramework = new AppFramework(); commented by vaibhav

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));

	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return ;
	}
	ptrIAppFramework->setLocaleId(localeID);

	//if(ptrIAppFramework !=nil)  commented by vaibhav
	//		delete ptrIAppFramework;
}


#include "VCPluginHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IWorkspace.h"
#include "IDatabase.h"
#include "IPMStream.h"
#include "CPMUnknown.h"

// General includes:
#include "HelperInterface.h"
#include "Trace.h"

// PstLst includes:
#include "IOptnsListUID.h"
#include "OptnsDlgID.h"

/**
	PstLstUIDList is a persistent \Ref{IPstLstUIDList} implementation, it is added to kDrawablePageItemBoss.
	When this plug-in is loaded, a page item will carry this UID list that contains the UIDs of 
	all the data object being added to itself through PstLstNewDataCmd.  PstLstUIDList allows 
	adding/removing of a UID to/from the list.  User can see this list thru a drop down list in
	the plug-in panel, and the current selected list item is tracked thru the data member fSelectedUID.
*/
class PstLstUIDList : public CPMUnknown<IPstLstUIDList>
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		PstLstUIDList(IPMUnknown* boss);
		
		/**
			Destructor, the UIDList is deleted.
		*/
		virtual ~PstLstUIDList();
		
		/**
			The selected UID of the list is set with the parameter being passed in.  It is called
			mainly by a undo operation of a command, for example, a "New" command will add a new
			UID to the list and the list will update its selected UID with the new one, the command
			then should remember the old selected UID for undo operation, so in an undo of a command
			it can call this method to reset to the old state.
			
			@param uid the UID of the object to be set as the selected one
		*/
		virtual void SetSelectedUID(const UID& uid);
		
		/**
			This function returns the UID of its current selection.
			@return UID the UID of the current selection (fSelectedUID).
		*/
		virtual const UID& GetSelectedUID();
		
		/**
			When a user selects a new item from the drop down list, its corresponding UID list needs
			to be updated with the current selection change, fSelectedUID will be updated with the 
			selected object's UID in this routine.
			
			@param index the new current selection index.
		*/
		virtual void SetSelectedIndex(const int32& index);
	
		/**
			This function returns the index of its current selection as represented in the UID list.
			@return index of the current selection.
		*/
		virtual int32 GetSelectedIndex();
		
		/**
			This function appends an uid of an object onto the UID list that it maintains.
			@param uid the UID of the object to be appened to the UID list.
		*/
		virtual void Append(const UID& uid);
		
		/**
			This function removes an uid of an object from the UID list that it maintains.
			@param uid the UID of the object to be removed from the UID list.
		*/
		virtual void Remove(const UID& uid);
	
		/**
			@return UID list this class maintains.
		*/
		virtual const UIDList* GetUIDList();
	
		/**
			@return the UIDRef of the current selected object.
		*/
		virtual UIDRef GetSelectedUIDRef();
	
		/**
			Given an index, this routine will returns its corresponding UIDRef of the object as referenced by the
			index.
			@param index the index of the object in the UID list that the user is interested in.
			@return the UIDRef of the object that the user is inquiring about.
		*/
		virtual UIDRef GetRef(int32 index);
		
		/** 
			@return the database that stores the UID list 
		*/
		virtual IDataBase* GetDataBase();
		
		/**
			Because this is a persistent interface, it must support the ReadWrite method. This method is used for 
			both writing information out to the database and reading information in from the database.
			
			The infomation it reads/writes include the current selected UID and the whole UID list.

			@param stream contains the stream to be written or read.
			@param implementation the implementation ID.
		*/
		virtual void ReadWrite(IPMStream* s, ImplementationID prop);
	
	private:
		UID fSelectedUID;
		UIDList* fUIDList;
		void ReadWriteUIDList(IPMStream* s, ImplementationID prop);
};

CREATE_PERSIST_PMINTERFACE(PstLstUIDList, kPstLstUIDListImpl)

/*	PstLstUIDList::PstLstUIDList
*/
PstLstUIDList::PstLstUIDList(IPMUnknown* boss) : 	
		CPMUnknown<IPstLstUIDList>(boss),
		fSelectedUID(kInvalidUID),
		fUIDList(nil)
{
	Trace("PstLstUIDList::Constructor(), iDataBase = %x\n", ::GetDataBase(this));
	fUIDList = new UIDList(::GetDataBase(this));
}

/*	PstLstUIDList::~PstLstUIDList
*/
PstLstUIDList::~PstLstUIDList()
{
	Trace("PstLstUIDList::Destructor()\n");
	if (fUIDList != nil)
		delete fUIDList;
}

/*	PstLstUIDList::SetSelectedUID
*/
void PstLstUIDList::SetSelectedUID(const UID& uid) 
{
	if (uid != fSelectedUID && fUIDList->Location(uid) >= 0)
	{
		fSelectedUID = uid;
		Dirty(kPstLstUIDListImpl);
	}
}

/*	PstLstUIDList::GetSelectedUID
*/
const UID& PstLstUIDList::GetSelectedUID()
{
	if (GetSelectedIndex() < 0)
		SetSelectedUID(kInvalidUID);
	return fSelectedUID;
}

/*	PstLstUIDList::SetSelectedIndex
*/
void PstLstUIDList::SetSelectedIndex(const int32& index)  
{
	if (!fUIDList->IsEmpty() && index >= 0 && index <= fUIDList->Length()-1)
		SetSelectedUID((*fUIDList) [index]);
}

/*	PstLstUIDList::GetSelectedIndex
*/
int32 PstLstUIDList::GetSelectedIndex()
{
	return fUIDList->Location(fSelectedUID);
}

/*	PstLstUIDList::GetDataBase
*/
IDataBase* PstLstUIDList::GetDataBase()
{
	return fUIDList->GetDataBase();
}

/*	PstLstUIDList::GetSelectedUIDRef
*/
UIDRef PstLstUIDList::GetSelectedUIDRef()
{
	UIDRef uidRef;
	int32 index = GetSelectedIndex();
	if (index >= 0)
		uidRef = fUIDList->GetRef(index);
	return uidRef;
}

/*	PstLstUIDList::GetRef
*/
UIDRef PstLstUIDList::GetRef(int32 index)
{
	UIDRef uidRef;
	if (index >= 0 && index < fUIDList->Length())
		uidRef = fUIDList->GetRef(index);
	return uidRef;
}

/*	PstLstUIDList::GetUIDList
*/
const  UIDList* PstLstUIDList::GetUIDList()
{
	return fUIDList;
}

/*	PstLstUIDList::Append
*/
void PstLstUIDList::Append(const UID& uid)  
{
	fUIDList->Append(uid);
	fSelectedUID = uid;
	Dirty(kPstLstUIDListImpl);
}

/*	PstLstUIDList::Remove
*/
void PstLstUIDList::Remove(const UID& uid)  
{
	int32 index = fUIDList->Location(uid);
	if (index >= 0)
	{
		fUIDList->Remove(index);
		if (uid == fSelectedUID)
		{
			//Change the selection since we're going to delete it
			if (!fUIDList->IsEmpty())
			{
				if (index > 0)
				{
					index--;
					fSelectedUID = (*fUIDList)[index];
				}
				else
				{
					fSelectedUID = (*fUIDList)[0];
				}
			}
			else
			{
				fSelectedUID = kInvalidUID;
			}
		}
		Dirty(kPstLstUIDListImpl);
	}
}

/*	PstLstUIDList::ReadWrite
*/
void PstLstUIDList::ReadWrite(IPMStream* s, ImplementationID prop)
{
	Trace("PstLstUIDList::ReadWrite()\n");
	s->XferReference(fSelectedUID);
	ReadWriteUIDList(s,prop);
}

/*	PstLstUIDList::ReadWriteUIDList
*/
void PstLstUIDList::ReadWriteUIDList(IPMStream* s, ImplementationID prop)
{
	int32 length = fUIDList->Length();
	s->XferInt32(length);

	if (s->IsReading())
	{
		fUIDList->Clear();
		fUIDList->Preallocate(length);

		for (int32 i = 0; i < length; i++)	
		{
			UID		u;
			fUIDList->Append(s->XferObject(u));
		}
		if (length > 0 && fSelectedUID == kInvalidUID)
		{
			// in copy/cut/paste, a new UID is created and data copied, thus the old fSelectedUID is invalid.
			fSelectedUID = (*fUIDList)[0];
			Dirty(kPstLstUIDListImpl);
		}
	}
	else
	{
		for (int32 i = 0; i < length; i++)
			s->XferObject(fUIDList->At(i));
	}	
}

// End, PstLstUIDList.cpp.

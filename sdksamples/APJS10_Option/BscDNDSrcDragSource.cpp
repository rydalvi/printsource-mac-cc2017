#include "VCPlugInHeaders.h"
#include "IPageItemScrapData.h"
#include "IPageItemLayerData.h"
#include "IPathUtils.h"
#include "IPMDataObject.h"
#include "IDragDropController.h"
#include "IGraphicAttributeUtils.h"
#include "IGraphicAttributeSuite.h"
#include "ISwatchUtils.h"
#include "ISwatchList.h"
#include "ILayoutUtils.h"
#include "SDKUtilities.h"
#include "CAlert.h"
#include "CDragDropSource.h"
#include "UIDList.h"
#include "CmdUtils.h"
#include "UIDRef.h"
#include "CmdUtils.h"
#include "SplineID.h"
#include "PMFlavorTypes.h"
#include "OptnsDlgId.h"
#include "ITextModel.h"
#include "ISelection.h"
#include "ErrorUtils.h"
#include "ITextModel.h"
#include "ITextEditSuite.h"
#include "TransformUtils.h"
#include "ICreateFrameData.h"
#include "INewPageItemCmdData.h"
#include "IHierarchy.h"
#include "IMargins.h"
#include "SystemUtils.h"
#include "ITextFocus.h" 
#include "SelectUtils.h"
#include "ICommand.h" 
#include "ISelectionUtils.h"
#include "ShuksanID.h"
#include "FrameUtils.h"
#include "LayoutUtils.h"
#include "InterfacePtr.h"
#include "UIDList.h"
#include "PMTextUtils.h"
#include "IFrameList.h"
#include "RangeData.h"
#include "SplineID.h"
#include "SpreadID.h"
#include "ITextFrame.h"
#include "theListData.h"

#define CA(x)	CAlert::InformationAlert(x)
#define ourFlavor PMFlavor(kPageItemFlavor)

class BscDNDSrcDragSource : public CDragDropSource
{
	public:
		BscDNDSrcDragSource(IPMUnknown* boss); 
		bool16 WillDrag(IEvent* e) const;
		bool16 DoAddDragContent(IDragDropController* DNDController);
		SysRgn DoMakeDragOutlineRegion() const;
	private:
		UIDRef fPageItem;

		ErrorCode CreatePageItem(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);
		ErrorCode CreatePicturebox(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);
		ErrorCode CreateTextbox(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);
};

CREATE_PMINTERFACE(BscDNDSrcDragSource, kBscDNDSrcDragSourceImpl)

BscDNDSrcDragSource::BscDNDSrcDragSource(IPMUnknown* boss)
: CDragDropSource(boss)
{
}

bool16 BscDNDSrcDragSource::WillDrag(IEvent* e) const
{
	IDocument * theFrontDoc = Utils<ILayoutUtils>()->GetFrontDocument();
	if (theFrontDoc == nil)
		return kFalse;
	return kTrue;
}

ErrorCode 
BscDNDSrcDragSource::CreatePicturebox(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{
	ErrorCode returnValue = kFailure;
	do 
	{
		ICommandSequence *sequence = CmdUtils::BeginCommandSequence();
		if (sequence == nil)
		{
			ASSERT_FAIL("Cannot create command sequence?");
			CAlert::InformationAlert("ICommandSequence nil");
			break;
		}

		PMPoint topLeft(5,5);
		PMPoint bottomRight(100,100);
		PMRect rect(topLeft, bottomRight);
		UIDRef newSplineItem = Utils<IPathUtils>()->CreateRectangleSpline
		(parent, rect, INewPageItemCmdData::kGraphicFrameAttributes);

		const UIDList splineItemList(newSplineItem);

		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==nil)
		{
			ASSERT_FAIL("Cannot create the command to stroke the spline?");
			CAlert::InformationAlert("ICommand nil");
			break;
		}
				
		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
		{
			ASSERT_FAIL("Failed to stroke the spline?");
			CAlert::InformationAlert("ProcessCommand failure");
			break;
		}

		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		if(iSwatchList==nil)
		{
			ASSERT_FAIL("Cannot get swatch list?");
			CAlert::InformationAlert("ISwatchList nil");
			break;
		}
		UID blackUID = iSwatchList->GetBlackSwatchUID();
		
		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
		if(applyStrokeCmd==nil)
		{
			ASSERT_FAIL("Cannot create the command to render the stroke?");
			CAlert::InformationAlert("ICommand nil");
			break;
		}

		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		{
			ASSERT_FAIL("Failed to render the stroke?");
			CAlert::InformationAlert("ProcessCommand failre");
			break;
		}
		
		CmdUtils::EndCommandSequence(sequence);
		newPageItem = newSplineItem;
		returnValue=kSuccess;
	} while(false);
	return returnValue;
}

ErrorCode BscDNDSrcDragSource::CreateTextbox(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{
	ErrorCode returnValue=kFailure;
	do 
	{
		ICommandSequence *sequence=CmdUtils::BeginCommandSequence();
		if (sequence==nil)
		{
			CAlert::InformationAlert("ICommandSequence nil");
			break;
		}

		PMPoint topLeft(5,5);
		PMPoint bottomRight(100,100);
		PMRect rect(topLeft, bottomRight);
		UIDRef newSplineItem = Utils<IPathUtils>()->CreateRectangleSpline
		(parent, rect, INewPageItemCmdData::kTextFrameAttributes, 0x263);

		const UIDList splineItemList(newSplineItem);

		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->
		CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==nil)
		{
			CAlert::InformationAlert("ICommand nil");
			break;
		}

		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
		{
			CAlert::InformationAlert("ProcessCommand failure");
			break;
		}

		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		if(iSwatchList==nil)
		{
			CAlert::InformationAlert("ISwatchList nil");
			break;
		}
		UID blackUID = iSwatchList->GetBlackSwatchUID();
		
		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
		if(applyStrokeCmd==nil)
		{
			CAlert::InformationAlert("ICommand nil");
			break;
		}

		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		{
			CAlert::InformationAlert("ProcessCommand failre");
			break;
		}

		CmdUtils::EndCommandSequence(sequence);
		newPageItem=newSplineItem;
		returnValue=kSuccess;
	} while(false);
	return returnValue;
}

bool16
BscDNDSrcDragSource::DoAddDragContent(IDragDropController* DNDController)
{
	bool16 result = kFalse;
	// do while(false) loop for error control
	do 
	{
		// Get the dataexchangehandler for the flavor we want to add 
		// The method QueryHandler is on the dataexchangecontroller, which adds extra functionality
		// to the dragdropcontroller. I am assuming we drag from our custom source to custom target
		// via the layout widget, if we attempt to go directly, the handler will be nil.
		InterfacePtr<IDataExchangeHandler> ourHandler(DNDController->QueryHandler(ourFlavor));
		// if we cannot get the handler for our flavor, we are dead in the water.
		// The handler gives us the scrap to store our content on...
		if (ourHandler==nil)
		{
			ASSERT_FAIL("Handler nil for our flavor?");
			break;
		}	
		// we need to place our content onto the scrap
		InterfacePtr<IPageItemScrapData> scrapData(ourHandler, UseDefaultIID());
		if (scrapData == nil)
		{
			ASSERT_FAIL("No scrap data for DEHandler?");
			break;
		}
		
		// get the root of the scrap	
		UIDRef 	scrapRoot =	scrapData->GetRootNode();
		// and its associated database
		IDataBase * scrapDB = scrapRoot.GetDataBase();

		// new page item
		UIDRef newPageItem;
		// set up a list of points for the page item
		PMPointList points(2);
		// define the text bounding box using two pmpoints
		PMPoint startPoint(0,0);
		PMPoint endPoint(100,100);
		points.Append( startPoint );
		points.Append( endPoint );
		PMReal strokeWeight(0.0);
		if (CreatePageItem(newPageItem,scrapRoot,points,strokeWeight)==kFailure)
		{
			ASSERT_FAIL("Failed to create page item?");
			break;
		}
		
		// Must manually clear the handler before adding any data...
		ourHandler->Clear();
		
		// we call replace to define the DB the item exists in. We add our new item to the scrap
		scrapData->Replace(UIDList(newPageItem));
		// Update the cached variable
		fPageItem = newPageItem;
		InterfacePtr<IPageItemLayerData> layerData(scrapData,IID_IPAGEITEMLAYERDATA);
		// I faux the layer the item is on.
		K2Vector<int32> layerIndexList(1);
		layerIndexList.Append(0);

		// Make a list of the layer names.
		K2Vector<PMString> layerNameList;
		layerNameList.Preallocate(1);
		PMString layerName("Untitled");
		layerNameList.Append(layerName);

		layerData->SetPageItemList(UIDList(newPageItem));
		layerData->SetLayerNameList(layerNameList);
		layerData->SetLayerIndexList(layerIndexList);
		layerData->SetIsGuideLayer(kFalse);

		
		// we point the controller at the pageitem handler 
		DNDController->SetSourceHandler(ourHandler);
			
		// we get the data object that represents the drag
		InterfacePtr<IPMDataObject> item(DNDController->AddDragItem(1));

		// no flavor flags	
		PMFlavorFlags flavorFlags = 0;
		
		// we set the type (flavour) in the drag object 
		item->PromiseFlavor(ourFlavor, flavorFlags);
		
 		result = kTrue;
	} while (false);

	

	return result; 
}

/* As we have a custom flavour, the drag target is unlikely to know how to draw feedback, therfore we draw it in the source */
SysRgn
BscDNDSrcDragSource::DoMakeDragOutlineRegion() const
{
	// do while(false) loop for error control
	do 
	{
		// get the geometry of the page item
		InterfacePtr<IGeometry> iGeometry(fPageItem,UseDefaultIID());
		if (iGeometry == nil)
		{
			ASSERT_FAIL("No geometry on scrap item!");
			break;
		}
		
		// we will draw a rectangle around the bounding box
		PMRect thePIGeo = iGeometry->GetStrokeBoundingBox();
	
		// we need to draw the bounding box at the current mouse location
		SysPoint currentMouse = GetMousePosition();
		PMPoint start(currentMouse);
		
		PMPoint lTop(thePIGeo.LeftTop());
		PMPoint rBottom(thePIGeo.RightBottom());

		lTop+=start;
		rBottom+=start;
		// create a new pmrect based on the page item, offset from the mouse location
		// On windows this feedback resorts to the default, on the mac it would be 
		PMRect offsetRect(lTop,rBottom);

		SysRect windowRect = ::ToSys(offsetRect);
		SysRgn origRgn = ::CreateRectSysRgn(windowRect);

		// Get the window region for the rectangle.
		::InsetSysRect(windowRect,7,7);
		
		SysRgn windowRgn = ::CreateRectSysRgn(windowRect);

		DiffSysRgn(origRgn,windowRgn,origRgn);
		return origRgn;
	}
	while (kFalse);
	return nil;
}

ErrorCode 
BscDNDSrcDragSource::CreatePageItem(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{
	ErrorCode returnValue = kFailure;
	do 
	{
		// wrap the commands in a sequence
		ICommandSequence *sequence = CmdUtils::BeginCommandSequence();
		if (sequence == nil)
		{
			ASSERT_FAIL("Cannot create command sequence?");
			break;
		}

		// create the page item
		UIDRef newSplineItem = Utils<IPathUtils>()->CreateLineSpline(parent, points, INewPageItemCmdData::kDefaultGraphicAttributes);
		
		// put the new item into a splinelist
		const UIDList splineItemList(newSplineItem);

		// we apply the stroke to our item
		InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->CreateStrokeWeightCommand(strokeWeight,&splineItemList,kTrue,kTrue));
		if(strokeSplineCmd==nil)
		{
			ASSERT_FAIL("Cannot create the command to stroke the spline?");
			break;
		}
				
		if(CmdUtils::ProcessCommand(strokeSplineCmd)==kFailure)
		{
			ASSERT_FAIL("Failed to stroke the spline?");
			break;
		}

		// we want to apply the stroke, color it black. We need to get the black swatch...
		InterfacePtr<ISwatchList> iSwatchList (Utils<ISwatchUtils>()->QueryActiveSwatchList());
		if(iSwatchList==nil)
		{
			ASSERT_FAIL("Cannot get swatch list?");
			break;
		}
		UID blackUID = iSwatchList->GetBlackSwatchUID();
		
		// Now get the command to apply the stroke...
		InterfacePtr<ICommand> applyStrokeCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeRenderingCommand(blackUID,&splineItemList,kTrue,kTrue));
		if(applyStrokeCmd==nil)
		{
			ASSERT_FAIL("Cannot create the command to render the stroke?");
			break;
		}

		if(CmdUtils::ProcessCommand(applyStrokeCmd)!=kSuccess)
		{
			ASSERT_FAIL("Failed to render the stroke?");
			break;
		}
		
		// now we can end the command sequence, we are done.
		CmdUtils::EndCommandSequence(sequence);
		newPageItem = newSplineItem;
		returnValue=kSuccess;
	} while(false);
	return returnValue;
}

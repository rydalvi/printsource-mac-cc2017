#include "VCPlugInHeaders.h"
#include "CDialogController.h"
#include "SystemUtils.h"
#include "OptnsDlgID.h"
#include "CAlert.h"

#define CA(x)	CAlert::InformationAlert(x)

class OptnsAttribPanelController : public CDialogController
{
	public:
		OptnsAttribPanelController(IPMUnknown* boss) : CDialogController(boss) {}
		virtual ~OptnsAttribPanelController() {}
		virtual void InitializeFields();
		virtual WidgetID ValidateFields();
		virtual void ApplyFields(const WidgetID& widgetId);
};

CREATE_PMINTERFACE(OptnsAttribPanelController, kAttribPanelControllerImpl)

void OptnsAttribPanelController::InitializeFields() 
{
	CDialogController::InitializeFields();
	this->SetTriStateControlData(kAppendRadBtnWidgetID, kTrue);
	this->SetTriStateControlData(kEmbedRadBtnWidgetID, kFalse);
}

WidgetID OptnsAttribPanelController::ValidateFields() 
{
	WidgetID result = CDialogController::ValidateFields();
	return result;
}

void OptnsAttribPanelController::ApplyFields(const WidgetID& widgetId) 
{
}

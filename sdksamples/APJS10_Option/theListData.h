#ifndef __THELISTDATA_H__
#define __THELISTDATA_H__
#include "vector"

using namespace std;

class listInfo
{
	public:
		char text[32];
		bool isSelected;
};

class listData
{
	private :
		static vector <listInfo>selectedRows;
	public:
		/* Will add data at specified index */
		void setData(bool,char*,  int);	
		/* Will get data of specified index from the list */
		bool getData(int, char*);
		/* Will add data at end of list */
		void setData(bool, char*);		
		/* The current selection of the list box */
		static int currentSelected;			
		/* Did the user drag a box successfully? */
		static bool didUserDragBox;			
		/* What was the last selected box when the user started the drag? */
		static UID lastSelectedUID;			
};

#endif
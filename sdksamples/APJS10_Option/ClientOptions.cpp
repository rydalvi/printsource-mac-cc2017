/*
//	File:	ClientOptions.cpp
//
//	Date:	17-Oct-2003
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "CPMUnknown.h"

// Project includes:
#include "IClientOptions.h"
#include "OptionsUtils.h"
#include "PubBrowseDlgCreator.h"
#include "PathBrowseDlgCreator.h"

#include "HelperInterface.h"
#include "OptnsDlgID.h"
#include "OptionsValue.h"
#include "OptionsStaticData.h"
#include "CAlert.h"
#include "ITextControlData.h"

#include "IAppFramework.h"
#include "IPanelMgr.h"
#include "IWindow.h"
#include "PaletteRefUtils.h"
#include "ICategoryBrowser.h"
#include "IWidgetParent.h"

#include "ISpecialChar.h"

#define CA(X) CAlert::InformationAlert(X)

bool16 doRefreshFlag = kFalse;

class ClientOptions : public CPMUnknown<IClientOptions>
{
	public:
		/**
		Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		ClientOptions(IPMUnknown*  boss);
		/** 
			Destructor.
		*/
		virtual ~ClientOptions();		
		virtual double getDefaultLocale(PMString&);
		virtual double getDefPublication(PMString&);
		virtual PMString getImageDownloadPath(void);
		virtual PMString getDocumentDownloadPath(void);
		virtual double getDefPublicationDlg(PMString&, bool16);
		virtual void setdoRefreshFlag(bool16);
		
		virtual bool8 checkforFolderExists(PMString&);
		PMString getWhiteBoardMediaPath();

		virtual void setLocale(PMString&, double&);
		virtual void setPublication(PMString&, double&);
		virtual void setSection(PMString& ,double& ) ;

		//virtual void setDefaultFLow(int32 );
		virtual void set_DisplayPartnerImages(int32 );
		virtual void set_DisplayPickListImages(int32 );

		void setAll(PMString& publicationName, double& publicationId ,PMString& languageName,
					double& languageId ,PMString& sectionName, double& sectionId);
    
        virtual void set_HorizontalAndVerticleSpacing(int32 hSpace, int32 vSpace );


	private:
		OptionsValue& getClientOptions(void);
		OptionsValue OptionsObj;
		double checkforPubExistance(PMString& ,double&);
		//bool8 checkforFolderExists(PMString&);
		double checkforLocaleExistance(PMString& lName,double& languageID);
		

};

CREATE_PMINTERFACE(ClientOptions, kClientOptionsImpl)

/* ClientOptions Constructor
*/
ClientOptions::ClientOptions(IPMUnknown* boss): 
	CPMUnknown<IClientOptions>(boss)
{
	
}

/* ClientOptions Destructor
*/
ClientOptions::~ClientOptions()
{
	// Add code to delete extra private data, if any.
}

OptionsValue& ClientOptions::getClientOptions(void)
{
	OptionsValue optnsVal;
	OptionsObj = optnsVal;
	do{
		ClientOptionsManip cops;		
		/*bool8 isFileExists=cops.IsClientOptionsFileExiats();
		if(isFileExists==FALSE){
			PMString filePath = cops.GetOptionsFilePath();
			if(cops.CreateOptionsFile(filePath)==FALSE){
				CAlert::InformationAlert("Failed to Create File");
				break;
			}
		}*/
		OptionsObj = cops.ReadClientOptions();
	}while(0);
	return OptionsObj;
}

double ClientOptions::getDefPublication(PMString& pubName)
{	
	//CA("Inside ClientOptions::getDefPublication");
	double val = -1;
	double pubID= -1;
	OptionsObj = getClientOptions();
	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return val;
	}

	double proID = (OptionsObj.getProjectID()).GetAsDouble();//added by mane

	if(OptionsObj.getPublicationName().NumUTF16TextChars() > 0 && checkforPubExistance(OptionsObj.getPublicationName(),proID) == -1)
	{
		//CA("OptionsObj.getPublicationName().NumUTF16TextChars() >0");
		PMString str("");
		str = OptionsObj.getPublicationName();
		if(str.NumUTF16TextChars() > 0 ){
			// if publication deleted from database
			PMString message("The current catalog \"");
			message += str;
			message += "\" does not exist. Please select another catalog.  ";
			PMString ok_btn("OK");			
			CAlert::ModalAlert(message,ok_btn,"","",1,CAlert::eInformationIcon);
            PMString asd("");
			OptionsObj.setPublicationName(asd);
			OptionsObj.setPublicationComment(asd);
			ClientOptionsManip cops;
			cops.WriteClientOptions(OptionsObj);	
		}		
	}
	if(OptionsObj.getPublicationName().NumUTF16TextChars() == 0)
	{
		//CA("OptionsObj.getPublicationName().NumUTF16TextChars() == 0");
		// tracking which button pressed
		PersistData::setPubBrowseDlgCancelPressFlag(FALSE);
		// for showing stand alone publication browse dialog
		/*PersistData::setPublicationDialogType(2);
		PubBrowseDlgCreator pubDlagObj(this);
		pubDlagObj.CreateDialog();*/

		//------------

		bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
		if(!isUserLoggedIn)
			return val;
		//CA("Inside selected class");
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil)
		{
			//CA("iApplication==nil");
			return val;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil)
		{
			//CA("iPanelMgr == nil");
			return val;
		}
		UID paletteUID = kInvalidUID;
		int32 TemplateTop	=0;	
		int32 TemplateLeft	=0;		
		int32 TemplateRight	=0;	
		int32 TemplateBottom =0;	

		const ActionID MyPalleteActionID = kCTBPanelWidgetActionID;

		IControlView* pnlControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
		if(pnlControlView == NULL)
		{
			//CA("pnlControlView is NULL");
			return val;
		}

		InterfacePtr<IWidgetParent> panelWidgetParent(pnlControlView, UseDefaultIID());
		if(panelWidgetParent == NULL)
		{
			//CA("panelWidgetParent is NULL");
			return val;
		}
		InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
		if(palette == NULL)
		{
			//CA("palette is NULL");
			return val;
		}
		InterfacePtr<IPMUnknown> unknown(iPanelMgr, IID_IUNKNOWN);
		PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(unknown);
		if(palRef.IsValid())
			//CA("validPaletRef");
		//----------------New CS3 Changes--------------------//
		bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
		
		if(palette)						//if(palette)
		{
			//CA("palette");
			palette->AddRef();
			GSysRect PalleteBounds = palette->GetFrameBBox();
			//SysRect PalleteBounds = PaletteRefUtils::GetPaletteBounds(palRef);
			TemplateTop		= 0; //PalleteBounds.top -30;
			TemplateLeft	= 0; //PalleteBounds.left;
			TemplateRight	= 50; //PalleteBounds.right;
			TemplateBottom	= 100; //PalleteBounds.bottom;
		
			InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
			if(!CatalogBrowserPtr)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSelectClassWidgetID--No CatalogBrowserPtr");
				return val;
			}
			CatalogBrowserPtr->OpenCategoryBrowser(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight ,1 ,-1);
		}

	//	OptionsObj = getClientOptions();
	//	pubID = checkforPubExistance(pubName,proID);//added by mane

	}
	pubName = OptionsObj.getPublicationName();
	pubID = checkforPubExistance(OptionsObj.getPublicationName(),proID);//added by mane

	//CA("pubName  :  "+pubName);

	ClientOptionsManip cops;
	cops.SetCatalogIDInAppFramework(pubID);
	/*if(PersistData::getPubBrowseDlgCancelPressFlag()==TRUE)
		return -1;*/
	//CA("Exit from ClientOptions::getDefPublication");
	return pubID;
}

double ClientOptions::getDefaultLocale(PMString& localeName)
{	
	//CA("ClientOptions::getDefaultLocale");
	double localeID= -1;
	OptionsObj = getClientOptions();
	double val = -1;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return val;
	}

	PMString languageIDStr = OptionsObj.getLocaleID();
	double languageID = languageIDStr.GetAsDouble();
	if(OptionsObj.getDefaultLocale().NumUTF16TextChars() >0 && checkforLocaleExistance(OptionsObj.getDefaultLocale(),languageID) == -1){
		//CA("OptionsObj.getDefaultLocale().NumUTF16TextChars() >0 && checkforLocaleExistance(OptionsObj.getDefaultLocale()) == -1");
		PMString str("");
		str = OptionsObj.getDefaultLocale();
		if(str.NumUTF16TextChars() > 0 ){
			// if publication deleted from database
			PMString message("The current Locale \"");
			message += str;
			message += "\" does not exist. Please select another Locale.  ";
			PMString ok_btn("OK");			
			CAlert::ModalAlert(message,ok_btn,"","",1,CAlert::eInformationIcon);
            PMString asd("");
			OptionsObj.setDefaultLocale(asd);
			ClientOptionsManip cops;
			cops.WriteClientOptions(OptionsObj);			
		}		
	}
	if(OptionsObj.getDefaultLocale().NumUTF16TextChars() == 0)
	{
		//CA("OptionsObj.getDefaultLocale().NumUTF16TextChars() == 0");
		// tracking which button pressed
		PersistData::setPubBrowseDlgCancelPressFlag(FALSE);
		// for showing stand alone publication browse dialog
		/*PersistData::setPublicationDialogType(2);
		PubBrowseDlgCreator pubDlagObj(this);
		pubDlagObj.CreateDialog();*/
		
		//-------------
		bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
		if(!isUserLoggedIn)			
			return val;
		//CA("Inside selected class");
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		if(iApplication==nil)
		{
			//CA("iApplication==nil");
			return val;
		}
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil)
		{
			//CA("iPanelMgr == nil");
			return val;
		}
		UID paletteUID = kInvalidUID;
		int32 TemplateTop	=0;	
		int32 TemplateLeft	=0;		
		int32 TemplateRight	=0;	
		int32 TemplateBottom =0;	

		const ActionID MyPalleteActionID = kCTBPanelWidgetActionID;

		IControlView* pnlControlView = iPanelMgr->GetPanelFromActionID(MyPalleteActionID);
		if(pnlControlView == NULL)
		{
			//CA("pnlControlView is NULL");
			return val;
		}

		InterfacePtr<IWidgetParent> panelWidgetParent( pnlControlView, UseDefaultIID());
		if(panelWidgetParent == NULL)
		{
			//CA("panelWidgetParent is NULL");
			return val;
		}
		InterfacePtr<IWindow> palette((IWindow*)panelWidgetParent->QueryParentFor(IWindow::kDefaultIID));
		if(palette == NULL)
		{
			//CA("palette is NULL");
			return val;
		}
		InterfacePtr<IPMUnknown> unknown(iPanelMgr, IID_IUNKNOWN);
		PaletteRef palRef=iPanelMgr->GetPaletteRefContainingPanel(unknown);
		//if(palRef.IsValid())
			//CA("validPaletRef");
		//----------------New CS3 Changes--------------------//
		//bool16 retval = PaletteRefUtils::IsPaletteVisible(palRef);
		
		if(palette)						//if(palette)
		{
			//CA("palette----1");
			palette->AddRef();
			//GSysRect PalleteBounds = palette->GetFrameBBox();
			//SysRect PalleteBounds = PaletteRefUtils::GetPaletteBounds(palRef);
			TemplateTop		= 0;//PalleteBounds.top -30;
			TemplateLeft	= 50;//PalleteBounds.left;
			TemplateRight	= 100; //PalleteBounds.right;
			TemplateBottom	= 100; //PalleteBounds.bottom;
	
			InterfacePtr<ICategoryBrowser> CatalogBrowserPtr((ICategoryBrowser*)::CreateObject(kCTBCategoryBrowserBoss, IID_ICATEGORYBROWSER));
			if(!CatalogBrowserPtr)
			{
				ptrIAppFramework->LogDebug("AP7_ProductFinder::SPSelectionObserver::Update::kSPSelectClassWidgetID--No CatalogBrowserPtr");
				return val;
			}
			CatalogBrowserPtr->OpenCategoryBrowser(TemplateTop, TemplateLeft, TemplateBottom, TemplateRight , 1 ,-1);
		}

	//	OptionsObj = getClientOptions();		
	//	localeID = checkforLocaleExistance(localeName,languageID);
	}

	
	localeName = OptionsObj.getDefaultLocale();
	localeID = checkforLocaleExistance(OptionsObj.getDefaultLocale(),languageID);	
	ClientOptionsManip cops;
	cops.SetLocaleIDInAppFramework(localeID);
	/*if(PersistData::getPubBrowseDlgCancelPressFlag()==TRUE)
		return -1;*/
	//CA("Exit from ClientOptions::getDefaultLocale");

	return localeID;
	
}



PMString ClientOptions::getImageDownloadPath(void)
{	//CA("ClientOptions::getImageDownloadPath");
	OptionsObj = getClientOptions();
	bool8 isDirExists = checkforFolderExists(OptionsObj.getImagePath());	
	//CA(OptionsObj.getImagePath());
	if(!isDirExists || OptionsObj.getImagePath().NumUTF16TextChars()==0 || OptionsObj.getImagePath() == "")
	{ 
		//CA("OptionsObj.getImagePath()="+OptionsObj.getImagePath());
		PersistData::setPathBrowseDialogType(1);
		PathBrowseDlgCreator pathDlgObj(this);
		pathDlgObj.CreateDialog();

		OptionsObj = getClientOptions();
	}
	return OptionsObj.getImagePath();
}

PMString ClientOptions::getDocumentDownloadPath(void)
{
	OptionsObj = getClientOptions();
	bool8 isDirExists = checkforFolderExists(OptionsObj.getIndesignDocPath());
	if(!isDirExists || OptionsObj.getIndesignDocPath().NumUTF16TextChars()==0 || OptionsObj.getIndesignDocPath() == "")
	{
		PersistData::setPathBrowseDialogType(2);
		PathBrowseDlgCreator pathDlgObj(this);
		pathDlgObj.CreateDialog();
		OptionsObj = getClientOptions();
	}
	return OptionsObj.getIndesignDocPath();
}

double ClientOptions::checkforPubExistance(PMString& pName,double& proID)
{
	//CA("ClientOptions::checkforPubExistance");
	ClientOptionsManip cops;
	double pubID;
	pubID = cops.checkforPublicationExistance(pName, proID);//added by mane
	PersistData::setPubId(pubID); //added by mane
	//CA("Premal1");
	return pubID;
}

double ClientOptions::checkforLocaleExistance(PMString& lName, double& languageID)
{
	double localeID=-1;
	ClientOptionsManip cops;
	localeID = cops.checkforLocaleExistance(lName,languageID);
	return localeID;
}

bool8 ClientOptions::checkforFolderExists(PMString& fpath)
{
	bool8 isFolderExists=FALSE;
	ClientOptionsManip cops;
	isFolderExists = cops.checkforFolderExistance(fpath);
	return isFolderExists;
}

double ClientOptions::getDefPublicationDlg(PMString& pubName, bool16 showDialog)
{//CA("ClientOptions::getDefPublicationDlg");
	double pubID= -1;
	OptionsObj = getClientOptions();	
	double proID = OptionsObj.getProjectID().GetAsDouble();//added by mane

	if(OptionsObj.getPublicationName().NumUTF16TextChars() >0 && checkforPubExistance(OptionsObj.getPublicationName(),proID) == -1)//argument(proID)added by mane
	{
		PMString str("");
		str = OptionsObj.getPublicationName();
		if(str.NumUTF16TextChars() > 0 ){
			// if publication deleted from database
			PMString message("The current catalog \"");
			message += str;
			message += "\" does not exist. Please select another catalog.  ";
			PMString ok_btn("OK");			
			CAlert::ModalAlert(message,ok_btn,"","",1,CAlert::eInformationIcon);
            PMString asd("");
			OptionsObj.setPublicationName(asd);
			OptionsObj.setPublicationComment(asd);
			ClientOptionsManip cops;
			cops.WriteClientOptions(OptionsObj);			
		}		
	}
	if(showDialog)
	{
		// tracking which button pressed
		PersistData::setPubBrowseDlgCancelPressFlag(FALSE);
		// for showing stand alone publication browse dialog
		PersistData::setPublicationDialogType(2);
		PubBrowseDlgCreator pubDlagObj(this);
		pubDlagObj.CreateDialog();
		OptionsObj = getClientOptions();		
		pubID = checkforPubExistance(pubName,proID);
	}
	pubName = OptionsObj.getPublicationName();
	proID = OptionsObj.getProjectID().GetAsDouble();//added by mane
	pubID = checkforPubExistance(OptionsObj.getPublicationName(), proID);//added by mane

	ClientOptionsManip cops;
	cops.SetCatalogIDInAppFramework(pubID);
	if(PersistData::getPubBrowseDlgCancelPressFlag()==TRUE)
		return -1;
	//CA("exit from showDialog");
	return pubID;
}


void ClientOptions::setdoRefreshFlag(bool16 Flag)
{
	doRefreshFlag = Flag;	
}

void ClientOptions::setLocale(PMString& localeName,double& localeId)
{
	//CA("ClientOptions::setLocale");

	PersistData::setLocaleName(localeName);
	
	ClientOptionsManip cop;
	double LocaleID= cop.checkforLocaleExistance(localeName,localeId);
	
	//localeId.AppendNumber(LocaleID);
	
	PersistData::setLocaleID(LocaleID);
	
	double localeID = PersistData::getLocaleID();

	PMString languageIdStr("");
	languageIdStr.AppendNumber(PMReal(localeId));
	
	OptionsValue optnsObj = cop.ReadClientOptions();
	optnsObj.setDefaultLocale(localeName);
	optnsObj.setLocaleID(languageIdStr);
	cop.WriteClientOptions(optnsObj);

	//CA("localeId : "+ localeId);

	IPanelControlData *panelControlData = PersistData::getGeneralPanelControlData();
	if(panelControlData==nil){
		//CAlert::InformationAlert("General panel control data nil");
		return;
	}
	IControlView* iControlView = panelControlData->FindWidget(kLocaleStaticWidgetID);
	if(iControlView == nil)
	{
		CAlert::InformationAlert("Fail to find publication static text widget");
		return;
	}	

	InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
	if (textControlData == nil)
	{
		CAlert::InformationAlert("ITextControlData nil");	
		return;
	}

	localeName.SetTranslatable(kFalse);
    localeName.ParseForEmbeddedCharacters();
	textControlData->SetString(localeName);

}

void ClientOptions::setPublication(PMString& pubName,double& pubId)
{
	//CA("ClientOptions::setPublication");
	
	PMString pubNewName = "";
	//pubNewName = pubName ;

	bool16 isOneSource = kFalse;	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return;
	}

	if(ptrIAppFramework->get_isONEsourceMode())
		isOneSource = kTrue;
	else
		isOneSource = kFalse;


	ClientOptionsManip cop;
	double publicationID= cop.checkforPublicationExistance(pubName,pubId);
	
	//localeId.AppendNumber(LocaleID);
	
	PersistData::setPubId(pubId);
	PersistData::setPubName(pubName);

	//int32 publicationID = PersistData::getPubId();

	PMString pubIdStr("");
	pubIdStr.AppendNumber(PMReal(publicationID));

	OptionsValue optnsObj = cop.ReadClientOptions();
	double localeID = PersistData::getLocaleID();

	PMString languageIdStr("");
	languageIdStr.AppendNumber(PMReal(localeID));
	optnsObj.setLocaleID(languageIdStr);
	optnsObj.setPublicationName(pubName);
	optnsObj.setProjectID(pubIdStr);

	PMString text = "";
	if(isOneSource)
	{
		text = "true";
		optnsObj.setIsOneSource(text);
	}
	else
	{
		text = "false";
		optnsObj.setIsOneSource(text);
	}

	cop.WriteClientOptions(optnsObj);	
	cop.SetCatalogIDInAppFramework(publicationID);

	//CA("localeId : "+ localeId);
	//CA("PubName : " + pubName);	
	//---------
	PMString strID = optnsObj.getLocaleID();
	double lang_ID = strID.GetAsDouble();
	if(isOneSource)
	{
		VectorClassInfoPtr vectClsValuePtr = ptrIAppFramework->ClassificationTree_getRoot(lang_ID); 
		if(vectClsValuePtr == nil){
			ptrIAppFramework->LogDebug("AP7_Options::ClientOptions::setPublication::vectClsValuePtr == nil ");
			return ;
		}
		VectorClassInfoValue::iterator itr;
		itr = vectClsValuePtr->begin();
		
		pubNewName = itr->getClassification_name();
	}
	else
	{

		CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(pubId,lang_ID);
									
		double pubRootID = pubModelRootValue.getRootID();
		
		CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(pubRootID,lang_ID);
		PMString CatalogName = "";

		PMString TempBuffer = pubModelValue.getName();
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{
			CatalogName=iConverter->translateString(TempBuffer);
		}
		else
			CatalogName = TempBuffer;

		pubName = CatalogName;
	}

	
	IPanelControlData *panelControlData = PersistData::getGeneralPanelControlData();
	if(panelControlData==nil){
		//CAlert::InformationAlert("General panel control data nil");
		return;
	}
	IControlView* iControlView = panelControlData->FindWidget(kPublicationStaticWidgetID);
	if(iControlView == nil)
	{
		//CAlert::InformationAlert("Fail to find publication static text widget");
		return;
	}	

	InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
	if (textControlData == nil)
	{
		//CAlert::InformationAlert("ITextControlData nil");	
		return;
	}

	if(isOneSource)
		{
			pubNewName.SetTranslatable(kFalse);
            pubNewName.ParseForEmbeddedCharacters();
			textControlData->SetString(pubNewName);
		}
	else
		{
			pubName.SetTranslatable(kFalse);
            pubName.ParseForEmbeddedCharacters();
			textControlData->SetString(pubName);
		}

}


PMString ClientOptions::getWhiteBoardMediaPath()
{
	//CA("ClientOptions::getWhiteBoardMediaPath");
	PMString WBMediaPath("");

	WBMediaPath = this->getImageDownloadPath();	
	if(WBMediaPath!="")
	{
		const char *imageP=  (WBMediaPath.GetPlatformString().c_str());
		if(imageP[std::strlen(imageP)-1]!='\\' || imageP[std::strlen(imageP)-1]!=':')
			#ifdef MACINTOSH
				WBMediaPath+=":";
			#else
				WBMediaPath+="\\";
			#endif
		WBMediaPath.Append("InDesign");
			#ifdef MACINTOSH
				WBMediaPath+=":";
			#else
				WBMediaPath+="\\";
			#endif
		WBMediaPath.Append("WhiteBoard");
			#ifdef MACINTOSH
				WBMediaPath+=":";
			#else
				WBMediaPath+="\\";
			#endif
	}

	return WBMediaPath;
}

void ClientOptions::setSection(PMString& secName,double& secId)
{
	//CA("ClientOptions::setSection");
	
	ClientOptionsManip cop;

	PMString secIdStr("");
	secIdStr.AppendNumber(PMReal(secId));

	OptionsValue optnsObj = cop.ReadClientOptions();
	optnsObj.setSectionName(secName);
	optnsObj.setSectionID(secIdStr);

	cop.WriteClientOptions(optnsObj);

}
//-----17-09-09------
void ClientOptions::set_DisplayPartnerImages(int32 partnerImgs)
{
	//CA("ClientOptions::set_DisplayPartnerImages");
	ClientOptionsManip cop;

	OptionsValue optnsObj = cop.ReadClientOptions();
	optnsObj.setDisplayPartnerImgs(partnerImgs);
	
	cop.WriteClientOptions(optnsObj);

}

void ClientOptions::set_DisplayPickListImages(int32 pickListImgs)
{
	//CA("ClientOptions::set_DisplayPickListImages");
	ClientOptionsManip cop;

	OptionsValue optnsObj = cop.ReadClientOptions();
	optnsObj.setDisplayPickListImgs(pickListImgs);
	
	cop.WriteClientOptions(optnsObj);

}


void ClientOptions::set_HorizontalAndVerticleSpacing(int32 hSpace, int32 vSpace )
{
	//CA("ClientOptions::set_HorizontalAndVerticleSpacing");
	ClientOptionsManip cop;
    
	OptionsValue optnsObj = cop.ReadClientOptions();
    
	optnsObj.setHorizontalSpacing(hSpace);
    optnsObj.setVerticalSpacing(vSpace);
	
	cop.WriteClientOptions(optnsObj);
    
}


//void ClientOptions::setAll(PMString& pubName, int32& pubId ,PMString& localeName,
//	int32& localeId ,PMString& sectionName, int32& sectionId)
//{
//	ClientOptionsManip cop;
//	OptionsValue optnsObj = cop.ReadClientOptions();
//
//	PMString publicationIdStr("");
//	publicationIdStr.AppendNumber(pubId);
//
//	PMString languageIdStr("");
//	languageIdStr.AppendNumber(localeId);
//
//	PMString sectionIdStr("");
//	sectionIdStr.AppendNumber(sectionId);
//
//
//	//CA("ClientOptions::setPublication");
//	
//	PMString pubNewName = "";
//	//pubNewName = pubName ;
//
//	bool16 isOneSource = kFalse;	
//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
//	if(ptrIAppFramework == nil)
//	{
//		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
//		return;
//	}
//
//	if(ptrIAppFramework->get_isONEsourceMode())
//		isOneSource = kTrue;
//	else
//		isOneSource = kFalse;
//
//
//	//ClientOptionsManip cop;
//	int32 publicationID= cop.checkforPublicationExistance(pubName,pubId);
//	
//	//localeId.AppendNumber(LocaleID);
//	
//	PersistData::setPubId(pubId);
//	PersistData::setPubName(pubName);
//
//	//int32 publicationID = PersistData::getPubId();
//
//	PMString pubIdStr("");
//	pubIdStr.AppendNumber(publicationID);
//
//	//OptionsValue optnsObj = cop.ReadClientOptions();
//	optnsObj.setPublicationName(pubName);
//	optnsObj.setProjectID(pubIdStr);
//
//	PMString text = "";
//	if(isOneSource)
//	{
//		text = "true";
//		optnsObj.setIsOneSource(text);
//	}
//	else
//	{
//		text = "false";
//		optnsObj.setIsOneSource(text);
//	}
//
//	//cop.WriteClientOptions(optnsObj);	
//	cop.SetCatalogIDInAppFramework(publicationID);
//
//	//CA("localeId : "+ localeId);
//	//CA("PubName : " + pubName);	
//	//---------
//	PMString strID = optnsObj.getLocaleID();
//	int32 lang_ID = strID.GetAsNumber();
//	if(isOneSource)
//	{
//		VectorClassInfoPtr vectClsValuePtr = ptrIAppFramework->ClassificationTree_getRoot(lang_ID); 
//		if(vectClsValuePtr == nil){
//			ptrIAppFramework->LogDebug("AP7_Options::ClientOptions::setPublication::vectClsValuePtr == nil ");
//			return ;
//		}
//		VectorClassInfoValue::iterator itr;
//		itr = vectClsValuePtr->begin();
//		
//		pubNewName = itr->getClassification_name();
//	}
//	else
//	{
//
//		CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(pubId,lang_ID);
//									
//		int32 pubRootID = pubModelRootValue.getRootID();
//		
//		CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(pubRootID,lang_ID);
//		PMString CatalogName = "";
//
//		PMString TempBuffer = pubModelValue.getName();
//		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
//		if(iConverter)
//		{
//			CatalogName=iConverter->translateString(TempBuffer);
//		}
//		else
//			CatalogName = TempBuffer;
//
//		pubName = CatalogName;
//	}
//
//	
//	IPanelControlData *panelControlData = PersistData::getGeneralPanelControlData();
//	if(panelControlData==nil){
//		//CAlert::InformationAlert("General panel control data nil");
//		return;
//	}
//	IControlView* iControlView = panelControlData->FindWidget(kPublicationStaticWidgetID);
//	if(iControlView == nil)
//	{
//		//CAlert::InformationAlert("Fail to find publication static text widget");
//		return;
//	}	
//
//	InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
//	if (textControlData == nil)
//	{
//		//CAlert::InformationAlert("ITextControlData nil");	
//		return;
//	}
//	if(isOneSource)
//		textControlData->SetString(pubNewName);
//	else
//		textControlData->SetString(pubName);
//
//
//
//
//
//
//
//
//	//CA("ClientOptions::setLocale");
//
//	PersistData::setLocaleName(localeName);
//	
//	//ClientOptionsManip cop;
//	int32 LocaleID= cop.checkforLocaleExistance(localeName,localeId);
//	
//	//localeId.AppendNumber(LocaleID);
//	
//	PersistData::setLocaleID(LocaleID);
//	
//	//int32 localeID = PersistData::getLocaleID();
//
//	//PMString languageIdStr("");
//	//languageIdStr.AppendNumber(localeId);
//	
//	//OptionsValue optnsObj = cop.ReadClientOptions();
//	//optnsObj.setDefaultLocale(localeName);
//	//optnsObj.setLocaleID(languageIdStr);
//	//cop.WriteClientOptions(optnsObj);
//
//	//CA("localeId : "+ localeId);
//
//	//IPanelControlData *panelControlData = PersistData::getGeneralPanelControlData();
//	//if(panelControlData==nil){
//	//	//CAlert::InformationAlert("General panel control data nil");
//	//	return;
//	//}
//	IControlView* iControlView1 = panelControlData->FindWidget(kLocaleStaticWidgetID);
//	if(iControlView1 == nil)
//	{
//		CAlert::InformationAlert("Fail to find publication static text widget");
//		return;
//	}	
//
//	InterfacePtr<ITextControlData> textControlData1(iControlView1,UseDefaultIID());
//	if (textControlData1 == nil)
//	{
//		CAlert::InformationAlert("ITextControlData1 nil");	
//		return;
//	}
//
//	textControlData1->SetString(localeName);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//	
//	//optnsObj.setPublicationName(pubName);
//	//optnsObj.setProjectID(publicationIdStr);
//	
//	optnsObj.setDefaultLocale(localeName);
//	optnsObj.setLocaleID(languageIdStr);
//	
//	optnsObj.setSectionName(sectionName);
//	optnsObj.setSectionID(sectionIdStr);
//
//	cop.WriteClientOptions(optnsObj);
//
//
//}




void ClientOptions::setAll(PMString& pubName,double& pubId,PMString& localeName,double& localeId,
						   PMString& secName,double& secId){

PersistData::setLocaleName(localeName);
	
	ClientOptionsManip cop;
	double LocaleID= cop.checkforLocaleExistance(localeName,localeId);
	
	//localeId.AppendNumber(LocaleID);
	
	PersistData::setLocaleID(LocaleID);
	
	double localeID = PersistData::getLocaleID();

	PMString languageIdStr("");
	languageIdStr.AppendNumber(PMReal(localeId));
	
	OptionsValue optnsObj = cop.ReadClientOptions();
	optnsObj.setDefaultLocale(localeName);
	optnsObj.setLocaleID(languageIdStr);
	//cop.WriteClientOptions(optnsObj);

	//CA("localeId : "+ localeId);

	IPanelControlData *panelControlData = PersistData::getGeneralPanelControlData();
	if(panelControlData==nil){
		//CAlert::InformationAlert("General panel control data nil");
		return;
	}
	IControlView* iControlView = panelControlData->FindWidget(kLocaleStaticWidgetID);
	if(iControlView == nil)
	{
		CAlert::InformationAlert("Fail to find publication static text widget");
		return;
	}	

	InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
	if (textControlData == nil)
	{
		CAlert::InformationAlert("ITextControlData nil");	
		return;
	}

	localeName.SetTranslatable(kFalse);
    localeName.ParseForEmbeddedCharacters();
	textControlData->SetString(localeName);

	PMString pubNewName = "";
	//pubNewName = pubName ;

	bool16 isOneSource = kFalse;	
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return;
	}

	if(ptrIAppFramework->get_isONEsourceMode())
		isOneSource = kTrue;
	else
		isOneSource = kFalse;


	//ClientOptionsManip cop;
	double publicationID= cop.checkforPublicationExistance(pubName,pubId);
	
	//localeId.AppendNumber(LocaleID);
	
	PersistData::setPubId(pubId);
	PersistData::setPubName(pubName);

	//int32 publicationID = PersistData::getPubId();

	PMString pubIdStr("");
	pubIdStr.AppendNumber(PMReal(publicationID));

	//OptionsValue optnsObj = cop.ReadClientOptions();
	optnsObj.setPublicationName(pubName);
	optnsObj.setProjectID(pubIdStr);

	PMString text = "";
	if(isOneSource)
	{
		text = "true";
		optnsObj.setIsOneSource(text);
	}
	else
	{
		text = "false";
		optnsObj.setIsOneSource(text);
	}

	//cop.WriteClientOptions(optnsObj);	
	cop.SetCatalogIDInAppFramework(publicationID);

	//CA("localeId : "+ localeId);
	//CA("PubName : " + pubName);	
	//---------
	PMString strID = optnsObj.getLocaleID();
	double lang_ID = strID.GetAsDouble();
	if(isOneSource)
	{
		VectorClassInfoPtr vectClsValuePtr = ptrIAppFramework->ClassificationTree_getRoot(lang_ID); 
		if(vectClsValuePtr == nil){
			ptrIAppFramework->LogDebug("AP7_Options::ClientOptions::setPublication::vectClsValuePtr == nil ");
			return ;
		}
		VectorClassInfoValue::iterator itr;
		itr = vectClsValuePtr->begin();
		
		pubNewName = itr->getClassification_name();
	}
	else
	{

		CPubModel pubModelRootValue = ptrIAppFramework->getpubModelByPubID(pubId,lang_ID);
									
		double pubRootID = pubModelRootValue.getRootID();
		
		CPubModel pubModelValue = ptrIAppFramework->getpubModelByPubID(pubRootID,lang_ID);
		PMString CatalogName = "";

		PMString TempBuffer = pubModelValue.getName();
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{
			CatalogName=iConverter->translateString(TempBuffer);
		}
		else
			CatalogName = TempBuffer;

		pubName = CatalogName;
	}

	
	IPanelControlData *panelControlData1 = PersistData::getGeneralPanelControlData();
	if(panelControlData1==nil){
		//CAlert::InformationAlert("General panel control data nil");
		return;
	}


	IControlView* iControlView1 = panelControlData1->FindWidget(kPublicationStaticWidgetID);
	if(iControlView1 == nil)
	{
		//CAlert::InformationAlert("Fail to find publication static text widget");
		return;
	}	

	InterfacePtr<ITextControlData> textControlData1(iControlView1,UseDefaultIID());
	if (textControlData1 == nil)
	{
		//CAlert::InformationAlert("ITextControlData nil");	
		return;
	}
	if(isOneSource)
		{
			pubNewName.SetTranslatable(kFalse);
            pubNewName.ParseForEmbeddedCharacters();
			textControlData1->SetString(pubNewName);
		}
	else
		{
			pubName.SetTranslatable(kFalse);
            pubName.ParseForEmbeddedCharacters();
			textControlData1->SetString(pubName);
		}
	//ClientOptionsManip cop;

	PMString secIdStr("");
	secIdStr.AppendNumber(PMReal(secId));

	//OptionsValue optnsObj = cop.ReadClientOptions();
	optnsObj.setSectionName(secName);
	optnsObj.setSectionID(secIdStr);

	cop.WriteClientOptions(optnsObj);

}

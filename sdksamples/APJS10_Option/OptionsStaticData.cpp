

#include "VCPlugInHeaders.h"
#include "OptionsStaticData.h"

bool8 PersistData::isPubBrowseCancelPress=FALSE;
PMString PersistData::PubName;
PMString PersistData::LocaleName;
OptionsValue PersistData::curOptions;
IPanelControlData *PersistData::generalPanelControlData; 
bool16 PersistData::PublicationDialogType=0;
bool16 PersistData::PathBrowseDialogType=0;
double PersistData::LocaleID = -1;
double PersistData::PubID=-1;


double PersistData::getPubId(void)
{
	return PersistData::PubID;
}
void  PersistData::setPubId(double id)
{
	PersistData::PubID=id;
}

//Publication name 
PMString PersistData::getPubName(void)
{
	return PersistData::PubName;
}
void  PersistData::setPubName(PMString str)
{
	PersistData::PubName=str;
}
PMString PersistData::getLocaleName(void)
{
	return PersistData::LocaleName;
}
void  PersistData::setLocaleName(PMString str)
{
	PersistData::LocaleName=str;
}

// OptionsValue
OptionsValue&  PersistData::getOptions(void)
{	
	return PersistData::curOptions;
}
void  PersistData::setOptions(OptionsValue& value)
{
	curOptions= value;
}

// PublicationDialogType : 1 - showing with options dialog, 
// 2 - stand along with some other plugins
bool16 PersistData::getPublicationDialogType(void)
{
	return PersistData::PublicationDialogType;
}
void  PersistData::setPublicationDialogType(bool16 dlgType)
{
	PersistData::PublicationDialogType=dlgType;
}

// PathBrowseDialogType : 1 - image path, 2- doc path
bool16 PersistData::getPathBrowseDialogType(void)
{
	return PersistData::PathBrowseDialogType;
}
void  PersistData::setPathBrowseDialogType(bool16 dlgType)
{
	PersistData::PathBrowseDialogType=dlgType;
}

bool8 PersistData::getPubBrowseDlgCancelPressFlag(void)
{
	return PersistData::isPubBrowseCancelPress;
}
void  PersistData::setPubBrowseDlgCancelPressFlag(bool8 cancelFlag)
{
	PersistData::isPubBrowseCancelPress=cancelFlag;
}

IPanelControlData* PersistData::getGeneralPanelControlData(void)
{
	return generalPanelControlData;
}
void PersistData::setGeneralPanelControlData(IPanelControlData *paneldata)
{
	generalPanelControlData=paneldata;
}

void PersistData::setLocaleID(double id)
{
	LocaleID = id;
}

double PersistData::getLocaleID(void)
{
	return LocaleID;
}
//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
//#include "IDialog.h"
#include "IDialogCreator.h"
#include "IPanelControlData.h"
#include "ISelectableDialogSwitcher.h"
#include "ISession.h"
#include "IActionStateList.h"

// General includes:
#include "SDKUtilities.h"
#include "CActionComponent.h"
#include "CAlert.h"
#include "IDialogMgr.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"



// Project includes:
#include "IAppFramework.h"
#include "OptnsDlgID.h"
#include "PMLocaleId.h"

// Interface includes:
/*#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialog.h"
#include "IDialogCreator.h"
#include "IPanelControlData.h"
#include "ISelectableDialogSwitcher.h"
#include "ISession.h"
#include "IActionStateList.h"

// General includes:
#include "SDKUtilities.h"
#include "CActionComponent.h"
#include "CAlert.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "IDialogMgr.h"

// Project includes:
#include "IAppFramework.h"
#include "OptnsDlgID.h"*/


/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup apjs9_option

*/
class OptnsDlgActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		OptnsDlgActionComponent(IPMUnknown* boss);

		/** 
			Destructor.
		*/
		~OptnsDlgActionComponent();

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/

		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
		void UpdateActionStates(IActionStateList*);
		virtual void    UpdateActionStates(IActiveContext* ac, IActionStateList *listToUpdate, GSysPoint mousePoint, IPMUnknown* widget); //Added;


	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();
		
		/** Opens this plug-in's dialog. */
		void DoDialog();

	/*		@param isTabStyle tells this function if the current dialog is tab style or not
		*/


		void DoSelectableDialog(bool	isTabStyle = kFalse);

		

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(OptnsDlgActionComponent, kOptnsDlgActionComponentImpl)

/* OptnsDlgActionComponent Constructor
*/
OptnsDlgActionComponent::OptnsDlgActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* OptnsDlgActionComponent Destructor
*/
OptnsDlgActionComponent::~OptnsDlgActionComponent()
{
	// Add code to delete extra private data, if any.
}


/* DoAction
*/
void OptnsDlgActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		case kOptnsDlgTabDialogActionID:
		{
			//CAlert::InformationAlert("kOptnsDlgTabDialogActionID");
			this->DoDialog();
			//this->DoSelectableDialog(kTrue);
			break;
		}

		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void OptnsDlgActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kOptnsDlgAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}


/* DoSelectableDialog
*/
/*void OptnsDlgActionComponent::DoSelectableDialog(bool	isTabStyle)
{
	do
	{
		// Get the registered services, including our selectable dialog service:
		InterfacePtr<IK2ServiceRegistry> sRegistry(gSession, UseDefaultIID());
		if (sRegistry == nil)
		{
			ASSERT_FAIL("OptnsDlgActionComponent::DoSelectableDialog: sRegistry invalid");
			break;
		}

		// The service is provided based on the service ID and the boss passed in 
		ClassID	classID;
		if (isTabStyle)
		{
			classID = kOptnsDlgTabDialogBoss;
		}
		else
		{
			classID = kOptnsDlgDialogBoss;
		}
		InterfacePtr<IK2ServiceProvider> 
			sdService(sRegistry->QueryServiceProviderByClassID(kSelectableDialogServiceImpl, classID));
		
		if (sdService == nil)
		{
			ASSERT_FAIL("OptnsDlgActionComponent::DoSelectableDialog: sdService invalid");
			break;
		}

		// This is an interface to our service, which will end up calling our 
		// plug-in's dialog creator implementation:
		InterfacePtr<IDialogCreator> dialogCreator(sdService, IID_IDIALOGCREATOR);
		if (dialogCreator == nil)
		{
			ASSERT_FAIL("OptnsDlgActionComponent::DoSelectableDialog: dialogCreator invalid");
			break;
		}
		
		// This is our CreateDialog, which loads the dialog from our resource:
		IDialog* dialog = dialogCreator->CreateDialog();
		if (dialog == nil)
		{
			ASSERT_FAIL("OptnsDlgActionComponent::DoSelectableDialog: could not create dialog");
			break;
		}

		// The panel data will be the selectable panel of the dialog:
		InterfacePtr<IPanelControlData> panelData(dialog, UseDefaultIID());
		if (panelData == nil)
		{
			ASSERT_FAIL("OptnsDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}

		// Find the panel:
		WidgetID	widgetID;
		if (isTabStyle)
		{
			widgetID = kOptnsDlgTabDialogWidgetID;
		}
		else
		{
			widgetID = kOptnsDlgDialogWidgetID;
		}
		IControlView* dialogView = panelData->FindWidget(widgetID);
		if (dialogView == nil)
		{
			ASSERT_FAIL("OptnsDlgActionComponent::DoSelectableDialog: dialogView invalid");
			break;
		}
		
		// The dialog switcher controls how the panels are switched between:
		InterfacePtr<ISelectableDialogSwitcher> 
			dialogSwitcher(dialogView, IID_ISELECTABLEDIALOGSWITCHER);
		if (dialogSwitcher == nil)
		{
			ASSERT_FAIL("OptnsDlgActionComponent::DoSelectableDialog: dialogSwitcher invalid");
			break;
		}
	
		dialogSwitcher->SetDialogServiceID(kOptnsDlgService);

		// Sets default panel.
		dialogSwitcher->SwitchDialogPanelByID(kGeneralPanelWidgetID);

		// Open the dialog.
		dialog->Open(); 
	
	} while (false);			
}*/

void OptnsDlgActionComponent::UpdateActionStates(IActionStateList* actionState)
{	
	//AppFramework* ptrIAppFramework = new AppFramework(); commented by vaibhav
	//CAlert::InformationAlert("OptnsDlgActionComponent::UpdateActionStates");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));

	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("12Pointer to IAppFramework is nil.");		
		return;
	}	
	//bool16 status = ptrIAppFramework->getLoginStatus(Indesign);	changes @ vaibhav
	bool16 status = ptrIAppFramework->getLoginStatus();	
	if(status == kTrue){
		CAlert::InformationAlert("status == kTrue ");
		actionState->SetNthActionState(0,/*kTrue*/kEnabledAction | kSelectedAction);  //cs4
	}
	else{
		CAlert::InformationAlert("status == kFalse ");
		actionState->SetNthActionState(0,kFalse);
	}
}



/* DoDialog
*/
void OptnsDlgActionComponent::DoDialog()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kOptnsDlgPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kGeneralPanelResourceID, //kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kModeless /*kMovableModal*/);
		ASSERT(dialog);
		if (dialog == nil) {
			break;
		}
		// Open the dialog.
		dialog->Open();	
	} while (false);			
}

//***** Added Cs4  By Sachin Sharma
void    OptnsDlgActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList *listToUpdate, GSysPoint mousePoint, IPMUnknown* widget)
{
	//CAlert::InformationAlert("OptnsDlgActionComponent::UpdateActionStates");
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		//CAlert::InformationAlert("12Pointer to IAppFramework is nil.");		
		return;
	}	
	
	bool16 status = ptrIAppFramework->getLoginStatus();	
	if(status == kTrue){
		
		listToUpdate->SetNthActionState(0,kTrue); 
	}
	else{
		
		listToUpdate->SetNthActionState(0,kFalse);
	}
}

//  Code generated by DollyXs code generator

#ifndef __SLUGSTRUCT_H__
#define __SLUGSTRUCT_H__
#include "VCPluginHeaders.h"

class SlugStruct
{
public:
	int32 elementId;
	int32 typeId;
	int32 parentId;
	PMString elementName;
	PMString colName;
	int16 whichTab;
	int32 reserved1;
	int32 reserved2;
};

#endif
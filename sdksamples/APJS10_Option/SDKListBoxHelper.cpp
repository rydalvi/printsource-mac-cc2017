#include "VCPlugInHeaders.h"

#include "IListControlDataOf.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IListBoxAttributes.h"
#include "IControlView.h"
#include "IApplication.h"

#include "PersistUtils.h"
#include "PalettePanelUtils.h"

#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "CAlert.h"
#include "RsrcSpec.h"
#include "OptnsDlgID.h"

#include "SDKListBoxHelper.h"
#include "IListBoxController.h"

#define CA(x)	CAlert::InformationAlert(x)

SDKListBoxHelper::SDKListBoxHelper(IPMUnknown * owner, int32 pluginId) : fOwner(owner), fOwnerPluginID(pluginId)
{
}

SDKListBoxHelper::~SDKListBoxHelper()
{
	fOwner=nil;
	fOwnerPluginID=0;
}
/* 
Here passing IPanelControlData of desired listbox got in ListboxObserver
*/
IControlView* SDKListBoxHelper ::FindCurrentListBox(InterfacePtr<IPanelControlData> iPanelControlData, int i)//Give me the iControlView of the listbox. huh big deal!!
{
	if(!verifyState())
		return nil;

	IControlView * listBoxControlView2 = nil;

	do {
/*		InterfacePtr<IPanelControlData> iPanelControlData(fOwner,UseDefaultIID());
		if(iPanelControlData == nil) 
		{
			CA("iPanelControlData is nil");
			break;
		}
		
		PMString s("i value here:");
		s.AppendNumber(i);
		CA(s);
*/
		if(iPanelControlData ==nil)
		{
			CA("iPanelControlData nil");
			break;
		}
		if(i==1)
		{
			listBoxControlView2 = 
				iPanelControlData->FindWidget(kGeneralPanelListBoxWidgetID);
		}
		else if(i==2)
		{
			listBoxControlView2 = 
				iPanelControlData->FindWidget(kAttribPanelListBoxWidgetID);
		}
		else if(i==3)
		{
			listBoxControlView2 = 
				iPanelControlData->FindWidget(kTaskPanelListBoxWidgetID);
		}
		else if(i==4)
		{
			listBoxControlView2 = 
				iPanelControlData->FindWidget(kItemPanelListBoxWidgetID);
		}
		
		if(listBoxControlView2 == nil) 
		{
			CA("listBoxControlView2 nil in FindCurrentListBox()");
			break;
		}
	} while(0);
	return listBoxControlView2;
}

/* 
Here passing IControlView* of desired listbox returned from FindCurrentListBox()
*/

void SDKListBoxHelper::AddElement(IControlView* lstboxControlView, const PMString & displayName, WidgetID updateWidgetId, int atIndex, int x)
{
	if(!verifyState())
		return;
	do	
	{
/*		IControlView * listBox = this->FindCurrentListBox(panel, x);
		if(listBox == nil) 
		{
			CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}*/
		InterfacePtr<IListBoxAttributes> listAttr(lstboxControlView, UseDefaultIID());
		if(listAttr == nil) 
		{
			CAlert::InformationAlert("FindCurrentListBox 1 nil");
			break;	
		}
		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID == 0)
		{
			CAlert::InformationAlert("FindCurrentListBox 2 nil");
			return;
		}
		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwnerPluginID, kViewRsrcType, widgetRsrcID);
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(lstboxControlView), elementResSpec, IID_ICONTROLVIEW));
		ASSERT_MSG(newElView != nil, "SDKListBoxHelper::AddElement() Cannot create element");
		if(newElView == nil) 
		{
			CAlert::InformationAlert("FindCurrentListBox 3 nil");
			break;
		}
		this->addListElementWidget(lstboxControlView, newElView, displayName, updateWidgetId, atIndex, x);
	}
	while (false);
}

/*
void SDKListBoxHelper::RemoveElementAt(int indexRemove, int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) 
		{
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveElementAt() Found listbox but not control data?");
		if(listControlData==nil) 
		{
			break;
		}
		if(indexRemove < 0 || indexRemove >= listControlData->Length()) 
		{
			break;
		}
		listControlData->Remove(indexRemove, x);
		removeCellWidget(listBox, indexRemove);
	}
	while (false);
}

void SDKListBoxHelper::RemoveLastElement(int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) 
		{
			break;
		}
		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveLastElement() Found listbox but not control data?");
		if(listControlData==nil) 
		{
			break;
		}
		int lastIndex = listControlData->Length()-1;
		if(lastIndex > 0) 
		{		
			listControlData->Remove(lastIndex, x);
			removeCellWidget(listBox, lastIndex);
		}
	}
	while (false);
}

int SDKListBoxHelper::GetElementCount(int x) 
{
	int retval=0;
	do {
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) {
			break;
		}

		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::GetElementCount() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		retval = listControlData->Length();
	} while(0);
	
	return retval;
}

void SDKListBoxHelper::removeCellWidget(IControlView * listBox, int removeIndex) {
	
	do {
		if(listBox==nil) break;

		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) 
		{
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) 
		{
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"SDKListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) 
		{
			break;
		}

		if(removeIndex < 0 || removeIndex >= cellPanelData->Length()) 
		{
			break;
		}
		cellPanelData->RemoveWidget(removeIndex);
	} while(0);
}
*/
/* 
Here passing IControlView* of desired listbox returned from FindCurrentListBox()
*/
void SDKListBoxHelper::addListElementWidget
(IControlView* lstboxControlView, InterfacePtr<IControlView> & elView, const PMString & displayName, WidgetID updateWidgetId, int atIndex, int x)
{
//	IControlView * listbox = this->FindCurrentListBox(panel, x);
	if(elView == nil || lstboxControlView == nil ) 
	{
		CAlert::InformationAlert("1");
		return;
	}

	do {
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if (newElPanelData == nil) 
		{
			CAlert::InformationAlert("2");
			break;
		}
		IControlView* nameTextView = newElPanelData->FindWidget(updateWidgetId);
		if ( (nameTextView == nil)  ) 
		{
			CAlert::InformationAlert("2.1");
			break;
		}

		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if (newEltext == nil) 
		{
			CAlert::InformationAlert("4");
			break;
		}	
		newEltext->SetString(displayName, kTrue, kTrue);
		
		InterfacePtr<IPanelControlData> panelData(lstboxControlView,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::addListElementWidget() Cannot get panelData");
		if(panelData == nil) 
		{
			CAlert::InformationAlert("5");
			break;
		}

		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::addListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) 
		{
			CAlert::InformationAlert("6");
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "SDKListBoxHelper::addListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) 
		{
			CAlert::InformationAlert("7");
			break;
		}
		cellPanelData->AddWidget(elView);				

		InterfacePtr< IListControlDataOf<IControlView*> > listData(lstboxControlView, UseDefaultIID());		
		if(listData == nil) { 
			CAlert::InformationAlert("listData nil");
			break;
		}
		listData->Add(elView);	
	} while(0);
}

void SDKListBoxHelper::CheckUncheckRow
(IControlView *listboxCntrlView, int32 index, bool checked)
{
	do
	{
/*		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil)
		{
			CAlert::InformationAlert("FindCurrentListBox nil");
			break;
		}
*/
		InterfacePtr<IListBoxController> listCntl(listboxCntrlView,IID_ILISTBOXCONTROLLER);		
		if(listCntl == nil) 
		{
			CAlert::InformationAlert("listCntl nil");
			break;
		}		
		
		InterfacePtr<IPanelControlData> panelData(listboxCntrlView, UseDefaultIID());
		if (panelData == nil) 
		{
			CAlert::InformationAlert("listBox IPanelControlData nil");
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
		if(cellControlView == nil)
		{
			CAlert::InformationAlert("FindWidget nil");
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
		if(cellPanelData == nil)
		{
			CAlert::InformationAlert("cellControlView IPanelControlData nil");
			break;
		}	
		
		IControlView* nameTextView = cellPanelData->GetWidget(index);
		if ( nameTextView == nil ) 
		{
			CAlert::InformationAlert("FindWidget kLNGTextWidget1ID nil");
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
		if(cellPanelDataChild == nil)
		{
			CAlert::InformationAlert("cellControlViewChild IPanelControlData nil");
			break;
		}

		int toHidden=0;
		int toShow=0;
		
		if(checked)
		{
			toHidden=0;
			toShow=1;
		}
		else
		{
			toHidden=1;
			toShow=0;
		}
		
		IControlView *childHideView=cellPanelDataChild->GetWidget(toHidden);
		if(childHideView==nil)
		{
			CAlert::InformationAlert("6");
			break;
		}

		IControlView *childShowView=cellPanelDataChild->GetWidget(toShow);
		if(childShowView==nil)
		{
			CAlert::InformationAlert("7");
			break;
		}

		childHideView->Hide();
		childShowView->Show();	
		
	}while(kFalse);
}

void SDKListBoxHelper::EmptyCurrentListBox(InterfacePtr<IPanelControlData> panel, int x)
{
	do {
		IControlView* listBoxControlView = this->FindCurrentListBox(panel, x);
		if(listBoxControlView == nil) 
		{
			CA("listBoxControlView is nil");
			break;
		}
		InterfacePtr<IListControlData> listData (listBoxControlView, UseDefaultIID());
		if(listData == nil) 
		{
			CA("listData  is nil");
			break;
		}
		InterfacePtr<IPanelControlData> iPanelControlData(listBoxControlView, UseDefaultIID());
		if(iPanelControlData == nil) 
		{
			CA("iPanelControlData is nil");
			break;
		}
		IControlView* panelControlView = iPanelControlData->FindWidget(kCellPanelWidgetID);
		if(panelControlView == nil) 
		{
			CA("panelControlView is nil");
			break;
		}
		InterfacePtr<IPanelControlData> panelData(panelControlView, UseDefaultIID());
		if(panelData == nil) 
		{
			CA("panelControlView is nil");
			break;
		}
		listData->Clear(kFalse, kFalse);
		panelData->ReleaseAll();
		listBoxControlView->Invalidate();
	} while(0);
}

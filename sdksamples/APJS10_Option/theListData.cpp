#include "VCPluginHeaders.h"
#include "theListData.h"
#include "string.h"

vector<listInfo> listData::selectedRows;
int listData::currentSelected=0;
bool listData::didUserDragBox=kFalse;
UID listData::lastSelectedUID;

bool listData::getData(int index, char *text)
{
	listInfo li;
	if(selectedRows.size()<=index)
		return kFalse;
	li=this->selectedRows[index];
	if(text)
		std::strcpy(text, li.text);
	return li.isSelected;
}

void listData::setData(bool state, char *text)
{
	listInfo li;
	if(text)
		std::strcpy(li.text, text);
	li.isSelected=state;
	selectedRows.push_back(li);
}

void listData::setData(bool state, char *text, int where)
{
	listInfo li;
	if(selectedRows.size()<=where)
		return;
	if(text)
		std::strcpy(li.text, text);
	li.isSelected=state;
	selectedRows[where]=li;
}


#include "VCPluginHeaders.h"

// Interface includes
#include "IUIDData.h"
#include "IDocument.h"
#include "ISubject.h"

// API includes
#include "ErrorUtils.h"
#include "PageItemUtils.h"	 
#include "Command.h"

// Project includes
#include "OptnsListUID.h"
#include "IOptnsListData.h"
#include "IOptnsListUID.h"

/** 
	PstLstNewDataCmd will create a new IPstLstData object and add its UID to the IPstLstUIDList 
	of all selected page items.  Note the page items to be added the new data object is passed
	in the fItemList; a data member fNewObjectList is used to store the newly created IPstLstData 
	objects' UIDs.  Also this class stores the old selected IPstLstData's UIDs of all selected 
	items in an array called fOldSelectedUID so its Undo can reset the page item's UID list to 
	its old state. 

	PstLstNewDataCmd implements ICommand based on the partial implementation Command. 

	@author Lee Huang
*/
class PstLstNewDataCmd : public Command
{
	public:
		/**
			Constructor, we initialize all the private members and pass the boss pointer to 
			the parent class (Command).
		*/
		PstLstNewDataCmd(IPMUnknown* boss);

		/**
			Destructor
		*/
		virtual ~PstLstNewDataCmd();

		/**
			Don't execute this command if the memory is low.
		*/
		bool16 LowMemIsOK() const { return kFalse; }

	protected:
		/**
			The Do method is where we create a new IPstLstData in the page item's database, and add
			its UID to the IPstLstUIDList of the page item.  Since there may be multiple items selected,
			we have to repeat this process thru all the page items passed in the fItemList.
		*/
		void Do();

		/**
			The Undo method deletes the object created in the Do() from the database, also removes the
			UID from the IPstLstUIDList and reset its selected UID.
		*/
		void Undo();

		/**
			Change the page item to be what the command changed it to in the Do method.
		*/
		void Redo();

		/**
			Notify all the document observers that a change to the page item's IPstLstData
			has happened.
		*/
		void DoNotify();

		/**
			Gives the name that shows up under the Edit menu and allows the user to undo or redo the command.

			@return pointer to a PMString allocated using the new operator. Deleted by caller.
		*/
		PMString* CreateName();

	private:
		UID* fOldSelectedUID;
		UIDList fNewObjectList;
		PMString fPstLstData;
		int32 eleid;
		int32 typid;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(PstLstNewDataCmd, kPstLstNewDataCmdImpl)

/* PstLstNewDataCmd Constructor
*/
PstLstNewDataCmd::PstLstNewDataCmd(IPMUnknown* boss) :
	Command(boss), 
	fOldSelectedUID(nil)
{
}

/* PstLstNewDataCmd Destructor
*/
PstLstNewDataCmd::~PstLstNewDataCmd()
{
	delete [] fOldSelectedUID;
}

/*	PstLstNewDataCmd::Do
*/
void PstLstNewDataCmd::Do()
{
	ErrorCode err = kSuccess;

	do
	{
		// Acquire command argument
		InterfacePtr<IPstLstData> iNewPstLstCmdData(this, UseDefaultIID());
		if (iNewPstLstCmdData == nil)
		{
			err = kFailure;
			ASSERT_FAIL("IPstLstData ptr nil, failed in Do");
			break;
		}
			
		fPstLstData = iNewPstLstCmdData->GetName();
		iNewPstLstCmdData->GetOtherInfo(&eleid, &typid);
		
		// Initialize an UIDList pointing at the workspace database, this list is to
		// hold the data object's UID that will be created in the command.
		UIDList itemList(fItemList.GetRef(0).GetDataBase());
		fNewObjectList = itemList;

		// Find out how many page items this command is going to act upon, we will create one new data object
		// for each page item.  Since the newly created object will become the new selected UID for that item,
		// we have to remember the original selected UID for undo purpose, we will use an array of UID 
		// fOldSelectedUID to hold all the original selected UID.
		int32	items = fItemList.Length();
		if (fOldSelectedUID != nil)
			delete [] fOldSelectedUID;		
		fOldSelectedUID = new UID[items];
		
		// We have to loop thru all selected page items to add an data object to it.
		for (int32 i = 0; i < items; i++)
		{
			// 1. Allocate a UID for the new data object (kPstLstDataBoss)
			if (err == kSuccess)
			{
				// need to create the new object in the database where the page item belongs
				IDataBase* iDatabase = fNewObjectList.GetDataBase();
				ASSERT_MSG(fNewObjectList.GetDataBase() != nil, "PstLstNewDataCmd::Do() fNewObjectList database not set");
				if (iDatabase != nil)
					fNewObjectList.Append(iDatabase->NewUID(kPstLstDataBoss));
				else
					err = kFailure;
			}

			// 2. Set the data (obtained thru command data) to the data object
			if (err == kSuccess)
			{
				// note the insert always append the new element in the last position
				InterfacePtr<IPstLstData>	iPstLstData(fNewObjectList.GetRef(fNewObjectList.Length()-1), UseDefaultIID());
				if (iPstLstData != nil)
				{

					iPstLstData->SetName(fPstLstData);
					iPstLstData->GetOtherInfo(&eleid, &typid);
				}
				else
					err = kFailure;
			}
			
			// 3. Update page item's object list (IPstLstUIDList) with the new object UID
			if (err == kSuccess)
			{
				InterfacePtr<IPstLstUIDList> iPstLstList(fItemList.GetRef(i), UseDefaultIID());
				if (iPstLstList != nil)
				{
					// remember the original selected UID for undo
					fOldSelectedUID[i] = iPstLstList->GetSelectedUID();
					// add the UID to page item's IPstLstUIDList
					iPstLstList->Append(fNewObjectList[fNewObjectList.Length()-1]);
				}
				else
					err = kFailure;
			}
		}
	} while (kFalse);
	//Handle errors
	if (err != kSuccess)
	{
		ErrorUtils::PMSetGlobalErrorCode(err);
	}
}

/*	PstLstNewDataCmd::Redo
*/
void PstLstNewDataCmd::Redo()
{
	// We have to loop thru all selected page items to re-add the data object to it.
	// note that the data object had been created once in Do() we can just use
	// UndeleteUID to restore it in database, that's why we don't just call Do() in 
	// this routine.
	for (int32 i = 0; i < fItemList.Length(); i++)
	{
		InterfacePtr<IPstLstUIDList> iPstLstList(fItemList.GetRef(i), UseDefaultIID());
		if (iPstLstList != nil)
		{
			fNewObjectList.GetDataBase()->UndeleteUID(fNewObjectList[i], kPstLstDataBoss);
			iPstLstList->Append(fNewObjectList[i]);
		}
	}
}

/*	PstLstNewDataCmd::Undo
*/
void PstLstNewDataCmd::Undo()
{
	// We have to loop thru all selected page items to remove its just added UID and remove the data object from database.
	for (int32 i = 0; i < fItemList.Length(); i++)
	{
		InterfacePtr<IPstLstUIDList> iPstLstList(fItemList.GetRef(i), UseDefaultIID());
		if (iPstLstList != nil)
		{
			iPstLstList->Remove(fNewObjectList[i]);
			fNewObjectList.GetDataBase()->DeleteUID(fNewObjectList[i]);
			iPstLstList->SetSelectedUID(fOldSelectedUID[i]);	// restore the original selected UID
		}
	}
}

/*	PstLstNewDataCmd::DoNotify
*/
void PstLstNewDataCmd::DoNotify()
{
	// For notfication we'll let PageItemUtils::NotifyDocumentObservers do the work.
	// Since this is the changed on the IPstLstData, we use its interface ID and the 
	// this command boss'd id as parameter to go into the Change() 
	PageItemUtils::NotifyDocumentObservers
	(
		fItemList, 
		kPstLstNewDataCmdBoss, 
		IID_IPSTLSTDATA, 
		this
	);
}

/* PstLstModifyDataCmd CreateName
*/
PMString* PstLstNewDataCmd::CreateName()
{
	PMString* str = new PMString(kPstLstNewStringKey);
	return str;
}
	
// End, PstLstNewDataCmd.cpp.


#include "VCPlugInHeaders.h"
#include "IPMStream.h"
#include "CPMUnknown.h"
#include "IDatabaseHandler.h"
#include "OptnsDlgID.h"
#include "CAlert.h"

#define CA(Z) CAlert::InformationAlert(Z)

class IPMStream;

class OptnsDatabaseHandler : public CPMUnknown<IDatabaseHandler>
{
	public:
		OptnsDatabaseHandler(IPMUnknown*  boss);
		virtual ~OptnsDatabaseHandler();

};

CREATE_PERSIST_PMINTERFACE(OptnsDatabaseHandler, kOptnsDatabaseActionImpl)

OptnsDatabaseHandler::OptnsDatabaseHandler(IPMUnknown* boss) : 
	CPMUnknown<IDatabaseHandler>(boss), 
{
}

OptnsDatabaseHandler::~OptnsDatabaseHandler()
{
}




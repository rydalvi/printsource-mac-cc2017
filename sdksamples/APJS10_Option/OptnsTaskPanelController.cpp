#include "VCPlugInHeaders.h"
#include "CDialogController.h"
#include "SystemUtils.h"
#include "OptnsDlgID.h"
#include "CAlert.h"

#define CA(x)	CAlert::InformationAlert(x)

class OptnsTaskPanelController : public CDialogController
{
	public:
		OptnsTaskPanelController(IPMUnknown* boss) : CDialogController(boss) {}
		virtual ~OptnsTaskPanelController() {}
		virtual void InitializeFields();
		virtual WidgetID ValidateFields();
		virtual void ApplyFields(const WidgetID& widgetId);
};

CREATE_PMINTERFACE(OptnsTaskPanelController, kTaskPanelControllerImpl)

void OptnsTaskPanelController::InitializeFields() 
{
	CDialogController::InitializeFields();
	this->SetTriStateControlData(kAppendRadBtnWidgetID, kTrue);
	this->SetTriStateControlData(kEmbedRadBtnWidgetID, kFalse);
}

WidgetID OptnsTaskPanelController::ValidateFields() 
{
	WidgetID result = CDialogController::ValidateFields();
	return result;
}

void OptnsTaskPanelController::ApplyFields(const WidgetID& widgetId) 
{
}

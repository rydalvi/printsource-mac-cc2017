#include "VCPluginHeaders.h"
#include "string.h"

class ListInfo
{
	private:
		int listSize;
		int selectedRowIndex;
		bool didUserDragBox;
		enum  type {PRODCOPYELE, ELEBYTYPEID, COREATTRIB} lstboxType;
	public :
		void setListSize(int listSize) 
		{
			this->listSize = listSize;
		}
		void setSelectedRowIndex(int rowIndex) 
		{
			this->selectedRowIndex = rowIndex;
		}
		void setDidUserDragBox(bool isDragged)
		{
			if(isDragged==true)
				this->didUserDragBox = true;
			else
				this->didUserDragBox = false;
		}
		void setType(int typeVal)
		{
			switch(typeVal)
			{
			case 1:
				lstboxType = PRODCOPYELE;
				break;
			case 2:
				lstboxType = ELEBYTYPEID;
				break;
			case 3:
				lstboxType = COREATTRIB;
				break;
			}
		}

		int getListSize()
		{
			return this->listSize;
		}
		int getSelectedRowIndex()
		{
			return this->selectedRowIndex;
		}
		enum type getLstboxType(int listboxType)
		{
			switch(listboxType)
			{
				case 1:
					return PRODCOPYELE;
				case 2:
					return ELEBYTYPEID;
				case 3:
					return COREATTRIB;
			}
		}
};

#include "VCPlugInHeaders.h"
#include "K2Vector.tpp"
#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "OptnsDlgID.h"
#include "SystemUtils.h"
#include "SDKUtilities.h"
#include "SDKListBoxHelper.h"
#include "IAppFramework.h"
#include "IPanelControlData.h"
#include "ISelection.h"
#include "IApplication.h"
#include "k2smartptr.h"
#include "LayoutUtils.h"
#include "listData.h"
#include "CmdUtils.h"
#include "IToolCmdData.h"
#include "ITool.h"
#include "IToolManager.h"
#include "ITextModel.h"
#include "FrameUtils.h"
#include "ISpecifier.h"
#include "IFrameList.h"
#include "ITextFrame.h"
#include "IHierarchy.h"
#include "ILayoutTarget.h"
#include "ITransform.h"
#include "ITextFocusManager.h"
#include "IDocument.h"
#include "ISpreadList.h"
#include "IGeometry.h"
#include "TransformUtils.h"
#include "ISpread.h"
#include "IScrapItem.h"
#include "ITextControlData.h"
#include "IHierarchyCmdData.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "GenericID.h"
#include "CommonFunctions.h"
#include "ISelectionUtils.h"
#include "ISlugData.h"
#include "vector"
#include "SlugStructure.h"
#include "k2vector.h"
#include "OptnsDragEventFinder.h"

typedef vector<SlugStruct> SlugList;

#define NAME_SEP "</#M^D<"
#define CA(Z) CAlert::InformationAlert(Z)

ITool* queryTool(const ClassID& toolClass)	
{
	ITool *tool = nil;
	InterfacePtr<IApplication> app(gSession->QueryApplication());
	ASSERT(app);
	InterfacePtr<IToolManager> toolMgr(app->QueryToolManager());
	ASSERT(toolMgr);
	if(toolMgr)
	{
		for(int32 i = 0; i < toolMgr->GetNumTools(); i++)
		{
			ToolRecord rec = toolMgr->GetNthTool(i);
			InterfacePtr<ITool> tool(rec.QueryTool());
			ASSERT(tool);
			if(::GetClass(tool) == toolClass)
			{
				return rec.QueryTool();
			}
		}
		for(int32 j = 0; j < toolMgr->GetNumSubTools(); j++)
		{
			ToolRecord rec = toolMgr->GetNthSubTool(j);
			InterfacePtr<ITool> tool(rec.QueryTool());
			ASSERT(tool);
			if(::GetClass(tool) == toolClass)
			{
				return rec.QueryTool();
			}
		}
	}				
	return tool;
}

int changeMode(int whichMode)
{
	int status=kFailure;
	if(whichMode==DRAGMODE)
	{
		InterfacePtr<ICommand> setToolCmd(CmdUtils::CreateCommand(kSetToolCmdBoss));
		if(!setToolCmd)
			return kFailure;

		InterfacePtr<IToolCmdData> setToolCmdData(setToolCmd, UseDefaultIID());
		if(!setToolCmdData)
			return kFailure;

		InterfacePtr<ITool> textTool(queryTool(kPointerToolBoss));
		if(!textTool)
			return kFailure;

		setToolCmdData->Set(textTool, textTool->GetToolType());
		status=CmdUtils::ProcessCommand(setToolCmd);
	}
	return 1;
}

int deleteThisBox(UIDRef boxUIDRef)
{
	InterfacePtr<IScrapItem> scrap(boxUIDRef, UseDefaultIID());
	if(scrap==nil)
		return 0;
	
	InterfacePtr<ICommand> command (scrap->GetDeleteCmd());
	command->SetItemList(UIDList(boxUIDRef));
	if(CmdUtils::ProcessCommand(command)!=kSuccess)
		return 0;
	return 1;
}

int convertBoxToTextBox(UIDRef boxUIDRef)
{
	if(OptnsDragEventFinder::isImgflag==kFalse)
	{
		InterfacePtr<ICommand> command ( CmdUtils::CreateCommand(kConvertItemToTextCmdBoss));
		command->SetItemList(UIDList(boxUIDRef));
		if(CmdUtils::ProcessCommand(command)!=kSuccess)
			return 0;
	}
	if(OptnsDragEventFinder::isImgflag==kTrue)
		return 0;
	return 1;
}

int appendTextIntoBox(UIDRef boxUIDRef, PMString textToInsert, bool16 prompt)
{
	UID textFrameUID;
	
	InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
	textFrameUID = FrameUtils::GetTextFrameUID(unknown);
	
	if(textFrameUID == kInvalidUID)
	{
		PMString message("The box you are trying to append data is not a text box. Would you like to convert it to a text box? The data in the box may be lost.");
		PMString okBtn("OK");
		PMString cancelBtn("Cancel");
		int answer=1;
		
		if(prompt)
			answer=CAlert::ModalAlert(message, okBtn, cancelBtn, "", 2, CAlert::eQuestionIcon);
		
		if(answer==1)
		{
			convertBoxToTextBox(boxUIDRef);
			InterfacePtr<IPMUnknown> unknown(boxUIDRef, IID_IUNKNOWN);
			textFrameUID = FrameUtils::GetTextFrameUID(unknown);
			if(textFrameUID == kInvalidUID)
			{
				return 0;
			}
		}
		else
			return -1;//special return value
	}

	//We can safely add the string here

	
	InterfacePtr<ITextFrame> textFrame(boxUIDRef.GetDataBase(), textFrameUID, ITextFrame::kDefaultIID);
	if (textFrame == nil)
		return 0;
	
	TextIndex startIndex = textFrame->TextStart();
	TextIndex finishIndex = startIndex + textFrame->TextSpan()-1;

	InterfacePtr<ITextModel> textModel(textFrame->QueryTextModel());
	if (textModel == nil)
		return 0;

	InterfacePtr<ITextFocusManager> textFocusManager(textModel, UseDefaultIID());
	if (textFocusManager == nil)
		return 0;

	InterfacePtr<ITextFocus> frameTextFocus(textFocusManager->NewFocus(RangeData(startIndex, finishIndex, RangeData::kLeanForward)));
	if (frameTextFocus == nil)
		return 0;

	if(finishIndex>0)
	{
		PMString tempStr("\n");
		tempStr.Append(textToInsert);
		textToInsert=tempStr;
	}

	if(finishIndex<0) finishIndex=0;

	WideString* myText=new WideString(textToInsert);

	InterfacePtr<ICommand> pInsertTextCommand(textModel->InsertCmd(finishIndex, myText, kFalse));
	if (pInsertTextCommand == nil )
		return 0;

	if (CmdUtils::ProcessCommand(pInsertTextCommand)!=kSuccess )
		return 0;
	return 1;
}


int embedBoxIntoBox(UIDRef toEmbed, UIDRef bigBox)
{
//	int flag=1;
//	PMRect tmpRect;

//	InterfacePtr<IGeometry> iGeometry(toEmbed, UseDefaultIID());
//	if(iGeometry==nil)
//		flag=0;
///	else
//		tmpRect = iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry))); 


	InterfacePtr<IPMUnknown> unknown(bigBox, IID_IUNKNOWN);
	UID textFrameUID = FrameUtils::GetTextFrameUID(unknown);
	
	if(textFrameUID == kInvalidUID)
		convertBoxToTextBox(bigBox);
	
	InterfacePtr<IHierarchy> iChild(toEmbed, IID_IHIERARCHY);
	if(iChild==nil)
		return 0;

	/////////////[Removing our page item from old hierarchy]////////////

	UIDList itemList(toEmbed);

	InterfacePtr<ICommand> iRemoveCmd(CmdUtils::CreateCommand(kRemoveFromHierarchyCmdBoss));
	if(!iRemoveCmd)
		return 0;

	iRemoveCmd->SetItemList(itemList.GetRef(0));
	int32 retval1=CmdUtils::ProcessCommand(iRemoveCmd);
	if(retval1!=kSuccess)
		return 0;

	/////////////[Adding our page item to new hierarchy]////////////
	
	InterfacePtr<ICommand> iMoveCmd(CmdUtils::CreateCommand(kAddToHierarchyCmdBoss));
	if(!iMoveCmd)
		return 0;

	InterfacePtr<IHierarchyCmdData> iHierarchyCmdData(iMoveCmd, IID_IHIERARCHYCMDDATA);
	if(!iHierarchyCmdData)
		return 0;

	iHierarchyCmdData->SetParent(bigBox);

	K2Vector<int32> children;
	children.push_back(0);
	iHierarchyCmdData->SetIndexInParent(children); 
	
	iMoveCmd->SetItemList(UIDList(itemList.GetRef(0)));
	if(CmdUtils::ProcessCommand(iMoveCmd)!=kSuccess)
		return 0;

//	if(flag)
//		iGeometry->SetStrokeBoundingBox(tmpRect, IGeometry::kResizeItemOnly);

	return 1;
}



int getSelectionFromLayout(UIDRef& selectedBox)
{
	InterfacePtr<ISelection> selection(::QuerySelection());
	if (selection == nil)
		return 0;
	
	scoped_ptr<UIDList> selectUIDList(selection->CreateUIDList());
	if (selectUIDList == nil)
		return 0;

	const int32 listLength=selectUIDList->Length();
	
	if(listLength==0)
		return 0;

	selectedBox=selectUIDList->GetRef(0);
	return 1;
}


int applySlugToBox(UIDRef boxUIDRef, int selIndex, int selTab)
{
	CA("in apply slug()");
	InterfacePtr<ISlugData> temp1Data(boxUIDRef, UseDefaultIID());
	if(temp1Data==nil)
		return 0;

	SlugList tempList;
	PMString name;
	PMString colName;
	temp1Data->GetList(tempList, name, colName);

//	CA(colName);

	InterfacePtr<ICommand> labelCommand(CmdUtils::CreateCommand(kSlugCmdBoss));
	if(!labelCommand)
		return 0;
	
	UIDList selectUIDList(boxUIDRef);

	labelCommand->SetItemList(selectUIDList);
	
	InterfacePtr<ISlugData> tempData(labelCommand, UseDefaultIID());
	if(tempData==nil)
		return 0;
	
	
	SlugStruct sT;
	listData data;
	listInfo li;
	
	li=data.getData(selTab, selIndex);
	if(selIndex==0)
	{
		sT.elementId=-1;
		sT.typeId=-1;
		sT.whichTab = selTab;
	}
	else
	{
		sT.elementId=li.id;
		sT.typeId=li.typeId;
		sT.whichTab = selTab;
		sT.reserved1=(int32)OptnsDragEventFinder::isImgflag;
	}

	tempList.push_back(sT);
	if(name!="")
		name.Append(NAME_SEP);
	if(colName!="")
		colName.Append(NAME_SEP);
	if(li.tableCol=="")
		colName.Append("NONE");
	else
		colName.Append(li.tableCol);
	name.Append(li.name);
	tempData->SetList(tempList, name, colName);
	CmdUtils::ProcessCommand(labelCommand);
	return 1;
}

void insertOrAppend(int selectedRowIndex, PMString theContent, int choice, int whichTab)
{
	bool16 successFlag=kTrue;
	UIDRef overlapBoxUIDRef;
	UIDList newList;
	UIDRef selBoxUIDRef;
	//ICommandSequence* seq = nil;
	do
	{		
		PMString tempStr("[");
		tempStr.Append(theContent);
		tempStr.Append("]");
		theContent.Clear();
		theContent.Append(tempStr);
			
		if(!getSelectionFromLayout(selBoxUIDRef))
		{
			successFlag=kFalse;
			break;
		}

		//Start compound undo
		//seq=CmdUtils::BeginCommandSequence();

		if(OptnsDragEventFinder::isImgflag!=kTrue)
		{
			convertBoxToTextBox(selBoxUIDRef);
			appendTextIntoBox(selBoxUIDRef, theContent, kTrue);
		}
		
		if(!checkForOverLaps(selBoxUIDRef, 0, overlapBoxUIDRef, newList))
		{
			applySlugToBox(selBoxUIDRef, selectedRowIndex, whichTab);
			break;
		}

		int result;

		switch(choice)
		{
		case 1:
			result=appendTextIntoBox(overlapBoxUIDRef, theContent, kTrue);
			if(result==0)
				successFlag=kFalse;
			else if(result!=-1)
				deleteThisBox(selBoxUIDRef);
			break;
		case 2:
			if(!embedBoxIntoBox(selBoxUIDRef, overlapBoxUIDRef))
				successFlag=kFalse;
			break;
		}

		//End compound undo
		//CmdUtils::EndCommandSequence(seq);

	}while(kFalse);

	UIDRef curBox;
	if(successFlag)
	{		
		if(choice == 1)
			curBox = overlapBoxUIDRef;
		else curBox =  selBoxUIDRef;

		applySlugToBox(curBox, selectedRowIndex, whichTab);
	}
	else
	{
		CAlert::ErrorAlert("Some unknown error occured");	
	}
	refreshLstbox(kInvalidUID);
}


int getBoxDimensions(UIDRef theBox, PMRect& bind)
{
	PMRect theArea;
	
	InterfacePtr<IGeometry> iGeometry(theBox, UseDefaultIID());
	if(iGeometry==nil)
		return kFalse;

	theArea=iGeometry->GetStrokeBoundingBox((InnerToPasteboardMatrix(iGeometry)));
	bind=theArea;

	return kTrue;
}


int checkForOverLaps(UIDRef draggedItem, int32 spreadNumber, UIDRef& overlappingItem, UIDList& theList)
{
	IDocument* fntDoc = ::GetFrontDocument();
	if(fntDoc==nil)
		return -1;

	InterfacePtr<ILayoutControlData> layout(::QueryFrontLayoutData());
	if (layout == nil)
		return -1;

	IDataBase* database = ::GetDataBase(fntDoc);
	if(database==nil)
		return -1;

	InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
	if (iSpreadList==nil)
		return -1;
	
	UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(spreadNumber));

	InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());

	UIDList allPageItems(database);

	int numPages=spread->GetNumPages();

	for(int i=0; i<numPages; i++)
	{
		UIDList tempList(database);
		spread->GetItemsOnPage(i, &tempList, kFalse);
		allPageItems.Append(tempList);
	}

	PMRect theArea, dragItemArea;
	PMRect smallestItem(0,0,0,0);


	if(!getBoxDimensions(draggedItem, dragItemArea))
		return 0;

	PMReal centerX, centerY;
	centerX=(dragItemArea.Bottom()+dragItemArea.Top())/2;
	centerY=(dragItemArea.Right()+dragItemArea.Left())/2;


	bool16 collisionFlag=kFalse;

	for(i=0; i<allPageItems.Length(); i++)
	{
		if(allPageItems.GetRef(i)==draggedItem)
			continue;
		if(getBoxDimensions(allPageItems.GetRef(i), theArea)<1)
			continue;

	
		//This box may have some kids
		InterfacePtr<IHierarchy> iChild(allPageItems.GetRef(i), IID_IHIERARCHY);
		if(iChild)
		{
			UID kidUID;
			int numKids=iChild->GetChildCount();
			
			for(int j=0; j<numKids; j++)
			{
				kidUID=iChild->GetChildUID(j);
				allPageItems.Append(kidUID);
			}
		}
		//Done
		
		if(centerX>=theArea.Top() && centerY>=theArea.Left() && centerX<=theArea.Bottom() && centerY<=theArea.Right())
		{
			if(!collisionFlag)
			{
				collisionFlag=kTrue;
				smallestItem=theArea;
				overlappingItem=allPageItems.GetRef(i);
				theList=allPageItems;
				continue;
			}
			if((theArea.Bottom()-theArea.Top()) < (smallestItem.Bottom()-smallestItem.Top()) &&
				(theArea.Right()-theArea.Left()) < (smallestItem.Right()-smallestItem.Left()))
			{
				overlappingItem=allPageItems.GetRef(i);
				theList=allPageItems;
				smallestItem=theArea;
			}
		}
	}
	if(collisionFlag)
		return 1;
	return 0;
}

int16 refreshLstbox(UID boxToSkip)
{
/* Resetting isDragged flag for all the listboxes entries */
	for(int a=1; a<=4; a++)
	{
		listData lstdta;
		int32 lstSize=0;
		lstSize = lstdta.returnListVectorSize(a);
		for(int w=0; w<lstSize ; w++)
		{
			lstdta.resetDraggedBox(a, w, kFalse);
		}
	}
/* Getting all the page items on the current document */
	do
	{
		IDocument* fntDoc = ::GetFrontDocument();
		if(fntDoc==nil)
			break;

		InterfacePtr<ILayoutControlData> layout(::QueryFrontLayoutData());
		if (layout == nil)
			break;

		IDataBase* database = ::GetDataBase(fntDoc);
		if(database==nil)
			break;

		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
		if (iSpreadList==nil)
			break;

	/* storing all the page items into allPageItems */
		UIDList allPageItems(database);

	/* Iterating thru all the spreads of the current document */
		for(int x=0; x<iSpreadList->GetSpreadCount(); x++)
		{
			UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(x));

			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());

	/* iterating thru all the pages of the spread under consideration */
			int numPages=spread->GetNumPages();

			for(int m=0; m<numPages; m++)
			{
				UIDList tempList(database);
				spread->GetItemsOnPage(m, &tempList, kFalse);
				allPageItems.Append(tempList);
			}
	/* Checking for the child boxes also */
			for(int i=0; i<allPageItems.Length(); i++)
			{
				InterfacePtr<IHierarchy> iChild(allPageItems.GetRef(i), IID_IHIERARCHY);
				if(iChild)
				{
					UID kidUID;
					int numKids=iChild->GetChildCount();
					for(int j=0; j<numKids-1; j++)
					{
						kidUID=iChild->GetChildUID(j);
						allPageItems.Append(kidUID);
					}
				}
			}
		}
	/* Setting isDragged to kTrue */
		for(int j=0; j<allPageItems.Length(); j++)
		{
			if( boxToSkip!=kInvalidUID &&
				boxToSkip==allPageItems.GetRef(j).GetUID())
				continue;
			InterfacePtr<ISlugData> temp1Data(allPageItems.GetRef(j), UseDefaultIID());
			if(temp1Data==nil)
			{
				CA("here continue");
				continue;
			}

			SlugList tempList;
			PMString name;
			PMString colName;
			temp1Data->GetList(tempList, name, colName);

			for(int i=0; i < tempList.size(); i++)
			{
				listData lstdta;
				int32 lstSize=0;
				lstSize = lstdta.returnListVectorSize(tempList[i].whichTab);
				PMString vectSize("Vect size ");
				vectSize.AppendNumber(lstSize);
				CA(vectSize);

				for(int y=0; y<lstSize ; y++)
				{
					listInfo info;
					info = lstdta.getData(tempList[i].whichTab, y);

					/* Case: for image types */
					if(info.isImageFlag==kTrue)
					{
						CA("Image box");
					}
					else
					{
						/* Case: for normal text boxes */
						/* No need to check for 0th row as both param are same */
						if(tempList[i].elementId==info.id)
						{
							lstdta.resetDraggedBox(tempList[i].whichTab, y, kTrue);
							break;
						}
					}
				}
			}
		}
	}while(kFalse);

	if(OptnsDragEventFinder::lst1CntrlView==nil
	|| OptnsDragEventFinder::lst2CntrlView==nil
	|| OptnsDragEventFinder::lst3CntrlView==nil
	|| OptnsDragEventFinder::lst4CntrlView==nil)
	{
		return -1;
	}

	for(int i=1;i<=4;i++)
	{
		listData lstdta;
		int32 lstSize=0;
		lstSize = lstdta.returnListVectorSize(i);

		for(int y=0; y<lstSize ; y++)
		{
			listData lstdta;
			listInfo info;
			info = lstdta.getData(i, y);
			switch(i)
			{
			case 1:
				if(info.isDragged)
					updateIcon(OptnsDragEventFinder::lst1CntrlView, y, kTrue);
				else
					updateIcon(OptnsDragEventFinder::lst1CntrlView, y, kFalse);
				break;

			case 2:
				if(info.isDragged)
					updateIcon(OptnsDragEventFinder::lst2CntrlView, y, kTrue);
				else
					updateIcon(OptnsDragEventFinder::lst2CntrlView, y, kFalse);
				break;

			case 3:
				if(info.isDragged)
					updateIcon(OptnsDragEventFinder::lst3CntrlView, y, kTrue);
				else
					updateIcon(OptnsDragEventFinder::lst3CntrlView, y, kFalse);
				break;

			case 4:
				if(info.isDragged)
					updateIcon(OptnsDragEventFinder::lst4CntrlView, y, kTrue);
				else
					updateIcon(OptnsDragEventFinder::lst4CntrlView, y, kFalse);
				break;
			}
		}
	}
	return 0;
}

void updateIcon(IControlView* lstcntrlview, int32 index, bool16 checked)
{
	do
	{
		InterfacePtr<IPanelControlData> panelData(lstcntrlview, UseDefaultIID());
		if (panelData == nil) 
		{
			CAlert::InformationAlert("listBox IPanelControlData nil");
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
		if(cellControlView == nil)
		{
			CAlert::InformationAlert("FindWidget nil");
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
		if(cellPanelData == nil)
		{
			CAlert::InformationAlert("cellControlView IPanelControlData nil");
			break;
		}	
		
		IControlView* nameTextView = cellPanelData->GetWidget(index);
		if ( nameTextView == nil ) 
		{
			CAlert::InformationAlert("FindWidget kLNGTextWidget1ID nil");
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
		if(cellPanelDataChild == nil)
		{
			CAlert::InformationAlert("cellControlViewChild IPanelControlData nil");
			break;
		}

		int toHidden=0;
		int toShow=0;
		
		if(checked)
		{
			toHidden=0;
			toShow=1;
		}
		else
		{
			toHidden=1;
			toShow=0;
		}
		
		IControlView *childHideView=cellPanelDataChild->GetWidget(toHidden);
		if(childHideView==nil)
		{
			CAlert::InformationAlert("6");
			break;
		}

		IControlView *childShowView=cellPanelDataChild->GetWidget(toShow);
		if(childShowView==nil)
		{
			CAlert::InformationAlert("7");
			break;
		}

		childHideView->Hide();
		childShowView->Show();	
	}while(kFalse);
}
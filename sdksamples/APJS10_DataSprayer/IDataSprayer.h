#ifndef __IDataSprayer_h_
#define __IDataSprayer_h_

#include "VCPluginHeaders.h"
#include "DSFID.h"
#include "UIDList.h"
#include "vector"
#include "ITextModel.h"
#include "TagStruct.h"

//#include "LayoutUtils.h" //Cs3
#include "ILayoutUtils.h"  //Cs4
#include "IHierarchy.h"
#include "CAlert.h"
#include "ITagReader.h"
//#include "PFTreeDataCache.h"
#include "PublicationNode.h"
//#include "DragDropHelper.h"
#include "IClientOptions.h"
#include "CmdUtils.h"
#include "CTUnicodeTranslator.h"
#include "TextIterator.h"
//#include "ITextFrame.h"
#include "ITextModel.h"
#include "ITextFocusManager.h"
#include "ITextFocus.h"
#include "Command.h"
#include "IFrameUtils.h"

//#include "IImportFileCmdData.h"
#include "IOpenFileDialog.h"
#include "IImportProvider.h"
#include "IGraphicFrameData.h"
#include "IPlaceGun.h"
#include "IPlaceBehavior.h"
//#include "IImportFileCmdData.h"
#include "SDKUtilities.h"
#include "TransformUtils.h"
#include "IPathUtils.h"
#include "IGraphicAttributeUtils.h"
#include "ISwatchUtils.h"
#include "ISwatchList.h"

#include "IAppFramework.h"
//#include "ISpecialChar.h"
//#include "PSPMediatorClass.h"
//#include "ITagWriter.h"
#include "IReplaceCmdData.h"
#include "ITableModel.h"
#include "ITableTextSelection.h"

using namespace std;
typedef vector<UID> VectorNewImageFrameUIDList;
typedef vector<UIDRef> VectorNewImageFrameUIDRefList;
typedef map<UID, VectorNewImageFrameUIDRefList> MapNewImageFrameUIDList; //In this map key is originalFrameUID and value is vector of newly Created Frames UID

class DynFrameStruct
{
public:
	int HorzCnt;
	int VertCnt;
	bool16 isLastHorzFrame;
	PMRect BoxBounds;
};
typedef vector<DynFrameStruct> FrameBoundsList;

class Parameter
{
public:
	int idxHorizontalCount;
	int idxVerticalCount;
	FrameBoundsList ProdBlockBoundList;
	bool16 fFaltuCase;

	bool16 isLeadingItemOnSamePage;
	PMReal RightMarkAfterSprayForLeadingItem;
	PMReal BottomMarkAfterSprayForLeadingItem;
	PMRect PagemarginBoxBounds;
	PMRect ItemStencilMaxBounds;

	bool16 fNonSprayItemPerFramePresent;

	PMReal WidthOfSprayItemPerFrames;

	//bool16 addPageSplCase;




	////for page add functionallity
	int32 PageCount;
	vector<UID> PageUIDList;
	bool16 fPageAdded;
	
};
typedef vector<Parameter> VecParameter;

class ReturnParameter
{
public:
	
	int32 noOfTimesColumnChange;

	int idxHorizontalCount;
	int idxVerticalCount;
	DynFrameStruct CurrentDynFrameStruct;

	VectorNewImageFrameUIDList NewImageFrameUIDList_WhenFrameMovesToNextColumn;
	/*FrameBoundsList ProdBlockBoundList;*/

	//for page add functionallity
	int32 PageCount;
	vector<UID> PageUIDList;
	bool16 fPageAdded;



};
typedef vector<ReturnParameter> VecReturnParameter;



class IDataSprayer: public IPMUnknown
{
public:

		enum { kDefaultIID = IID_IDataSprayer };

	//virtual void startSpraying(void)=0;
	virtual void showTagInfo(UIDRef)=0;
	//virtual bool16 doesExist(IIDXMLElement * ptr, UIDRef BoxRef)=0;
	virtual bool16 doesExist(IIDXMLElement*, UIDRef BoxUidref)=0;
	virtual void sprayForThisBox(UIDRef, TagList& )=0;
	virtual bool16 isFrameTagged(const UIDRef&)=0;
	virtual bool16 sprayForTaggedBox(const UIDRef&)=0;
	//virtual bool16 sprayForTaggedBox(const UIDRef& boxUIDRef, int32 OBjectID)=0;//added by rahul
	virtual void getAllIds(double)=0;

	UIDList selectUIDList;
	vector<double> idList;
	PMString imagePath;


	
//	void showTagInfo(UIDRef);
	virtual bool16 GetStoryFromBox(InterfacePtr<ITextModel>, int32, int32, PMString&)=0;
	virtual bool16 GetTextDimensions(char*, char*, int32*, int32*)=0;
	virtual void ReplicateText(UIDRef, int32, TagList)=0;
	virtual void fillDataInBox(const UIDRef&, TagStruct&, double, bool16 isTaggedFrame=kFalse,bool16 isFlag=kFalse)=0;
	virtual bool16 fillImageInBox(const UIDRef&, TagStruct& , double)=0;
	virtual ErrorCode CreatePicturebox(UIDRef&, UIDRef, PMRect, PMReal strokeWeight=0.0)=0;
    virtual bool16 ImportFileInFrame(const UIDRef& imageBox, const PMString& fromPath)=0;
	//virtual void forceRecompose(InterfacePtr<ITextFrame>)=0;
	virtual void forceRecompose(InterfacePtr<ITextFrameColumn>)=0;
	//virtual bool16 isTextFrameOverset(const InterfacePtr<ITextFrame>, int32&)=0;
	virtual bool16 isTextFrameOverset(const InterfacePtr<ITextFrameColumn>, int32&)=0;
	virtual bool16 removeAllText(const UIDRef& boxUIDRef)=0;
	virtual void fitImageInBox(const UIDRef& boxUIDRef)=0;
	virtual void insertNewTable(const InterfacePtr<ITextModel>& textModel, 
												int numRows, 
												int numCols,
												TextIndex index,
												int32 len, 
												PMReal rowHeight,
												PMReal colWidth) const=0;
	virtual void Test()=0;
	virtual void FillPnodeStruct(PublicationNode pubNode , double SectionID=0, double PublicationID=0,double SubSectionID=0)=0;
	virtual void SlugReader(const UIDRef&,int32,int32,double&,double&)=0;
	virtual void NewSlugReader(InterfacePtr<ITableModel> &tblModel,int32 rowIndex,int32 colIndex,double& Attr_id,double& Item_id,InterfacePtr<ITableTextSelection> &tblTxtSel)=0;
	virtual bool8 IsAttrInTextSlug(const UIDRef& tableUIDRef,double Attr_id)=0;
	virtual bool8 UpdateDataInTextSlug(const UIDRef& tableUIDRef,double Attr_id,PMString Table_col)=0;
	virtual ErrorCode ProcessSimpleCommand(const ClassID& commandClass, const UIDList& itemsIn, UIDList& itemsOut)=0;
	virtual void fillTaggedTableInBoxCS2(const UIDRef& curBox, TagStruct& slugInfo)=0;
	//following function is added by vijay for spraying of image inside table cell
	//on 5-1-2007
	virtual void sprayImageInTable(UIDRef& , TagStruct&) = 0;
	//following function is added by vijay on 9-1-2007 to fitInlineImageInBox
	virtual void fitInlineImageInBoxInsideTableCell(UIDRef& boxUIDRef) = 0;
	virtual void sprayTableInsideTableCell(UIDRef& boxUIDRef, TagStruct & tagInfo) =0;

	virtual void sprayItemImage(UIDRef& boxUIDRef, TagStruct & tagInfo, PublicationNode& pNode,double itemID) = 0;

	virtual void sprayXRefTableScreenPrintInTabbedTextForm(TagStruct & tagInfo)=0;

	virtual void sprayAccessoryTableScreenPrintInTabbedTextForm(TagStruct & tagInfo)=0;

	virtual bool16 isHorizontalFlow()=0;
	virtual void setFlow(bool16 flow)=0;
	
	virtual void ClearNewImageFrameList()=0;
	virtual VectorNewImageFrameUIDList getNewImageFrameList()=0;
	virtual bool16 fillBMSImageInBox(const UIDRef& boxUIDRef, TagStruct& slugInfo, double objectId) = 0;
	virtual void sprayItemImageInsideHybridTable(UIDRef& boxUIDRef, TagStruct & tagInfo, double itemID,int32 imageno = 1) = 0;
	virtual bool16 copySelectedItems(UIDRef &newItem , PMRect pageBounds) = 0;	

	virtual bool16 fillPVAndMPVImageInBox(const UIDRef& boxUIDRef, TagStruct& slugInfo, double parentId,bool16 isForTable=kFalse, bool16 isSprayText= kFalse) = 0;
	virtual void fitInlineImageInBoxInsideTableCellNEW(const UIDRef& boxUIDRef,InterfacePtr<ITextModel>& textModel,IIDXMLElement* cellXMLElementPtr) = 0;

	virtual bool16 resetLetterOrNumberkeySeqCount(int32 val )=0; //******

	virtual void ClearNewImageFrameUIDList()=0;
	virtual MapNewImageFrameUIDList getNewImageFrameUIDList()=0;

	virtual bool16 setDifferentParameters(VecParameter& vecParameter) = 0;
	virtual void ClearDifferentParameters()=0;

	virtual bool16 getIsColumnChange() = 0;
	virtual void resetIsColumnChange() = 0;

	virtual VecReturnParameter getReturnParameter() = 0;
	virtual void ClearReturnParameter() = 0;

	virtual void setItemSprayOverflowFlag(int32 oversetflag) = 0;
	virtual int32 getItemSprayOverflowFlag() = 0;

	virtual void clearZeroByteImageList() =0;
	virtual PMString getZeroByteImageList() =0;
	virtual bool16 fileExists(PMString& path, PMString& name)=0;

};




// Before using IDataSprayer functions Call all functions of Publicationnode.h file to fill the structure of pnode.. which in turn provides current status of PFTree.
// Way get the interface pointer for AP_DataSprayer 	
// add two header file 1. DSFID.h & 2. IDataSprayer.h
// InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));

#endif
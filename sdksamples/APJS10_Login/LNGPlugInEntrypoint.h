#ifndef __LNGPlugInEntrypoint_h__
#define __LNGPlugInEntrypoint_h__

#include "PlugIn.h"
#include "GetPlugin.h"
#include "ISession.h"
#include "LNGMediator.h"

class LNGPlugInEntrypoint : public PlugIn
{
public:
	virtual bool16 Load(ISession* theSession);
	virtual bool16 Unload();
};

extern "C"
{
		LNGMediator* getMediators(void);
}
#pragma export list getMediators
/*
extern "C" 
{
	LNGPlugInEntrypoint* GetLoginPlugin();
}

#pragma export list GetLoginPlugin
*/
#endif
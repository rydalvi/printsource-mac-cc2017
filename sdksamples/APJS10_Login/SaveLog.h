#ifndef _SaveLog_
#define _SaveLog_

// Interface includes:

// Forward declarations:
//class K2Vector;
class PMString;
//class InterfacePtr;

class ITextControlData;
class ICounterControlData;
class IPMStream;

/**
	Log that records messages in a multi line text widget on a panel.

	@ingroup snippetrunner
	
*/
class SaveLog
{
protected:
	/**
		Constructor is protected because this is a singleton class.
		@param logPanelWidgetID IN of panel containing the text widget that will display log.
		@param logWidgetID IN of text widget that will display messages.
		@param scrollBarWidgetID IN of log scroll bar.
	*/
	SaveLog(const WidgetID& LoginDialogWidgetID, const WidgetID& HTIMultilineTextExpanderWidgetID, const WidgetID& scrollBarWidgetID);

public:
	/**
		Returns the global instance of this singleton class.
		@return the global instance of this singleton class.
	*/
	static SaveLog* Instance();

	/**
		Destroys the single global instance of this singleton class.
	*/
	void DeleteInstance();

	/** Destructor
	*/
	virtual ~SaveLog();

	/**
		Clears the log of messages.
	*/
	void Clear();

	/**
		UpdateTextControl that displays the log.
	*/
	void UpdateTextControl();

	/**
		SaveLog
		@param stream IN log data to be saved into.
	*/
	void Save(InterfacePtr<IPMStream> stream);

	void updateLog();


private:
	void ScrollToTop();
	void ScrollToBottom();
	static SaveLog* fSaveLog;
	InterfacePtr<ITextControlData> fLogTextControlData;
	InterfacePtr<ICounterControlData> fScrollBarCounterData;
	PMString fLog;
};

#endif // _SaveLog_


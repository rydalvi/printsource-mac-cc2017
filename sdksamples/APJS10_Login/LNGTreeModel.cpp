#include "VCPlugInHeaders.h"
#include "IHierarchy.h"	
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
#ifdef	 DEBUG
#include "IPlugInList.h"
#include "IObjectModel.h"
#endif
#include "ILayoutUtils.h" //Cs4
#include "LNGID.h"
#include "LNGTreeModel.h"
#include "CAlert.h"
//#include "LNGMediatorClass.h"


#include "LNGDataNode.h"
//#include "IClientOptions.h"
#include "IAppFrameWork.h"
#include "LNGTreeDataCache.h"
//#include "ISpecialChar.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("LNGTreeModel.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

#define CA(X) CAlert::InformationAlert(X);

LNGTreeDataCache dc;

int32 LNGTreeModel::root=0;
PMString LNGTreeModel::publicationName;
int32 LNGTreeModel::classId=0;

int32 RowCountForTree = -1;
int32 displayEnvList = 0;// 0 = client list, 1 = env list  // GLOBAL VALRIABLE//

extern int32 SelectedRowNo;

extern VectorClientModelPtr vectorClientModelPtr;


LNGTreeModel::LNGTreeModel()
{
	//root=-1;
	//classId=11002;
}

LNGTreeModel::~LNGTreeModel() 
{
}


UID LNGTreeModel::GetRootUID() const
{	
	//CA("LNGTreeModel::GetRootUID");
	LNGDataNode node;
	if(dc.isExist(root, node))
	{
		//CA("found in cache");		
		return root;
	}

	//dc.clearMap();

	//LNGDataNode node;	

	if(displayEnvList == 0)
	{

		if(vectorClientModelPtr != NULL)
		{

			int32 noOfClient = static_cast<int32>(vectorClientModelPtr->size());
			node.setAll("100 Root",root,-2,0,noOfClient,"", "", "");
			dc.add(node);

			VectorClientModel::iterator itr;
		
			int32 seqNo = 0;
			for(itr = vectorClientModelPtr->begin(); itr != vectorClientModelPtr->end(); itr++)
			{
				//CA("inside for client name = " + itr->getName());
				LNGDataNode node1;

				PMString str("");
				str.AppendNumber(PMReal(itr->getClient_id()));
				str.Append("    ");
				str.Append(itr->getName());
				node1.setAll(str,itr->getClient_id(),root,seqNo,0,itr->getName(), "", "");
				dc.add(node1);

				seqNo++;
			}
		}
	} 
	else if(displayEnvList == 1) // for Enviornment name list
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			//CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return root;
		}
		VectorLoginInfoPtr vectorLoginList = ptrIAppFramework->getLoginInfoProperties();		

		if(vectorLoginList != NULL && vectorLoginList->size() > 0)
		{
			int32 noOfClient = static_cast<int32>(vectorLoginList->size());
			node.setAll("101 Root",root,-2,0,noOfClient,"", "", "");
			dc.add(node);

			VectorLoginInfoValue::iterator itr;
		
			int32 seqNo = 0;
			for(itr = vectorLoginList->begin(); itr != vectorLoginList->end(); itr++)
			{
				//CA("inside for client name = " + itr->getName());
				LNGDataNode node1;

				PMString str("");
				
				node1.setAll(itr->getEnvName(),seqNo +1,root,seqNo,0,"", itr->getEnvName(),  itr->getCmurl());
				dc.add(node1);

				seqNo++;
			}
		}
	}	
	return root;
}


int32 LNGTreeModel::GetRootCount() const
{
	//CA("LNGTreeModel::GetRootCount");
	int32 retval=0;
	LNGDataNode pNode;
	if(dc.isExist(root, pNode))
	{
//CAI(pNode.getChildCount());
		return pNode.getChildCount();
	}
	return 0;
}

int32 LNGTreeModel::GetChildCount(const UID& uid) const
{
	//CA("LNGTreeModel::GetChildCount");
	LNGDataNode pNode;

	if(dc.isExist(uid.Get(), pNode))
	{
		//CAI(pNode.getChildCount());
		return pNode.getChildCount();
	}
	return -1;
}

UID LNGTreeModel::GetParentUID(const UID& uid) const
{
	
	LNGDataNode pNode;

	if(dc.isExist(uid.Get(), pNode))
	{
		if(pNode.getParentId()==-2)
			return kInvalidUID;
		return pNode.getParentId();
	}
	return kInvalidUID;
}


int32 LNGTreeModel::GetChildIndexFor(const UID& parentUID, const UID& childUID) const
{
	LNGDataNode pNode;
	int32 retval=-1;
	
	if(dc.isExist(childUID.Get(), pNode))
		return pNode.getSequence();
	return -1;
}


UID LNGTreeModel::GetNthChildUID(const UID& parentUID, const int32& index) const
{
	LNGDataNode pNode;
	int32 retval=-1;

	if(dc.isExist(parentUID.Get(), index, pNode))
	{
	
		return pNode.getClientId();
	}
	return kInvalidUID;
}


UID LNGTreeModel::GetNthRootChild(const int32& index) const
{
	//CA("LNGTreeModel::GetNthRootChild");
	LNGDataNode pNode;
	if(dc.isExist(root, index, pNode))
	{
		//CAI(pNode.getClientId());
		return pNode.getClientId();
	}
	return kInvalidUID;
}

int32 LNGTreeModel::GetIndexForRootChild(const UID& uid) const
{
	LNGDataNode pNode;
	if(dc.isExist(uid.Get(), pNode))
		return pNode.getSequence();
	return -1;
}

PMString LNGTreeModel::ToString(const UID& uid, int32 *Rowno) const
{
	LNGDataNode pNode;
	PMString name("");

	if(dc.isExist(uid.Get(), pNode))
	{	
		*Rowno= pNode.getSequence();
		//*Rowno = pNode.getHitCount();
		/*PMString ASD("getHitCount in ToolString : ");
		ASD.AppendNumber(pNode.getHitCount());
		CA(ASD);*/
		return pNode.getName();
	}
	return name;
}


int32 LNGTreeModel::GetNodePathLengthFromRoot(const UID& uid) 
{
	LNGDataNode pNode;
	if(uid.Get()==root)
		return 0;
	if(dc.isExist(uid.Get(), pNode))
		return (1);
	return -1;
}

/*
//	File:	LNG3DialogController.cpp
//
//	Date:	17-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2003 Next Millenium Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
// none.

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"

// Project includes:
#include "LNGID.h"

#include "envData.h"
#include "lst.h"
#include "..\Source\Value\ServerInfoValue.h"
#include "CAlert.h"

/** LNG3DialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author raghu
*/
class LNG3DialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		LNG3DialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~LNG3DialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeFields();

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateFields to be called. When all widgets are valid, 
			ApplyFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateFields();

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyFields(const WidgetID& widgetId);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(LNG3DialogController, kLNG3DialogControllerImpl)

/* ApplyFields
*/
void LNG3DialogController::InitializeFields()
{
	CDialogController::InitializeFields();
	// Put code to initialize widget values here.
	if(EvnironmentData::getIsNewServer()== FALSE){ // for edit exizting server	

		int selectedRow = EvnironmentData::getSelectedserver();
		PMString selectedEnv;
		EvnironmentData::getSelectedserverEnvName(&selectedEnv);
		
		listBoxHelper listHelper(this, kLNGPluginID);
		CServerInfoValue selectedServerProp;
		listHelper.getSelectedEnvironmentProperties(selectedEnv,selectedServerProp);
		
		long port =0;
		port=selectedServerProp.getServerPort();
		PMString portStr("");
		portStr.AppendNumber(port);

		SetTextControlData(kLNG31TextEditWidgetID, selectedServerProp.getEnvName());
		SetTextControlData(kLNG32TextEditWidgetID, selectedServerProp.getServerName());
		SetTextControlData(kLNG33TextEditWidgetID, portStr);
		SetTextControlData(kLNG34TextEditWidgetID, selectedServerProp.getSchema());
		SetTextControlData(kLNG35TextEditWidgetID, selectedServerProp.getUserName());
		SetTextControlData(kLNG36TextEditWidgetID, selectedServerProp.getPassword());
	}
	else { //if(EvnironmentData::getIsNewServer()== TURE) // for create new server
		SetTextControlData(kLNG31TextEditWidgetID, "");
		SetTextControlData(kLNG32TextEditWidgetID, "");
		SetTextControlData(kLNG33TextEditWidgetID, "");
		SetTextControlData(kLNG34TextEditWidgetID, "");
		SetTextControlData(kLNG35TextEditWidgetID, "");
		SetTextControlData(kLNG36TextEditWidgetID, "");
	}
}

/* ValidateFields
*/
WidgetID LNG3DialogController::ValidateFields()
{
	WidgetID result = CDialogController::ValidateFields();
	// Put code to validate widget values here.
	PMString editFieldString;
	editFieldString=GetTextControlData(kLNG31TextEditWidgetID);	
	if(editFieldString=="")
		return kLNG31TextEditWidgetID;
	
	editFieldString="\0";
	editFieldString=GetTextControlData(kLNG32TextEditWidgetID);	
	if(editFieldString=="")
		return kLNG32TextEditWidgetID;

	editFieldString="\0";
	editFieldString=GetTextControlData(kLNG33TextEditWidgetID);	
	if(editFieldString=="")
		return kLNG33TextEditWidgetID;

	editFieldString="\0";
	editFieldString=GetTextControlData(kLNG34TextEditWidgetID);	
	if(editFieldString=="")
		return kLNG34TextEditWidgetID;

	editFieldString="\0";
	editFieldString=GetTextControlData(kLNG35TextEditWidgetID);	
	if(editFieldString=="")
		return kLNG35TextEditWidgetID;

	editFieldString="\0";
	editFieldString=GetTextControlData(kLNG36TextEditWidgetID);	
	if(editFieldString=="")
		return kLNG36TextEditWidgetID;

	return result;	
}

/* ApplyFields
*/
void LNG3DialogController::ApplyFields(const WidgetID& widgetId)
{
	// Replace with code that gathers widget values and applies them.
	PMString editFieldString("");
	CServerInfoValue serverProperties;
	
	editFieldString=GetTextControlData(kLNG31TextEditWidgetID);
	serverProperties.setEnvName(editFieldString);
	editFieldString="\0";

	editFieldString=GetTextControlData(kLNG32TextEditWidgetID);
	serverProperties.setServerName(editFieldString);
	editFieldString="\0";

	editFieldString=GetTextControlData(kLNG33TextEditWidgetID);
	serverProperties.setServerPort(editFieldString.GetAsNumber());
	editFieldString="\0";

	editFieldString=GetTextControlData(kLNG34TextEditWidgetID);
	serverProperties.setSchema(editFieldString);
	editFieldString="\0";

	editFieldString=GetTextControlData(kLNG35TextEditWidgetID);
	serverProperties.setUserName(editFieldString);
	editFieldString="\0";

	editFieldString=GetTextControlData(kLNG36TextEditWidgetID);
	serverProperties.setPassword(editFieldString);
	editFieldString="\0";		
	
}
//  Generated by Dolly build 17: template "Dialog".
// End, LNG3DialogController.cpp.



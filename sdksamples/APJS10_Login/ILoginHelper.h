#ifndef __LOGINHELPER_H__
#define __LOGINHELPER_H__

#include "IAppFramework.h"
#include "IPMUnknown.h"
#include "LNGID.h"

class ILoginHelper:public IPMUnknown
{
public:
		enum	{kDefaultIID = IID_ILOGINHELPER};
	    virtual VectorLoginInfoPtr getAvailableServers(void)=0;
		//virtual VectorVendorInfoPtr getVendors(void)=0;
		virtual bool16 getCurrentServerInfo(PMString&,LoginInfoValue&)=0;
		virtual bool16 addServerInfo(LoginInfoValue&)=0;
		virtual bool16 editServerInfo(LoginInfoValue& ,double clientID)=0;
		virtual bool16 deleteServer(PMString& envName)=0;
		virtual bool16 isValidServer(PMString &dbServerName)=0;
		virtual bool16 setCurrentServer(PMString &envName)=0;
		virtual bool16 loginUser(PMString &envName,PMString &password,PMString &clientName,VectorClientModelPtr vectorClientModelPtr, PMString & errorMsg)=0;
		
		virtual bool8 isAvailableServersFileExists(PMString &folderName)=0;		
		virtual bool8 isEnvironmentKeyWordExistsInFile(PMString &fPath)=0;
		virtual bool8 createAvailableServersFile(PMString &folderName)=0;		
		virtual bool8 getAppFolder(PMString &appFolder)=0;
		virtual void  appendFileName(PMString &folderName)=0;
		virtual bool16 getCurrentServerInfo(LoginInfoValue&)= 0;//--added on 17 feb ....
		virtual void setEditedServerInfo( LoginInfoValue ) = 0 ; //----added on 17 feb ....

		virtual void callfireResetPlugins() = 0;
		virtual ClientInfoValue getCurrentClientInfoValue(void)=0;
};


#endif
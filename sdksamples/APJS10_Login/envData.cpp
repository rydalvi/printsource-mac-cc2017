

#include "VCPlugInHeaders.h"
#include"PMString.h"
#include"envData.h"


PMString EvnironmentData::envName("");
CServerInfoValue EvnironmentData::selectedServerProperties;
int EvnironmentData::selectedServer=-1;
bool16 EvnironmentData::serverStatus=-1;
bool8 EvnironmentData::isNewServer=FALSE;

CServerInfoValue&  EvnironmentData::getServerProperties(void)
{	
	return EvnironmentData::selectedServerProperties;
}
void  EvnironmentData::setServerServerProperties(CServerInfoValue& value)
{
	//selectedServerProperties=value;
	selectedServerProperties.setEnvName(value.getEnvName());
	selectedServerProperties.setServerName(value.getServerName());
	selectedServerProperties.setServerPort(value.getServerPort());
	selectedServerProperties.setSchema(value.getSchema());
	selectedServerProperties.setUserName(value.getUserName());
	selectedServerProperties.setPassword(value.getPassword());
}

bool16  EvnironmentData::getServerStatus(void)
{
	return serverStatus;
}
void EvnironmentData::setServerStatus(bool16 status)
{
	serverStatus=status;
}

bool8  EvnironmentData::getIsNewServer(void)
{
	return isNewServer;
}
void EvnironmentData::setIsNewServer(bool8 isNew)
{
	isNewServer=isNew;
}

int  EvnironmentData::getSelectedserver(void)
{
	return selectedServer;
}
void EvnironmentData::setSelectedserver(int i)
{
	selectedServer=i;
}

void EvnironmentData::getSelectedserverEnvName(PMString* str)
{
	str->SetString(envName);
}
void EvnironmentData::setSelectedServerEnvName(PMString* str)
{
	envName.SetString(*str);
}
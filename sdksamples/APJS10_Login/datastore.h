#ifndef DATASTORE_H
#define DATASTORE_H

#include"AnsiBasedTypes.h"
#include"PMString.h"
#include"InterfacePtr.h"
#include"IPanelControlData.h"
#include"LoginInfoValue.h"

class EvnironmentData
{	
	static IPanelControlData* launcpadPanel;
	static PMString availableServerFilePath;
	static PMString availableServerFileName;
	static int selectedServer;
	static int selectedVendorRow;
	static PMString envName;
	static bool16 serverStatus;
	static bool8 isNewServer; // 1 for edit existing server, 2 for create new server
	static LoginInfoValue selectedServerProperties;

	static IPanelControlData* loginPanel;
	
public:
	static int SelctedConnMode;
	static LoginInfoValue&  getServerProperties(void);
	static void  setSelectedServerProperties(LoginInfoValue&);

	static bool8 getIsEditServerFlag(void);
	static void  setIsEditServerFlag(bool8 isNewServer);

	static bool16 getServerStatus(void);
	static void   setServerStatus(bool16 status);

	static int  getSelectedserver(void);
	static void setSelectedserver(int row);

	static void setPrevSelectedVendor(int);
	static int  getPrevSelectedVendor(void);

	static PMString getAvailableServerFileName(void);
	static PMString getAvailableServerFilePath(void);
	static void setAvailableServerFilePath(PMString& str);

	static PMString getSelectedServerEnvName();
	static void setSelectedServerEnvName(PMString& str);

	static IPanelControlData* getlauncpadPanel(void);
	static void setlauncpadPanel(IPanelControlData* paneldata);

	//------------added on 17 feb -----------
	static void setProject(PMString);
	static void setLanguage(PMString);

	static IPanelControlData* getLoginPanel(void);
	static void setLoginPanel(IPanelControlData* paneldata);
};

#endif
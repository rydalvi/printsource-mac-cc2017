#include "VCPlugInHeaders.h"

//#include "LoginDialogController.h"
#include "CDialogObserver.h"
#include "SystemUtils.h"
//#include "IAppFramework.h"
#include "CAlert.h"
#include "LNGID.h"
#include "ISubject.h"
//8-march
#include "IPanelControlData.h"
#include "IControlView.h"
#include "listBoxHelperFuncs.h"
#include "LoginHelper.h"
#include "ICounterControlData.h"
#include "ITextControlData.h"
#include "dataStore.h"
#include "time.h"
#include "ProgressBar.h"
#include "LNGActionComponent.h"

class LNGVersionUpdaterDialogObserver: public CDialogObserver
{
	public:
		LNGVersionUpdaterDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {  bp = boss;}
		virtual ~LNGVersionUpdaterDialogObserver() {}
		void AutoAttach();
		void AutoDetach();
		void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);

		private:
		IPMUnknown* bp;
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(LNGVersionUpdaterDialogObserver, kLNGVersionUpdaterDialogObserverImpl)


void LNGVersionUpdaterDialogObserver::AutoAttach()
{
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());		
		if (panelControlData == nil){
			ASSERT_FAIL("LNGDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}
		AttachToWidget(kOKButtonWidgetID,  IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kCancelButton_WidgetID,  IID_ITRISTATECONTROLDATA,panelControlData);
	}while(kFalse);
}

void LNGVersionUpdaterDialogObserver::AutoDetach()
{
	CDialogObserver::AutoDetach();	
	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			ASSERT_FAIL("LNGDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}
		// Detach from other widgets you handle dynamically here.		
		//this is deleted by alok
		DetachFromWidget(kOKButtonWidgetID,  IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kCancelButton_WidgetID,  IID_ITRISTATECONTROLDATA,panelControlData);
	}while(kFalse);
}

void LNGVersionUpdaterDialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID& protocol, 
	void* changedBy
)
{
	do{
		InterfacePtr<IControlView> controlView(theSubject,UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("LNGDialogObserver::Update() controlView invalid");
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();

		if((theSelectedWidget== kOKButtonWidgetID || theSelectedWidget== kOKButtonWidgetID) && theChange == kTrueStateMessage)
		{
			//CAlert::InformationAlert("theSelectedWidget== kOKButtonWidgetID");

			//***** updater Download functioality
			#ifdef MACINTOSH
				//MAC code
				ConstStr255Param urlStr=(const unsigned char*)LNGActionComponent::printSourceUpdatesURL.GrabCString();
				OSStatus err;
				ICInstance inst;
				long startSel;

				long endSel=LNGActionComponent::printSourceUpdatesURL.Length();
				OSType sign='Psi\0';
				err = ::ICStart(&inst, sign);           // Use your creator code if you have one!
				if (err == noErr) 
				{
					//err = ::ICFindConfigFile(inst, 0, nil);
					if (err == noErr) 
					{
						startSel = 0;
						//endSel = urlStr[0];
						ConstStr255Param hint(0x0);
						err = ::ICLaunchURL(inst, "\p",/*(char *) &urlStr[1]*/LNGActionComponent::printSourceUpdatesURL.GrabCString(),
						LNGActionComponent::printSourceUpdatesURL.Length(), &startSel, &endSel);
					}
					(void) ICStop(inst);
				}			
			#else					
				InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
				if(ptrIAppFramework == nil)
				{
					CAlert::InformationAlert("ptrIAppFramework == nil");
					break;
				}
				PMString updaterURL("");
				updaterURL.Append(LNGActionComponent::printSourceUpdatesURL);
				//CAlert::InformationAlert(updaterURL);
				
				if(updaterURL.NumUTF16TextChars()==0)
					break;
				HINSTANCE p = ShellExecuteA(NULL,"open",updaterURL.GrabCString(),NULL,NULL, SW_SHOWNORMAL);
			#endif;			
				CDialogObserver::CloseDialog();	

			break;
		}
		else if((theSelectedWidget == kCancelButton_WidgetID || theSelectedWidget == kCancelButton_WidgetID) && theChange == kTrueStateMessage)
		{
			CDialogObserver::CloseDialog();	
		}
	}while(kFalse);
}
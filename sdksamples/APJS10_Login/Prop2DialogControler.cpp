/*
//	File:	LNG3DialogController.cpp
//
//	Date:	17-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2003 Next Millenium Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "IStringListControlData.h"

// Project includes:
#include "LNGID.h"

#include "dataStore.h"
//#include "listBoxHelperFuncs.h"
#include "IDropDownListController.h"
#include "IPanelControlData.h"
#include "IControlView.h"

#include "CAlert.h"
#include "dataStore.h"
#include "LoginHelper.h"
#include "LNGTreeModel.h"
#include "ITreeViewMgr.h"
#include "LNGTreeDataCache.h"

//#include "Mediator.h"
#define CA(X) CAlert::InformationAlert(X)

extern IPanelControlData * LauchPadPanelControlData;
extern int32 displayEnvList ;

bool16 ISChangeInModewhileEdit = kFalse;

/** LNG3DialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author raghu
*/
class LNG4DialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		LNG4DialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~LNG4DialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext*); 

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateFields to be called. When all widgets are valid, 
			ApplyFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext*);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext*, const WidgetID&);

		//virtual void disableControls(void);
		virtual void EnableAllTheControls(void);
		//virtual void DisableControlsDependOnServer(PMString& vendorName);
		//virtual PMString getSelectedVendor(void);
		//virtual PMString getSelectedVersion(void);
		//virtual void  addVersions(PMString& versionName);
		void modifyLaunchpadListData(void);
		void addServerToLaunchpadList(void);		

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(LNG4DialogController, kLNGProp2DlgControllerImpl)

/* ApplyFields
*/
void LNG4DialogController::InitializeDialogFields(IActiveContext* myContext)
{
	CDialogController::InitializeDialogFields(myContext);	
	int i=0;
	//vector<CVendorInfoValue> *VectorVendorInfoPtr =nil;
	 
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("InitializeDialogFields panelControlData nil");
		return ;
	}

	//Put code to initialize widget values here.
		LoginHelper LngHelpObj(this);
	
	IControlView* iControlView = panelControlData->FindWidget(kProp2ConnModeDropDownWidgetID);
	if (iControlView == nil)
		return;

	InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());
	if (iDropDownListController == nil)
		return;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return ;
	}

	if(EvnironmentData::getIsEditServerFlag()== TRUE)
	{ // for edit exizting server

		int selectedRow=-1;// = EvnironmentData::getSelectedserver();
		PMString selectedEnv;
		selectedEnv = EvnironmentData::getSelectedServerEnvName();
		/*CA("Inside EvnironmentData::getIsEditServerFlag()== TRUE");
		CA(selectedEnv);*/
		LoginInfoValue selectedServerProp;
		
		if(LngHelpObj.getCurrentServerInfo(selectedEnv,selectedServerProp)==-1){
			CAlert::InformationAlert("finding server fail...");
			return;
		}

		//if(selectedServerProp.getdbConnectionMode() == 1)
		//{	//CA("From Server to Http");
		//	SetTextControlData(kProp2EnvTextEditWidgetID, selectedServerProp.getEnvName());
		//	SetTextControlData(kProp2SrvTextEditWidgetID, "");

		//	iDropDownListController->Select(0);
		//	ptrIAppFramework->setSelectedConnMode(0); // for Http	

		//	ISChangeInModewhileEdit = kTrue;
		//}
		//else
		{
			ISChangeInModewhileEdit = kFalse;
			SetTextControlData(kProp2EnvTextEditWidgetID, selectedServerProp.getEnvName());
			SetTextControlData(kProp2SrvTextEditWidgetID, selectedServerProp.getCmurl());

			iDropDownListController->Select(0);
			//ptrIAppFramework->setSelectedConnMode(0); // for Http
		}
	
	}
	else { 
		//CA("inside Else part");
		ISChangeInModewhileEdit = kFalse;
		//if(EvnironmentData::setIsEditServerFlag()== FALSE) // for create new server
		SetTextControlData(kProp2EnvTextEditWidgetID, "");
		SetTextControlData(kProp2SrvTextEditWidgetID, "");

		iDropDownListController->Select(0);
		//ptrIAppFramework->setSelectedConnMode(0); // for Http	
		
	}
	/*if(VectorVendorInfoPtr != nil)
		delete VectorVendorInfoPtr;*/
}

/* ValidateFields
*/
WidgetID LNG4DialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	bool8 isEnable=TRUE;	
	IControlView* controlView=NULL;
//	CAlert::InformationAlert("ValidateFields...");
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("ValidateFields panelControlData nil");
		return result;
	}
	do{
		PMString editFieldString;
		editFieldString=GetTextControlData(kProp2EnvTextEditWidgetID);	
		if(editFieldString=="")
			return kProp2EnvTextEditWidgetID;

		//editFieldString=getSelectedVendor();
		//if(editFieldString=="")
		//	return kProp2EnvTextEditWidgetID;
		
		//editFieldString=getSelectedVersion();
		//if(editFieldString=="")
		//	return kPropVersionDropDownWidgetID;
		controlView = panelControlData->FindWidget(kProp2SrvTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("ValidateFields DBServer ControlView nil");
			break;
		}
		isEnable=controlView->IsEnabled();
		if(isEnable ==TRUE)
		{
			editFieldString="\0";
			editFieldString=GetTextControlData(kProp2SrvTextEditWidgetID);	
			if(editFieldString=="")
				return kProp2SrvTextEditWidgetID;			
		}
		/*controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("ValidateFields port ControlView nil");
			break;
		}
		isEnable=controlView->IsEnabled();
		if(isEnable ==TRUE){
			editFieldString="\0";
			editFieldString=GetTextControlData(kPropPrtTextEditWidgetID);	
			if(editFieldString==""){				
				SetTextControlData(kPropPrtTextEditWidgetID,editFieldString);	
				return kPropPrtTextEditWidgetID;			
			}
		}

		controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("ValidateFields Schema ControlView nil");
			break;
		}
		isEnable=controlView->IsEnabled();
		if(isEnable ==TRUE){
			editFieldString="\0";
			editFieldString=GetTextControlData(kPropSchemaTextEditWidgetID);	
			if(editFieldString=="")
				return kPropSchemaTextEditWidgetID;
			
		}

		controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("ValidateFields Username ControlView nil");
			break;
		}
		isEnable=controlView->IsEnabled();
		if(isEnable ==TRUE){
			editFieldString="\0";
			editFieldString=GetTextControlData(kPropUnameTextEditWidgetID);	
			if(editFieldString=="")
				return kPropUnameTextEditWidgetID;			
		}

		controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("ValidateFields passeord ControlView nil");
			break;
		}
		isEnable=controlView->IsEnabled();
		if(isEnable ==TRUE){
			editFieldString="\0";
			editFieldString=GetTextControlData(kPropPwordTextEditWidgetID);	
			if(editFieldString=="")
				return kPropPwordTextEditWidgetID;
		}*/
		
	}while(0);
	return result;	
}

/* ApplyFields
*/
void LNG4DialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetID)
{
	// Replace with code that gathers widget values and applies them.
	 
	bool8 isEnable=TRUE;
	IControlView* icontrolView=NULL;
	LoginInfoValue serverProperties;
	PMString envName("");
	PMString ServerName("");
	PMString editFieldString("");
	PMString defaultString("");
	double clientID = -1;
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("ApplyFields panelControlData nil");
		return;
	}
	do{		
		envName=GetTextControlData(kProp2EnvTextEditWidgetID);
		serverProperties.setEnvName(envName);
		ServerName=GetTextControlData(kProp2SrvTextEditWidgetID);
		serverProperties.setCmurl(ServerName);
		//serverProperties.setdbConnectionMode(0);
		/*editFieldString=getSelectedVendor();
		serverProperties.setVendorName(editFieldString);
		
		editFieldString="\0";
		editFieldString=getSelectedVersion();
		serverProperties.setVersion(editFieldString);*/
		////////////////////////////
		
		
		/*InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
			if (panelControlData == nil)
			{
				CAlert::InformationAlert("panelControlData nil");
				break;
			}*/
			IPanelControlData *panelControlData = LauchPadPanelControlData;
				if (panelControlData == nil)
				{
					CAlert::InformationAlert("panelControlData nil");
					break;
				} 

			IControlView* iControlView = panelControlData->FindWidget(/*kLaunchpadListBoxWidgetID*/kLaunchpadTreeViewWidgetID);
				if (iControlView == nil)
			{
				CAlert::InformationAlert("iControlView nil");
				break;
			}
		//////////////////////////////
		
			isEnable=iControlView->IsEnabled();
			if(isEnable==TRUE){
	 		editFieldString="\0";
			editFieldString=GetTextControlData(kProp2SrvTextEditWidgetID);
			serverProperties.setCmurl(editFieldString);
		}
		else{ 
			serverProperties.setCmurl(defaultString);
		}

		 
	}while(0);
	LoginHelper LngHelpObj(this);		
	// adding new server
	if(EvnironmentData::getIsEditServerFlag()==FALSE)
	{	
		if(LngHelpObj.addServerInfo(serverProperties) == TRUE)
		{	
			EvnironmentData::setSelectedServerProperties(serverProperties);			
			addServerToLaunchpadList();		
			//CAlert::InformationAlert("Added new server");
			return;
		}
	}

	if(ISChangeInModewhileEdit)  // Use when user changes the connection Mode at the Properties dialog
	{	
	//CA("ISChangeInModewhileEdit");
        PMString asd = serverProperties.getEnvName();
	 	bool16 bStatus = LngHelpObj.deleteServer(asd);
		if(LngHelpObj.addServerInfo(serverProperties) == TRUE)
		{			
			EvnironmentData::setSelectedServerProperties(serverProperties);			
			modifyLaunchpadListData();
			return;
		}
	}
	// edit server properties
	int32 isEditServer=0;
	if(isEditServer == envName.Compare(TRUE,EvnironmentData::getSelectedServerEnvName()))
	{	
		if(LngHelpObj.editServerInfo(serverProperties,clientID) == TRUE){
			EvnironmentData::setSelectedServerProperties(serverProperties);
			//CAlert::InformationAlert("edit existing server");
		}

	}
	else { // if the environment is changes( adding new one and deleting old one)
		//CA("1");
        PMString asd = EvnironmentData::getSelectedServerEnvName();
		if(LngHelpObj.deleteServer(asd) == TRUE){
			//CAlert::InformationAlert("deleted old server");						
			if(LngHelpObj.addServerInfo(serverProperties) == TRUE){
				//CAlert::InformationAlert("Added new server");
				EvnironmentData::setSelectedServerProperties(serverProperties);
			}
		}
	}
	if(EvnironmentData::getIsEditServerFlag()==TRUE)
		modifyLaunchpadListData();	
}

//  Generated by Dolly build 17: template "Dialog".
// End, LNG3DialogController.cpp.


void LNG4DialogController::modifyLaunchpadListData(void)
{	 
	LoginInfoValue currentServerProps = EvnironmentData::getServerProperties();
	IPanelControlData* panelControlData = EvnironmentData::getlauncpadPanel();

	IControlView* view = panelControlData->FindWidget(/*kLaunchpadListBoxWidgetID*/kLaunchpadTreeViewWidgetID);
	if(view == nil){
		CAlert::InformationAlert("Fail to find LaunchPad ListBox");
		return;
	}
	//listBoxHelper listHelper(this, kLNGPluginID);
	//listHelper.modifyListElementData(panelControlData);	
	view->ShowView();
	view->Enable();
			
	InterfacePtr<ITreeViewMgr> treeViewMgr(view, UseDefaultIID());
	if(!treeViewMgr)
	{
		CA("treeViewMgr is nil");
		return ;
	}

	LNGTreeDataCache dc;
	dc.clearMap();

	LNGTreeModel treeModel;
	displayEnvList = 1;
	PMString pfName("101 Root");
	treeModel.setRoot(-1, pfName, 1);
	treeViewMgr->ClearTree(kTrue);
	treeModel.GetRootUID();
	
	treeViewMgr->ChangeRoot();				
	view->Invalidate();
}

void LNG4DialogController::addServerToLaunchpadList(void)
{	
	IPanelControlData* panelControlData = EvnironmentData::getlauncpadPanel();
	IControlView* view = panelControlData->FindWidget(/*kLaunchpadListBoxWidgetID*/kLaunchpadTreeViewWidgetID);
	if(view == nil){
		CAlert::InformationAlert("Fail to find LaunchPad ListBox");
		return;
	}
	//listBoxHelper listHelper(this, kLNGPluginID);
	//listHelper.AddElement(view,EvnironmentData::getServerProperties());
	
	view->ShowView();
	view->Enable();
			
	InterfacePtr<ITreeViewMgr> treeViewMgr(view, UseDefaultIID());
	if(!treeViewMgr)
	{
		CA("treeViewMgr is nil");
		return ;
	}

	LNGTreeDataCache dc;
	dc.clearMap();

	LNGTreeModel treeModel;
	displayEnvList = 1;
	PMString pfName("101 Root");
	treeModel.setRoot(-1, pfName, 1);
	treeViewMgr->ClearTree(kTrue);
	treeModel.GetRootUID();
	
	treeViewMgr->ChangeRoot();				
	view->Invalidate();
}


/*void LNG4DialogController::disableControls(void)
{
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("InitializeFields panelControlData nil");
		return;
	}

	do{		
		IControlView* controlView = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
			break;
		}
		controlView->Disable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields servername controlView nil");
			break;
		}
		controlView->Disable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields port controlView nil");
			break;
		}
		controlView->Disable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields schema controlView nil");
			break;
		}
		controlView->Disable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields username controlView nil");
			break;
		}
		controlView->Disable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields password controlView nil");
			break;
		}
		controlView->Disable(kTrue);
	}while(0);
}*/

/*void LNG4DialogController::DisableControlsDependOnServer(PMString& vendorName)
{
	PMString oracle("Oracle"),sql("SQLServer"),access("Access");
	
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("InitializeFields panelControlData nil");
		return;
	}

	do{		
		if(vendorName == access){
			IControlView* controlView= panelControlData->FindWidget(kPropSrvTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields servername controlView nil");
				break;
			}
			bool8 flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields port controlView nil");
				break;
			}
			flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields username controlView nil");
				break;
			}
			flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields password controlView nil");
				break;
			}
			flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);		
			
		}
		if(vendorName == oracle ){			
			IControlView* controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields schema controlView nil");
				break;
			}
			bool8 flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);
		}
	}while(0);
}*/

void LNG4DialogController::EnableAllTheControls(void)
{	
	do{	
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("EnableAllTheControls panelControlData nil");
			break;
		}
		
		IControlView* controlView= panelControlData->FindWidget(kProp2SrvTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" servername controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kProp2EnvTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" port controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		/*controlView=nil;
		controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" username controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" password controlView nil");
			break;
		}
		controlView->Enable(kTrue);
	
		controlView=nil;
		controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema
		if (controlView == nil){
			CAlert::InformationAlert(" schema controlView nil");
			break;
		}		
		controlView->Enable(kTrue);	*/
	}while(0);
}

/*PMString LNG4DialogController::getSelectedVendor(void)
{
	PMString vendorName("");
	//CAlert::InformationAlert("getSelectedVendor");
	do
	{	
		// Get the name of the selected snippet.
		InterfacePtr<IStringListControlData> vendorDropListData(this->QueryListControlDataInterface(kProp2EnvTextEditWidgetID));	
		if (vendorDropListData == nil)		{
			CAlert::InformationAlert("Vendor vendorDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> vendorDropListController(vendorDropListData, UseDefaultIID());
		if (vendorDropListController == nil){
			CAlert::InformationAlert("vendorDropListController nil");
			break;
		}
		int32 selectedRow = vendorDropListController->GetSelected();

		if(selectedRow <0){
			CAlert::InformationAlert("invalid row");
			return PMString("");
		}		
		vendorName.SetString(vendorDropListData->GetString(selectedRow));
	}while(0);
	return vendorName;
}*/

/*PMString LNG4DialogController::getSelectedVersion(void)
{
	//CAlert::InformationAlert("getSelectedVersion");
	PMString versionName("");
	do
	{
		// Get the name of the selected snippet.
		InterfacePtr<IStringListControlData> versionDropListData(this->QueryListControlDataInterface(kPropVersionDropDownWidgetID));
		if (versionDropListData == nil){
			CAlert::InformationAlert(" versionDropListData nil");
			break;
		}		
		
		InterfacePtr<IDropDownListController> versionDropListController(versionDropListData, UseDefaultIID());
		if (versionDropListController == nil){
			CAlert::InformationAlert("versionDropListController nil");
			break;
		}
		int32 selectedRow = versionDropListController->GetSelected();
		if(selectedRow <0){			
			return versionName;
		}		
		versionName.SetString(versionDropListData->GetString(selectedRow));
	}while(0);
	return versionName;
}*/

/*void LNG4DialogController::addVersions(PMString& selectedeVendorName)
{	
	//CAlert::InformationAlert("LNG3DialogController addVersions");

	LoginHelper LngHelpObj;
	vector<CVendorInfoValue> *VectorVendorInfoPtr = LngHelpObj.getVendors();
	VectorVendorInfoValue::iterator it;
		
	InterfacePtr<IStringListControlData> versionDropListData(this->QueryListControlDataInterface(kPropVersionDropDownWidgetID));
	if (versionDropListData == nil){
		CAlert::InformationAlert("versionDropListData nil");	
	}
	versionDropListData->Clear(kFalse, kFalse);

	for(it = VectorVendorInfoPtr->begin(); it != VectorVendorInfoPtr->end(); it++)
	{		
		int32 isVendorMatch = selectedeVendorName.Compare(TRUE,it->getVendorName());
		if(isVendorMatch==0)
		{
			vector<PMString> versions;
			vector<PMString>::iterator versionItrator;
			versions=it->getVendorVersion();
			int j=0;
			for(versionItrator = versions.begin(); versionItrator != versions.end(); versionItrator++)
			{
				if(versionDropListData)
					versionDropListData->AddString(*versionItrator, j, kFalse, kFalse);
				j++;				
			}	
		}
	}
}*/

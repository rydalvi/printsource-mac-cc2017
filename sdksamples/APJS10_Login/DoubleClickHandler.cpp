#include "VCPluginHeaders.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextControlData.h"
#include "IListBoxController.h"
#include "IWidgetParent.h"
#include "IDialogController.h"
#include "CEventHandler.h"
#include "EventUtilities.h"
#include "CAlert.h"
#include "LNGID.h"
#include "LNGActionComponent.h"
//#include "listBoxHelperFuncs.h"
#include "dataStore.h"
#include "LoginHelper.h"

#include "IStringListControlData.h"
#include "IDropDownListController.h"
//12-march
extern bool16 isValidServerStatus;
//12-march
#define CA(X) CAlert::InformationAlert(X)
class DoubleClickHandler : public CEventHandler
{
public:
	DoubleClickHandler(IPMUnknown *boss);	
	
	virtual ~DoubleClickHandler(){}
	virtual bool16 ButtonDblClk(IEvent* e);
	
private:
	void closeOptnsDialog(void);
	void changeStatusText(PMString& , bool8);

};

CREATE_PMINTERFACE(DoubleClickHandler , DoubleClickEventHandlerImpl)

DoubleClickHandler::DoubleClickHandler(IPMUnknown *boss) : CEventHandler(boss)
{
	//CA("DoubleClickHandler's Constructor");
}

bool16 DoubleClickHandler::ButtonDblClk(IEvent* e)
{
	
	do
	{	
		//CA("DoubleClickHandler::ButtonDblClk");
		 InterfacePtr<IControlView> ictrlView(this,IID_ICONTROLVIEW);
		 if (ictrlView == nil)
		 {
			CAlert::InformationAlert("ictrlView nil");
			break;
		 }	

		PMString envName("");
		int selection=-1;
		selection= EvnironmentData::getSelectedserver();	
		envName = EvnironmentData::getSelectedServerEnvName();

		changeStatusText(envName,FALSE);

		IPanelControlData* panelControlData = EvnironmentData::getLoginPanel();
			
		IControlView* iHostDropDownWidgetControlView = panelControlData->FindWidget(kHostDropDownWidgetID);
		if (iHostDropDownWidgetControlView == nil)
			break ;

		InterfacePtr<IStringListControlData> HostDropListData(iHostDropDownWidgetControlView,UseDefaultIID());
		if(HostDropListData == nil)
			break;	

		InterfacePtr<IDropDownListController> HostDropListController(HostDropListData,UseDefaultIID());
		if (HostDropListController == nil)		
			break;			
		
		HostDropListController->Select(EvnironmentData::getSelectedserver());

		//LoginHelper lngHelperObj(this);

		//isValidServerStatus = lngHelperObj.isValidServer(envName);

		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
				break;
		}


		/*if(EvnironmentData::SelctedConnMode == 1)
		{			
			ptrIAppFramework->setSelectedConnMode(1); // for JDBC
			bool16 bStatus = lngHelperObj.setCurrentServer(envName);

			if(bStatus != TRUE)
			{
				//CAlert::InformationAlert("Failed to set Current Server... ");
				PMString str("Please select an environment to connect to.");
				changeStatusText(str,TRUE);
				break;
			} 	
		}*/
		if(EvnironmentData::SelctedConnMode == 0)
		{
			//ptrIAppFramework->setSelectedConnMode(0); // for Http	
			//CA("Http Connection Selected");
		}
		//closeOptnsDialog();
		LNGActionComponent actionObj(this);
		actionObj.CloseLatestDialog();

	}while(0);
	return CEventHandler::ButtonDblClk(e);
}



void DoubleClickHandler::closeOptnsDialog(void)
{
	InterfacePtr<IWidgetParent> parentHolder(this, IID_IWIDGETPARENT);
	if(parentHolder==nil){
		//CAlert::InformationAlert("parentHolder nil");
		return;
	}

	InterfacePtr<IDialog> dialog((IDialog*) parentHolder->QueryParentFor(IID_IDIALOG));
	if(dialog==nil){
		//CAlert::InformationAlert("dialog nil");
		return;
	}
	dialog->Close();            
}


void DoubleClickHandler::changeStatusText(PMString& endName,bool8 isDefaultText)
{	
	PMString status("");
	if(isDefaultText==FALSE){ // connecting string
		status+="Connecting to"; 
		status+=endName;
		status+="...   ";
	}
	else status+=endName;
	do{
		IPanelControlData* panelControlData = EvnironmentData::getlauncpadPanel();

		//IControlView* iControlView = panelControlData->FindWidget(kLaunchpadStatusTextWidgetID);
		//if (iControlView == nil){
		//	//CAlert::InformationAlert("iControlView of status text nil");
		//	break;
		//}	
		//InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
		//if (textControlData == nil){
		//	//CAlert::InformationAlert("versionDropListData nil");	
		//	break;
		//}
		//textControlData->SetString(status);
		//iControlView->Invalidate();

		IDialog* dlgPtr = LNGActionComponent::getDlgPtr();
		if(dlgPtr){			
			IControlView* view = dlgPtr->GetDialogPanel();
			view->Invalidate();
		}
		//CAlert::InformationAlert(endName);
	}
	while(0);
}


#include "VCPlugInHeaders.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "CAlert.h"
#include "LNGID.h"
#include "LNGActionComponent.h"
#include "dataStore.h"
#include "IClientOptions.h"
//6-march
#include "SDKFileHelper.h"
#include "IPMStream.h"
#include "StreamUtil.h"
#include "SaveLog.h"
#include "IWidgetParent.h"
#include "time.h"
#include "ICounterControlData.h"
#include "ITextControlData.h"
#include "IAppFramework.h"
#include "ILoginEvent.h"
#include "LNGMediator.h"
//#include "LNGPlugInEntrypoint.h"

//------------
#include "ICategoryBrowser.h"
#include "IPanelMgr.h"
#include "IWindow.h"
#include "PaletteRefUtils.h"
#include "LoginDialogControler.h"
#include "IDropDownListController.h"
#include "LoginHelper.h"
#include "AcquireModalCursor.h"
#include "LNGTreeDataCache.h"
#include "RsrcSpec.h"

#include "LNGTreeModel.h"  //****url

IPanelControlData* iPanelCntrlDataPtr;
extern PMString logTxt;
extern bool16 bResult;
extern bool16 isValidServerStatus;
extern PMString selectedClientName;
extern VectorClientModelPtr vectorClientModelPtr;
extern PMString UserNameStr;
extern PMString PasswordStr;
extern double selectedClientId;




//#define CA(X) CAlert::InformationAlert(X)
//6-march
class LNG2DialogObserver : public CDialogObserver
{
	public:
		LNG2DialogObserver(IPMUnknown* boss) : CDialogObserver(boss) { bp = boss;}
		virtual ~LNG2DialogObserver() {}
		virtual void AutoAttach();
		virtual void AutoDetach();
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
	private:
		IPMUnknown* bp;
		void fireUserLoggedIn();
};

CREATE_PMINTERFACE(LNG2DialogObserver, kLNGLoginDlgObserverImpl)

void LNG2DialogObserver::AutoAttach()
{
	CDialogObserver::AutoAttach();

	do
	{


		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("LNG2DialogObserver::AutoAttach() panelControlData invalid");
			break;
		}	
			//12-march
			InterfacePtr<IWidgetParent> findParentOfRollOverIconButtonWidget(this,UseDefaultIID());
			if(findParentOfRollOverIconButtonWidget == nil)
			{
				CAlert::InformationAlert("findParentOfRollOverIconButtonWidget == nil");
				break;
			}
			IPMUnknown *  primaryResourcePanelWidgetUnKnown = findParentOfRollOverIconButtonWidget->GetParent();
			if(primaryResourcePanelWidgetUnKnown == nil)
			{
				CAlert::InformationAlert("primaryResourcePanelWidgetUnKnown == nil");
				break;
			}
			InterfacePtr<IControlView>primaryResourcePanelWidgetControlView(primaryResourcePanelWidgetUnKnown,UseDefaultIID());
			if(primaryResourcePanelWidgetControlView == nil)
			{
				CAlert::InformationAlert("primaryResourcePanelWidgetControlView == nil");
				break;
			}
			InterfacePtr<IPanelControlData>primaryResourcePanelWidgetPanelControlData(primaryResourcePanelWidgetControlView,UseDefaultIID());
			if(primaryResourcePanelWidgetPanelControlData == nil)			
			{
				CAlert::InformationAlert("primaryResourcePanelWidgetPanelControlData == nil");
				break;
			}

			iPanelCntrlDataPtr = primaryResourcePanelWidgetPanelControlData ;
			SaveLog::Instance();
//12-march
		AttachToWidget(kSaveButtonWidgetID,  IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kLNGSaveRollOverWidgetID,  IID_ITRISTATECONTROLDATA,panelControlData);//15-march
		AttachToWidget(kLNGClearRollOverWidgetID,  IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kSecondCancelButtonWidgetID,  IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kLoginButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		
		
		//AttachWidget(panelControlData, kLNGSaveRollOverWidgetID, IID_ITRISTATECONTROLDATA);
		//AttachWidget(panelControlData, kLNGClearRollOverWidgetID, IID_ITRISTATECONTROLDATA);
		AttachToWidget(kOptionsButtonWidgetID,  IID_ITRISTATECONTROLDATA,panelControlData);
		AttachToWidget(kHostDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		AttachToWidget(kCancelButton1WidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kContinueButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		
	} while (false);
}

void LNG2DialogObserver::AutoDetach()
{
	CDialogObserver::AutoDetach();

	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("LNG2DialogObserver::AutoDetach() panelControlData invalid");
			break;
		}		

		DetachFromWidget(kSaveButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);	
		DetachFromWidget(kLNGSaveRollOverWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);//15-march
		DetachFromWidget(kLNGClearRollOverWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kSecondCancelButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kLoginButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kOptionsButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kHostDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		DetachFromWidget(kCancelButton1WidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachFromWidget(kContinueButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);

	} while (false);

	// Destroy the log.
	SaveLog::Instance()->DeleteInstance();
}

void LNG2DialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{	
	//CDialogObserver::Update(theChange,theSubject,protocol,changedBy);
	//CAlert::InformationAlert(" -- inside up[date of login dialog observer -- ");
	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("LNG2DialogObserver::Update() controlView invalid");
			break;
		}
		
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil){
			CAlert::InformationAlert("Pointer to IAppFramework is nil.");
			return ;
		}

		


		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			//CA("panelControlData == nil ");
			break;
		}
		IControlView* iContinueButtonWidgetControlView = panelControlData->FindWidget(kContinueButtonWidgetID);
		if (iContinueButtonWidgetControlView == nil)
			return;
		IControlView* iLoginButtonWidgetControlView = panelControlData->FindWidget(kLoginButtonWidgetID);
		if (iLoginButtonWidgetControlView == nil)
			return ;

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();				
		
		if((theSelectedWidget== kLoginButtonWidgetID /*|| (theSelectedWidget == kOKButtonWidgetID && iLoginButtonWidgetControlView->IsEnabled())*/ ) && theChange == kTrueStateMessage)
		{
			//CAlert::InformationAlert("-- kLoginButtonWidgetID button clicked --");
			//13-march

			AcquireWaitCursor awc;	
			awc.Animate(); 

			LoginHelper LoginHelperObj(this);

			PMString envName;
			envName = EvnironmentData::getSelectedServerEnvName();
			LoginHelperObj.isValidServer(envName);
			

			IControlView* iMultilineTextExpanderControlView = panelControlData->FindWidget(kLNGMultilineTextExpanderWidgetID);
			if (iMultilineTextExpanderControlView == nil){
				//CA("iMultilineTextExpanderControlView == nil");
				break;
			}

			InterfacePtr<ITextControlData> iTextControlData (iMultilineTextExpanderControlView,UseDefaultIID());
			if (iTextControlData == nil) {
				//CA("iTextControlData == nil ");
				break;
			}

			IControlView* iScrollBarControlView = panelControlData->FindWidget(kScrollBarWidgetID);
			if (iScrollBarControlView == nil){
				//CA("iScrollBarControlView == nil ");
				break;
			}

			InterfacePtr<ICounterControlData> counterData(iScrollBarControlView, UseDefaultIID());
			if(counterData==nil){ 
				//CA("counterData==nil ");
				break;
			}
			
			if(/*theSelectedWidget == */kLoginButtonWidgetID){
				LNG2DialogController  objlogin(this);
				WidgetID returnWidgetID = objlogin.ValidateDialogFields(nil); 
				if(returnWidgetID == kDefaultWidgetId)
					objlogin.ApplyDialogFields(nil,kInvalidWidgetID); 
			}
			int32 minValue = counterData->GetMinimum();	
			counterData->SetValue(minValue);
			logTxt.SetTranslatable(kFalse);
            logTxt.ParseForEmbeddedCharacters();
			iTextControlData->SetString(logTxt);
			int32 maxValue = counterData->GetMaximum();	
			counterData->SetValue(maxValue);


			//13-march	

			if(bResult)
			{						
				//ptrIAppFramework->loadAllCaches();

				counterData->SetValue(maxValue);
				//Sleep(1000);
				
				//if(theSelectedWidget== kLoginButtonWidgetID ||  theSelectedWidget == kOKButtonWidgetID)
				//{
				LNGActionComponent actionObj(this);
				actionObj.CloseDialog();						
				//}
				logTxt.Clear();					
				
				//********** Get Current VersionofPlugin and DB Version of Plugin if Mismatch then show Updater Dialog					
			//	VersionUpdateValue *versionValue ;
			//	try{
			//		versionValue = new VersionUpdateValue;
			//	}
			//	catch(bad_alloc){
			//		CAlert::InformationAlert("Sorry Memory allocation fails.");
			//	}
			//	ptrIAppFramework->GetVersionUpdate_getVersionUpdateValue(versionValue);	

			//	PMString  currentBuildVersion = ptrIAppFramework->get_currentBuildVersion();
			//	PMString currentDBBuildVersion =  versionValue->PRINTsource_Version_Number ;

			//	//CA(currentBuildVersion);
			//	//CA(versionValue->PRINTsource_Version_Number);
			//	if(versionValue != NULL  && (versionValue->PRINTsource_Version_Number != "" ) && currentBuildVersion.Compare(kTrue,versionValue->PRINTsource_Version_Number))
			//	{									
			//		LNGActionComponent::printSourceUpdatesURL.clear();
			//		#ifdef MACINTOSH
			//			LNGActionComponent::printSourceUpdatesURL.Append(versionValue->PRINTsource_Macintosh_Installer_Download_URL); 
			//		#else						
			//			LNGActionComponent::printSourceUpdatesURL.Append(versionValue->PRINTsource_Windows_Installer_Download_URL); 
			//		#endif
			//		//CA(LNGActionComponent::printSourceUpdatesURL);
			//		//if(currentBuildVersion.Compare(kTrue,currentDBBuildVersion))
			//		{
			//			//CA("Build Are Not Same.");							
			//			InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication()); //Cs4
			//			ASSERT(application);
			//			if (application == nil) {	
			//				break;
			//			}
			//			InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
			//			ASSERT(dialogMgr);
			//			if (dialogMgr == nil) {
			//				break;
			//			}						
			//			//Load the plug-in's resource.
			//			PMLocaleId nLocale = LocaleSetting::GetLocale();
			//			RsrcSpec dialogSpec
			//			(
			//				nLocale,					// Locale index from PMLocaleIDs.h. 
			//				kLNGPluginID,			    // Our Plug-in ID  
			//				kViewRsrcType,				// This is the kViewRsrcType.
			//				kLNGVesionUpdaterDialogResourceID,//		kSDKDefDialogResourceID,	// Resource ID for our dialog.
			//				kTrue						// Initially visible.
			//			);
			//			IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
			//									
			//			dialog->Open();	
			//			dialog->WaitForDialog();							
			//		}					
			//	}							
			//	if(versionValue)
			//		delete versionValue;
			//	//****Up to Here				
			}
									
			if(EvnironmentData::getServerStatus()==1)
			{
				//CA("EvnironmentData::getServerStatus()==1");	
				InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
				if(cop != nil && bResult)
				{
					//CAlert::InformationAlert("Got the Options Interface");
					PMString name("");
					PMString LocaleName("");
					//CAlert::InformationAlert("-- calling get dufault locale and publication name --");
					cop->setdoRefreshFlag(kTrue);

					PMString imagePath = cop->getImageDownloadPath();


					double localeID = cop->getDefaultLocale(LocaleName);
					if(localeID != -1)
					{
						//CA("localeID != -1");
						double pubID = cop->getDefPublication(name);					
					}
					
					//CAlert::InformationAlert(" --publication name-- "+name);
					//CAlert::InformationAlert(" --locale name-- "+LocaleName);		

				}
				else{
					if(cop == nil)
						CAlert::InformationAlert(" Options plugin not available !!");
				}
			}	
			if(bResult)
				fireUserLoggedIn();					
		}
		else if((theSelectedWidget == kSecondCancelButtonWidgetID || theSelectedWidget == kCancelButton_WidgetID) && theChange == kTrueStateMessage)
		{			
			//LNGActionComponent actionObj(bp);
			//actionObj.DoDialog(1);
			//if(theSelectedWidget == kSecondCancelButtonWidgetID /*|| theSelectedWidget == kCancelButton_WidgetID*/ ){
			//****			

			CDialogObserver::CloseDialog();						 
			break;
			//}			
		}		
		else if (theSelectedWidget == kLNGSaveRollOverWidgetID && theChange == kTrueStateMessage)
		{
			ErrorCode status = kFailure;
			bool16 userCancelled = kFalse;
			do
			{
				// Choose file to save the log into.
				SDKFileSaveChooser fileChooser;
				PMString title("Save Log");
				title.SetTranslatable(kFalse);
				fileChooser.SetTitle(title);
				fileChooser.AddFilter('CWIE', 'TEXT', "txt", PMString("Text file(txt)", PMString::kUnknownEncoding));
				fileChooser.SetFilename("Log");
				fileChooser.ShowDialog();
				if (!fileChooser.IsChosen()) {
					userCancelled = kTrue;
					break;
				}	 
				SaveLog::Instance()->updateLog ();
				// Open a stream for the file selected by the user.
				InterfacePtr<IPMStream> stream(StreamUtil::CreateFileStreamWrite(fileChooser.GetIDFile(), kOpenOut, 'TEXT', 'CWIE'));
				if (stream == nil)
				{
					break;
				}
				// Save the log into the file.
				stream->Open();
				if (stream->GetStreamState() == kStreamStateGood)
				{
					stream->SetEndOfStream();
					SaveLog::Instance()->Save(stream);
					stream->Flush();
				}
				stream->Close();
			} while(false);
		}else if(theSelectedWidget ==  kLNGClearRollOverWidgetID && theChange == kTrueStateMessage)
		{
			logTxt .Clear();
			SaveLog::Instance()->Clear ();
		}
		else if (theSelectedWidget == kOptionsButtonWidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kOptionsButtonWidgetID");
			LNGActionComponent actionObj(bp);
			actionObj.DoDialog(1);
		}
		else if (theSelectedWidget == kCancelButton1WidgetID && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kOptionsButtonWidgetID");
			LNGActionComponent actionObj(bp);
			actionObj.CloseDialog();
			selectedClientName = "";
		}
		else if (theSelectedWidget== kHostDropDownWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		{
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			if (panelControlData == nil)
				return ;

			InterfacePtr<IDropDownListController> iDropDownListController(controlView, UseDefaultIID());
			if (iDropDownListController == nil)
				return;

			int32 selectedRowIndex=-1; 
			selectedRowIndex = iDropDownListController->GetSelected();
			
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil){
				CAlert::InformationAlert("Pointer to IAppFramework is nil.");
				return ;
			}		
			
			/*LNGTreeDataCache cacheObj;
			cacheObj.clearMap();*/

			LoginHelper LoginHelperObj(this);
			LoginInfoValue currentServerInfo;

			VectorLoginInfoPtr result=NULL;
			result=	LoginHelperObj.getAvailableServers();
			if(result== NULL){
				CAlert::InformationAlert(" Please Create New Environment and Server name/URL Properties. "); //AvailableServers File not found...
				return;
			}
			if(result->size()==0){
				//CAlert::InformationAlert("Available Servers File show zero entries.");
				return;
			}	

			int32 dropDownListRowIndex = 0;
			VectorLoginInfoValue::iterator it;
			for(it = result->begin(); it != result->end(); it++)
			{
				currentServerInfo = *it;	
				
				if(dropDownListRowIndex == selectedRowIndex)
				{
					EvnironmentData::setSelectedserver(dropDownListRowIndex);
                    PMString temp = currentServerInfo.getEnvName();
					EvnironmentData::setSelectedServerEnvName(temp);
					EvnironmentData::SelctedConnMode = 0; // currentServerInfo.getdbConnectionMode();
					EvnironmentData::setSelectedServerProperties(currentServerInfo);
					break;
				}
				dropDownListRowIndex++;				
			}
			delete result;
			
			PMString envName;
			envName = EvnironmentData::getSelectedServerEnvName();
			//CA("envName = " + envName);
			isValidServerStatus = kTrue; //LoginHelperObj.isValidServer(envName);

			IControlView * iOkButtonCntrlView = panelControlData->FindWidget(kLoginButtonWidgetID/*kOKButtonWidgetID*/);
			if(iOkButtonCntrlView == nil)
			{
				//CA("iOkButtonCntrlView == nil");
				return;
			}
			if(!isValidServerStatus)
			{				
				iOkButtonCntrlView->Disable();
			}
			else
			{				
				iOkButtonCntrlView->Enable();
			}
			//if(EvnironmentData::SelctedConnMode == 1)
			//{	//CA("Server Connection Selected");
			//	ptrIAppFramework->setSelectedConnMode(1); // for Server	

			//	bool16 bStatus = LoginHelperObj.setCurrentServer(envName);			
			//	if(bStatus  != TRUE){
			//		CAlert::InformationAlert("Failed to set Current Server... ");
			//		
			//		return;
			//	}  
			//}
			//if(EvnironmentData::SelctedConnMode == 0)
			//{
			//	ptrIAppFramework->setSelectedConnMode(0); // for Http	
			//	//CA("Http Connection Selected");
			//}
			
			PMString userName = EvnironmentData::getServerProperties().getUsername();
			IControlView * userNameView = panelControlData->FindWidget(kLoginUserNameTextEditWidgetID);
			if(userNameView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RegisterPubToONESource::RPTOSDialogObserver::bookInfo::userNameView == nil");
				return ;
			}
			InterfacePtr<ITextControlData> iTextControlData1(userNameView,UseDefaultIID());
			if(iTextControlData1 == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RegisterPubToONESource::RPTOSDialogObserver::bookInfo::iTextControlData1 == nil");
				return ;
			}		
			//CA("userName : " + userName);
			userName.SetTranslatable(kFalse);
            userName.ParseForEmbeddedCharacters();
			iTextControlData1->SetString(userName);
			PMString password = EvnironmentData::getServerProperties().getPassword();/*getdbAppenderForPassword();*/
			IControlView * passwordView = panelControlData->FindWidget(kLoginPasswordTextEditWidgetID);
			if(passwordView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RegisterPubToONESource::RPTOSDialogObserver::bookInfo::passwordView == nil");
				return ;
			}
			InterfacePtr<ITextControlData> iTextControlData2(passwordView,UseDefaultIID());
			if(iTextControlData2 == nil)
			{
				ptrIAppFramework->LogDebug("AP7_RegisterPubToONESource::RPTOSDialogObserver::bookInfo::iTextControlData2 == nil");
				return ;
			}		
			password.SetTranslatable(kFalse);
            password.ParseForEmbeddedCharacters();
			iTextControlData2->SetString(password);


			IControlView* iMultilineTextExpanderControlView = panelControlData->FindWidget(kLNGMultilineTextExpanderWidgetID);
			if (iMultilineTextExpanderControlView == nil)
				return;

			iMultilineTextExpanderControlView->ShowView();

			InterfacePtr<ITextControlData> iTextControlData (iMultilineTextExpanderControlView,UseDefaultIID());
			if (iTextControlData == nil) 
				return;

			IControlView* iScrollBarControlView = panelControlData->FindWidget(kScrollBarWidgetID);
			if (iScrollBarControlView == nil)
				return;

			InterfacePtr<ICounterControlData> counterData(iScrollBarControlView, UseDefaultIID());
			if(counterData==nil) 	
				return;

			//logTxt.Clear();
			PMString blank("  ");
			blank.SetTranslatable(kFalse);
			logTxt.Append(blank); //(blank);
			//WideString attempting(kAttemptingConnectionStaticTextKey);
			logTxt.Append("<-- Attempting Connection to -->  \n"); //(attempting);// 
			//14-march
			logTxt.Append((WideString)ptrIAppFramework ->GetTimeStamp ());
			logTxt.Append(blank); //((WideString)kBlankStringTextKey); //
			//14-march
			PMString envNamee ("Enviroment Name : ");//(kEnviornmentNameTextKey);
			 envNamee.Append (envName);

			logTxt.Append (envNamee);
			logTxt.Append("\n");

			logTxt.Append ("Server URL : "); //((WideString)kServerUrlTextKey);//
			logTxt.Append (currentServerInfo.getCmurl ());
			logTxt.Append (blank); // ((WideString)kBlankStringTextKey);//
			logTxt.Append("\n");

			if(currentServerInfo.getVendorName () == kUseProxyStaticKey)//"Use_Proxy")
			{
				logTxt.Append((WideString)"Proxy settings :");
				logTxt.Append(blank);
			
				logTxt.Append((WideString)"Protocol : ");
				logTxt.Append((WideString)currentServerInfo.getVersion());
				logTxt.Append(blank);

				logTxt.Append((WideString)"Host server : ");
				logTxt .Append((WideString)currentServerInfo.getServerName());
				logTxt.Append(blank);

				logTxt.Append((WideString)"Port : ");
				logTxt.AppendNumber(currentServerInfo.getServerPort()); //got a error
				logTxt.Append(blank);

				logTxt.Append((WideString)"User Name : ");
				logTxt.Append((WideString)currentServerInfo.getDbUserName());
				logTxt.Append(blank);
				logTxt.Append("\n");
			}

			logTxt.SetTranslatable(kFalse);
			ptrIAppFramework->LogDebug(logTxt);

			int32 minValue = counterData->GetMinimum();	
			counterData->SetValue(minValue);
			logTxt.SetTranslatable(kFalse);
            logTxt.ParseForEmbeddedCharacters();
			iTextControlData->SetString(logTxt);
			int32 maxValue = counterData->GetMaximum();	
			counterData->SetValue(maxValue);

			int32 minVaalue = counterData->GetMinimum();	
			counterData->SetValue(minVaalue);

			iMultilineTextExpanderControlView->Invalidate();
			iScrollBarControlView->Invalidate();
		}	
		else if((theSelectedWidget == kContinueButtonWidgetID || (theSelectedWidget == kOKButtonWidgetID && iContinueButtonWidgetControlView->IsEnabled()))&& theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kContinueButtonWidgetID");
			//vectorClientModelPtr = NULL;
			AcquireWaitCursor awc;	
				awc.Animate();

			LoginHelper LngHelpObj(this);

			//CA("selectedClientName = " + selectedClientName);
			if(selectedClientName == "")
			{
				CA("Please select a client to continue.");
				return;
			}
			PMString errorMsg("");
			bResult=LngHelpObj.loginUser(UserNameStr,PasswordStr,selectedClientName,vectorClientModelPtr, errorMsg);
			
			if(bResult==TRUE)
			{
				ptrIAppFramework->setClientID(selectedClientId);
				PMString envName = EvnironmentData::getSelectedServerEnvName();
				logTxt.Append ("    ");			
				logTxt.Append (ptrIAppFramework ->GetTimeStamp ());
				logTxt.Append (" ");
				logTxt.Append ("User ");
				logTxt.Append (UserNameStr );
				logTxt .Append (" connected successfully to " + envName + " !");
				logTxt .Append ("  ");
				//logTxt.Append ("    ");
				ptrIAppFramework->LogDebug( "User " + UserNameStr + " connected successfully to " + envName + " !");
							
				logTxt.Append (ptrIAppFramework ->GetTimeStamp ());
				logTxt.Append (" ");
				logTxt.Append ("Loading client caches ... ");
				logTxt.Append ("        ");

				ptrIAppFramework->setSelectedEnvName(envName);
				ptrIAppFramework->setClientID(selectedClientId);

				PMString ASD("clientId: ");
				ASD.AppendNumber(selectedClientId);
				ptrIAppFramework->LogDebug(ASD);
				//CA("Successfully connected to " + envName + " !");								
				InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
				if (panelControlData == nil)
					return ;

				IControlView* iMultilineTextExpanderControlView = panelControlData->FindWidget(kLNGMultilineTextExpanderWidgetID);
				if (iMultilineTextExpanderControlView == nil)
					return ;

				InterfacePtr<ITextControlData> iTextControlData (iMultilineTextExpanderControlView,UseDefaultIID());
				if (iTextControlData == nil) 
					return ;

				IControlView* iScrollBarControlView = panelControlData->FindWidget(kScrollBarWidgetID);
				if (iScrollBarControlView == nil)
					return ;

				InterfacePtr<ICounterControlData> counterData(iScrollBarControlView, UseDefaultIID());
				if(counterData==nil) 	
					return  ;

				int32 minValue = counterData->GetMinimum();	
				counterData->SetValue(minValue);
				logTxt.SetTranslatable(kFalse);
                logTxt.ParseForEmbeddedCharacters();
				iTextControlData->SetString(logTxt);
				int32 maxValue = counterData->GetMaximum();	
				counterData->SetValue(maxValue);

				//13-march
				/*RangeProgressBar progressBar("PRINTsource Login", 0, 1000, kFalse, kFalse);
				progressBar.SetTaskText("Connecting to " + envName );
				for(int32 i=0; i<1000; i++)
				{	
					progressBar.SetTaskText("Connecting to " + envName + "...");
					progressBar.SetPosition(i);
				}
				progressBar.Abort();	*/

				if(bResult)
				{							
					//ptrIAppFramework->loadAllCaches();

					counterData->SetValue(maxValue);
					//Sleep(1000);
					
					logTxt.Clear();				
				}
				
				LNGActionComponent actionObj(bp);
				actionObj.CloseDialog();
				
				EvnironmentData::setServerStatus(1);
				if(EvnironmentData::getServerStatus()==1)
				{
					//CA("EvnironmentData::getServerStatus()==1");	
					InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
					if(cop != nil && bResult)
					{
						//CAlert::InformationAlert("Got the Options Interface");
						PMString name("");
						PMString LocaleName("");
						//CAlert::InformationAlert("-- calling get dufault locale and publication name --");
						cop->setdoRefreshFlag(kTrue);
						double localeID = cop->getDefaultLocale(LocaleName);
						if(localeID != -1){
							double pubID = cop->getDefPublication(name);
						}
						PMString imagePath = cop->getImageDownloadPath();
						//CAlert::InformationAlert(" --publication name-- "+name);
						//CAlert::InformationAlert(" --locale name-- "+LocaleName);
					}
					else{
						if(cop == nil)
							CAlert::InformationAlert(" Options plugin not available !!");
					}
				}	
				fireUserLoggedIn();			
				
				//iMultilineTextExpanderControlView->Invalidate();
				//iScrollBarControlView->Invalidate();

				int32 isEditServer=0;
				if(isEditServer == envName.Compare(TRUE,EvnironmentData::getSelectedServerEnvName()))
				{	
					LoginInfoValue serverProperties  = EvnironmentData::getServerProperties();
					serverProperties.setUsername(UserNameStr);
					//serverProperties.setdbAppenderForPassword(PasswordStr);
					serverProperties.setPassword(PasswordStr);
					//serverProperties.setServerPort(1);
					//int32 clientID = -1;
					if(LngHelpObj.editServerInfo(serverProperties,selectedClientId) == TRUE){
						EvnironmentData::setSelectedServerProperties(serverProperties);						
					}
				}				
				LoginInfoValue currentServerInfo;
				
				VectorLoginInfoPtr result1=NULL;
				result1=	LngHelpObj.getAvailableServers();
				if(result1== NULL){
					CAlert::InformationAlert(" Please Create New Environment and Server name/URL Properties. "); //AvailableServers File not found...
					return ;
				}
				if(result1->size()==0){
					//CAlert::InformationAlert("Available Servers File show zero entries.");
					return ;
				}	

				VectorLoginInfoValue::iterator it;
				for(it = result1->begin(); it != result1->end(); it++)
				{
					currentServerInfo = *it;						
					if(envName != currentServerInfo.getEnvName())
					{
						//CA("inside for = " + currentServerInfo.getEnvName());
						//currentServerInfo.setServerPort(0);
						//int32 clientID = -1;
						//if(LngHelpObj.editServerInfo(currentServerInfo,clientID) == TRUE){
						//	//CAlert::InformationAlert("edit existing server");
						//}						
					}														
				}
				delete result1;
				return;					
			}
			if(bResult!=TRUE)
			{				
				EvnironmentData::setServerStatus(-1);
				//13-march
				//logTxt.Append ("  ");
				logTxt.Append (ptrIAppFramework->GetTimeStamp ());
				logTxt.Append (" ");
				PMString loginErrorString(""); /*= ptrIAppFramework->getLoginErrorString ();*/
				if(loginErrorString.NumUTF16TextChars() == 0)
				{
					loginErrorString = "Some unknwon error occured. Please contact your System Administrator!" ;
				}

				logTxt .Append(loginErrorString);
				ptrIAppFramework->LogError(loginErrorString);
				logTxt .Append ("  ");

				//int32 minValue = counterData->GetMinimum();	
				//counterData->SetValue(minValue);
				//iTextControlData->SetString(logTxt);
				//int32 maxValue = counterData->GetMaximum();	
				//counterData->SetValue(maxValue);

		 		return ;			
			}	
		}
	} while (false);
}


void LNG2DialogObserver::fireUserLoggedIn()
{
	vector<int32 > observers=getMediators()->getObservers();
	vector<int32 >::iterator it;

	for(it = observers.begin() ; it != observers.end() ; it++)
	{
		int32 i = *it;

		InterfacePtr<ILoginEvent> evt((ILoginEvent*) ::CreateObject(i,IID_ILOGINEVENT));
		if(!evt)
		{
			//CA("Invalid logEvtHndler in LNG2DialogController");
			continue;
		}
		//CA("Executing user login event");
		evt->userLoggedIn();
	}
}



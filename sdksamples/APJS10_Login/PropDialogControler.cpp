/*
//	File:	LNG3DialogController.cpp
//
//	Date:	17-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2003 Next Millenium Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "IStringListControlData.h"

// Project includes:
#include "LNGID.h"

#include "dataStore.h"
//#include "listBoxHelperFuncs.h"
#include "IDropDownListController.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "LoginHelper.h"
#include "ITextControlData.h"

#include "CAlert.h"
#include "dataStore.h"
#include "ILoginHelper.h"

#include "PropDialogControler.h"
#include "LNGTreeModel.h"
#include "ITreeViewMgr.h"
#include "LNGTreeDataCache.h"

//#define CA(X) CAlert::InformationAlert(X)
//inline PMString numToPMString(int32 num)
//{
//	PMString x;
//	x.AppendNumber(num);
//	return x;
//}
//#define CA(X) CAlert::InformationAlert \
//	( \
//		PMString("MyControlView.cpp") + PMString("\n") + \
//		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
//		PMString("\n Message : ")+ X \
//	)
//#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//
//
 bool16 ISChangeInModewhileEdit1 =kFalse;

 extern int32 displayEnvList ;
 //extern bool16 JDBCEnable;



/** LNG3DialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author raghu
*/
//class LNG3DialogController : public CDialogController
//{
//	public:
//		/**
//			Constructor.
//			@param boss interface ptr from boss object on which this interface is aggregated.
//		*/
//		LNG3DialogController(IPMUnknown* boss) : CDialogController(boss) {}
//
//		/** 
//			Destructor.
//		*/
//		virtual ~LNG3DialogController() {}
//
//		/**
//			Initialize each widget in the dialog with its default value.
//			Called when the dialog is opened.
//		*/
//		virtual void InitializeDialogFields(IActiveContext*); 
//
//		/**
//			Validate the values in the widgets. 
//			By default, the widget with ID kOKButtonWidgetID causes 
//			ValidateFields to be called. When all widgets are valid, 
//			ApplyFields will be called.			
//			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.
//
//		*/
//		virtual WidgetID ValidateDialogFields(IActiveContext*);
//
//		/**
//			Retrieve the values from the widgets and act on them.
//			@param widgetId identifies the widget on which to act.
//		*/
//		virtual void ApplyDialogFields(IActiveContext*, const WidgetID&);
//
//		virtual void disableControls(void);
//		virtual void EnableAllTheControls(void);
//		virtual void DisableControlsDependOnServer(PMString& vendorName);
//		virtual PMString getSelectedVendor(void);
//		virtual PMString getSelectedVersion(void);
//		virtual void  addVersions(PMString& versionName);
//		void modifyLaunchpadListData(void);
//		void addServerToLaunchpadList(void);
//
//};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(LNG3DialogController, kLNGPropDlgControllerImpl)

/* ApplyFields
*/
void LNG3DialogController::InitializeDialogFields(IActiveContext* myContext)
{
	
	CDialogController::InitializeDialogFields(myContext);	
	int i=0;
	//vector<CVendorInfoValue> *VectorVendorInfoPtr =nil;
	//CAlert::InformationAlert("Prop InitializeFields");
	// Put code to initialize widget values here.

	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("InitializeDialogFields panelControlData nil");
		return ;
	}

	LoginHelper LngHelpObj(this);

	InterfacePtr<IStringListControlData> vendorDropListData(this->QueryListControlDataInterface(kPropVendorDropDownWidgetID));
	if (vendorDropListData == nil){
		CAlert::InformationAlert("vendorDropListData nil");	
	}
	vendorDropListData->Clear(kFalse, kFalse);
	
	InterfacePtr<IStringListControlData> versionDropListData(this->QueryListControlDataInterface(kPropVersionDropDownWidgetID));
	if (versionDropListData == nil){
		CAlert::InformationAlert("versionDropListData nil");	
	}
	versionDropListData->Clear(kFalse, kFalse);

	//IControlView* iControlView = panelControlData->FindWidget(kPropConnModeDropDownWidgetID);mane
	//if (iControlView == nil)
	//{
	//	CA("4444");
	//	return;
	//}

	/*InterfacePtr<IStringListControlData> propConnModeListData(iControlView,UseDefaultIID());	mane
	if (propConnModeListData == nil)		{
		CAlert::InformationAlert(" propConnModeListData nil");
		return;
	}*/
	//CA("111");
	/*InterfacePtr<IDropDownListController> iDropDownListController(iControlView, UseDefaultIID());mane
	if (iDropDownListController == nil)
		return;*/
////////21-feb
	//mane
	/*IControlView* iLoglevelControlView = panelControlData->FindWidget(kPropLogLevelDropDownWidgetID);
	if (iLoglevelControlView == nil)
		return;*/

	//InterfacePtr<IDropDownListController> iLoglevelDropDownListController(iLoglevelControlView, UseDefaultIID());//mane
	//if (iLoglevelDropDownListController == nil)
	//	return;
//Commented by nitin
//	IControlView* iChkboxCntrlView=panelControlData->FindWidget(kPropSameAsAssetWidgetID);
//	if(iChkboxCntrlView==nil) 
//		return;
//
//	InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
//	if(itristatecontroldata==nil) 
//		return;
//
//upto here itristatecontroldataMissingFlag


///////21-feb
///06_03_07////
	IControlView* iPropDbVerStaticTextCntrlView=panelControlData->FindWidget(kPropDbVerStaticTextWidgetID);
	if(iPropDbVerStaticTextCntrlView==nil) 
		return;

	IControlView* iPropDbSrvStaticTextCntrlView=panelControlData->FindWidget(kPropDbSrvStaticTextWidgetID);
	if(iPropDbSrvStaticTextCntrlView==nil) 
		return;

	IControlView* iPropDbPortStaticTextCntrlView=panelControlData->FindWidget(kPropDbPortStaticTextWidgetID);
	if(iPropDbPortStaticTextCntrlView==nil) 
		return;

	IControlView* iPropDbUnameStaticTextCntrlView=panelControlData->FindWidget(kPropDbUnameStaticTextWidgetID);
	if(iPropDbUnameStaticTextCntrlView==nil) 
		return;

	IControlView* iPropDbPwordStaticTextCntrlView=panelControlData->FindWidget(kPropDbPwordStaticTextWidgetID);
	if(iPropDbPwordStaticTextCntrlView==nil) 
		return;
//CA("1");
	/*IControlView* iPropSchemaTextEditCntrlView=panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
	if(iPropSchemaTextEditCntrlView==nil) 
		return;*/

	IControlView* iPropEnvTextCntrlView=panelControlData->FindWidget(kPropEnvTextEditWidgetID);
	if(iPropEnvTextCntrlView==nil) 
		return;

	IControlView* iPropSrvURLTextCntrlView=panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
	if(iPropSrvURLTextCntrlView==nil) 
		return;
//mane
	/*IControlView* iImgPathButtonCntrlView=panelControlData->FindWidget(kImgPathButtonWidgetID1);
	if(iImgPathButtonCntrlView==nil) 
		return;*/
	//added by nitin//
//mane
	/*IControlView* iDownloadImgPathButtonCntrlView=panelControlData->FindWidget(kImgPathButtonWidgetID2);
	if(iDownloadImgPathButtonCntrlView==nil) 
		return;*/
	IControlView* iOKButtonCntrlView=panelControlData->FindWidget(kNewOKButtonWidgetID);
	if(iOKButtonCntrlView==nil) 
		return;	

//mane
	/*IControlView* iPropImagePathTextCntrlView=panelControlData->FindWidget(kPropImagePathTextEditWidgetID);
	if(iPropImagePathTextCntrlView==nil) 
		return;*/
	//added by nitin
//mane	//IControlView* iPropDownloadImagePathTextCntrlView=panelControlData->FindWidget(kPropDownloadImagePathTextEditWidgetID);
	//if(iPropDownloadImagePathTextCntrlView==nil) 
	//{
	//	//CA("iPropDownloadImagePathTextCntrlView==nil");
	//	return;
	//}
	//mane
	//IControlView* iPropMaptoRepositoryControlView=panelControlData->FindWidget(kPropMaptoRepositoryWidgetID);
	//if(iPropMaptoRepositoryControlView==nil) 
	//{
	//	//CA("iPropMaptoRepositoryControlView==nil");
	//	return;
	//}
	//IControlView* iPropDownloadImagestoControlView=panelControlData->FindWidget(kPropDownloadImagestoWidgetID);
	//if(iPropDownloadImagestoControlView==nil) 
	//{
	//	//CA("iPropDownloadImagestoControlView==nil");
	//	return;
	//}

	//InterfacePtr<ITriStateControlData> itristatecontroldataMaptoRepository(iPropMaptoRepositoryControlView, UseDefaultIID());//manes
	//if(itristatecontroldataMaptoRepository==nil)
	//{
	//	//CA("itristatecontroldataMaptoRepository==nil");
	//	return;
	//}
	//InterfacePtr<ITriStateControlData> itristatecontroldataDownloadImagesto(iPropDownloadImagestoControlView, UseDefaultIID());//mane
	//if(itristatecontroldataDownloadImagesto==nil)
	//{
	//	//CA("itristatecontroldataDownloadImagesto==nil");
	//	return;
	//}
	//mane
	/*IControlView* iMissingFlagControlView = panelControlData->FindWidget(kPropUseMissingFlagWidgetID);
	if (iLoglevelControlView == nil)
		return;*/

	//InterfacePtr<ITriStateControlData> itristatecontroldataMissingFlag(iMissingFlagControlView, UseDefaultIID()); //mane
	//if(itristatecontroldataMissingFlag==nil)
	//{
	//	//CA("itristatecontroldataMissingFlag==nil");
	//	return;
	//}



	//upto here///////////
	
	IControlView* iPropVersionDropDownCntrlView=panelControlData->FindWidget(kPropVersionDropDownWidgetID);
	if(iPropVersionDropDownCntrlView==nil) 
		return;
	
///06_03_07////
//CA("2");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil){
		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		return ;
	}
//CA("222");
	if(EvnironmentData::getIsEditServerFlag()== TRUE)                // for edit existing server
	{
		//CA("EvnironmentData::getIsEditServerFlag()== TRUE");
		int selectedRow=-1;// = EvnironmentData::getSelectedserver();
		PMString selectedEnv;
		selectedEnv = EvnironmentData::getSelectedServerEnvName();
		LoginInfoValue selectedServerProp;
		
		if(LngHelpObj.getCurrentServerInfo(selectedEnv,selectedServerProp)==-1){
			CAlert::InformationAlert("finding server fail...");
			return;
		}

		/*if(!LngHelpObj.getCurrentServerInfo(selectedServerProp)) mane
			return ;*/

		//added by Tushar on 12_03_07
		if(JDBCEnable == kFalse)
		{
			//CA("JDBCEnable == kFalse");
			IControlView* vendorDropControlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
			if (vendorDropControlView == nil){
				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
				return;
			}
//			if(selectedServerProp.getdbConnectionMode() == 1)//that is invisible mode of jdbc connection
//			{
//				//CA("for jdbc");
//				SetTextControlData(kPropDbVendStaticTextWidgetID, "Database Vendor:");
//				SetTextControlData(kPropDbVerStaticTextWidgetID, "Database Version:");
//				SetTextControlData(kPropDbSrvStaticTextWidgetID, "Database Server:");
//				SetTextControlData(kPropDbPortStaticTextWidgetID, "Database Port:");
//				//SetTextControlData(kPropDbSchemStaticTextWidgetID, "Database Schema:");
//				SetTextControlData(kPropDbUnameStaticTextWidgetID, "Database Userid:");
//				SetTextControlData(kPropDbPwordStaticTextWidgetID, "Database Password:");
//
//				SetTextControlData(kPropEnvTextEditWidgetID, "");
//				SetTextControlData(kPropSrvURLTextEditWidgetID, "");
//				SetTextControlData(kPropSrvTextEditWidgetID, "");
//				SetTextControlData(kPropPrtTextEditWidgetID, "");
//				//SetTextControlData(kPropSchemaTextEditWidgetID, "");
//				SetTextControlData(kPropUnameTextEditWidgetID, "");
//				SetTextControlData(kPropPwordTextEditWidgetID, "");
//				//SetTextControlData(kPropImagePathTextEditWidgetID, "");
//				//added by nitin
//				//SetTextControlData(kPropDownloadImagePathTextEditWidgetID, "");		            //mane
//				//iDropDownListController->Select(-1);
//				//iControlView->Disable();
//
//				iPropEnvTextCntrlView->Disable();
//				iPropSrvURLTextCntrlView->Disable();
//				vendorDropControlView->Disable();
//				disableControls();
//				//iPropImagePathTextCntrlView->Disable(); //mane
//				//iPropSchemaTextEditCntrlView->ShowView();   //manes
//				//iPropSchemaTextEditCntrlView->Disable();
//				//iChkboxCntrlView->Disable(); Commented by nitin
////				iLoglevelDropDownListController->Select(-1);  mane
//				//iLoglevelControlView->Disable();     //mane
//
//				//iImgPathButtonCntrlView->Disable();        //manes
//				iOKButtonCntrlView->Disable();
//				//added by nitin
//				//iDownloadImgPathButtonCntrlView->Disable();//for button //mane
//				//iPropDownloadImagePathTextCntrlView->Disable();//for edit box    //mane       
////				itristatecontroldataMissingFlag->Deselect();  //mane
////				itristatecontroldataMaptoRepository->Deselect(); mane
//				//iPropMaptoRepositoryControlView->Disable();                  //mane
////				itristatecontroldataDownloadImagesto->Deselect();  mane
//				//iPropDownloadImagestoControlView->Disable();   //mane
//				//iMissingFlagControlView->Disable();           //mane
//
//				//iControlView->Disable();				
//				return;
//			}
//			if(selectedServerProp.getdbConnectionMode() == 0)//for http for visibley set
			{
				//CA("for http");
				//iDropDownListController->Select(0);
				//iControlView->Disable();

				iPropEnvTextCntrlView->Enable();
				iPropSrvURLTextCntrlView->Enable();
				vendorDropControlView->Enable();
				iPropVersionDropDownCntrlView->Enable();
				EnableAllTheControls();
				//iPropImagePathTextCntrlView->Enable();
				//iPropSchemaTextEditCntrlView->ShowView();
				//iPropSchemaTextEditCntrlView->Enable();
				//iChkboxCntrlView->Enable(); Commented by nitin
				//iLoglevelControlView->Enable();      //mane
				
				iOKButtonCntrlView->Enable();
				//added by nitin
				//iPropMaptoRepositoryControlView->Enable();         //mane
				//iPropDownloadImagestoControlView->Enable();           //mane
				//Added by nitin////////////
				//if(itristatecontroldataMaptoRepository->IsSelected())mane
				//{
				//	iDownloadImgPathButtonCntrlView->Disable();//for button
				//	iPropDownloadImagePathTextCntrlView->Disable();//for edit box
				//	iPropImagePathTextCntrlView->Enable();
				//	iImgPathButtonCntrlView->Enable();
				//}
				//if(itristatecontroldataDownloadImagesto->IsSelected())
				//{
				//	iDownloadImgPathButtonCntrlView->Enable();//for button
				//	iPropDownloadImagePathTextCntrlView->Enable();//for edit box
				//	iPropImagePathTextCntrlView->Disable();
				//	iImgPathButtonCntrlView->Disable();
				//}
				//iMissingFlagControlView->Enable();
				///upto here

				
				
			}
		}
		//upto here added by Tushar on 12_03_07

		if(JDBCEnable == kTrue)
		{
			//iControlView->Enable();
		}
//CA("333");
		if(1/*selectedServerProp.getdbConnectionMode() == 0*/)  // if Properties are for HTTP mode
		{	
			//CA("Converting From Http to Server");
			//iPropSchemaTextEditCntrlView->ShowView();
			//iPropSchemaTextEditCntrlView->Enable();
			//ISChangeInModewhileEdit1 = kTrue; // commented 23 Feb Vaibhav
			//iDropDownListController->Select(0);
			//iLoglevelDropDownListController->Select(3);
			SetTextControlData(kPropEnvTextEditWidgetID, selectedServerProp.getEnvName());
			SetTextControlData(kPropSrvURLTextEditWidgetID, selectedServerProp.getCmurl());

			SetTextControlData(kPropSrvTextEditWidgetID, "");
			SetTextControlData(kPropPrtTextEditWidgetID, "");
			//SetTextControlData(kPropSchemaTextEditWidgetID, "");
			SetTextControlData(kPropUnameTextEditWidgetID, "");
			SetTextControlData(kPropPwordTextEditWidgetID, "");
			
			//added by Tushar on 06_03_07
			PMString Proxy("Proxy:");
			Proxy.SetTranslatable(kFalse);
			SetTextControlData(kPropDbVendStaticTextWidgetID, Proxy);
			PMString Protocol("Protocol:");
			Protocol.SetTranslatable(kFalse);
			SetTextControlData(kPropDbVerStaticTextWidgetID, Protocol);
			PMString HostServer("Host Server:");
			HostServer.SetTranslatable(kFalse);
			SetTextControlData(kPropDbSrvStaticTextWidgetID, HostServer);
			PMString Port("Port:");
			Port.SetTranslatable(kFalse);
			SetTextControlData(kPropDbPortStaticTextWidgetID, Port);
			//SetTextControlData(kPropDbSchemStaticTextWidgetID, "");
			PMString Userid("Userid:");
			Userid.SetTranslatable(kFalse);
			SetTextControlData(kPropDbUnameStaticTextWidgetID, Userid);
			PMString Password("Password:");
			Password.SetTranslatable(kFalse);
			SetTextControlData(kPropDbPwordStaticTextWidgetID, Password);
			//upto here 

			//CA(" setting doc path in properties "+selectedServerProp.getDocumentPath());
			//CA(" setting image path in properties "+selectedServerProp.getImagePath());
			//SetTextControlData(kPropDocPathTextEditWidgetID , selectedServerProp.getDocumentPath());			
			/////21-feb			
//			iLoglevelDropDownListController->Select(selectedServerProp.getLogLevel());         mane
			//Commented by nitin
			//if(selectedServerProp.getAssetStatus())
			//		itristatecontroldata->Deselect();
			//else
			//		itristatecontroldata->Select();

//			//Added by nitin
//         //mane
//			if(selectedServerProp.getAssetStatus())//add radio button added by nitin
//			{
//				//CA("itristatecontroldataMaptoRepository->Select()");
//				//itristatecontroldata->Select();//commented and changed by nitin
//				itristatecontroldataMaptoRepository->Select();
//				iPropImagePathTextCntrlView->Enable();
//				iImgPathButtonCntrlView->Enable();
//				SetTextControlData(kPropImagePathTextEditWidgetID , selectedServerProp.getImagePath());
//				InterfacePtr<ITextControlData>clearDownloadImagePathText(iPropDownloadImagePathTextCntrlView, UseDefaultIID());
//				if(clearDownloadImagePathText==nil)
//					return;
//				clearDownloadImagePathText->SetString("");
//				iDownloadImgPathButtonCntrlView->Disable();//for button
//				iPropDownloadImagePathTextCntrlView->Disable();
//			}
//			else
//			{
//				//CA("itristatecontroldataDownloadImagesto->Select()");
//				//itristatecontroldata->Deselect();//commented and changed by nitin
//				itristatecontroldataDownloadImagesto->Select();
//				InterfacePtr<ITextControlData>clearImagePathText(iPropImagePathTextCntrlView, UseDefaultIID());
//				if(clearImagePathText==nil)
//					return;
//				iDownloadImgPathButtonCntrlView->Enable();
//				iPropDownloadImagePathTextCntrlView->Enable();
//				SetTextControlData(kPropDownloadImagePathTextEditWidgetID , selectedServerProp.getImagePath());
//				clearImagePathText->SetString("");
//				iPropImagePathTextCntrlView->Disable();
//				iImgPathButtonCntrlView->Disable();				
//			}
//// Missing Flag Check box
//	
//			if(selectedServerProp.getMissingFlag()==1)
//				itristatecontroldataMissingFlag->Select();
//			else
//				itristatecontroldataMissingFlag->Deselect();
//			
//
//			////upto here
//21-feb
			disableControls();
			//iPropSchemaTextEditCntrlView->HideView();mane

			IControlView* controlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
				return;
			}
			//controlView->Disable(kTrue);
			//added by Tushar on 06_03_07
			InterfacePtr<IDropDownListController> iPropVendorDropDownListController(controlView, UseDefaultIID());
			if (iPropVendorDropDownListController == nil)
				return;

			controlView->Enable(kTrue);
			vendorDropListData->Clear(kFalse, kFalse);
			PMString No_Proxy("No_Proxy");
			No_Proxy.SetTranslatable(kFalse);
			vendorDropListData->AddString(No_Proxy);
			PMString Use_Proxy("Use_Proxy");
			Use_Proxy.SetTranslatable(kFalse);
			vendorDropListData->AddString(Use_Proxy);
			iPropVendorDropDownListController->Select(0);
			//upto here

			IControlView* controlView1 = panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
			if (controlView1 == nil){
				CAlert::InformationAlert("InitializeFields port controlView nil");
				return;
			}
			controlView1->Enable();
//CA("selectedServerProp.getVendorName() : "+selectedServerProp.getVendorName());			
			//added by Tushar on 08_03_07
			if(selectedServerProp.getVendorName() == "Use_Proxy")
			{
				EnableAllTheControls();
				controlView->Enable(kTrue);
				iPropVendorDropDownListController->Select(1);
				
				IControlView* propVersionDropDownControlView = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
				if (propVersionDropDownControlView == nil){
					CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
					return;
				}
				propVersionDropDownControlView->Enable();
				
				InterfacePtr<IDropDownListController> iPropVersionDropDownListController(propVersionDropDownControlView, UseDefaultIID());
				if (iPropVendorDropDownListController == nil)
					return;
				
				versionDropListData->Clear(kFalse, kFalse);
				PMString HTTP("HTTP");
				HTTP.SetTranslatable(kFalse);
				versionDropListData->AddString(HTTP);
				PMString HTTPS("HTTPS");
				HTTPS.SetTranslatable(kFalse);
				versionDropListData->AddString(HTTPS);
				//versionDropListData->AddString("SOCKS");
				
				if(selectedServerProp.getVersion() == "HTTP")
					iPropVersionDropDownListController->Select(0);
				if(selectedServerProp.getVersion() == "HTTPS")
					iPropVersionDropDownListController->Select(1);
				//if(selectedServerProp.getVersion() == "SOCKS")
					//iPropVersionDropDownListController->Select(2);

				PMString ServerPort = "";
				ServerPort.AppendNumber(selectedServerProp.getServerPort());
				ServerPort.SetTranslatable(kFalse);
				SetTextControlData(kPropSrvTextEditWidgetID, selectedServerProp.getServerName());
				SetTextControlData(kPropPrtTextEditWidgetID, ServerPort);
				SetTextControlData(kPropUnameTextEditWidgetID, selectedServerProp.getDbUserName());
				SetTextControlData(kPropPwordTextEditWidgetID, selectedServerProp.getDbPassword());
			}
			//upto here added by Tushar on 08_03_07
//			ptrIAppFramework->setSelectedConnMode(0); // for HTTP	
		}
//		else if(selectedServerProp.getdbConnectionMode() == 1) // if Properties are for JDBC mode
//		{
//			//CA("selectedServerProp.getdbConnectionMode() == 1 ,if Properties are for JDBC mode");
//			//iPropSchemaTextEditCntrlView->ShowView();//added by tushar on 07_03_07    mane
//			//iPropSchemaTextEditCntrlView->Enable();//added by tushar on 07_03_07       mane
//
//			ISChangeInModewhileEdit1 = kFalse;
//			long port =0;
//			port=selectedServerProp.getServerPort();		
//			PMString portStr("");
//			if(port>0)
//				portStr.AppendNumber(port);
//
//			SetTextControlData(kPropEnvTextEditWidgetID, selectedServerProp.getEnvName());
//			SetTextControlData(kPropSrvTextEditWidgetID, selectedServerProp.getServerName());
//			SetTextControlData(kPropSrvURLTextEditWidgetID, "");
//			SetTextControlData(kPropPrtTextEditWidgetID, portStr);
//			SetTextControlData(kPropSchemaTextEditWidgetID, selectedServerProp.getSchema());
//			SetTextControlData(kPropUnameTextEditWidgetID, selectedServerProp.getUserName());
//			SetTextControlData(kPropPwordTextEditWidgetID, selectedServerProp.getPassword());
//			//CA(" setting doc path in properties "+selectedServerProp.getDocumentPath());
//			//CA(" setting image path in properties "+selectedServerProp.getImagePath());
//			//SetTextControlData(kPropDocPathTextEditWidgetID , selectedServerProp.getDocumentPath());
//			//SetTextControlData(kPropImagePathTextEditWidgetID , selectedServerProp.getImagePath()); //mane
//			//added by nitin
//			//SetTextControlData(kPropDownloadImagePathTextEditWidgetID , selectedServerProp.getImagePath());//mane
//
//			//added by Tushar on 06_03_07
//			SetTextControlData(kPropDbVendStaticTextWidgetID, "Database Vendor:");
//			SetTextControlData(kPropDbVerStaticTextWidgetID, "Database Version:");
//			SetTextControlData(kPropDbSrvStaticTextWidgetID, "Database Server:");
//			SetTextControlData(kPropDbPortStaticTextWidgetID, "Database Port:");
//			SetTextControlData(kPropDbSchemStaticTextWidgetID, "Database Schema:");
//			SetTextControlData(kPropDbUnameStaticTextWidgetID, "Database Userid:");
//			SetTextControlData(kPropDbPwordStaticTextWidgetID, "Database Password:");
//			//upto here 
////21-feb
////			iLoglevelDropDownListController->Select(selectedServerProp.getLogLevel());mane
//			//Commented by nitin
//			//if(selectedServerProp.getAssetStatus())
//			//	 itristatecontroldata->Select();
//			//else
//			//	 itristatecontroldata->Deselect();
//			//Upto here
//
//			//Change by nitin
//			//mane
//			//if(selectedServerProp.getAssetStatus())
//			//{
//			//	itristatecontroldataMaptoRepository->Select();
//			//	iPropImagePathTextCntrlView->Enable();
//			//	iImgPathButtonCntrlView->Enable();
//			//	SetTextControlData(kPropImagePathTextEditWidgetID , selectedServerProp.getImagePath());
//			//	InterfacePtr<ITextControlData>clearDownloadImagePathText(iPropDownloadImagePathTextCntrlView, UseDefaultIID());
//			//	if(clearDownloadImagePathText==nil)
//			//		return;
//			//	clearDownloadImagePathText->SetString("");
//			//	iDownloadImgPathButtonCntrlView->Disable();//for button
//			//	iPropDownloadImagePathTextCntrlView->Disable();
//			//	
//			//}
//			//else
//			//{
//			//	itristatecontroldataDownloadImagesto->Select();
//			//	iPropDownloadImagePathTextCntrlView->Enable();
//			//	iDownloadImgPathButtonCntrlView->Enable();
//			//	SetTextControlData(kPropDownloadImagePathTextEditWidgetID , selectedServerProp.getImagePath());
//			//	InterfacePtr<ITextControlData>clearImagePathText(iPropImagePathTextCntrlView, UseDefaultIID());
//			//	if(clearImagePathText==nil)
//			//		return;
//			//	clearImagePathText->SetString("");
//			//	iPropImagePathTextCntrlView->Disable();
//			//	iImgPathButtonCntrlView->Disable();				
//			//}
//
//			//// Missing Flag Check box
//	
//			//if(selectedServerProp.getMissingFlag()==1)       mane
//			//	itristatecontroldataMissingFlag->Select();
//			//else
//			//	itristatecontroldataMissingFlag->Deselect();
//
//		//upto here
////21-feb
//			EnableAllTheControls();
//			
//			IControlView* controlView1 = panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
//			if (controlView1 == nil){
//				CAlert::InformationAlert("InitializeFields port controlView nil");
//				return;
//			}
//			controlView1->Disable(kTrue);
//
//			IControlView* controlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
//			if (controlView == nil){
//				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
//				return;
//			}
//			controlView->Enable();	
//
//			IControlView* controlView2 = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
//			if (controlView2 == nil){
//				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
//				return;
//			}
//			controlView2->Enable();	
//			
//
//			PMString vendorName(selectedServerProp.getVendorName());
//			
//			VectorVendorInfoPtr=LngHelpObj.getVendors();
//
//			if(VectorVendorInfoPtr==NULL){
//				CAlert::InformationAlert("DBproviders file not found/no entries");	
//				return;
//			}
//
//			VectorVendorInfoValue::iterator it;
//
//			for(it = VectorVendorInfoPtr->begin(); it != VectorVendorInfoPtr->end(); it++)
//			{			
//				// Put the 'None' entry as the first in both lists. we'll eventually make it the default
//				PMString scratchName(it->getVendorName());
//				if(vendorDropListData)
//					vendorDropListData->AddString(scratchName, i, kFalse, kFalse);
//				i++;
//			}
//
//			for(it = VectorVendorInfoPtr->begin(); it != VectorVendorInfoPtr->end(); it++)
//			{
//				selectedRow++;
//				PMString CurrVendorName(it->getVendorName());
//				int32 match = vendorName.Compare(TRUE,CurrVendorName);
//				if(match==0)
//				{
//					//CAlert::InformationAlert("Match");
//					InterfacePtr<IDropDownListController> vendorDropListController(vendorDropListData, UseDefaultIID());
//					if (vendorDropListController == nil){
//						CAlert::InformationAlert("vendorDropListController nil");
//						break;
//					}
//					vendorDropListController->Select(selectedRow);
//					vector<PMString> versions;
//					vector<PMString>::iterator versionItrator;
//					versions =it->getVendorVersion();
//					int j=0;
//					for(versionItrator = versions.begin(); versionItrator != versions.end(); versionItrator++)
//					{
//						if(versionDropListData)
//						{   PMString CurrVersionIterr(*versionItrator);
//							versionDropListData->AddString(CurrVersionIterr, j, kFalse, kFalse);
//						}
//						j++;
//					}
//
//					PMString versionName(selectedServerProp.getVersion());
//					int selectedVersionRow=-1;
//					for(versionItrator = versions.begin(); versionItrator != versions.end(); versionItrator++)
//					{
//						PMString CurrVersionIter(*versionItrator);
//						int32 isVersionMatch = versionName.Compare(TRUE,CurrVersionIter);
//						selectedVersionRow++;
//						if(isVersionMatch == 0)
//						{
//							//CAlert::InformationAlert("isVersionMatch");
//							InterfacePtr<IDropDownListController> versionDropListController(versionDropListData, UseDefaultIID());
//							if (versionDropListController == nil){
//								CAlert::InformationAlert("versionDropListController nil");
//								break;
//							}
//							versionDropListController->Select(selectedVersionRow);
//						}					
//					}
//				}
//			}		
//
//			//iDropDownListController->Select(1);		
//			ptrIAppFramework->setSelectedConnMode(1); // for SERVER	
//			DisableControlsDependOnServer(vendorName);
//		}
	}
	else
	{ 
		//if(EvnironmentData::setIsEditServerFlag()== FALSE) // for create new server
		
		//added by tushar on 12_03_07
		iPropEnvTextCntrlView->Enable(); 
		//iPropImagePathTextCntrlView->Enable();mane
		//iImgPathButtonCntrlView->Enable();*/
		iOKButtonCntrlView->Enable();
		//iChkboxCntrlView->Enable(); commented by nitin
		//iPropMaptoRepositoryControlView->Enable();//Added by nitin    mane
		//iPropDownloadImagestoControlView->Enable();//added by nitin
		//iLoglevelControlView->Enable();
		//iPropSchemaTextEditCntrlView->ShowView();//added by tushar on 07_03_07
		//iPropSchemaTextEditCntrlView->Enable();//added by tushar on 07_03_07 mane

//		iDownloadImgPathButtonCntrlView->Disable();//for button added by nitin   //mane
//		iPropDownloadImagePathTextCntrlView->Disable();//for edit box added by nitin      mane
//		iMissingFlagControlView->Enable();  //mane
		ISChangeInModewhileEdit1 = kFalse;		
//21-feb
//		iLoglevelDropDownListController->Select(0);     mane
		//itristatecontroldata->Select(1);//Commented by nitin
//		itristatecontroldataMaptoRepository->Select(1);//added by nitin          mane
//21-feb		
		SetTextControlData(kPropEnvTextEditWidgetID, "");
		SetTextControlData(kPropSrvTextEditWidgetID, "");
		SetTextControlData(kPropSrvURLTextEditWidgetID, "");
		SetTextControlData(kPropPrtTextEditWidgetID, "");
		SetTextControlData(kPropSchemaTextEditWidgetID, "");
		SetTextControlData(kPropUnameTextEditWidgetID, "");
		SetTextControlData(kPropPwordTextEditWidgetID, "");	
	//	SetTextControlData(kPropDocPathTextEditWidgetID , "");
		//SetTextControlData(kPropImagePathTextEditWidgetID , "");//mane
		//SetTextControlData(kPropDownloadImagePathTextEditWidgetID , "");//added by nitin mane		

		//added by Tushar on 07_03_07
		PMString Proxy("Proxy:");
		Proxy.SetTranslatable(kFalse);
		SetTextControlData(kPropDbVendStaticTextWidgetID, Proxy);
		PMString Protocol("Protocol:");
		Protocol.SetTranslatable(kFalse);
		SetTextControlData(kPropDbVerStaticTextWidgetID, Protocol);
		PMString HostServer("Host Server:");
		HostServer.SetTranslatable(kFalse);
		SetTextControlData(kPropDbSrvStaticTextWidgetID, HostServer);
		PMString Port("Port:");
		Port.SetTranslatable(kFalse);
		SetTextControlData(kPropDbPortStaticTextWidgetID, Port);
		//SetTextControlData(kPropDbSchemStaticTextWidgetID, "");
		PMString Userid("Userid:");
		Userid.SetTranslatable(kFalse);
		SetTextControlData(kPropDbUnameStaticTextWidgetID, Userid);
		PMString Password("Password:");
		Password.SetTranslatable(kFalse);
		SetTextControlData(kPropDbPwordStaticTextWidgetID, Password);
		//upto here 
//		itristatecontroldataMissingFlag->Deselect();//added by nitin mane

		disableControls();
		//iPropSchemaTextEditCntrlView->HideView();//added by tushar on 07_03_07        mane
		
		if( EvnironmentData::SelctedConnMode== 0)//for http
		{
			IControlView* controlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
				return;
			}
			//controlView->Disable(kTrue);	
			
			//added by Tushar on 06_03_07
			InterfacePtr<IDropDownListController> iPropVendorDropDownListController(controlView, UseDefaultIID());
			if (iPropVendorDropDownListController == nil)
				return;

			controlView->Enable(kTrue);
			vendorDropListData->Clear(kFalse, kFalse);
			PMString No_Proxy("No_Proxy");
			No_Proxy.SetTranslatable(kFalse);
			vendorDropListData->AddString(No_Proxy);
			PMString Use_Proxy("Use_Proxy");
			Use_Proxy.SetTranslatable(kFalse);
			vendorDropListData->AddString(Use_Proxy);
			iPropVendorDropDownListController->Select(0);
			//upto here
//			itristatecontroldataMaptoRepository->Select();//added by nitin         mane

			IControlView* controlView1 = panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
			if (controlView1 == nil){
				CAlert::InformationAlert("InitializeFields port controlView nil");
				return;
			}
			controlView1->Enable();
			//iDropDownListController->Select(0);
//			ptrIAppFramework->setSelectedConnMode(0); // for HTTP	

		}
		//else if(EvnironmentData::SelctedConnMode== 1)
		//{
		//	IControlView* controlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
		//	if (controlView == nil){
		//		CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
		//		return;
		//	}
		//	controlView->Enable();	

		//	IControlView* controlView1 = panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
		//	if (controlView1 == nil){
		//		CAlert::InformationAlert("InitializeFields port controlView nil");
		//		return;
		//	}
		//	controlView1->Disable(kTrue);;

		//	VectorVendorInfoPtr=LngHelpObj.getVendors();
		//	if(VectorVendorInfoPtr==NULL){
		//		CAlert::InformationAlert("DBproviders file not found/no entries");	
		//		return;
		//	}

		//	VectorVendorInfoValue::iterator it;

		//	for(it = VectorVendorInfoPtr->begin(); it != VectorVendorInfoPtr->end(); it++)
		//	{			
		//		// Put the 'None' entry as the first in both lists. we'll eventually make it the default
		//		PMString scratchName(it->getVendorName());
		//		if(vendorDropListData)
		//			vendorDropListData->AddString(scratchName, i, kFalse, kFalse);
		//		i++;
		//	}

		//	//iDropDownListController->Select(1);
		//	ptrIAppFramework->setSelectedConnMode(1); // for SERVER	
		//}
	}
	//if(VectorVendorInfoPtr != nil)
	//	delete VectorVendorInfoPtr;
	
}

/* ValidateFields
*/
WidgetID LNG3DialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	
	bool8 isEnable = TRUE;	
	IControlView* controlView=NULL;
	//	CAlert::InformationAlert("ValidateFields...");
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		//CAlert::InformationAlert("ValidateFields panelControlData nil");
		return result;
	}
	
	do{
			//added by nitin//from here to
		//mane
			//IControlView* iPropMaptoRepositoryControlView=panelControlData->FindWidget(kPropMaptoRepositoryWidgetID);
			//if(iPropMaptoRepositoryControlView==nil) 
			//{
			//	//CA("iPropMaptoRepositoryControlView==nil");
			//	break;
			//}
			//IControlView* iPropDownloadImagestoControlView=panelControlData->FindWidget(kPropDownloadImagestoWidgetID);
			//if(iPropDownloadImagestoControlView==nil) 
			//{
			//	//CA("iPropDownloadImagestoControlView==nil");
			//	break;
			//}
			//	InterfacePtr<ITriStateControlData> itristatecontroldataMaptoRepository(iPropMaptoRepositoryControlView, UseDefaultIID());
			//if(itristatecontroldataMaptoRepository==nil)
			//{
			//	//CA("itristatecontroldataMaptoRepository==nil");
			//	break;
			//}
			//InterfacePtr<ITriStateControlData> itristatecontroldataDownloadImagesto(iPropDownloadImagestoControlView, UseDefaultIID());
			//if(itristatecontroldataDownloadImagesto==nil)
			//{
			//	//CA("itristatecontroldataDownloadImagesto==nil");
			//	break;
			//}
		
		/////////upto here/////////////
		//------------------------- server mode setting starts here---------------------------------------//
		if(EvnironmentData::SelctedConnMode == 1)
		{	
			//CA("Server connection");
			PMString editFieldString;
			editFieldString = GetTextControlData(kPropEnvTextEditWidgetID);	
			if(editFieldString == "")
				return kPropEnvTextEditWidgetID;

			editFieldString = getSelectedVendor();
			if(editFieldString=="")
				return kPropVendorDropDownWidgetID;
	
			editFieldString = getSelectedVersion();
			if(editFieldString=="")
				return kPropVersionDropDownWidgetID;

			controlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("ValidateFields DBServer ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString = GetTextControlData(kPropSrvTextEditWidgetID);	
				if(editFieldString=="")
					return kPropSrvTextEditWidgetID;			
			}
		
			controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("ValidateFields port ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropPrtTextEditWidgetID);	
				if(editFieldString==""){				
					SetTextControlData(kPropPrtTextEditWidgetID,editFieldString);	
					return kPropPrtTextEditWidgetID;			
				}
			}
			//mane
			//controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
			//if (controlView == nil){
			//	//CAlert::InformationAlert("ValidateFields Schema ControlView nil");
			//	break;
			//}
			//isEnable=controlView->IsEnabled();
			//if(isEnable ==TRUE){
			//	editFieldString="\0";
			//	editFieldString=GetTextControlData(kPropSchemaTextEditWidgetID);	
			//	if(editFieldString=="")
			//		return kPropSchemaTextEditWidgetID;
			//	
			//}

			controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("ValidateFields Username ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropUnameTextEditWidgetID);	
				if(editFieldString=="")
					return kPropUnameTextEditWidgetID;			
			}

			controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("ValidateFields passeord ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropPwordTextEditWidgetID);	
				if(editFieldString=="")
					return kPropPwordTextEditWidgetID;
			}
			
			//mane
			//controlView = panelControlData->FindWidget(kPropImagePathTextEditWidgetID);
			//if (controlView == nil){
			//	//CAlert::InformationAlert("ValidateFields Username ControlView nil");
			//	break;
			//}
			//isEnable=controlView->IsEnabled();
			//if(isEnable ==TRUE){
			//	editFieldString="\0";
			//	editFieldString=GetTextControlData(kPropImagePathTextEditWidgetID);	
			//	if(editFieldString=="")
			//		return kPropImagePathTextEditWidgetID;			
			//}
			///////////added by nitin from here to/////////
			//controlView = panelControlData->FindWidget(kPropDownloadImagePathTextEditWidgetID);
			//if (controlView == nil){
			//	//CAlert::InformationAlert("ValidateFields Username ControlView nil");
			//	break;
			//}
			//isEnable=controlView->IsEnabled();
			//if(isEnable ==TRUE){
			//	editFieldString="\0";
			//	editFieldString=GetTextControlData(kPropDownloadImagePathTextEditWidgetID);	
			//	if(editFieldString=="")
			//		return kPropDownloadImagePathTextEditWidgetID;			
			//}
			////upto here

			/*controlView = panelControlData->FindWidget(kPropDocPathTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("ValidateFields Username ControlView nil");
				break;
			}*/
			/*isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropDocPathTextEditWidgetID);	
				if(editFieldString=="")
					return kPropDocPathTextEditWidgetID;			
			}*/

		}
		//-----------------------------------server mode setting ends here ------------------------------//
		else if(EvnironmentData::SelctedConnMode == 0)
		{	
			PMString editFieldString;
			editFieldString=GetTextControlData(kPropEnvTextEditWidgetID);	
			if(editFieldString=="")
				return kPropEnvTextEditWidgetID;

			controlView = panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("ValidateFields ServerURL ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropSrvURLTextEditWidgetID);	
				if(editFieldString=="")
					return kPropSrvURLTextEditWidgetID;			
			}
			
//mane
			//controlView = panelControlData->FindWidget(kPropImagePathTextEditWidgetID);
			//if (controlView == nil){
			//	//CAlert::InformationAlert("ValidateFields Username ControlView nil");
			//	break;
			//}
			//isEnable=controlView->IsEnabled();
			//if(isEnable ==TRUE){
			//	editFieldString="\0";
			//	editFieldString=GetTextControlData(kPropImagePathTextEditWidgetID);	
			//	if(editFieldString=="")
			//		return kPropImagePathTextEditWidgetID;			
			//}
			/////////////added by nitin///////////////////////
			//controlView = panelControlData->FindWidget(kPropDownloadImagePathTextEditWidgetID);
			//if (controlView == nil){
			//	//CAlert::InformationAlert("ValidateFields Username ControlView nil");
			//	break;
			//}
			//isEnable=controlView->IsEnabled();
			//if(isEnable ==TRUE){
			//	editFieldString="\0";
			//	editFieldString=GetTextControlData(kPropDownloadImagePathTextEditWidgetID);	
			//	if(editFieldString=="")
			//		return kPropDownloadImagePathTextEditWidgetID;			
			//}
			//upto here//////

			controlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("ValidateFields Username ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropVendorDropDownWidgetID);	
				if(editFieldString=="")
				{
					//return kPropImagePathTextEditWidgetID;//Changed by nitin
					/*if(itristatecontroldataMaptoRepository->IsSelected())   mane
						return kPropImagePathTextEditWidgetID;
					else 
						return kPropDownloadImagePathTextEditWidgetID;*/
				}
			}

			controlView = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("ValidateFields Userid ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropVersionDropDownWidgetID);
				if(editFieldString=="")
				{
					//return kPropImagePathTextEditWidgetID;//Changed by nitin
					/*if(itristatecontroldataMaptoRepository->IsSelected())     mane    
						return kPropImagePathTextEditWidgetID;
					else
						return kPropDownloadImagePathTextEditWidgetID;*/
				}
			}

			controlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("ValidateFields Username ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropSrvTextEditWidgetID);	
				if(editFieldString=="")
				{
					//return kPropImagePathTextEditWidgetID;//Changed by nitin
					/*if(itristatecontroldataMaptoRepository->IsSelected()) mane
						return kPropImagePathTextEditWidgetID;
					else
						return kPropDownloadImagePathTextEditWidgetID;*/


				}
			}

			controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("ValidateFields Username ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropPrtTextEditWidgetID);	
				if(editFieldString=="")
				{
					//return kPropImagePathTextEditWidgetID;//Changed by nitin
					//if(itristatecontroldataMaptoRepository->IsSelected())//mane
					//	return kPropImagePathTextEditWidgetID;
					//else
					//	return kPropDownloadImagePathTextEditWidgetID;
				}
			}
//mane
			//controlView = panelControlData->FindWidget(kPropImagePathTextEditWidgetID);
			//if (controlView == nil){
			//	//CAlert::InformationAlert("ValidateFields Username ControlView nil");
			//	break;
			//}
			//isEnable=controlView->IsEnabled();
			//if(isEnable ==TRUE){
			//	editFieldString="\0";
			//	editFieldString=GetTextControlData(kPropImagePathTextEditWidgetID);	
			//	if(editFieldString=="")
			//		return kPropImagePathTextEditWidgetID;			
			//}
			//added by nitin//from here to//
			//mane
			/*controlView = panelControlData->FindWidget(kPropDownloadImagePathTextEditWidgetID);
			if (controlView == nil){
				///CAlert::InformationAlert("ValidateFields Username ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropDownloadImagePathTextEditWidgetID);	
				if(editFieldString=="")
					return kPropDownloadImagePathTextEditWidgetID;			
			}*/
			//upto here////

			/*controlView = panelControlData->FindWidget(kPropDocPathTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("ValidateFields Username ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable ==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropDocPathTextEditWidgetID);	
				if(editFieldString=="")
					return kPropDocPathTextEditWidgetID;			
			}*/
		}
	}while(0);
	
	return result;	
}

/* ApplyFields
*/
void LNG3DialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetID)
{
	// Replace with code that gathers widget values and applies them.
	//CAlert::InformationAlert("LNG3DialogController::ApplyDialogFields");

	bool8 isEnable=TRUE;
	IControlView* controlView=NULL;
	LoginInfoValue serverProperties;
	PMString envName("");
	PMString editFieldString("");
	PMString defaultString("");
	double clientID = -1;
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("ApplyFields panelControlData nil");
		return;
	}
////////////////21-feb
	//manes
//	IControlView* iLoglevelControlView = panelControlData->FindWidget(kPropLogLevelDropDownWidgetID);
//	if (iLoglevelControlView == nil)
//		return;

	/*InterfacePtr<IDropDownListController> iLoglevelDropDownListController(iLoglevelControlView, UseDefaultIID());mane
	if (iLoglevelDropDownListController == nil)
		return;*/
	//commented by nitin
	//	IControlView* iChkboxCntrlView=panelControlData->FindWidget(kPropSameAsAssetWidgetID);
	//	if(iChkboxCntrlView==nil) 
	//		return;
	//
	//	InterfacePtr<ITriStateControlData> itristatecontroldata(iChkboxCntrlView, UseDefaultIID());
	//	if(itristatecontroldata==nil) 
	//		return;
	//upto here 

	//Added by nitin from here to//////////////
	//mane
	//IControlView* iPropMaptoRepositoryControlView=panelControlData->FindWidget(kPropMaptoRepositoryWidgetID);
	//if(iPropMaptoRepositoryControlView==nil) 
	//{
	//	//CA("iPropMaptoRepositoryControlView==nil");
	//	return;
	//}
	//IControlView* iPropDownloadImagestoControlView=panelControlData->FindWidget(kPropDownloadImagestoWidgetID);
	//if(iPropDownloadImagestoControlView==nil) 
	//{
	//	//CA("iPropDownloadImagestoControlView==nil");
	//	return;
	////}
	//	InterfacePtr<ITriStateControlData> itristatecontroldataMaptoRepository(iPropMaptoRepositoryControlView, UseDefaultIID());mane
	//if(itristatecontroldataMaptoRepository==nil)
	//{
	//	//CA("itristatecontroldataMaptoRepository==nil");
	//	return;
	//}
	//InterfacePtr<ITriStateControlData> itristatecontroldataDownloadImagesto(iPropDownloadImagestoControlView, UseDefaultIID());manes
	//if(itristatecontroldataDownloadImagesto==nil)
	//{
	//	//CA("itristatecontroldataDownloadImagesto==nil");
	//	return;
	//}
	//mane
	//IControlView* iMissingFlagControlView=panelControlData->FindWidget(kPropUseMissingFlagWidgetID);
	//if(iMissingFlagControlView==nil) 
	//	return;

	//InterfacePtr<ITriStateControlData> itristatecontroldataMissingFlag(iMissingFlagControlView, UseDefaultIID());
	//if(itristatecontroldataMissingFlag==nil)
	//{
	//	//CA("itristatecontroldataMissingFlag==nil");
	//	return;
	//}
	///////upto here////////////////////////////

///////////////21-feb


	do{		
		if(EvnironmentData::SelctedConnMode == 1)
		{
			//******************JDBC PROPERTIES SETTING STARTS HERE --  SAVED IN LoginInfoValue*****//
			//CA("JDBC");
			//------------- IMAGE PATH SET HERE--------------------------------------------//
			//PMString testStringImagePath;
			//testStringImagePath = GetTextControlData(kPropImagePathTextEditWidgetID);//mane
			////if(testStringImagePath == "") testStringImagePath = "NOT--SET";
			////CA(" IMAGE PATH IS  " + testStringImagePath);
			//serverProperties.setImagePath(testStringImagePath);

			//----------------DOCUMENT PATH SET HERE  kPropDocPathTextEditWidgetID ------------------//
			//PMString testStringDocPath;
			//testStringDocPath = GetTextControlData(kPropDocPathTextEditWidgetID);
			////if(testStringDocPath == "") testStringDocPath = "NOT--SET";
			////CA(" DOC PATH IS  " + testStringDocPath);
			//serverProperties.setDocumentPath(testStringDocPath);

			//---------------------ENVIRONMENT NAME SET HERE-------------------------------//
			envName = GetTextControlData(kPropEnvTextEditWidgetID);
			serverProperties.setEnvName(envName);
			//serverProperties.setdbConnectionMode(1);

			//------------------- VENDOR NAME SET HERE------------------------------------//
			editFieldString = getSelectedVendor();
			serverProperties.setVendorName(editFieldString);
			
			//------------------- DATABASE VERSION NUMBER SET HERE-----------------------//
			editFieldString="\0";
			editFieldString=getSelectedVersion();
			serverProperties.setVersion(editFieldString);
		
			//--------------------- set server name -------------------------------------//
			controlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields ServerName nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable==TRUE){
				editFieldString="\0";
				editFieldString.Clear();
				editFieldString=GetTextControlData(kPropSrvTextEditWidgetID);
				serverProperties.setServerName(editFieldString);
			}
			else{			
				serverProperties.setServerName(defaultString);
			}

			//-------------set server poart -------------------------------------//
			controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields ServerPort ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable==TRUE){
				//editFieldString="";
				editFieldString.Clear();
				editFieldString=GetTextControlData(kPropPrtTextEditWidgetID);
				serverProperties.setServerPort(editFieldString.GetAsNumber());
			}
			else serverProperties.setServerPort(0);

			//---------------------- set schema ----------------------------------------//
			//mane
			/*controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields Schema ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropSchemaTextEditWidgetID);
				serverProperties.setSchema(editFieldString);
			}
			else serverProperties.setSchema(defaultString);*/

			//---------------- set user name -------------------------------------------//
			controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("InitializeFields setUserName ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropUnameTextEditWidgetID);
				serverProperties.setDbUserName(editFieldString);
			}
			else serverProperties.setDbUserName(defaultString);

			//----------------- set password -----------------------------------------------//
			controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("InitializeFields setPassword ControlView nil");
				break;
			}
			isEnable=controlView->IsEnabled();
			if(isEnable==TRUE){
				editFieldString="\0";
				editFieldString=GetTextControlData(kPropPwordTextEditWidgetID);
				serverProperties.setPassword(editFieldString);	
			}
			else serverProperties.setPassword(defaultString);

			//********************************JDBC POPERTIES SETTINGS ENDS HHERE******************//
		}
		else if(EvnironmentData::SelctedConnMode ==0 )
		{  
			//CA("Http Selected");
			envName=GetTextControlData(kPropEnvTextEditWidgetID);
			serverProperties.setEnvName(envName);

			PMString ServerURL = GetTextControlData(kPropSrvURLTextEditWidgetID);
			serverProperties.setCmurl(ServerURL);

			//------------------- VENDOR NAME SET HERE------------------------------------//
			editFieldString = getSelectedVendor();
			serverProperties.setVendorName(editFieldString);
			
			//CA("editFieldString = " + editFieldString);
			if(editFieldString == "Use_Proxy")
			{
				//CA("inside Use_Proxy");
				//------------------- DATABASE VERSION NUMBER SET HERE-----------------------//
				editFieldString="\0";
				editFieldString=getSelectedVersion();
				serverProperties.setVersion(editFieldString);

				//CA("setVersion  = " + editFieldString);
				//--------------------- set server name -------------------------------------//
				controlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
				if (controlView == nil){
					CAlert::InformationAlert("InitializeFields ServerName nil");
					break;
				}
				isEnable=controlView->IsEnabled();
				if(isEnable==TRUE){
					editFieldString="\0";
					editFieldString.Clear();
					editFieldString=GetTextControlData(kPropSrvTextEditWidgetID);
					serverProperties.setServerName(editFieldString);
					//CA("setServerName  = " + editFieldString);
				}
				else{			
					serverProperties.setServerName(defaultString);
					//CA("setServerName  = " + editFieldString);
				}
				
				//-------------set server port -------------------------------------//
				controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
				if (controlView == nil){
					//CAlert::InformationAlert("InitializeFields ServerPort ControlView nil");
					break;
				}
				isEnable=controlView->IsEnabled();
				if(isEnable==TRUE){
					//editFieldString="";
					editFieldString.Clear();
					editFieldString=GetTextControlData(kPropPrtTextEditWidgetID);
					serverProperties.setServerPort(editFieldString.GetAsNumber());
					//CA("setServerPort  = " + editFieldString);
				}
				else serverProperties.setServerPort(0);

				//---------------- set user name -------------------------------------------//
				controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
				if (controlView == nil){
					//CAlert::InformationAlert("InitializeFields setUserName ControlView nil");
					break;
				}
				isEnable=controlView->IsEnabled();
				if(isEnable==TRUE){
					editFieldString="\0";
					editFieldString=GetTextControlData(kPropUnameTextEditWidgetID);
					serverProperties.setDbUserName(editFieldString);
					//CA("setUserName  = " + editFieldString);
				}
				else serverProperties.setDbUserName(defaultString);

				//----------------- set password -----------------------------------------------//
				controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
				if (controlView == nil){
					//CAlert::InformationAlert("InitializeFields setPassword ControlView nil");
					break;
				}
				isEnable=controlView->IsEnabled();
				if(isEnable==TRUE){
					editFieldString="\0";
					editFieldString=GetTextControlData(kPropPwordTextEditWidgetID);
					serverProperties.setDbPassword(editFieldString);	
				}
				else serverProperties.setDbPassword(defaultString);
			}
			//serverProperties.setdbConnectionMode(0);
		}
					
	}while(0);

	/////added and updated by nitin from here to
	/*IControlView* iPropImagePathTextCntrlView=panelControlData->FindWidget(kPropImagePathTextEditWidgetID);//mane
	if(iPropImagePathTextCntrlView==nil) 
		return;	*/		
	//IControlView* iPropDownloadImagePathTextCntrlView=panelControlData->FindWidget(kPropDownloadImagePathTextEditWidgetID);
	//if(iPropDownloadImagePathTextCntrlView==nil) 
	//{
	//	//CA("iPropDownloadImagePathTextCntrlView==nil");
	//	return;
	//}	
	///*IControlView* iImgPathButtonCntrlView=panelControlData->FindWidget(kImgPathButtonWidgetID1);//mane
	//if(iImgPathButtonCntrlView==nil) 
	//	return;*/
	//IControlView* iDownloadImgPathButtonCntrlView=panelControlData->FindWidget(kImgPathButtonWidgetID2);
	//if(iDownloadImgPathButtonCntrlView==nil) 
	//	return;
	/////////upto here////////////////////////////
	//
	//editFieldString.Clear();
	//if(itristatecontroldataMaptoRepository->IsSelected())
	//{
	//	serverProperties.setAssetStatus(1);
	//	iPropImagePathTextCntrlView->Enable();
	//	iImgPathButtonCntrlView->Enable();
	//	editFieldString = GetTextControlData(kPropImagePathTextEditWidgetID);
	//	if(editFieldString == "") 
	//		editFieldString = "";
	//}
	//else	
	//{
	//	serverProperties.setAssetStatus(0);
	//	iPropDownloadImagePathTextCntrlView->Enable();
	//	iDownloadImgPathButtonCntrlView->Enable();
	//	editFieldString = GetTextControlData(kPropDownloadImagePathTextEditWidgetID);
	//	if(editFieldString == "") 
	//		editFieldString = "";
	//}
	////PMString s("editFieldString : ");
	////s.Append(editFieldString);
	////CA(s);
	//serverProperties.setImagePath(editFieldString);  //  set imagePath		
	//upto here/////////////mane

		//CA(" IMAGE PATH " + serverProperties.getImagePath());

	editFieldString.Clear();
	///*editFieldString = GetTextControlData(kPropDocPathTextEditWidgetID);
	//if(editFieldString == "") editFieldString = "NOT-SET";
	//serverProperties.setDocumentPath(editFieldString);*/ // set Document Path
	//CA(" DOC PATH " + serverProperties.getDocumentPath());
    	
	// end vaibhav15 Feb

//////////////21-feb
	/*int32 loglevel = iLoglevelDropDownListController->GetSelected();mane
	serverProperties.setLogLevel(loglevel);*/
//Commented by nitin
	//if(itristatecontroldata->IsSelected())
	//	serverProperties.setAssetStatus(1);
	//else
	//	serverProperties.setAssetStatus(0);
	
	
	//////Added by nitin
	//if(itristatecontroldataMaptoRepository->IsSelected())
	//{
	//	serverProperties.setAssetStatus(1);
	//}
	//else if(itristatecontroldataDownloadImagesto->IsSelected())
	//{
	//	
	//	serverProperties.setAssetStatus(0);
	//}
// Missing Flag Check box
	//manes
	/*if(itristatecontroldataMissingFlag->IsSelected())
		serverProperties.setMissingFlag(1);
	else
		serverProperties.setMissingFlag(0);*/

	//upto here//////////

//////////////////21-feb

	LoginHelper LngHelpObj(this);	
	// adding new server
	if(EvnironmentData::getIsEditServerFlag()==FALSE){
		if(LngHelpObj.addServerInfo(serverProperties) == TRUE){		
			EvnironmentData::setSelectedServerProperties(serverProperties);
			addServerToLaunchpadList();
			addServerToLoginHostDropDownList();
			//CAlert::InformationAlert("Added new server");
			return;
		}
	}

	if(ISChangeInModewhileEdit1) // Use when user changes the connection Mode at the Properties dialog
	{	    
	//CA("ISChangeInModewhileEdit1");
        PMString asd = serverProperties.getEnvName();
		bool16 bStatus = LngHelpObj.deleteServer(asd);
		if(LngHelpObj.addServerInfo(serverProperties) == TRUE)
		{	
			EvnironmentData::setSelectedServerProperties(serverProperties);				
			modifyLaunchpadListData();			
			return;
		}
	}
	// edit server properties
	int32 isEditServer=0;
	//PMString s("envName	:	");
	//s.Append(envName);
	//s.Append("\nEvnironmentData::getSelectedServerEnvName()	:	");
	//s.Append(EvnironmentData::getSelectedServerEnvName());
	//CA(s);
	if(isEditServer == envName.Compare(TRUE,EvnironmentData::getSelectedServerEnvName()))
	{
			LoginInfoValue OldServerPorperties;

			LoginHelper LngHelper(this);
			PMString EnvName1("");
			LngHelper.getCurrentServerInfo(EnvName1, OldServerPorperties);

			serverProperties.setClientList(OldServerPorperties.getClientList());
			//CA(OldServerPorperties.getLanguage() + OldServerPorperties.getProject());
			//serverProperties.setLanguage(OldServerPorperties.getLanguage());
			//serverProperties.setProject(OldServerPorperties.getProject());
			//serverProperties.setSectionID(OldServerPorperties.getSectionID());
			//serverProperties.setIsONESource(OldServerPorperties.getIsONESource());
			////-----
			//serverProperties.setDisplayPartnerImages(OldServerPorperties.getDisplayPartnerImages());
			//serverProperties.setDisplayPickListImages(OldServerPorperties.getDisplayPickListImages());
			//serverProperties.setShowObjectCountFlag(OldServerPorperties.getShowObjectCountFlag());

			//CAlert::InformationAlert("OldServerPorperties.getProject()  :  "+OldServerPorperties.getProject());
		if(LngHelpObj.editServerInfo(serverProperties,clientID) == TRUE){
			EvnironmentData::setSelectedServerProperties(serverProperties);
			//CAlert::InformationAlert("edit existing server");

		}
	}
	else { // if the environment is changes( adding new one and deleting old one)
		//CA("2");
        PMString asd = EvnironmentData::getSelectedServerEnvName();
		if(LngHelpObj.deleteServer(asd) == TRUE){
				//CAlert::InformationAlert("deleted old server");						
			if(LngHelpObj.addServerInfo(serverProperties) == TRUE){
				//CAlert::InformationAlert("Added new server");
				EvnironmentData::setSelectedServerProperties(serverProperties);
			}
		}
	}
	if(EvnironmentData::getIsEditServerFlag()==TRUE){
	modifyLaunchpadListData();	
	modifyLoginHostDropDownList();
	}
}

//  Generated by Dolly build 17: template "Dialog".
// End, LNG3DialogController.cpp.


void LNG3DialogController::modifyLaunchpadListData(void)
{	 
	LoginInfoValue currentServerProps = EvnironmentData::getServerProperties();
	IPanelControlData* panelControlData = EvnironmentData::getlauncpadPanel();

	IControlView* view = panelControlData->FindWidget(/*kLaunchpadListBoxWidgetID*/kLaunchpadTreeViewWidgetID);
	if(view == nil){
		//CAlert::InformationAlert("Fail to find LaunchPad ListBox");
		return;
	}
	//listBoxHelper listHelper(this, kLNGPluginID);
	//listHelper.modifyListElementData(panelControlData);	

	view->ShowView();
	view->Enable();
			
	InterfacePtr<ITreeViewMgr> treeViewMgr(view, UseDefaultIID());
	if(!treeViewMgr)
	{
		CA("treeViewMgr is nil");
		return ;
	}

	LNGTreeDataCache dc;
	dc.clearMap();

	LNGTreeModel treeModel;
	displayEnvList = 1;
	PMString pfName("101 Root");
	treeModel.setRoot(-1, pfName, 1);
	treeViewMgr->ClearTree(kTrue);
	treeModel.GetRootUID();
	
	treeViewMgr->ChangeRoot();				
	view->Invalidate();
}

void LNG3DialogController::addServerToLaunchpadList(void)
{	
	IPanelControlData* panelControlData = EvnironmentData::getlauncpadPanel();
	IControlView* view = panelControlData->FindWidget(/*kLaunchpadListBoxWidgetID*/kLaunchpadTreeViewWidgetID);
	if(view == nil){
		//CAlert::InformationAlert("Fail to find LaunchPad ListBox");
		return;
	}
	//listBoxHelper listHelper(this, kLNGPluginID);
	//listHelper.AddElement(view,EvnironmentData::getServerProperties());

	view->ShowView();
	view->Enable();
			
	InterfacePtr<ITreeViewMgr> treeViewMgr(view, UseDefaultIID());
	if(!treeViewMgr)
	{
		CA("treeViewMgr is nil");
		return ;
	}

	LNGTreeDataCache dc;
	dc.clearMap();

	LNGTreeModel treeModel;
	displayEnvList = 1;
	PMString pfName("101 Root");
	treeModel.setRoot(-1, pfName, 1);
	treeViewMgr->ClearTree(kTrue);
	treeModel.GetRootUID();
	
	treeViewMgr->ChangeRoot();				
	view->Invalidate();
}


void LNG3DialogController::disableControls(void)
{
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		//CAlert::InformationAlert("InitializeFields panelControlData nil");
		return;
	}

	do{		
		IControlView* controlView = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
		if (controlView == nil){
			//CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
			break;
		}
		controlView->Disable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
		if (controlView == nil){
			//CAlert::InformationAlert("InitializeFields servername controlView nil");
			break;
		}
		controlView->Disable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
		if (controlView == nil){
			//CAlert::InformationAlert("InitializeFields port controlView nil");
			break;
		}
		controlView->Disable(kTrue);
//mane
		//controlView=nil;
		//controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
		//if (controlView == nil){
		//	//CAlert::InformationAlert("InitializeFields schema controlView nil");
		//	break;
		//}
		//controlView->Disable(kTrue);
		//controlView->HideView();//added by Tushar on 07_03_07.

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
		if (controlView == nil){
			//CAlert::InformationAlert("InitializeFields username controlView nil");
			break;
		}
		controlView->Disable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
		if (controlView == nil){
			//CAlert::InformationAlert("InitializeFields password controlView nil");
			break;
		}
		controlView->Disable(kTrue);
	}while(0);
}

void LNG3DialogController::DisableControlsDependOnServer(PMString& vendorName)
{
	PMString oracle("Oracle"),sql("SQLServer"),access("Access");
	
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		//CAlert::InformationAlert("InitializeFields panelControlData nil");
		return;
	}

	do{		
		if(vendorName == access){
			IControlView* controlView= panelControlData->FindWidget(kPropSrvTextEditWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("InitializeFields servername controlView nil");
				break;
			}
			bool8 flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("InitializeFields port controlView nil");
				break;
			}
			flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("InitializeFields username controlView nil");
				break;
			}
			flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
			if (controlView == nil){
				//CAlert::InformationAlert("InitializeFields password controlView nil");
				break;
			}
			flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);		
			
		}
		if(vendorName == oracle ){			
			//IControlView* controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema
			//if (controlView == nil){
			//	CAlert::InformationAlert("InitializeFields schema controlView nil");
			//	break;
			//}
			//bool8 flag = controlView->IsEnabled();
			//if(flag==TRUE)
			//	controlView->Disable(kTrue);
		}
	}while(0);
}

void LNG3DialogController::EnableAllTheControls(void)
{	
	do{	
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			//CAlert::InformationAlert("EnableAllTheControls panelControlData nil");
			break;
		}
		
		IControlView* controlView= panelControlData->FindWidget(kPropSrvTextEditWidgetID);
		if (controlView == nil){
			//CAlert::InformationAlert(" servername controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
		if (controlView == nil){
			//CAlert::InformationAlert(" port controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
		if (controlView == nil){
			//CAlert::InformationAlert(" username controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
		if (controlView == nil){
			//CAlert::InformationAlert(" password controlView nil");
			break;
		}
		controlView->Enable(kTrue);
	//mane
		//controlView=nil;
		//controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema
		//if (controlView == nil){
		//	//CAlert::InformationAlert(" schema controlView nil");
		//	break;
		//}		
		//controlView->Enable(kTrue);	
	}while(0);
}

PMString LNG3DialogController::getSelectedVendor(void)
{
	PMString vendorName("");
	//CAlert::InformationAlert("getSelectedVendor");
	do
	{	
		// Get the name of the selected snippet.
		InterfacePtr<IStringListControlData> vendorDropListData(this->QueryListControlDataInterface(kPropVendorDropDownWidgetID));	
		if (vendorDropListData == nil)		{
			//CAlert::InformationAlert("Vendor vendorDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> vendorDropListController(vendorDropListData, UseDefaultIID());
		if (vendorDropListController == nil){
			//CAlert::InformationAlert("vendorDropListController nil");
			break;
		}
		int32 selectedRow = vendorDropListController->GetSelected();

		if(selectedRow <0){
			//CAlert::InformationAlert("invalid row");
			return PMString("");
		}		
		vendorName.SetString(vendorDropListData->GetString(selectedRow));
	}while(0);
	return vendorName;
}

PMString LNG3DialogController::getSelectedVersion(void)
{
	//CAlert::InformationAlert("getSelectedVersion");
	PMString versionName("");
	do
	{
		// Get the name of the selected snippet.
		InterfacePtr<IStringListControlData> versionDropListData(this->QueryListControlDataInterface(kPropVersionDropDownWidgetID));
		if (versionDropListData == nil){
			//CAlert::InformationAlert(" versionDropListData nil");
			break;
		}		
		
		InterfacePtr<IDropDownListController> versionDropListController(versionDropListData, UseDefaultIID());
		if (versionDropListController == nil){
			//CAlert::InformationAlert("versionDropListController nil");
			break;
		}
		int32 selectedRow = versionDropListController->GetSelected();
		if(selectedRow <0){			
			return versionName;
		}		
		versionName.SetString(versionDropListData->GetString(selectedRow));
	}while(0);
	return versionName;
}

void LNG3DialogController::addVersions(PMString& selectedeVendorName)
{	
//CAlert::InformationAlert("LNG3DialogController addVersions");
//
//	//CAlert::InformationAlert("addVersions");
//	PMString noProxy("No_Proxy"),useProxy("Use_Proxy");//added by Tushar on 06_03_07
//
//	/*LoginHelper LngHelpObj(this);
//	vector<CVendorInfoValue> *VectorVendorInfoPtr = LngHelpObj.getVendors();
//	VectorVendorInfoValue::iterator it;*/
//	
//	do{
//		InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
//		if (panelControlData == nil){
//			CAlert::InformationAlert("panelControlData nil");
//			break;
//		}
//
//		IControlView* iControlView = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
//		if (iControlView == nil){
//			CAlert::InformationAlert("iControlView nil");
//			break;
//		}
//
//		InterfacePtr<IStringListControlData> versionDropListData(iControlView,UseDefaultIID());
//		if (versionDropListData == nil){
//			CAlert::InformationAlert("versionDropListData nil");	
//			break;
//		}
//		versionDropListData->Clear(kFalse, kFalse);
//
//		if(selectedeVendorName == useProxy)
//		{
//			InterfacePtr<IDropDownListController> iPropVersionDropDownListController(iControlView, UseDefaultIID());
//			if (iPropVersionDropDownListController == nil){
//				CA("iPropVersionDropDownListController nil");
//				return;
//			}
//
//			versionDropListData->AddString("HTTP");
//			versionDropListData->AddString("HTTPS");
//			//versionDropListData->AddString("SOCKS");
//			iPropVersionDropDownListController->Select(0);
//			return;
//		}
//		else if(selectedeVendorName == noProxy)
//		{
//			return;
//		}
//		else
//		{
//			LoginHelper LngHelpObj(this);
//			vector<CVendorInfoValue> *VectorVendorInfoPtr = LngHelpObj.getVendors();
//			VectorVendorInfoValue::iterator it;
//
//			for(it = VectorVendorInfoPtr->begin(); it != VectorVendorInfoPtr->end(); it++)
//			{	
//				PMString CurrentVendor(it->getVendorName());
//				int32 isVendorMatch = selectedeVendorName.Compare(TRUE,CurrentVendor);
//				if(isVendorMatch==0)
//				{
//					vector<PMString> versions;
//					vector<PMString>::iterator versionItrator;
//					versions=it->getVendorVersion();
//					int j=0;
//					for(versionItrator = versions.begin(); versionItrator != versions.end(); versionItrator++)
//					{	
//						PMString CurrentVerIterator(*versionItrator);
//						if(versionDropListData)
//							versionDropListData->AddString(CurrentVerIterator, j, kFalse, kFalse);
//						j++;				
//					}	
//				}
//			}
//		}
//	}while(0);
}

void LNG3DialogController::addServerToLoginHostDropDownList(void)
{	
	IPanelControlData* panelControlData = EvnironmentData::getLoginPanel();
			
	IControlView* iHostDropDownWidgetControlView = panelControlData->FindWidget(kHostDropDownWidgetID);
	if (iHostDropDownWidgetControlView == nil)
		return ;

	InterfacePtr<IStringListControlData> HostDropListData(iHostDropDownWidgetControlView,UseDefaultIID());
	if(HostDropListData == nil)
		return;
		
	HostDropListData->AddString(EvnironmentData::getServerProperties().getCmurl(), IStringListControlData::kEnd, kFalse, kFalse);

	InterfacePtr<IDropDownListController> HostDropListController(HostDropListData,UseDefaultIID());
	if (HostDropListController == nil)		
		return;
		
	EvnironmentData::setSelectedserver(HostDropListData->Length() - 1);
	HostDropListController->Select(EvnironmentData::getSelectedserver());

}

void LNG3DialogController::modifyLoginHostDropDownList(void)
{
	//CA("LNG3DialogController::modifyLoginHostDropDownList");
	IPanelControlData* panelControlData = EvnironmentData::getLoginPanel();
			
	IControlView* iHostDropDownWidgetControlView = panelControlData->FindWidget(kHostDropDownWidgetID);
	if (iHostDropDownWidgetControlView == nil)
		return ;

	InterfacePtr<IStringListControlData> HostDropListData(iHostDropDownWidgetControlView,UseDefaultIID());
	if(HostDropListData == nil)
		return;
		
	HostDropListData->RemoveString(EvnironmentData::getSelectedserver(), kFalse, kFalse);
	HostDropListData->AddString(EvnironmentData::getServerProperties().getCmurl(), EvnironmentData::getSelectedserver(), kFalse, kFalse);

	InterfacePtr<IDropDownListController> HostDropListController(HostDropListData,UseDefaultIID());
	if (HostDropListController == nil)		
		return;
		
	HostDropListController->Select(EvnironmentData::getSelectedserver());
}
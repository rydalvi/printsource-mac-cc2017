#include "VCPlugInHeaders.h"
#include "ILoginEvent.h"
#include "LNGID.h"
#include "CPMUnknown.h"
#include "IAppFramework.h"

class CLoginEvent:public CPMUnknown<ILoginEvent>
{
public:
	CLoginEvent(IPMUnknown* );
	~CLoginEvent();
	bool16 userLoggedIn();
	bool16 userLoggedOut();
};

CREATE_PMINTERFACE(CLoginEvent,kCLoginEventImpl)

CLoginEvent::CLoginEvent(IPMUnknown* boss):CPMUnknown<ILoginEvent>(boss)
{}

CLoginEvent::~CLoginEvent()
{}

bool16 CLoginEvent::userLoggedIn()
{
	bool16 retVal=kFalse;
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		bool16 result=ptrIAppFramework->LOGINMngr_getLoginStatus();
		retVal=result;
	}while(kFalse);
	return retVal;
}

bool16 CLoginEvent::userLoggedOut()
{
	bool16 retVal=kFalse;
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			break;

		bool16 result=ptrIAppFramework->LOGINMngr_getLoginStatus();
		if(result==kTrue)
			retVal=kFalse;
		else
			retVal=kTrue;
	}while(kFalse);
	return retVal;
}
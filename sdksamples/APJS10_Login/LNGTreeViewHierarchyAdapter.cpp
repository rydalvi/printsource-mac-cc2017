#include "VCPlugInHeaders.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IUIDData.h"
#include "CPMUnknown.h"
#include "LNGID.h"
#include "UIDNodeID.h"
#include "LNGTreeModel.h"

#include "CAlert.h"
#define CA(X) CAlert::InformationAlert(X);


class LNGTreeViewHierarchyAdapter : public CPMUnknown<ITreeViewHierarchyAdapter>
{
public:
	LNGTreeViewHierarchyAdapter(IPMUnknown* boss);
	virtual ~LNGTreeViewHierarchyAdapter();
	virtual NodeID_rv	GetRootNode() const;
	virtual NodeID_rv	GetParentNode( const NodeID& node ) const;
	virtual int32		GetNumChildren( const NodeID& node ) const;
	virtual NodeID_rv	GetNthChild( const NodeID& node, const int32& nth ) const;
	virtual int32		GetChildIndex( const NodeID& parent, const NodeID& child ) const;
	virtual NodeID_rv	GetGenericNodeID() const;
	virtual bool16		ShouldAddNthChild( const NodeID& node, const int32& nth ) const { return kTrue; }

private:
	LNGTreeModel	fBscTreeModel;
};	

CREATE_PMINTERFACE(LNGTreeViewHierarchyAdapter, kLNGTreeViewHierarchyAdapterImpl)

LNGTreeViewHierarchyAdapter::LNGTreeViewHierarchyAdapter(IPMUnknown* boss) : 
	CPMUnknown<ITreeViewHierarchyAdapter>(boss)
{
}

LNGTreeViewHierarchyAdapter::~LNGTreeViewHierarchyAdapter()
{
}

NodeID_rv	LNGTreeViewHierarchyAdapter::GetRootNode() const
{
	//CA("LNGTreeViewHierarchyAdapter::GetRootNode");
	UID rootUID = fBscTreeModel.GetRootUID();
	return UIDNodeID::Create(rootUID);
}

NodeID_rv	LNGTreeViewHierarchyAdapter::GetParentNode( const NodeID& node ) const
{
	do 
	{
		TreeNodePtr<UIDNodeID> uidNodeID(node);
		if (uidNodeID == nil) 
			break; 

		UID uid = uidNodeID->GetUID();
		if(uid == fBscTreeModel.GetRootUID()) 
			break;
		
		ASSERT(uid != kInvalidUID);
		if(uid == kInvalidUID)
			break;
		
		UID uidParent = fBscTreeModel.GetParentUID(uid);
		if(uidParent != kInvalidUID) 
			return UIDNodeID::Create(uidParent);

	}while(kFalse);
	return kInvalidNodeID;	
}

int32 LNGTreeViewHierarchyAdapter::GetNumChildren( const NodeID& node ) const
{
	//CA("LNGTreeViewHierarchyAdapter::GetNumChildren");
	int32 retval=0;
	do 
	{
		TreeNodePtr<UIDNodeID> uidNodeID(node);
		if (uidNodeID == nil) 
			break;
		
		UID uid = uidNodeID->GetUID();
		if(uid == kInvalidUID) 
			break;
		
		if(uid == fBscTreeModel.GetRootUID()) 
			retval = fBscTreeModel.GetRootCount();
		else 
			retval = fBscTreeModel.GetChildCount(uid);
		
	} while(kFalse);
	return retval;
}

NodeID_rv	LNGTreeViewHierarchyAdapter::GetNthChild( const NodeID& node, const int32& nth ) const
{
	//CA("LNGTreeViewHierarchyAdapter::GetNthChild");
	TreeNodePtr<UIDNodeID>	uidNodeID(node);
	if( uidNodeID != nil)
	{
		UID uidChild = kInvalidUID;
		if(uidNodeID->GetUID() == fBscTreeModel.GetRootUID()) 
		{
			uidChild = fBscTreeModel.GetNthRootChild(nth);
		}
		else 
		{
			uidChild = fBscTreeModel.GetNthChildUID(uidNodeID->GetUID(), nth);			
		}

		if(uidChild != kInvalidUID)
			return UIDNodeID::Create(uidChild);
	}
	return kInvalidNodeID;	
}

int32 LNGTreeViewHierarchyAdapter::GetChildIndex
	(const NodeID& parent, const NodeID& child ) const
{
	do 
	{
		TreeNodePtr<UIDNodeID>	parentUIDNodeID(parent);
		ASSERT(parentUIDNodeID);
		if(parentUIDNodeID==nil) 
			break;
		
		TreeNodePtr<UIDNodeID>	childUIDNodeID(child);
		ASSERT(childUIDNodeID);
		if(childUIDNodeID==nil) 
			break;
		
		if(parentUIDNodeID->GetUID() == kInvalidUID) 
			break;
		
		if(childUIDNodeID->GetUID() == kInvalidUID) 
			break;

		if(parentUIDNodeID->GetUID() == fBscTreeModel.GetRootUID()) 
			return fBscTreeModel.GetIndexForRootChild(childUIDNodeID->GetUID());
		else 
			return fBscTreeModel.GetChildIndexFor(parentUIDNodeID->GetUID(), childUIDNodeID->GetUID());			
	} while(kFalse);
	return (-1);
}

NodeID_rv	LNGTreeViewHierarchyAdapter::GetGenericNodeID() const
{
	return UIDNodeID::Create(kInvalidUID);
}






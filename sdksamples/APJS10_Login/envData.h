
#include"AnsiBasedTypes.h"
#include"PMString.h"
#include"Value\ServerInfoValue.h"


class EvnironmentData
{	
	static int selectedServer;
	static PMString envName;
	static bool16 serverStatus;
	static bool8 isNewServer; // 1 for edit existing server, 2 for create new server
	static CServerInfoValue selectedServerProperties;
public:

	static CServerInfoValue&  getServerProperties(void);
	static void  setServerServerProperties(CServerInfoValue&);

	static bool8 getIsNewServer(void);
	static void  setIsNewServer(bool8 isNewServer);

	static bool16 getServerStatus(void);
	static void   setServerStatus(bool16 status);

	static int  getSelectedserver(void);
	static void setSelectedserver(int row);

	static void getSelectedserverEnvName(PMString* str);
	static void setSelectedServerEnvName(PMString* str);
};

/*
//	File:	LNG3DialogObserver.cpp
//
//	Date:	17-Sep-2003
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2003 Next Millenium Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"

// General includes:
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "CAlert.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "IDialogController.h"
// Project includes:
//#include "IAppFramework.h"
#include "dataStore.h"
#include "LoginHelper.h"
//#include "listBoxHelperFuncs.h"
#include "LNGActionComponent.h"
#include "IWidgetParent.h"
#include "IAppFramework.h"
// new added
#include "SysFileList.h"
#include "PropDialogControler.h" 

#include "IOpenFileDialog.h"        //CS4 
#include "ITextControlData.h"


//#define CA(X) CAlert::InformationAlert(X)

//extern bool16 ISChangeInModewhileEdit1;
/** LNG3DialogObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author raghu
*/
class LNG3DialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		LNG3DialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~LNG3DialogObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();	
		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
		/**
			This routine is a template for attaching a widget to a subject to be observed.
			@param iPanelControlData panel that contains the widget which we want to attach to.
			@param widgetID	the widget id that we want to attach to.
			@param interfaceID protocol we are observering.
		*/
		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		
		/**
			This routine is a template for detaching a widget from a subject being observed.
			@param iPanelControlData panel that contains the widget which we want to detach from.
			@param widgetID	the widget id that we want to detach from.
			@param interfaceID protocol we are observering.
		*/
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void disableControls(void);
		void DisableControlsDependOnServer(PMString& vendorName);
		void EnableAllTheControls(void);
		// new added @Vaibhav 15 Feb
		void ShowBrowseDialog(PMString& path,PMString& infoMsg);


	private:
		void addVersionsForSelectedVendor(IControlView*);
		void validatePropFeilds(PMString&);
		//void EnableAllTheControls(void);
		void addVersions(PMString& );	
		void setControlText(WidgetID , PMString);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(LNG3DialogObserver, kLNGPropDlgObserverImpl)

/* AutoAttach
*/
void LNG3DialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();	
	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){			
			//CAlert::InformationAlert("AutoAttach panelControlData nil");
			break;
		}
		// Attach to other widgets you want to handle dynamically here.
		AttachWidget(panelControlData, kPropVendorDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		AttachWidget(panelControlData, kPropVersionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		AttachWidget(panelControlData, kPropConnModeDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		AttachWidget(panelControlData, kNewOKButtonWidgetID, IID_ITRISTATECONTROLDATA);
		
		// new added by vaibhav 15 Feb

		//AttachToWidget(kImgPathButtonWidgetID1,IID_IBOOLEANCONTROLDATA,panelControlData);

		//Added by nitin 

		//AttachWidget(panelControlData, kPropImagePathTextEditWidgetID,IID_ITEXTCONTROLDATA );		
		//AttachWidget(panelControlData, kPropDownloadImagePathTextEditWidgetID,IID_ITEXTCONTROLDATA );
		//AttachToWidget(kImgPathButtonWidgetID2,IID_IBOOLEANCONTROLDATA,panelControlData);
		//AttachWidget(panelControlData, kPropMaptoRepositoryWidgetID,IID_ITRISTATECONTROLDATA );
		//AttachWidget(panelControlData, kPropDownloadImagestoWidgetID,IID_ITRISTATECONTROLDATA );
		//AttachToWidget(kDocumentPathButtonWidgetID,IID_IBOOLEANCONTROLDATA,panelControlData);
		AttachToWidget(kThirdCancelButtonWidgetID,  IID_ITRISTATECONTROLDATA,panelControlData);

		//added by Tushar on 06_03_07


	} while (false);
}

/* AutoDetach
*/
void LNG3DialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();	
	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("AutoDetach panelControlData nil");
			break;
		}
		// Detach from other widgets you handle dynamically here.		
		DetachWidget(panelControlData, kPropVendorDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		DetachWidget(panelControlData, kPropVersionDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		DetachWidget(panelControlData, kPropConnModeDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
		//DetachFromWidget(kImgPathButtonWidgetID1,	IID_IBOOLEANCONTROLDATA,panelControlData);
		//DetachWidget(panelControlData, kPropImagePathTextEditWidgetID,IID_ITRISTATECONTROLDATA );
		//DetachWidget(panelControlData, kPropDownloadImagePathTextEditWidgetID,IID_ITRISTATECONTROLDATA );
		//DetachFromWidget(kImgPathButtonWidgetID2,	IID_IBOOLEANCONTROLDATA,panelControlData);
		//DetachWidget(panelControlData, kPropMaptoRepositoryWidgetID,IID_ITRISTATECONTROLDATA );
		//DetachWidget(panelControlData, kPropDownloadImagestoWidgetID,IID_ITRISTATECONTROLDATA );
		//DetachFromWidget(kDocumentPathButtonWidgetID,	IID_IBOOLEANCONTROLDATA,panelControlData);
		DetachFromWidget(kThirdCancelButtonWidgetID,IID_ITRISTATECONTROLDATA,panelControlData);
		DetachWidget(panelControlData, kNewOKButtonWidgetID,IID_ITRISTATECONTROLDATA );

	} while (false);
}

/* Update
*/
void LNG3DialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject,
	const PMIID &protocol,
	void* changedBy
)
{
	//CAlert::InformationAlert("PropDialogObserver::Update");
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil){
			CAlert::InformationAlert("controlView nil");
			break;
		}		

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("Update panelControlData nil");
			return ;
		}

		

		WidgetID theSelectedWidget = controlView->GetWidgetID();

		if(theSelectedWidget== kPropVendorDropDownWidgetID)
		{	
			//CA("theSelectedWidget== kPropVendorDropDownWidgetID");
			addVersionsForSelectedVendor(controlView);

		}

		else if((theSelectedWidget == kThirdCancelButtonWidgetID || theSelectedWidget == kCancelButton_WidgetID) && theChange == kTrueStateMessage)
		{			
			if(theSelectedWidget == kThirdCancelButtonWidgetID){
				CDialogObserver::CloseDialog();
				break;
			}
		}	

		if((theSelectedWidget == kNewOKButtonWidgetID || theSelectedWidget == kOKButtonWidgetID)  && theChange == kTrueStateMessage)
		{
			//CA("theSelectedWidget == kNewOKButtonWidgetID && theChange == kTrueStateMessage");
			if(theSelectedWidget == kNewOKButtonWidgetID){
				LNG3DialogController objstring(this);
				//CA("theSelectedWidget == kNewOKButtonWidgetID");
				WidgetID returnWidgetID = objstring.ValidateDialogFields(nil);
				if(returnWidgetID == kDefaultWidgetId){
					objstring.ApplyDialogFields(nil,kInvalidWidgetID);
					CDialogObserver::CloseDialog();
				}
				
			}
		}

		
		
		//------------------------- server mode setting starts here---------------------------------------//
		//else if (theSelectedWidget== kPropConnModeDropDownWidgetID && protocol==IID_ISTRINGLISTCONTROLDATA)
		//{//CAlert::InformationAlert("kPropConnModeDropDownWidgetID");
		//	InterfacePtr<IDropDownListController> iDropDownListController(controlView, UseDefaultIID());
		//	if (iDropDownListController == nil)
		//		return;

		//	int32 selectedRowIndex=0; 
		//	//selectedRowIndex = iDropDownListController->GetSelected();
		//	
		//	LoginHelper LngHelpObj(this);

		//	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		//	if(ptrIAppFramework == nil){
		//		CAlert::InformationAlert("Pointer to IAppFramework is nil.");
		//		return ;
		//	}		

		//	InterfacePtr<IDialogController> theCntrller(this,IID_IDIALOGCONTROLLER);
		//	if(theCntrller==nil)
		//	{
		//		CA("theCntrller is nil");
		//		return;
		//	}
		//	if(selectedRowIndex == 0)    /// Row Index 0 for HTTP is selected
		//	{	
		//		//CA("selectedRowIndex == 0");
		//		EvnironmentData::SelctedConnMode = 0;

		//		if(EvnironmentData::getIsEditServerFlag()== TRUE) // if Edit server flag is true ie. Dialog open through Properties Button
		//		{
		//			PMString selectedEnv;
		//			selectedEnv = EvnironmentData::getSelectedServerEnvName();
		//			CServerInfoValue selectedServerProp;
		//			
		//			if(LngHelpObj.getCurrentServerInfo(selectedEnv,selectedServerProp)==-1){
		//				CAlert::InformationAlert("finding server fail...");
		//				return;
		//			}
	
		//			if(selectedServerProp.getdbConnectionMode() == 1)  // if Properties are by default for JDBC and we want to check the HTTP part 
		//			{
		//				//CA("selectedServerProp.getdbConnectionMode() == 1,     if Properties are by default for JDBC and we want to check the HTTP part");
		//				//added by Tushar on 07_03_07
		//				theCntrller->SetTextControlData(kPropDbVendStaticTextWidgetID, "Proxy:");
		//				theCntrller->SetTextControlData(kPropDbVerStaticTextWidgetID, "Protocol:");
		//				theCntrller->SetTextControlData(kPropDbSrvStaticTextWidgetID, "Host Server:");
		//				theCntrller->SetTextControlData(kPropDbPortStaticTextWidgetID, "Port:");
		//				theCntrller->SetTextControlData(kPropDbSchemStaticTextWidgetID, "");
		//				theCntrller->SetTextControlData(kPropDbUnameStaticTextWidgetID, "Userid:");
		//				theCntrller->SetTextControlData(kPropDbPwordStaticTextWidgetID, "Password:");
		//				//upto here 
		//				ISChangeInModewhileEdit1 = kTrue;  //commented on 23 Feb
		//				theCntrller->SetTextControlData(kPropEnvTextEditWidgetID, selectedServerProp.getEnvName());
		//				theCntrller->SetTextControlData(kPropSrvURLTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropSrvTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropPrtTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropSchemaTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropUnameTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropPwordTextEditWidgetID, "");
		//				disableControls();

		//				//added by tushar on 07_03_07
		//				IControlView* iPropSchemaTextEditCntrlView=panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
		//				if(iPropSchemaTextEditCntrlView==nil) 
		//					return;
		//				iPropSchemaTextEditCntrlView->Hide();
		//				//upto here..

		//				InterfacePtr<IStringListControlData> vendorDropListData(theCntrller->QueryListControlDataInterface(kPropVendorDropDownWidgetID));
		//				if (vendorDropListData == nil){
		//					CAlert::InformationAlert("vendorDropListData nil");	
		//				}
		//				vendorDropListData->Clear(kFalse, kFalse);
		//				
		//				InterfacePtr<IStringListControlData> versionDropListData(theCntrller->QueryListControlDataInterface(kPropVersionDropDownWidgetID));
		//				if (versionDropListData == nil){
		//					CAlert::InformationAlert("versionDropListData nil");	
		//				}
		//				versionDropListData->Clear(kFalse, kFalse);

		//				IControlView* controlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
		//				if (controlView == nil){
		//					CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
		//					return;
		//				}
		//				//controlView->Disable(kTrue);
		//				
		//				//added by Tushar on 07_03_07
		//				controlView->Enable(kTrue);

		//				InterfacePtr<IDropDownListController> iPropVendorDropDownListController(controlView, UseDefaultIID());
		//				if (iPropVendorDropDownListController == nil)
		//					return;
		//										
		//				vendorDropListData->AddString("No_Proxy");
		//				vendorDropListData->AddString("Use_Proxy");
		//				iPropVendorDropDownListController->Select(0);
		//				//upto here

		//				IControlView* controlView1 = panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
		//				if (controlView1 == nil){
		//					CAlert::InformationAlert("InitializeFields port controlView nil");
		//					return;
		//				}
		//				controlView1->Enable();
		//				EvnironmentData::SelctedConnMode = 0;
		//				ptrIAppFramework->setSelectedConnMode(0); // for HTTP	
		//			}
		//			else if(selectedServerProp.getdbConnectionMode() == 0) // if Properties are by default for HTTP and we again want to check the HTTP part
		//			{	
		//				//CA("selectedServerProp.getdbConnectionMode() == 0,  if Properties are by default for HTTP and we again want to check the HTTP part  ");
		//				//added by Tushar on 07_03_07
		//				theCntrller->SetTextControlData(kPropDbVendStaticTextWidgetID, "Proxy:");
		//				theCntrller->SetTextControlData(kPropDbVerStaticTextWidgetID, "Protocol:");
		//				theCntrller->SetTextControlData(kPropDbSrvStaticTextWidgetID, "Host Server:");
		//				theCntrller->SetTextControlData(kPropDbPortStaticTextWidgetID, "Port:");
		//				theCntrller->SetTextControlData(kPropDbSchemStaticTextWidgetID, "");
		//				theCntrller->SetTextControlData(kPropDbUnameStaticTextWidgetID, "Userid:");
		//				theCntrller->SetTextControlData(kPropDbPwordStaticTextWidgetID, "Password:");
		//				//upto here 

		//				ISChangeInModewhileEdit1= kFalse;
		//				theCntrller->SetTextControlData(kPropEnvTextEditWidgetID, selectedServerProp.getEnvName());
		//				theCntrller->SetTextControlData(kPropSrvURLTextEditWidgetID, selectedServerProp.getServerURL());

		//				theCntrller->SetTextControlData(kPropSrvTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropPrtTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropSchemaTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropUnameTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropPwordTextEditWidgetID, "");
		//				
		//				disableControls();
		//				
		//				//added by tushar on 07_03_07
		//				IControlView* iPropSchemaTextEditCntrlView=panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
		//				if(iPropSchemaTextEditCntrlView==nil) 
		//					return;
		//				iPropSchemaTextEditCntrlView->Hide();
		//				//upto here..

		//				IControlView* controlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
		//				if (controlView == nil){
		//					CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
		//					return;
		//				}
		//				//controlView->Disable(kTrue);
		//				
		//				//added by Tushar on 07_03_07
		//				controlView->Enable(kTrue);

		//				InterfacePtr<IDropDownListController> iPropVendorDropDownListController(controlView, UseDefaultIID());
		//				if (iPropVendorDropDownListController == nil)
		//					return;
		//				
		//				InterfacePtr<IStringListControlData> vendorDropListData(controlView,UseDefaultIID());	
		//				if (vendorDropListData == nil)		{
		//					CAlert::InformationAlert("Vendor vendorDropListData nil");
		//					break;
		//				}
		//				vendorDropListData->Clear(kFalse, kFalse);
		//				vendorDropListData->AddString("No_Proxy");
		//				vendorDropListData->AddString("Use_Proxy");
		//				iPropVendorDropDownListController->Select(0);
		//				//upto here
		//
		//				IControlView* controlView1 = panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
		//				if (controlView1 == nil){
		//					CAlert::InformationAlert("InitializeFields port controlView nil");
		//					return;
		//				}
		//				controlView1->Enable();
		//				EvnironmentData::SelctedConnMode = 0;
		//			
		//				//added by Tushar on 08_03_07
		//				if(selectedServerProp.getVendorName() == "Use_Proxy")
		//				{
		//					EnableAllTheControls();
		//					controlView->Enable(kTrue);
		//					iPropVendorDropDownListController->Select(1);
		//					
		//					IControlView* propVersionDropDownControlView = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
		//					if (propVersionDropDownControlView == nil){
		//						CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
		//						return;
		//					}
		//					propVersionDropDownControlView->Enable();
		//					
		//					InterfacePtr<IDropDownListController> iPropVersionDropDownListController(propVersionDropDownControlView, UseDefaultIID());
		//					if (iPropVendorDropDownListController == nil)
		//						return;
		//					
		//					InterfacePtr<IStringListControlData> versionDropListData(theCntrller->QueryListControlDataInterface(kPropVersionDropDownWidgetID));
		//					if (versionDropListData == nil){
		//						CAlert::InformationAlert("versionDropListData nil");	
		//					}
		//					versionDropListData->Clear(kFalse, kFalse);
		//					versionDropListData->AddString("HTTP");
		//					versionDropListData->AddString("HTTPS");
		//					//versionDropListData->AddString("SOCKS");
		//					
		//					if(selectedServerProp.getVersion() == "HTTP")
		//						iPropVersionDropDownListController->Select(0);
		//					if(selectedServerProp.getVersion() == "HTTPS")
		//						iPropVersionDropDownListController->Select(1);
		//					//if(selectedServerProp.getVersion() == "SOCKS")
		//						//iPropVersionDropDownListController->Select(2);

		//					PMString ServerPort = "";
		//					ServerPort.AppendNumber(selectedServerProp.getServerPort());
		//					theCntrller->SetTextControlData(kPropSrvTextEditWidgetID, selectedServerProp.getServerName());
		//					theCntrller->SetTextControlData(kPropPrtTextEditWidgetID, ServerPort);
		//					theCntrller->SetTextControlData(kPropUnameTextEditWidgetID, selectedServerProp.getUserName());
		//					theCntrller->SetTextControlData(kPropPwordTextEditWidgetID, selectedServerProp.getPassword());
		//				}
		//				//upto here added by Tushar on 08_03_07
		//				ptrIAppFramework->setSelectedConnMode(0); // for HTTP	

		//			}
		//		}
		//		else // if New Properties option selected ie> Dialog opened through New Button
		//		{
		//			//CA("if New Properties option selected ie> Dialog opened through New Button @@@@@@@@@@");
		//			//added by Tushar on 07_03_07
		//			theCntrller->SetTextControlData(kPropDbVendStaticTextWidgetID, "Proxy:");
		//			theCntrller->SetTextControlData(kPropDbVerStaticTextWidgetID, "Protocol:");
		//			theCntrller->SetTextControlData(kPropDbSrvStaticTextWidgetID, "Host Server:");
		//			theCntrller->SetTextControlData(kPropDbPortStaticTextWidgetID, "Port:");
		//			theCntrller->SetTextControlData(kPropDbSchemStaticTextWidgetID, "");
		//			theCntrller->SetTextControlData(kPropDbUnameStaticTextWidgetID, "Userid:");
		//			theCntrller->SetTextControlData(kPropDbPwordStaticTextWidgetID, "Password:");
		//			
		//			IControlView* iPropSchemaTextEditCntrlView=panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
		//			if(iPropSchemaTextEditCntrlView==nil) 
		//				return;

		//			InterfacePtr<IStringListControlData> vendorDropListData(theCntrller->QueryListControlDataInterface(kPropVendorDropDownWidgetID));
		//			if (vendorDropListData == nil){
		//				CAlert::InformationAlert("vendorDropListData nil");	
		//			}
		//			vendorDropListData->Clear(kFalse, kFalse);

		//			InterfacePtr<IStringListControlData> versionDropListData(theCntrller->QueryListControlDataInterface(kPropVersionDropDownWidgetID));
		//			if (versionDropListData == nil){
		//				CAlert::InformationAlert("versionDropListData nil");	
		//			}
		//			versionDropListData->Clear(kFalse, kFalse);
		//			
		//			//iPropSchemaTextEditCntrlView->Show();//added by tushar on 07_03_07
		//			//iPropSchemaTextEditCntrlView->Enable();//added by tushar on 07_03_07
		//			//upto here 

		//			ISChangeInModewhileEdit1 = kFalse;
		//			theCntrller->SetTextControlData(kPropEnvTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropSrvTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropSrvURLTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropPrtTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropSchemaTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropUnameTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropPwordTextEditWidgetID, "");	
		//			disableControls();
		//			iPropSchemaTextEditCntrlView->Hide();//added by tushar on 07_03_07

		//			IControlView* controlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
		//			if (controlView == nil){
		//				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
		//				return;
		//			}
		//			//controlView->Disable(kTrue);	
		//			
		//			//added by Tushar on 07_03_07
		//			controlView->Enable(kTrue);

		//			InterfacePtr<IDropDownListController> iPropVendorDropDownListController(controlView, UseDefaultIID());
		//			if (iPropVendorDropDownListController == nil)
		//				return;
		//			
		//			vendorDropListData->Clear(kFalse, kFalse);
		//			vendorDropListData->AddString("No_Proxy");
		//			vendorDropListData->AddString("Use_Proxy");
		//			iPropVendorDropDownListController->Select(0);
		//			//upto here



		//			IControlView* controlView1 = panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
		//			if (controlView1 == nil){
		//				CAlert::InformationAlert("InitializeFields port controlView nil");
		//				return;
		//			}
		//			controlView1->Enable();		
		//			EvnironmentData::SelctedConnMode = 0;
		//			ptrIAppFramework->setSelectedConnMode(0); // for HTTP	

		//		}

		//		//ptrIAppFramework->setSelectedConnMode(0); // for Http
		//		
		//	}
		//	else if(selectedRowIndex == 1)  // if JDBC option is selected by User
		//	{				
		//		EvnironmentData::SelctedConnMode = 1;

		//		if(EvnironmentData::getIsEditServerFlag()== TRUE) // if Edit Properties
		//		{
		//			PMString selectedEnv;
		//			selectedEnv = EvnironmentData::getSelectedServerEnvName();
		//			CServerInfoValue selectedServerProp;
		//			
		//			if(LngHelpObj.getCurrentServerInfo(selectedEnv,selectedServerProp)==-1){
		//				CAlert::InformationAlert("finding server fail...");
		//				return;
		//			}

		//			IControlView* propSchemaTextEditControlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
		//			if (propSchemaTextEditControlView == nil){
		//				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
		//				break;
		//			}

		//			propSchemaTextEditControlView->Show();

		//			if(selectedServerProp.getdbConnectionMode() == 0)  // if Properties are by default for HTTP and we want to check the JDBC part
		//			{	
		//				//CA("selectedServerProp.getdbConnectionMode() == 0 , if Properties are by default for HTTP and we want to check the JDBC part");
		//				//added by Tushar on 06_03_07
		//				theCntrller->SetTextControlData(kPropDbVendStaticTextWidgetID, "Database Vendor:");
		//				theCntrller->SetTextControlData(kPropDbVerStaticTextWidgetID, "Database Version:");
		//				theCntrller->SetTextControlData(kPropDbSrvStaticTextWidgetID, "Database Server:");
		//				theCntrller->SetTextControlData(kPropDbPortStaticTextWidgetID, "Database Port:");
		//				theCntrller->SetTextControlData(kPropDbSchemStaticTextWidgetID, "Database Schema:");
		//				theCntrller->SetTextControlData(kPropDbUnameStaticTextWidgetID, "Database Userid:");
		//				theCntrller->SetTextControlData(kPropDbPwordStaticTextWidgetID, "Database Password:");
		//				//upto here 

		//				ISChangeInModewhileEdit1 = kTrue;
		//				theCntrller->SetTextControlData(kPropEnvTextEditWidgetID, selectedServerProp.getEnvName());
		//				theCntrller->SetTextControlData(kPropSrvURLTextEditWidgetID, "");

		//				theCntrller->SetTextControlData(kPropSrvTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropPrtTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropSchemaTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropUnameTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropPwordTextEditWidgetID, "");

		//				//added by Tushar on _07_03_07
		//				InterfacePtr<IStringListControlData> versionDropListData(theCntrller->QueryListControlDataInterface(kPropVersionDropDownWidgetID));
		//				if (versionDropListData == nil){
		//					CAlert::InformationAlert("versionDropListData nil");	
		//				}
		//				versionDropListData->Clear(kFalse, kFalse);
		//				//upto here

		//				disableControls();	

		//				IControlView* controlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
		//				if (controlView == nil){
		//					CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
		//					return;
		//				}
		//				controlView->Enable();	

		//				IControlView* controlView1 = panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
		//				if (controlView1 == nil){
		//					CAlert::InformationAlert("InitializeFields port controlView nil");
		//					return;
		//				}
		//				controlView1->Disable(kTrue);

		//				InterfacePtr<IStringListControlData> vendorDropListData(theCntrller->QueryListControlDataInterface(kPropVendorDropDownWidgetID));
		//				if (vendorDropListData == nil){
		//					CAlert::InformationAlert("vendorDropListData nil");	
		//				}
		//				vendorDropListData->Clear(kFalse, kFalse);
		//				

		//				int i=0;
		//				vector<CVendorInfoValue> *VectorVendorInfoPtr =nil;

		//				VectorVendorInfoPtr=LngHelpObj.getVendors();
		//				if(VectorVendorInfoPtr==NULL){
		//					CAlert::InformationAlert("DBproviders file not found/no entries");	
		//					return;
		//				}

		//				VectorVendorInfoValue::iterator it;

		//				for(it = VectorVendorInfoPtr->begin(); it != VectorVendorInfoPtr->end(); it++)
		//				{			
		//					// Put the 'None' entry as the first in both lists. we'll eventually make it the default
		//					PMString scratchName(it->getVendorName());
		//					if(vendorDropListData)
		//						vendorDropListData->AddString(scratchName, i, kFalse, kFalse);
		//					i++;
		//				}
		//				
		//				EvnironmentData::SelctedConnMode = 1;
		//				ptrIAppFramework->setSelectedConnMode(1); // for SERVER

		//			}
		//			else  if(selectedServerProp.getdbConnectionMode() == 1) //// if Properties are by default for JDBC and we want to check the JDBC part
		//			{
		//				//CA("if Properties are by default for JDBC and we want to check the JDBC part");
		//				//added by Tushar on 06_03_07
		//				theCntrller->SetTextControlData(kPropDbVendStaticTextWidgetID, "Database Vendor:");
		//				theCntrller->SetTextControlData(kPropDbVerStaticTextWidgetID, "Database Version:");
		//				theCntrller->SetTextControlData(kPropDbSrvStaticTextWidgetID, "Database Server:");
		//				theCntrller->SetTextControlData(kPropDbPortStaticTextWidgetID, "Database Port:");
		//				theCntrller->SetTextControlData(kPropDbSchemStaticTextWidgetID, "Database Schema:");
		//				theCntrller->SetTextControlData(kPropDbUnameStaticTextWidgetID, "Database Userid:");
		//				theCntrller->SetTextControlData(kPropDbPwordStaticTextWidgetID, "Database Password:");
		//				//upto here

		//				ISChangeInModewhileEdit1 = kFalse;
		//				InterfacePtr<IStringListControlData> vendorDropListData(theCntrller->QueryListControlDataInterface(kPropVendorDropDownWidgetID));
		//				if (vendorDropListData == nil){
		//					CAlert::InformationAlert("vendorDropListData nil");	
		//				}
		//				vendorDropListData->Clear(kFalse, kFalse);
		//				
		//				InterfacePtr<IStringListControlData> versionDropListData(theCntrller->QueryListControlDataInterface(kPropVersionDropDownWidgetID));
		//				if (versionDropListData == nil){
		//					CAlert::InformationAlert("versionDropListData nil");	
		//				}
		//				versionDropListData->Clear(kFalse, kFalse);

		//				long port =0;
		//				port=selectedServerProp.getServerPort();		
		//				PMString portStr("");
		//				if(port>0)
		//					portStr.AppendNumber(port);

		//				theCntrller->SetTextControlData(kPropEnvTextEditWidgetID, selectedServerProp.getEnvName());
		//				theCntrller->SetTextControlData(kPropSrvTextEditWidgetID, selectedServerProp.getServerName());
		//				theCntrller->SetTextControlData(kPropSrvURLTextEditWidgetID, "");
		//				theCntrller->SetTextControlData(kPropPrtTextEditWidgetID, portStr);
		//				theCntrller->SetTextControlData(kPropSchemaTextEditWidgetID, selectedServerProp.getSchema());
		//				theCntrller->SetTextControlData(kPropUnameTextEditWidgetID, selectedServerProp.getUserName());
		//				theCntrller->SetTextControlData(kPropPwordTextEditWidgetID, selectedServerProp.getPassword());
		//				EnableAllTheControls();
		//	
		//				IControlView* controlView1 = panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
		//				if (controlView1 == nil){
		//					CAlert::InformationAlert("InitializeFields port controlView nil");
		//					return;
		//				}
		//				controlView1->Disable(kTrue);

		//				IControlView* controlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
		//				if (controlView == nil){
		//					CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
		//					return;
		//				}
		//				controlView->Enable();	
		//	
		//				IControlView* controlView3 = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
		//				if (controlView3 == nil){
		//					CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
		//					return;
		//				}
		//				controlView3->Enable();	

		//				PMString vendorName(selectedServerProp.getVendorName());
		//				
		//				int i=0;
		//				int selectedRow=-1;
		//				vector<CVendorInfoValue> *VectorVendorInfoPtr =nil;
		//				VectorVendorInfoPtr=LngHelpObj.getVendors();

		//				if(VectorVendorInfoPtr==NULL){
		//					CAlert::InformationAlert("DBproviders file not found/no entries");	
		//					return;
		//				}

		//				VectorVendorInfoValue::iterator it;

		//				for(it = VectorVendorInfoPtr->begin(); it != VectorVendorInfoPtr->end(); it++)
		//				{			
		//					// Put the 'None' entry as the first in both lists. we'll eventually make it the default
		//					PMString scratchName(it->getVendorName());
		//					if(vendorDropListData)
		//						vendorDropListData->AddString(scratchName, i, kFalse, kFalse);
		//					i++;
		//				}

		//				for(it = VectorVendorInfoPtr->begin(); it != VectorVendorInfoPtr->end(); it++)
		//				{
		//					selectedRow++;
		//					PMString CurrVendorName(it->getVendorName());
		//					int32 match = vendorName.Compare(TRUE,CurrVendorName);
		//					if(match==0)
		//					{
		//						//CAlert::InformationAlert("Match");
		//						InterfacePtr<IDropDownListController> vendorDropListController(vendorDropListData, UseDefaultIID());
		//						if (vendorDropListController == nil){
		//							CAlert::InformationAlert("vendorDropListController nil");
		//							break;
		//						}
		//						vendorDropListController->Select(selectedRow);

		//						vector<PMString> versions;
		//						versions.clear();
		//						vector<PMString>::iterator versionItrator;
		//						versions =it->getVendorVersion();
		//						int j=0;
		//						versionDropListData->Clear(kFalse, kFalse);								

		//						for(versionItrator = versions.begin(); versionItrator != versions.end(); versionItrator++)
		//						{
		//							if(versionDropListData)
		//							{   PMString CurrVersionIterr(*versionItrator);
		//								//CA(CurrVersionIterr);
		//								versionDropListData->AddString(CurrVersionIterr, j, kFalse, kFalse);
		//							}
		//							j++;
		//						}

		//						PMString versionName(selectedServerProp.getVersion());
		//						int selectedVersionRow=-1;
		//						for(versionItrator = versions.begin(); versionItrator != versions.end(); versionItrator++)
		//						{
		//							PMString CurrVersionIter(*versionItrator);
		//							int32 isVersionMatch = versionName.Compare(TRUE,CurrVersionIter);
		//							selectedVersionRow++;
		//							if(isVersionMatch == 0)
		//							{
		//								//CAlert::InformationAlert("isVersionMatch");
		//								InterfacePtr<IDropDownListController> versionDropListController(versionDropListData, UseDefaultIID());
		//								if (versionDropListController == nil){
		//									CAlert::InformationAlert("versionDropListController nil");
		//									break;
		//								}
		//								versionDropListController->Select(selectedVersionRow);
		//							}					
		//						}
		//					}
		//				}		
		//				
		//				EvnironmentData::SelctedConnMode = 1;
		//				ptrIAppFramework->setSelectedConnMode(1); // for SERVER	
		//				DisableControlsDependOnServer(vendorName);


		//			}
		//		}
		//		else  // if New Properties option selected ie> Dialog opened through New Button
		//		{
		//			//CA("if New Properties option selected ie> Dialog opened through New Button");

		//			//added by Tushar on 07_03_07
		//			theCntrller->SetTextControlData(kPropDbVendStaticTextWidgetID, "Database Vendor:");
		//			theCntrller->SetTextControlData(kPropDbVerStaticTextWidgetID, "Database Version:");
		//			theCntrller->SetTextControlData(kPropDbSrvStaticTextWidgetID, "Database Server:");
		//			theCntrller->SetTextControlData(kPropDbPortStaticTextWidgetID, "Database Port:");
		//			theCntrller->SetTextControlData(kPropDbSchemStaticTextWidgetID, "Database Schema:");
		//			theCntrller->SetTextControlData(kPropDbUnameStaticTextWidgetID, "Database Userid:");
		//			theCntrller->SetTextControlData(kPropDbPwordStaticTextWidgetID, "Database Password:");
		//			
		//			IControlView* propSchemaTextEditControlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
		//			if (propSchemaTextEditControlView == nil){
		//				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
		//				break;
		//			}

		//			propSchemaTextEditControlView->Show();
		//			//upto here 

		//			ISChangeInModewhileEdit1 = kFalse;
		//			InterfacePtr<IStringListControlData> vendorDropListData(theCntrller->QueryListControlDataInterface(kPropVendorDropDownWidgetID));
		//			if (vendorDropListData == nil){
		//				CAlert::InformationAlert("vendorDropListData nil");	
		//			}
		//			vendorDropListData->Clear(kFalse, kFalse);
		//			
		//			InterfacePtr<IStringListControlData> versionDropListData(theCntrller->QueryListControlDataInterface(kPropVersionDropDownWidgetID));
		//			if (versionDropListData == nil){
		//				CAlert::InformationAlert("versionDropListData nil");	
		//			}
		//			versionDropListData->Clear(kFalse, kFalse);

		//			theCntrller->SetTextControlData(kPropEnvTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropSrvTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropSrvURLTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropPrtTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropSchemaTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropUnameTextEditWidgetID, "");
		//			theCntrller->SetTextControlData(kPropPwordTextEditWidgetID, "");	
		//			disableControls();

		//			IControlView* controlView = panelControlData->FindWidget(kPropVendorDropDownWidgetID);
		//			if (controlView == nil){
		//				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
		//				return;
		//			}
		//			controlView->Enable();	

		//			IControlView* controlView1 = panelControlData->FindWidget(kPropSrvURLTextEditWidgetID);
		//			if (controlView1 == nil){
		//				CAlert::InformationAlert("InitializeFields port controlView nil");
		//				return;
		//			}
		//			controlView1->Disable(kTrue);

		//			int i=0;
		//			vector<CVendorInfoValue> *VectorVendorInfoPtr =nil;

		//			VectorVendorInfoPtr=LngHelpObj.getVendors();
		//			if(VectorVendorInfoPtr==NULL){
		//				CAlert::InformationAlert("DBproviders file not found/no entries");	
		//				return;
		//			}

		//			VectorVendorInfoValue::iterator it;

		//			for(it = VectorVendorInfoPtr->begin(); it != VectorVendorInfoPtr->end(); it++)
		//			{			
		//				// Put the 'None' entry as the first in both lists. we'll eventually make it the default
		//				PMString scratchName(it->getVendorName());
		//				if(vendorDropListData)
		//					vendorDropListData->AddString(scratchName, i, kFalse, kFalse);
		//				i++;
		//			}					
		//			EvnironmentData::SelctedConnMode = 1;
		//			ptrIAppFramework->setSelectedConnMode(1); // for SERVER				
		//		
		//		}


		//	}
		//}

		
		
		//new added by vaibhav for image path
		//////////////////////////////////////////////////////////////////// Commented by mane 

		//else if(theSelectedWidget == kImgPathButtonWidgetID1 && theChange == kTrueStateMessage)
		//{
		//	//CA("1");
		//	PMString infoMsg("Browse for Map to Repository ");//("Browse for Image Download Path");
		//	//CA("1.1");
		//	PMString path("");
		//	//CA("1.2");
		//	//OptionsValue optnsObj = PersistData::getOptions();
		//	//CA("1.3");
		//	//path = optnsObj.getImagePath();
		//	//CA("1.4");
		//	//CA(path);
		//	ShowBrowseDialog(path,infoMsg);
		//	//CA(" IMAGE PATH " + path);
		//	//setControlText(kImagePathStaticTextWidgetID,path);//kPropImagePathStaticTextWidgetID
		//	//kPropImagePathTextEditWidgetID
		//	setControlText(kPropImagePathTextEditWidgetID , path);
		//	//CA("1.5");
		//	//setControlText(kImagePathStaticTextWidgetID,path);
		//	//CA("1.6");
		//	//optnsObj.setImagePath(path);
		//	//CA("1.7");
		//	//PersistData::setOptions(optnsObj);
		//	//CA("1.8");
		//}
		//new added by nitin
		//else if(theSelectedWidget == kImgPathButtonWidgetID2 && theChange == kTrueStateMessage)
		//{
		//	//CA("1");
		//	PMString infoMsg("Browse for Download Images to");
		//	//CA("1.1");
		//	PMString path("");
		//	//CA("1.2");
		//	//OptionsValue optnsObj = PersistData::getOptions();
		//	//CA("1.3");
		//	//path = optnsObj.getImagePath();
		//	//CA("1.4");
		//	//CA(path);
		//	ShowBrowseDialog(path,infoMsg);
		//	//CA(" IMAGE PATH " + path);
		//	//setControlText(kImagePathStaticTextWidgetID,path);//kPropImagePathStaticTextWidgetID
		//	//kPropImagePathTextEditWidgetID
		//	setControlText(kPropDownloadImagePathTextEditWidgetID , path);
		//	//CA("1.5");
		//	//setControlText(kImagePathStaticTextWidgetID,path);
		//	//CA("1.6");
		//	//optnsObj.setImagePath(path);
		//	//CA("1.7");
		//	//PersistData::setOptions(optnsObj);
		//	//CA("1.8");
		//}//up to here
		//// end

		//// new added by vaibhav for Document path

		//else if(theSelectedWidget==kDocumentPathButtonWidgetID && theChange == kTrueStateMessage)//mane
		//{
		//	PMString infoMsg("Browse for Document Download Path");
		//	PMString path("");
		////	CA("For document");
		////	OptionsValue optnsObj = PersistData::getOptions();
		////	path = optnsObj.getIndesignDocPath();
		//	ShowBrowseDialog(path,infoMsg);		
		////	CA(" DOC PATH " + path);
		//	setControlText(kPropDocPathTextEditWidgetID , path);
		////	optnsObj.setIndesignDocPath(path);
		////	PersistData::setOptions(optnsObj);			
		//}		
		//// end



		////Added by nitin from here to                //mane
		//else if(theSelectedWidget == kPropMaptoRepositoryWidgetID && theChange == kTrueStateMessage) 
		//{
		//		//CA("1");
		//	IControlView* iPropImagePathTextCntrlView=panelControlData->FindWidget(kPropImagePathTextEditWidgetID);
		//	if(iPropImagePathTextCntrlView==nil) 
		//		return;			
		//	IControlView* iPropDownloadImagePathTextCntrlView=panelControlData->FindWidget(kPropDownloadImagePathTextEditWidgetID);
		//	if(iPropDownloadImagePathTextCntrlView==nil) 
		//	{
		//		//CA("iPropDownloadImagePathTextCntrlView==nil");
		//		return;
		//	}
		//	IControlView* iImgPathButtonCntrlView=panelControlData->FindWidget(kImgPathButtonWidgetID1);
		//	if(iImgPathButtonCntrlView==nil) 
		//		return;
		//	IControlView* iDownloadImgPathButtonCntrlView=panelControlData->FindWidget(kImgPathButtonWidgetID2);
		//	if(iDownloadImgPathButtonCntrlView==nil) 
		//		return;

		//	InterfacePtr<ITextControlData>clearDownloadImagePathText(iPropDownloadImagePathTextCntrlView, UseDefaultIID());
		//	if(clearDownloadImagePathText==nil)
		//		break;
		//	
		//	if(!iPropImagePathTextCntrlView->IsEnabled())
		//		iPropImagePathTextCntrlView->Enable();

		//	if(iPropDownloadImagePathTextCntrlView->IsEnabled())
		//	{
		//		clearDownloadImagePathText->SetString("");
		//		iPropDownloadImagePathTextCntrlView->Disable();
		//	}

		//	if(!iImgPathButtonCntrlView->IsEnabled())
		//		iImgPathButtonCntrlView->Enable();

		//	if(iDownloadImgPathButtonCntrlView->IsEnabled())
		//		iDownloadImgPathButtonCntrlView->Disable();
		//		
		//}
		//else if(theSelectedWidget == kPropDownloadImagestoWidgetID && theChange == kTrueStateMessage)	
		//{	
		//	//CA("2");
		//	IControlView* iPropImagePathTextCntrlView=panelControlData->FindWidget(kPropImagePathTextEditWidgetID);
		//	if(iPropImagePathTextCntrlView==nil) 
		//		return;			
		//	IControlView* iPropDownloadImagePathTextCntrlView=panelControlData->FindWidget(kPropDownloadImagePathTextEditWidgetID);
		//	if(iPropDownloadImagePathTextCntrlView==nil) 
		//	{
		//		//CA("iPropDownloadImagePathTextCntrlView==nil");
		//		return;
		//	}

		//	IControlView* iImgPathButtonCntrlView=panelControlData->FindWidget(kImgPathButtonWidgetID1);
		//	if(iImgPathButtonCntrlView==nil) 
		//		return;
		//	IControlView* iDownloadImgPathButtonCntrlView=panelControlData->FindWidget(kImgPathButtonWidgetID2);
		//	if(iDownloadImgPathButtonCntrlView==nil) 
		//		return;

		//	InterfacePtr<ITextControlData>clearImagePathText(iPropImagePathTextCntrlView, UseDefaultIID());
		//	if(clearImagePathText==nil)
		//		break;

		//	if(iPropImagePathTextCntrlView->IsEnabled())
		//	{
		//		clearImagePathText->SetString("");
		//		iPropImagePathTextCntrlView->Disable();
		//	}

		//	if(!iPropDownloadImagePathTextCntrlView->IsEnabled())
		//		iPropDownloadImagePathTextCntrlView->Enable();

		//	if(iImgPathButtonCntrlView->IsEnabled())
		//		iImgPathButtonCntrlView->Disable();

		//	if(!iDownloadImgPathButtonCntrlView->IsEnabled())
		//		iDownloadImgPathButtonCntrlView->Enable();
		//		
		//		
		//} 
		//////////////////////////////////////////////////////////////////////////////////////// Commented by mane up to here 
		///*else if(theSelectedWidget == kPropUseMissingFlagWidgetID &&  ((theChange ==kFalseStateMessage )||(theChange == kTrueStateMessage)) && protocol == IID_ITRISTATECONTROLDATA )
		//{
		//	CA("for missing flag");
		//	CA("kPropUseMissingFlagWidgetID");


		//}*/
		//upto here//////////commented code 
	} while (false);
}
//  Generated by Dolly build 17: template "Dialog".
// End, LNG3DialogObserver.cpp.

void LNG3DialogObserver::addVersionsForSelectedVendor(IControlView* vendorControlView)
{
	do{
		InterfacePtr<IStringListControlData> vendorDropListData(vendorControlView,UseDefaultIID());	
		if (vendorDropListData == nil)		{
			CAlert::InformationAlert("Vendor vendorDropListData nil");
			break;
		}
		InterfacePtr<IDropDownListController> vendorDropListController(vendorDropListData, UseDefaultIID());
		if (vendorDropListController == nil){
			CAlert::InformationAlert("vendorDropListController nil");
			break;
		}
		int32 selectedRow = vendorDropListController->GetSelected();
		if(selectedRow <0){
			CAlert::InformationAlert("invalid row");
			break;
		}		
		PMString vendorName = vendorDropListData->GetString(selectedRow);		
		validatePropFeilds(vendorName);		
		
		addVersions(vendorName);

	}while(0);
} 

/*
void LNG3DialogObserver::EnableAllTheControls(void)
{	
	do{	
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("EnableAllTheControls panelControlData nil");
			break;
		}
		
		IControlView* controlView= panelControlData->FindWidget(kPropSrvTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" servername controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" port controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" username controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" password controlView nil");
			break;
		}
		controlView->Enable(kTrue);
	
		controlView=nil;
		controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema
		if (controlView == nil){
			CAlert::InformationAlert(" schema controlView nil");
			break;
		}		
		controlView->Enable(kTrue);	
	}while(0);
}
*/
void LNG3DialogObserver::validatePropFeilds(PMString& vendorName)
{
	PMString oracle("Oracle"),sql("SQLServer"),access("Access");
	PMString noProxy("No_Proxy"),useProxy("Use_Proxy");//added by Tushar on 06_03_07
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("InitializeFields panelControlData nil");
		return;
	}

	do{		
		IControlView* controlView = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
			break;
		}
		controlView->Enable(kTrue);

		if(vendorName == oracle || vendorName == sql){
			//CAlert::InformationAlert("oracle or sql");
			controlView=nil;
			controlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields servername controlView nil");
				break;
			}
			controlView->Enable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields port controlView nil");
				break;
			}
			controlView->Enable(kTrue);		

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields username controlView nil");
				break;
			}
			controlView->Enable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields password controlView nil");
				break;
			}
			controlView->Enable(kTrue);

			/*if(vendorName == oracle ){*/
				//controlView=nil;			
				//controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema    mane
				//if (controlView == nil){
					//CAlert::InformationAlert("InitializeFields schema controlView nil");
					//break;
				//}
				//controlView->Show();//added by Tushar on 07_03_07
				//controlView->Enable();
			//}
		}
		if(vendorName == access || vendorName == sql){
			controlView=nil;
			//CAlert::InformationAlert("access or sql");
			//controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema
			/*if (controlView == nil){
				CAlert::InformationAlert("InitializeFields schema controlView nil");mane
				break;
			}
			controlView->Enable(kTrue);*/

			if(vendorName == access ){
				controlView=nil;
				controlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
				if (controlView == nil){
					CAlert::InformationAlert("InitializeFields servername controlView nil");
					break;
				}
				controlView->Disable(kTrue);

				controlView=nil;
				controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
				if (controlView == nil){
					CAlert::InformationAlert("InitializeFields port controlView nil");
					break;
				}
				controlView->Disable(kTrue);		

				controlView=nil;
				controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
				if (controlView == nil){
					CAlert::InformationAlert("InitializeFields username controlView nil");
					break;
				}
				controlView->Disable(kTrue);

				controlView=nil;
				controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
				if (controlView == nil){
					CAlert::InformationAlert("InitializeFields password controlView nil");
					break;
				}
				controlView->Disable(kTrue);
			}
		}
		
		//added by Tushar on 06_03_07
		if(vendorName == useProxy || vendorName == noProxy)
		{	
			IControlView* propSrvTextEditControlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
			if (propSrvTextEditControlView == nil){
				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
				break;
			}
			
			IControlView* propPrtTextEditControlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
			if (propPrtTextEditControlView == nil){
				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
				break;
			}
			
			IControlView* propUnameTextEditControlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
			if (propUnameTextEditControlView == nil){
				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
				break;
			}
			
			IControlView* propPwordTextEditControlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
			if (propPwordTextEditControlView == nil){
				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
				break;
			}

			/*IControlView* propSchemaTextEditControlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID);mane
			if (propSchemaTextEditControlView == nil){
				CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
				break;
			}*/
			
			if(vendorName == useProxy)
			{
				propSrvTextEditControlView->Enable(kTrue);
				propPrtTextEditControlView->Enable(kTrue);
				propUnameTextEditControlView->Enable(kTrue);
				propPwordTextEditControlView->Enable(kTrue);
				//propSchemaTextEditControlView->Hide();     mane
			}

			if(vendorName == noProxy)
			{
				controlView->Disable(kTrue);
				propSrvTextEditControlView->Disable(kTrue);
				propPrtTextEditControlView->Disable(kTrue);
				propUnameTextEditControlView->Disable(kTrue);
				propPwordTextEditControlView->Disable(kTrue);
				//propSchemaTextEditControlView->Hide(); mane
			}
		}
		//upto here added by Tushar on 06_03_07
	}while(0);
}


void LNG3DialogObserver::addVersions(PMString& selectedeVendorName)
{	
	//CAlert::InformationAlert("LNG3DialogObserver::::::addVersions");
	PMString noProxy("No_Proxy"),useProxy("Use_Proxy");//added by Tushar on 06_03_07

	/*LoginHelper LngHelpObj(this);
	vector<CVendorInfoValue> *VectorVendorInfoPtr = LngHelpObj.getVendors();
	VectorVendorInfoValue::iterator it;*/
	
	do{
		InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("panelControlData nil");
			break;
		}

		IControlView* iControlView = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
		if (iControlView == nil){
			CAlert::InformationAlert("iControlView nil");
			break;
		}

		InterfacePtr<IStringListControlData> versionDropListData(iControlView,UseDefaultIID());
		if (versionDropListData == nil){
			CAlert::InformationAlert("versionDropListData nil");	
			break;
		}
		versionDropListData->Clear(kFalse, kFalse);

		if(selectedeVendorName == useProxy)
		{
			InterfacePtr<IDropDownListController> iPropVersionDropDownListController(iControlView, UseDefaultIID());
			if (iPropVersionDropDownListController == nil){
				//CA("iPropVersionDropDownListController nil");
				return;
			}
			PMString HTTP("HTTP");
			HTTP.SetTranslatable(kFalse);
			versionDropListData->AddString(HTTP);
			PMString HTTPS("HTTPS");
			HTTPS.SetTranslatable(kFalse);
			versionDropListData->AddString(HTTPS);
			//versionDropListData->AddString("SOCKS");
			iPropVersionDropDownListController->Select(0);
			return;
		}
		else if(selectedeVendorName == noProxy)
		{
			return;
		}
		/*else
		{
			LoginHelper LngHelpObj(this);
			vector<CVendorInfoValue> *VectorVendorInfoPtr = LngHelpObj.getVendors();
			VectorVendorInfoValue::iterator it;

			for(it = VectorVendorInfoPtr->begin(); it != VectorVendorInfoPtr->end(); it++)
			{	
				PMString CurrentVendor(it->getVendorName());
				int32 isVendorMatch = selectedeVendorName.Compare(TRUE,CurrentVendor);
				if(isVendorMatch==0)
				{
					vector<PMString> versions;
					vector<PMString>::iterator versionItrator;
					versions=it->getVendorVersion();
					int j=0;
					for(versionItrator = versions.begin(); versionItrator != versions.end(); versionItrator++)
					{	
						PMString CurrentVerIterator(*versionItrator);
						if(versionDropListData)
							versionDropListData->AddString(CurrentVerIterator, j, kFalse, kFalse);
						j++;				
					}	
				}
			}
		}*/
	}while(0);
}

/*	::AttachWidget
*/
void LNG3DialogObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{	
	do
	{

		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil){
			//CAlert::InformationAlert("AutoAttach panelControlData nil");
			break;
		}

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil){
			ASSERT_FAIL("widget has no ISubject... Ouch!");
			break;
		}
		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); // only do once
}

/*	::DetachWidget
*/
void LNG3DialogObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
		{
			ASSERT_FAIL("iControlView invalid. Where the widget are you?");
			break;
		}

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
		{
			ASSERT_FAIL("PstLst Panel widget has no ISubject... Ouch!");
			break;
		}
		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}

void LNG3DialogObserver::disableControls(void)
{
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("InitializeFields panelControlData nil");
		return;
	}

	do{		
		IControlView* controlView = panelControlData->FindWidget(kPropVersionDropDownWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields DropDown ControlView nil");
			break;
		}
		controlView->Disable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropSrvTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields servername controlView nil");
			break;
		}
		controlView->Disable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields port controlView nil");
			break;
		}
		controlView->Disable(kTrue);

		/*controlView=nil;                      mane
		controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields schema controlView nil");
			break;
		}
		controlView->Disable(kTrue);*/

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields username controlView nil");
			break;
		}
		controlView->Disable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert("InitializeFields password controlView nil");
			break;
		}
		controlView->Disable(kTrue);
	}while(0);
}


void LNG3DialogObserver::DisableControlsDependOnServer(PMString& vendorName)
{
	PMString oracle("Oracle"),sql("SQLServer"),access("Access");
	
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil){
		CAlert::InformationAlert("InitializeFields panelControlData nil");
		return;
	}

	do{		
		if(vendorName == access){
			IControlView* controlView= panelControlData->FindWidget(kPropSrvTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields servername controlView nil");
				break;
			}
			bool8 flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields port controlView nil");
				break;
			}
			flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields username controlView nil");
				break;
			}
			flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);

			controlView=nil;
			controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
			if (controlView == nil){
				CAlert::InformationAlert("InitializeFields password controlView nil");
				break;
			}
			flag = controlView->IsEnabled();
			if(flag==TRUE)
				controlView->Disable(kTrue);		
			
		}
		if(vendorName == oracle ){			
			//IControlView* controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema
			//if (controlView == nil){
			//	CAlert::InformationAlert("InitializeFields schema controlView nil");
			//	break;
			//}
			//bool8 flag = controlView->IsEnabled();
			//if(flag==TRUE)
			//	controlView->Disable(kTrue);
		}
	}while(0);
}

void LNG3DialogObserver::EnableAllTheControls(void)
{	
	do{	
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil){
			CAlert::InformationAlert("EnableAllTheControls panelControlData nil");
			break;
		}
		
		IControlView* controlView= panelControlData->FindWidget(kPropSrvTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" servername controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPrtTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" port controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropUnameTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" username controlView nil");
			break;
		}
		controlView->Enable(kTrue);

		controlView=nil;
		controlView = panelControlData->FindWidget(kPropPwordTextEditWidgetID);
		if (controlView == nil){
			CAlert::InformationAlert(" password controlView nil");
			break;
		}
		controlView->Enable(kTrue);
	
		controlView=nil;
		controlView = panelControlData->FindWidget(kPropSchemaTextEditWidgetID); // schema
		if (controlView == nil){
			CAlert::InformationAlert(" schema controlView nil");
			break;
		}		
		controlView->Enable(kTrue);	
	}while(0);
}

/// new added by vaibhav

// Commented by mane

//void LNG3DialogObserver::ShowBrowseDialog(PMString& path,PMString& infoMsg)
//{
//	do {
//    	SysFileList	fileList;
//    	InterfacePtr</*ISelectFolderDialog*/IOpenFileDialog>	openDlg(::CreateObject2<IOpenFileDialog/*ISelectFolderDialog*/>(kSelectFolderDialogBoss));
//    	
//    	infoMsg.SetTranslatable(kFalse);
//		IDFile	fPath;	
//		if (SDKUtilities::AbsolutePathToSysFile(path, fPath) != kSuccess){
//			CAlert::InformationAlert("Can not convert to SysFile format...");
//			break;
//		}
//    	if (openDlg->DoDialog(&fPath, fileList, kTrue, nil, &infoMsg))
//    	{   					
//    		IDFile	folderPath = *fileList.GetNthFile(0);
//    		PMString	folderName;    		
//     		//folderPath.SetTranslatable(kFalse);
//			folderName = folderPath.GetString();
//			if(folderName.NumUTF16TextChars()>0){				
//				path.Clear();
//				path.SetString(folderName);
//				path.Append("\\");
//				
//			}			
//    	}				
//	} while(false);	
//}

 //Commented by mane
//void LNG3DialogObserver::setControlText(WidgetID widgetID,PMString txt)
//{
//	do{
//		InterfacePtr<IPanelControlData> panelControlData(this,UseDefaultIID());
//		if (panelControlData == nil){
//			CAlert::InformationAlert("panelControlData nil");
//			break;
//		}
//
//		IControlView* iControlView = panelControlData->FindWidget(widgetID);
//		if (iControlView == nil){
//			CAlert::InformationAlert("iControlView nil");
//			break;
//		}	
//		InterfacePtr<ITextControlData> textControlData(iControlView,UseDefaultIID());
//		if (textControlData == nil){
//			CAlert::InformationAlert("ITextControlData nil");	
//			break;
//		}
//
//		textControlData->SetString(txt);
//	}
//	while(0);
//}
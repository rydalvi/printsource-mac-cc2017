//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__


// Japanese string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_jaJP)
{
        k_jaJP,	// Locale Id
        0,		// Character encoding converter

        {
        	// ----- Menu strings
                kTSCompanyKey,					kTSCompanyValue,
                kTSAboutMenuKey,					kTSPluginName "[JP]...",
                kTSPluginsMenuKey,				kTSPluginName "[JP]",

                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_jaJP,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings
                kTSPanelTitleKey,				kTSPluginName "[JP]",
                kTSStaticTextKey,				kTSPluginName "[JP]",

              // ----- Error strings

                // ----- Misc strings
                kTSAboutBoxStringKey,			kTSPluginName " [JP], version " kTSVersion " by " kTSAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_jaJP,

				kTSListTableNameKey,			"List/Table Name : ",
				kTSStencilNaleKey,				"Stencil Name : ",
				kTSUpdateDateKey,				"Update Date : ",
				kTSReloadKey,					"Reload",
				kTSSprayKey,					"Spray",
				kTSPreviewKey,					"Preview",
				kTSEditStencilkey,				"Edit Stencil",

        }

};

#endif // __ODFRC__

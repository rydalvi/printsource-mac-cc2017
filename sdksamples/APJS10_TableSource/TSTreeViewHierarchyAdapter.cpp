#include "VCPlugInHeaders.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IUIDData.h"
#include "CPMUnknown.h"
#include "TSID.h"
#include "UIDNodeID.h"
#include "TSTreeModel.h"


class TSTreeViewHierarchyAdapter : public CPMUnknown<ITreeViewHierarchyAdapter>
{
public:
	TSTreeViewHierarchyAdapter(IPMUnknown* boss);
	virtual ~TSTreeViewHierarchyAdapter();
	virtual NodeID_rv	GetRootNode() const;
	virtual NodeID_rv	GetParentNode( const NodeID& node ) const;
	virtual int32		GetNumChildren( const NodeID& node ) const;
	virtual NodeID_rv	GetNthChild( const NodeID& node, const int32& nth ) const;
	virtual int32		GetChildIndex( const NodeID& parent, const NodeID& child ) const;
	virtual NodeID_rv	GetGenericNodeID() const;
	virtual bool16  ShouldAddNthChild( const NodeID& node, const int32& nth ) const { return kTrue; }

private:
	TSTreeModel	fBscTreeModel;
};	

CREATE_PMINTERFACE(TSTreeViewHierarchyAdapter, kTSTreeViewHierarchyAdapterImpl)

TSTreeViewHierarchyAdapter::TSTreeViewHierarchyAdapter(IPMUnknown* boss) : 
	CPMUnknown<ITreeViewHierarchyAdapter>(boss)
{
}

TSTreeViewHierarchyAdapter::~TSTreeViewHierarchyAdapter()
{
}

NodeID_rv	TSTreeViewHierarchyAdapter::GetRootNode() const
{
	UID rootUID = fBscTreeModel.GetRootUID();
	return UIDNodeID::Create(rootUID);
}

NodeID_rv	TSTreeViewHierarchyAdapter::GetParentNode( const NodeID& node ) const
{
	do 
	{
		TreeNodePtr<UIDNodeID> uidNodeID(node);
		if (uidNodeID == nil) 
			break; 

		UID uid = uidNodeID->GetUID();
		if(uid == fBscTreeModel.GetRootUID()) 
			break;
		
		ASSERT(uid != kInvalidUID);
		if(uid == kInvalidUID)
			break;
		
		UID uidParent = fBscTreeModel.GetParentUID(uid);
		if(uidParent != kInvalidUID) 
			return UIDNodeID::Create(uidParent);

	}while(kFalse);
	return kInvalidNodeID;	
}

int32 TSTreeViewHierarchyAdapter::GetNumChildren( const NodeID& node ) const
{
	int32 retval=0;
	do 
	{
		TreeNodePtr<UIDNodeID> uidNodeID(node);
		if (uidNodeID == nil) 
			break;
		
		UID uid = uidNodeID->GetUID();
		if(uid == kInvalidUID) 
			break;
		
		if(uid == fBscTreeModel.GetRootUID()) 
			retval = fBscTreeModel.GetRootCount();
		else 
			retval = fBscTreeModel.GetChildCount(uid);
		
	} while(kFalse);
	return retval;
}

NodeID_rv	TSTreeViewHierarchyAdapter::GetNthChild( const NodeID& node, const int32& nth ) const
{
	TreeNodePtr<UIDNodeID>	uidNodeID(node);
	if( uidNodeID != nil)
	{
		UID uidChild = kInvalidUID;
		if(uidNodeID->GetUID() == fBscTreeModel.GetRootUID()) 
		{
			uidChild = fBscTreeModel.GetNthRootChild(nth);
		}
		else 
		{
			uidChild = fBscTreeModel.GetNthChildUID(uidNodeID->GetUID(), nth);			
		}

		if(uidChild != kInvalidUID)
			return UIDNodeID::Create(uidChild);
	}
	return kInvalidNodeID;	
}

int32 TSTreeViewHierarchyAdapter::GetChildIndex
	(const NodeID& parent, const NodeID& child ) const
{
	do 
	{
		TreeNodePtr<UIDNodeID>	parentUIDNodeID(parent);
		ASSERT(parentUIDNodeID);
		if(parentUIDNodeID==nil) 
			break;
		
		TreeNodePtr<UIDNodeID>	childUIDNodeID(child);
		ASSERT(childUIDNodeID);
		if(childUIDNodeID==nil) 
			break;
		
		if(parentUIDNodeID->GetUID() == kInvalidUID) 
			break;
		
		if(childUIDNodeID->GetUID() == kInvalidUID) 
			break;

		if(parentUIDNodeID->GetUID() == fBscTreeModel.GetRootUID()) 
			return fBscTreeModel.GetIndexForRootChild(childUIDNodeID->GetUID());
		else 
			return fBscTreeModel.GetChildIndexFor(parentUIDNodeID->GetUID(), childUIDNodeID->GetUID());			
	} while(kFalse);
	return (-1);
}

NodeID_rv	TSTreeViewHierarchyAdapter::GetGenericNodeID() const
{
	return UIDNodeID::Create(kInvalidUID);
}






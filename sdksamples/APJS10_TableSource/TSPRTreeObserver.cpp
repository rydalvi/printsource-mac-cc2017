#include "VCPlugInHeaders.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "TSID.h"
#include "SDKListBoxHelper.h"
#include "IPanelControlData.h"
#include "TSMediatorClass.h"
#include "TSListboxData.h"
#include "IListControlData.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TSPRListBoxObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

int32 PRRow = -1;
class TSPRListBoxObserver : public CObserver
{
public:
	TSPRListBoxObserver(IPMUnknown *boss);
	~TSPRListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(TSPRListBoxObserver, kTSPRListBoxObserverImpl)

TSPRListBoxObserver::TSPRListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

TSPRListBoxObserver::~TSPRListBoxObserver()
{
}

void TSPRListBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void TSPRListBoxObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_ILISTCONTROLDATA);      //CS3 Change
	}
}

void TSPRListBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	//CAlert::InformationAlert(__FUNCTION__);
	if (protocol == IID_ILISTCONTROLDATA && theChange == kListSelectionChangedByUserMessage ) 
	{
		do 
		{
			PRRow = -1;
			/*InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if(panel==nil)
				break;

			SDKListBoxHelper listHelper(this, kTSPluginID);

			IControlView* PRLstboxCntrlView = listHelper.FindCurrentListBox(panel, 3);
			if(PRLstboxCntrlView == nil) 
				break;*/

			InterfacePtr<IListBoxController> listCntl(TSMediatorClass::PRLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
				break;

			K2Vector<int32> curSelection ;
			listCntl->GetSelected( curSelection ) ;

			const int kSelectionLength =  curSelection.Length() ;

			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			if(kSelectionLength>0)
			{
				curSelRowIndex=curSelection[0];
				TSListboxData PRLstboxData;
				int32 vectSize=PRLstboxData.returnListVectorSize(3);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==curSelRowIndex)
					{
						TSListboxInfo PRListboxInfo=PRLstboxData.getData(3,y);
						curSelRowStr=PRListboxInfo.name;
						TSMediatorClass::tableFlag=PRListboxInfo.tableFlag;
						TSMediatorClass::imageFlag=PRListboxInfo.isImageFlag;
						break;
					}
				}
			}
			TSMediatorClass::curSelLstbox=3;
			TSMediatorClass::curSelRowIndex=curSelRowIndex;
			PRRow = curSelRowIndex;
			TSMediatorClass::curSelRowString=curSelRowStr;
			//TSMediatorClass::PRLstboxCntrlView=PRLstboxCntrlView;
		} while(0);
	}
}


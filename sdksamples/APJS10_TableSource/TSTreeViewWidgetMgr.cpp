#include "VCPlugInHeaders.h"
#include "IControlView.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "UIDNodeID.h"
#include "CTreeViewWidgetMgr.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SysControlIds.h"
#include "TSID.h"
#include "TSTreeModel.h"
#include "TSDataNode.h"
#include "TSMediatorClass.h"
#include "CAlert.h"
#include "TSTreeDataCache.h"
//#include "ISpecialChar.h"
//#include "ISpecialChar.h"
#include "CAlert.h"
#include "IAppFramework.h"
//#include "IMessageServer.h"
//#define FILENAME			PMString("TSTreeViewWidgetMgr.cpp")
//#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
//#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
#define CA(X) CAlert::InformationAlert(X); 

extern TSDataNodeList ProjectDataNodeList;
extern int32 RowCountForTree;
extern int32 SelectedRowNo;
extern TSDataNodeList PFDataNodeList;
extern TSDataNodeList PGDataNodeList;
extern TSDataNodeList PRDataNodeList;
extern TSDataNodeList ITEMDataNodeList;
//extern TSDataNodeList CatagoryDataNodeList;


class TSTreeViewWidgetMgr: public CTreeViewWidgetMgr
{
public:
	TSTreeViewWidgetMgr(IPMUnknown* boss);
	virtual ~TSTreeViewWidgetMgr() {}
	virtual	IControlView*	CreateWidgetForNode(const NodeID& node) const;
	virtual	WidgetID		GetWidgetTypeForNode(const NodeID& node) const;
	virtual	bool16 ApplyNodeIDToWidget
		( const NodeID& node, IControlView* widget, int32 message = 0 ) const;
	virtual PMReal GetIndentForNode(const NodeID& node) const;
private:
	PMString getNodeText(const UID& uid, int32 *RowNo) const;
	void indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const;
	enum {ePFTreeIndentForNode=3};
};	

CREATE_PMINTERFACE(TSTreeViewWidgetMgr, kTSTreeViewWidgetMgrImpl)

TSTreeViewWidgetMgr::TSTreeViewWidgetMgr(IPMUnknown* boss) :
	CTreeViewWidgetMgr(boss)
{	//CA("Inside WidgetManager constructor");
}

IControlView* TSTreeViewWidgetMgr::CreateWidgetForNode(const NodeID& node) const
{	//CA("CreateWidgetForNode");
	IControlView* retval =
		(IControlView*) ::CreateObject(::GetDataBase(this),
							RsrcSpec(LocaleSetting::GetLocale(), 
							kTSPluginID, 
							kViewRsrcType, 
							kTSTreePanelNodeRsrcID),IID_ICONTROLVIEW);
	ASSERT(retval);
	return retval;
}

WidgetID TSTreeViewWidgetMgr::GetWidgetTypeForNode(const NodeID& node) const
{	//CA("GetWidgetTypeForNode");
	return kTSTreeViewWidgetID;//kTSTreePanelNodeWidgetID; //-------
}

bool16 TSTreeViewWidgetMgr::ApplyNodeIDToWidget
(const NodeID& node, IControlView* widget, int32 message) const
{	
	//CA("TSTreeViewWidgetMgr::ApplyNodeToWidget");
	CTreeViewWidgetMgr::ApplyNodeIDToWidget( node, widget );
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return kFalse;
	}
	do
	{
	
		InterfacePtr<IPanelControlData> panelControlData(widget, UseDefaultIID());
		ASSERT(panelControlData);
		if(panelControlData==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLESource::TSTreeViewWidgetMgr::ApplyNodeIDToWidget::panelControlData is nil");		
			break;
		}

		IControlView*   expanderWidget = panelControlData->FindWidget(kTSTreeNodeExpanderWidgetID);
		ASSERT(expanderWidget);
		if(expanderWidget == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TABLESource::TSTreeViewWidgetMgr::expanderWidget is nil");		
			break;
		}


		IControlView*   TableIconWidget = panelControlData->FindWidget(kTSTreeTableIconSuiteWidgetID);
		//ASSERT(TableIconWidget);
		if(TableIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLESource::TSSelectionObserver::ApplyNodeIDToWidget::TableIconWidget is nil");
			break;
		}
		IControlView*   ListIconWidget = panelControlData->FindWidget(kTSTreeListIconSuiteWidgetID);
		if(ListIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLESource::TSSelectionObserver::ApplyNodeIDToWidget::ListIconWidget is nil");
			break;
		}

		InterfacePtr<const ITreeViewHierarchyAdapter>   adapter(this, UseDefaultIID());
		ASSERT(adapter);
		if(adapter==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLESource::TSTreeViewWidgetMgr::ApplyNodeIDToWidget::adapter is nil");		
			break;
		}

		TSTreeModel model;
		TreeNodePtr<UIDNodeID>  uidNodeIDTemp(node);
		int32 uid= uidNodeIDTemp->GetUID().Get();

		TSDataNode pNode;
		TSTreeDataCache dc;

		dc.isExist(uid, pNode);

		TreeNodePtr<UIDNodeID>  uidNodeID(node);
		ASSERT(uidNodeID);
		if(uidNodeID == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLESource::TSTreeViewWidgetMgr::ApplyNodeIDToWidget::uidNodeID is nil");		
			break;
		}

		int32 *RowNo= NULL;
		int32 LocalRowNO;
		RowNo = &LocalRowNO;

		PMString stringToDisplay( this->getNodeText(uidNodeID->GetUID(), RowNo)/*"Amit Awasthi"*/);
		stringToDisplay.SetTranslatable( kFalse );
		int result = -1; 

		/*PMString QWE("SelectRowNIO : ");
		QWE.AppendNumber(SelectedRowNo);
		CA(QWE);*/
		switch(SelectedRowNo)
		{
	
			case 1:
				result = PFDataNodeList[*RowNo].getHitCount();
				break;
			case 2:
				result = PGDataNodeList[*RowNo].getHitCount();
				break;
			case 3:
				result = PRDataNodeList[*RowNo].getHitCount();
				break;
			case 4:
				result = ITEMDataNodeList[*RowNo].getHitCount();
				break;

		}
		/*PMString ASD("result : ");
				ASD.AppendNumber(result);
				ASD.Append("Row no : ");
				ASD.AppendNumber(*RowNo);
				CA(ASD);*/
				
			switch(result)
			{
			case 0:
				
	/*			ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				TextIconWidget->ShowView();
		*/		ListIconWidget->HideView();
				break;
			case 1:
	/*			TextIconWidget->HideView();
				TableIconWidget->HideView();
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				ImageIconWidget->ShowView();
		*/		ListIconWidget->HideView();
				break;
			case 2:
		/*		TextIconWidget->HideView();
				ImageIconWidget->HideView();
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				TableIconWidget->ShowView();
		*/		ListIconWidget->HideView();
				break;
			
			case 3:
		/*		ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				TextParaIconWidget->ShowView();
		*/		ListIconWidget->HideView();
				break;

			case 4:				
		/*		ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextIconWidget->HideView();				
				TextParaIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				ParaImageIconWidget->ShowView();
			*/	ListIconWidget->HideView();
				break;

			case 5:						
		/*		ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextIconWidget->HideView();				
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				PVBoxIconWidget->ShowView();
			*/	ListIconWidget->HideView();
				break;

		//case 6: added by Tushar on 13/12/06
			case 6:						
		/*		ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextIconWidget->HideView();				
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				DollerIconWidget->ShowView();
		*/		ListIconWidget->HideView();
				break;
		//case 7: added by Dattatray on 10-01-07
			case 7:						
		/*		ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextIconWidget->HideView();				
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->ShowView();
		*/		ListIconWidget->HideView();
				break;
			case 8:						
			//	ImageIconWidget->HideView();
				TableIconWidget->HideView();
			//	TextIconWidget->HideView();				
			//	TextParaIconWidget->HideView();
			//	ParaImageIconWidget->HideView();
			//	PVBoxIconWidget->HideView();
			//	DollerIconWidget->HideView();
			//	ProductGroupNameIconWidget->HideView();
				ListIconWidget->ShowView();
				break;
			default:
			//	TextIconWidget->HideView();
			//	ImageIconWidget->HideView();
				TableIconWidget->HideView();
			//	TextParaIconWidget->HideView();
			//	ParaImageIconWidget->HideView();
			//	PVBoxIconWidget->HideView();
			//	DollerIconWidget->HideView();
			//	ProductGroupNameIconWidget->HideView();
				ListIconWidget->HideView();
				break;
			}

			/*if(adapter->GetNumChildren(node)<=0)*/
				expanderWidget->HideView();	
			/*else
				expanderWidget->ShowView();
		}
*/
		IControlView* displayStringView = panelControlData->FindWidget( kTSTreeNodeNameWidgetID );
		ASSERT(displayStringView);
		if(displayStringView == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TABLESource::TSTreeViewWidgetMgr::ApplyNodeIDToWidget::displayStringView is nil");
			break;
		}
		InterfacePtr<ITextControlData>  textControlData( displayStringView, UseDefaultIID() );
		ASSERT(textControlData);
		if(textControlData== nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLESource::TSTreeViewWidgetMgr::ApplyNodeIDToWidget::textControlData is nil");		
			break;		
		}
		
		/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		break;
		PMString stringToInsert("");
		stringToInsert.Append(iConverter->handleAmpersandCase(stringToDisplay));
		*/
        stringToDisplay.ParseForEmbeddedCharacters();
		stringToDisplay.SetTranslatable(kFalse);
		textControlData->SetString(stringToDisplay);
		
		this->indent( node, widget, displayStringView );
	} while(kFalse);
	return kTrue;
}

PMReal TSTreeViewWidgetMgr::GetIndentForNode(const NodeID& node) const
{	//CA("Inside GetIndentForNode");
	do
	{
		TreeNodePtr<UIDNodeID>  uidNodeID(node);
		ASSERT(uidNodeID);
		if(uidNodeID == nil) 
			break;
		
		TSTreeModel model;
		int nodePathLengthFromRoot = model.GetNodePathLengthFromRoot(uidNodeID->GetUID());

		if( nodePathLengthFromRoot <= 0 ) 
			return 0.0;
		
		return  PMReal((nodePathLengthFromRoot * ePFTreeIndentForNode)+0.5);
	} while(kFalse);
	return 0.0;
}

PMString TSTreeViewWidgetMgr::getNodeText(const UID& uid, int32 *RowNo) const
{	//CA("getNodeText");
	TSTreeModel model;
	return model.ToString(uid, RowNo);
}

void TSTreeViewWidgetMgr::indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const
{	//CA("Inside indent");
	const PMReal indent = this->GetIndent(node);	
	PMRect widgetFrame = widget->GetFrame();
	widgetFrame.Left() = indent;
	widget->SetFrame( widgetFrame );
	staticTextWidget->WindowChanged();
	PMRect staticTextFrame = staticTextWidget->GetFrame();
	staticTextFrame.Right( widgetFrame.Right()+1500 );
	
	widgetFrame.Right(widgetFrame.Right()+1500);
	widget->SetFrame(widgetFrame);
	
	staticTextWidget->SetFrame( staticTextFrame );
}
	

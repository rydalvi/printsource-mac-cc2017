#ifndef __TSPlugInEntrypoint_h__
#define __TSPlugInEntrypoint_h__

#include "PlugIn.h"
#include "GetPlugin.h"
#include "ISession.h"

class TSPlugInEntrypoint : public PlugIn
{
public:
	virtual bool16 Load(ISession* theSession);

#ifdef WINDOWS
	static ITypeLib* fSPTypeLib;
#endif                    
};

#endif
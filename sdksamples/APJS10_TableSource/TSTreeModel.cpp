#include "VCPlugInHeaders.h"
#include "IHierarchy.h"	
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
#ifdef	 DEBUG
#include "IPlugInList.h"
#include "IObjectModel.h"
#endif
#include "ILayoutUtils.h" //Cs4
#include "TSID.h"
#include "TSTreeModel.h"
#include "CAlert.h"
#include "TSMediatorClass.h"


#include "TSDataNode.h"
//#include "IClientOptions.h"
//#include "IAppFrameWork.h"
#include "TSTreeDataCache.h"
//#include "ISpecialChar.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TSTreeModel.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

TSTreeDataCache dc;

int32 TSTreeModel::root=0;
PMString TSTreeModel::publicationName;
double TSTreeModel::classId=0;
extern TSDataNodeList ProjectDataNodeList;
extern TSDataNodeList PFDataNodeList;
extern TSDataNodeList PGDataNodeList;
extern TSDataNodeList PRDataNodeList;
extern TSDataNodeList ITEMDataNodeList;
//extern TSDataNodeList CatagoryDataNodeList;
int32 RowCountForTree = -1;
extern int32 SelectedRowNo;


TSTreeModel::TSTreeModel()
{
	//root=-1;
	//classId=11002;
}

TSTreeModel::~TSTreeModel() 
{
}


UID TSTreeModel::GetRootUID() const
{	
	if(classId<=0)
		return kInvalidUID;
	
	TSDataNode pNode;
	/*if(dc.isExist(root, pNode))
	{
		return root;
	}*/
	
	dc.clearMap();
	
	
	TSDataNodeList CurrentNodeList;
	CurrentNodeList.clear();
	switch(SelectedRowNo)
	{
	//	case 0:
//following codn is added by vijay on 9-10-2006
	//		if(TSMediatorClass::IsOneSourceMode)
	//		{
	//			CurrentNodeList = CatagoryDataNodeList;
	//		}
	//		else
	//		{
	//			CurrentNodeList = ProjectDataNodeList;
	//		}
	//		break;
		case 1:
			CurrentNodeList = PFDataNodeList;
			break;
		case 2:
			CurrentNodeList = PGDataNodeList;
			break;
		case 3:
			CurrentNodeList = PRDataNodeList;
			break;
		case 4:
			CurrentNodeList = ITEMDataNodeList;
			break;

	}

	RowCountForTree = 0;
	PMString name("Root");
	pNode.setPubId(root);
	pNode.setChildCount(static_cast<int32>(CurrentNodeList.size()));
	/*pNode.setLevel(0);*/
	pNode.setParentId(-2);
	pNode.setPublicationName(name);
	pNode.setSequence(0);
	dc.add(pNode);

	/*Adding kids*/
	/*VectorPubObjectValuePointer::iterator it1;
	VectorPubObjectValue::iterator it2;*/
	
	int count=0;
	for(int i =0 ; i<CurrentNodeList.size(); i++)
	{ //CAlert::InformationAlert(" 123");
			/*pNode.setLevel(1);*/
			
		pNode.setParentId(root);
		pNode.setSequence(i);
		count++;
		pNode.setPubId(count);
		PMString ASD(CurrentNodeList[i].getName());
		pNode.setPublicationName(ASD);
		//CA(ASD);
		pNode.setChildCount(0);
        pNode.setHitCount(CurrentNodeList[i].getHitCount());
		dc.add(pNode);
	}
	return root;
}


int32 TSTreeModel::GetRootCount() const
{
	int32 retval=0;
	TSDataNode pNode;
	if(dc.isExist(root, pNode))
	{
		return pNode.getChildCount();
	}
	return 0;
}

int32 TSTreeModel::GetChildCount(const UID& uid) const
{
	TSDataNode pNode;

	if(dc.isExist(uid.Get(), pNode))
	{
		return pNode.getChildCount();
	}
	return -1;
}

UID TSTreeModel::GetParentUID(const UID& uid) const
{
	TSDataNode pNode;

	if(dc.isExist(uid.Get(), pNode))
	{
		if(pNode.getParentId()==-2)
			return kInvalidUID;
		return pNode.getParentId();
	}
	return kInvalidUID;
}


int32 TSTreeModel::GetChildIndexFor(const UID& parentUID, const UID& childUID) const
{
	TSDataNode pNode;
	int32 retval=-1;
	
	if(dc.isExist(childUID.Get(), pNode))
		return pNode.getSequence();
	return -1;
}


UID TSTreeModel::GetNthChildUID(const UID& parentUID, const int32& index) const
{
	TSDataNode pNode;
	int32 retval=-1;

	if(dc.isExist(parentUID.Get(), index, pNode))
		return pNode.getPubId();
	return kInvalidUID;
}


UID TSTreeModel::GetNthRootChild(const int32& index) const
{
	TSDataNode pNode;
	if(dc.isExist(root, index, pNode))
	{
		return pNode.getPubId();
	}
	return kInvalidUID;
}

int32 TSTreeModel::GetIndexForRootChild(const UID& uid) const
{
	TSDataNode pNode;
	if(dc.isExist(uid.Get(), pNode))
		return pNode.getSequence();
	return -1;
}

PMString TSTreeModel::ToString(const UID& uid, int32 *Rowno) const
{
	TSDataNode pNode;
	PMString name("");

	if(dc.isExist(uid.Get(), pNode))
	{	
		*Rowno= pNode.getSequence();
		//*Rowno = pNode.getHitCount();
		/*PMString ASD("getHitCount in ToolString : ");
		ASD.AppendNumber(pNode.getHitCount());
		CA(ASD);*/
		return pNode.getName();
	}
	return name;
}


int32 TSTreeModel::GetNodePathLengthFromRoot(const UID& uid) 
{
	TSDataNode pNode;
	if(uid.Get()==root)
		return 0;
	if(dc.isExist(uid.Get(), pNode))
		return (1);
	return -1;
}

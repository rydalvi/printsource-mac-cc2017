#ifndef __SDKLISTBOXHelper_h__
#define __SDKLISTBOXHelper_h__

class IPMUnknown;
class IControlView;
class IPanelControlData;

class SDKListBoxHelper
{
public:
	SDKListBoxHelper(IPMUnknown * fOwner, int32 pluginId);
	virtual ~SDKListBoxHelper();
	
	void		AddElement(IControlView* lstboxControlView, const PMString & displayName,   WidgetID updateWidgetId, int atIndex = -2 /* kEnd */, int x=1);
	void		RemoveElementAt(int indexRemove, int x);
	void		RemoveLastElement( int x);
	IControlView* FindCurrentTree(IPanelControlData* panel, int x);
	void		EmptyCurrentTree(IPanelControlData* panel, int x);
	int			GetElementCount(int x);
	void		CheckUncheckRow(IControlView* TreeCntrlView, int32, bool);
	void		AddIcons(IControlView *TreeCntrlView, int32 index, int32 Check);

private:
	bool16		verifyState() { return (fOwner!=nil) ? kTrue : kFalse; }
	void		addListElementWidget(IControlView* lstboxControlView, InterfacePtr<IControlView> & elView, const PMString & displayName, WidgetID updateWidgetId, int atIndex, int x);
	void		removeCellWidget(IControlView * Tree, int removeIndex);
	IPMUnknown* fOwner;
	int32		fOwnerPluginID;
};

#endif 




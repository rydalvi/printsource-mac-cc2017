#include "VCPlugInHeaders.h"
#include "TSPlugInEntrypoint.h"
#include "CAlert.h"
#include "TSID.h"
#include "TSLoginEventsHandler.h"
#include "LNGID.h"
#include "ILoginEvent.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TSPluginEntryPoint.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

#ifdef WINDOWS
	ITypeLib* TSPlugInEntrypoint::fSPTypeLib = nil;
#endif

bool16 TSPlugInEntrypoint::Load(ISession* theSession)
{
	bool16 retVal=kFalse;
	do
	{
		InterfacePtr<IRegisterLoginEvent> regEvt
			((IRegisterLoginEvent*) ::CreateObject(kLNGLoginEventsHandler,IID_IREGISTERLOGINEVENT));
		if(!regEvt)
		{
			//CA("Invalid regEvt");
			break;
		}
		regEvt->registerLoginEvent(kTSLoginEventsHandlerBoss); 
	}while(kFalse);
	return kTrue;
}

/* Global
*/
static TSPlugInEntrypoint gPlugIn;

/* GetPlugIn
	The application calls this function when the plug-in is installed 
	or loaded. This function is called by name, so it must be called 
	GetPlugIn, and defined as C linkage. See GetPlugIn.h.
*/
IPlugIn* GetPlugIn()
{
	return &gPlugIn;
}

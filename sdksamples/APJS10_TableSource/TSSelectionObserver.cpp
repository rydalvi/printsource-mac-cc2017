#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "ISelectableDialogSwitcher.h"
#include "IAppFramework.h"
#include "IListBoxController.h"
#include "ITextControlData.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
#include "IEventHandler.h"
#include "IEventDispatcher.h"
#include "IApplication.h"
#include "ITreeViewMgr.h"
#include "IClientOptions.h"
#include "ICategoryBrowser.h"
#include "AcquireModalCursor.h"
#include "IContentSprayer.h"
//#include "ILoginHelper.h" 
#include "IStringListControlData.h"
#include "SelectionObserver.h"
#include "IPanelControlData.h"
#include "CPMUnknown.h"
#include "TSID.h"
#include "TSTableSourceHelper.h"
#include "TSSelectionObserver.h"
#include "TSActionComponent.h"
#include "TSMediatorClass.h"
#include "ITreeViewController.h"
#include "UIDNodeID.h"
#include "TSDataNode.h"
#include "TSTreeDataCache.h"
#include "NodeID.h"
#include "K2Vector.tpp" 
#include "IAppFramework.h"
#include "ILayoutControlData.h"
#include "IDataBase.h"
#include "ISelectionUtils.h"


#include "TSTreeData.h"
#include "PublicationNode.h"
#include "IDataSprayer.h"
#include "ITagReader.h"
#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ITableUtility.h"
#include "TSTreeModel.h"
#include "TableSourceInfoValue.h"
#include "TSItemItemGroupSpray.h"
#include "ISubSectionSprayer.h"
#include "ILayoutUIUtils.h"
#include "IPageItemTypeUtils.h"
#include "IGeometry.h"
#include "IHierarchy.h"
#include "IPMPersist.h"

#include "HelperInterface.h"

#include "CPMUnknown.h"
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SSSID.h"

#include "ILayoutControlData.h"
#include "IDataBase.h"
#include "IGeometry.h"
#include "IHierarchy.h"
#include "ILayoutUIUtils.h" //Cs4
#include "UIDRef.h"
#include "CAlert.h"
#include "ISelectionManager.h"
#include "ITextMiscellanySuite.h"
#include "ILayoutControlData.h"
#include "IDocument.h"
#include "IHierarchy.h"
#include "ITagReader.h"
#include "ISelectionUtils.h"
#include "IDataSprayer.h"
#include "SDKListBoxHelper.h"
#include "TagStruct.h"
#include "iterator"
#include "IIDXMLElement.h"
#include "ISpread.h"
#include "PMString.h"
#include "UIDList.h"
#include <vector>
#include "IPanelControlData.h"
#include "IPageItemTypeUtils.h"
#include "ILoginHelper.h"
#include "ITextControlData.h"
#include"IDialogController.h"
#include "CSprayStencilInfo.h"

//MessageServer
#include "CAlert.h"

#define CA(X) CAlert::InformationAlert(X)


extern double currentSectionID;
extern double currentobjectID;

extern VectorScreenTableInfoPtr tableInfo;
extern TSDataNodeList PRDataNodeList;
extern VectorAdvanceTableScreenValuePtr hybridTableInfo;

extern int32 SelectedRowNo;
extern VectorScreenTableInfoPtr typeTableValObj;
extern VectorAdvanceTableScreenValuePtr typeHybridTableValObj;
extern TSDataNodeList ITEMDataNodeList;
vectorCSprayStencilInfo CSprayStencilInfoVector;
bool16 SprayAllButtonSelected = kFalse;

/*class TSSelectionObserver : public ActiveSelectionObserver
{
	public:
		TSSelectionObserver(IPMUnknown *boss);
		~TSSelectionObserver();
		void AutoAttach();
		void AutoDetach();
		void Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		void loadPaletteData();	
	protected:	
		IPanelControlData*	QueryPanelControlData();
		void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		
	
};*/

CREATE_PMINTERFACE(TSSelectionObserver, kTSSelectionObserverImpl)

TSSelectionObserver::TSSelectionObserver(IPMUnknown *boss ) : ActiveSelectionObserver(boss)
{
}

TSSelectionObserver::~TSSelectionObserver()
{
}

void TSSelectionObserver::AutoAttach()
{
	do
	{
		ActiveSelectionObserver::AutoAttach();
		
		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData==nil)
		{
			//CA("iPanelControlData is nil");
			return;
		}
		TSMediatorClass::iPanelCntrlDataPtr = iPanelControlData;
		AttachWidget(iPanelControlData, kTSRelodButtonWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kTSPreviewButtonWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kTSEditStencilButtonWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kTSSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kTSUpButtonWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kTSDownButtonWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kTSSprayAllButtonWidgetID, IID_ITRISTATECONTROLDATA);		
		
		loadPaletteData();
	
	}while(kFalse);

}

void TSSelectionObserver::AutoDetach()
{
	ActiveSelectionObserver::AutoDetach();
	do
	{
		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData==nil)
		{
			//CA("iPanelControlData is nil");
			return;
		}
		DetachWidget(iPanelControlData, kTSRelodButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(iPanelControlData, kTSPreviewButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(iPanelControlData, kTSEditStencilButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(iPanelControlData, kTSSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(iPanelControlData, kTSUpButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(iPanelControlData, kTSDownButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(iPanelControlData, kTSSprayAllButtonWidgetID, IID_ITRISTATECONTROLDATA);


	} while (kFalse);
}


void TSSelectionObserver::loadPaletteData()
{
	//CA("loadPaletteData");
		do
		{				
			/* Check if the user has LoggedIn successfully */
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				//CA("ptrIAppFramework == nil");			
				return;
			}
		
			bool16 result=ptrIAppFramework->getLoginStatus();

			if(TSMediatorClass::iPanelCntrlDataPtr == nil)
			{
				return;
				//InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
				//if(!iPanelControlData) 
				//{
				//	//CA("!iPanelControlData");
				//	return;
				//}
				//TSMediatorClass::iPanelCntrlDataPtr = iPanelControlData;

			}
		
			TSMediatorClass::CurrLanguageID = ptrIAppFramework->getLocaleId();

			IControlView* iTableSourceGroupCntrlView=TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSTableSourceGroupPanelWidgetID);
			if(iTableSourceGroupCntrlView==nil) 
			{
				//CA("iTableSourceGroupCntrlView==nil");
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::iTableSourceGroupCntrlView == nil");
				break ;
			}
			
			IControlView* listTableNameCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kListTableNameWidgetID);
			if(listTableNameCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::listTableNameCtrlView is nil");
				break ;
			}
			InterfacePtr<ITextControlData> listTableNameTxtCntrlData(listTableNameCtrlView, UseDefaultIID());
			if(listTableNameTxtCntrlData==nil)
			{
				//CA("listTableNameTxtCntrlData is NULL");
				break ;
			}
			
			IControlView* stencilNameCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kStencilNameWidgetID);
			if (stencilNameCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::stencilNameCtrlView is nil");
				break;
			}
			InterfacePtr<ITextControlData> stencilNameTxtCntrlData(stencilNameCtrlView, UseDefaultIID());
			if(stencilNameTxtCntrlData==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::stencilNameTxtCntrlData is nil");
				break;
			}
			
			IControlView* updateDateCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kUpdateDateWidgetID);
			if (updateDateCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::updateDateCtrlView is nil");
				break;
			}
			InterfacePtr<ITextControlData> updateDateTxtCntrlData(updateDateCtrlView, UseDefaultIID());
			if(updateDateTxtCntrlData==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::updateDateTxtCntrlData is nil");
				break;
			}

			IControlView* RelodButtonCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSRelodButtonWidgetID);
			if (RelodButtonCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::RelodButtonCtrlView is nil");
				break;
			}
			
			IControlView* UpButtonCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSUpButtonWidgetID);
			if (UpButtonCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::UpButtonCtrlView is nil");
				break;
			}
					
			IControlView* DownButtonCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSDownButtonWidgetID);
			if (DownButtonCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::DownButtonCtrlView is nil");
				break;
			}
						
			IControlView* PreviewButtonCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSPreviewButtonWidgetID);
			if (PreviewButtonCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::PreviewButtonCtrlView is nil");
				break;
			}

			IControlView* SprayButtonCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSSprayButtonWidgetID);//Added by mane
			if (SprayButtonCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_TABLEsource::TSSelectionObserver::loadPaletteData::SprayButtonCtrlView is nil");
				break;
			}

			IControlView* SprayAllButtonCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSSprayAllButtonWidgetID);
			if (SprayAllButtonCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP46_TABLEsource::TSSelectionObserver::loadPaletteData::SprayAllButtonCtrlView is nil");
				break;
			}
			
									
			if(result)
			{  
				RelodButtonCtrlView->Enable();
				UpButtonCtrlView->Enable();
				DownButtonCtrlView->Enable();
				PreviewButtonCtrlView->Enable();
				SprayButtonCtrlView->Enable(); 
				SprayAllButtonCtrlView->Enable();

				IControlView* ProductTreePanelCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSTreeViewWidgetID);
				if (ProductTreePanelCtrlView == nil)
				{
					//CA("ProductTreePanelCtrlView == nil");
					ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::ProductTreePanelCtrlView is nil");
					break;
				}
				TSMediatorClass::PRLstboxCntrlView = ProductTreePanelCtrlView;

				InterfacePtr<ITreeViewController> treeViewMgr(ProductTreePanelCtrlView, UseDefaultIID());
				if(treeViewMgr==nil)
				{
					//CA("treeViewMgr==nil");
					ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::treeViewMgr is nil");
					break;
				}
				
				NodeIDList selectedItem;
				treeViewMgr->GetSelectedItems(selectedItem);
				if(selectedItem.size()<=0)
				{
					//CA("selectedItem.size()<=0");
					ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::selectedItem.size()<=0");
					break;
				}
				NodeID nid=selectedItem[0];

				TreeNodePtr<UIDNodeID>uidNodeIDTemp(nid);

				int32 uid= uidNodeIDTemp->GetUID().Get();
				TSDataNode tsNode;
				TSTreeDataCache dc;

				bool16 isPresent = dc.isExist(uid, tsNode);
				if(isPresent == kFalse)
				{
					//CA("isPresent == kFalse");
					ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::loadPaletteData::isPresent == kFalse");
					break;
				}
				if(SelectedRowNo == 3)
				{
					PMString tableName,updateDate,stencilName;

					int32 hitCnt = PRDataNodeList.at(uid-1).getHitCount();
					if(hitCnt == 8)
					{
						tableName.Append(tableInfo->at(uid-1).getName());
						//updateDate.Append(tableInfo->at(uid-1).getobjectTableValueValue().getformattedUpdateDate());
						stencilName.Append(tableInfo->at(uid-1).getstencil_name());
						tableName.SetTranslatable(kFalse);
						stencilName.SetTranslatable(kFalse);
                        tableName.ParseForEmbeddedCharacters();
                        stencilName.ParseForEmbeddedCharacters();
						listTableNameTxtCntrlData->SetString(tableName);
						stencilNameTxtCntrlData->SetString(stencilName);
						//updateDateTxtCntrlData->SetString(updateDate);
					}
					if(hitCnt == 2)
					{
						int32 size=static_cast<int32>(tableInfo->size());
						tableName.Append(hybridTableInfo->at(uid-size-1).getTableName());
						//updateDate.Append(hybridTableInfo->at(uid-size-1).getupdateDate());
						//stencilName.Append(hybridTableInfo->at(uid-size-1).getstencil_name());
						tableName.ParseForEmbeddedCharacters();
						tableName.SetTranslatable(kFalse);
						listTableNameTxtCntrlData->SetString(tableName);
						stencilNameTxtCntrlData->SetString(stencilName);
						updateDateTxtCntrlData->SetString(updateDate);
					}
				}
				if(SelectedRowNo == 4)
				{
					PMString tableName,updateDate,stencilName;
					int32 hitCount=ITEMDataNodeList.at(uid-1).getHitCount();

					if(hitCount == 8)  //----Condtion For Table(List)------
					{
						tableName.Append(typeTableValObj->at(uid-1).getName());
						//updateDate.Append(typeTableValObj->at(uid-1).getformattedUpdateDate());
						stencilName.Append(typeTableValObj->at(uid-1).getstencil_name());
						tableName.SetTranslatable(kFalse);
						stencilName.SetTranslatable(kFalse);
                        tableName.ParseForEmbeddedCharacters();
                        stencilName.ParseForEmbeddedCharacters();
						listTableNameTxtCntrlData->SetString(tableName);
						stencilNameTxtCntrlData->SetString(stencilName);	
						//updateDateTxtCntrlData->SetString(updateDate);
					}
					if(hitCount == 2)	//-----Condition For Hybrid Table-----
					{
						int32 size=static_cast<int32>(typeHybridTableValObj->size());
						tableName.Append(typeHybridTableValObj->at(uid-size-1).getTableName());
						//updateDate.Append(typeHybridTableValObj->at(uid-size-1).getupdateDate());
						//stencilName.Append(typeHybridTableValObj->at(uid-size-1).getstencil_name());
						tableName.ParseForEmbeddedCharacters();
						tableName.SetTranslatable(kFalse);
						listTableNameTxtCntrlData->SetString(tableName);
						stencilNameTxtCntrlData->SetString(stencilName);
						updateDateTxtCntrlData->SetString(updateDate);
					}
				}

			}
			else
			{
				listTableNameTxtCntrlData->SetString("");
				stencilNameTxtCntrlData->SetString("");
				updateDateTxtCntrlData->SetString("");

				RelodButtonCtrlView->Disable();
				UpButtonCtrlView->Disable();
				DownButtonCtrlView->Disable();
				PreviewButtonCtrlView->Disable();
				SprayButtonCtrlView->Disable();        //Added by mane
				SprayAllButtonCtrlView->Disable();
				//TSActionComponent actionObsever(this);
				//actionObsever.CloseTSPalette();
			
			}
			
		}while(kFalse);

}

void TSSelectionObserver::Update(const ClassID &theChange, ISubject *theSubject, const PMIID &protocol, void *changedBy)
{
	//CA("TSSelectionObserver::Update");
	//ActiveSelectionObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			//CA("ptrIAppFramework == nil");
			return;
		}

		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TSSelectionObserver::Update::controlView == nil");
			return;	
		}

		

		WidgetID theSelectedWidget = controlView->GetWidgetID();

		if(theSelectedWidget==kTSRelodButtonWidgetID && theChange==kTrueStateMessage)
		{
			//CA("Reload The list");
			AcquireWaitCursor awc;
			awc.Animate();
			InterfacePtr<ITSTableSourceHelper> ptrTableSourceHelper((static_cast<ITSTableSourceHelper*> (CreateObject(kTSTableSourceHelperBoss ,ITSTableSourceHelper::kDefaultIID))));
		    if(ptrTableSourceHelper == nil)
		    {
				//CA("TABLEsource not found ");
				return;
		    }
			ptrTableSourceHelper->showTableList(TSMediatorClass::objectID  , TSMediatorClass::CurrLanguageID , TSMediatorClass::parentID , TSMediatorClass::parentTypeID , TSMediatorClass::sectionID,SelectedRowNo , TSMediatorClass::pbObjectID);			
		}
		if(theSelectedWidget==kTSUpButtonWidgetID && theChange==kTrueStateMessage)
		{
			//CA("kTSUpButtonWidgetID");
			IControlView* ProductTreePanelCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSTreeViewWidgetID);

			if (ProductTreePanelCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::Update::ProductTreePanelCtrlView is nil");
				break;
			}
			TSMediatorClass::PRLstboxCntrlView = ProductTreePanelCtrlView;

			InterfacePtr<ITreeViewController>treeViewController(ProductTreePanelCtrlView, UseDefaultIID());
			if(treeViewController==nil)
			{
				break;
			}

			NodeIDList selectedItem;
			treeViewController->GetSelectedItems(selectedItem);

			if(selectedItem.size() < 1)
			{
				break;
			}
			
			/*for(int32 selectedNodeIndex = 0 ; selectedNodeIndex < selectedItem.size() ; selectedNodeIndex ++)
			{*/
				NodeID nid = selectedItem[selectedItem.size()-1];/*selectedItem[selectedNodeIndex];*/

				TreeNodePtr<UIDNodeID>uidNodeIDTemp(nid);

				int32 uid= uidNodeIDTemp->GetUID().Get();
				if(uid == 1)
				{
					break;
				}
			if(SelectedRowNo == 3)
			{
				//CA("SelectedRowNo == 3");
				int32 hybridTableSize=0;
				int32 tableInfoSize=0;
				if(hybridTableInfo != NULL)
				{
					hybridTableSize = static_cast<int32>(hybridTableInfo->size());
				}
				
				if(tableInfo != NULL)
				{
					tableInfoSize = static_cast<int32>(tableInfo->size());
				}

				int32 hitCount=0;
				hitCount=PRDataNodeList.at(uid-1).getHitCount();
				PMString tableName("");
				if(hitCount == 8)
				{
					tableName = PRDataNodeList[uid-1].getName();
					
					vector<TSDataNode>ProductDataNode;	
					ProductDataNode.push_back(PRDataNodeList[uid-1]);
					ProductDataNode.push_back(PRDataNodeList[uid-2]);
					
					PRDataNodeList[uid-1] = ProductDataNode[1];
					PRDataNodeList[uid-2] = ProductDataNode[0];
					
					vector<CItemTableValue> screenTableInfo;

					screenTableInfo.push_back(tableInfo->at(uid-1));
					screenTableInfo.push_back(tableInfo->at(uid-2));

					tableInfo->at(uid-1) = screenTableInfo.at(1);
					tableInfo->at(uid-2) = screenTableInfo.at(0);
				
					vector<TSTreeInfo> treeInfo;
					TSTreeInfo PRTreeInfo;
					TSTreeData PRTreeData;
					PRTreeInfo = PRTreeData.getData(SelectedRowNo,uid-1);
					treeInfo.push_back(PRTreeInfo);
					PRTreeInfo = PRTreeData.getData(SelectedRowNo,uid-2);
					treeInfo.push_back(PRTreeInfo);
					
					PRTreeData.changeData(SelectedRowNo,uid-1,treeInfo[1]);
					PRTreeData.changeData(SelectedRowNo,uid-2,treeInfo[0]);

				}
				if(hitCount == 2)
				{
					
					if(uid - tableInfoSize == 1)
						break;

					tableName = PRDataNodeList[uid-1].getName();
					vector<TSDataNode>ProductDataNode;	
					ProductDataNode.push_back(PRDataNodeList[uid-1]);
					ProductDataNode.push_back(PRDataNodeList[uid-2]);
					
					PRDataNodeList[uid-1] = ProductDataNode[1];
					PRDataNodeList[uid-2] = ProductDataNode[0];
					
					vector<CAdvanceTableScreenValue> hybridScreenTableInfo;
					
					hybridScreenTableInfo.push_back(hybridTableInfo->at(uid-tableInfoSize-1));
					hybridScreenTableInfo.push_back(hybridTableInfo->at(uid-tableInfoSize-2));

					hybridTableInfo->at(uid-tableInfoSize-1) = hybridScreenTableInfo.at(1);
					hybridTableInfo->at(uid-tableInfoSize-2) = hybridScreenTableInfo.at(0);
				
					vector<TSTreeInfo> treeInfo;
					TSTreeInfo PRTreeInfo;
					TSTreeData PRTreeData;
					PRTreeInfo = PRTreeData.getData(SelectedRowNo,uid-1);
					treeInfo.push_back(PRTreeInfo);
					PRTreeInfo = PRTreeData.getData(SelectedRowNo,uid-2);
					treeInfo.push_back(PRTreeInfo);
					
					PRTreeData.changeData(SelectedRowNo,uid-1,treeInfo[1]);
					PRTreeData.changeData(SelectedRowNo,uid-2,treeInfo[0]);
							
				}
			}
			if(SelectedRowNo == 4)
			{
				//CA("SelectedRowNo == 4");

				int32 hybridTableSize=0;
				int32 tableInfoSize=0;
				if(typeHybridTableValObj != NULL)
				{
					hybridTableSize =static_cast<int32> (typeHybridTableValObj->size());
				}
				
				if(typeTableValObj != NULL)
				{
					tableInfoSize =static_cast<int32>( typeTableValObj->size());
				}

				int32 hitCount=0;
				hitCount=ITEMDataNodeList.at(uid-1).getHitCount();
				PMString tableName("");
				if(hitCount == 8)
				{
					//CA("hitCount == 8");
					tableName = ITEMDataNodeList[uid-1].getName();
					
					vector<TSDataNode>ItemDataNode;	
					ItemDataNode.push_back(ITEMDataNodeList[uid-1]);
					ItemDataNode.push_back(ITEMDataNodeList[uid-2]);
					
					ITEMDataNodeList[uid-1] = ItemDataNode[1];
					ITEMDataNodeList[uid-2] = ItemDataNode[0];
					
					vector<CItemTableValue> screenTableInfo;

					screenTableInfo.push_back(typeTableValObj->at(uid-1));
					screenTableInfo.push_back(typeTableValObj->at(uid-2));

					typeTableValObj->at(uid-1) = screenTableInfo.at(1);
					typeTableValObj->at(uid-2) = screenTableInfo.at(0);
				
					vector<TSTreeInfo> treeInfo;
					TSTreeInfo ItemTreeInfo;
					TSTreeData ItemTreeData;
					ItemTreeInfo = ItemTreeData.getData(SelectedRowNo,uid-1);
					treeInfo.push_back(ItemTreeInfo);
					ItemTreeInfo = ItemTreeData.getData(SelectedRowNo,uid-2);
					treeInfo.push_back(ItemTreeInfo);
				
					ItemTreeData.changeData(SelectedRowNo,uid-1,treeInfo[1]);
					ItemTreeData.changeData(SelectedRowNo,uid-2,treeInfo[0]);


				}
				if(hitCount == 2)
				{
					
					if(uid - tableInfoSize == 1)
						break;

					tableName = ITEMDataNodeList[uid-1].getName();
					vector<TSDataNode>ItemDataNode;	
					ItemDataNode.push_back(ITEMDataNodeList[uid-1]);
					ItemDataNode.push_back(ITEMDataNodeList[uid-2]);
					
					ITEMDataNodeList[uid-1] = ItemDataNode[1];
					ITEMDataNodeList[uid-2] = ItemDataNode[0];
					
					vector<CAdvanceTableScreenValue> hybridScreenTableInfo;
					
					hybridScreenTableInfo.push_back(typeHybridTableValObj->at(uid-tableInfoSize-1));
					hybridScreenTableInfo.push_back(typeHybridTableValObj->at(uid-tableInfoSize-2));

					typeHybridTableValObj->at(uid-tableInfoSize-1) = hybridScreenTableInfo.at(1);
					typeHybridTableValObj->at(uid-tableInfoSize-2) = hybridScreenTableInfo.at(0);
				
					vector<TSTreeInfo> treeInfo;
					TSTreeInfo ItemTreeInfo;
					TSTreeData ItemTreeData;
					ItemTreeInfo = ItemTreeData.getData(SelectedRowNo,uid-1);
					treeInfo.push_back(ItemTreeInfo);
					ItemTreeInfo = ItemTreeData.getData(SelectedRowNo,uid-2);
					treeInfo.push_back(ItemTreeInfo);
					
					ItemTreeData.changeData(SelectedRowNo,uid-1,treeInfo[1]);
					ItemTreeData.changeData(SelectedRowNo,uid-2,treeInfo[0]);
							
				}

			}
				
			IControlView* TreeCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSTreeViewWidgetID);
			if(TreeCtrlView==nil)
			{
				break;
			}
			InterfacePtr<ITreeViewMgr> treeViewMgr(TreeCtrlView, UseDefaultIID());
			if(!treeViewMgr) { 
				//CA("!treeViewMgr");
				break;
			}
			TSTreeModel pModel;
			PMString root("Root");
							
			switch(SelectedRowNo) 
			{
				case 0:
					pModel.setRoot(-1, root, 1);
					break;
				case 1:
					pModel.setRoot(-1, root, 2);
					break;
				case 2:
					pModel.setRoot(-1, root, 3);
					break;
				case 3:
					pModel.setRoot(-1, root, 4);
					break;
				case 4:
					pModel.setRoot(-1, root, 5);
					break;
			}		
			
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();

			treeViewController->Select(nid,kTrue,kTrue);
			
			//--------Test code for selected item is hilited after up button ----
			//NodeID_rv nodeID_rv= treeViewMgr->Search(tableName);
			////NodeIDClass *nodeIDClass = nodeID_rv.Get();
			//NodeID nodeId(nodeID_rv);

			//treeViewController->Select(nodeId,kTrue,kTrue);
		//}
		
		}
		if(theSelectedWidget==kTSDownButtonWidgetID	&& theChange==kTrueStateMessage)
		{
			IControlView* ProductTreePanelCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSTreeViewWidgetID);
			if (ProductTreePanelCtrlView == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::Update::ProductTreePanelCtrlView is nil");
				break;
			}
			TSMediatorClass::PRLstboxCntrlView = ProductTreePanelCtrlView;

			InterfacePtr<ITreeViewController> treeViewController(ProductTreePanelCtrlView, UseDefaultIID());
			if(treeViewController==nil)
			{
				break;
			}
			NodeIDList selectedItem;
			treeViewController->GetSelectedItems(selectedItem);

			if(selectedItem.size() < 1)
			{
				break;
			}
				
			NodeID nid = selectedItem[0];

			TreeNodePtr<UIDNodeID>uidNodeIDTemp(nid);

			int32 uid= uidNodeIDTemp->GetUID().Get();
			
			//UID TextFrameuid = uidNodeIDTemp->GetUID();
			//TSDataNode pNode;
			//TSTreeDataCache dc;
			//dc.isExist(uid, pNode);
			//PMString pfName = pNode.getName();
			//CA("Table Name : "+pfName);

			if(SelectedRowNo == 3)
			{
				//CA("SelectedRowNo == 3");
					
				if(uid == PRDataNodeList.size())
					break;
				int32 tableInfoSize=0;
				int32 hybridTableSize=0;
				if(tableInfo != NULL)
				{
					tableInfoSize =static_cast<int32>( tableInfo->size());
				}

				int32 NodeListsize=static_cast<int32>(PRDataNodeList.size());
				if(hybridTableInfo != NULL)
				{
					hybridTableSize =static_cast<int32> (hybridTableInfo->size());
				}

				int32 hitCount=0;	
				hitCount=PRDataNodeList.at(uid-1).getHitCount();
				if(hitCount == 8)
				{
			
					if(uid == tableInfoSize)
						break;

					vector<TSDataNode>ProductDataNode;	
					ProductDataNode.push_back(PRDataNodeList[uid-1]);
					ProductDataNode.push_back(PRDataNodeList[uid]);
					
					PRDataNodeList[uid-1] = ProductDataNode[1];
					PRDataNodeList[uid] = ProductDataNode[0];
					
					vector<CItemTableValue> screenTableInfo;
					
					screenTableInfo.push_back(tableInfo->at(uid-1));
					screenTableInfo.push_back(tableInfo->at(uid));

					tableInfo->at(uid-1) = screenTableInfo.at(1);
					tableInfo->at(uid) = screenTableInfo.at(0);
					
					vector<TSTreeInfo> treeInfo;
					TSTreeInfo PRTreeInfo;
					TSTreeData PRTreeData;
					PRTreeInfo = PRTreeData.getData(SelectedRowNo,uid-1);
					treeInfo.push_back(PRTreeInfo);
					PRTreeInfo = PRTreeData.getData(SelectedRowNo,uid);
					treeInfo.push_back(PRTreeInfo);
					
					PRTreeData.changeData(SelectedRowNo,uid-1,treeInfo[1]);
					PRTreeData.changeData(SelectedRowNo,uid,treeInfo[0]);


				}
				if(hitCount == 2)
				{
					//CA("hitCount == 2");
					
					vector<TSDataNode>ProductDataNode;	

					ProductDataNode.push_back(PRDataNodeList[uid-1]);
					ProductDataNode.push_back(PRDataNodeList[uid]);
					
					PRDataNodeList[uid-1] = ProductDataNode[1];
					PRDataNodeList[uid] = ProductDataNode[0];
					
					vector<CAdvanceTableScreenValue> hybridScreenTableInfo;
								
					hybridScreenTableInfo.push_back(hybridTableInfo->at(uid-tableInfoSize-1));
					hybridScreenTableInfo.push_back(hybridTableInfo->at(uid-tableInfoSize));

					hybridTableInfo->at(uid-tableInfoSize-1) = hybridScreenTableInfo.at(1);
					hybridTableInfo->at(uid-tableInfoSize) = hybridScreenTableInfo.at(0);
					
					vector<TSTreeInfo> treeInfo;
					TSTreeInfo PRTreeInfo;
					TSTreeData PRTreeData;
			
					PRTreeInfo = PRTreeData.getData(SelectedRowNo,uid-1);
					treeInfo.push_back(PRTreeInfo);
					PRTreeInfo = PRTreeData.getData(SelectedRowNo,uid);
					treeInfo.push_back(PRTreeInfo);
					
					PRTreeData.changeData(SelectedRowNo,uid-1,treeInfo[1]);
					PRTreeData.changeData(SelectedRowNo,uid,treeInfo[0]);

				}
			}
			if(SelectedRowNo == 4)
			{
				//CA("SelectedRowNo == 4");
				
				if(uid == ITEMDataNodeList.size())
					break;
				int32 tableInfoSize=0;
				int32 hybridTableSize=0;
				if(typeTableValObj != NULL)
				{
					tableInfoSize = static_cast<int32>(typeTableValObj->size());
				}

				int32 NodeListsize=static_cast<int32>(ITEMDataNodeList.size());
				if(typeHybridTableValObj != NULL)
				{
					hybridTableSize = static_cast<int32>(typeHybridTableValObj->size());
				}

				int32 hitCount=0;	
				hitCount=ITEMDataNodeList.at(uid-1).getHitCount();
				if(hitCount == 8)
				{
	
					if(uid == tableInfoSize)
						break;
					
					vector<TSDataNode>ItemDataNode;	
					ItemDataNode.push_back(ITEMDataNodeList[uid-1]);
					ItemDataNode.push_back(ITEMDataNodeList[uid]);
					
					ITEMDataNodeList[uid-1] = ItemDataNode[1];
					ITEMDataNodeList[uid] = ItemDataNode[0];
					
					vector<CItemTableValue> screenTableInfo;
					
					screenTableInfo.push_back(typeTableValObj->at(uid-1));
					screenTableInfo.push_back(typeTableValObj->at(uid));

					typeTableValObj->at(uid-1) = screenTableInfo.at(1);
					typeTableValObj->at(uid) = screenTableInfo.at(0);
					
					
					vector<TSTreeInfo> treeInfo;
					TSTreeInfo ItemTreeInfo;
					TSTreeData ItemTreeData;
					ItemTreeInfo = ItemTreeData.getData(SelectedRowNo,uid-1);
					treeInfo.push_back(ItemTreeInfo);
					ItemTreeInfo = ItemTreeData.getData(SelectedRowNo,uid);
					treeInfo.push_back(ItemTreeInfo);
					
					ItemTreeData.changeData(SelectedRowNo,uid-1,treeInfo[1]);
					ItemTreeData.changeData(SelectedRowNo,uid,treeInfo[0]);


				}
				if(hitCount == 2)
				{
					//CA("hitCount == 2");
					
					vector<TSDataNode>ItemDataNode;	

					ItemDataNode.push_back(ITEMDataNodeList[uid-1]);
					ItemDataNode.push_back(ITEMDataNodeList[uid]);
					
					ITEMDataNodeList[uid-1] = ItemDataNode[1];
					ITEMDataNodeList[uid] = ItemDataNode[0];
					
					vector<CAdvanceTableScreenValue> hybridScreenTableInfo;
									
					hybridScreenTableInfo.push_back(typeHybridTableValObj->at(uid-tableInfoSize-1));
					hybridScreenTableInfo.push_back(typeHybridTableValObj->at(uid-tableInfoSize));

					typeHybridTableValObj->at(uid-tableInfoSize-1) = hybridScreenTableInfo.at(1);
					typeHybridTableValObj->at(uid-tableInfoSize) = hybridScreenTableInfo.at(0);
											
					vector<TSTreeInfo> treeInfo;
					TSTreeInfo ItemTreeInfo;
					TSTreeData ItemTreeData;
			
					ItemTreeInfo = ItemTreeData.getData(SelectedRowNo,uid-1);
					treeInfo.push_back(ItemTreeInfo);
					ItemTreeInfo = ItemTreeData.getData(SelectedRowNo,uid);
					treeInfo.push_back(ItemTreeInfo);
					
					ItemTreeData.changeData(SelectedRowNo,uid-1,treeInfo[1]);
					ItemTreeData.changeData(SelectedRowNo,uid,treeInfo[0]);

				}
			}

			IControlView* TreeCtrlView = TSMediatorClass::iPanelCntrlDataPtr->FindWidget(kTSTreeViewWidgetID);
			if(TreeCtrlView==nil)
			{
				//CA("TreeCtrlView==nil");
				break;
			}
			InterfacePtr<ITreeViewMgr> treeViewMgr(TreeCtrlView, UseDefaultIID());
			if(!treeViewMgr) { 
				//CA("!treeViewMgr");
				break;
			}
			TSTreeModel pModel;
			PMString root("Root");
							
			switch(SelectedRowNo) 
			{
				case 0:
					pModel.setRoot(-1, root, 1);
					break;
				case 1:
					pModel.setRoot(-1, root, 2);
					break;
				case 2:
					pModel.setRoot(-1, root, 3);
					break;
				case 3:
					pModel.setRoot(-1, root, 4);
					break;
				case 4:
					pModel.setRoot(-1, root, 5);
					break;
			}		
			treeViewMgr->ClearTree(kTrue);
			pModel.GetRootUID();
			treeViewMgr->ChangeRoot();

			treeViewController->Select(nid,kTrue,kTrue); 
			//}
		}
		
		if(theSelectedWidget==kTSPreviewButtonWidgetID	&& theChange==kTrueStateMessage)
		{
			//CA("Preview");
		}
		//if(theSelectedWidget==kTSEditStencilButtonWidgetID	&& theChange==kTrueStateMessage)
		//{
			//CA("Edit Stencil");
		//}
		if((theSelectedWidget==kTSSprayButtonWidgetID || theSelectedWidget==kTSSprayAllButtonWidgetID) && theChange==kTrueStateMessage)
		{
			//CA("Spray Button");
			SprayAllButtonSelected = kFalse;

			if( theSelectedWidget==kTSSprayAllButtonWidgetID)
			{
				SprayAllButtonSelected =  kTrue;
			}

			AcquireWaitCursor awc;
			awc.Animate(); 
			InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
			if(iSSSprayer==nil)
			{
				CA("Plugin Ap_SubSectionSprayer.pln was not found in the plugins directory of adobe.");
				break;
			}
			/*if(!iSSSprayer->getHorizFlowType())
			{
				CA("if(!iSSSprayer->getHorizFlowType())");
			}
			else if(iSSSprayer->getHorizFlowType())
			{
				CA("iSSSprayer->getHorizFlowType()");
			}*/
			
			ptrIAppFramework->clearAllStaticObjects();

			PMString itemFieldIds("");
			PMString itemAssetTypeIds("");
			PMString itemGroupFieldIds("");
			PMString itemGroupAssetTypeIds("");
			PMString listTypeIds("");
			PMString listItemFieldIds("");
			bool16 isSprayItemPerFrameFlag = kFalse;
			double langId = ptrIAppFramework->getLocaleId();

			PMString currentSectionitemGroupIds("");
			PMString currentSectionitemIds("");
			CSprayStencilInfoVector.clear();
			CSprayStencilInfo objCSprayStencilInfo;

			UIDList selectUIDList;
			this->getSelectedBoxIds(selectUIDList);

			bool16 result = this->getStencilInfo(selectUIDList, objCSprayStencilInfo);
			if(result == kFalse)
				break;

			CSprayStencilInfoVector.push_back(objCSprayStencilInfo);
			vectorCSprayStencilInfo::iterator itr;

			for(itr = CSprayStencilInfoVector.begin();itr != CSprayStencilInfoVector.end(); itr++)
			{
				// Added by Apsiva for New JSON section Call.
				// collecting listTypes for Item Group and iTem
				if(  itr->dBTypeIds.size() > 0)
				{
					for(int ct =0; ct < itr->dBTypeIds.size(); ct++ )
					{
						listTypeIds.AppendNumber(PMReal(itr->dBTypeIds.at(ct)));
						if(ct != (itr->dBTypeIds.size() -1) )
						{
							listTypeIds.Append(",");
						}
					}
				}
				// collecting Copy Attributes for Item Group
				if(  itr->ProductAttributeIds.size() > 0)
				{
					for(int ct =0; ct < itr->ProductAttributeIds.size(); ct++ )
					{
						itemGroupFieldIds.AppendNumber(PMReal(itr->ProductAttributeIds.at(ct)));
						if(ct != (itr->ProductAttributeIds.size() -1) )
						{
							itemGroupFieldIds.Append(",");
						}
					}
				}
			
				if(itr->productPVAssetIdList.size()>0)
				{
					if(itemGroupFieldIds.NumUTF16TextChars () > 0)
					{
						itemGroupFieldIds.Append(",");
					}

					for(int32 i = 0; i < itr->productPVAssetIdList.size(); i++)
					{
						itemGroupFieldIds.AppendNumber(PMReal(itr->productPVAssetIdList[i]));
						if( i < itr->productPVAssetIdList.size() -1 )
							itemGroupFieldIds.Append(",");	
					}
				}

				// collecting Copy Attributes for Item
				if(  itr->itemAttributeIds.size() > 0)
				{
					for(int ct =0; ct < itr->itemAttributeIds.size(); ct++ )
					{
						itemFieldIds.AppendNumber(PMReal(itr->itemAttributeIds.at(ct)));
						if(ct != (itr->itemAttributeIds.size() -1) )
						{
							itemFieldIds.Append(",");
						}
					}
				}

				if(itr->itemPVAssetIdList.size()>0)
				{
					if(itemFieldIds.NumUTF16TextChars () > 0)
					{
						itemFieldIds.Append(",");
					}

					for(int32 i = 0; i < itr->itemPVAssetIdList.size(); i++)
					{
						itemFieldIds.AppendNumber(PMReal(itr->itemPVAssetIdList[i]));
						if( i < itr->itemPVAssetIdList.size() -1 )
							itemFieldIds.Append(",");	
					}
				}
				// collecting Asset type Ids for Item Group
				if(  itr->ProductAssetIds.size() > 0)
				{
					for(int ct =0; ct < itr->ProductAssetIds.size(); ct++ )
					{
						itemGroupAssetTypeIds.AppendNumber(PMReal(itr->ProductAssetIds.at(ct)));
						if(ct != (itr->ProductAssetIds.size() -1) )
						{
							itemGroupAssetTypeIds.Append(",");
						}
					}
				}
				// collecting Asset type Ids for Item
				if(  itr->itemAssetIds.size() > 0)
				{
					for(int ct =0; ct < itr->itemAssetIds.size(); ct++ )
					{
						itemAssetTypeIds.AppendNumber(PMReal(itr->itemAssetIds.at(ct)));
						if(ct != (itr->itemAssetIds.size() -1) )
						{
							itemAssetTypeIds.Append(",");
						}
					}
				}
			
				// collecting listItemFieldIds for Item
				if(  itr->childItemAttributeIds.size() > 0)
				{
					for(int ct =0; ct < itr->childItemAttributeIds.size(); ct++ )
					{
						listItemFieldIds.AppendNumber(PMReal(itr->childItemAttributeIds.at(ct)));
						if(ct != (itr->childItemAttributeIds.size() -1) )
						{
							listItemFieldIds.Append(",");
						}
					}
				}
				
				if (isSprayItemPerFrameFlag == kFalse)
				{
					isSprayItemPerFrameFlag = itr->isSprayItemPerFrame;
				}
			}
				
			if(TSMediatorClass::isProduct == 3)
			{
				//CA("isProductOrItem == 3");
				
				int32 localisProduct =1;
				//ptrIAppFramework->EventCache_setCurrentObjectData(TSMediatorClass::sectionID, TSMediatorClass::objectID, localisProduct , langId, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, itemGroupAssetTypeIds,  listTypeIds, listItemFieldIds);
				currentSectionitemGroupIds.AppendNumber(PMReal(TSMediatorClass::objectID));

			}
			else if(TSMediatorClass::isProduct == 4)
			{
				//CA("isProductOrItem == 4");
				
				int32 localisProduct =0;
				//ptrIAppFramework->EventCache_setCurrentObjectData(TSMediatorClass::sectionID, TSMediatorClass::objectID, localisProduct , langId, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, itemGroupAssetTypeIds,  listTypeIds, listItemFieldIds);
				currentSectionitemIds.AppendNumber(PMReal(TSMediatorClass::objectID));

			}

			ptrIAppFramework->EventCache_setCurrentSectionData( TSMediatorClass::sectionID, langId , currentSectionitemGroupIds, currentSectionitemIds, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds,itemGroupAssetTypeIds,listTypeIds, listItemFieldIds, kFalse, isSprayItemPerFrameFlag);


			ItemItemGroupSpray itemItemGroupSprayObj;
			itemItemGroupSprayObj.startSpraying();
			
			ptrIAppFramework->clearAllStaticObjects();
			
		}

	}while(kFalse);
}




IPanelControlData* TSSelectionObserver::QueryPanelControlData()
{
	IPanelControlData* iPanel = nil;
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return iPanel;
	}
	do
	{
		InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID());
		if (iWidgetParent == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::QueryPanelControlData::iWidgetParent == nil");
			return iPanel;
		}

		InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
		if (iPanelControlData == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TABLEsource::TSSelectionObserver::QueryPanelControlData::iPanelControlData == nil");
			break;
		}
		iPanelControlData->AddRef();
		iPanel = iPanelControlData;

	}
	while (kFalse); 
	return iPanel;
}

void TSSelectionObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->AttachObserver(this, interfaceID);
	}
	while (kFalse); 
}

void TSSelectionObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->DetachObserver(this, interfaceID);
	}
	while (kFalse); 
}


bool16 TSSelectionObserver::getStencilInfo(const UIDList& SelUIDList,CSprayStencilInfo& objCSprayStencilInfo)
{
	//CA("SubSectionSprayer::getStencilInfo");
	bool16 result = kFalse;

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return result;
	}

	InterfacePtr<ITagReader> itagReader
		((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		return result;
	}

	InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
	if(!DataSprayerPtr)
	{
		ptrIAppFramework->LogDebug("ProductFinderPalete::SubSectionSprayer::getStencilInfo:Pointer to DataSprayerPtr not found");//
		return result;
	}


	do
	{
		for(int32 index = 0; index < SelUIDList.Length(); index++)
		{
			UIDRef boxUIDRef = SelUIDList.GetRef(index);
			
			TagList tList=itagReader->getTagsFromBox(boxUIDRef);
			int numTags=static_cast<int>(tList.size());
			if(numTags<0)
			{
				//CA("numTags<0");
			}

			/*PMString numOfTags("numTags = ");
			numOfTags.AppendNumber(numTags);
			CA(numOfTags);*/

			if(numTags<=0)//This can be a Tagged Frame
			{	
				tList=itagReader->getFrameTags(boxUIDRef);				
						
			}	

			int numTags1=static_cast<int>(tList.size());
			if(numTags1<0)
			{
				//CA("numTags1<0");
				continue;
			}

			bool16 isCustomTablePresent = kFalse;
			TagList tableTagList1;
			for(int32 tagIndex = 0; tagIndex < tList.size(); tagIndex++)
			{
				TagStruct & tagInfo = tList[tagIndex];

				objCSprayStencilInfo.langId = tagInfo.languageID;
				if(tagInfo.isSprayItemPerFrame != -1)	
				{
					objCSprayStencilInfo.isChildTag = kTrue;
					objCSprayStencilInfo.isCustomTablePresent = kTrue;
					objCSprayStencilInfo.isSprayItemPerFrame = kTrue;
				}
				
				
				
				if(tagInfo.isTablePresent)
				{
					//CA("Table Present");
					if(tagInfo.whichTab == 3 && tagInfo.tableType == 2)
					{
						isCustomTablePresent = kTrue;
						objCSprayStencilInfo.isProductDBTable = kTrue;
						if(tagInfo.typeId > 0)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);
						
						TagList tableTagList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxUIDRef);
						int32 numTags=static_cast<int>(tableTagList.size());
						if(numTags < 0)
						{
							//CA("numTags<0");
							continue;
						}

						//PMString numOfTags("numTags 33333333333= ");
						//numOfTags.AppendNumber(numTags);
						//CA(numOfTags);

						for(int32 index1 = 0; index1 < numTags; index1++)
						{
							TagStruct & tagInfoo = tableTagList[index1];
							tableTagList1.push_back(tagInfoo);
						}						
					}
					else if(tagInfo.whichTab == 4  && tagInfo.tableType == 2)
					{
						isCustomTablePresent = kTrue;

						objCSprayStencilInfo.isDBTable = kTrue;
						objCSprayStencilInfo.isCustomTablePresent = kTrue;
						if(tagInfo.typeId > 0)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);

						TagList tableTagList=itagReader->getTagsFromBox_ForRefresh_ByAttribute(boxUIDRef);
						int numTags=static_cast<int>(tableTagList.size());
						if(numTags<0)
						{
							//CA("numTags<0");
							continue;
						}

						//PMString numOfTags("numTags 2222222222= ");
						//numOfTags.AppendNumber(numTags);
						//CA(numOfTags);

						for(int32 index1 = 0; index1 < numTags; index1++)
						{
							TagStruct & tagInfoo = tableTagList[index1];
							tableTagList1.push_back(tagInfoo);
						}	
					}

					
					continue;
				 }
			}

			
			if(isCustomTablePresent)
			{
				int numTags=static_cast<int32>(tableTagList1.size());
				if(numTags > 0)
				{
					//CA("numTags<0");
					//PMString numOfTags("numTags = ");
					//numOfTags.AppendNumber(numTags);
					//CA(numOfTags);

					for(int32 index2 = 0; index2 < numTags; index2++)
					{
						TagStruct & tagInfoo = tableTagList1[index2];
						tList.push_back(tagInfoo);
					}	
									
					int numTagsnew=static_cast<int>(tList.size());
					if(numTagsnew < 0)
					{
						//CA("numTags<0");
						
					}

					//PMString numOfTagss("numTags 11111111111= ");
					//numOfTagss.AppendNumber(numTagsnew);
					//CA(numOfTagss);
				}
			}

			for(int32 tagIndex = 0; tagIndex < tList.size(); tagIndex++)
			{
				TagStruct & tagInfo = tList[tagIndex];
				objCSprayStencilInfo.langId = tagInfo.languageID;
				if(tagInfo.isTablePresent)
				{
					if(tagInfo.elementId == -115 && tagInfo.whichTab == 3)
					{
						objCSprayStencilInfo.isProductHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.elementId == -115 && tagInfo.whichTab == 4)
					{
						objCSprayStencilInfo.isHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.elementId == -115 && tagInfo.whichTab == 5)
					{
						objCSprayStencilInfo.isSectionLevelHyTable = kTrue;
						objCSprayStencilInfo.HyTypeIds.push_back(tagInfo.typeId);
					}
					else if(tagInfo.whichTab == 3)
					{
						objCSprayStencilInfo.isProductDBTable = kTrue;
						if(tagInfo.typeId > 0)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);						
					}
					else if(tagInfo.whichTab == 4)
					{
						objCSprayStencilInfo.isDBTable = kTrue;
						if(tagInfo.typeId > 0)
							objCSprayStencilInfo.dBTypeIds.push_back(tagInfo.typeId);
					}

					if(tagInfo.whichTab == 3)
					{
						objCSprayStencilInfo.isProductCopy = kTrue;
					}
						
					objCSprayStencilInfo.isCopy = kTrue;
					objCSprayStencilInfo.isProductChildTag = kTrue;
					
					if( (tagInfo.childTag == 1) && (tagInfo.whichTab == 4))
						objCSprayStencilInfo.isChildTag = kTrue;
					continue;
				}

				if(tagInfo.imgFlag == 1)
				{
					//CA("image Present");
					//objCSprayStencilInfo.isAsset = kTrue;
					if(tagInfo.typeId > 0)
						objCSprayStencilInfo.AssetIds.push_back(tagInfo.typeId);
					
					if(tagInfo.whichTab == 3)
					{
						if(tagInfo.typeId > 0)
							objCSprayStencilInfo.ProductAssetIds.push_back(tagInfo.typeId);
						if(tagInfo.typeId <= -207 && tagInfo.typeId >= -221)
						{
							objCSprayStencilInfo.isProductAsset = kTrue; //-------
							objCSprayStencilInfo.isProductBMSAssets = kTrue;
						}
						else if(tagInfo.elementId > 0)
						{
							objCSprayStencilInfo.isProductPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.productPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.productPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.productPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.productPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.productPVAssetIdList.push_back(tagInfo.elementId);
						}
						else
							objCSprayStencilInfo.isProductAsset = kTrue;

					}
					else if(tagInfo.whichTab == 4)
					{
						if(tagInfo.typeId > 0)
							objCSprayStencilInfo.itemAssetIds.push_back(tagInfo.typeId);

						objCSprayStencilInfo.isProductChildTag = kTrue;

						if(tagInfo.typeId <= -207 && tagInfo.typeId >= -221)
							objCSprayStencilInfo.isBMSAssets = kTrue;
						else if(tagInfo.elementId > 0)
						{
							objCSprayStencilInfo.isItemPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.itemPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.itemPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.itemPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.itemPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.itemPVAssetIdList.push_back(tagInfo.elementId);
						}
						else
							objCSprayStencilInfo.isAsset = kTrue;

					}
					else if(tagInfo.whichTab == 5)
					{
						if(tagInfo.typeId == -222 || tagInfo.typeId == -223)
							objCSprayStencilInfo.isSectionLevelBMSAssets = kTrue;
						else if(tagInfo.colno == -28)
						{
							objCSprayStencilInfo.isSectionPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.sectionPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.sectionPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.sectionPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.sectionPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.sectionPVAssetIdList.push_back(tagInfo.elementId);
						}
						else if(tagInfo.colno == -27)
						{
							objCSprayStencilInfo.isPublicationPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.publicationPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.publicationPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.publicationPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.publicationPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.publicationPVAssetIdList.push_back(tagInfo.elementId);
						}
						else if(tagInfo.colno > 0)
						{
							objCSprayStencilInfo.isCatagoryPVMPVAssets = kTrue;
							if(objCSprayStencilInfo.catagoryPVAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.catagoryPVAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.catagoryPVAssetIdList[i] == tagInfo.elementId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									objCSprayStencilInfo.catagoryPVAssetIdList.push_back(tagInfo.elementId);
							}
							else
								objCSprayStencilInfo.catagoryPVAssetIdList.push_back(tagInfo.elementId);
						}
						else if(tagInfo.catLevel < 0 )
						{
							objCSprayStencilInfo.isEventSectionImages = kTrue;
							if(objCSprayStencilInfo.eventSectionAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.eventSectionAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.eventSectionAssetIdList[i] == tagInfo.typeId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									{
										if(tagInfo.typeId > 0)
										objCSprayStencilInfo.eventSectionAssetIdList.push_back(tagInfo.typeId);
									}
							}
							else
								{
									if(tagInfo.typeId > 0)
									objCSprayStencilInfo.eventSectionAssetIdList.push_back(tagInfo.typeId);
								}
						}
						else if(tagInfo.catLevel > 0)
						{
							objCSprayStencilInfo.isCategoryImages = kTrue;
							if(objCSprayStencilInfo.categoryAssetIdList.size()>0)
							{
								bool16 isAlreadyPresent = kFalse;
								for(int32 i = 0; i < objCSprayStencilInfo.categoryAssetIdList.size(); i++)
								{
									if(objCSprayStencilInfo.categoryAssetIdList[i] == tagInfo.typeId)
									{
										isAlreadyPresent = kTrue;
										break;
									}
								}

								if(!isAlreadyPresent)
									{
										if(tagInfo.typeId > 0)
										objCSprayStencilInfo.categoryAssetIdList.push_back(tagInfo.typeId);
									}
							}
							else
								{
									if(tagInfo.typeId > 0)
									objCSprayStencilInfo.categoryAssetIdList.push_back(tagInfo.typeId);
								}
						}

					}	

					continue;
				}
				
				//CA("copy Attribute");
				if(tagInfo.elementId > 0)
				objCSprayStencilInfo.AttributeIds.push_back(tagInfo.elementId);

				if(tagInfo.whichTab == 3)
				{
					objCSprayStencilInfo.isProductCopy = kTrue;
					if(tagInfo.elementId > 0)
					objCSprayStencilInfo.ProductAttributeIds.push_back(tagInfo.elementId);
				}
				if(tagInfo.whichTab == 4)
				{
					objCSprayStencilInfo.isCopy = kTrue;
					objCSprayStencilInfo.isProductChildTag = kTrue;
					if(tagInfo.elementId > 0)
						objCSprayStencilInfo.itemAttributeIds.push_back(tagInfo.elementId);
					else if( tagInfo.elementId == -703 || tagInfo.elementId == -704)
					{
						double eventPriceId = -1;
						double regularPriceId = -1;	
						eventPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_SALE_PRICE");
						regularPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_REGULAR_PRICE");	
						objCSprayStencilInfo.itemAttributeIds.push_back(eventPriceId);
						objCSprayStencilInfo.itemAttributeIds.push_back(regularPriceId);
					}
				}
				if(tagInfo.whichTab == 5)
					objCSprayStencilInfo.isSectionCopy = kTrue;
				

				/*if(!isSpreadBasedLetterKeys)
				{
					if(tagInfo.elementId == -803 && tagInfo.typeId == 1)
					{
						isSpreadBasedLetterKeys = kTrue;
					}
					else 
					{
						isSpreadBasedLetterKeys = kFalse;
					}
				}*/

				if(tagInfo.childTag == 1)
				{
					objCSprayStencilInfo.isChildTag = kTrue;
					if(tagInfo.elementId > 0)
						objCSprayStencilInfo.childItemAttributeIds.push_back(tagInfo.elementId);
					else if( tagInfo.elementId == -703 || tagInfo.elementId == -704)
					{
						double eventPriceId = -1;
						double regularPriceId = -1;	
						eventPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_SALE_PRICE");
						regularPriceId = ptrIAppFramework->ConfigCache_getintConfigValue1ByConfigName("EVENT_REGULAR_PRICE");	
						objCSprayStencilInfo.childItemAttributeIds.push_back(eventPriceId);
						objCSprayStencilInfo.childItemAttributeIds.push_back(regularPriceId);
					}
				}

				if(tagInfo.isEventField == 1)
					objCSprayStencilInfo.isEventField = kTrue;

				if(tagInfo.isSprayItemPerFrame != -1)	
				{
					objCSprayStencilInfo.isChildTag = kTrue;
					objCSprayStencilInfo.isCustomTablePresent = kTrue;
					objCSprayStencilInfo.isSprayItemPerFrame = kTrue;
				}
				
				
				

			}
			//------------
			for(int32 tagIndex = 0 ; tagIndex < tList.size() ; tagIndex++)
			{
				tList[tagIndex].tagPtr->Release();
			}
		}
	}while(kFalse);
	result = kTrue;
	return result;
}


bool16 TSSelectionObserver::getSelectedBoxIds(UIDList& selectUIDList)
{	
	InterfacePtr<ITagReader> itagReader
		   ((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
		return kFalse;

	InterfacePtr<ISelectionManager>	iSelectionManager (Utils<ISelectionUtils> ()->QueryActiveSelection ());
	if(!iSelectionManager)
		return kFalse;
	InterfacePtr<ITextMiscellanySuite> txtMisSuite(static_cast<ITextMiscellanySuite* >
	( Utils<ISelectionUtils>()->QuerySuite(ITextMiscellanySuite::kDefaultIID,iSelectionManager))); 
	if(!txtMisSuite)
		return kFalse; 
	txtMisSuite->GetUidList(selectUIDList);
	const int32 listLength=selectUIDList.Length();
	if(listLength==0)
		return kFalse;
	///////////////////////for passing database pointer////////////////
	InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()/*::QueryFrontLayoutData()*/); //Cs4
	if (layoutData == nil)
		return kFalse;
	

	IDocument* document = (IDocument*)layoutData->GetDocument();
	if (document == nil)
		return kFalse;

	IDataBase* database = ::GetDataBase(document);
	if(!database)
		return kFalse;

	UIDList TempUIDList(database);
	TagList NewList;
	
	UIDList tempList(selectUIDList.GetDataBase());
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		if(!iHier)
		{
			//CA(" !iHier >> Continue ");
			continue;
		}
		UID kidUID;
		
		int32 numKids=iHier->GetChildCount();
		//PMString ASD("numKids : ");
		//ASD.AppendNumber(numKids);
		//CA(ASD);

		bool16 isGroupFrame = kFalse ;
		isGroupFrame = Utils<IPageItemTypeUtils>()->IsGroup(selectUIDList.GetRef(i));

		if(isGroupFrame == kTrue) 
		{
			IIDXMLElement* ptr = NULL;
			for(int j=0;j<numKids;j++)
			{
				kidUID=iHier->GetChildUID(j);
				UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);	


				InterfacePtr<IHierarchy> iHierarchy(boxRef, UseDefaultIID());
				if(!iHierarchy)
				{
					//CA(" !iHier >> Continue ");
					continue;
				}
				UID newkidUID;
				
				int32 numNewKids=iHierarchy->GetChildCount();
				/*PMString ASD("numKids ................: ");
				ASD.AppendNumber(numKids);
				CA(ASD);*/

				bool16 isGroupFrameAgain = kFalse ;
				isGroupFrameAgain = Utils<IPageItemTypeUtils>()->IsGroup(boxRef);

				if(isGroupFrameAgain == kTrue) 
				{
					IIDXMLElement* newPtr = NULL;
					for(int k=0;k<numNewKids;k++)
					{
						//CA("Inside For Loop");
						newkidUID=iHierarchy->GetChildUID(k);
						UIDRef childBoxRef(selectUIDList.GetDataBase(), newkidUID);	

					
						//CA("isGroupFrame == kTrue");
						TagList NewList = itagReader->getTagsFromBox(childBoxRef, &newPtr);
						
						/*PMString s("NewList.size() : ");
						s.AppendNumber(NewList.size());
						CA(s);*/
					
					

						if(!this->doesExist(NewList,tempList))
						{
							tempList.Append(newkidUID);				
							//CA("After Appending to tempList");
						}
						//------------
						for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
						{
							NewList[tagIndex].tagPtr->Release();
						}
					}
				}
				else
				{
					TagList NewList = itagReader->getTagsFromBox(boxRef, &ptr);
					
					/*PMString s("NewList.size() : ");
					s.AppendNumber(NewList.size());
					CA(s);*/
				
				

					if(!doesExist(NewList,tempList))
					{
						tempList.Append(kidUID);				
					}
					for(int32 tagIndex = 0 ; tagIndex < NewList.size() ; tagIndex++)
					{
						NewList[tagIndex].tagPtr->Release();
					}
				}			
			}
		}
		else
		{
			//CA("isGroupFrame == kFalse");
			tempList.Append(selectUIDList.GetRef(i).GetUID());
		}
	}

	UIDList OriginalFramesUIDList(selectUIDList.GetDataBase());
	OriginalFramesUIDList = selectUIDList;
	selectUIDList = tempList;

	for(int i=0; i<selectUIDList.Length(); i++)
	{
		//InterfacePtr<IHierarchy> iHier(selectUIDList.GetRef(i), UseDefaultIID());
		//if(!iHier)
		//	continue;

		//UID kidUID;
		//int32 numKids=iHier->GetChildCount();
		/*PMString temp = "";
		temp.AppendNumber(numKids);
		CA("childCount = " + temp);*/

		IIDXMLElement* ptr = NULL;

//		for(int j=0;j<numKids;j++)
//	   {
//		   kidUID=iHier->GetChildUID(j);
			
		 /*  PMString uid ="kidUID = ";
		   uid.AppendNumber((uint32)kidUID.Get());
		   uid.Append(" , ");
		   UID temp = selectUIDList.GetRef(i).GetUID();
		   uid.AppendNumber((uint32)temp.Get());
		   CA(uid);*/
		
//		   UIDRef boxRef(selectUIDList.GetDataBase(), kidUID);
		   
			
		    NewList=itagReader->getTagsFromBox(selectUIDList.GetRef(i), &ptr);
			vector<TagStruct>::iterator itertr;
	//Push the text and image tag which we got from getTagsFromBox in the NewList into vector.	
			for(itertr= NewList.begin();itertr!=NewList.end();itertr++)
			{
				PMString story;
				int32 index=0;
				IIDXMLElement* tempXml = itertr->tagPtr;//to get tag From Vector in PMString Format it is required.
				if(tempXml)
				story=tempXml->GetTagString();
				//CA(story);
				PMString tagName("");
				//To remove "_" which present in between single tag name and repace it with " "
				for(int i=0;i<story.NumUTF16TextChars();i++)
				{
					if(story.GetChar(i) =='_')
						tagName.Append(" ");
					else
						tagName.Append(story.GetChar(i));
				}
				
			}
		//This is get called when there is/are Selected Boxes.		
			if(NewList.size()<=0)//This can be a Tagged Frame
			 {	
				bool16 flaG = kFalse;
				//InterfacePtr<ITagReader> itagReader
				//((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
				/*if(!itagReader)
					return 0;*/
				NewList.clear();
				NewList=itagReader->getFrameTags(selectUIDList.GetRef(i));
				//insert box tag into Vector.
				vector<TagStruct>::iterator itertr;
				for( itertr= NewList.begin();itertr!=NewList.end();itertr++)
				{
					PMString story;
					int32 index=0;
					IIDXMLElement* tempXml = itertr->tagPtr;
					if(tempXml)
					story=tempXml->GetTagString();
					//CA(story);
					PMString tagName("");
					//To remove "_" which present in tag name and replace it with " "
					for(int i=0;i<story.NumUTF16TextChars();i++)
					{
						if(story.GetChar(i) =='_')
							tagName.Append(" ");
						else
							tagName.Append(story.GetChar(i));
					}
					
					if(NewList.size()==0)
						return 0;
			}
			//	}
		 }

			for(int32 tagIndex = 0 ; tagIndex < NewList.size(); tagIndex++)
			{
				NewList[tagIndex].tagPtr->Release();
			}
	}

	selectUIDList = OriginalFramesUIDList;
	//-----------
	

	return kTrue;
}

bool16 TSSelectionObserver::doesExist(TagList &tagList,const UIDList &selectUIDList)
{
	bool16 result = kFalse;
	InterfacePtr<ITagReader> itagReader((static_cast<ITagReader*> (CreateObject(kTGRTagReaderBoss,IID_ITAGREADER))));
	if(!itagReader)
	{ 
		return kFalse;
	}
	TagList tList;
	for(int i=0; i<selectUIDList.Length(); i++)
	{
		tList = itagReader->getTagsFromBox(selectUIDList.GetRef(i));

		if(tList.size()==0||tagList.size()==0 || !tList[0].tagPtr || !tagList[0].tagPtr )
			continue;

		if(tagList[0].tagPtr == tList[0].tagPtr )
		{
			//CA("return kTrue");
			result = kTrue;
		}

		for(int32 tagIndex = 0 ; tagIndex < tList.size(); tagIndex++)
		{
			tList[tagIndex].tagPtr->Release();
		}
		if(result)
			break;
	}
	//CA("return kFalse");
	return result;
}
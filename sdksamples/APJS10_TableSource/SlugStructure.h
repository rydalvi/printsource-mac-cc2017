#ifndef __SLUGSTRUCT_H__
#define __SLUGSTRUCT_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class SlugStruct
{
public:
	double elementId;
	double typeId;
	double parentId;
	int32 imgflag;
	double parentTypeId;
    PMString elementName;
	PMString TagName;
	double LanguageID;			//PMString colName;
	int16 whichTab;
	int32 imgFlag;
	double sectionID;
	int32 isAutoResize;

	//added on 16Jun for Item table spray in tabbed text format
	int32 tableFlag;
	int32 row_no;
	int32 col_no;
	int32 tagStartPos;
	int32 tagEndPos;
	//end 16Jun
	double tableTypeId;  // added for attaching correct type id for table.

	SlugStruct()
	{
		elementId = -1;
		typeId = -1;
		parentId = -1;
		imgflag = -1;
		parentTypeId = -1;
		elementName.Clear();
		TagName.Clear();
		LanguageID = -1;//colName.Clear();
		whichTab = -1;
		imgFlag = -1;
		sectionID = -1;
		isAutoResize = -1;

	//added on 16Jun for Item table spray in tabbed text format
		tableFlag = 0;
		row_no=-1;
		col_no=-1;
		tagStartPos= -1;
		tagEndPos = -1;
	//end 16Jun
		tableTypeId = -1;
	}
};

typedef vector<SlugStruct> SlugList;

#endif
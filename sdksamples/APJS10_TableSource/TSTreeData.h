#ifndef __TSTreeData_H__
#define __TSTreeData_H__

#include "VCPlugInHeaders.h"
#include "vector"

using namespace std;

class TSTreeInfo
{
	public:	
		int16 index;
		double id;
		double typeId;
		double LanguageID;
		PMString name;
		bool16 isSelected;
		UID boxUID;
		bool16 isDragged;
		PMString code;
		bool16 isImageFlag; // kFalse:Text box kTrue:Image box
		int32	tableFlag;	// for Tables // 0: No table 1:Table

		//added on 11July...serial blast day
		int32 TreeType;

		TSTreeInfo()
		{
			index=0;
			id=0;
			typeId=0;
			isSelected=kFalse;
			boxUID=kInvalidUID;
			isDragged=kFalse;
			isImageFlag=kFalse;
			tableFlag=0;
			LanguageID=0;

			//added on 11July...serial blast day
			TreeType = -1;

			name = "";
			code = "";
		}
};

class TSTreeData
{
	private :
		static vector<TSTreeInfo> PFTreeData;
		static vector<TSTreeInfo> PGTreeData;
		static vector<TSTreeInfo> PRTreeData;
		static vector<TSTreeInfo> ItemTreeData;
		static vector<TSTreeInfo> ProjectTreeData;
//added by vijay on 10-10-2006
		//static vector<TSTreeInfo> CatagoryTreeData;
	public:
		void setData
		(int16 , int16 , double , double , double , PMString , bool16 , PMString, bool16, int32);
		TSTreeInfo getData(int16 , int16);
		int32 returnListVectorSize(int16);
		void clearVector(int16);
		void setIsBoxDragged(int16 , int16 , bool16);
		void changeData(int32 treeType , int32 index , TSTreeInfo treeInfo);
};
#endif
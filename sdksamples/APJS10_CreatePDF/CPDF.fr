//========================================================================================
//  
//  $File: $
//  
//  Owner: Catsy
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// General includes:
#include "MenuDef.fh"
#include "ActionDef.fh"
#include "ActionDefs.h"
#include "AdobeMenuPositions.h"
#include "LocaleIndex.h"
#include "PMLocaleIds.h"
#include "StringTable.fh"
#include "ObjectModelTypes.fh"
#include "ShuksanID.h"
#include "ActionID.h"
#include "CommandID.h"
#include "WorkspaceID.h"
#include "WidgetID.h"
#include "BuildNumber.h"
#include "PlugInModel_UIAttributes.h"

#include "InterfaceColorDefines.h"
#include "IControlViewDefs.h"
#include "SysControlIDs.h"
#include "Widgets.fh"	// for PalettePanelWidget or DialogBoss

#include "EveInfo.fh"	// Required when using EVE for dialog layout/widget placement

// Project includes:
#include "CPDFID.h"
#include "GenericID.h"
#include "ShuksanID.h"
#include "TextID.h"


#ifdef __ODFRC__

/*  
 * Plugin version definition.
 */
resource PluginVersion (kSDKDefPluginVersionResourceID)
{
	kTargetVersion,
	kCPDFPluginID,
	kSDKDefPlugInMajorVersionNumber, kSDKDefPlugInMinorVersionNumber,
	kSDKDefHostMajorVersionNumber, kSDKDefHostMinorVersionNumber,
	kCPDFCurrentMajorFormatNumber, kCPDFCurrentMinorFormatNumber,
	{ kInDesignProduct, kInCopyProduct },
	{ kWildFS },
	kUIPlugIn,
	kCPDFVersion
};

/*  
 * The ExtraPluginInfo resource adds extra information to the Missing Plug-in dialog
 * that is popped when a document containing this plug-in's data is opened when
 * this plug-in is not present. These strings are not translatable strings
 * since they must be available when the plug-in isn't around. They get stored
 * in any document that this plug-in contributes data to.
 */
resource ExtraPluginInfo(1)
{
	kCPDFCompanyValue,			// Company name
	kCPDFMissingPluginURLValue,	// URL 
	kCPDFMissingPluginAlertValue,	// Missing plug-in alert text
};

/* 
 * Boss class definitions.
 */
resource ClassDescriptionTable(kSDKDefClassDescriptionTableResourceID)
{{{

	/*
	 * This boss class supports two interfaces:
	 * IActionComponent and IPMPersist.
     *
	 * 
	 * @ingroup apjs10_createpdf
	 */
	Class
	{
		kCPDFActionComponentBoss,
		kInvalidClass,
		{
			// Handle the actions from the menu.
			IID_IACTIONCOMPONENT, kCPDFActionComponentImpl,
			// Persist the state of the menu across application instantiation. Implementation provided by the API.
			IID_IPMPERSIST, kPMPersistImpl
		}
	},

	/*
	 * This boss class implements the dialog for this plug-in. All
	 * dialogs must implement IDialogController. Specialisation of
	 * IObserver is only necessary if you want to handle widget
	 * changes dynamically rather than just gathering their values
	 * and applying in the IDialogController implementation.
	 * In this implementation IObserver is specialised so that the
	 * plug-in's about box is popped when the info button widget
	 * is clicked.
     *
	 * 
	 * @ingroup apjs10_createpdf
	 */
	Class
	{
		kCPDFDialogBoss,
		kDialogBoss,
		{
			// Provides management and control over the dialog.
			IID_IDIALOGCONTROLLER, kCPDFDialogControllerImpl,
			
			// Allows dynamic processing of dialog changes.
			IID_IOBSERVER, kCPDFDialogObserverImpl,
		}
	},
	
	//****Added
	Class
	{
		kCPDFSelectDocsDialogBoss,
		kDialogBoss,
		{
			/** Provides management and control over the dialog.
			*/
			IID_IDIALOGCONTROLLER, kCPDFSelectDocsDialogControllerImpl,
			/** Allows dynamic processing of dialog changes.
			*/
			IID_IOBSERVER, kCPDFSelectDocsDialogObserverImpl,
		}

	},
	
/*	Class 
	{
		kCPDFListBoxWidgetBoss,
		kWidgetListBoxWidgetNewBoss,
		{
			//IID_IOBSERVER,	kCMMTemplateFileListBoxObserverImpl,
		}
	},
*/	
	Class 
	{
		kCPDFCustomIconSuiteWidgetBoss,
		kIconSuiteWidgetBoss,
		{
			/** 
				We're overriding the left-button down method.
			*/
//			IID_IEVENTHANDLER, kCMMCustomIconSuitWidgetEHImpl,
			/** 
				Flag used to preserve the internal state (selected or otherwise)
			*/
//			IID_IBOOLEANCONTROLDATA,kCBooleanControlDataImpl,
			/** 
				This interface has state data consisting of two integers, one for the default
				state and one for the 'selected' or hilight state. Although it seems to be about fonts, it
				isn't specific to fonts and 
				it is appropriate here since we have two int32 resource IDs that we wish to maintain on
				a data interface. This saves us having to write a custom data interface or use two int-data
				interfaces with different IIDs. 
			*/
//			IID_IUIFONTSPEC, kUIFontSpecImpl
			// I know they're not fonts.. but we need two integers to be read in... 
			// so it's a hack... but an efficient one.
		}
	},

//--------ADD By Lalit------------
	Class
	{
		kCPDFIconSuiteWidgetBoss,
		kIconSuiteWidgetBoss,
		{			
			//IID_IOBSERVER, kCPDFIconWidgetObserverImpl,
			//IID_IEVENTHANDLER,	kEventHandlerImpl,
		}
	},
	
	Class 
	{
		kCPDFListBoxWidgetBoss,
		kWidgetListBoxWidgetNewBoss,
		{
			//IID_IOBSERVER,	kCPDFListBoxObserverImpl,
		}
	},

}}};

/*  
 * Implementation definition.
 */
resource FactoryList (kSDKDefFactoryListResourceID)
{
	kImplementationIDSpace,
	{
		#include "CPDFFactoryList.h"
	}
};

/*  
 * Menu definition.
 */
resource MenuDef (kSDKDefMenuResourceID)
{
	{
		// The About Plug-ins sub-menu item for this plug-in.
		kCreatePDFsActionID, //kCPDFAboutActionID,			// ActionID (kInvalidActionID for positional entries)
		"BookPanelPopup", //kCPDFAboutMenuPath,			// Menu Path.
		1305,	//kSDKDefAlphabeticPosition,			// Menu Position.
		kSDKDefIsNotDynamicMenuFlag,		// kSDKDefIsNotDynamicMenuFlag or kSDKDefIsDynamicMenuFlag


	// The Plug-ins menu sub-menu items for this plug-in.
		/*kCPDFDialogActionID,
		kCPDFPluginsMenuPath,
		kCPDFDialogMenuItemPosition,
		kSDKDefIsNotDynamicMenuFlag,*/
		

	}
};

/* 
 * Action definition.
 */
resource ActionDef (kSDKDefActionResourceID)
{
	{
		//kCPDFActionComponentBoss, 		// ClassID of boss class that implements the ActionID.
		//kCPDFAboutActionID,	// ActionID.
		//kCPDFAboutMenuKey,	// Sub-menu string.
		//kOtherActionArea,				// Area name (see ActionDefs.h).
		//kNormalAction,					// Type of action (see ActionDefs.h).
		//kDisableIfLowMem,				// Enabling type (see ActionDefs.h).
		//kInvalidInterfaceID,			// Selection InterfaceID this action cares about or kInvalidInterfaceID.
		//kSDKDefInvisibleInKBSCEditorFlag, // kSDKDefVisibleInKBSCEditorFlag or kSDKDefInvisibleInKBSCEditorFlag.


		kCPDFActionComponentBoss,
		kCreatePDFsActionID, //kCPDFDialogActionID,		
		"Create PDFs",		
		kOtherActionArea,			
		kNormalAction,				
		kCustomEnabling, //kDisableIfLowMem,	
		kInvalidInterfaceID,		
		kSDKDefInvisibleInKBSCEditorFlag, //kSDKDefVisibleInKBSCEditorFlag,
	}
};


/*  
 * Locale Indicies.
 * The LocaleIndex should have indicies that point at your
 * localizations for each language system that you are localized for.
 */

/*  
 * String LocaleIndex.
 */
resource LocaleIndex ( kSDKDefStringsResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_enUS, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_enGB, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_deDE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_frFR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_esES, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_ptBR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_svSE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_daDK, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nlNL, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_itIT, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nbNO, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_fiFI, kSDKDefStringsResourceID + index_enUS
		kInDesignJapaneseFS, k_jaJP, kSDKDefStringsResourceID + index_jaJP
	}
};

resource LocaleIndex (kSDKDefStringsNoTransResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_Wild, kSDKDefStringsNoTransResourceID + index_enUS
	}
};

// Strings not being localized
resource StringTable (kSDKDefStringsNoTransResourceID + index_enUS)
{
	k_enUS,									// Locale Id
	kEuropeanMacToWinEncodingConverter,		// Character encoding converter
	{
	}
};

/*  
 * Dialog LocaleIndex.
 */
resource LocaleIndex (kSDKDefDialogResourceID)
{
   kViewRsrcType,
	{
		kWildFS, k_Wild, kSDKDefDialogResourceID + index_enUS
	}
};
resource LocaleIndex (kSDKDefDialog1ResourceID)
{
   kViewRsrcType,
	{
		kWildFS,	k_Wild, kSDKDefDialog1ResourceID + index_enUS
	}
};
resource LocaleIndex (kCPDFListElementRsrcID)
{
	kViewRsrcType,
	{
		kWildFS, k_Wild, kCPDFListElementRsrcID + index_enUS
	}
};

/*  
 * Type definition.
 */
type CPDFDialogWidget(kViewRsrcType) : DialogBoss(ClassID = kCPDFDialogBoss)
{
};

//****Added
type CPDFSelectDocsDialogWidget(kViewRsrcType):DialogBoss(ClassID = kCPDFSelectDocsDialogBoss)
{
};
//type CPDFListBox	(kViewRsrcType) : WidgetListBoxWidgetN	(ClassID = kCPDFListBoxWidgetBoss) { };
type CPDFCustomIconSuiteWidget(kViewRsrcType) : IconSuiteWidget(ClassID = kCPDFCustomIconSuiteWidgetBoss) 
{
	UIFontSpec;	//because we want to read in two integers for the default and hilite states.
};

//----------ADD By Lalit----------
type CPDFIconSuiteWidget(kViewRsrcType) :IconSuiteWidget(ClassID = kCPDFIconSuiteWidgetBoss)
{ 
};

type CPDFListBox(kViewRsrcType):WidgetListBoxWidgetN(ClassID = kCPDFListBoxWidgetBoss)
{ 
};
//****

/*  Dialog definition.
	This view is not localised: therefore, it can reside here.
	However, if you wish to localise it, it is recommended to locate it in one of
	the localised framework resource files (i.e. CPDF_enUS.fr etc.) and
	update your Dialog LocaleIndex accordingly.
*/
//********************************************************************************
	resource PNGA(kPNGOK1IconRsrcID)  "Ok_hover65x18.png"
	resource PNGR(kPNGOK1IconRollRsrcID)  "Ok_65x18.png"
	
	resource PNGA(kPNGCancel1IconRsrcID)  "Cancel_hover65x18.png"
	resource PNGR(kPNGCancel1IconRollRsrcID)  "Cancel_65x18.png"
	
	resource PNGA(kPNGOK2IconRsrcID)  "Ok_hover65x18.png"
	resource PNGR(kPNGOK2IconRollRsrcID)  "Ok_65x18.png"

//**********************************************************************************
resource CPDFDialogWidget (kSDKDefDialogResourceID + index_enUS)
{
	__FILE__, __LINE__,
	kCPDFDialogWidgetID,	// WidgetID
	kPMRsrcID_None,				// RsrcID
	kBindNone,					// Binding
	0, 0, 388,112,				// Frame (l,t,r,b)
	kTrue, kTrue,				// Visible, Enabled
	kCPDFDialogTitleKey,	// Dialog name
	{
	//<FREDDYWIDGETDEFLISTUS>                   //last ok, cancel buttons
		DefaultButtonWidget
		(
    		kOKButtonWidgetID,		// WidgetID
			kSysButtonPMRsrcId,		// RsrcID
			kBindNone,				// Binding
			292, 16, 372, 36,		// Frame (l,t,r,b)
			kTrue, kTrue,			// Visible, Enabled
			kSDKDefOKButtonApplicationKey,	// Button text
		),
		CancelButtonWidget
		(
			kCancelButton_WidgetID,	// WidgetID
			kSysButtonPMRsrcId,		// RsrcID
			kBindNone,				// Binding
			292, 46, 372, 66,		// Frame (l,t,r,b)
			kTrue, kTrue,			// Visible, Enabled
			kSDKDefCancelButtonApplicationKey,	// Button name
			kTrue,					// Change to Reset on option-click.
		),
		
	
	//</FREDDYWIDGETDEFLISTUS>
	},
};

//******From Here Added By Sachin Sharma on 14-08-08
resource CPDFSelectDocsDialogWidget(kSDKDefDialog1ResourceID + index_enUS)
{
	__FILE__, __LINE__,
	kCPDFSelectDocsDialogWidgetID,	// WidgetID
	kPMRsrcID_None,				// RsrcID
	kBindNone,					// Binding
	0, 0, 400,200,				// Frame (l,t,r,b)
	kTrue, kTrue,				// Visible, Enabled	
	kCPDFSelectDocsDialogTitleKey,	// Dialog name
	{
	
		GroupPanelWidget
       (
			kInvalidWidgetID ,					// widget ID
			kPMRsrcID_None ,					// PMRsrc ID
			kBindLeft | kBindRight ,			// frame binding
			Frame(1 , 2 , 399 , 198)				//-- testing only  //Frame(1,2,219,65)	// left, top, right, bottom//(1,2,247,75)
			kTrue ,								// visible
			kTrue ,								// enabled
			0 ,									// header widget ID
			{
			
				//*****For SelectAll CheckBox
				CheckBoxWidget
				(
					kSelectAllCheckBoxWidgetID,					// WidgetId
					kSysCheckBoxPMRsrcId,				// RsrcId
					kBindRight | kBindBottom,
					Frame(10,5,25,30)						// Frame30,121.0,260,141
					kTrue,								// Visible
					kTrue,								// Enabled
					kAlignLeft,							// Alignment
					""							// Initial text
				),				
				StaticTextWidget
				(
					// CControlView properties
					kInvalidWidgetID, // widget ID
					kSysStaticTextPMRsrcId, // PMRsrc ID
					kBindNone, // frame binding
					Frame(30,5,395,30)//Frame(5,5,395,35) //  left, top, right, bottom
					kTrue, // visible
					kTrue, // enabled
					// StaticTextAttributes properties
					kAlignLeft, // Alignment
					kDontEllipsize,kTrue , // Ellipsize style
					// CTextControlData properties
					"Select the content to export.", // control label
					// AssociatedWidgetAttributes properties
					kInvalidWidgetID, // associated widget ID
				),
				
				GroupPanelWidget
				(
					kInvalidWidgetID ,					// widget ID
					kPMRsrcID_None ,					// PMRsrc ID
					kBindLeft | kBindRight ,			// frame binding
					Frame(2 , 37 , 396 , 170)				//-- testing only  //Frame(2 , 37 , 396 , 170)	// left, top, right, bottom//(1,2,247,75)
					kTrue ,								// visible
					kTrue ,								// enabled
					0 ,									// header widget ID
					{
						/* ***List Box
						CPDFListBox
						(
							kCPDFListBoxWidgetID, 
							kSysOwnerDrawListBoxPMRsrcId,	// WidgetId, RsrcId
							kBindAll,						// Frame bindin							
							Frame(3,3,391,130)             //4,3,390,165
							kTrue, kTrue,									// Visible, Enabled
							1,
							0,												// List dimensions
							19,													// Cell height
							2,													// Border width
							kFalse,kTrue,									// Has scroll bar (h,v)
							kTrue,												// Multiselection
							kFalse,												// List items can be reordered
							kFalse,												// Draggable to new/delete buttons
							kFalse,												// Drag/Dropable to other windows
							kFalse,												// An item always has to be selected
							kFalse,												// Don't notify on reselect
							kCPDFListElementRsrcID								// Fill list box with widgets with this ID (default is 0)
							{
									CellPanelWidget
									(
										kCellPanelWidgetID, kPMRsrcID_None,			// WidgetId, RsrcId
										kBindAll,									// Frame binding										
										Frame(-1,1,390,130)                         //-1,1,390,130
										kTrue, kTrue								// Visible, Enabled
										{
										// ----- This is the CPanelControlData that holds the widgets
										//		 that are items in the list box. They are not persistent
										//		 and so are not specified as part of the resource. [amb]
										}
									)
							},
						),
						*/
					}
				),
				
				DefaultButtonWidget
				(
					kOKButtonWidgetID,		// WidgetID
					kSysButtonPMRsrcId,		// RsrcID
					kBindNone,				// Binding
					0,0,1,1,	// Frame (l,t,r,b)
					kTrue, kTrue,			// Visible, Enabled
					kSDKDefOKButtonApplicationKey,	// Button text
				),
	
				CancelButtonWidget
				(
					kCancelButton_WidgetID,	// WidgetID
					kSysButtonPMRsrcId,		// RsrcID
					kBindNone,				// Binding
					0,0,1,1,		// Frame (l,t,r,b)
					kTrue, kTrue,			// Visible, Enabled
					kSDKDefCancelButtonApplicationKey,	// Button name
					kTrue,					// Change to Reset on option-click.
				),
//*****************************************************************************
				RollOverIconButtonWidget
				(
					kRfhDlgOK1ButtonWidgetID, 
					kPNGOK1IconRsrcID,
					kCPDFPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(120,173,185,191),	                    //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),	
				
				RollOverIconButtonWidget
				(
					kRfhDlgCancel1ButtonWidgetID, 
					kPNGCancel1IconRsrcID,
					kCPDFPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(200,173,265,191),	                    //Frame(171+2 , 85 , 200 , 108 )
					kTrue, kTrue,
					kADBEIconSuiteButtonType,
				),	
//*********************************************************************************	
		
			}			
		),
	
	},
};

resource PrimaryResourcePanelWidget (kCPDFListElementRsrcID + index_enUS)
{
	__FILE__, __LINE__,
	kCPDFListParentWidgetId,
	0,	// WidgetId, RsrcId
	kCPDFPluginID,	
	kBindLeft | kBindRight,			// Frame binding	
	Frame(0, 0,390, 19),            //0, 0,170, 19
	kTrue, kTrue,					// Visible, Enabled
	"",								// Panel name
	{
			
		CPDFIconSuiteWidget
		(
			kCPDFUnCheckIconWidgetID,
			kUncheckIconID,
			kCPDFPluginID,
			kBindLeft,
			Frame(2,0, 28, 19),  //1,0, 19, 20
			kTrue, kTrue, 0,
			kADBEIconSuiteButtonType
		),
		CPDFIconSuiteWidget
		(
			kCPDFCheckIconWidgetID,
			kCheckIconID,
			kCPDFPluginID,
			kBindLeft,
			Frame(2,0, 28, 19),
			kTrue, kTrue, 0,
			kADBEIconSuiteButtonType
		),
		
/*		 CPDFCustomIconSuiteWidget //index 0
		 (
			kCheckIconWidgetID,
			kUncheckIconID,  // kUncheckIconID Modified...16-06-07
			kCPDFPluginID,
			kBindLeft,
			Frame(1,0, 19, 20),//Frame(2,0, 21, 20),
			kTrue, kTrue, 0,
			kADBEIconSuiteButtonType,
			// Custom fields
			kUncheckIconID,
			kCheckIconID,
			
		),
*/

/*		IconSuiteWidget //index 1
		(
			kCheckIconWidgetID,
			kCheckIconID,
			kCPDFPluginID,
			kBindLeft,
			Frame(2,0, 21, 20),
			kTrue, kTrue, 0,
			kADBEIconSuiteButtonType
		),
*/
		
		// Just a info-static text widget with about-box text view to get white bg.
		InfoStaticTextWidget
		(
			kPDFNameTextWidgetID, 
			kPMRsrcID_None,//WidgetId, RsrcId
			kBindLeft | kBindRight,								// Frame binding
			//Frame(21,0,80,19)
			Frame(30,0,390,19),//Frame(21,0,208,21)										// Frame
			kTrue, kTrue, kAlignLeft,kEllipsizeEnd,kTrue,		// Visible, Enabled, Ellipsize style
			"",													// Initial text
			0,													// Associated widget for focus
			kPaletteWindowSystemScriptFontId,					// default font 
			kPaletteWindowSystemScriptFontId, //kPaletteWindowSystemScriptHiliteFontId,				// for highlight state.
		),	
		
	}
};

//*****

#endif // __ODFRC__

#include "CPDF_enUS.fr"
#include "CPDF_jaJP.fr"

//  Code generated by DollyXs code generator

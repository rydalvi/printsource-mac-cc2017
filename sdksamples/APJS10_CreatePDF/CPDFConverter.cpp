#include "VCPlugInHeaders.h"

#include "CPDFConverter.h"
//#include "HelperInterface.h"
//#include "CAlert.h"
//#include "PMString.h"
//#include "ETPID.h"
//#include "IPDFConverter.h"
//
//#include "IBoolData.h"
//#include "ICommand.h" 
//#include "IDataBase.h"
//#include "IOutputPages.h"
//#include "IPDFExportPrefs.h"
//#include "IPDFSecurityPrefs.h"
//#include "IPrintContentPrefs.h"
//#include "ISysFileData.h"
//#include "IUIFlagData.h"
//
//#include "CmdUtils.h"
//#include "K2SmartPtr.h"
//#include "ProgressBar.h"
//#include "SDKUtilities.h"		
//#include "UIDList.h"
//#include "ISpreadList.h"
//#include "ISpread.h"
//#include "BookID.h" 
//#include "DocumentID.h" // for IID_IDOCUMENT, IID_ISYSFILEDATA and IID_IUIFLAGDATA
//#include "PDFID.h" // for kPDFExportCmdBoss and IID_IPDFEXPORTPREFS
//#include "PrintID.h" // for IID_IPRINTCONTENTPREFS
//
//#include "SDKUtilities.h"
//#include "CActionComponent.h"
//#include "ErrorUtils.h"
//#include "ExportID.h"
//#include "IDocument.h"
//#include "IExportProvider.h"
//#include "IK2ServiceProvider.h"
//#include "IK2ServiceRegistry.h"
//#include "IObjectModel.h"
//#include "ISession.h"
//#include "ISelectionUtils.h"
//#include "ITextFocus.h"
//#include "ITextSelectionSuite.h"
//#include "LayoutUtils.h"
//#include "PersistUtils.h"
//#include "SDKUtilities.h"
//#include "Utils.h"
//#include "IBookUtils.h"
//#include "IApplication.h"
//#include "IDocumentUtils.h"
//#include "IDocumentList.h"

//#include "IPDFExportStyleLastUsed.h"
#include "PreferenceUtils.h"
#include "IPDFExptStyleListMgr.h"
#include "IStringData.h"

#include "IBook.h"
#include "IBookCmdData.h"
#include "IBookContent.h"
#include "IBookContentMgr.h"
#include "IBookManager.h"
#include "IBookOutputActionCmdData.h"
#include "ICommand.h"
#include "IDataBase.h"
#include "IDataLink.h"
#include "IPDFExportPrefs.h"
#include "IPDFExptStyleListMgr.h"
#include "ISession.h"
#include "IStringData.h"
#include "IDFile.h"

// General includes:
#include "BookID.h"
#include "CmdUtils.h"
#include "K2Vector.h"
#include "PersistUtils.h"
#include "PreferenceUtils.h"
#include "SDKFileHelper.h"
#include "UIDList.h"
#include "Utils.h"
#include "LocaleSetting.h"

// Framework includes:
//#include "SnpRunnable.h"
//#include "ISnipRunParameterUtils.h"
//#include "SnipRunLog.h"

#include "IWorkspace.h"
#include "PMString.h"
#define CA(x) CAlert::InformationAlert(x)

int32 index1 = 0;
//IPDFExportPrefs iPDFExportPrefsPtr = NULL;

//class CPDFConverter: public CPMUnknown<IPDFConverter>
//{
//
//public:
//
//	CPDFConverter(IPMUnknown* boss);
//
//		~CPDFConverter()
//		{
//			//Close the file
//		}
//
//	ErrorCode OutputPagesToPDF(const PMString& OutputfileName, 
//						   const UIDList& pagesUIDList, 
//						   const K2::UIFlags uiFlags1 = kSuppressUI);
//
//	//Takes the input parameters as path of Input file and output file and then converts to PDF
//	void ConvertoPDF(const SysFile& Inputfile, const PMString& Outputfile);
//
//	//Takes the input parameters as Pointer to current open document And path of output file
//	void ConvertoPDFwithDoc(IDocument* doc, const PMString& Outputfile);
//
//	
//}; 
//
//
//CREATE_PMINTERFACE (CPDFConverter, kPDFConverterImpl)
//
//CPDFConverter::CPDFConverter(IPMUnknown* boss):CPMUnknown<IPDFConverter>(boss)
//		{
//		}


ErrorCode CPDFConverter::OutputPagesToPDF(const PMString& OutputfileName, 
						   const UIDList& pagesUIDList, 
						   const K2::UIFlags uiFlags1)
{
	//CA("CPDFConverter::OutputPagesToPDF");
	ErrorCode status1 = kFailure;
	do
	{
	
		//if ( (OutputfileName.CountChars()/*ObsoleteLength()*/ < 1) || 
		//	 (pagesUIDList.Length() < 1) ) 
		//{
		//	CA("OutputPagesToPDF(): Preconditions not met!");
		//	ASSERT_FAIL("OutputPagesToPDF(): Preconditions not met!");
		//	break;
		//}

		
		// obtain command boss
        InterfacePtr<ICommand> pdfExportCmd1(CmdUtils::CreateCommand(kPDFExportCmdBoss));
	
		// set export prefs
		InterfacePtr<IPDFExportPrefs> pdfExportPrefs1(pdfExportCmd1, UseDefaultIID());
		// todo: call specific methods...
		int32 on = 1;
		int32 off = 2;
		pdfExportPrefs1->SetPDFExThumbnails(IPDFExportPrefs::kExportThumbnailsON );
		
		pdfExportPrefs1->SetIncludeBookmarks(kTrue);
		pdfExportPrefs1->SetVisibleGuidesGrids(kFailure);
		pdfExportPrefs1->SetPDFExIncHyperLinks(off);
		pdfExportPrefs1->SetNonprintingObjects(kTrue);
		//pdfExportPrefs->SetPDFExIPDFExportPrefs::kExportReaderSpreadsON ) ;
		pdfExportPrefs1->SetPDFExLinearized(IPDFExportPrefs::kExportLinearizedON ); 
		pdfExportPrefs1->SetPDFExOutputOPI(IPDFExportPrefs::kExportOutputOPION );
		pdfExportPrefs1->SetPDFExPageInfo(IPDFExportPrefs::kExportPageInfoON );
		pdfExportPrefs1->SetPDFExOmitPDF(IPDFExportPrefs::kExportOmitPDFON ); 
		pdfExportPrefs1->SetNonprintingObjects(kTrue);
//		pdfExportPrefs1->SetPDFExFormIntact(kTrue); 
		pdfExportPrefs1->SetPDFExIgnoreFlattenerSpreadOverrides(kTrue); 
		pdfExportPrefs1->SetPDFExLaunchAcrobat(IPDFExportPrefs::kExportLaunchAcrobatON );
	
		// set SysFileData
		PMString pmFilePath1(OutputfileName);
		pmFilePath1.SetTranslatable(kFalse);
		IDFile sysFilePath1 = SDKUtilities::PMStringToSysFile(&pmFilePath1);
		InterfacePtr<ISysFileData> trgSysFileData(pdfExportCmd1, IID_ISYSFILEDATA);
		
		trgSysFileData->Set(sysFilePath1);
		
		// set security preferences
		InterfacePtr<IPDFSecurityPrefs> securityPrefs1(pdfExportCmd1, UseDefaultIID());
		// todo: add calls to specific methods...
		

 
		// set print content prefs
		InterfacePtr<IPrintContentPrefs> printContentPrefs1(pdfExportCmd1, IID_IPRINTCONTENTPREFS);
		// todo: add calls to specific methods...
		
		// set to true if outputting a book file 
		InterfacePtr<IBoolData> bookExport1(pdfExportCmd1, IID_IBOOKEXPORT);
		bookExport1->Set(kFalse); 
		
		// set UI flags
		InterfacePtr<IUIFlagData> uiFlagData1(pdfExportCmd1, IID_IUIFLAGDATA);
		uiFlagData1->Set(uiFlags1);
		
		// set UIDList containing pages to output (item list of command)
		UIDList theList1(pagesUIDList);
		pdfExportCmd1->SetItemList(theList1);
		
		// set UIDRefs containing pages to output (IOutputPages)
		InterfacePtr<IOutputPages> outputPages1(pdfExportCmd1, UseDefaultIID());
		outputPages1->Clear();
		IDataBase* db1 = theList1.GetDataBase();
		outputPages1->SetMasterDataBase(db1);
		int32 nProgressItems1 = theList1.Length(); 
		for (int32 index1 = 0 ; index1 < nProgressItems1 ; index1++) 
		{
        	outputPages1->AppendOutputPageUIDRef(theList1.GetRef(index1));
		}
		
		// set PDF document name (get name from IDocument)
		InterfacePtr<IDocument> doc1 ((IDocument*)db1->QueryInstance(db1->GetRootUID(), IID_IDOCUMENT));
		PMString documentName1; 
		doc1->GetName(documentName1);
		outputPages1->SetName(documentName1);
		
		// setup progress bar, if not suppressing the UI.
		K2::scoped_ptr<RangeProgressBar> deleteProgressBar;
		bool16 bShowImmediate1 = kTrue;
		if( uiFlags1 != kSuppressUI )
		{
			RangeProgressBar *progress1 = new RangeProgressBar( "Generating PDF", 0, nProgressItems1, bShowImmediate1, kTrue, nil, kTrue); 
			pdfExportPrefs1->SetProgress(progress1);
			deleteProgressBar.reset( progress1 );
		}
		
		// finally process command
		status1 = CmdUtils::ProcessCommand(pdfExportCmd1);
		
	} while (false);
	return status1;
} 





//void CPDFConverter::ConvertoPDF(const SysFile& Inputfile, const PMString& Outputfile)
//{
//  ErrorCode status = kFailure;
//  do
//  {
//	if ( (Inputfile.ObsoleteLength() < 1) || 
//			 (Outputfile.ObsoleteLength() < 1) ) 
//		{
//			CA("ConvertoPDF(): Preconditions not met! ");
//			//ASSERT_FAIL("OutputPagesToPDF(): Preconditions not met!");
//			break;
//		}
//	
//	bool16 HIJ = kFalse;
//	bool16 HPP = kFalse;
//	// opens the input file in the Background
//	IDocument* fntDoc = Utils<IBookUtils>()->OpenDocAtBackground 
//						(
//							Inputfile, // in
//							HIJ,// out
//							HPP
//						);
//	
//	if(fntDoc==nil)
//	{
//		CA("Document not found");
//		break;
//	}
//
//	IDataBase* database = ::GetDataBase(fntDoc);
//	if(database==nil)
//	{
//		CA("Error database==nil");
//		break;
//	}
//		// get UIDs of all pages in the Document
//		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)fntDoc,UseDefaultIID());
//		if (iSpreadList==nil)
//			{
//				CA("Error iSpreadList==nil");
//				break;
//			}
//
//			UIDList allPageItems(database);
//	
//		for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
//		{
//			UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
//	
//			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
//			if(!spread)
//			{
//				CA("Error !spread");
//				break;
//			}
//			int numPages=spread->GetNumPages();
//
//			for(int i=0; i<numPages; i++)
//			{
//				UIDList tempList(database);
//				UID pageUIDs = spread->GetNthPageUID(i);
//				tempList.Append(pageUIDs);
//				allPageItems.Append(tempList);
//			}
//		}
//		
//		// Call the OutputPagesToPDF function 
//		status = this->OutputPagesToPDF(Outputfile, allPageItems);
//		if (!(status == kSuccess))
//			{
//				CA("Error while trying to export selected File to Adobe PDF");
//				break;
//			}
//
//		//  code to close the document open at Back ground
//		InterfacePtr<IApplication> AppHandler(gSession->QueryApplication());
//		if (AppHandler == nil)
//		{
//			CA("AppHandler invalid");
//			break;
//		}
//
//		InterfacePtr<IDocumentList> Doclist(AppHandler->QueryDocumentList());
//   		if (Doclist == nil)
//		{
//			CA("Doclist invalid");
//			break;
//		}
//
//		Doclist->CloseDoc(fntDoc);
//		//CA("Document close");
//
//	}while(0);
//}
//
//
//

void CPDFConverter::ConvertoPDFwithDoc(IDocument* doc, const PMString& Outputfile)
{
	//CA("CPDFConverter::ConvertoPDFwithDoc");
ErrorCode status = kFailure;
  do
  {
	  
	//if ( Outputfile.ObsoleteLength() < 1 ) 
	//	{
	//		CA("Output file path not found");
	//		//ASSERT_FAIL("OutputPagesToPDF(): Preconditions not met!");
	//		break;
	//	}

		IDataBase* database = ::GetDataBase(doc);
		if(database==nil)
			{
				CA("Error database==nil");
				break;
			}

		// get UIDs of all pages in the Document
		InterfacePtr<ISpreadList> iSpreadList((IPMUnknown*)doc,UseDefaultIID());
		if (iSpreadList==nil)
			{
				CA("Error iSpreadList==nil");
				break;
			}

			UIDList allPageItems(database);
	
		for(int numSp=0; numSp< iSpreadList->GetSpreadCount(); numSp++)
		{
			UIDRef spreadUIDRef(database, iSpreadList->GetNthSpreadUID(numSp));
	
			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
			if(!spread)
			{
				CA("Error !spread");
				break;
			}
			int numPages=spread->GetNumPages();

			for(int i=0; i<numPages; i++)
			{
				UIDList tempList(database);
				UID pageUIDs = spread->GetNthPageUID(i);
				tempList.Append(pageUIDs);
				allPageItems.Append(tempList);
			}
		}

		// Call the OutputPagesToPDF function 
		//status = this->OutputPagesToPDF(Outputfile, allPageItems);
		status = this->ExportDocUsingOldSettings(Outputfile, allPageItems);
		if (!(status == kSuccess))
		{
			CA("Error while trying to export selected File to Adobe PDF");
			break;
		}
		
	}while(0);
}


void CPDFConverter::ExportPDF(IDocument *frontDoc,IDFile idFile,bool16 first_Doc)
{
	PMString PDFFormat("Adobe PDF");
	PDFFormat.SetTranslatable(kFalse);
	
	//InterfacePtr<ISelectionManager> selection(SelectionUtils::QueryActiveSelection());
	InterfacePtr<ISelectionManager> selection(Utils<ISelectionUtils>()->QueryActiveSelection ());
	if (selection == nil)
	{
		//CA("selection == nil");
		return;
	}
	
	//IDocument *frontDoc = ::GetFrontDocument();

	InterfacePtr<IK2ServiceRegistry> k2ServiceRegistry(/*gSession*/GetExecutionContextSession(), UseDefaultIID());//Cs4
	if(k2ServiceRegistry == nil)
	{
		//CA("k2ServiceRegistry == nil");
		return;
	}
	// Look for all service providers with kExportProviderService.
	int32 exportProviderCount = k2ServiceRegistry->GetServiceProviderCount(kExportProviderService);
	
	// Iterate through them.
	bool found = kFalse;
	for(int32 exportProviderIndex = 0; exportProviderIndex < exportProviderCount; exportProviderIndex++)
	{
		// get the service provider boss class
		InterfacePtr<IK2ServiceProvider> k2ServiceProvider(k2ServiceRegistry->QueryNthServiceProvider(kExportProviderService,exportProviderIndex));
		if(k2ServiceProvider == nil)
		{
			//CA("k2ServiceProvider== nil");
			break;
		}

		// Get the export provider implementation itself.
		InterfacePtr<IExportProvider> exportProvider(k2ServiceProvider,	IID_IEXPORTPROVIDER);
		if(exportProvider == nil)
		{
			//CA("exportProvider== nil");
			break;
		}

		// Check to see if the current selection specifier can be exported by this provider.
		bool16 canExportByTarget = exportProvider->CanExportThisFormat(frontDoc,selection, PDFFormat);
		if(canExportByTarget)
		{
			found = kTrue;
			// assume idFile is a valid IDFile to hold the soon to be created PDF
			if(first_Doc)
			{
				exportProvider->ExportToFile(idFile, frontDoc, selection, PDFFormat, kFullUI);	
			}
			else
			{
				exportProvider->ExportToFile(idFile, frontDoc, selection, PDFFormat, kSuppressUI);	
			}
			
			InterfacePtr<IPDFExptStyleListMgr> styleMgr
            ((IPDFExptStyleListMgr*)::QuerySessionPreferences(IPDFExptStyleListMgr::kDefaultIID));
			if (styleMgr == nil)
			{
				//CA("Failed to get IPDFExptStyleListMgr");
				break;
			}
			// get the last use export style
			InterfacePtr<IStringData> lastPDFExportStyleUsed((IStringData*)::QuerySessionPreferences(IID_IPDFEXPORTSTYLELASTUSED));
			//	 ((IPDFExptStyleListMgr*)::QuerySessionPreferences(IID_IStringData));
			if (lastPDFExportStyleUsed == nil)
			{
				 //CA("Failed to get IStringData");
				 break;
			}
			PMString lastStyleName = lastPDFExportStyleUsed->Get();
			//CA("lastStyleName = " + lastStyleName);

			if (lastStyleName.IsEmpty()) 
			{
				CA("(BENIGN) There was no last used PDF export style. To quiet this assert, open the PDF Export Styles dialog, select one of the styles, and click on the 'Edit' button.");
				break;
			}
			// ask for the index of the last used style
			index1 = styleMgr->GetStyleIndexByName(lastStyleName);
			//if (nStyle != -1)
			//{
			//	UIDRef styleRef = styleMgr->GetNthStyleRef(nStyle);
			//	InterfacePtr<IPDFExportPrefs> pStylePrefs(styleRef, UseDefaultIID());
			//	if( pStylePrefs )
			//	{
			//		// assume myExportPrefs is an IPDFExportPrefs I am trying to set up
			//		myExportPrefs->CopyPrefs(pStylePrefs);
			//		//iPDFExportPrefsPtr = pStylePrefs;
			//	};
			//}
		}
		if(found)
			break;		
	}

}

ErrorCode CPDFConverter::ExportDocUsingOldSettings(const PMString& OutputfileName, 
						   const UIDList& pagesUIDList, 
						   const K2::UIFlags uiFlags1)
{
	//CA("inside CPDFConverter::ExportDocUsingOldSettings");
	ErrorCode status = kFailure;
	do
	{
//////////////////////////
		// obtain command boss
        InterfacePtr<ICommand> pdfExportCmd1(CmdUtils::CreateCommand(kPDFExportCmdBoss));
		if(pdfExportCmd1 == nil)
		{
			CA("pdfExportCmd1 == nil");
			break;
		}

		// set export prefs
		InterfacePtr<IPDFExportPrefs> myExportPrefs(pdfExportCmd1, UseDefaultIID());
		if(myExportPrefs == nil)
		{
			CA("myExportPrefs == nil");
			break;
		}

		PMString lastPreset;
		/*InterfacePtr<IPDFExportStyleLastUsed>iStyleLast((IPDFExportStyleLastUsed*)::QuerySessionPreferences(IID_IPDFEXPORTSTYLELASTUSED));
		if (iStyleLast)
			lastPreset = iStyleLast->GetString();*/
		
InterfacePtr<IPDFExportPrefs> pdfExportPrefs(pdfExportCmd1, UseDefaultIID());
if(!pdfExportPrefs)
{
	CA("!pdfExportPrefs");
}
InterfacePtr<IPDFExportPrefs> appExportPrefs((IPDFExportPrefs*)::QuerySessionPreferences(IID_IPDFEXPORTPREFS));
if(!appExportPrefs)
{
	CA("!appExportPrefs");
}
pdfExportPrefs->CopyPrefs(appExportPrefs);



//// Get the workspace
//InterfacePtr<IWorkspace> theWorkspace(gSession->QueryWorkspace());
//if(theWorkspace == nil)
//{
//	CA("theWorkspace == nil");
//	break;
//}	
//
//InterfacePtr<IPDFExportPrefs> thePDFExportWorkspacePrefs(theWorkspace, UseDefaultIID());
//if(thePDFExportWorkspacePrefs == nil)
//{
//	CA("thePDFExportWorkspacePrefs == nil");
//	break;
//}
//myExportPrefs->CopyPrefs(thePDFExportWorkspacePrefs);


		 InterfacePtr<IPDFExptStyleListMgr> styleMgr
         ((IPDFExptStyleListMgr*)::QuerySessionPreferences(IPDFExptStyleListMgr::kDefaultIID));
		 if (styleMgr == nil)
		 {
			 CA("Failed to get IPDFExptStyleListMgr");
			 break;
		 }
		 // get the last use export style
		 InterfacePtr<IStringData> lastPDFExportStyleUsed
			 ((IStringData*)::QuerySessionPreferences(IID_IPDFEXPORTSTYLELASTUSED));
			//	 ((IPDFExptStyleListMgr*)::QuerySessionPreferences(IID_IStringData));
		 if (lastPDFExportStyleUsed == nil)
		 {
			 CA("Failed to get IStringData");
			 break;
		 }
		 lastPreset = lastPDFExportStyleUsed->Get();
		 //CA("lastStyleName = " + lastPreset);

		 if (lastPreset.IsEmpty()) 
		 {
			 CA("(BENIGN) There was no last used PDF export style. To quiet this assert, open the PDF Export Styles dialog, select one of the styles, and click on the 'Edit' button.");
			 break;
		 }
		 // ask for the index of the last used style
		index1 = styleMgr->GetStyleIndexByName(lastPreset);
		
		/*PMString style1("nStyle first  = ");
		style1.AppendNumber(index);
		CA(style1);*/

		lastPreset.SetTranslatable(false);
		
		//Getting a Style Object using Name
		//InterfacePtr<IPDFExptStyleListMgr>styleMgr((IPDFExptStyleListMgr*)::QuerySessionPreferences(IID_IPDFEXPORTSTYLELISTMGR));

		
		int32 nStyle = styleMgr->GetStyleIndexByName(lastPreset);
		/*PMString style("nStyle = ");
		style.AppendNumber(nStyle);
		CA(style);*/

		if (nStyle != -1)
		{
			UIDRef styleRef = styleMgr->GetNthStyleRef(nStyle);
			InterfacePtr<IPDFExportPrefs> pStylePrefs(styleRef, UseDefaultIID());
			if(pStylePrefs)
			{
				
				// assume myExportPrefs is an IPDFExportPrefs I am trying to set up
				/*int32 launchPDFoldBefore = pStylePrefs->GetPDFExLaunchAcrobat();
				int32 launchPDFBefore = myExportPrefs->GetPDFExLaunchAcrobat();

				PMString launchPDFStr("");
				launchPDFStr.Append("launchPDFold = ");
				launchPDFStr.AppendNumber(launchPDFoldBefore);
				launchPDFStr.Append(" , launchPDF = ");
				launchPDFStr.AppendNumber(launchPDFBefore);
				CA(launchPDFStr);*/

				myExportPrefs->CopyPrefs(pStylePrefs);
				
				int32 launchPDFold = pStylePrefs->GetPDFExLaunchAcrobat();
				int32 launchPDF = myExportPrefs->GetPDFExLaunchAcrobat();
				
				PMString launchPDFStr("");
				launchPDFStr.Clear();
				launchPDFStr.Append("launchPDFold = ");
				launchPDFStr.AppendNumber(launchPDFold);
				launchPDFStr.Append(" , launchPDF = ");
				launchPDFStr.AppendNumber(launchPDF);
				//CA(launchPDFStr);				 
			}
		}		
		
		// set SysFileData
		PMString pmFilePath1(OutputfileName);
		pmFilePath1.SetTranslatable(kFalse);
		IDFile sysFilePath1 = SDKUtilities::PMStringToSysFile(&pmFilePath1);
		InterfacePtr<ISysFileData> trgSysFileData(pdfExportCmd1, IID_ISYSFILEDATA);
		if(trgSysFileData == nil)
		{
			CA("trgSysFileData == nil");
			break;
		}
		
		trgSysFileData->Set(sysFilePath1);
		
		// set security preferences
		InterfacePtr<IPDFSecurityPrefs> securityPrefs1(pdfExportCmd1, UseDefaultIID());
		// todo: add calls to specific methods...
		

 
		// set print content prefs
		InterfacePtr<IPrintContentPrefs> printContentPrefs1(pdfExportCmd1, IID_IPRINTCONTENTPREFS);
		if(printContentPrefs1 == nil)
		{
			CA("printContentPrefs1 == nil");
		}

		// todo: add calls to specific methods...
		
		// set to true if outputting a book file 
		InterfacePtr<IBoolData> bookExport1(pdfExportCmd1, IID_IBOOKEXPORT);
		if(bookExport1 == nil)
		{
			CA("bookExport1 == nil");
		}
		bookExport1->Set(kFalse); 
		
		// set UI flags
		InterfacePtr<IUIFlagData> uiFlagData1(pdfExportCmd1, IID_IUIFLAGDATA);
		if(uiFlagData1 == nil)
		{
			CA("uiFlagData1 == nil");
		}
		uiFlagData1->Set(uiFlags1);
		
		// set UIDList containing pages to output (item list of command)
		UIDList theList1(pagesUIDList);
		pdfExportCmd1->SetItemList(theList1);
		
		// set UIDRefs containing pages to output (IOutputPages)
		InterfacePtr<IOutputPages> outputPages1(pdfExportCmd1, UseDefaultIID());
		if(outputPages1 == nil)
		{
			CA("outputPages1 == nil");
		}
		outputPages1->Clear();

		IDataBase* db1 = theList1.GetDataBase();
		outputPages1->SetMasterDataBase(db1);
		int32 nProgressItems1 = theList1.Length(); 
		for (int32 index1 = 0 ; index1 < nProgressItems1 ; index1++) 
		{
        	outputPages1->AppendOutputPageUIDRef(theList1.GetRef(index1));
		}
		
		// set PDF document name (get name from IDocument)
		InterfacePtr<IDocument> doc1 ((IDocument*)db1->QueryInstance(db1->GetRootUID(), IID_IDOCUMENT));
		PMString documentName1; 
		doc1->GetName(documentName1);
		outputPages1->SetName(documentName1);
		
		// setup progress bar, if not suppressing the UI.
		K2::scoped_ptr<RangeProgressBar> deleteProgressBar;
		bool16 bShowImmediate1 = kTrue;
		if( uiFlags1 != kSuppressUI )
		{
			RangeProgressBar *progress1 = new RangeProgressBar( "Generating PDF", 0, nProgressItems1, bShowImmediate1, kTrue, nil, kTrue); 
			//myExportPrefs->SetProgress(progress1);
			pdfExportPrefs->SetProgress(progress1);
			//thePDFExportPrefs->SetProgress(progress1);
			
			deleteProgressBar.reset( progress1 );
		}
		
		// finally process command
		status = CmdUtils::ProcessCommand(pdfExportCmd1);

	} while (false);
	return status;
}

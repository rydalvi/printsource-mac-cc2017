//========================================================================================
//  
//  $File: //depot/indesign_5.0/gm/source/sdksamples/writefishprice/WFPDialogObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:37:33 $
//  
//  $Revision: #1 $
//  
//  $Change: 505969 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
// General includes:
#include "CDialogObserver.h"
#include "CDialogController.h"
// Project includes:
#include "CPDFID.h"

#include "SDKListBoxHelper.h"
#include "IListBoxController.h"
#include "CAlert.h"
#define CA(x) CAlert::InformationAlert(x)

extern K2Vector<PMString> bookContentNames;		//----ADDED BY LALIT------
extern K2Vector<bool16>  isSelected;
/** Implements IObserver based on the partial implementation CDialogObserver.

	
	@ingroup writefishprice
*/
class CPDFSelectDocsDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CPDFSelectDocsDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~CPDFSelectDocsDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(CPDFSelectDocsDialogObserver, kCPDFSelectDocsDialogObserverImpl)


void CPDFSelectDocsDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Attach to other widgets you want to handle dynamically here.

//-------NEW ADDED BY LALIT---------
		AttachToWidget(kSelectAllCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kRfhDlgOK1ButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kRfhDlgCancel1ButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//AttachToWidget(kRfhDlgOK2ButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
	} while (kFalse);
}

/* AutoDetach
*/
void CPDFSelectDocsDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Detach from other widgets you handle dynamically here.

//-------NEW ADDED BY LALIT---------
		DetachFromWidget(kSelectAllCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhDlgOK1ButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kRfhDlgCancel1ButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//DetachFromWidget(kRfhDlgOK2ButtonWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
	} while (kFalse);
}

/* Update
*/
void CPDFSelectDocsDialogObserver::Update
(
	const ClassID& theChange,
	ISubject* theSubject,
	const PMIID& protocol,
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	do
	{
		
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		ASSERT(controlView);
		if(!controlView) {
			break;
		}
		
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		// TODO: process this

//-------NEW ADDED BY LALIT ---------	
		InterfacePtr<IPanelControlData> pPanelData(this, UseDefaultIID());
		if(theSelectedWidget==kSelectAllCheckBoxWidgetID && theChange==kTrueStateMessage)
		{
			//CA("Check Box Select");
			SDKListBoxHelper listHelper(this, kCPDFPluginID);
			IControlView *listBoxControlView  = pPanelData->FindWidget(kCPDFListBoxWidgetID);
			InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
				return;

			K2Vector<PMString>::iterator itr;
			if(bookContentNames.size()==0)
			{
				CA("bookContentNames.size()==0kCPDFCheckIconWidgetID");			
				return;
			}
			int32 index=0;
			K2Vector<bool16>::iterator   itrIsSelected;
			itrIsSelected =  isSelected.begin();
			for(itr=bookContentNames.begin();itr!=bookContentNames.end() && itrIsSelected !=  isSelected.end() ;itr++,itrIsSelected++)
			{
				//CA("Inside for loop");
				listHelper.CheckUncheckRow(listBoxControlView,index++,kTrue);
				(*itrIsSelected) = kTrue;
			}

		//	break;
		}
		if((theSelectedWidget == kRfhDlgOK1ButtonWidgetID || theSelectedWidget == kOKButtonWidgetID) && theChange == kTrueStateMessage)
		{
			if(theSelectedWidget == kRfhDlgOK1ButtonWidgetID)
				CDialogObserver::CloseDialog();
			break;
		}

		if(theSelectedWidget == kOKButtonWidgetID){
			///writting code for avoiding the crash hitting on enter key
		}


		if(theSelectedWidget == kRfhDlgCancel1ButtonWidgetID /*|| theSelectedWidget == kCancelButton_WidgetID)*/&& theChange == kTrueStateMessage)
		{
			//sif(theSelectedWidget == kRfhDlgCancel1ButtonWidgetID || theSelectedWidget == kCancelButton_WidgetID)
			CDialogObserver::CloseDialog();
			break;
		}

		if(theSelectedWidget==kSelectAllCheckBoxWidgetID && theChange==kFalseStateMessage)
		{
			//CA("Check Box Select");
			SDKListBoxHelper listHelper(this, kCPDFPluginID);
			IControlView *listBoxControlView  = pPanelData->FindWidget(kCPDFListBoxWidgetID);
			InterfacePtr<IListBoxController> listCntl(listBoxControlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
				return;

			K2Vector<PMString>::iterator itr;
			if(bookContentNames.size()==0)
			{
				CA("bookContentNames.size()==0kCPDFCheckIconWidgetID");			
				return;
			}
			int32 index=0;
			K2Vector<bool16>::iterator   itrIsSelected;
			itrIsSelected =  isSelected.begin();
			for(itr=bookContentNames.begin();itr!=bookContentNames.end()&& itrIsSelected !=  isSelected.end() ;itr++,itrIsSelected++)
			{
				//CA("Inside for loop");
				listHelper.CheckUncheckRow(listBoxControlView,index++,kFalse);
				(*itrIsSelected) = kFalse;
			}

		//	break;
		}
//---------Up To Here--------
	} while (kFalse);
}

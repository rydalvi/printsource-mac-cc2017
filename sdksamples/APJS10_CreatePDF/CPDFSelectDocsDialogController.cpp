//========================================================================================
//  
//  $File: //depot/indesign_5.0/gm/source/sdksamples/writefishprice/WFPDialogController.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:37:33 $
//  
//  $Revision: #1 $
//  
//  $Change: 505969 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
#include "ITextEditSuite.h"  			// STEP 7.1
#include "ISelectionManager.h"			// STEP 7.1
#include "IPanelControlData.h" 			// STEP 9.1
#include "IDropDownListController.h" 	// STEP 9.1

// General includes:
#include "CDialogController.h"

// Project includes:
#include "CPDFID.h"
#include "CAlert.h"
#include "SDKListBoxHelper.h"
#include "CAlert.h"


#define CA(x) CAlert::InformationAlert(x)
extern K2Vector<PMString> bookContentNames;  
IControlView* SelectAllControlView = nil;

//****
K2Vector<bool16>  isSelected;  //**This Attribute Stores the Value of current Status of ListBox for checked(kTrue) and for Unchecked(kFalse) 

/** WFPDialogController

	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.

	
	@ingroup writefishprice
*/
class CPDFSelectDocsDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CPDFSelectDocsDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~CPDFSelectDocsDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	    virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
};

CREATE_PMINTERFACE(CPDFSelectDocsDialogController, kCPDFSelectDocsDialogControllerImpl)


void CPDFSelectDocsDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	//CA("CPDFSelectDocsDialogController::InitializeDialogFields_111");
	CDialogController::InitializeDialogFields(dlgContext);

	// STEP 9.1  - BEGIN
	do {
		// Get current panel control data.
		InterfacePtr<IPanelControlData> pPanelData(QueryIfNilElseAddRef(nil));
		if (pPanelData == nil)
		{
			//CA("CPDDialogController::InitializeFields: PanelControlData is nil!");
			break;
		}
		else
		{			
			IControlView *iListBoxControlView  = pPanelData->FindWidget(kCPDFListBoxWidgetID);
			if(iListBoxControlView ==  nil)
			{
				CA("iListBoxControlView ====== nil");
				return;
			}						
			SDKListBoxHelper listHelper(this,kCPDFPluginID);
			listHelper.EmptyCurrentListBox(iListBoxControlView);
									
			K2Vector<PMString>::iterator  itr; 
			int32 index=0;
			
			isSelected.clear();
			for(itr =bookContentNames.begin(); itr != bookContentNames.end(); itr++)
			{				
				listHelper.AddElement(iListBoxControlView, *itr,kPDFNameTextWidgetID, index, kTrue);
				listHelper.CheckUncheckRow(iListBoxControlView,index,kTrue);
				isSelected.push_back(kTrue);
				//isSelected.insert(itr,kFalse);
				index++;
			}

			//****Initialising SelectAll CkeckBox
			IControlView * selectAllSectionCheckBoxControlView = pPanelData->FindWidget(kSelectAllCheckBoxWidgetID);
			if(selectAllSectionCheckBoxControlView == nil)
			{
				CA("selectAllSectionCheckBoxControlView == nil");
				break;
			}
			InterfacePtr<ITriStateControlData>selectAllSectionCheckBoxTriState(selectAllSectionCheckBoxControlView,UseDefaultIID());
			if(selectAllSectionCheckBoxTriState == nil)
			{
				CA("selectAllSectionCheckBoxTriState == nil");
				break;
			}
			SelectAllControlView =  selectAllSectionCheckBoxControlView;
	
			this->SetTriStateControlData(kSelectAllCheckBoxWidgetID,kTrue);//------ADDED BY LALIT-------------
		}
	} while (kFalse);
	// STEP 9.1  - END
}

/* ValidateFields
*/
WidgetID CPDFSelectDocsDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	return result;
}

/* ApplyFields
*/
void CPDFSelectDocsDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	// STEP 5.1 - BEGIN
	// DropDownList result string.
	//CA("CPDFSelectDocsDialogController::ApplyDialogFields");
	PMString resultString;
	
	//Get Selected text of DropDownList.
	//resultString = this->GetTextControlData(kWFPDropDownListWidgetID);
	//resultString.Translate(); // Look up our string and replace.
	//// STEP 5.1 - END

	//// STEP 6.1 - BEGIN
	//// Get the editbox list widget string.
	//PMString editBoxString = this->GetTextControlData(kWFPTextEditBoxWidgetID);
	//// STEP 6.1 - END

	//// STEP 6.2 - BEGIN
	//PMString moneySign(kWFPStaticTextKey);
	//moneySign.Translate(); // Look up our string and replace.
	//
	//resultString.Append('\t'); // Append tab code.
	//resultString.Append(moneySign);
	//resultString.Append(editBoxString);
	//resultString.Append('\r'); // Append return code.
	//// STEP 6.2 - END

	//// STEP 7.1 - BEGIN
 //   if (myContext == nil)
	//{
	//	ASSERT(myContext);
	//	return;
	//}
	//// Insert resultString to TextFrame.
 //   InterfacePtr<ITextEditSuite> textEditSuite(myContext->GetContextSelection(), UseDefaultIID());

	//// STEP 7.2 - BEGIN
 //   if (textEditSuite && textEditSuite->CanEditText())
 //   {
	//	// STEP 7.3 - BEGIN
 //       ErrorCode status = textEditSuite->InsertText(WideString(resultString));
 //       ASSERT_MSG(status == kSuccess, "CPDDialogController::ApplyDialogFields: can't insert text"); 
	//	// STEP 7.3 - END
 //   }
	// STEP 7.2 - END
}

/* ApplyFields
*/
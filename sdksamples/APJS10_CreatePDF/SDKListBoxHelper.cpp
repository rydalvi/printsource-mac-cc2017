#include "VCPlugInHeaders.h"
#include "IListControlDataOf.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "IListBoxAttributes.h"
#include "IControlView.h"
#include "IApplication.h"
#include "PersistUtils.h"
//#include "PalettePanelUtils.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "CAlert.h"
#include "RsrcSpec.h"
#include "CPDFID.h"
//#include "RefreshData.h"
#include "SDKListBoxHelper.h"
#include "IListBoxController.h"
//#include "ISpecialChar.h"
#include "ISubject.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("SDKListBoxHelper.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
#define CA(x)	CAlert::InformationAlert(x)
//extern RefreshDataList rDataList;

////Global Pointers
//extern ISpecialChar* iConverter;

///////////////

SDKListBoxHelper::SDKListBoxHelper(IPMUnknown * owner, int32 pluginId) : fOwner(owner), fOwnerPluginID(pluginId)
{
	//CA("SDKListBoxHelper::SDKListBoxHelper");
}

SDKListBoxHelper::~SDKListBoxHelper()
{
	fOwner=nil;
	fOwnerPluginID=0;
}

IControlView* SDKListBoxHelper ::FindCurrentListBox(InterfacePtr<IPanelControlData> iPanelControlData, int i)//Give me the iControlView of the listbox. huh big deal!!
{
	//CA("SDKListBoxHelper ::FindCurrentListBox");
	if(!verifyState())
		return nil;

	IControlView * listBoxControlView2 = nil;

	do {
		if(iPanelControlData ==nil)
		{
			break;
		}
		if(i==1)
		{
			listBoxControlView2 = iPanelControlData->FindWidget(kCPDFListBoxWidgetID);
		}
		
		if(listBoxControlView2 == nil) 
			break;

	} while(0);
	return listBoxControlView2;
}

void SDKListBoxHelper::AddElement(IControlView* lstboxControlView,PMString & displayName, WidgetID updateWidgetId, int atIndex, int x, bool16 isObject)
{	
	if(!verifyState())
		return;
	do	
	{
		//CA("SDKListBoxHelper::AddElement ");
		InterfacePtr<IListBoxAttributes> listAttr(lstboxControlView, UseDefaultIID());
		if(listAttr == nil) 
		{
			CA("listAttr == nil");
			break;	
		}
	
		RsrcID widgetRsrcID = listAttr->GetItemWidgetRsrcID();
		if (widgetRsrcID==0)
		{
            CA("widgetRsrcID==0");
			break;
		}

		RsrcSpec elementResSpec(LocaleSetting::GetLocale(), fOwnerPluginID, kViewRsrcType, widgetRsrcID);
		InterfacePtr<IControlView> newElView( (IControlView*) ::CreateObject(::GetDataBase(lstboxControlView), elementResSpec, IID_ICONTROLVIEW));
		if(newElView==nil) 
		{
			CA("newElView==nil");
			break;
		}

		this->addListElementWidget(lstboxControlView, newElView, displayName, updateWidgetId, atIndex, x, isObject);
	}
	while (false);
}

/*
void SDKListBoxHelper::RemoveElementAt(int indexRemove, int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) 
		{
			break;
		}
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveElementAt() Found listbox but not control data?");
		if(listControlData==nil) 
		{
			break;
		}
		if(indexRemove < 0 || indexRemove >= listControlData->Length()) 
		{
			break;
		}
		listControlData->Remove(indexRemove, x);
		removeCellWidget(listBox, indexRemove);
	}
	while (false);
}

void SDKListBoxHelper::RemoveLastElement(int x)
{
	if(!verifyState())
		return;
	do
	{
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) 
		{
			break;
		}
		
		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::RemoveLastElement() Found listbox but not control data?");
		if(listControlData==nil) 
		{
			break;
		}
		int lastIndex = listControlData->Length()-1;
		if(lastIndex > 0) 
		{		
			listControlData->Remove(lastIndex, x);
			removeCellWidget(listBox, lastIndex);
		}
	}
	while (false);
}

int SDKListBoxHelper::GetElementCount(int x) 
{
	int retval=0;
	do {
		IControlView * listBox = this->FindCurrentListBox(x);
		if(listBox == nil) {
			break;
		}

		InterfacePtr<IListControlData> listControlData(listBox, UseDefaultIID());
		ASSERT_MSG(listControlData != nil, "SDKListBoxHelper::GetElementCount() Found listbox but not control data?");
		if(listControlData==nil) {
			break;
		}
		retval = listControlData->Length();
	} while(0);
	
	return retval;
}

void SDKListBoxHelper::removeCellWidget(IControlView * listBox, int removeIndex) {
	
	do {
		if(listBox==nil) break;

		InterfacePtr<IPanelControlData> panelData(listBox, UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::removeCellWidget()  Cannot get panelData");
		if(panelData == nil) 
		{
			break;
		}
		
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::removeCellWidget() cannot find cellControlView");
		if(cellControlView == nil) 
		{
			break;
		}

		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil,"SDKListBoxHelper::removeCellWidget() cellPanelData nil"); 
		if(cellPanelData == nil) 
		{
			break;
		}

		if(removeIndex < 0 || removeIndex >= cellPanelData->Length()) 
		{
			break;
		}
		cellPanelData->RemoveWidget(removeIndex);
	} while(0);
}
*/

void SDKListBoxHelper::addListElementWidget(IControlView* lstboxControlView, InterfacePtr<IControlView> & elView,PMString & displayName, WidgetID updateWidgetId, int atIndex, int x, bool16 isObject)
{
	
	if(elView == nil || lstboxControlView == nil ) 
		return;
	do {
	
		//CA("addListElementWidget");
		InterfacePtr<IPanelControlData> newElPanelData (elView, UseDefaultIID());
		if(newElPanelData == nil)
		{
			CA("newElPanelData == nil");
			break;
		}

		IControlView* nameTextView = newElPanelData->FindWidget(updateWidgetId);
		if(nameTextView == nil)
		{
			CA("nameTextView == nil");
			break;
		}
//CA("addListElementWidget  2");
		InterfacePtr<ITextControlData> newEltext (nameTextView,UseDefaultIID());
		if(newEltext == nil)
		{
			CA("newEltext == nil");		
			break;
		}
//CA("addListElementWidget  3");
		// Awasthi
		/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		break;
		PMString stringToInsert("");
		stringToInsert.Append(iConverter->handleAmpersandCase(displayName));*/
		// End Awasthi
		PMString stringToInsert("");
		stringToInsert.Append(displayName);
//CA("addListElementWidget  4" + stringToInsert);

		newEltext->SetString(stringToInsert, kTrue, kTrue);
		
		InterfacePtr<IPanelControlData> panelData(lstboxControlView,UseDefaultIID());
		ASSERT_MSG(panelData != nil, "SDKListBoxHelper::addListElementWidget() Cannot get panelData");
		if(panelData == nil) 
			break;
//CA("addListElementWidget  5");
	
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);
		ASSERT_MSG(cellControlView != nil, "SDKListBoxHelper::addListElementWidget() cannot find cellControlView");
		if(cellControlView == nil) 
			break;

//CA("addListElementWidget  6");
		/*if(!isObject)
		{
			int indent=10;
			PMRect cellRect;
			elView->GetFrame();
			cellRect.Left(cellRect.Left()+indent);
			cellRect.Right(cellRect.Right()+indent);
			elView->SetFrame(cellRect);
		}*/
		InterfacePtr<IPanelControlData> cellPanelData (cellControlView, UseDefaultIID());
		ASSERT_MSG(cellPanelData != nil, "SDKListBoxHelper::addListElementWidget()  cellPanelData nil");
		if(cellPanelData == nil) 
			break;

//CA("addListElementWidget  7");
		cellPanelData->AddWidget(elView);				
		InterfacePtr< IListControlDataOf<IControlView*> > listData(lstboxControlView, UseDefaultIID());		
		if(listData == nil)  
			break;
//CA("addListElementWidget  8");
		listData->Add(elView);	
	} while(0);
}

void SDKListBoxHelper::CheckUncheckRow(IControlView *listboxCntrlView, int32 index, bool checked)
{		
	do
	{
		InterfacePtr<IListBoxController> listCntl(listboxCntrlView,IID_ILISTBOXCONTROLLER);		
		if(listCntl == nil) 
			break;

		InterfacePtr<IPanelControlData> panelData(listboxCntrlView, UseDefaultIID());
		if (panelData == nil) 
			break;
				
		IControlView* cellControlView = panelData->FindWidget(kCellPanelWidgetID);		
		if(cellControlView == nil)
			break;

		InterfacePtr<IPanelControlData> cellPanelData(cellControlView, UseDefaultIID());		
		if(cellPanelData == nil)
			break;

		IControlView* nameTextView = cellPanelData->GetWidget(index);
		if ( nameTextView == nil ) 
			break;

		InterfacePtr<IPanelControlData> cellPanelDataChild(nameTextView, UseDefaultIID());		
		if(cellPanelDataChild == nil)
			break;

		int toHidden=0;
		int toShow=0;
		
		if(checked)
		{
			toHidden=0;
			toShow=1;
		}
		else
		{
			toHidden=1;
			toShow=0;
		}

		IControlView *childHideView=cellPanelDataChild->GetWidget(toHidden);
		if(childHideView==nil)
		{
			CA("childHideView==nil");
			break;
		}

        IControlView *childShowView=cellPanelDataChild->GetWidget(toShow);
		if(childShowView==nil)
		{
			CA("childShowView==nil");		
			break;
		}

      	childHideView->HideView();
		childShowView->ShowView();	

	}while(kFalse);
}

void SDKListBoxHelper::EmptyCurrentListBox(IControlView *listboxCntrlView)
{
	
	do {
		//IControlView* listBoxControlView = this->FindCurrentListBox(panel, x);
		//if(listBoxControlView == nil) 
		//	break;
		InterfacePtr<IListControlData> listData (listboxCntrlView, UseDefaultIID());
		if(listData == nil) 
			break;
		InterfacePtr<IPanelControlData> iPanelControlData(listboxCntrlView, UseDefaultIID());
		if(iPanelControlData == nil) 
			break;
		IControlView* panelControlView = iPanelControlData->FindWidget(kCellPanelWidgetID);
		if(panelControlView == nil) 
			break;
		InterfacePtr<IPanelControlData> panelData(panelControlView, UseDefaultIID());
		if(panelData == nil) 
			break;
		listData->Clear(kFalse, kFalse);
		panelData->ReleaseAll();
		//listBoxControlView->Invalidate();
		
	} while(0);
}

//void SDKListBoxHelper::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
//{
//	if(protocol==IID_ILISTCONTROLDATA && theChange==kListSelectionChangedByUserMessage)
//	{
//		CA("ListBoxObserver::Update");
//		SDKListBoxHelper sList(this, kRfhPluginID);
//		InterfacePtr<IListBoxController> listCntl(Mediator::listControlView,IID_ILISTBOXCONTROLLER);
//		if(listCntl == nil) 
//			return;
//
//		K2Vector<int32> curSelection ;
//		listCntl->GetSelected(curSelection ) ;
//		const int kSelectionLength =  curSelection.Length();
//		if(kSelectionLength<=0)
//			return;
//		rDataList[curSelection[0]].isSelected=(rDataList[curSelection[0]].isSelected)? kFalse: kTrue;
//		sList.CheckUncheckRow(Mediator::listControlView, curSelection[0], rDataList[curSelection[0]].isSelected);
//	//	listCntl->DeselectAll();
//
//		bool16 isGroupByObj=Mediator::isGroupByObj;
//
//		///////////////////////////[ GROUPED BY OBJECT ]//////////////////////////////
//        CA("ListBoxObserver::Update 1");
//		if(isGroupByObj)//If its grouped by object
//		{
//			if(rDataList[curSelection[0]].isObject && curSelection[0]<rDataList.size())
//			{
//				 CA("ListBoxObserver::Update 2");
//				for(int j=curSelection[0]+1; j<rDataList.size(); j++)
//				{
//					CA("ListBoxObserver::Update 3");
//					if(rDataList[j].isObject)
//						break;
//					sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
//					rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
//                    CA("ListBoxObserver::Update 4");
//				}
//			}
//			else if(!rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
//			{
//				CA("ListBoxObserver::Update 5");
//				for(int j=curSelection[0]; j>=0; j--)
//				{
//					if(rDataList[j].isObject)
//					{
//						CA("ListBoxObserver::Update 6");
//						sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
//						rDataList[j].isSelected=kFalse;
//						CA("ListBoxObserver::Update 7");
//						break;
//					}
//					
//				}
//			}
//			else if(!rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
//			{
//				for(int i=curSelection[0]; i<rDataList.size(); i++)
//				{
//					if(rDataList[i].isObject)
//						break;
//					if(!rDataList[i].isSelected)
//						return;
//				}
//				int32 k=0;
//				for(k=curSelection[0]; k>=0; k--)
//				{
//					if(rDataList[k].isObject)
//						break;
//		
//					if(!rDataList[k].isSelected)
//						return;
//				}
//				rDataList[k].isSelected=kTrue;
//				sList.CheckUncheckRow(Mediator::listControlView, k, kTrue);
//			}
//		}
//
//		///////////////////////////[ GROUPED BY ELEMENT ]//////////////////////////////
//
//		else//Its grouped by element
//		{
//			if((!rDataList[curSelection[0]].isObject) && curSelection[0]<rDataList.size())
//			{	
//				for(int j=curSelection[0]+1; j<rDataList.size(); j++)
//				{
//					if(!rDataList[j].isObject)
//						break;
//					sList.CheckUncheckRow(Mediator::listControlView, j, rDataList[curSelection[0]].isSelected);				
//					rDataList[j].isSelected=rDataList[curSelection[0]].isSelected;
//				}
//			}
//			else if(rDataList[curSelection[0]].isObject && !rDataList[curSelection[0]].isSelected)
//			{	
//				for(int j=curSelection[0]; j>=0; j--)
//				{	
//					if(!rDataList[j].isObject)
//					{
//						sList.CheckUncheckRow(Mediator::listControlView, j, kFalse);
//						rDataList[j].isSelected=kFalse;
//						break;
//					}
//					
//				}
//			}
//			else if(rDataList[curSelection[0]].isObject && rDataList[curSelection[0]].isSelected)
//			{
//				for(int i=curSelection[0]; i<rDataList.size(); i++)
//				{
//					if(!rDataList[i].isObject)
//						break;
//					if(!rDataList[i].isSelected)
//						return;
//				}
//				int32 k=0;
//				for(k=curSelection[0]; k>=0; k--)
//				{
//					if(!rDataList[k].isObject)
//						break;
//		
//					if(!rDataList[k].isSelected)
//						return;
//				}
//				rDataList[k].isSelected=kTrue;
//				sList.CheckUncheckRow(Mediator::listControlView, k, kTrue);
//			}
//
//		}		
//		
//	}
//}



#include "VCPlugInHeaders.h"
#include "EventCache.h"
#include "CAlert.h"
#include "IAppFramework.h"

#define CA(x) CAlert::InformationAlert(x)


EventCache* EventCache::ecObjPtr = NULL;
bool EventCache::ecInstanceFlag = false;


EventCache* EventCache::getInstance() // SingleTone Implementation.
{
	//CA("Inside EventCache::getInstance");
	if(!ecInstanceFlag)
	{
		//CA("Creating Instance");
		ecObjPtr = new EventCache();
		ecInstanceFlag = true;
		ecObjPtr->loadEventCache();
		return ecObjPtr;
	}
	else
	{
		if(!ecObjPtr->isEventTreeLoaded)
		{
			ecObjPtr->loadEventCache();
		}
		return ecObjPtr;
	}
}

void EventCache::clearInstace()
{
	isEventTreeLoaded = kFalse;
	ecInstanceFlag = false;
	eventCacheMap.clear();	
}


bool16 EventCache::loadEventCache()
{
	//EventCache::scValueObjPtr = new StructureCacheValue();
	//CA("Inside loadStructureCache ");

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return kFalse;

	}

	//PMString structureJson;

	bool16 result=ptrIAppFramework->callAllTreeJson(ecValueObj);
	if(result)
	{		
		//CA(ecValueObj.getClassificationName());
		ecValueObj.setParentId(-3);
		ecValueObj.setRootId(-3);
		isEventTreeLoaded = kTrue;
		if(ecValueObj.getChildren().size() >0)
		{
			for(int j=0; j < ecValueObj.getChildren().size() ; j++)
			{
				double eventId = ecValueObj.getChildren().at(j).getEventId();

				//PMString asd("In loadEventCache Parent_ID: ");
				//asd.AppendNumber(ecValueObj.getParentId());
				//asd.Append("  rootID: ");
				//asd.AppendNumber(ecValueObj.getRootId());
				//ptrIAppFramework->LogError(asd);

				loadEventCacheMap(ecValueObj.getChildren().at(j), eventId, eventId);
			}
		}
	}
	else
	{
		isEventTreeLoaded = kFalse;
		return kFalse;
	}
	return kTrue;
}

//void EventCache::updateParentIdRootId(EventCacheValue & eventChildObj, int32 parentId, int32 rootId)
//{
//	eventChildObj.setParentId(parentId);
//	eventChildObj.setRootId(rootId);
//
//	if(eventChildObj.getChildren().size() >0)
//	{
//		for(int j=0; j < eventChildObj.getChildren().size() ; j++)
//		{
//			eventChildObj.getChildren().at(j).setRootId(ecValueObj.getChildren().at(j).getEventId());
//
//			eventChildObj(ecValueObj.getChildren().at(j));
//		}
//	}
//}
	
void EventCache::loadEventCacheMap(EventCacheValue & eventChildObj, double rootId, double parentId)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;

	}

	eventChildObj.setParentId(parentId);
	eventChildObj.setRootId(rootId);

	/*PMString asd("In loadEventCacheMap before insert  Parent_ID: ");
	asd.AppendNumber(eventChildObj.getParentId());
	asd.Append("  rootID: ");
	asd.AppendNumber(eventChildObj.getRootId());
	ptrIAppFramework->LogError(asd);*/

	eventCacheMap.insert(map<double ,EventCacheValue > ::value_type(eventChildObj.getEventId(),eventChildObj));
	//ptrIAppFramework->LogDebug("INsert into Map : " + eventChildObj.getName());
	for(int childCount =0; childCount < eventChildObj.getChildren().size(); childCount++ )
	{
		/*PMString asd("In loadEventCacheMap before Parent_ID: ");
		asd.AppendNumber(eventChildObj.getChildren().at(childCount).getParentId());
		asd.Append("  rootID: ");
		asd.AppendNumber(eventChildObj.getChildren().at(childCount).getRootId());
		ptrIAppFramework->LogError(asd);*/

		loadEventCacheMap(eventChildObj.getChildren().at(childCount), rootId , eventChildObj.getEventId());
	}
}

VectorPubModelPtr EventCache::ProjectCache_getAllProjects(double languageId)
{
	VectorPubModelPtr vectorPubModel = NULL;
	

	if(!isEventTreeLoaded)
	{
		loadEventCache();
	}
	
	if(isEventTreeLoaded)
	{
		//CA("isEventTreeLoaded");
		vectorPubModel = new VectorPubModel;

		for(int count =0; count < ecValueObj.getChildren().size(); count++)
		{
			CPubModel ObjPubModel;
			EventCacheValue ecChildObj = ecValueObj.getChildren().at(count);

			ObjPubModel.setName(ecChildObj.getName());
			//CA(ecChildObj.getName());
			ObjPubModel.setEventId(ecChildObj.getEventId());
			ObjPubModel.setParentId(ecChildObj.getParentId()/*-3*/);		
			ObjPubModel.setchild_Count((int32)(ecChildObj.getChildren().size()));	
			ObjPubModel.setlevel_No(3);
			ObjPubModel.setEventType(ecChildObj.getEventType());
			ObjPubModel.setRootID(ecChildObj.getRootId());
			ObjPubModel.setPubTypeId(ecChildObj.getPubTypeId());
			vectorPubModel->push_back(ObjPubModel);

		}
	}

	return vectorPubModel;
}


EventCacheValue EventCache::getEventCacheValueByEventId(double eventID)
{
	EventCacheValue ecChidObj;
	map< double, EventCacheValue>::iterator it;
	int iskeyInMap = (int)eventCacheMap.count(eventID);
	if(iskeyInMap == 1)
	{
		it = eventCacheMap.find(eventID);
		ecChidObj = it->second;
	}
	return ecChidObj;
}

VectorPubModelPtr EventCache::ProjectCache_getAllChildren(double eventId, double LanguageID)
{
	VectorPubModelPtr vectorEventValue = NULL;
	if(!isEventTreeLoaded)
	{
		loadEventCache();
	}
	
	if(isEventTreeLoaded)
	{
		EventCacheValue ecValueObj = getEventCacheValueByEventId(eventId);

		if(ecValueObj.getChildren().size() > 0)
		{
			vectorEventValue = new VectorPubModel;

			for(int count =0; count < ecValueObj.getChildren().size(); count++)
			{
				CPubModel ObjPubModel;
				EventCacheValue ecChildObj = ecValueObj.getChildren().at(count);

				ObjPubModel.setName(ecChildObj.getName());
				ObjPubModel.setEventId(ecChildObj.getEventId());
				ObjPubModel.setParentId(eventId);	//ObjPubModel.setParentId(ecValueObj.getEventId());		
				ObjPubModel.setchild_Count((int32)(ecChildObj.getChildren().size()));	
				ObjPubModel.setlevel_No(4);
				ObjPubModel.setEventType(ecChildObj.getEventType());
				ObjPubModel.setRootID(ecValueObj.getRootId());
				ObjPubModel.setPubTypeId(ecChildObj.getPubTypeId());
				vectorEventValue->push_back(ObjPubModel);
			}
		}				
	}

	return vectorEventValue;
}

CPubModel EventCache::getpubModelByPubID(double PubID,double languageID)
{
	CPubModel ObjPubModel;

	if(!isEventTreeLoaded)
	{
		loadEventCache();
	}
	if(isEventTreeLoaded)
	{
		EventCacheValue ecValueObj = getEventCacheValueByEventId(PubID);		

		ObjPubModel.setName(ecValueObj.getName());
		ObjPubModel.setEventId(ecValueObj.getEventId());
		ObjPubModel.setParentId(ecValueObj.getParentId());		
		ObjPubModel.setRootID(ecValueObj.getRootId());
		ObjPubModel.setchild_Count((int32)(ecValueObj.getChildren().size()));	
		//ObjPubModel.setlevel_No(4);
		ObjPubModel.setEventType(ecValueObj.getEventType());	
		ObjPubModel.setPubTypeId(ecValueObj.getPubTypeId());
	}
	return ObjPubModel;
}

//This funtion is used to grab all item, Item Group & Child Item details under one section
bool16 EventCache::setCurrentSectionData( double sectionId, double langId , PMString itemGroupIds, PMString itemIds, PMString itemFieldIds,
	PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds , PMString listItemFieldIds,
	bool16 isCategorySpecificResultCall,  bool16 isSprayItemPerFrameFlag)
{
	bool16 resultFlag = kFalse;
	// call getAllproductsAndItemsForSectionInDetail
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return resultFlag;
	}

	if(multipleSectionDetailsMap.size() > 0)
	{
		map< double,  VectorPbObjectValuePtr>::iterator it;
		int iskeyInMap = (int)multipleSectionDetailsMap.count(sectionId);
		if(iskeyInMap == 1)
		{
			it = multipleSectionDetailsMap.find(sectionId);
			(it->second)->clear();
			delete it->second;
			it->second = NULL;
			multipleSectionDetailsMap.erase(sectionId);
		}
	}
	/*map< int32, EventCacheValue>::iterator it;
	int iskeyInMap = (int)eventCacheMap.count(eventID);
	if(iskeyInMap == 1)
	{
		it = eventCacheMap.find(eventID);
		ecChidObj = it->second;
	}

	eventCacheMap.insert(map<int32 ,EventCacheValue > ::value_type(eventChildObj.getEventId(),eventChildObj));
*/

		//this->clearCurrentSectionData();
		currentSectionId = sectionId;		
		VectorPbObjectValuePtr vectorPtrPbObjectValueForCurrentSectionId = NULL;
			
		if(!isCategorySpecificResultCall)
			vectorPtrPbObjectValueForCurrentSectionId = ptrIAppFramework->getProductsAndItemsForSectionInDetail(sectionId, langId, itemGroupIds, itemIds, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, itemGroupAssetTypeIds, listTypeIds ,  listItemFieldIds , isSprayItemPerFrameFlag );
		else
			vectorPtrPbObjectValueForCurrentSectionId = ptrIAppFramework->getProductsAndItemsForCategorySpecificResult(langId, itemGroupIds, itemIds, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, itemGroupAssetTypeIds);


		if(vectorPtrPbObjectValueForCurrentSectionId != NULL && vectorPtrPbObjectValueForCurrentSectionId->size() > 0)
		{
			
			for(int32 count =0; count < vectorPtrPbObjectValueForCurrentSectionId->size(); count++)
			{
				if(vectorPtrPbObjectValueForCurrentSectionId->at(count).getisProduct() == 1)
				{
					if(vectorPtrPbObjectValueForCurrentSectionId->at(count).getObjectValue().getLists().size() > 0)
					{
						this->loadListItemToSectionVectorList(vectorPtrPbObjectValueForCurrentSectionId->at(count).getObjectValue().getLists(), vectorPtrPbObjectValueForCurrentSectionId);
					}
                    
                    if(vectorPtrPbObjectValueForCurrentSectionId->at(count).getObjectValue().getAdvanceTables().size() > 0)
					{
						this->loadAdvancedTableItemToSectionVectorList(vectorPtrPbObjectValueForCurrentSectionId->at(count).getObjectValue().getAdvanceTables(), vectorPtrPbObjectValueForCurrentSectionId);
					}
				}
				else if(vectorPtrPbObjectValueForCurrentSectionId->at(count).getisProduct() == 0)
				{
					if(vectorPtrPbObjectValueForCurrentSectionId->at(count).getItemModel().getLists().size() > 0)
					{
						this->loadListItemToSectionVectorList(vectorPtrPbObjectValueForCurrentSectionId->at(count).getItemModel().getLists(), vectorPtrPbObjectValueForCurrentSectionId);
					}
                    
                    if(vectorPtrPbObjectValueForCurrentSectionId->at(count).getItemModel().getAdvanceTables().size() > 0)
					{
						this->loadAdvancedTableItemToSectionVectorList(vectorPtrPbObjectValueForCurrentSectionId->at(count).getItemModel().getAdvanceTables(), vectorPtrPbObjectValueForCurrentSectionId);
					}
                    
				}
			}
			resultFlag = kTrue;
		}

		multipleSectionDetailsMap.insert(map<double , VectorPbObjectValuePtr > ::value_type(sectionId,vectorPtrPbObjectValueForCurrentSectionId));

		return resultFlag;

}
	
// Before Calling this method make sure you have called either setCurrentSectionData or setCurrentObjectData with proper Input;
CPbObjectValue EventCache::getPbObjectValueBySectionIdObjectId(double sectionId, double ObjectId, int32 isProduct, double langId) // ObjectId can be ItemGroupId or ItemId
{
	CPbObjectValue pbObjectValue;
	VectorPbObjectValuePtr  vectorPtrPbObjectValueForCurrentSectionId = NULL;

	if(ObjectId == -1 || sectionId == -1)
		return pbObjectValue;
	//// First find if 
	/*if(currentObjectId == ObjectId )
	{
		pbObjectValue.setObject_id(ObjectId);
		pbObjectValue.setisProduct(isProduct);
		if(isProduct == 1)
		{
			pbObjectValue.setObjectValue(*currentItemGroupValue);
			pbObjectValue.setPub_object_id(currentItemGroupValue->getSectionResultId());

		}
		else if(isProduct == 0)
		{
			pbObjectValue.setItemModel(*currentItemModelObj);
			pbObjectValue.setPub_object_id(currentItemModelObj->getSectionResultId());
		}

		return pbObjectValue;
	}*/

	if(multipleSectionDetailsMap.size() > 0)
	{
		map< double,  VectorPbObjectValuePtr>::iterator it;
		int iskeyInMap = (int)multipleSectionDetailsMap.count(sectionId);
		if(iskeyInMap == 1)
		{
			it = multipleSectionDetailsMap.find(sectionId);
			vectorPtrPbObjectValueForCurrentSectionId = (it->second);
		}
	}


	if((vectorPtrPbObjectValueForCurrentSectionId != NULL) && (vectorPtrPbObjectValueForCurrentSectionId->size() > 0))
	{
		for(int32 count =0; count < vectorPtrPbObjectValueForCurrentSectionId->size(); count++ )
		{
			if(ObjectId == vectorPtrPbObjectValueForCurrentSectionId->at(count).getObject_id())
			{
				pbObjectValue = vectorPtrPbObjectValueForCurrentSectionId->at(count);
				break;
			}
		}
	}

	return pbObjectValue;
}


void EventCache::clearCurrentSectionData()
{
	if(multipleSectionDetailsMap.size() > 0)
	{
		
		map< double,  VectorPbObjectValuePtr>::iterator it;
		for(it=multipleSectionDetailsMap.begin(); it!=multipleSectionDetailsMap.end(); it++)
		{
			if(it->second != NULL)
			{
				(it->second)->clear();
				delete it->second;
				it->second = NULL;
			}			
		}	
		multipleSectionDetailsMap.clear();
	}

}


void EventCache::setCurrentObjectData(double sectionId, double ObjectId, int32 isProduct, double langId, PMString itemFieldIds, PMString itemAssetTypeIds, PMString itemGroupFieldIds, PMString itemGroupAssetTypeIds, PMString listTypeIds , PMString listItemFieldIds )
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;
	}

	
		currentObjectId = ObjectId;	
		VectorPbObjectValuePtr  vectorPtrPbObjectValueForCurrentSectionId =  new vector<CPbObjectValue>;
		
		if(multipleSectionDetailsMap.size() > 0)
		{
			
			clearCurrentSectionData();
			/*map< int32,  VectorPbObjectValuePtr>::iterator it;
			int iskeyInMap = (int)multipleSectionDetailsMap.count(sectionId);
			if(iskeyInMap == 1)
			{
				it = multipleSectionDetailsMap.find(sectionId);
				if(it->second != nil)
				{
					(it->second)->clear();
					delete it->second;
					it->second = NULL;
				}
			}
			multipleSectionDetailsMap.clear();*/
		}
		

		if(isProduct ==1)
		{
			currentItemGroupValue = new CObjectValue();
			ptrIAppFramework->callItemGroupDetailJson(ObjectId, sectionId, langId, *currentItemGroupValue, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, itemGroupAssetTypeIds, listTypeIds, listItemFieldIds );
			CPbObjectValue pbObjectValue;
			pbObjectValue.setisProduct(1);
			pbObjectValue.setObject_id(currentItemGroupValue->getObject_id());
			pbObjectValue.setPub_object_id(currentItemGroupValue->getSectionResultId());
			pbObjectValue.setObjectValue(*currentItemGroupValue);
			vectorPtrPbObjectValueForCurrentSectionId->push_back(pbObjectValue);

			if(currentItemGroupValue->getLists().size() > 0)
			{// Start Adding List item to sectionVector list so that it will be useful in spraying item copy
				this->loadListItemToSectionVectorList(currentItemGroupValue->getLists(), vectorPtrPbObjectValueForCurrentSectionId);
			}

		}
		else if(isProduct ==0)
		{
			currentItemModelObj =  new CItemModel();
			ptrIAppFramework->callItemDetailJson(ObjectId, sectionId, langId, *currentItemModelObj, itemFieldIds, itemAssetTypeIds, itemGroupFieldIds, itemGroupAssetTypeIds, listTypeIds, listItemFieldIds );
			CPbObjectValue pbObjectValue;
			pbObjectValue.setisProduct(0);
			pbObjectValue.setObject_id(currentItemModelObj->getItemID());
			pbObjectValue.setPub_object_id(currentItemModelObj->getSectionResultId());
			pbObjectValue.setItemModel(*currentItemModelObj);
			vectorPtrPbObjectValueForCurrentSectionId->push_back(pbObjectValue);

			if(currentItemModelObj->getLists().size() > 0)
			{// Start Adding List item to sectionVector list so that it will be useful in spraying item copy
				this->loadListItemToSectionVectorList(currentItemModelObj->getLists(), vectorPtrPbObjectValueForCurrentSectionId);
			}

		}

		multipleSectionDetailsMap.insert(map<double , VectorPbObjectValuePtr > ::value_type(sectionId,vectorPtrPbObjectValueForCurrentSectionId));
	
}

void EventCache::clearCurrentObjectData()
{
	currentObjectId = -1;
	if(currentItemGroupValue != NULL)
	{
		delete currentItemGroupValue;
		currentItemGroupValue =  NULL;
	}
	if(currentItemModelObj != NULL)
	{
		delete currentItemModelObj;
		currentItemModelObj =  NULL;
	}
	this->clearCurrentSectionData();
	
}


void EventCache::loadListItemToSectionVectorList(vector<CItemTableValue> listVectorObj, VectorPbObjectValuePtr vectorPtrPbObjectValueForCurrentSectionId)
{
	if(listVectorObj.size() > 0)
	{// Start Adding List item to sectionVector list so that it will be useful in spraying item copy
		for(int32 count =0; count < listVectorObj.size(); count++ )
		{// For start currentItemGroupValue->getLists()
			CItemTableValue oTableValue = listVectorObj.at(count);
					
			if(oTableValue.getItemIds().size() > 0 && oTableValue.getTableData().size() >0)
			{
				for(int32 rcount=0; rcount < oTableValue.getItemIds().size(); rcount++ )
				{ // For start oTableValue.getItemIds()
					double itemId = oTableValue.getItemIds().at(rcount);
					vector<PMString> rowValues = oTableValue.getTableData().at(rcount);
							
					CItemModel itemModelObj;
					CPbObjectValue childpbObjectValue;

					bool16 itemIdAlreadyPresentInVector = kFalse;
					int32 countOfItemInVectorList =-1;

					CItemModel existingitemObj;
					vector<ItemValue> existingItemValue;

					for(int32 vCount =0; vCount < vectorPtrPbObjectValueForCurrentSectionId->size(); vCount++)
					{
						if(vectorPtrPbObjectValueForCurrentSectionId->at(vCount).getObject_id() == itemId)
						{
							countOfItemInVectorList = vCount;
							itemIdAlreadyPresentInVector = kTrue;
							break;
						}
					}

					if(itemIdAlreadyPresentInVector == kTrue)
					{
						existingitemObj = vectorPtrPbObjectValueForCurrentSectionId->at(countOfItemInVectorList).getItemModel();
						existingItemValue = existingitemObj.getValues();																	
					}
							
					vector<ItemValue> itemValueVector;
					if( (oTableValue.getColumn().size() > 0) && (rowValues.size() > 0) && (oTableValue.getColumn().size() == rowValues.size()))
					{
						for(int32 colCount=0; colCount < oTableValue.getColumn().size(); colCount++ )
						{// For Start oTableValue.getColumn()
							ItemValue itemValueObj;
							itemValueObj.setFieldId(((oTableValue.getColumn()).at(colCount)).getFieldId());
							itemValueObj.setFieldName(((oTableValue.getColumn()).at(colCount)).getFieldName());
							itemValueObj.setFieldValue(rowValues.at(colCount));
							itemValueVector.push_back(itemValueObj);

							if(itemIdAlreadyPresentInVector == kTrue)
							{
								if(existingItemValue.size() == 0)
								{
									existingItemValue.push_back(itemValueObj);		
								}
								else
								{
									bool16 attrIdAlreadyPresent = kFalse;
									for(int32 attCount =0; attCount < existingItemValue.size(); attCount++)
									{
										if(existingItemValue.at(attCount).getFieldId() == itemValueObj.getFieldId())
										{
											attrIdAlreadyPresent = kTrue;
											break;
										}
									}
									if(attrIdAlreadyPresent == kFalse)
									{
										existingItemValue.push_back(itemValueObj);
									}
								}
							}
						} // For End oTableValue.getColumn()
					}

					if(itemIdAlreadyPresentInVector == kFalse)
					{
						CItemModel itemModelObj;
						CPbObjectValue childpbObjectValue;

						itemModelObj.setItemID(itemId);
						itemModelObj.setValues(itemValueVector);

						childpbObjectValue.setisProduct(0);
						childpbObjectValue.setObject_id(itemId);
						childpbObjectValue.setPub_object_id(-1);
						childpbObjectValue.setItemModel(itemModelObj);
						vectorPtrPbObjectValueForCurrentSectionId->push_back(childpbObjectValue);
					}
					else if(itemIdAlreadyPresentInVector == kTrue)
					{
						existingitemObj.setValues(existingItemValue);
						vectorPtrPbObjectValueForCurrentSectionId->at(countOfItemInVectorList).setItemModel(existingitemObj);
					}
							
				}// For End oTableValue.getItemIds()

				// For CustomColums and Custom ROws
				for(int32 rcount=0; rcount < oTableValue.getCustomItemIds().size(); rcount++ )
				{ // For start oTableValue.getItemIds()
					double itemId = oTableValue.getCustomItemIds().at(rcount);
					vector<PMString> rowValues = oTableValue.getCustomTableData().at(rcount);
							
					CItemModel itemModelObj;
					CPbObjectValue childpbObjectValue;

					bool16 itemIdAlreadyPresentInVector = kFalse;
					int32 countOfItemInVectorList =-1;

					CItemModel existingitemObj;
					vector<ItemValue> existingItemValue;

					for(int32 vCount =0; vCount < vectorPtrPbObjectValueForCurrentSectionId->size(); vCount++)
					{
						if(vectorPtrPbObjectValueForCurrentSectionId->at(vCount).getObject_id() == itemId)
						{
							countOfItemInVectorList = vCount;
							itemIdAlreadyPresentInVector = kTrue;
							break;
						}
					}

					if(itemIdAlreadyPresentInVector == kTrue)
					{
						existingitemObj = vectorPtrPbObjectValueForCurrentSectionId->at(countOfItemInVectorList).getItemModel();
						existingItemValue = existingitemObj.getValues();																	
					}
							
					vector<ItemValue> itemValueVector;
					if( (oTableValue.getCustomColumn().size() > 0) && (rowValues.size() > 0) && (oTableValue.getCustomColumn().size() == rowValues.size()))
					{
						for(int32 colCount=0; colCount < oTableValue.getCustomColumn().size(); colCount++ )
						{// For Start oTableValue.getColumn()
							ItemValue itemValueObj;
							itemValueObj.setFieldId(((oTableValue.getCustomColumn()).at(colCount)).getFieldId());
							itemValueObj.setFieldName(((oTableValue.getCustomColumn()).at(colCount)).getFieldName());
							itemValueObj.setFieldValue(rowValues.at(colCount));
							itemValueVector.push_back(itemValueObj);

							if(itemIdAlreadyPresentInVector == kTrue)
							{
								if(existingItemValue.size() == 0)
								{
									existingItemValue.push_back(itemValueObj);		
								}
								else
								{
									bool16 attrIdAlreadyPresent = kFalse;
									for(int32 attCount =0; attCount < existingItemValue.size(); attCount++)
									{
										if(existingItemValue.at(attCount).getFieldId() == itemValueObj.getFieldId())
										{
											attrIdAlreadyPresent = kTrue;
											break;
										}
									}
									if(attrIdAlreadyPresent == kFalse)
									{
										existingItemValue.push_back(itemValueObj);
									}
								}
							}
						} // For End oTableValue.getColumn()
					}

					if(itemIdAlreadyPresentInVector == kFalse)
					{
						CItemModel itemModelObj;
						CPbObjectValue childpbObjectValue;

						itemModelObj.setItemID(itemId);
						itemModelObj.setValues(itemValueVector);

						childpbObjectValue.setisProduct(0);
						childpbObjectValue.setObject_id(itemId);
						childpbObjectValue.setPub_object_id(-1);
						childpbObjectValue.setItemModel(itemModelObj);
						vectorPtrPbObjectValueForCurrentSectionId->push_back(childpbObjectValue);
					}
					else if(itemIdAlreadyPresentInVector == kTrue)
					{
						existingitemObj.setValues(existingItemValue);
						vectorPtrPbObjectValueForCurrentSectionId->at(countOfItemInVectorList).setItemModel(existingitemObj);
					}
							
				}// For End oTableValue.getItemIds()


			}
		} // For End currentItemGroupValue->getLists()
	}//// End Adding List item to sectionVector list
}


double EventCache::getSectionIdByLevelAndEventId(int32 sectionLevel, double sectionId, double LangId)
{
	double resultSectionId = -1;

	vector<double> idList;
	idList.push_back(sectionId);

	CPubModel pubModel = getpubModelByPubID(sectionId, LangId);
	//CA(pubModel.getName());
	while(pubModel.getRootID() != pubModel.getParentId())
	{
		idList.push_back(pubModel.getParentId());
		pubModel = getpubModelByPubID(pubModel.getParentId(), LangId);
		//CA(pubModel.getName());
	}

	idList.push_back(pubModel.getRootID());

	vector<double> reverseIdList;

	for(int i = (int)idList.size()-1; i >= 0; i --)
	{
		reverseIdList.push_back(idList.at(i));
	}

	int levels = (int)reverseIdList.size();

	if(sectionLevel == (-1))
	{
		if(levels > 0)
			resultSectionId = reverseIdList.at(0);
	}
	else if(sectionLevel == (-2))
	{
		if(levels > 1)
			resultSectionId = reverseIdList.at(1);
	}
	else if(sectionLevel == (-3))
	{
		if(levels > 2)
			resultSectionId = reverseIdList.at(2);
	}
	else if(sectionLevel == (-4))
	{
		if(levels > 3)
			resultSectionId = reverseIdList.at(3);
	}
	else if(sectionLevel == (-5))
	{
		if(levels > 4)
			resultSectionId = reverseIdList.at(4);
	}
	else if(sectionLevel == (-6))
	{
		if(levels > 5)
			resultSectionId = reverseIdList.at(5);
	}

	return resultSectionId;
	
}



void EventCache::loadAdvancedTableItemToSectionVectorList(vector<CAdvanceTableScreenValue> listVectorObj, VectorPbObjectValuePtr vectorPtrPbObjectValueForCurrentSectionId)
{
	if(listVectorObj.size() > 0)
	{// Start Adding List item to sectionVector list so that it will be useful in spraying item copy
		for(int32 count =0; count < listVectorObj.size(); count++ )
		{// For start currentItemGroupValue->getLists()
			CAdvanceTableScreenValue oTableValue = listVectorObj.at(count);
            
			if(oTableValue.getCells().size() > 0 )
			{
				for(int32 rcount=0; rcount < oTableValue.getCells().size(); rcount++ )
				{ // For start oTableValue.getItemIds()
                    
                    CAdvanceTableCellValue cellObj =oTableValue.getCells().at(rcount);
                    
					double itemId = (cellObj.getItemId());
                    if(itemId == -1)
                        continue;
                    
					CItemModel itemModelObj;
					CPbObjectValue childpbObjectValue;
                    
					bool16 itemIdAlreadyPresentInVector = kFalse;
					int32 countOfItemInVectorList =-1;
                    
					CItemModel existingitemObj;
					vector<ItemValue> existingItemValue;
                    vector<CAssetValue> existingAssetValue;
                    
					for(int32 vCount =0; vCount < vectorPtrPbObjectValueForCurrentSectionId->size(); vCount++)
					{
						if(vectorPtrPbObjectValueForCurrentSectionId->at(vCount).getObject_id() == itemId)
						{
							countOfItemInVectorList = vCount;
							itemIdAlreadyPresentInVector = kTrue;
							break;
						}
					}
                    
					if(itemIdAlreadyPresentInVector == kTrue)
					{
						existingitemObj = vectorPtrPbObjectValueForCurrentSectionId->at(countOfItemInVectorList).getItemModel();
						existingItemValue = existingitemObj.getValues();
                        existingAssetValue = existingitemObj.getAssetList();
					}
					
                    if(cellObj.getFieldId() != -1)
                    {
                        vector<ItemValue> itemValueVector;
                        ItemValue itemValueObj;
                        itemValueObj.setFieldId(cellObj.getFieldId());
                        //itemValueObj.setFieldName();
                        itemValueObj.setFieldValue(cellObj.getValue());
                        itemValueVector.push_back(itemValueObj);
                            
							if(itemIdAlreadyPresentInVector == kTrue)
							{
								if(existingItemValue.size() == 0)
								{
									existingItemValue.push_back(itemValueObj);
								}
								else
								{
									bool16 attrIdAlreadyPresent = kFalse;
									for(int32 attCount =0; attCount < existingItemValue.size(); attCount++)
									{
										if(existingItemValue.at(attCount).getFieldId() == itemValueObj.getFieldId())
										{
											attrIdAlreadyPresent = kTrue;
											break;
										}
									}
									if(attrIdAlreadyPresent == kFalse)
									{
										existingItemValue.push_back(itemValueObj);
									}
								}
							}

                        if(itemIdAlreadyPresentInVector == kFalse)
                        {
                            CItemModel itemModelObj;
                            CPbObjectValue childpbObjectValue;
                        
                            itemModelObj.setItemID(itemId);
                            itemModelObj.setValues(itemValueVector);
                        
                            childpbObjectValue.setisProduct(0);
                            childpbObjectValue.setObject_id(itemId);
                            childpbObjectValue.setPub_object_id(-1);
                            childpbObjectValue.setItemModel(itemModelObj);
                            vectorPtrPbObjectValueForCurrentSectionId->push_back(childpbObjectValue);
                        }
                        else if(itemIdAlreadyPresentInVector == kTrue)
                        {
                            existingitemObj.setValues(existingItemValue);
                            vectorPtrPbObjectValueForCurrentSectionId->at(countOfItemInVectorList).setItemModel(existingitemObj);
                        }
                    }
                    
                    if(cellObj.getAssetTypeId() != -1)
                    {
                        vector<CAssetValue> assetValueVector;
                        CAssetValue assetValueObj;
                        assetValueObj.setType_id(cellObj.getAssetTypeId());
                        //itemValueObj.setFieldName();
                        assetValueObj.setUrl(cellObj.getValue());
                        assetValueVector.push_back(assetValueObj);
                        
                        if(itemIdAlreadyPresentInVector == kTrue)
                        {
                            if(existingAssetValue.size() == 0)
                            {
                                existingAssetValue.push_back(assetValueObj);
                            }
                            else
                            {
                                bool16 attrIdAlreadyPresent = kFalse;
                                for(int32 attCount =0; attCount < existingAssetValue.size(); attCount++)
                                {
                                    if(existingAssetValue.at(attCount).getType_id() == assetValueObj.getType_id())
                                    {
                                        attrIdAlreadyPresent = kTrue;
                                        break;
                                    }
                                }
                                if(attrIdAlreadyPresent == kFalse)
                                {
                                    existingAssetValue.push_back(assetValueObj);
                                }
                            }
                        }
                        
                        if(itemIdAlreadyPresentInVector == kFalse)
                        {
                            CItemModel itemModelObj;
                            CPbObjectValue childpbObjectValue;
                            
                            itemModelObj.setItemID(itemId);
                            itemModelObj.setAssetList(assetValueVector);
                            
                            childpbObjectValue.setisProduct(0);
                            childpbObjectValue.setObject_id(itemId);
                            childpbObjectValue.setPub_object_id(-1);
                            childpbObjectValue.setItemModel(itemModelObj);
                            vectorPtrPbObjectValueForCurrentSectionId->push_back(childpbObjectValue);
                        }
                        else if(itemIdAlreadyPresentInVector == kTrue)
                        {
                            existingitemObj.setAssetList(existingAssetValue);
                            vectorPtrPbObjectValueForCurrentSectionId->at(countOfItemInVectorList).setItemModel(existingitemObj);
                        }
                    }
                    
				}// For End oTableValue.getItemIds()
                

			}
		} // For End currentItemGroupValue->getLists()
	}//// End Adding List item to sectionVector list
}
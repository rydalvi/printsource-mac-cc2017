#include "VCPlugInHeaders.h"
#include "ConfigCache.h"
#include "CAlert.h"
#include "IAppFramework.h"

#define CA(x) CAlert::InformationAlert(x)


ConfigCache* ConfigCache::ccObjPtr = NULL;
bool ConfigCache::ccInstanceFlag = false;


ConfigCache* ConfigCache::getInstance() // SingleTone Implementation.
{
	//CA("Inside ConfigCache::getInstance");
	if(!ccInstanceFlag)
	{
		//CA("Creating Instance");
		ccObjPtr = new ConfigCache();
		ccInstanceFlag = true;
		ccObjPtr->loadConfigCache();
		return ccObjPtr;
	}
	else
	{
		if(!ccObjPtr->isConfigCacheLoaded)
		{
			ccObjPtr->loadConfigCache();
		}
		return ccObjPtr;
	}
}

void ConfigCache::clearInstace()
{
	isConfigCacheLoaded = kFalse;
	ccInstanceFlag = kFalse;
	configCacheMap.clear();	
}


bool16 ConfigCache::loadConfigCache()
{

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return kFalse;
	}
	
	bool16 result=ptrIAppFramework->callConfigJson(ccVectorObj);
	if(result)
	{				
		if(ccVectorObj.size() >0)
		{
			isConfigCacheLoaded = kTrue;
			for(int j=0; j < ccVectorObj.size() ; j++)
			{
				loadConfigCacheMap(ccVectorObj.at(j));
			}
		}
	}
	else
	{
		isConfigCacheLoaded = kFalse;
		return kFalse;
	}
	return kTrue;
}
	
void ConfigCache::loadConfigCacheMap(ConfigValue & configValueObj)
{
	/*InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		CAlert::InformationAlert("ptrIAppFramework is nil");
		return ;

	}*/
	configCacheMap.insert(map<PMString ,ConfigValue > ::value_type(configValueObj.getConfigName(),configValueObj));
	//ptrIAppFramework->LogDebug("INsert into Map : " + configValueObj.getConfigName());
	
}


ConfigValue ConfigCache::getConfigCacheByConfigName(PMString configName)
{
	ConfigValue cfvalObj;
	map< PMString, ConfigValue>::iterator it;
	int iskeyInMap = (int)configCacheMap.count(configName);
	if(iskeyInMap == 1)
	{
		it = configCacheMap.find(configName);
		cfvalObj = it->second;
	}
	return cfvalObj;
}

double ConfigCache::getintConfigValue1ByConfigName(PMString configName)
{
	ConfigValue cfvalObj = getConfigCacheByConfigName(configName);
	if( cfvalObj.getConfigValue1() == "")
		return -1;

	return cfvalObj.getConfigValue1().GetAsDouble();
}

PMString ConfigCache::getPMStringConfigValue1ByConfigName(PMString configName)
{
	ConfigValue cfvalObj = getConfigCacheByConfigName(configName);
	return cfvalObj.getConfigValue1();
}
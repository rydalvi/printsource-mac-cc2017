

#include "VCPlugInHeaders.h"

#include "OptionsValue.h"

OptionsValue::~OptionsValue()
{
}
OptionsValue::OptionsValue()
{
	PMString nStr("");
	LocaleName.SetString(nStr);
	PublicationName.SetString(nStr);
	ImagePath.SetString(nStr);
	DocPath.SetString(nStr);
	LocaleID.SetString(nStr);
	ProjectID.SetString(nStr);
//21-feb
	assetStatus		= 0;
	logLevel		= 0;
	missingFlag		= 0;
//21-feb
	SectionName.SetString(nStr);
	SectionID.SetString(nStr);
	isOneSource.SetString(nStr);

	default_Flow = -1;
	displayPartnerImgs	= 1;
	displayPickListImgs	= 1;
	showObjCount = 0;

	byPassForSingleSelectionSpray = 1;
	horizontalSpacing = 0;
	verticalSpacing = 0;

}

OptionsValue::OptionsValue(char* lName, char* pName,char* iPath,char* dPath, char* lID, char* pID, int32 aStatus, int32 lLevel, int32 MissingFlag ,char* sName, char* sID,char* isONE , int32 isDef ,int32 partnerImgs,int32 pickListImgs ,int32 isCntFlg, int32 inByPassForSingleSelectionSpray, int32 inHorizontalSpacing, int32 inVerticalSpacing) /*21-feb*/
{	
	LocaleName.SetCString(lName);
	PublicationName.SetCString(pName);	
	ImagePath.SetCString(iPath);
	DocPath.SetCString(dPath);
	LocaleID.SetCString(lID);
	ProjectID.SetCString(pID);
//21-feb
	assetStatus		= aStatus;
	logLevel		= lLevel;
	missingFlag		= MissingFlag;
//21-feb

	SectionName.SetString(sName);
	SectionID.SetString(sID);
	isOneSource.SetString(isONE);
	default_Flow = isDef;
	displayPartnerImgs	= partnerImgs;
	displayPickListImgs	= pickListImgs;
	showObjCount = isCntFlg;

	byPassForSingleSelectionSpray = inByPassForSingleSelectionSpray;
	horizontalSpacing = inHorizontalSpacing;
	verticalSpacing = inVerticalSpacing;
}

OptionsValue::OptionsValue(PMString lName,PMString pName,PMString iPath,PMString dPath, PMString lID, PMString pID, int32 aStatus, int32 lLevel,int32 MissingFlag ,PMString sName,PMString sID,PMString isONE ,int32 isDef,int32 partnerImgs,int32 pickListImgs ,int32 isCntFlg,  int32 inByPassForSingleSelectionSpray, int32 inHorizontalSpacing, int32 inVerticalSpacing)/*21-feb*/
{
	LocaleName      = lName;
	PublicationName = pName;	
	ImagePath		= iPath;
	DocPath			= dPath;
	LocaleID		= lID;
	ProjectID		= pID;
//21-feb
	assetStatus		= aStatus;
	logLevel		= lLevel;
	missingFlag		= MissingFlag;
//21-feb
	SectionName		= sName ;
	SectionID		= sID;
	isOneSource		= isONE;
	default_Flow		= isDef;
	displayPartnerImgs	= partnerImgs;
	displayPickListImgs	= pickListImgs;
	showObjCount = isCntFlg;

	byPassForSingleSelectionSpray = inByPassForSingleSelectionSpray;
	horizontalSpacing = inHorizontalSpacing;
	verticalSpacing = inVerticalSpacing;
}
OptionsValue::OptionsValue(OptionsValue& optsVal)
{
	LocaleName      = optsVal.getDefaultLocale();
	PublicationName = optsVal.getPublicationName();	
	ImagePath		= optsVal.getImagePath();
	DocPath			= optsVal.getIndesignDocPath();
	LocaleID		= optsVal.getLocaleID();
	ProjectID		= optsVal.getProjectID();
//21-feb
	assetStatus		= optsVal.getAssetStatus();
	logLevel		= optsVal.getLogLevel();
	missingFlag		= optsVal.getMissingFlag();
//21-feb
//06-sep
	PublicationComment = optsVal.getPublicationComment();
//06-sep	
	SectionName		= optsVal.getSectionName();
	SectionID		= optsVal.getSectionID();
	isOneSource		= optsVal.getIsOneSource();
	default_Flow		= optsVal.getDefault_Flow();
	displayPartnerImgs	= optsVal.getDisplayPartnerImgs();
	displayPickListImgs	= optsVal.getDisplayPickListImgs();
	showObjCount	=  optsVal.getShowObjCountFlag();

	byPassForSingleSelectionSpray = optsVal.getByPassForSingleSelectionSprayFlag();
	horizontalSpacing = optsVal.getHorizontalSpacing();
	verticalSpacing = optsVal.getVerticalSpacing();
}
void OptionsValue::operator=(OptionsValue& optsVal)
{	
	LocaleName      = optsVal.getDefaultLocale();
	PublicationName = optsVal.getPublicationName();	
	ImagePath		= optsVal.getImagePath();
	DocPath			= optsVal.getIndesignDocPath();
	LocaleID		= optsVal.getLocaleID();
	ProjectID		= optsVal.getProjectID();
//21-feb
	assetStatus		= optsVal.getAssetStatus();
	logLevel		= optsVal.getLogLevel();
	missingFlag		= optsVal.getMissingFlag();
//21-feb
//06-sep
	PublicationComment = optsVal.getPublicationComment();
//06-sep 

	SectionName		= optsVal.getSectionName();
	SectionID		= optsVal.getSectionID();
	isOneSource		= optsVal.getIsOneSource();
	
	default_Flow		= optsVal.getDefault_Flow();
	displayPartnerImgs	= optsVal.getDisplayPartnerImgs();
	displayPickListImgs	= optsVal.getDisplayPickListImgs();
	showObjCount	=	optsVal.getShowObjCountFlag();

	byPassForSingleSelectionSpray = optsVal.getByPassForSingleSelectionSprayFlag();
	horizontalSpacing = optsVal.getHorizontalSpacing();
	verticalSpacing = optsVal.getVerticalSpacing();
	
}

PMString& OptionsValue::getDefaultLocale(void)
{
	return LocaleName;
}
PMString& OptionsValue::getPublicationName(void)
{
	return PublicationName;
}
PMString& OptionsValue::getImagePath(void)
{
	return ImagePath;
}
PMString& OptionsValue::getIndesignDocPath(void)
{
	return DocPath;
}
PMString& OptionsValue::getLocaleID(void)
{
	return LocaleID;
}
PMString& OptionsValue::getProjectID(void)
{
	return ProjectID;
}

void OptionsValue::setDefaultLocale(PMString& name)
{
	LocaleName = name;
}

void OptionsValue::setPublicationName(PMString& name)
{
	PublicationName = name;
}
void OptionsValue::setImagePath(PMString& ipath)
{
	ImagePath.Clear();
	ipath.SetTranslatable(kFalse);
	ImagePath.SetString(ipath);	
}
void OptionsValue::setIndesignDocPath(PMString& dpath)
{
	DocPath.Clear();
	DocPath.SetString(dpath);	
}

void OptionsValue::setLocaleID(PMString& name)
{
	LocaleID = name;
}
void OptionsValue::setProjectID(PMString& name)
{
	ProjectID = name;
}

//------------ 21-feb chetan
int32 OptionsValue::getAssetStatus(void)
{
	return assetStatus;
}

int32 OptionsValue::getLogLevel(void)
{
	return logLevel;
}

void OptionsValue::setAssetStatus(int32 aStatus)
{
	assetStatus = aStatus ;
}

void OptionsValue::setLogLevel(int32 LogLevel)
{
	logLevel = LogLevel;
}
//------------ 21-feb end

//-- 06-sep
PMString& OptionsValue::getPublicationComment(void)
{
	return PublicationComment;
}

void OptionsValue::setPublicationComment(PMString& name)
{
	PublicationComment = name;
}
//-- 06-sep

//added by nitin
void OptionsValue:: setMissingFlag(int32 MissingFlag)
{
	missingFlag=MissingFlag;

}
int32 OptionsValue::getMissingFlag(void)
{
	return missingFlag;
}
//------------ 12 Sep end

void OptionsValue::setclientID(int32 clientid)
{
	clientID = clientid;

}
int32 OptionsValue::getclientID(void)
{
	return clientID;
}

		
PMString& OptionsValue::getSectionName(void)
{
	return SectionName;
}
void OptionsValue::setSectionName(PMString& sName )
{
	SectionName = sName;
}

PMString& OptionsValue::getSectionID(void)
{
	return SectionID;
}
void OptionsValue::setSectionID(PMString& sID)
{
	SectionID = sID;
}

PMString& OptionsValue::getIsOneSource(void)
{
	return isOneSource;
}
void OptionsValue::setIsOneSource(PMString& isOne)
{
	isOneSource = isOne ;
}
//------16-09-09---

int32 OptionsValue::getDefault_Flow(void)
{
	return default_Flow;
}

void OptionsValue::setDisplayPartnerImgs(int32 partnerImgs)
{
	displayPartnerImgs = partnerImgs;
}
int32 OptionsValue::getDisplayPartnerImgs(void)
{
	return displayPartnerImgs;
}
	
void OptionsValue::setDisplayPickListImgs(int32 pickListImgs)
{
	displayPickListImgs = pickListImgs;
}
int32 OptionsValue::getDisplayPickListImgs(void)
{
	return displayPickListImgs;
}

int32 OptionsValue::getShowObjCountFlag(void)
{
	return showObjCount;
}

void  OptionsValue::setShowObjCountFlag(int32 isShowCnt)
{
	showObjCount = isShowCnt;
}
	

int32 OptionsValue::getByPassForSingleSelectionSprayFlag(void)
{
	return byPassForSingleSelectionSpray;
}

void OptionsValue::setByPassForSingleSelectionSprayFlag(int32 inbyPassForSingleSelectionSpray)
{
	byPassForSingleSelectionSpray = inbyPassForSingleSelectionSpray;
}

int32 OptionsValue::getHorizontalSpacing(void)
{
	return horizontalSpacing;
}

void OptionsValue::setHorizontalSpacing(int32 inhorizontalSpacing)
{
	horizontalSpacing = inhorizontalSpacing;
}

int32 OptionsValue::getVerticalSpacing(void)
{
	return verticalSpacing;
}

void OptionsValue::setVerticalSpacing(int32 inverticalSpacing)
{
	verticalSpacing = inverticalSpacing;
}
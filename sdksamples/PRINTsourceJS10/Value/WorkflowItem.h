#ifndef __WORKFLOWITEM__
#define __WORKFLOWITEM__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "WorkflowItemValue.h"

using namespace std;

class WorkflowItem
{
private:
    double updateDate;
    double categoryId;
    int32 seqOrder;
    int32 allListsItemCount;
    double masterCategoryId;
    double typeId;
    int32 listsCount;
    int32 advTablesCount;
    double itemId;
    PMString number;
    PMString type;
    int32 statusId;
    vector<WorkflowItemValue> values;

public:
	WorkflowItem()
	{
        updateDate= -1;
        categoryId = -1;
        seqOrder = -1;
        allListsItemCount=-1;
        masterCategoryId =-1;
        typeId= -1;
        listsCount =-1;
        advTablesCount =-1;
        itemId =-1;
        number = "";
        type = "";
        statusId =-1;
        
	}
	
	~WorkflowItem()
	{
	}
	
    
    double getUpdateDate()
    {
        return updateDate;
    }
    
    void setUpdateDate(double id)
    {
        updateDate = id;
    }

    double getCategoryId()
    {
        return categoryId;
    }
    
    void setCategoryId(double id)
    {
        categoryId = id;
    }
    
    int32 getSeqOrder()
    {
        return seqOrder;
    }
    
    void setSeqOrder(int32 id)
    {
        seqOrder = id;
    }
    
    int32 getAllListsItemCount()
    {
        return allListsItemCount;
    }
    
    void setAllListsItemCount(int32 id)
    {
        allListsItemCount = id;
    }
    
    double getMasterCategoryId()
    {
        return masterCategoryId;
    }
    
    void setMasterCategoryId(double id)
    {
        masterCategoryId = id;
    }
    
    double getTypeId()
    {
        return typeId;
    }
    
    void setTypeId(double id)
    {
        typeId = id;
    }
    
    int32 getListsCount()
    {
        return listsCount;
    }
    
    void setListsCount(int32 id)
    {
        listsCount = id;
    }
    
    int32 getAdvTablesCount()
    {
        return advTablesCount;
    }
    
    void setAdvTablesCount(int32 id)
    {
        advTablesCount = id;
    }
    
    double getItemId()
    {
        return itemId;
    }
    
    void setItemId(double id)
    {
        itemId = id;
    }
    
    PMString getNumber()
    {
        return number;
    }
    
    void setNumber(PMString stringObj)
    {
        number = stringObj;
    }
    
    PMString getType()
    {
        return type;
    }
    
    void setType(PMString stringObj)
    {
        type = stringObj;
    }
    
    int32 getStatusId()
    {
        return statusId;
    }
    
    void setStatusId(int32 id)
    {
        statusId = id;
    }
    
    vector<WorkflowItemValue> getValues()
    {
        return values;
    }
    
    void setValues(vector<WorkflowItemValue> values)
    {
        this->values = values;
    }
};

#endif
#ifndef __ITEMVALUE_h__
#define __ITEMVALUE_h__

#include "VCPluginHeaders.h"
#include "PMString.h"

class ItemValue
{
private:	
	
	PMString	fieldName;	
	double		fieldId;
	PMString	fieldValue;
	double		lineId;
	PMString	picklistValueIdsString;
	
public:
	ItemValue()
	{		
		
		fieldName ="";	
		fieldId =-1;		
		fieldValue ="";		
		lineId =-1;
		picklistValueIdsString = "";
				
	}
	~ItemValue(){ }
	
	PMString getFieldName() {
		return fieldName;
	}
	double getFieldId() {
		return fieldId;
	}
	PMString getFieldValue() {
		return fieldValue;
	}
	double getLineId() {
		return lineId;
	}	

	PMString getPicklistValueIdsString(){
		return picklistValueIdsString;
	}
			
	
	void setFieldName(PMString str) {
		fieldName = str;
	}
	void setFieldId(double id) {
		fieldId = id;
	}
	void setFieldValue(PMString str) {
		fieldValue = str;
	}	
	void setLineId(double id) {
		lineId = id;
	}
	
	void setPicklistValueIdsString(PMString str){
		picklistValueIdsString = str;
	}


};

#endif
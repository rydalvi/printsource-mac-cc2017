#ifndef __PUBLICATIONNODE_H__
#define __PUBLICATIONNODE_H__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class PublicationNode
{
private:
	PMString strPublicationName;//Product Name/ Item no (Unique key attribute for Item)
	double pubId;				//will contain either ProductID/Item ID
	double parentId;
	int level;
	int32 seqNumber;
	int32 childCount;
	int hitCount;
	double referenceId;
	double typeId;				//will contain either Product Type id or Item Type ID
	int32 DesignerAction;
	int32 NewProduct;
	double PBObjectID;
	int32 isProduct;  // isProduct = 1 for Product , = 0 for Item, =2 for Hybrid Table
	bool16 isONEsource; // isONEsource = kTrue for ONEsource (Class)navigation. isONEsource = kFalse for Publication navogation.

	bool16 isStarred; // Used to show 'stared' staus of Product/Item in publication.

	double publicationID;
	double sectionID;
	double subSectionID;
	int32 iconCount;
	int32 childItemCount;
    double treeNodeId;
    double treeParentNodeId;

public:
	/*
		Constructor
		Initialise the members to proper values
	*/
	PublicationNode():strPublicationName(""), pubId(-1),parentId(-1), level(0), childCount(0), hitCount(0), PBObjectID(-1),isONEsource(kFalse), publicationID(-1)
		, seqNumber(0), isStarred(kFalse), NewProduct(0), isProduct(0), childItemCount(0), referenceId(-1), typeId(-1), DesignerAction(-1), sectionID(-1), subSectionID(-1), iconCount(0)  {}
	/*
		One time access to all the private members
	*/
	void setAll(PMString& pubName, double pubId,double pbobjectId, double parentId, int level,
		 int32 seqNumber, int32 childCount, int hitCount, double TypeID , double ReferenceId, int32 IsProduct, bool16 IsOneSource = kFalse)
	{
		this->strPublicationName=pubName;
		this->pubId=pubId;
		
		this->PBObjectID = pbobjectId;

		this->parentId=parentId;
		this->level=level;
		this->seqNumber=seqNumber;
		this->childCount=childCount;
		this->hitCount=hitCount;
		this->typeId =TypeID; 
		this->referenceId = ReferenceId;
		this->isProduct = IsProduct;
		this->isONEsource = IsOneSource;
		
	}

	double getReferenceId(void) { return this->referenceId; }
	void setReferenceId(double id) { this->referenceId=id; }
	double getTypeId(void) { return this->typeId; }
	void setTypeId(double id) { this->typeId=id; }
	
	/*
		@returns the sequence of the child
	*/
	int32 getSequence(void) { return this->seqNumber; }
	/*
		@returns name of the publication
	*/
	PMString getName(void) { return this->strPublicationName; }
	/*
		@returns the id of the publication
	*/
	double getPubId(void) { return this->pubId; }
	/*
		@returns the parent id for that node
	*/
	double getParentId(void) { return this->parentId; }
	/*
		@returns the level (distance from the root) of the publication
	*/
	int getLevel(void) { return this->level; }
	/*
		@returns the number of child for that parent
	*/
	int32 getChildCount(void) { return this->childCount; }
	/*
		@returns the number of count the node was accessed
	*/
	int getHitCount(void) { return this->hitCount; }
	/*
		Sets the publication id for the publication
		@returns none
	*/
	void setPubId(double pubId) { this->pubId=pubId; }
	/*
		Sets the parent id for the publication
		@returns none
	*/
	void setParentId(double parentId) { this->parentId=parentId; }
	/*
		Sets the level for the publication
		@returns none
	*/
	void setLevel(int level) { this->level=level; }
	/*
		Sets the child count for the publication
		@returns none
	*/
	void setChildCount(int32 childCount) { this->childCount=childCount; }
	/*
		Sets the publication name for the publication
		@returns none
	*/
	void setPublicationName(PMString& pubName) { this->strPublicationName=pubName; }
	/*
		Sets the number of hits for the publication
		@returns none
	*/
	void setHitCount(int hitCount) { this->hitCount=hitCount; }
	/*
		Sets the sequence number for the child
		@returns none
	*/
	void setSequence(int32 seqNumber){ this->seqNumber=seqNumber; }

	void setDesignerAction(int32 DesignerAction){ this->DesignerAction=DesignerAction; }

	int32 getDesignerAction(void) { return this->DesignerAction; }

	void setNewProduct(int32 NewProduct){this->NewProduct= NewProduct;}

	int32 getNewProduct(void) {return this->NewProduct; }	

	void setPBObjectID(double PBObjectID){this->PBObjectID= PBObjectID;}

	double getPBObjectID(void) {return this->PBObjectID; }

	void setIsProduct(int32 Flag){ this->isProduct = Flag;}

	int32 getIsProduct(void){ return this->isProduct;}

	void setIsONEsource(bool16 Flag){ this->isONEsource = Flag;}

	bool16 getIsONEsource(void){ return this->isONEsource;}

	void setIsStarred(bool16 Flag){ this->isStarred = Flag;}

	bool16 getIsStarred(void){ return this->isStarred;}

	void setPublicationID(double publication_id){ this->publicationID = publication_id;}

	double getPublicationID(void){ return this->publicationID;}

	void setSectionID(double section_id){ this->sectionID = section_id;}

	double getSectionID(void){ return this->sectionID;}

	void setSubSectionID(double subSection_id){ this->subSectionID = subSection_id;}

	double getSubSectionID(void){ return this->subSectionID;}

	//iconCount Added to hide show designer action icons
	void setIconCount(int32 count){ this->iconCount = count;}

	int32 getIconCount(void){ return this->iconCount;}

	
	void setChildItemCount(int32 count){ this->childItemCount = count;}

	int32 getChildItemCount(void){ return this->childItemCount;}
    
    void setTreeNodeId(double treeNodeId){ this->treeNodeId = treeNodeId;}
    
	double getTreeNodeId(void){ return this->treeNodeId;}
    
    void setTreeParentNodeId(double treeParentNodeId){ this->treeParentNodeId = treeParentNodeId;}
    
	double getTreeParentNodeId(void){ return this->treeParentNodeId;}

};

typedef vector<PublicationNode>PublicationNodeList, *PublicationNodeListPtr;

#endif
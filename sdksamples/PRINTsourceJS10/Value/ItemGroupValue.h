#ifndef __ITEMGROUPVALUE_h__
#define __ITEMGROUPVALUE_h__

#include "VCPluginHeaders.h"
#include "PMString.h"

class ItemGroupValue
{
private:	
	double		objectTypeId;
	PMString	fieldName;	
	double		elementId;
	//PMString	objectName;
	PMString    code;	
	PMString	objectValue;
	PMString	picklistValueIdsString;
	
public:
	ItemGroupValue()
	{		
		objectTypeId =-1;
		fieldName ="";	
		elementId =-1;
		//objectName ="";
		code ="";
		objectValue ="";
		picklistValueIdsString = "";
				
	}
	~ItemGroupValue(){ }
	
	PMString getFieldName() {
		return fieldName;
	}
	double getElementId() {
		return elementId;
	}
	double getObjectTypeId() {
		return objectTypeId;
	}	

	/*PMString getObjectName() {
		return objectName;
	}*/

	PMString getCode() {
		return code;
	}

	PMString getObjectValue() {
		return objectValue;
	}

	PMString getPicklistValueIdsString(){
		return picklistValueIdsString;
	}
		
	void setobjectTypeId(double id) {
		objectTypeId = id;
	}
	void setFieldName(PMString str) {
		fieldName = str;
	}
	void setElementId(double id) {
		elementId = id;
	}
	void setCode(PMString codeString) {
		code = codeString;
	}	
	/*void setObjectName(PMString name){
		objectName = name;
	}*/
	void setObjectValue(PMString value){
		objectValue = value;
	}

	void setPicklistValueIdsString(PMString str){
		picklistValueIdsString = str;
	}
};

#endif
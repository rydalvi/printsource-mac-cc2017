#ifndef __STRUCTURECACHEVALUE__
#define __STRUCTURECACHEVALUE__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"
#include "Attribute.h"
#include "ElementModel.h"
#include "ItemGroupValue.h"
#include "LanguageModel.h"
#include "PicklistGroup.h"
#include "StageValue.h"
#include "TypeModel.h"

using namespace std;

class StructureCacheValue
{
private:
	double		classificationId;
	PMString	classificationName;
	PMString	classNo;
	PMString	keywords;
	double		primaryContentManager;
	int32		level;
	double		parentId;
	vector<StructureCacheValue> children;
	vector<Attribute> itemFields;
	vector<CElementModel> itemGroupFields;
	vector<CTypeModel> itemAssetType;
	vector<CTypeModel> itemGroupAssetTypes;
	vector<CTypeModel> listType;
	vector<ItemGroupValue> objectValues;
	vector<CElementModel> sectionFields;
	vector<CTypeModel> sectionAssetTypes;
	vector<CLanguageModel> languages;
	vector<StageValue> statuses;
	vector<PicklistGroup> picklistGroups;

public:
	StructureCacheValue()
	{
	
		classificationId = -1 ;
		classificationName = "";
		classNo = "";
		keywords = "";
		primaryContentManager = -1;
		level = -1;
		parentId = -1;
	}
	
	~StructureCacheValue()
	{
	}
	

	
	void setLanguages(vector<CLanguageModel> languages) {
		this->languages = languages;
	}
	vector<CLanguageModel> getLanguages() {
		return languages;
	}
	void setStatuses(vector<StageValue> stages) {
		this->statuses = stages;
	}
	vector<StageValue> getStatuses() {
		return statuses;
	}
	vector<PicklistGroup> getPicklistGroups() {
		return picklistGroups;
	}
	void setPicklistGroups(vector<PicklistGroup> picklistGroups) {
		this->picklistGroups = picklistGroups;
	}

	vector<CTypeModel> getItemAssetTypes() {
		return itemAssetType;
	}
	void setItemAssetTypes(vector<CTypeModel> itemAssetTypes) {
		this->itemAssetType = itemAssetTypes;
	}
	vector<CTypeModel> getItemGroupAssetTypes() {
		return itemGroupAssetTypes;
	}
	void setItemGroupAssetTypes(vector<CTypeModel> itemGroupAssetTypes) {
		this->itemGroupAssetTypes = itemGroupAssetTypes;
	}
	vector<CTypeModel> getListTypes() {
		return listType;
	}
	void setListTypes(vector<CTypeModel> listTypes) {
		this->listType = listTypes;
	}
	vector<Attribute> getItemFields() {
		return itemFields;
	}
	void setItemFields(vector<Attribute> itemFields) {
		this->itemFields = itemFields;
	}
	vector<CElementModel> getItemGroupFields() {
		return itemGroupFields;
	}
	void setItemGroupFields(vector<CElementModel> itemGroupFields) {
		this->itemGroupFields = itemGroupFields;
	}
	vector<StructureCacheValue> getChildren() {
		return children;
	}
	double getCategoryId() {
		return classificationId;
	}

	double getClassificationId() {
		return classificationId;
	}
	void setClassificationId(double classificationId) {
		this->classificationId = classificationId;
	}
	PMString getClassificationName() {
		return  classificationName;
	}
	void setClassificationName(PMString classificationName) {
		this->classificationName = classificationName;
	}
	PMString getClassNo() {
		return classNo;
	}
	void setClassNo(PMString classNo) {
		this->classNo = classNo;
	}
	PMString getKeywords() {
		return keywords;
	}
	void setKeywords(PMString keywords) {
		this->keywords = keywords;
	}

	double getPrimaryContentManager() {
		return primaryContentManager;
	}
	void setPrimaryContentManager(double primaryContentManager) {
		this->primaryContentManager = primaryContentManager;
	}
	void setChildren(vector<StructureCacheValue> children) {
		this->children = children;
	}
	vector<ItemGroupValue> getObjectValues() {
		return objectValues;
	}
	void setObjectValues(vector<ItemGroupValue> objectValues) {
		this->objectValues = objectValues;
	}


	
	void setSectionFields(vector<CElementModel> categoryFields) {
		this->sectionFields = categoryFields;
	}
	vector<CElementModel> getSectionFields() {
		return sectionFields;
	}
	void setSectionAssetTypes(vector<CTypeModel> assetTypes) {
		this->sectionAssetTypes = assetTypes;
	}
	vector<CTypeModel> getSectionAssetTypes() {
		return sectionAssetTypes;
	}

	int32 getLevel() {
		return level;
	}
	void setLevel(int32 level) {
		this->level = level;
	}

	double getParentId() {
		return parentId;
	}
	void setParentId(double id) {
		this->parentId = id;
	}


};

#endif
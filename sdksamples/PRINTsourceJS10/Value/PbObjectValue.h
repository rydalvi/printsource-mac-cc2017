#ifndef __CPbObjectValue_h__
#define __CPbObjectValue_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "ObjectValue.h"
#include "ItemModel.h"

class CPbObjectValue
{
private:
	double		object_id;
	double		pub_object_id;
	double		publication_id;
	PMString	name;
	PMString	comments;
	int32		level_no;
	int32		childCount;
	int32		index; 
	int32		start_loc;
	int32		end_loc;	
	double		spreadId;
	bool8		new_product;
	bool8		add_to_spread;
	bool8		update_spread;
	bool8		update_copy;
	bool8		update_art;
	bool8		update_item_table;
	bool8		delete_from_spread;


	//bool8		legalApprovalFlag;

	CObjectValue ObValue;
	int32		page_no;
	int32		seq_order;
	CItemModel	ItemModelObj;
	int32		isProduct;  // isProduct = 1 for Product , = 0 for Item , = 2 for Hybrid Table
	double		object_type_id;

	bool8		StarredFlag1; // Flag1 from PbObjectValue.java
	PMString	Col1;				// Added By Rahul for WhirlPool
	PMString	Col2;
	PMString	number;
	
public:
	CPbObjectValue()
	{
		object_id = -1;	
		pub_object_id = -1;	
		publication_id = -1;	
		name = "";
		comments = "";
		level_no = -1;	
		childCount = -1;	
		index = -1;	
		start_loc = -1;	
		end_loc = -1;		
		spreadId = -1;	
		new_product = kFalse;
		add_to_spread = kFalse;
		update_spread = kFalse;
		update_copy = kFalse;
		update_art = kFalse;
		update_item_table = kFalse;
		delete_from_spread = kFalse;

		page_no = -1;
		seq_order = -1;		
		isProduct = -1; 
		object_type_id = -1;		
		StarredFlag1 = kFalse;
		Col1 = "";			
		Col2 = "";
		number = "";
	
	}
	~CPbObjectValue(){ }

	double getObject_id() {
		return object_id;
	}
	double getPub_object_id() {
		return pub_object_id;
	}
	double getPublication_id() {
		return publication_id;
	}
	PMString getName() {
		return name;
	}
	PMString getComments() {
		return comments;
	}
	int32 getLevel_no() {
		return level_no;
	}
	int32 getChildCount(){
		return childCount;
	}
	int32 getIndex() {
		return index;
	}
	int32 getStart_loc(){
		return start_loc;
	}
	int32 getEnd_loc(){
		return end_loc;
	}
	CObjectValue getObjectValue() {
		return ObValue;
	}

	CItemModel getItemModel() {
		return ItemModelObj;
	}

	double getSpreadId(){
		return spreadId;
	}
	bool8 getNew_product(){
		return new_product;
	}
	bool8 getAdd_to_spread(){
		return add_to_spread;
	}
	bool8 getUpdate_spread(){
		return  update_spread;
	}
	bool8 getUpdate_copy(){
		return  update_copy;
	}
	bool8 getUpdate_art(){
		return  update_art;
	}
	bool8 getUpdate_item_table(){
		return  update_item_table;
	}
	bool8 getDelete_from_spread(){
		return  delete_from_spread;
	}
	void setObject_id(double id) {
		object_id = id;
	}
	void setPub_object_id(double id) {
		pub_object_id = id;
	}
	void setPublication_id(double id) {
		publication_id = id;
	}
	void setName(PMString str) {
		name = str;
	}
	void setComments(PMString str){
		comments = str;
	}
	void setLevel_no(int32 id) {
		level_no = id;
	}
	void setChildCount(int32 count) {
		childCount=count;
	}
	void setIndex(int32 i) {
		index = i;
	}
	void setObjectValue(CObjectValue val){
		ObValue = val;
	}

	void setItemModel(CItemModel val){
		ItemModelObj = val;
	}

	void setStart_loc(int32 id){
		start_loc = id;
	}
	void setEnd_loc(int32 id){
		end_loc = id;
	}
	void setSpreadId(double sprdId){
		spreadId=sprdId;
	}
	void setNew_product(bool8 des1){
		new_product = des1;
	}
	void setAdd_to_spread(bool8 des1){
		add_to_spread = des1;
	}
	void setUpdate_spread(bool8 des1){
		update_spread = des1;
	}
	void setUpdate_copy(bool8 des1){
		update_copy = des1;
	}
	void setUpdate_art(bool8 des1){
		update_art = des1;
	}
	void setUpdate_item_table(bool8 des1){
		update_item_table = des1; 
	}
	void setDelete_from_spread(bool8 des1){
		delete_from_spread = des1;
	}

	//29 Mar addtion ..Yogesh 
	//void setLegal_approval(bool8 flag)
	//{
		//legalApprovalFlag = flag;
	//}
	//bool8 getLegal_approval()
	//{
		//return legalApprovalFlag;
	//}

	//8th May
	void setPage_no(int32 pageno)
	{
		page_no = pageno;
	}
	int32 getPage_no()
	{
		return page_no;
	}
	void setSeq_order(int32 seqorder)
	{
		seq_order = seqorder;
	}
	int32 getSeq_order()
	{
		return seq_order;
	}

	int32 getisProduct()
	{
		return isProduct;
	}

	void setisProduct(int32 Flag)
	{
		isProduct = Flag;
	}

	double getobject_type_id(void) {
		return object_type_id;
	}

	void setobject_type_id(double id)
	{
		object_type_id = id;
	}

 
	bool8 getStarredFlag1(){
		return  StarredFlag1;
	}
	
	void setStarredFlag1(bool8 flag1){
		StarredFlag1 = flag1;
	}

	PMString getCol1() {
		return Col1;
	}

	void setCol1(PMString str) {
		Col1 = str;
	}

	PMString getCol2() {
		return Col2;
	}

	void setCol2(PMString str) {
		Col2 = str;
	}

};

#endif
#ifndef __CItemTableValue_h__
#define __CItemTableValue_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"
#include "Column.h"
using namespace std;

class CItemTableValue
{
private:
	
	double tableTypeId;
	PMString name;
	bool8 transpose;
	bool8 printHeader;
	double tableID; //eventListId/ pubListId
	PMString stencil_name;	
	PMString description;

	vector<double> itemIds;
	vector<double> tableHeader;
	vector<vector<PMString> > tableData;
	
	vector<PMString> vec_notes;
	vector<Column> vecCol;

	vector<Column> vecCustomCol;
	vector<double> customitemIds;
	vector<vector<PMString> > customtableData;
	vector<double> customtableHeader;
	//bool red;

public:

	CItemTableValue()
	{
		tableTypeId =-1;
		name = "";
		transpose = false;
		printHeader = false;
		tableID = -1;
		stencil_name = "";
		description = "";
	}

//tabelTypeId	
	double getTableTypeID()
	{
		return tableTypeId;
	}
	void setTableTypeID(double id)
	{
		tableTypeId=id;
	}
//name
	PMString getName()
	{
		return name;
	}
	void setName(PMString tempName)
	{
		name = tempName;
	}
//transpose
	bool8 getTranspose()
	{
		return transpose;
	}
	void setTranspose(bool8 isTranspose)
	{
		transpose=isTranspose;
	}
//printHeader
	bool8 getPrintHeader()
	{
		return transpose;
	}
	void setPrintHeader(bool8 header)
	{
		printHeader=header;
	}
//itemIds
	vector<double> getItemIds()
	{
		return itemIds;
	}

	void setItemIds(vector<double> headers)
	{
		itemIds = headers;
	}
//tableHeader
	vector<double> getTableHeader()
	{
		return tableHeader;
	}

	void setTableHeader(vector<double> headers)
	{
		tableHeader = headers;
	}
	

//tableData
	vector<vector<PMString> > getTableData()
	{
		return tableData;
	}

	void setTableData(vector<vector<PMString> > data)
	{
		tableData = data;
	}

	//added on 22Sept..EventPrice addition
	vector<PMString> getNotesList()
	{
		return vec_notes;
	}

	void setNotesList(vector<PMString> data)
	{
		vec_notes = data;
	}
	//ended on 22Sept..EventPrice addition
	double getTableID()
	{
		return tableID;
	}
	void setTableID(double id)
	{
		tableID=id;
	}
	
	PMString getstencil_name()
	{
		return stencil_name;
	}
	void setstencil_name(PMString stnm)
	{
		stencil_name=stnm;
	}

	/*PMString getformattedUpdateDate()
	{
		return formattedUpdateDate;
	}
	void setformattedUpdateDate(PMString date)
	{
		formattedUpdateDate=date;
	}*/

	PMString getDescription()
	{
		return description;
	}
	void setDescription(PMString desp)
	{
		description=desp;
	}

	vector<Column> getColumn()
	{
		return vecCol;
	}

	void setColumn(vector<Column> data)
	{
		vecCol = data;
	}

	/*void setRed(bool redFlag)
	{
		red = redFlag;
	}
	bool getRed()
	{
		return red;
	}*/
	
	//customTableData
	vector<vector<PMString> > getCustomTableData()
	{
		return customtableData;
	}

	void setCustomTableData(vector<vector<PMString> > data)
	{
		customtableData = data;
	}

	//customColumn
	vector<Column> getCustomColumn()
	{
		return vecCustomCol;
	}

	void setCustomColumn(vector<Column> data)
	{
		vecCustomCol = data;
	}

	//customitemIds
	vector<double> getCustomItemIds()
	{
		return customitemIds;
	}

	void setCustomItemIds(vector<double> ids)
	{
		customitemIds = ids;
	}

	//customtableHeader
	vector<double> getCustomTableHeader()
	{
		return customtableHeader;
	}

	void setCustomTableHeader(vector<double> headers)
	{
		customtableHeader = headers;
	}

};

#endif
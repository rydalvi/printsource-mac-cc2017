#ifndef __CObjectValue_h__
#define __CObjectValue_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "AssetValue.h"
#include "ItemTableValue.h"
#include "ItemGroupValue.h"
#include "AdvanceTableScreenValue.h"

class CObjectValue
{
private:
	double		object_id;
	double		object_type_id;
	PMString    number;
	PMString	name;	
	PMString	comment;
	double		parent_id;
	//double		level_no;
	int32		childCount;
	//int32		index; // 
	//int32		ref_id;
	
	//added by Yogesh on 6 May 05
	double		vendor_id;
	//int32		root_id;
	double		class_id;
	//int32		seq_order;
	//int32		locale_id;
	double		brandId;
	double		manufacturerId;
	double		eventId;
	double		sectionResultId;
	int32		isStared;  // values 0 or 1
	int32		isNewProduct; // values 0 or 1

	vector<CAssetValue>		assetList;
	vector<ItemGroupValue>	values;
	vector<CItemTableValue>	lists;
	vector<CAdvanceTableScreenValue> advanceTables;
	
public:
	CObjectValue()
	{
		object_id = -1;
		object_type_id =-1;
		name ="";
		number ="";
		comment = "";
		parent_id =-1;
		//level_no =-1;;
		childCount =-1;
		//index = -1; // 
		//ref_id = -1;
		
		vendor_id =-1;
		//root_id =-1;
		class_id =-1;
		//seq_order =-1;
		//locale_id =-1;

		brandId =-1;
		manufacturerId =-1;
		eventId =-1;
		sectionResultId =-1;
		isStared =0;
		isNewProduct =0;
	}
	~CObjectValue(){ }
	//operator = (CObjectValue)

	double getObject_id() {
		return object_id;
	}
	double getObject_type_id() {
		return object_type_id;
	}
	PMString getName() {
		return name;
	}
	double getParent_id() {
		return parent_id;
	}
	/*int32 getLevel_no() {
		return level_no;
	}*/
	int32 getChildCount() {
		return childCount;
	}
	/*int32 getIndex() {
		return index;
	}
	int32 getRef_id() {
		return ref_id;
	}*/
	PMString get_number() {
		return number;
	}
	
	
	void setObject_id(double id) {
		object_id = id;
	}
	void setObject_type_id(double id) {
		object_type_id = id;
	}
	void setName(PMString str) {
		name = str;
	}
	void setParent_id(double id) {
		parent_id = id;
	}
	/*void setLevel_no(int32 id) {
		level_no = id;
	}*/
	void setChildCount(int32 count) {
		childCount=count;
	}
	/*void setIndex(int32 i) {
		index = i;
	}
	void setRef_id(int32 id) {
		ref_id = id;
	}*/
	void setNumber(PMString num){
		number = num;
	}

	//added by Yogesh on 6 May 05
	void setVendor_id(double vID)
	{
		vendor_id = vID;
	}

	double getVendor_id()
	{
		return vendor_id;
	}
	//end 6 May 05

	//-----------------------------xx----------------------------------------//
	//--------
	/*int32 getroot_id() {
		return root_id;
	}

	void setroot_id(int32 id) {
		root_id = id;
	}*/

	//-------
	double getclass_id() {
		return class_id;
	}

	void setclass_id(double id) {
		class_id = id;
	}

	//-------
	/*int32 getseq_order() {
		return seq_order;
	}

	void setseq_order(int32 id) {
		seq_order = id;
	}*/

	//----------
	/*int32 getlocale_id() {
		return locale_id;
	}

	void setlocale_id(int32 id) {
		locale_id = id;
	}*/

	PMString getComment() {
		return comment;
	}

	void setComment(PMString str) {
		comment = str;
	}

	double getBrandId() {
		return brandId;
	}

	void setBrandId(double id) {
		brandId = id;
	}

	double getManufacturerId() {
		return manufacturerId;
	}

	void setManufacturerId(double id) {
		manufacturerId = id;
	}

	double getEventId() {
		return eventId;
	}

	void setEventId(double id) {
		eventId = id;
	}

	
	double getSectionResultId() {
		return sectionResultId;
	}

	void setSectionResultId(double id) {
		sectionResultId = id;
	}

	void setAssets(vector<CAssetValue> vectorObj)
	{
		assetList = vectorObj;
	}

	vector<CAssetValue> getAssets()
	{
		return assetList;
	}

	void setObjectValues(vector<ItemGroupValue> vectorObj)
	{
		values = vectorObj;
	}

	vector<ItemGroupValue> getObjectValues()
	{
		return values;
	}

	void setLists(vector<CItemTableValue> vectorObj)
	{
		lists = vectorObj;
	}

	vector<CItemTableValue> getLists()
	{
		return lists;
	}

	//isStared
	int32 getIsStared() {
		return isStared;
	}

	void setIsStared(int32 id) {
		isStared = id;
	}

	//isNewProduct
	int32 getIsNewProduct() {
		return isNewProduct;
	}

	void setIsNewProduct(int32 id) {
		isNewProduct = id;
	}

	void setAdvanceTables(vector<CAdvanceTableScreenValue> vectorObj)
	{
		advanceTables = vectorObj;
	}

	vector<CAdvanceTableScreenValue> getAdvanceTables()
	{
		return advanceTables;
	}

};

#endif
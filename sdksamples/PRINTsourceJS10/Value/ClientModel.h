#ifndef __CClientModel_h__
#define __CClientModel_h__

#include "VCPluginHeaders.h"
#include "PMString.h"

class CClientModel
{
private : 
	double client_id;
	PMString name;
	double address_id;
	PMString assets_context;
	PMString websource_url;
	PMString cm_context;

public : 

	double getClient_id()
	{
		return client_id;
	}

	void setClient_id(double id)
	{
		client_id = id;
	}

	PMString getName()
	{
		return name;
	}

	void setName(PMString nam)
	{
		name = nam;
	}

	double getAddress_id()
	{
		return address_id;
	}

	void setAddress_id(double id)
	{
		address_id = id;
	}

	PMString getAssets_context()
	{
		return assets_context;
	}

	void setAssets_context(PMString context)
	{
		assets_context = context;
	}

	PMString getWebsource_url()
	{
		return websource_url;
	}

	void setWebsource_url(PMString url)
	{
		websource_url = url;
	}

	PMString getCm_context()
	{
		return cm_context;
	}

	void setCm_context(PMString context)
	{
		cm_context = context;
	}


};




#endif
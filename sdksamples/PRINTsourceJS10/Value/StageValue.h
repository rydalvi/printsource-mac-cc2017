#ifndef __STAGEVALUE__
#define __STAGEVALUE__

#include "VCPlugInHeaders.h"
#include "PMString.h"


using namespace std;

class StageValue
{
private:
		double stageId;
		PMString name;
		bool16 initialStage;

public:
	StageValue()
	{		
		stageId =-1;
		name ="";	
		initialStage =kFalse;				
	}

	~StageValue(){ }
	

	PMString getName() {
		return name;
	}
	void setName(PMString str) {
		name = str;
	}

	double getStageId() {
		return stageId;
	}
	void setStageId(double id) {
		stageId = id;
	}

	bool16 getInitialStage() {
		return initialStage;
	}
	void setInitialStage(int32 intiStage) {
		initialStage = intiStage;
	}
};

#endif
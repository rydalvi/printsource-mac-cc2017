#ifndef __EVENTCACHEVALUE__
#define __EVENTCACHEVALUE__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "AssetValue.h"
#include "ItemGroupValue.h"
#include "vector"

using namespace std;

class EventCacheValue
{
private:

	double		publicationId;
	PMString	name;
	PMString	pubTypeString;
	int32		webActive;
	vector<EventCacheValue> children;
	vector<ItemGroupValue> objectValues;
	vector<CAssetValue> assets;
	double		parentId;
	double		rootId;
	double		pubTypeId;
	

public:
	
	EventCacheValue()
	{
		publicationId = -1;
		name = "";
		pubTypeString = "";
		webActive = -1;
		parentId = -1;
		rootId = -1;
		pubTypeId = -1;
	}
	
	~EventCacheValue()
	{
	}

	vector<EventCacheValue> getChildren() {
		return children;
	}
	void setChildren(vector<EventCacheValue> children) {
		this->children = children;
	}
	double getEventId() {
		return publicationId;
	}
	void setEventId(double publicationId) {
		this->publicationId = publicationId;
	}
	PMString getName() {
		return name;
	}
	void setName(PMString name) {
		this->name = name;
	}
	
	PMString getEventType() {
		return pubTypeString;
	}
	void setEventType(PMString pubTypeString) {
		this->pubTypeString = pubTypeString;
	}
	void setObjectValues(vector<ItemGroupValue> eventSectionObjectValues) {
		objectValues = eventSectionObjectValues;
	}
	vector<ItemGroupValue> getObjectValues() {
		return objectValues;
	}
	void setAssets(vector<CAssetValue> sectionAssets) {
		this->assets = sectionAssets;
	}
	vector<CAssetValue> getAssets() {
		return assets;
	}
	void setWebActive(int32 int1) {
		webActive = int1;
	}
	int32 getWebActive() {
		return webActive;
	}

	void setParentId(double intId) {
		parentId = intId;
	}
	double getParentId() {
		return parentId;
	}
	void setRootId(double intId) {
		rootId = intId;
	}
	double getRootId() {
		return rootId;
	}

	PMString getObjectValueByAttributeIdLangId(double attrId, double langId)
	{
		PMString result("");
		if(attrId == -1 || attrId == -102 || attrId == -105 || attrId == -108 ) // Name
		{
			result = getName();
		}
		else if(attrId == 0 || attrId == -101 || attrId == -104 || attrId == -107) // Number
		{
			result.AppendNumber(PMReal(getEventId()));
		}
		else if(objectValues.size() > 0)
		{
			for(int count=0; count < objectValues.size(); count++)
			{
				if(objectValues.at(count).getElementId() == attrId)
				{
					result = objectValues.at(count).getObjectValue();
					break;
				}					
			}
		}
		return result;
	}


	void setPubTypeId(double intId) {
		pubTypeId = intId;
	}
	double getPubTypeId() {
		return pubTypeId;
	}
};

#endif
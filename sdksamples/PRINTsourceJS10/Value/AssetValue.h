#ifndef __CAssetValue_h__
#define __CAssetValue_h__

#include "VCPluginHeaders.h"
#include "PMString.h"

class CAssetValue
{
private:
	double asset_id;
	//long parent_id;
	double parent_type_id;
	long seq_order;
	double type_id;
	PMString url;
	//PMString file_name;
	PMString description;
	bool16 cd_active;
	bool16 print_active;
	bool16 web_active;
	long missing;
	long thumbnail;
	long flag1;
	long flag2;
	double locale_id;	
	int32 seqOrder;
	double mpvValueId;
    PMString pickListValue;
    PMString abbreviation;


public:

	CAssetValue()
	{
		asset_id = -1;
		parent_type_id = -1;
		seq_order = -1;
		type_id = -1;
		url = "";
		description = "";
		cd_active = kFalse;
		print_active = kFalse;
		web_active = kFalse;
		missing = -1;
		thumbnail = -1;
		flag1 = -1;
		flag2 = -1;
		locale_id = -1;
		seqOrder = -1;
		mpvValueId = -1;
        pickListValue ="";
        abbreviation="";
	}
	
	double getAsset_id()
	{
		return asset_id;
	}
	
	bool16 getCd_active()
	{
		return cd_active;
	}
	
	PMString getDescription()
	{
		return description;
	}
	
	PMString geturl()
	{
		return url;
	}
	
	double getLocale_id()
	{
		return locale_id;
	}
	
	double getParent_type_id()
	{
		return parent_type_id;
	}
	
	bool16 getPrint_active()
	{
		return print_active;
	}
	
	long getSeq_order()
	{
		return seq_order;
	}
	
	double getType_id()
	{
		return type_id;
	}
	
	bool16 getWeb_active()
	{
		return web_active;
	}
	
	void setAsset_id(double newAsset_id)
	{
		asset_id=newAsset_id;	
		
	}
	
	void setCd_active(bool16 newCd_active)
	{
		cd_active = newCd_active;
	}
	
	void setDescription(PMString newDescription)
	{		
		description=newDescription;		
	}

	void setUrl(PMString newUrl)
	{		
		url=newUrl;		
	}

	void setLocale_id(double newLocale_id)
	{
		locale_id = newLocale_id;
	}

	void setParent_type_id(double newParent_type_id)
	{
		
		parent_type_id=newParent_type_id;	
		
	}
	
	void setPrint_active(bool16 newPrint_active)
	{
		print_active = newPrint_active;
	}
	
	void setSeq_order(long newSeq_order)
	{		
		seq_order=newSeq_order;		
	}
	
	void setType_id(double newType_id)
	{		
		type_id=newType_id;		
	}
	
	void setWeb_active(bool16 newWeb_active)
	{
		web_active = newWeb_active;
	}

	/*void setMpv_value_id(long mpv_val)
	{
		Mpv_value_id = mpv_val;
	}
	long getMpv_value_id()
	{
		return Mpv_value_id;
	}*/

	long getMissing()
	{
		return missing;
	}

	void setMissing(long newMissing)
	{
		missing = newMissing;
	}

	long getThumbnail()
	{
		return thumbnail;
	}

	void setThumbnail(long newThumbnail)
	{
		thumbnail = newThumbnail;
	}

	long getFlag1()
	{
		return flag1;
	}

	void setFlag1(long newFlag1)
	{
		flag1 = newFlag1;
	}

	long getFlag2()
	{
		return flag2;
	}

	void setFlag2(long newFlag2)
	{
		flag2 = newFlag2;
	}

	int32 getSeqOrder() {
		return seqOrder;
	}
	void setSeqOrder(int32 seqOrder) {
		seqOrder = seqOrder;
	}

	double getMpv_value_id()
	{
		return mpvValueId;
	}

	void setMpv_value_id(double id)
	{
		mpvValueId = id;
	}
    
    PMString getPickListValue()
	{
		return pickListValue;
	}
    
    void setPickListValue(PMString newpickListValue)
	{
		pickListValue=newpickListValue;
	}
    
    PMString getAbbreviation()
	{
		return abbreviation;
	}
    
    void setAbbreviation(PMString newAbbreviation)
	{
		abbreviation=newAbbreviation;
	}


};

#endif
#ifndef __FILTERVALUE__
#define __FILTERVALUE__

#include "VCPluginHeaders.h"
#include "PMString.h"

using namespace std;

class FilterValue
{
private:
    PMString filterId;
    PMString filterName;
    
public:
    FilterValue()
    {
        filterId = "";
        filterName = "";
    }
    
    ~FilterValue()
    {
    }
    
    PMString getFilterId() {
        return filterId;
    }
    void setFilterId(PMString filterId) {
        this->filterId = filterId;
    }
    PMString getFilterName() {
        return filterName;
    }
    void setFilterName(PMString filterName) {
        this->filterName = filterName;
    }
};

#endif
#ifndef __PICKLISTFIELDS__
#define __PICKLISTFIELDS__


#include "VCPlugInHeaders.h"


using namespace std;

class PicklistField
{
 private: 
		PMString mpvPrefix;
        PMString mpvSeparator;
		PMString mpvSuffix;
        double pvTypeId;
		
	public:
	
	PicklistField()
	{
		mpvPrefix = "";
		mpvSeparator = "";
		mpvSuffix = "";
		pvTypeId = -1;
	}

	PMString getMpvPrefix() {
		return mpvPrefix;
	}

	void setMpvPrefix(PMString mpvPrefix) {
		this->mpvPrefix = mpvPrefix;
	}

	PMString getMpvSeparator() {
		return mpvSeparator;
	}

	void setMpvSeparator(PMString mpvSeparator) {
		this->mpvSeparator = mpvSeparator;
	}

	PMString getMpvSuffix() {
		return mpvSuffix;
	}

	void setMpvSuffix(PMString mpvSuffix) {
		this->mpvSuffix = mpvSuffix;
	}

	double getPicklistGroupId() {
		return pvTypeId;
	}

	void setPicklistGroupId(double pvTypeId) {
		this->pvTypeId = pvTypeId;
	}
	
	//abstract int32 getFieldId();

	
};

#endif
#ifndef __LANGUAGEMODEL_H__
#define __LANGUAGEMODEL_H__

#include "VCPlugInHeaders.h"
#include "PMString.h"

class CLanguageModel{

private:

	double language_ID;
    PMString language_Name; 
    PMString abbreviation; 
	double defaultLanguage;

public:

	double getLanguageID()
	{
		return language_ID;
	}

	PMString getLangugeName()
	{
		return language_Name;
	}

	PMString getAbbrevation()
	{
		return abbreviation;
	}

	void setLanguageID(double langID)
	{
		language_ID = langID;
	}

	void setLanguageName(PMString langName)
	{
		language_Name = langName;	
	}

	void setAbbreviation(PMString abbre)
	{
		abbreviation = abbre;
	}

	double getdefaultLanguage()
	{
		return defaultLanguage;
	}

	void setdefaultLanguage(double deflangID)
	{
		defaultLanguage = deflangID;
	}

};
#endif
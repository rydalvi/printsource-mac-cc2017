#ifndef __CONFIGVALUE__
#define __CONFIGVALUE__

#include "VCPluginHeaders.h"
#include "PMString.h"

using namespace std;

class ConfigValue
{
private:
	double configId;
	double clientId;
	PMString configName;
	PMString configValue1;
	PMString configValue2;
	PMString configValue3;
	int32 active;

public:
	ConfigValue()
	{
		configId = -1;
		clientId = -1;
		configValue1 = "";
		configValue2 = "";
		configValue3 = "";
		active = -1;
	}
	
	~ConfigValue()
	{
	}

	double getConfigId() {
		return configId;
	}
	void setConfigId(double configId) {
		this->configId = configId;
	}
	double getClientId() {
		return clientId;
	}
	void setClientId(double clientId) {
		this->clientId = clientId;
	}
	PMString getConfigName() {
		return configName;
	}
	void setConfigName(PMString configName) {
		this->configName = configName;
	}
	PMString getConfigValue1() {
		return configValue1;
	}
	void setConfigValue1(PMString configValue1) {
		this->configValue1 = configValue1;
	}
	PMString getConfigValue2() {
		return configValue2;
	}
	void setConfigValue2(PMString configValue2) {
		this->configValue2 = configValue2;
	}
	PMString getConfigValue3() {
		return configValue3;
	}
	void setConfigValue3(PMString configValue3) {
		this->configValue3 = configValue3;
	}
	int32 getActive() {
		return active;
	}
	void setActive(int32 active) {
		this->active = active;
	}
	
};

#endif
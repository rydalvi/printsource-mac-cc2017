
#ifndef __IClientOptions__
#define __IClientOptions__

#include "IPMUnknown.h"
#include "OptnsDlgID.h"

class IClientOptions : public IPMUnknown
{
	public:	
		enum { kDefaultIID = IID_ICLIENTOPTIONS };
	
		virtual double getDefaultLocale(PMString&)=0;
		virtual double getDefPublication(PMString&)=0;
		virtual PMString getImageDownloadPath(void)=0;
		virtual PMString getDocumentDownloadPath(void)=0;
		virtual double getDefPublicationDlg(PMString&, bool16)=0;
		virtual void setdoRefreshFlag(bool16)=0;

		virtual PMString getWhiteBoardMediaPath()=0;
		virtual bool8 checkforFolderExists(PMString&) = 0;

		virtual void setLocale(PMString&, double&)=0;
		virtual void setPublication(PMString&, double&)=0;
		virtual void setSection(PMString& ,double&)=0;
		
		//virtual void setDefaultFLow(int32 );
		virtual void set_DisplayPartnerImages(int32 )=0;
		virtual void set_DisplayPickListImages(int32 )=0;
    
        virtual void set_HorizontalAndVerticleSpacing(int32 hSpace, int32 vSpace)=0;
};

#endif


/*
InterfacePtr<IClientOptions> cop((IClientOptions*) ::CreateObject(kClientOptionsReaderBoss,IID_ICLIENTOPTIONS));
if(cop!=nil){
	// call IClientOptions methods
	PMString imagePath = getImageDownloadPath();
}

*/
#ifndef __PRIMAGEHELPER_H__
#define __PRIMAGEHELPER_H__

#include "IPMUnknown.h"
#include "DCNID.h"
#include "vector"
using namespace std;
class IPRImageHelper : public IPMUnknown
{
public:
		enum	{kDefaultIID = IID_IPRODUCTIMAGEIFACE};
		//IPRImageHelper(){};
		virtual void showImagePanel() = 0 ;
		virtual void showProductImages(double objectID , double langID , double parentID , double parentTypeID , double sectionID ) = 0 ;
		virtual void positionImagePanel(int32 left , int32 righ , int32 top , int32 bottom) = 0 ;
		virtual void closeImagePanel() = 0 ;
		virtual void showItemImages(double objectID , double langID , double parentID , double parentTypeID , double sectionID, int32 isProduct) = 0 ;
		virtual bool16 isImagePanelOpen() = 0 ;
		virtual bool16 EmptyImageGrid() = 0;
		virtual void WrapperForAddThumbnailImage(PMString wrpperPath, PMString wrpperDisplayName) = 0 ;

		virtual void setattributesWithPVMPV(vector<double> temp,bool16 isProduct) = 0;
		virtual bool16 isAtt_EleIDSVecFilled() = 0;

		
};


#endif
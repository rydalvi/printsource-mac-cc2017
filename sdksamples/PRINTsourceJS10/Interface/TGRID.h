//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __TGRID_h__
#define __TGRID_h__

#include "SDKDef.h"

// Company:
#define kTGRCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kTGRCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kTGRPluginName	"APJS9_TagReader"			// Name of this plug-in.
#define kTGRPrefixNumber	0xDBA1C00 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kTGRVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kTGRAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kTGRPrefixNumber above to modify the prefix.)
#define kTGRPrefix		RezLong(kTGRPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kTGRStringPrefix	SDK_DEF_STRINGIZE(kTGRPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kTGRMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kTGRMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kTGRPluginID, kTGRPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kTGRTagReaderBoss,	kTGRPrefix + 3)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 4)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kTGRBoss, kTGRPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_ITAGREADER, kTGRPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_ITGRINTERFACE, kTGRPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kTagReaderImpl,	kTGRPrefix + 1 )
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 1)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 2)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 3)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 4)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kTGRImpl, kTGRPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kTGRAboutActionID, kTGRPrefix + 0)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kTGRActionID, kTGRPrefix + 25)


// WidgetIDs:
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 2)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 3)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 4)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kTGRWidgetID, kTGRPrefix + 25)


// "About Plug-ins" sub-menu:
#define kTGRAboutMenuKey			kTGRStringPrefix "kTGRAboutMenuKey"
#define kTGRAboutMenuPath		kSDKDefStandardAboutMenuPath kTGRCompanyKey

// "Plug-ins" sub-menu:
#define kTGRPluginsMenuKey 		kTGRStringPrefix "kTGRPluginsMenuKey"
#define kTGRPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kTGRCompanyKey kSDKDefDelimitMenuPath kTGRPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kTGRAboutBoxStringKey	kTGRStringPrefix "kTGRAboutBoxStringKey"
#define kTGRTargetMenuPath kTGRPluginsMenuPath

// Menu item positions:


// Initial data format version numbers
#define kTGRFirstMajorFormatNumber  RezLong(1)
#define kTGRFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kTGRCurrentMajorFormatNumber kTGRFirstMajorFormatNumber
#define kTGRCurrentMinorFormatNumber kTGRFirstMinorFormatNumber

#endif // __TGRID_h__

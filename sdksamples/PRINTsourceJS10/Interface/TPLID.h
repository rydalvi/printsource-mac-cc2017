//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __TPLID_h__
#define __TPLID_h__

#include "SDKDef.h"

// Company:
#define kTPLCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kTPLCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kTPLPluginName	"Template Builder"			// Name of this plug-in.
#define kTPLPrefixNumber	0xDBA1700 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kTPLVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kTPLAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kTPLPrefixNumber above to modify the prefix.)
#define kTPLPrefix		RezLong(kTPLPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kTPLStringPrefix	SDK_DEF_STRINGIZE(kTPLPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kTPLMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kTPLMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kTPLPluginID, kTPLPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kTPLActionComponentBoss, kTPLPrefix + 30)
DECLARE_PMID(kClassIDSpace, kTPLStringRegisterBoss,		kTPLPrefix + 1)
DECLARE_PMID(kClassIDSpace, kTPLMenuRegisterBoss,		kTPLPrefix + 2)
DECLARE_PMID(kClassIDSpace, kTPLActionRegisterBoss,		kTPLPrefix + 3)
DECLARE_PMID(kClassIDSpace, kTPLPanelWidgetBoss,		kTPLPrefix + 54)
DECLARE_PMID(kClassIDSpace, kTPLPanelRegisterBoss,		kTPLPrefix + 5)
DECLARE_PMID(kClassIDSpace, kTPLPFPanelBoss,			kTPLPrefix + 6)
DECLARE_PMID(kClassIDSpace, kTPLDropDownListWidgetBoss,	kTPLPrefix + 7)
DECLARE_PMID(kClassIDSpace, kTPLPFListBoxWidgetBoss,	kTPLPrefix + 8)
DECLARE_PMID(kClassIDSpace, kTPLPGListBoxWidgetBoss,	kTPLPrefix + 9)
DECLARE_PMID(kClassIDSpace, kTPLPRListBoxWidgetBoss,	kTPLPrefix + 10)
DECLARE_PMID(kClassIDSpace, kTPLItemListBoxWidgetBoss,	kTPLPrefix + 11)
DECLARE_PMID(kClassIDSpace, kTPLLstboxStatTxtWidgetBoss,kTPLPrefix + 12)
DECLARE_PMID(kClassIDSpace, kTPLCustFlavHlprBoss,		kTPLPrefix + 13)
DECLARE_PMID(kClassIDSpace, kTPLPageItemScrapHandlerBoss,kTPLPrefix + 14)
DECLARE_PMID(kClassIDSpace, kTPLDocBoss,				kTPLPrefix + 15)
DECLARE_PMID(kClassIDSpace, kTPLDocResponderBoss,		kTPLPrefix + 16)
DECLARE_PMID(kClassIDSpace, kTPLRadioBtnWidgetBoss,		kTPLPrefix + 17)
DECLARE_PMID(kClassIDSpace, kTemplatesDialogCloserBoss,	kTPLPrefix + 19)
DECLARE_PMID(kClassIDSpace, kTPLLoginEventsHandler,		kTPLPrefix + 20)
DECLARE_PMID(kClassIDSpace, kTPLProjectListBoxWidgetBoss,kTPLPrefix + 21)
DECLARE_PMID(kClassIDSpace, kTPLIconSuiteWidgetBoss,    kTPLPrefix + 22)
DECLARE_PMID(kClassIDSpace, kActSelBoss,				kTPLPrefix + 23)
// for tree list
DECLARE_PMID(kClassIDSpace, kTPLTreeViewWidgetBoss,		kTPLPrefix + 24)
DECLARE_PMID(kClassIDSpace, kTPLTreeTextBoxWidgetBoss,	kTPLPrefix + 25)
DECLARE_PMID(kClassIDSpace, kTPLTreeNodeWidgetBoss,		kTPLPrefix + 26)
DECLARE_PMID(kClassIDSpace, kTPLPanelActionFilterBoss,	kTPLPrefix + 27)



// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_ITPLWIDGETOBSERVER,		kTPLPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_ITPLSELECTIONOBSERVER,	kTPLPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_IPSTLSTOBSERVER,		kTPLPrefix + 2)
DECLARE_PMID(kInterfaceIDSpace, IID_ITPLTBLBSCSUITE,			kTPLPrefix + 3)
DECLARE_PMID(kInterfaceIDSpace, IID_ITEMPLATEDIALOGCLOSER,	kTPLPrefix + 5)
DECLARE_PMID(kInterfaceIDSpace, IID_ITPLTEXTMISCELLANYSUITEE,	kTPLPrefix + 6)
// for tree list
DECLARE_PMID(kInterfaceIDSpace, IID_IPNLTRVSHADOWEVENTHANDLER,	kTPLPrefix + 7)

DECLARE_PMID(kInterfaceIDSpace, IID_IOVERLAPPROPERTIES,	kTPLPrefix + 8)



// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kTPLActionComponentImpl,		kTPLPrefix + 0)
DECLARE_PMID(kImplementationIDSpace, kTPLSelectionObserverImpl,		kTPLPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kTPLWidgetObserverImpl,		kTPLPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kTPLPFListBoxObserverImpl,		kTPLPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kTPLPGListBoxObserverImpl,		kTPLPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kTPLPRListBoxObserverImpl,		kTPLPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kTPLItemListBoxObserverImpl,	kTPLPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kTPLCustFlavHlprImpl,			kTPLPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kTPLDragSourceImpl,			kTPLPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kTPLLstboxTextObserverImpl,	kTPLPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kTPLPageItemScrapHandlerImpl,	kTPLPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kTPLTblBscSuiteASBImpl,			kTPLPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kTPLTblBscSuiteTextCSBImpl,		kTPLPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kTPLDocObserverImpl,			kTPLPrefix + 13)
DECLARE_PMID(kImplementationIDSpace, kTPLDocServiceProviderImpl,	kTPLPrefix + 14)
DECLARE_PMID(kImplementationIDSpace, kTPLDocResponderImpl,			kTPLPrefix + 15)
DECLARE_PMID(kImplementationIDSpace, kTPLRadioBtnsObserverImpl,		kTPLPrefix + 16)
DECLARE_PMID(kImplementationIDSpace, kTemplatesDialogCloserImpl,	kTPLPrefix + 19)
DECLARE_PMID(kImplementationIDSpace, kTPLLoginEventsHandlerImpl,	kTPLPrefix + 20)
DECLARE_PMID(kImplementationIDSpace, kTPLTextMiscellanySuiteASBImpl,	kTPLPrefix + 21)
DECLARE_PMID(kImplementationIDSpace, kTPLTextMiscellanySuiteLayoutCSBImpl,kTPLPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kTPLProjectListBoxObserverImpl,kTPLPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kMYframeObserverImpl,			kTPLPrefix + 24)
//// for tree list
DECLARE_PMID(kImplementationIDSpace, kTPLTreeViewHierarchyAdapterImpl,kTPLPrefix + 25)
DECLARE_PMID(kImplementationIDSpace, kTPLTreeViewWidgetMgrImpl,     kTPLPrefix + 26)
DECLARE_PMID(kImplementationIDSpace, kTPLTreeObserverImpl,			kTPLPrefix + 27)
DECLARE_PMID(kImplementationIDSpace, kPnlTrvNodeEHImpl,				kTPLPrefix + 28)
DECLARE_PMID(kImplementationIDSpace, kDynMnuDynamicMenuImpl,		kTPLPrefix + 29)
DECLARE_PMID(kImplementationIDSpace, kTPLActionFilterImpl,			kTPLPrefix + 30)
DECLARE_PMID(kImplementationIDSpace, kTPLPanalControlViewImpl,	kTPLPrefix + 31)
DECLARE_PMID(kImplementationIDSpace, kIoverlapPropertiesImpl,	kTPLPrefix + 32)

// ActionIDs:
//DECLARE_PMID(kActionIDSpace, kTPLAboutActionID, kTPLPrefix + 0)
DECLARE_PMID(kActionIDSpace, kTPLPanelWidgetActionID,		kTPLPrefix + 0)
DECLARE_PMID(kActionIDSpace, kTPLPanelPSMenuActionID,		kTPLPrefix + 2)
DECLARE_PMID(kActionIDSpace, kTPLAddTabbedTextID,			kTPLPrefix + 3)
DECLARE_PMID(kActionIDSpace, kTPLAddTableHeaderID,			kTPLPrefix + 4)
DECLARE_PMID(kActionIDSpace, kTPLSeparatorActionID,			kTPLPrefix + 5)
DECLARE_PMID(kActionIDSpace, kTPLLanguageRealID,			kTPLPrefix + 6)
DECLARE_PMID(kActionIDSpace, kTPLShowCategoryTreeID,		kTPLPrefix + 7)
DECLARE_PMID(kActionIDSpace, kTPLSeparator2ActionID,		kTPLPrefix + 8)
DECLARE_PMID(kActionIDSpace, kTPLAddAsDisplayNameID,		kTPLPrefix + 9)
DECLARE_PMID(kActionIDSpace, kTPLAddAsCompAttWidgetID,		kTPLPrefix + 10)
DECLARE_PMID(kActionIDSpace, kTPLAddAsXRefAttWidgetID,		kTPLPrefix + 11)
DECLARE_PMID(kActionIDSpace, kTPLAddAsAccessoryAttWidgetID,	kTPLPrefix + 12)
DECLARE_PMID(kActionIDSpace, kTPLAddImageDescriptionID,		kTPLPrefix + 13)
DECLARE_PMID(kActionIDSpace, kTPLAddAsMMYAttWidgetID,		kTPLPrefix + 14)
DECLARE_PMID(kActionIDSpace, kTPLAddAsMMYSortAttWidgetID,		kTPLPrefix + 15)
DECLARE_PMID(kActionIDSpace, kTPLTagInfoWidgetID,		kTPLPrefix + 16)
DECLARE_PMID(kActionIDSpace, kTPLSeparator3ActionID,		kTPLPrefix + 17)
DECLARE_PMID(kActionIDSpace, kTPLSeparator4ActionID,		kTPLPrefix + 18)

//Warning
/* Always put the following entry at the last.If you want any more 
actionIDs add it before the following entry.
*/
DECLARE_PMID(kActionIDSpace, kTPLDynMenuStartID,			kTPLPrefix + 19)

// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kTPLPanelWidgetID,			kTPLPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kTPLPFPanelWidgetID,		kTPLPrefix + 1) 
DECLARE_PMID(kWidgetIDSpace, kTPLPFPanelTextWidgetID,	kTPLPrefix + 2) 
DECLARE_PMID(kWidgetIDSpace, kTPLDropDownWidgetID,		kTPLPrefix + 3) 

DECLARE_PMID(kWidgetIDSpace, kTPLPFPanelGroupPanelID,	kTPLPrefix + 4) 
DECLARE_PMID(kWidgetIDSpace, kTPLPFGroupPanelID,		kTPLPrefix + 5) 
DECLARE_PMID(kWidgetIDSpace, kTPLPGGroupPanelID,		kTPLPrefix + 6) 
DECLARE_PMID(kWidgetIDSpace, kTPLPRGroupPanelID,		kTPLPrefix + 7) 
DECLARE_PMID(kWidgetIDSpace, kTPLItemGroupPanelID,		kTPLPrefix + 8) 

DECLARE_PMID(kWidgetIDSpace, kTPLPFPanelLstboxWidgetID,	kTPLPrefix + 9) 
DECLARE_PMID(kWidgetIDSpace, kTPLPGPanelLstboxWidgetID,	kTPLPrefix + 10) 
DECLARE_PMID(kWidgetIDSpace, kTPLPRPanelLstboxWidgetID,	kTPLPrefix + 11) 
DECLARE_PMID(kWidgetIDSpace, kTPLItemPanelLstboxWidgetID,		kTPLPrefix + 12) 

DECLARE_PMID(kWidgetIDSpace, kAppendRadBtnWidgetID,				kTPLPrefix + 13) 
DECLARE_PMID(kWidgetIDSpace, kEmbedRadBtnWidgetID,				kTPLPrefix + 14) 

DECLARE_PMID(kWidgetIDSpace, kPFPanelListboxParentWidgetId,		kTPLPrefix + 15) 
DECLARE_PMID(kWidgetIDSpace, kPFPanelTextWidgetID,				kTPLPrefix + 16) 

DECLARE_PMID(kWidgetIDSpace, kPGPanelListboxParentWidgetId,		kTPLPrefix + 17) 
DECLARE_PMID(kWidgetIDSpace, kPGPanelTextWidgetID,				kTPLPrefix + 18) 

DECLARE_PMID(kWidgetIDSpace, kPRPanelListboxParentWidgetId,		kTPLPrefix + 19) 
DECLARE_PMID(kWidgetIDSpace, kPRPanelTextWidgetID,				kTPLPrefix + 20) 

DECLARE_PMID(kWidgetIDSpace, kItemPanelListboxParentWidgetId,	kTPLPrefix + 21) 
DECLARE_PMID(kWidgetIDSpace, kItemPanelTextWidgetID,			kTPLPrefix + 22) 

DECLARE_PMID(kWidgetIDSpace, kTPLChkIconSuiteWidgetID,			kTPLPrefix + 23) 
DECLARE_PMID(kWidgetIDSpace, kTPLUnChkIconSuiteWidgetID,		kTPLPrefix + 24) 
DECLARE_PMID(kWidgetIDSpace, kTPLClassTreeIconSuiteWidgetID,	kTPLPrefix + 25) 
DECLARE_PMID(kWidgetIDSpace, kClassHierarchyStaticTxtWidgetID,	kTPLPrefix + 26) 
DECLARE_PMID(kWidgetIDSpace, kTagFrameWidgetID,					kTPLPrefix + 27) 
DECLARE_PMID(kWidgetIDSpace, kTPLRefreshIconSuiteWidgetID,		kTPLPrefix + 28) 
DECLARE_PMID(kWidgetIDSpace, kTPLCopyIconSuiteWidgetID,			kTPLPrefix + 29) 
DECLARE_PMID(kWidgetIDSpace, kTPLImageIconSuiteWidgetID,		kTPLPrefix + 30) 
DECLARE_PMID(kWidgetIDSpace, kTPLTableIconSuiteWidgetID,		kTPLPrefix + 31) 

DECLARE_PMID(kWidgetIDSpace, kTPLProjectPanelID,				kTPLPrefix + 32) 
DECLARE_PMID(kWidgetIDSpace, kTPLProjectPanelLstboxWidgetID,	kTPLPrefix + 33) 
DECLARE_PMID(kWidgetIDSpace, kProjectPanelListboxParentWidgetId,kTPLPrefix + 34) 
DECLARE_PMID(kWidgetIDSpace, kProjectPanelTextWidgetID,         kTPLPrefix + 35) 

/// for tree list
DECLARE_PMID(kWidgetIDSpace, kProjectTreeViewWidgetID,         kTPLPrefix + 36) 
DECLARE_PMID(kWidgetIDSpace, kTPLTreePanelNodeWidgetID,        kTPLPrefix + 37) 
DECLARE_PMID(kWidgetIDSpace, kTPLTreeNodeExpanderWidgetID,     kTPLPrefix + 38) 
DECLARE_PMID(kWidgetIDSpace, kTPLTreeNodeNameWidgetID,		   kTPLPrefix + 39) 
DECLARE_PMID(kWidgetIDSpace, kTPLTreeCopyIconSuiteWidgetID,	   kTPLPrefix + 40) 
DECLARE_PMID(kWidgetIDSpace, kTPLTreeImageIconSuiteWidgetID,   kTPLPrefix + 41) 
DECLARE_PMID(kWidgetIDSpace, kTPLTreeTableIconSuiteWidgetID,   kTPLPrefix + 42) 
DECLARE_PMID(kWidgetIDSpace, kAutoResizeWidgetID,			   kTPLPrefix + 43) 
DECLARE_PMID(kWidgetIDSpace, kTPLTreeCopyParaIconSuiteWidgetID,kTPLPrefix + 44) 
DECLARE_PMID(kWidgetIDSpace, kTPLTreeParaImageIconSuiteWidgetID,kTPLPrefix + 45) 
DECLARE_PMID(kWidgetIDSpace, kTPLTreePVBoxIconSuiteWidgetID,	kTPLPrefix + 46) 

//added by Tushar on 11/12/06
DECLARE_PMID(kWidgetIDSpace,	kOverFlowWidgetID,						kTPLPrefix + 47)
 //added on 13/12/06 by Tushar
DECLARE_PMID(kWidgetIDSpace,	kTPLTreeDollorIconSuiteWidgetID,		kTPLPrefix + 48)
DECLARE_PMID(kWidgetIDSpace,	kTPLProductGroupNameIconSuiteWidgetID,	kTPLPrefix + 49)

DECLARE_PMID(kWidgetIDSpace,	kTableGroupPanelWidgetID,				kTPLPrefix + 50)
DECLARE_PMID(kWidgetIDSpace,	kOutputTableNameWidgetID,				kTPLPrefix + 51)
DECLARE_PMID(kWidgetIDSpace,	kOutputAsTabbedTextWidgetID,			kTPLPrefix + 52)
DECLARE_PMID(kWidgetIDSpace,	kOutputAsTableWidgetID,					kTPLPrefix + 53)
DECLARE_PMID(kWidgetIDSpace,	kImageGroupPanelWidgetID,				kTPLPrefix + 54)
DECLARE_PMID(kWidgetIDSpace,	kOutputAsImageDescriptionWidgetID,		kTPLPrefix + 55)
DECLARE_PMID(kWidgetIDSpace,	kOutputAsFieldNameCheckBoxWidgetID,		kTPLPrefix + 56)
DECLARE_PMID(kWidgetIDSpace,	kAttrGroupPanelWidgetID,				kTPLPrefix + 57)
DECLARE_PMID(kWidgetIDSpace,	kOutputAsItemListWidgetID,				kTPLPrefix + 58)	
DECLARE_PMID(kWidgetIDSpace,	kOutputAsIncludeHeaderWidgetID,			kTPLPrefix + 59)
DECLARE_PMID(kWidgetIDSpace,	kOutputAsAttributeNameWidgetID,			kTPLPrefix + 60)
//DECLARE_PMID(kWidgetIDSpace,	kSeparatorWidgetID,						kTPLPrefix + 61)
DECLARE_PMID(kWidgetIDSpace,	kFirstGroupPanelClusterPanelWidgetID,	kTPLPrefix + 62)
DECLARE_PMID(kWidgetIDSpace,	kOutputIncludeTableHeaderWidgetID,		kTPLPrefix + 63)
DECLARE_PMID(kWidgetIDSpace,	kDelUnSprayedFieldsWidgetID,			kTPLPrefix + 64)
DECLARE_PMID(kWidgetIDSpace,	kItemListTextWidgetID,					kTPLPrefix + 65)
//DECLARE_PMID(kWidgetIDSpace,	kHorizontalFlowCheckBoxWidgetID,		kTPLPrefix + 66)
DECLARE_PMID(kWidgetIDSpace,	kTPLTreeListIconSuiteWidgetID,			kTPLPrefix + 67)
DECLARE_PMID(kWidgetIDSpace,	kSpreadBasedLetterKeysWidgetID,			kTPLPrefix + 68)
DECLARE_PMID(kWidgetIDSpace,	kHorizontalFlowRadioButtonWidgetID,		kTPLPrefix + 69)
DECLARE_PMID(kWidgetIDSpace,	kVerticalFlowRadioButtonWidgetID,		kTPLPrefix + 70)
DECLARE_PMID(kWidgetIDSpace,	kImageClusterPanelWidgetID,				kTPLPrefix + 71)
DECLARE_PMID(kWidgetIDSpace,	kSprayItemPerFrameWidgetID,				kTPLPrefix + 72)
DECLARE_PMID(kWidgetIDSpace,	kHorizontalFlowWidgetID,				kTPLPrefix + 73)
DECLARE_PMID(kWidgetIDSpace,	kTPLTableListDropDownWidgetID,			kTPLPrefix + 74)


// "About Plug-ins" sub-menu:
#define kTPLAboutMenuKey		kTPLStringPrefix "kTPLAboutMenuKey"
#define kTPLAboutMenuPath		kSDKDefStandardAboutMenuPath kTPLCompanyKey

// "Plug-ins" sub-menu:
#define kTPLPluginsMenuKey 		kTPLStringPrefix "kTPLPluginsMenuKey"
#define kTPLPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kTPLCompanyKey kSDKDefDelimitMenuPath kTPLPluginsMenuKey
#define kTPLPluginInternalPopUP	kTPLInternalPopupMenuNameKey

// Menu item keys:
#define kTPLItemOneMenuItemKey	 			""

// Other StringKeys:
#define kTPLAboutBoxStringKey				kTPLStringPrefix "kTPLAboutBoxStringKey"
#define kTPLItemOneStringKey				kTPLStringPrefix	"kTPLItemOneStringKey"
#define kTPLPanelTitleKey					kTPLStringPrefix	"kTPLPanelTitleKey"
#define kTPLStaticTextKey					kTPLStringPrefix	"kTPLStaticTextKey"
#define kTPLInternalPopupMenuNameKey		kTPLStringPrefix	"kTPLInternalPopupMenuNameKey"
#define kTPLTargetMenuPath kTPLInternalPopupMenuNameKey

#define kTPLShowCategoriesStringKey		kTPLStringPrefix	"kTPLShowCategoriesStringKey"
#define kTPLAddAsAccessoryAttrStringKey	kTPLStringPrefix	"kTPLAddAsAccessoryAttrStringKey"
#define kTPLAddAsComponentAttrStringKey	kTPLStringPrefix	"kTPLAddAsComponentAttrStringKey"
#define kTPLAddAsCrossRefAttrStringKey	kTPLStringPrefix	"kTPLAddAsCrossRefAttrStringKey"
#define kTPLAddAsMMYAttrStringKey		kTPLStringPrefix	"kTPLAddAsMMYAttrStringKey"
#define kTPLAddAsMMYSortAttrStringKey	kTPLStringPrefix	"kTPLAddAsMMYSortAttrStringKey"
#define kTPLTagInfoStringKey			kTPLStringPrefix	"kTPLTagInfoStringKey"
#define kTPLAddAsComponentAttrStringKey	kTPLStringPrefix	"kTPLAddAsComponentAttrStringKey"
#define kTPLAttributesStringKey			kTPLStringPrefix	"kTPLAttributesStringKey"

#define kSelectStringKey				kTPLStringPrefix	"kSelectStringKey"
#define kAssignCurrentFrameStringKey	kTPLStringPrefix	"kAssignCurrentFrameStringKey"
#define kCreateNewFrameStringKey		kTPLStringPrefix	"kCreateNewFrameStringKey"
#define kFrameTagStringKey				kTPLStringPrefix	"kFrameTagStringKey"
#define kAutoResizeStringKey			kTPLStringPrefix	"kAutoResizeStringKey"
#define kOverflowStringKey				kTPLStringPrefix	"kOverflowStringKey"
#define kOutputasStringKey				kTPLStringPrefix	"kOutputasStringKey"
#define kSpreadBasedStringKey			kTPLStringPrefix	"kSpreadBasedStringKey"
#define kIncludeHeaderStringKey			kTPLStringPrefix	"kIncludeHeaderStringKey"
#define kDisplayNameStringKey			kTPLStringPrefix	"kDisplayNameStringKey"
#define kDeleteIfEmptyStringKey			kTPLStringPrefix	"kDeleteIfEmptyStringKey"
#define kSprayItemPerFrameStringKey		kTPLStringPrefix	"kSprayItemPerFrameStringKey"
#define kHorizontalFlowStringKey		kTPLStringPrefix	"kHorizontalFlowStringKey"
#define kTabbedTextStringKey			kTPLStringPrefix	"kTabbedTextStringKey"
#define kTableStringKey					kTPLStringPrefix	"kTableStringKey"
#define kTableNameStringKey				kTPLStringPrefix	"kTableNameStringKey"
#define kItemListStringKey				kTPLStringPrefix	"kItemListStringKey"
#define kImageDescriptionStringKey		kTPLStringPrefix	"kImageDescriptionStringKey"
#define kHorizontalFlowStringKey1		kTPLStringPrefix	"kHorizontalFlowStringKey1"
#define kVerticalFlowStringKey			kTPLStringPrefix	"kVerticalFlowStringKey"
#define kReloadListStringKey			kTPLStringPrefix	"kReloadListStringKey"
#define kPrintsourcetStringKey			kTPLStringPrefix	"kPrintsourcetStringKey"
#define kTPLFrameOptionsStringKey		kTPLStringPrefix	"kTPLFrameOptionsStringKey"
#define kMainStringKey					kTPLStringPrefix	"kMainStringKey"


// Menu item positions:

#define kTPLItemOneMenuItemPosition			1.0
#define	kTPLSeparator1MenuItemPosition		10.0
#define kTPLAboutThisMenuItemPosition		11.0


//Others
#define kTPLPanelOrderingResourceID			700
#define kTPLPFPanelResourceID				720
#define kTPLPFPanelCreatorResourceID		730

#define kTPLPFPanelLstBoxRsrcID				800
#define kTPLPGPanelLstBoxRsrcID				810
#define kTPLPRPanelLstBoxRsrcID				820
#define kTPLItemPanelLstBoxRsrcID			830
#define kTPLProjectPanelLstBoxRsrcID		840
#define kTPLPFPanelTitleKey					"Product Family"
#define kTPLPFStrKey						"Product Family"
#define kTPLPGStrKey						"Product Group"
#define kTPLPRStrKey						"Item Group"
#define kTPLProjectStrkey					"Event"
#define kTPLItemStrKey						"Item"

#define kTPLChkIconID						10001
#define kTPLUnChkIconID						10002
#define kTPLClassTreeIconID					10003
#define kTPLRefreshIconID					10004

#define kTPLCopyIcon						10005
#define kTPLImageIcon						10006
#define kTPLTableIcon						10007
#define kTPLCopyParaIcon					10008
#define kTPLParaImageIcon					10009	
#define kTPLPVBoxIcon						10010
#define kTPLDollorIcon						10011   //added on 13/12/06 by Tushar
#define kTPLProductGroupNameIcon			10012   //added on 10/01/07 by Dattatray
#define kTPLListIcon						10013	//15-10-08	Amit

#define ourFlavor1 PMFlavor('APS5')

#define kTPLTreePanelNodeRsrcID 3200

// Initial data format version numbers
#define kTPLFirstMajorFormatNumber  RezLong(1)
#define kTPLFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kTPLCurrentMajorFormatNumber kTPLFirstMajorFormatNumber
#define kTPLCurrentMinorFormatNumber kTPLFirstMinorFormatNumber

#endif // __TPLID_h__

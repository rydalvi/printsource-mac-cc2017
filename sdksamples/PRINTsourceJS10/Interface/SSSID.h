//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __SSSID_h__
#define __SSSID_h__

#include "SDKDef.h"

// Company:
#define kSSSCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kSSSCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kSSSPluginName	"Section Spray Settings"//"APJS9_SubSectionSprayer"			// Name of this plug-in.
#define kSSSPrefixNumber	0xDB1B00 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kSSSVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kSSSAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kSSSPrefixNumber above to modify the prefix.)
#define kSSSPrefix		RezLong(kSSSPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kSSSStringPrefix	SDK_DEF_STRINGIZE(kSSSPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kSSSMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kSSSMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kSSSPluginID, kSSSPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kSSSActionComponentBoss, kSSSPrefix + 0)
DECLARE_PMID(kClassIDSpace, kSSSStringRegisterBoss, kSSSPrefix + 1)
DECLARE_PMID(kClassIDSpace, kSSSMenuRegisterBoss, kSSSPrefix + 2)
DECLARE_PMID(kClassIDSpace, kSSSActionRegisterBoss, kSSSPrefix + 3)
DECLARE_PMID(kClassIDSpace, kSSSDialogBoss, kSSSPrefix + 4)
DECLARE_PMID(kClassIDSpace, kSubSectionSprayerBoss, kSSSPrefix + 5)
DECLARE_PMID(kClassIDSpace, kSSSListBoxWidgetBoss,	kSSSPrefix + 8)
DECLARE_PMID(kClassIDSpace, kSSSTextWidgetBoss,		kSSSPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kSSSBoss, kSSSPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_ISUBSECTIONSPRAYER, kSSSPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_SSSITEXTMISCELLANYSUITEE/*IID_ITEXTMISCELLANYSUITEE*/,	kSSSPrefix + 1)//
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_ISSSINTERFACE, kSSSPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kSSSActionComponentImpl, kSSSPrefix + 0)
DECLARE_PMID(kImplementationIDSpace, kSSSDialogControllerImpl, kSSSPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kSSSDialogObserverImpl, kSSSPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kSubSectionSprayerImpl, kSSSPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kSSSTextMiscellanySuiteASBImpl/*kTextMiscellanySuiteASBImpl*/,kSSSPrefix + 4)//added by dattatray
DECLARE_PMID(kImplementationIDSpace, kSSSTextMiscellanySuiteLayoutCSBImpl/*kTextMiscellanySuiteLayoutCSBImpl*/,kSSSPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 3)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 4)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kSSSImpl, kSSSPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kSSSProductActionID, kSSSPrefix + 1)
DECLARE_PMID(kActionIDSpace, kSSSItemActionID, kSSSPrefix + 2)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kSSSActionID, kSSSPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kSSSDialogWidgetID, kSSSPrefix + 0)
//DECLARE_PMID(kWidgetIDSpace, kOptionsDropDownWidgetID, kSSSPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kMarginsPanelWidgetID, kSSSPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kFlowPanelWidgetID, kSSSPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kRepeatingDataPanelWidgetID, kSSSPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kMarginsSpacingPanelWidgetID, kSSSPrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kTopMarginWidgetID, kSSSPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kBottomMarginWidgetID, kSSSPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kLeftMarginWidgetID, kSSSPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kRightMarginWidgetID, kSSSPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kHorizontalSpacingWidgetID, kSSSPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kVerticalSpacingWidgetID, kSSSPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kLeftToRightWidgetID, kSSSPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kRightToLeftWidgetID, kSSSPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kAlternateValueWidgetID, kSSSPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kTopToBottomWidgetID, kSSSPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kBottomToTopWidgetID, kSSSPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kHorizontalFlowWidgetID, kSSSPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kVerticalFlowWidgetID, kSSSPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kIsAllSectionSprayWidgetID, kSSSPrefix + 19)
DECLARE_PMID(kWidgetIDSpace, kSSSListParentWidgetID,	 kSSSPrefix + 22)
DECLARE_PMID(kWidgetIDSpace, kSSSListBoxWidgetID, kSSSPrefix + 23)
DECLARE_PMID(kWidgetIDSpace,kSSSTextWidgetID, kSSSPrefix + 24)
DECLARE_PMID(kWidgetIDSpace, kSSSListBoxTwoWidgetID, kSSSPrefix + 25)
DECLARE_PMID(kWidgetIDSpace, kProductButtonWidgetID, kSSSPrefix + 26)
DECLARE_PMID(kWidgetIDSpace, kItemButtonWidgetID, kSSSPrefix + 27)

DECLARE_PMID(kWidgetIDSpace, kSSSAltVerticalFLowRadioButtonWidgetID,	kSSSPrefix + 28)
DECLARE_PMID(kWidgetIDSpace, kSSSVerticalFlowRadioButtonWidgetID,		kSSSPrefix + 29)
DECLARE_PMID(kWidgetIDSpace, kSSSAltHorzintalFlowRadioButtonWidgetID,	kSSSPrefix + 30)
DECLARE_PMID(kWidgetIDSpace, kSSSHorzintalFlowRadioButtonWidgetID,		kSSSPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kHybridButtonWidgetID, kSSSPrefix + 32)
DECLARE_PMID(kWidgetIDSpace, kSSSListBoxThreeWidgetID, kSSSPrefix + 33)
DECLARE_PMID(kWidgetIDSpace, kIsSprayItemPerFrameWidgetID, kSSSPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kIsHorizontalFlowForAllImageSprayWidgetID, kSSSPrefix + 35)
DECLARE_PMID(kWidgetIDSpace, kSSSListBoxFourWidgetID, kSSSPrefix + 36)
DECLARE_PMID(kWidgetIDSpace, kSectionButtonWidgetID, kSSSPrefix + 37)
DECLARE_PMID(kWidgetIDSpace, kIsWithoutPageBreakWidgetID, kSSSPrefix + 38)
DECLARE_PMID(kWidgetIDSpace, kIsAddSectionStencilWidgetID, kSSSPrefix + 39)
DECLARE_PMID(kWidgetIDSpace, kIsAtStartOfSectionWidgetID, kSSSPrefix + 40)
DECLARE_PMID(kWidgetIDSpace, kIsAtStartOfEachPageWidgetID, kSSSPrefix + 41)
DECLARE_PMID(kWidgetIDSpace, kIsAtStartOfFirstPageWidgetID, kSSSPrefix + 42)
DECLARE_PMID(kWidgetIDSpace, kItemGroupItemWidgetID, kSSSPrefix + 43)
DECLARE_PMID(kWidgetIDSpace, kSprayItemWidgetID, kSSSPrefix + 44)
DECLARE_PMID(kWidgetIDSpace, kSectionSprayOKButtonWidgetID, kSSSPrefix + 45)
DECLARE_PMID(kWidgetIDSpace, kSectionSprayCancelButtonWidgetID, kSSSPrefix + 46)
DECLARE_PMID(kWidgetIDSpace, kFirstGroupPanelWidgetID1, kSSSPrefix + 47)
DECLARE_PMID(kWidgetIDSpace, kSecondGroupPanelWidgetID1, kSSSPrefix + 48)
DECLARE_PMID(kWidgetIDSpace, kThirdGroupPanelWidgetID, kSSSPrefix + 49)
DECLARE_PMID(kWidgetIDSpace, kFourthGroupPanelWidgetID, kSSSPrefix + 50)
DECLARE_PMID(kWidgetIDSpace, kIsAllSectionSpraySecondLineWidgetID, kSSSPrefix + 51)


// "About Plug-ins" sub-menu:
#define kSSSAboutMenuKey			kSSSStringPrefix "kSSSAboutMenuKey"
#define kSSSAboutMenuPath		kSDKDefStandardAboutMenuPath kSSSCompanyKey

// "Plug-ins" sub-menu:
#define kSSSPluginsMenuKey 		kSSSStringPrefix "kSSSPluginsMenuKey"
#define kSSSPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kSSSCompanyKey kSDKDefDelimitMenuPath kSSSPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kSSSAboutBoxStringKey	kSSSStringPrefix "kSSSAboutBoxStringKey"
#define kSSSTargetMenuPath kSSSPluginsMenuPath

//added by avinash
#define kSSSHorizontalStringKey	kSSSStringPrefix	"kSSSHorizontalStringKey"
#define kSSSVerticalStringKey	kSSSStringPrefix	"kSSSVerticalStringKey"
#define kSSSWithoutPageBreakStringKey	kSSSStringPrefix	"kSSSWithoutPageBreakStringKey"
#define kSSSEventStringKey	kSSSStringPrefix	"kSSSEventStringKey"
#define kPageStringKey	kSSSStringPrefix	"kPageStringKey"

#define kAddSectionStencilStringKey	kSSSStringPrefix	"kAddSectionStencilStringKey"
#define kAtStartOfSectionStringKey	kSSSStringPrefix	"kAtStartOfSectionStringKey"
#define kAtStartOfEachPageStringKey	kSSSStringPrefix	"kAtStartOfEachPageStringKey"
#define kAtStartOfFirstPageOfSpreadStringKey	kSSSStringPrefix	"kAtStartOfFirstPageOfSpreadStringKey"
#define kHorizontalFlowForAllImageSpray	kSSSStringPrefix	"kHorizontalFlowForAllImageSpray"
#define kBkanStringKey	kSSSStringPrefix	"kBkanStringKey"
#define kSelectStencilStringKey	kSSSStringPrefix		"kSelectStencilStringKey"
#define kRightStringKey 	kSSSStringPrefix	     "kRightStringKey"
#define kLeftStringKey		kSSSStringPrefix		 "kLeftStringKey"			
#define kBottomStringKey	kSSSStringPrefix		 "kBottomStringKey"		
#define kTopStringKey		kSSSStringPrefix		 "kTopStringKey"			
#define kMarginOffsetStringKey kSSSStringPrefix	 "kMarginOffsetStringKey" 

// till here

// Menu item positions:

#define kSSSDialogTitleKey kSSSStringPrefix "kSSSDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kSSSDialogMenuItemKey kSSSStringPrefix "kSSSDialogMenuItemKey"
// "Plug-ins" sub-menu item position for dialog:
#define kSSSDialogMenuItemPosition	12.0
#define kSSSListElementRsrcID 1000.0

#define kSSSVerticalWidget1IconID		1001
#define kSSSVerticalWidget1IconRsrcID	1001	//--CS5--
#define kSSSVerticalWidgetIconID		1002
#define kSSSVerticalWidgetIconRsrcID	1002
#define kSSSHorizontalWidget1IconID		1003
#define kSSSHorizontalWidget1IconRsrcID 1003
#define kSSSHorizontalWidgetIconID		1004
#define kSSSHorizontalWidgetIconRsrcID  1004

// Initial data format version numbers
#define kSSSFirstMajorFormatNumber  RezLong(1)
#define kSSSFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kSSSCurrentMajorFormatNumber kSSSFirstMajorFormatNumber
#define kSSSCurrentMinorFormatNumber kSSSFirstMinorFormatNumber


//PNGR ID
#define kPNGProductIconRsrcID                      4000
#define kPNGProductIconRollRsrcID                  4000

#define kPNGItemIconRsrcID                         4001
#define kPNGItemIconRollRsrcID                     4001

#define kPNGTableIconRsrcID                        4002
#define kPNGTableIconRollRsrcID                    4002

#define kPNGSectionIconRsrcID                      4003
#define kPNGSectionIconRollRsrcID                  4003

#define kPNGSectionSprayOKIconRsrcID               4004
#define kPNGSectionSprayOKIconRollRsrcID           4004

#define kPNGSectionSprayCancelIconRsrcID           4005
#define kPNGSectionSprayCancelIconRollRsrcID       4005

#endif // __SSSID_h__

//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __OptnsDlgID_h__
#define __OptnsDlgID_h__

#include "SDKDef.h"

// Company:
#define kOptnsDlgCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kOptnsDlgCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kOptnsDlgPluginName	"Settings"			// Name of this plug-in.
#define kOptnsDlgPrefixNumber	0xDBA890 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kOptnsDlgVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kOptnsDlgAuthor		"Apsiva Inc."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kOptnsDlgPrefixNumber above to modify the prefix.)
#define kOptnsDlgPrefix		RezLong(kOptnsDlgPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kOptnsDlgStringPrefix	SDK_DEF_STRINGIZE(kOptnsDlgPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kOptnsDlgMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kOptnsDlgMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kOptnsDlgPluginID, kOptnsDlgPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kOptnsDlgActionComponentBoss, kOptnsDlgPrefix + 0)
DECLARE_PMID(kClassIDSpace, kOptnsDlgStringRegisterBoss,	kOptnsDlgPrefix + 11)
DECLARE_PMID(kClassIDSpace, kOptnsDlgMenuRegisterBoss,		kOptnsDlgPrefix + 12)
DECLARE_PMID(kClassIDSpace, kOptnsDlgActionRegisterBoss,	kOptnsDlgPrefix + 13)
DECLARE_PMID(kClassIDSpace, kOptnsDlgDialogBoss,			kOptnsDlgPrefix + 14)
DECLARE_PMID(kClassIDSpace, kOptnsDlgTabDialogBoss,			kOptnsDlgPrefix + 15)

DECLARE_PMID(kClassIDSpace, kGeneralPanelBoss,				kOptnsDlgPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kOptnsOKButtonWidgetBoss,		kOptnsDlgPrefix + 17)
DECLARE_PMID(kClassIDSpace, kTasksPanelBoss,				kOptnsDlgPrefix + 18)
DECLARE_PMID(kClassIDSpace, kClientOptionsReaderBoss,		kOptnsDlgPrefix + 19)
DECLARE_PMID(kClassIDSpace, kPubBrowseDialogBoss,			kOptnsDlgPrefix + 20)
DECLARE_PMID(kClassIDSpace, kPathBrowse1DialogBoss,			kOptnsDlgPrefix + 21)

// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_ICLIENTOPTIONS,		kOptnsDlgPrefix + 0)

// Implementation IDs:
DECLARE_PMID(kImplementationIDSpace, kOptnsDlgActionComponentImpl, kOptnsDlgPrefix + 0)
DECLARE_PMID(kImplementationIDSpace, kOptnsDlgDialogControllerImpl,kOptnsDlgPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kOptnsDlgDialogObserverImpl,  kOptnsDlgPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kOptnsDlgDialogCreatorImpl,   kOptnsDlgPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kOptnsDlgTabDialogCreatorImpl,kOptnsDlgPrefix + 4)

DECLARE_PMID(kImplementationIDSpace, kGeneralPanelCreatorImpl,		kOptnsDlgPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kGeneralPanelObserverImpl,		kOptnsDlgPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kTasksPanelCreatorImpl,		kOptnsDlgPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kOptnsTabOKBtnObserverImpl,	kOptnsDlgPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kClientOptionsImpl,			kOptnsDlgPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kPublicationDialogCreatorImpl,		kOptnsDlgPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kPublicationDialogControllerImpl,	kOptnsDlgPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kPublicationDialogObserverImpl,	kOptnsDlgPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kPathBrowseDlgCreatorImpl,		kOptnsDlgPrefix + 13)
DECLARE_PMID(kImplementationIDSpace, kPathBrowseDlgControllerImpl,	kOptnsDlgPrefix + 14)
DECLARE_PMID(kImplementationIDSpace, kPathBrowseDlgObserverImpl,	kOptnsDlgPrefix + 15)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kOptnsDlgAboutActionID, kOptnsDlgPrefix + 0)
DECLARE_PMID(kActionIDSpace, kOptnsDlgTabDialogActionID,kOptnsDlgPrefix + 1)

// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kOptnsDlgDialogWidgetID, kOptnsDlgPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kOptnsDlgTabDialogWidgetID, kOptnsDlgPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kApplyButtonWidgetID, kOptnsDlgPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kOptnsOKButtonWidgetID, kOptnsDlgPrefix + 3)

DECLARE_PMID(kWidgetIDSpace, kGeneralPanelWidgetID, kOptnsDlgPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kGenPanelGroupPanelWidgetID, kOptnsDlgPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kGenAssetsGroupPanelWidgetID, kOptnsDlgPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kPublicationStaticWidgetID, kOptnsDlgPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kGenSysDataGroupPanelWidgetID, kOptnsDlgPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kImagePathStaticTextWidgetID, kOptnsDlgPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kGenDispForamtGroupPanelWidgetID, kOptnsDlgPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kDocpathStaticTextWidgetID, kOptnsDlgPrefix + 11)

DECLARE_PMID(kWidgetIDSpace, kPunNameButtonWidgetID, kOptnsDlgPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kImagePathButtonWidgetID, kOptnsDlgPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kDocPathButtonWidgetID, kOptnsDlgPrefix + 15)

DECLARE_PMID(kWidgetIDSpace, kGenPanelGeneralStaticTextWidgetID, kOptnsDlgPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kGenPanelAssetsStaticTextWidgetID, kOptnsDlgPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, kGenPanelSysdataStaticTextWidgetID, kOptnsDlgPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kGenPanelDispFormatStaticTextWidgetID, kOptnsDlgPrefix + 19)

DECLARE_PMID(kWidgetIDSpace, kTasksPanelWidgetID,			kOptnsDlgPrefix + 20)
DECLARE_PMID(kWidgetIDSpace, kTaskPanelGroupPanelWidgetID, kOptnsDlgPrefix + 21)
DECLARE_PMID(kWidgetIDSpace, kTaskManagerGroupPanelWidgetID, kOptnsDlgPrefix + 22)
DECLARE_PMID(kWidgetIDSpace, kTaskLauchOnStartUpChkBoxWidgetID, kOptnsDlgPrefix + 23)
DECLARE_PMID(kWidgetIDSpace, kTasksTskManagerStaticTextWidgetID, kOptnsDlgPrefix + 24)

DECLARE_PMID(kWidgetIDSpace, kPubBrowseDialogWidgetID,	kOptnsDlgPrefix + 25)
DECLARE_PMID(kWidgetIDSpace, kPubDlgGroupPanelWidgetID, kOptnsDlgPrefix + 26)
DECLARE_PMID(kWidgetIDSpace, kPubDlgStaticTextWidgetID, kOptnsDlgPrefix + 27)
DECLARE_PMID(kWidgetIDSpace, kPubDropDownWidgetID,		kOptnsDlgPrefix + 28)


DECLARE_PMID(kWidgetIDSpace, PathBrowse1DialogWidget,	kOptnsDlgPrefix + 30)
DECLARE_PMID(kWidgetIDSpace, kPathDlgGroupPanelWidgetID,kOptnsDlgPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kPathDlgStaticTextWidgetID,kOptnsDlgPrefix + 32)
DECLARE_PMID(kWidgetIDSpace, kBrowsePathButtonWidgetID, kOptnsDlgPrefix + 33)

DECLARE_PMID(kWidgetIDSpace, kLocaleButtonWidgetID, kOptnsDlgPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kLocaleStaticWidgetID, kOptnsDlgPrefix + 35)
DECLARE_PMID(kWidgetIDSpace, kLocaleDropDownWidgetID, kOptnsDlgPrefix + 36)
//DECLARE_PMID(kWidgetIDSpace, kCancelButtonWidgetID, kOptnsDlgPrefix + 38)  // for cancle button  testing only 
DECLARE_PMID(kWidgetIDSpace, kSameAsAssetWidgetID, kOptnsDlgPrefix + 39) 
DECLARE_PMID(kWidgetIDSpace, kLogLevelStaticTextWidgetID, kOptnsDlgPrefix + 40) 
DECLARE_PMID(kWidgetIDSpace, kLogLevelDropDownWidgetID, kOptnsDlgPrefix + 41) 
DECLARE_PMID(kWidgetIDSpace, kMissingFlagWidgetID, kOptnsDlgPrefix + 42)
DECLARE_PMID(kWidgetIDSpace, kCatalogDataGroupPanelWidgetID,		kOptnsDlgPrefix + 43)
DECLARE_PMID(kWidgetIDSpace, kGenPanelCatalogdataStaticTextWidgetID, kOptnsDlgPrefix + 44)
DECLARE_PMID(kWidgetIDSpace, kCatalogPathStaticTextWidgetID, kOptnsDlgPrefix + 45)

//added by nitin from here to///
//kOptnsImagesGroupPanelWidgetID
DECLARE_PMID(kWidgetIDSpace, kOptnsImagesGroupPanelWidgetID, kOptnsDlgPrefix + 46)
DECLARE_PMID(kWidgetIDSpace, kOptnsClusterPanelWidgetID, kOptnsDlgPrefix + 47)
DECLARE_PMID(kWidgetIDSpace, kOptnsMaptoRepositryWidgetID, kOptnsDlgPrefix + 48)
DECLARE_PMID(kWidgetIDSpace, kOptnsDownloadImagesToWidgetID, kOptnsDlgPrefix + 49)

DECLARE_PMID(kWidgetIDSpace, kClientEnvironmentPanelWidgetID, kOptnsDlgPrefix + 51)
DECLARE_PMID(kWidgetIDSpace, kOptnsClientEnvNamePanelStaticTextWidgetID, kOptnsDlgPrefix + 52)
DECLARE_PMID(kWidgetIDSpace, kOptnsClientEnvNameStaticTextWidgetID, kOptnsDlgPrefix + 53)
DECLARE_PMID(kWidgetIDSpace, kOptnsClientServerUrlStaticTextWidgetID, kOptnsDlgPrefix + 54)
DECLARE_PMID(kWidgetIDSpace, kOptnsDownloadImagesToStaticTextWidgetID, kOptnsDlgPrefix + 55)
DECLARE_PMID(kWidgetIDSpace, kOptnsMaptoRepositryStaticTextWidgetID, kOptnsDlgPrefix + 56)
DECLARE_PMID(kWidgetIDSpace, kDownloadImagePathButtonWidgetID, kOptnsDlgPrefix + 57)
DECLARE_PMID(kWidgetIDSpace, kDownloadImagePathStaticTextWidgetID, kOptnsDlgPrefix + 58)
DECLARE_PMID(kWidgetIDSpace, kselectMaptoRepositryWidgetID, kOptnsDlgPrefix + 59)//ch
DECLARE_PMID(kWidgetIDSpace, kselectDownloadImagesToWidgetID, kOptnsDlgPrefix + 60)//ch
DECLARE_PMID(kWidgetIDSpace, kselectMaptoRepositryStaticTextWidgetID, kOptnsDlgPrefix + 61)//ch
DECLARE_PMID(kWidgetIDSpace, kselectimagePathStaticTextWidgetID, kOptnsDlgPrefix + 62)//ch
DECLARE_PMID(kWidgetIDSpace, kselectimagePathButtonWidgetID, kOptnsDlgPrefix + 63)//ch
DECLARE_PMID(kWidgetIDSpace, kselectDownloadImagesToStaticTextWidgetID, kOptnsDlgPrefix + 64)//ch
DECLARE_PMID(kWidgetIDSpace, kselectDownloadImagePathButtonWidgetID, kOptnsDlgPrefix + 65)//ch
DECLARE_PMID(kWidgetIDSpace, kselectDownloadImagePathStaticTextWidgetID, kOptnsDlgPrefix + 66)//ch
DECLARE_PMID(kWidgetIDSpace, kselectPathDlgGroupPanelWidgetID, kOptnsDlgPrefix + 67)//ch
DECLARE_PMID(kWidgetIDSpace, kselectMissingFlagWidgetID, kOptnsDlgPrefix + 68)//ch
DECLARE_PMID(kWidgetIDSpace, kOptsCancelButtonWidgetID, kOptnsDlgPrefix + 69)//ch
DECLARE_PMID(kWidgetIDSpace, kUploadButtonWidgetID, kOptnsDlgPrefix + 70)
DECLARE_PMID(kWidgetIDSpace, kResetButtonWidgetID, kOptnsDlgPrefix + 71)
DECLARE_PMID(kWidgetIDSpace, kBrwsImgPathCancelWidgetID, kOptnsDlgPrefix + 72)
DECLARE_PMID(kWidgetIDSpace, kBrwsImgPathOKBtnWidgetID, kOptnsDlgPrefix + 73)
DECLARE_PMID(kWidgetIDSpace, kShowObjectCountWidgetID, kOptnsDlgPrefix + 74)

// added by avinash
DECLARE_PMID(kWidgetIDSpace, kOptnsSettingsSavedStaticTextWidgetID, kOptnsDlgPrefix + 75)
DECLARE_PMID(kWidgetIDSpace, kByPassPromptWidgetID, kOptnsDlgPrefix + 76)
DECLARE_PMID(kWidgetIDSpace, kByPassPromptStaticTextWidgetID, kOptnsDlgPrefix + 79)
DECLARE_PMID(kWidgetIDSpace, kOptVerticalSpacingWidgetID, kOptnsDlgPrefix + 77)
DECLARE_PMID(kWidgetIDSpace, kVerticalSpacingStaticTextWidgetID, kOptnsDlgPrefix + 80)
DECLARE_PMID(kWidgetIDSpace, kOptHorizontalSpacingWidgetID, kOptnsDlgPrefix + 78)




// Service IDs
DECLARE_PMID(kServiceIDSpace, kOptnsDlgService, kOptnsDlgPrefix + 0)

// "About Plug-ins" sub-menu:
#define kOptnsDlgAboutMenuKey		kOptnsDlgStringPrefix "kSelDlgAboutMenuKey"
#define kOptnsDlgAboutMenuPath		kSDKDefStandardAboutMenuPath kOptnsDlgCompanyKey

// "Plug-ins" sub-menu:
#define kOptnsDlgOptionsMenuKey		kOptnsDlgStringPrefix "kOptnsDlgOptionsMenuKey"
#define kOptnsDlgOptionsMenuPath	kOptnsDlgMainMenuKey

// "Plug-ins" sub-menu item keys:
#define kOptnsDlgMainMenuKey		kOptnsDlgStringPrefix "kOptnsDlgMainMenuKey"

// "Plug-ins" sub-menu item positions:
#define kOptnsDlgTabDialogMenuItemPosition	8.0

// Other StringKeys:
#define kDefPublicationKey				"kDefPublicationKey"
#define kImageDownLoadPathKey			"kImageDownLoadPathKey"
#define kIndesignDocPathKey				"kIndesignDocPathKey"
//added by avinash
#define kSCTImageRepositryStringKey	kOptnsDlgStringPrefix "kSCTImageRepositryStringKey"
#define kSCTMapRepositryStringKey kOptnsDlgStringPrefix	"kSCTMapRepositryStringKey"
#define kSCTDownloadImagesToStringKey kOptnsDlgStringPrefix	"kSCTDownloadImagesToStringKey"
#define kSCTDotDotDotStringKey	kOptnsDlgStringPrefix	"kSCTDotDotDotStringKey"
#define kSCTUSeMissingFLagStringKey kOptnsDlgStringPrefix	"kSCTUSeMissingFLagStringKey"
#define kOptnsDlgPrintSourceKey	kOptnsDlgStringPrefix	"kOptnsDlgPrintSourceKey"
#define kApsivaHostInformationStringKey		kOptnsDlgStringPrefix	"kApsivaHostInformationStringKey"
#define kCurrentEventAndLanguageStringKey	kOptnsDlgStringPrefix	"kCurrentEventAndLanguageStringKey"
#define kImagesStringKey	kOptnsDlgStringPrefix	"kImagesStringKey"
#define kWhiteBoardAndDemanCatalogsStringKey	kOptnsDlgStringPrefix "kWhiteBoardAndDemanCatalogsStringKey"
#define kUploadmasterPagesAndStencilsStringKey	kOptnsDlgStringPrefix "kUploadmasterPagesAndStencilsStringKey"
#define kOutputMediaToStringKey		kOptnsDlgStringPrefix "kOutputMediaToStringKey"
#define kShowObjectCountStringKey	kOptnsDlgStringPrefix "kShowObjectCountStringKey"
#define kResetStringKey		kOptnsDlgStringPrefix "kResetStringKey"
#define kResetAllPlnsStringKey	kOptnsDlgStringPrefix "kResetAllPlnsStringKey"
#define kSingleSelectionSprayDefaults kOptnsDlgStringPrefix "kSingleSelectionSprayDefaults"
#define kVerticalSpacingPointsStringKey		kOptnsDlgStringPrefix "kVerticalSpacingPointsStringKey"
#define kHorizontalSpacingPointsStringKey	kOptnsDlgStringPrefix "kHorizontalSpacingPointsStringKey"
#define kBypassPromtStringKey	kOptnsDlgStringPrefix	"kBypassPromtStringKey"
#define	kLogLevelStringKey	kOptnsDlgStringPrefix "kLogLevelStringKey"
#define kErrorStringKey		kOptnsDlgStringPrefix	"kErrorStringKey"
#define	kFatalStringKey		kOptnsDlgStringPrefix	"kFatalStringKey"
#define kDebugInfoStringKey	kOptnsDlgStringPrefix	"kDebugInfoStringKey"
#define	kInfoStringKey		kOptnsDlgStringPrefix	"kInfoStringKey"
#define kBlankStringKey		kOptnsDlgStringPrefix	"kBlankStringKey"
// till here


#define kOptnsDlgAboutBoxStringKey	kOptnsDlgStringPrefix "kOptnsDlgAboutBoxStringKey"
//#define kOptnsDlgTargetMenuPath kOptnsDlgPluginsMenuPath
#define kOptnsDlgDialogTitleKey			kOptnsDlgStringPrefix "kOptnsDlgDialogTitleKey"
#define kGeneralPanelTitleKey			"kGeneralPanelTitleKey"
#define kAttribsPanelTitleKey			"kAttribsPanelTitleKey"
#define kTasksPanelTitleKey				"kTasksPanelTitleKey"

#define kPubDropdownItemKey				kOptnsDlgStringPrefix"kPubDropdownItemKey"

#define kOptnsDlgPanelOrderingResourceID	700
#define kOptnsDlgDialogResourceID			710
#define kOptnsDlgTabDialogResourceID		711

#define kGeneralPanelResourceID				720
#define kGeneralPanelCreatorResourceID		730
#define kAttribsPanelResourceID				740
#define kAttribsPanelCreatorResourceID		750
#define kAttribPanelListElementRsrcID		760

#define kTasksPanelResourceID				770
#define kTasksPanelCreatorResourceID		780

#define kPubBrowseDialogResourceID			790
#define kPathBrowse1DialogResourceID		800


// Initial data format version numbers
#define kOptnsDlgFirstMajorFormatNumber  RezLong(1)
#define kOptnsDlgFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kOptnsDlgCurrentMajorFormatNumber kOptnsDlgFirstMajorFormatNumber
#define kOptnsDlgCurrentMinorFormatNumber kOptnsDlgFirstMinorFormatNumber


//PNGR ID
#define kPNGOptsOKIconRsrcID						1000
#define kPNGOptsOKIconRollRsrcID					1000

#define kPNGOptsApplyIconRsrcID						1001
#define kPNGOptsApplyIconRollRsrcID					1001

#define kPNGOptsCancelIconRsrcID					1002
#define kPNGOptsCancelIconRollRsrcID				1002

#define kPNGOptsUploadIconRsrcID					1003
#define kPNGOptsUploadIconRollRsrcID				1003

#define kPNGOptsChangeIconRsrcID					1004
#define kPNGOptsChangeIconRollRsrcID				1004

#define kPNGOptsResetIconRsrcID						1005
#define kPNGOptsResetIconRollRsrcID					1005

#define kPNGOptnsDlgCancelIconRsrcID				1006
#define kPNGOptnsDlgCancelIconRollRsrcID			1006

#define kPNGOptsDlgOKIconRsrcID						1007
#define kPNGOptsDlgOKIconRollRsrcID					1007

#endif // __SelDlgID_h__

//#endif // __OptnsDlgID_h__

//  Code generated by DollyXs code generator

#ifndef __TSTABLESOURCEHELPER_H__
#define __TSTABLESOURCEHELPER_H__

//#include "VCPlugInHeaders.h"
#include "IPMUnknown.h"
#include "TSID.h"
#include "TableSourceInfoValue.h"

class ITSTableSourceHelper : public IPMUnknown
{

public:

	enum	{kDefaultIID = IID_IPRODUCTTABLEIFACE};
	virtual void showTableSourcePanel()=0;

	virtual void positionTABLESourcePanel(int32 left , int32 righ , int32 top , int32 bottom) = 0 ;
	virtual void showTableList(double objectID , double langID , double parentID , double parentTypeID , double sectionID ,int32 isProductOrItem,double pbObjectID) = 0;
	virtual void populateProductTableList(double,double) = 0;	
	virtual bool16  isTABLESourcePanelOpen() = 0;
	virtual void populateItemTableList(double sectionID , double objectID) = 0 ;

	virtual void setTableSourceInfoValueObj(TableSourceInfoValue &obj) = 0;
	virtual TableSourceInfoValue * getTableSourceInfoValueObj() = 0;
	//	TSTableSourceHelper();
	//TSTableSourceHelper(IPMUnknown*  boss);
	//~TSTableSourceHelper();

};

#endif

#ifndef __ICategoryBrowser__
#define __ICategoryBrowser__

#include "IPMUnknown.h"
#include "CTBID.h"

class ICategoryBrowser : public IPMUnknown
{
	public:	
		enum { kDefaultIID = IID_ICATEGORYBROWSER };
		virtual void OpenCategoryBrowser(int32 Top, int32 Left, int32 Right, int32 Bottom ,int32 isContentFlg , double currSelected_ClassID)=0;
		virtual double GetSelectedClassID(PMString &ClassName)=0;
		virtual void CloseCategoryBrowser()=0;

};

#endif

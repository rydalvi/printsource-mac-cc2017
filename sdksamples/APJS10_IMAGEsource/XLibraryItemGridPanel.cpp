
#include "VCPluginHeaders.h"


// ----- Interface Includes -----

#include "IPanelControlData.h"

// ----- Implementation Includes -----

#include "InterfacePtr.h"
#include "IXLibraryViewController.h"
#include "IPMStream.h"
#include "IWindow.h"
#include "DVPanelView.h"
#include "IGraphicsPort.h"
#include "AGMGraphicsContext.h"
#include "Utils.h"
#include "IWidgetUtils.h"
#include "CPMUnknown.h"
#ifdef WINDOWS
	#include "WSystemUtils.h"   //Added by Sachin Sharma
	#include "WSysType.h"
#endif
#include "CAlert.h"
#include "DCNID.h"
#include "AutoGSave.h"

#define kMinGridWidth			64.0
#define kMinGridHeight			77.0

#define CA(x) CAlert::InformationAlert(x)

class XLibraryItemGridPanel : public AGMDrawnPanelView
{
public:
	// ----- Initialization -----
	
	XLibraryItemGridPanel(IPMUnknown *boss);
	virtual ~XLibraryItemGridPanel();

	virtual void		ReadWrite(IPMStream *s, ImplementationID prop);
	
	virtual void		Draw(IViewPort* viewPort, SysRgn updateRgn);
	virtual void		Show(bool16 doShow = kTrue);
	virtual void 		Resize(const PMPoint& dimensions, bool16 invalidate);

	virtual void 		WindowChanged();

	virtual PMPoint		ConstrainDimensions(const PMPoint& dimensions) const; 
	
	virtual void		AdaptToParentsSize(const PMPoint& delta);

private:

	void				DrawBackground(IGraphicsPort* gPort, const PMRect& viewRt);
	void				DrawCellOutlines(IGraphicsPort* gPort, const PMRect& viewRt);
	
	bool16 fStartUp;
};
												  
CREATE_PERSIST_PMINTERFACE(XLibraryItemGridPanel, kXLibraryItemGridPanelImpl)

//========================================================================================
// CLASS XLibraryItemGridPanel
//========================================================================================

XLibraryItemGridPanel::XLibraryItemGridPanel(IPMUnknown *boss) :
	AGMDrawnPanelView(boss), fStartUp( kFalse )
{
}

XLibraryItemGridPanel::~XLibraryItemGridPanel()
{
}

void XLibraryItemGridPanel::Show(bool16 doShow )
{
	//CA("Show");
	AGMDrawnPanelView::Show(doShow);

	// Tell the view controller to setup the scroll bars
	InterfacePtr<IXLibraryViewController> viewController( this, IID_IXLIBRARYVIEWCONTROLLER );
	if(viewController == NULL)
	{
		//CA("viewController is nil");
		return;
	}
	viewController->SetupScrollBarConnections();
}


void XLibraryItemGridPanel::ReadWrite(IPMStream *s, ImplementationID prop)
{
	//CA("ReadWrite");
	AGMDrawnPanelView::ReadWrite(s, prop);

	if ( !fStartUp && s->IsReading() )
		fStartUp = kTrue;
}

void XLibraryItemGridPanel::Draw(IViewPort* viewPort, SysRgn updateRgn)
{
	//CA("Draw----");
	AGMGraphicsContext gc(viewPort, this, updateRgn);
	InterfacePtr<IGraphicsPort> gPort(gc.GetViewPort(), UseDefaultIID());	// IID_IGRAPHICSPORT);
	if(gPort == nil)
	{
		//CA("gPort is nil");
		return;
	}
		
	AutoGSave autoGSave(gPort);
	
	SysRect vbbox = GetBBox();
	//--vbbox.right, --vbbox.bottom;
			
	PMRect viewRt(vbbox);
//--------------------------------
//	PMString dim("Dimensions::");
//	dim.AppendNumber(viewRt.left);
//	dim.Append("--");
//	dim.AppendNumber(viewRt.top);
//	dim.Append("--");
//	dim.AppendNumber(viewRt.right);
//	dim.Append("--");
//	dim.AppendNumber(viewRt.bottom);
	//CA(dim);
//-------------------------------

	gc.GetViewToContentTransform().Transform(&viewRt);

	// Clip the children so they dont hammer the borders
//APSCC2017_Comment
//	SysRgn gridContentRgn = ::CreateRectSysRgn (vbbox);
//	SysRgn clippedUpdateRgn = nil;
//	if (updateRgn != nil)
//		clippedUpdateRgn = ::CopySysRgn(updateRgn);
//	::IntersectSysRgn(gridContentRgn, clippedUpdateRgn, clippedUpdateRgn);

	DrawBackground(gPort, viewRt);
//	AGMDrawnPanelView::Draw(viewPort, clippedUpdateRgn);
	DrawCellOutlines(gPort, viewRt);
	
//	::DeleteSysRgn(gridContentRgn);
//	::DeleteSysRgn(clippedUpdateRgn);
	
	gPort->grestore();
}

//========================================================================================
// Resize
//========================================================================================
void XLibraryItemGridPanel::Resize(const PMPoint& dimensions, bool16 invalidate)
{
	//CA("Resize...");
	PMRect frame = this->GetFrame();
	const PMReal nDeltaX = dimensions.X() - frame.Width();
	const PMReal nDeltaY = dimensions.Y() - frame.Height();
	
	frame.Right( frame.Right() + nDeltaX );
	frame.Bottom( frame.Bottom() + nDeltaY );

	if (invalidate)
		Invalidate();

	AGMDrawnPanelView::Resize( frame.Dimensions(), invalidate );
	InterfacePtr<IPanelControlData> panelData(this, IID_IPANELCONTROLDATA);
	if(panelData == NULL)
	{
		//CA("panelData is null");
		return;
	}
	Utils<IWidgetUtils>()->GridPanel_PositionWidgets(panelData, 0, panelData->Length() - 1);
	
	//Dirty();
	PreDirty();//Modified By Sachin sharma on 22/6/07
}


//========================================================================================
// ConstrainDimensions
//========================================================================================
PMPoint XLibraryItemGridPanel::ConstrainDimensions(const PMPoint& dimensions) const
{
	PMPoint newPoint(dimensions);
	if ( newPoint.X() < kMinGridWidth ) newPoint.X(kMinGridWidth);
	if ( newPoint.Y() < kMinGridHeight ) newPoint.Y(kMinGridHeight);
	return newPoint;
}

void XLibraryItemGridPanel::WindowChanged()
{
	//CAlert::InformationAlert("WindowChanged");
	if ( !fStartUp )
		AGMDrawnPanelView::WindowChanged();
	fStartUp = kFalse;
}

void XLibraryItemGridPanel::AdaptToParentsSize(const PMPoint& delta)
{

	PMRect frame = GetFrame();
	
	PMPoint dimensions = frame.Dimensions() + delta;
	PMPoint constrainedDimensions = ConstrainDimensions(dimensions);
	frame.SetDimensions(constrainedDimensions.X(), constrainedDimensions.Y());
	
	SetFrame(frame);
}

void XLibraryItemGridPanel::DrawBackground(IGraphicsPort* gPort, const PMRect& viewRt)
{
	Utils<IWidgetUtils>()->DrawPaletteFill(this,gPort,&viewRt);

	//gPort->setrgbcolor(PMReal(236), PMReal(0), PMReal(140));
//	gPort->rectstroke(viewRt);
}

void XLibraryItemGridPanel::DrawCellOutlines(IGraphicsPort* gPort, const PMRect& viewRt)
{
	//CA("DrawCellOutlines");
	InterfacePtr<IPanelControlData> panel(this, IID_IPANELCONTROLDATA);
	if(panel == nil)
	{
		//CA("panelll is nil");
	}
	
	gPort->setrgbcolor(PMReal(0), PMReal(0), PMReal(0));
	
	gPort->setlinewidth(0.0);

	PMRect frame;
	for (int32 i = 0; i < panel->Length(); i++)
	{
		//CA("in");
		PMRect	frame = panel->GetWidget(i)->GetFrame();
		gPort->rectpath(frame);
	}

	gPort->stroke();
}

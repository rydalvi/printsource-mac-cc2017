
#include "VCPlugInHeaders.h"

#include "IGraphicStyleReference.h"
#include "IGraphicStyleNameTable.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "ITextControlData.h"

#include "StringUtils.h"
#include "CObserver.h"	
#include "GraphicsID.h"
#include "WidgetID.h"
#include "PMCollection.h"
#include "IApplication.h"
#include "IEventHandler.h"
#include "IEventDispatcher.h"

#include "ISelectionManager.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "CAlert.h"

#include "DCNID.h"

extern bool16 isBrowsePrintSource;
extern bool16 BrowseFolderOption;
#define CA(X) CAlert::InformationAlert(X)
class XLibrarySelectionObserver : public CObserver
{
	
	public:
		XLibrarySelectionObserver(IPMUnknown*);
		virtual ~XLibrarySelectionObserver();
		
		// ----- IObserver protocol
		
		virtual void AutoAttach();
		virtual void AutoDetach();
		virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		
		void AttachWidget(IPanelControlData* , const WidgetID& , const PMIID&);
		void DetachWidget(IPanelControlData* , const WidgetID& , const PMIID&);


	protected:
		virtual void ChangeToNoSelection();
		virtual void ChangeToOneSelection();
		virtual void ChangeToMultipleSelection( int32 count ); 
		void loadPaletteData();
		IEventHandler* piEventHandler ;


	};
	
//___________________________________________________________________________________
//	SHUKSAN OBJECT MACROS
//___________________________________________________________________________________
CREATE_PMINTERFACE (XLibrarySelectionObserver, kXLibrarySelectionObserverImpl)
	

//===================================================================================
//___________________________________________________________________________________
//	DESCR:		Constructor
//___________________________________________________________________________________
XLibrarySelectionObserver::XLibrarySelectionObserver(IPMUnknown* boss) :
	CObserver (boss)
{
}

//========================================================================================
//	DESCR:		Destructor
//========================================================================================
XLibrarySelectionObserver::~XLibrarySelectionObserver()
{
}

//========================================================================================
//	DESCR:		AutoAttach
//========================================================================================
void XLibrarySelectionObserver::AutoAttach()
{
	InterfacePtr<IPanelControlData>	panelData( this, IID_IPANELCONTROLDATA );
	IControlView* viewWidget = panelData->FindWidget( kXLibraryItemGridWidgetId );
	IControlView* listViewWidget = panelData->FindWidget ( kXLibraryItemListBoxWidgetId );
	IControlView* listViewIconWidget = panelData->FindWidget ( kDCNListViewImageWidgetID);

	InterfacePtr<ISubject> subject(viewWidget, IID_ISUBJECT);
	InterfacePtr<ISubject> listSubject( listViewWidget, IID_ISUBJECT );
	InterfacePtr<ISubject> listViewIconSubject( listViewIconWidget, IID_ISUBJECT );

	if ( !subject->IsAttached( this, IID_IXLIBRARYSELECTIONOBSERVER ) )
		subject->AttachObserver(this, IID_IXLIBRARYSELECTIONOBSERVER);
	if ( !listSubject->IsAttached( this, IID_IXLIBRARYSELECTIONOBSERVER) )
		listSubject->AttachObserver(this, IID_IXLIBRARYSELECTIONOBSERVER);
	if ( !listSubject->IsAttached( this, IID_IXLIBRARYSELECTIONOBSERVER) )
		listSubject->AttachObserver(this, IID_IXLIBRARYSELECTIONOBSERVER);
	
	


	//loadPaletteData();

}

//========================================================================================
//	DESCR:		AutoDetach
//========================================================================================
void XLibrarySelectionObserver::AutoDetach()
{
	InterfacePtr<IPanelControlData>	panelData( this, IID_IPANELCONTROLDATA );
	
	IControlView* viewWidget = panelData->FindWidget( kXLibraryItemGridWidgetId );
	IControlView* listViewWidget = panelData->FindWidget ( kXLibraryItemListBoxWidgetId );
		IControlView* listViewIconWidget = panelData->FindWidget ( kDCNListViewImageWidgetID);

	InterfacePtr<ISubject> subject(viewWidget, IID_ISUBJECT);
	InterfacePtr<ISubject> listSubject( listViewWidget, IID_ISUBJECT );
		InterfacePtr<ISubject> listViewIcon( listViewIconWidget, IID_ISUBJECT );

	if ( subject->IsAttached(this, IID_IXLIBRARYSELECTIONOBSERVER) )
		subject->DetachObserver(this, IID_IXLIBRARYSELECTIONOBSERVER);
	if ( listSubject->IsAttached(this, IID_IXLIBRARYSELECTIONOBSERVER) )
		listSubject->DetachObserver(this, IID_IXLIBRARYSELECTIONOBSERVER);

}


void XLibrarySelectionObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->AttachObserver(this, interfaceID, IID_IXLIBRARYSELECTIONOBSERVER);
	}
	while (false); 
}

void XLibrarySelectionObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;
		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;
		iSubject->DetachObserver(this, interfaceID, IID_IXLIBRARYSELECTIONOBSERVER);
	}
	while (false); 
}


//========================================================================================
//	DESCR:		Update
//========================================================================================
void XLibrarySelectionObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{
		
	//switch(protocol.Get()) {
	//
	//// Handle changes to the Library Selection
	//case IID_IXLIBRARYSELECTIONOBSERVER:
	//	int32 selCount = (int32) changedBy;
	//	if ( selCount <= 0 )
	//		ChangeToNoSelection();
	//	else if ( selCount == 1 )
	//		ChangeToOneSelection();
	//	else
	//		ChangeToMultipleSelection( selCount );
	//	break;
	//}

}


//___________________________________________________________________________________
//	DESCR:		The selection has changed to an empty selection.
//___________________________________________________________________________________
void XLibrarySelectionObserver::ChangeToNoSelection()
{
	// Get the panel data interface
	InterfacePtr<IPanelControlData>	panelData( this, IID_IPANELCONTROLDATA );
	
	// Disable the new button.
	//IControlView* newButton = panelData->FindWidget( kNewButtonWidgetID );
	//newButton->Disable();

	// Disable the Library Item Info Button
//	IControlView* infoButton = panelData->FindWidget( kInfoButtonWidgetId );
//	infoButton->Disable();
	
	// Disable the Delete Library Item Button
//	IControlView* deleteButton = panelData->FindWidget( kDeleteLibraryButtonWidgetID );
//	deleteButton->Disable();
}


//___________________________________________________________________________________
//	DESCR:		The selection has changed to a single page item.
//___________________________________________________________________________________
void XLibrarySelectionObserver::ChangeToOneSelection()
{
	// Get the panel data interface
	InterfacePtr<IPanelControlData>	panelData( this, IID_IPANELCONTROLDATA );

	// Enable theLibrary Item Info Button
/*	IControlView* infoButton = panelData->FindWidget( kInfoButtonWidgetId );
	IControlView* deleteButton = panelData->FindWidget( kDeleteLibraryButtonWidgetID );

	infoButton->Enable();
	
	// Check if the library is unlocked
	InterfacePtr<ILibraryPanelData>	libraryData( this, IID_ILIBRARYDATA );
	ILibrary *library = libraryData->GetLibrary();
	if ( ! library->IsLocked() ) {	
		// Enable the Delete Library Item Button
		deleteButton->Enable();
	}
	else{
		//Disable the Delete Library Item button
		deleteButton->Disable();
	}	*/
}


//___________________________________________________________________________________
//	DESCR:		The selection has changed to multiple page items.
//___________________________________________________________________________________
void XLibrarySelectionObserver::ChangeToMultipleSelection( int32 count )
{
	// Get the panel data interface
	InterfacePtr<IPanelControlData>	panelData( this, IID_IPANELCONTROLDATA );
	
	// Disable the Library Item Info Button
/*	IControlView* infoButton = panelData->FindWidget( kInfoButtonWidgetId );
	IControlView* deleteButton = panelData->FindWidget( kDeleteLibraryButtonWidgetID );

	infoButton->Disable();
	
	// Check if the library is locked
	InterfacePtr<ILibraryPanelData>	libraryData( this, IID_ILIBRARYDATA );
	ILibrary *library = libraryData->GetLibrary();
	if ( ! library->IsLocked() ) {	
		// Enable the Delete Library Item Button
		deleteButton->Enable();
	}
	else{
		deleteButton->Disable();
	}	*/
}

void XLibrarySelectionObserver::loadPaletteData()
{
		
	//CA(__FUNCTION__);
		//InterfacePtr<IApplication> dcnApp(gSession->QueryApplication());
		//InterfacePtr<IEventDispatcher> piEventDispatcher(dcnApp, UseDefaultIID());
		//this->piEventHandler = (IEventHandler*) ::CreateObject(kDCNActiveSelectionBoss,IID_IEVENTHANDLER);
		//// Increase refcount on it so that it is not disposed of and save it
		////  for removing it later
		//this->piEventHandler->AddRef();
		//// Push it on the top of the event handler stack
		//piEventDispatcher->Push(this->piEventHandler);
}


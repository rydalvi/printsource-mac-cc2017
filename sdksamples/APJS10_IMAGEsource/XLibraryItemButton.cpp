
#include "VCPlugInHeaders.h"

// ----- Interface Includes -----

//#include "ShuksanBravo.h"
#include "ISession.h"
#include "IWorkspace.h"
#include "IWidgetParent.h"
#include "IGraphicsContext.h"
#include "IGraphicsPort.h"
#include "IViewPort.h"
#include "IColorViewData.h"
#include "IInterfaceColors.h"
#include "IWindow.h"
#include "IBooleanControlData.h"
#include "IXLibraryViewController.h"
//#include "IAGMImageViewPort.h"
#include "IInterfaceFonts.h"
#include "IRasterPort.h"
// ----- Implementation Includes -----


//#include "ColorSystemID.h"
#include "WidgetID.h"
#include "AGMGraphicsContext.h"
#include "PMString.h"
#include "PMMatrix.h"
//#include "DrawStringUtils.h"
#include "StringUtils.h"
#include "IXLibraryButtonData.h"
//#include "PlatformIconClass.h"
#include "ResourceEnabler.h"

#include "DVControlView.h"
#include "CAlert.h"

#include "FileUtils.h"
#include "SDKUtilities.h"
#include "IEPSAttributes.h"
#include "EPSID.h"
// General includes:
#include "CreateObject.h"
#include "ImageID.h"
#include "ImageTypes.h"
#include "StreamUtil.h"
#include "SysControlIds.h"
#include "GraphicsExternal.h"	// _t_AGM...
#include "IInterfaceColors.h"
#include "ISysFileData.h"
#include "IImportPreview.h"
#include "IImportProvider.h"
#include "IK2ServiceProvider.h"
#include "IK2ServiceRegistry.h"
#include "FileUtils.h"
#include "IBoolData.h"
#include "IFormatImageStreamData.h"
#include "IGraphicsPort.h"
#include "IImageAttributes.h"
#include "IImageFormatManager.h"
#include "IImageReadFormat.h"
#include "IImageReadFormatInfo.h"
#include "IImageStream.h"
#include "IImageStreamManager.h"
#include "IPMStream.h"
#include "ISession.h"
#include "ISysFileData.h"
#include "IWorkspace.h"
#include "ICoreFilename.h"

#include "DCNID.h"

#define CA(X) CAlert::InformationAlert(X);
extern bool16 isThumb;
// ----- Constant Definitions -----


/** Color space family from AGM, subsetted to meet our needs here.
	@ingroup paneltreeview
*/
enum SubsettedAGMColorSpaceFamily
{
	/** */
	kDontCare,
	/** */
	kAGMCsRGB
	/** */
};


class XLibraryItemButton : public DVControlView
{
public:
	// ----- Initialization -----
	
	XLibraryItemButton(IPMUnknown *boss);
	virtual ~XLibraryItemButton();

	// ----- Rendering -----
		/** Called when widget is being initialised.
	@param widgetId [IN] specifies WidgetID to associate with this widget
	@param frame [IN] specifies bounding box for the control
	@param rsrcID [IN] specifies resource to associate with this widget
	*/
	//virtual void Init(
	//	const WidgetID& widgetId, const PMRect& frame, RsrcID rsrcID);
	
	virtual void	Draw(IViewPort* vp, SysRgn updateRgn);

protected:
	virtual void	DrawThumbnailName(IViewPort* vp, SysRgn updateRgn);
	virtual void	DrawThumbnailImage(IViewPort* vp, SysRgn updateRgn);
	virtual void	DrawThumbnailBorder(IViewPort* vp, SysRgn updateRgn);

	ErrorCode HandlePreview_NEW(const IDFile & previewFile, 
						uint32 nWidthWidget, uint32 nHeightWidget);

	ErrorCode HandlePreview(const IDFile& previewFile, 
						uint32 nWidthWidget, uint32 nHeightWidget);		
	ErrorCode createPreview(const IDFile& previewFile, uint32 width, 
						uint32 height, 	uint8 backGrey);

	bool16 areEqual(const IDFile& file1, const IDFile& file2);
	void deleteBuffers();


	PMReal fTextFrameHeight;
	AGMImageRecord *fCurAGMImageRecordPtr;
	AGMImageRecord *fpCurAGMImage;
	IDFile *fpCurImageSysFile;
	uint8* fDataBuffer;
	int32 fCachedImWidth;
	int32 fCachedImHeight;
	IDFile fCurImageSysFile;

};


CREATE_PMINTERFACE(XLibraryItemButton, kXLibraryItemButtonImpl)


//========================================================================================
// CLASS XLibraryItemButton
//========================================================================================

XLibraryItemButton::XLibraryItemButton(IPMUnknown *boss) :
	DVControlView(boss),
	fTextFrameHeight(14),
	fCurAGMImageRecordPtr(nil),
	 fpCurAGMImage(nil),
    fpCurImageSysFile(nil),
	fCachedImWidth(0),
	fCachedImHeight(0)
{
}

XLibraryItemButton::~XLibraryItemButton()
{
	if (fCurAGMImageRecordPtr)
    {	
        //delete fCurAGMImageRecordPtr->baseAddr;
        delete fCurAGMImageRecordPtr;
        fCurAGMImageRecordPtr = nil;
    }
    if (fpCurImageSysFile)
    {
        delete fpCurImageSysFile;
        fpCurImageSysFile = nil;
    }
}

///* Init
//*/
//void XLibraryItemButton::Init(
//	const WidgetID& widgetId, const PMRect& frame, RsrcID rsrcID)
//{
//	DVControlView::Init(widgetId, frame, rsrcID);
//}
//========================================================================================
// XLibraryItemButton::Draw
//========================================================================================
void XLibraryItemButton::Draw(IViewPort* vp, SysRgn updateRgn)
{
	//CA(__FUNCTION__);
	
		DrawThumbnailImage(vp,updateRgn);
		//DrawThumbnailName(vp,updateRgn);
		DrawThumbnailBorder(vp,updateRgn);
	
}

//========================================================================================
// XLibraryItemButton::DrawThumbnailName
//========================================================================================
void XLibraryItemButton::DrawThumbnailName(IViewPort* vp, SysRgn updateRgn)
{
	//CA(__FUNCTION__);
	AGMGraphicsContext gc(vp, this, updateRgn);
	InterfacePtr<IGraphicsPort> gPort(gc.GetViewPort(), IID_IGRAPHICSPORT);
	if(gPort == NULL)
	{
		//CA("IGraphicsPort is null");
		return;
	}
//	InterfacePtr<IWindow> window((IWindow*) QueryParentFor(IID_IWINDOW));

	PMRect frame = GetFrame();
	frame.MoveTo(0, 0);
	
	InterfacePtr<IBooleanControlData> buttonData(this, IID_IBOOLEANCONTROLDATA);
	if(buttonData == NULL)
	{
		//CA("IBooleanControlData is NULL");
		return;
	}
	bool16 isSelected = buttonData->IsSelected();
	
	// Get the data for this particular widget
	InterfacePtr<IXLibraryButtonData> data(this, IID_IXLIBRARYBUTTONDATA);
	if ( data ) {
		
		// Get the Asset's Name
		PMString name = data->GetName(); // "Rahul Dalvi"; //
		name.SetTranslatable( kFalse );
		
		// Save the current gPort
		gPort->gsave();

		// Now center the string in the width by measuring its width and length.
//		const InterfaceFontInfo& fontInfo = isSelected ? window->GetHiliteFont() : window->GetFont();
		InterfacePtr<IInterfaceFonts> fonts(/*gSession*/GetExecutionContextSession(),IID_IINTERFACEFONTS); //Cs4
		//int32 interfaceFontID = isSelected ? kDialogWindowHiliteFontId : kDialogWindowFontId;
		int32 interfaceFontID = isSelected ? kPaletteWindowSystemScriptHiliteFontId : kPaletteWindowSystemScriptFontId;
		const InterfaceFontInfo& fontInfo = fonts->GetFont(interfaceFontID);	
		
		fTextFrameHeight = fontInfo.GetFontSize() + 5;
		
		//PMPoint textDimensions = StringUtils::PMMeasureString(&gc, name, fontInfo);
		//PMPoint textPoint(frame.Width()/2 + 1, frame.Height() );
		//textPoint.X() -= textDimensions.X() / 2;

		//AG:: Multiplication factor applied to contain the text within
		//textPoint.Y() -= 0.4*fontInfo.GetFontSize();
	
		// Check if the string needs to be ellipsized
		//PMString theString(name);
		//theString.SetTranslatable( kFalse );

		//if ( textPoint.X() < 5 ) {
		//	textPoint.X() = 5;
			//theString = StringUtils::PMEllipsizeString(&gc, frame.Width()-10, name, fontInfo);
		//}
		//theString.SetTranslatable(kFalse);
		// Draw the Text
		//StringUtils::PMDrawString(&gc, textPoint, theString, fontInfo, kInterfaceBlack);
		gPort->grestore();
	}
}

//========================================================================================
// XLibraryItemButton::DrawThumbnailImage
//========================================================================================
void XLibraryItemButton::DrawThumbnailImage(IViewPort* vp, SysRgn updateRgn)
{
	//CA(__FUNCTION__);
	ErrorCode result = kFailure;
	AGMGraphicsContext gc(vp, this, updateRgn);
	InterfacePtr<IGraphicsPort> gPort(gc.GetViewPort(), IID_IGRAPHICSPORT);

	PMRect frame = GetFrame();
	frame.Bottom() = frame.Bottom();//- 15;
	//frame.Top() = frame.Top()- 2;
	frame.MoveTo(0, 0);
	
	uint32 nWidthWidget=1;
	uint32 nHeightWidget=1;

	InterfacePtr<IInterfaceColors>iInterfaceColors(GetExecutionContextSession()/*gSession*/, IID_IINTERFACECOLORS);//Cs4
	if(iInterfaceColors == nil)
	{
		//CA("iInterfaceColors is nil");
		return;
	}
	do
	{
		ErrorCode resultOfCreatingPreview = kFailure;
		// draw the 3D border		
		RealAGMColor borderColor, highLightColor, shadowColor, fillColor;
		iInterfaceColors->GetRealAGMColor(kInterfaceBorder, borderColor);
		iInterfaceColors->GetRealAGMColor(kInterfaceBevelHighlight, highLightColor);
		iInterfaceColors->GetRealAGMColor(kInterfaceBevelShadow, shadowColor);
		iInterfaceColors->GetRealAGMColor(kInterfacePaletteFill, fillColor);  
    
		// draw bevel shadow along top left
		gPort->setrgbcolor(shadowColor.red, shadowColor.green, shadowColor.blue);
		gPort->moveto(frame.Left(), frame.Bottom());
		gPort->lineto(frame.Left(), frame.Top());
		gPort->lineto(frame.Right(), frame.Top());
		gPort->stroke();

		// draw bevel high light along bottom right
		gPort->setrgbcolor(highLightColor.red, highLightColor.green, highLightColor.blue);
		gPort->moveto(frame.Right() - 1, frame.Top());
		gPort->lineto(frame.Right(), frame.Bottom() );
		gPort->lineto(frame.Left(), frame.Bottom() );
		gPort->stroke();

		// fill with background color - in case image aspect ratio does not match widget
	//	frame.Inset(1,1);
	//	gPort->setrgbcolor(fillColor.red, fillColor.green, fillColor.blue);
	//	gPort->rectpath(frame);
	//	gPort->fill();

		nWidthWidget = ToInt32(Round(frame.Width())) /*-2*/ ;  // leave a little border inside the bevel
		nHeightWidget = ToInt32(Round(frame.Height())) ;  // leave a little border inside the bevel

		// The bool-data state tells this code whether the image is valid to display (or attempt to display)
		InterfacePtr<IBoolData> iIsFileValid(this, UseDefaultIID());
		//ASSERT(iIsFileValid);
		if(!iIsFileValid)
		{
			//CA("Image Not valid to display");
			break;
		}
		
		// get filespec currently associated with widget
		InterfacePtr<ISysFileData> iImageSysFile(this, IID_ISYSFILEDATA);
		//ASSERT(iImageSysFile);
		if(!iImageSysFile)
		{
			//CA("!iImageSysFile");
			break;
		}
		
		IDFile previewFile = iImageSysFile->GetSysFile(); 

		RealAGMColor whiteFill;
		iInterfaceColors->GetRealAGMColor(kInterfaceWhite, whiteFill);
		RealAGMColor defaultGreyFill;
		iInterfaceColors->GetRealAGMColor(kInterfacePaletteFill, defaultGreyFill); 
		
	
		// if filespec has been updated, delete the cached image (if present)
		//if (fCurAGMImageRecordPtr)
		//{
		//	if (iIsFileValid->GetBool())
		//	{
		//		
		//		// If we have no cached image or if the files do not match
		//		if (!fpCurImageSysFile || (!this->areEqual(*fpCurImageSysFile,previewFile))/*|| Mediator::testFlag==1*/)
		//		{
		//			delete fCurAGMImageRecordPtr->baseAddr;
		//			delete fCurAGMImageRecordPtr;
		//			fCurAGMImageRecordPtr = nil;
		//		}
		//		else{
		//			
		//			delete fCurAGMImageRecordPtr->baseAddr;
		//			delete fCurAGMImageRecordPtr;
		//			fCurAGMImageRecordPtr = nil;
		//		}
		//	}
		//	else
		//	{
		//		delete fCurAGMImageRecordPtr->baseAddr;
		//		delete fCurAGMImageRecordPtr;
		//		fCurAGMImageRecordPtr = nil;
		//	}
		//}

		//if (!fCurAGMImageRecordPtr && fpCurImageSysFile)
		//{
		//	delete fpCurImageSysFile;
		//	fpCurImageSysFile = nil;
		//}

		//if (!fCurAGMImageRecordPtr && iIsFileValid->GetBool())
		//{
		//	
		//	const IDFile fil=previewFile;
		//	PMString lcPath=SDKUtilities::SysFileToPMString(&fil);

		//	InterfacePtr<IInterfaceColors>
		//	iInterfaceColors(gSession, IID_IINTERFACECOLORS);
		//	ASSERT(iInterfaceColors);
		//	if(!iInterfaceColors){
		//		break;
		//	}
		//	RealAGMColor whiteFill;
		//	iInterfaceColors->GetRealAGMColor(kInterfaceWhite, whiteFill);
		//	RealAGMColor defaultGreyFill;
		//	iInterfaceColors->GetRealAGMColor(kInterfacePaletteFill, defaultGreyFill); 
		//	
		//
		//	uint8 backgroundGrey = ToInt32(Round(
		//		255.0* (whiteFill.red +  whiteFill.green + whiteFill.blue)/3.0));

		//	result = this->createPreview(previewFile, nWidthWidget, nHeightWidget, backgroundGrey);
		////	if(lcPath.Contains(PMString(".eps"))||lcPath.Contains(PMString(".ai"))||lcPath.Contains(PMString(".ps")))
		////		result = this->HandlePreview_NEW(previewFile, nWidthWidget, nHeightWidget);
		////	else
		////		result = this->HandlePreview(previewFile, nWidthWidget, nHeightWidget);
		//}

			if(fCurImageSysFile != previewFile) {
			// Take average greyvalue of what we're filling background to
			/*uint8 backgroundGrey = ToInt32(Round(
				255.0* (whiteFill.red +  whiteFill.green + whiteFill.blue)/3.0));*/
			uint8 backgroundGrey = ToInt32(Round(
				255.0* (fillColor.red +  fillColor.green + fillColor.blue)/3.0));

			// Create another preview, if we need to
			resultOfCreatingPreview = this->createPreview(previewFile, nWidthWidget, nHeightWidget, backgroundGrey);
			
			} else {
				// That's OK- assume safe to use the old one
				resultOfCreatingPreview = kSuccess;
			}
		
			// if we got an image, then display it
			// otherwise, draw a red diagonal line through the widget
			if (resultOfCreatingPreview == kSuccess) {
				// Fill the background with white
				gPort->setrgbcolor(whiteFill.red, whiteFill.green, whiteFill.blue);
				gPort->rectpath(frame);
				gPort->fill();
				PMReal imageWidth = (fpCurAGMImage->bounds.xMax - fpCurAGMImage->bounds.xMin);
				PMReal imageHeight = ((fpCurAGMImage->bounds.yMax)  - fpCurAGMImage->bounds.yMin);
				PMReal xOffset = frame.GetHCenter() - imageWidth/2;
				PMReal yOffset = frame.GetVCenter() - imageHeight/2;
				// Centered
				gPort->translate(xOffset, yOffset);
				PMMatrix theMatrix;	// No transform 
				ASSERT(fpCurAGMImage);
				gPort->image(fpCurAGMImage, theMatrix, 0);
			}
			else {
				// Fill with the palette default background fill
				gPort->setrgbcolor(defaultGreyFill.red, defaultGreyFill.green, defaultGreyFill.blue);
				gPort->rectpath(frame);
				gPort->fill();
				// Render a red stroke over the background
				gPort->setrgbcolor(PMReal(1), PMReal(0), PMReal(0));
				gPort->setlinewidth(PMReal(2));
				gPort->setmiterlimit(PMReal(2));  // SPAM: what units does this use?
				gPort->moveto(frame.Left()+2, frame.Bottom()-2 );
				gPort->lineto(frame.Right()-2, frame.Top()+2);
				gPort->stroke();
			}		
	gPort->grestore();
	} while(kFalse);

	//if (result == kSuccess)
	//{
	//	
	//	PMMatrix matrix;
	//	uint32   nCenterX, nCenterY;

	//	nCenterX = (nWidthWidget - fCurAGMImageRecordPtr->bounds.xMax) / 2 + 2;  // extra pixels to allow for border
	//	nCenterY = (nHeightWidget - fCurAGMImageRecordPtr->bounds.yMax) / 2 + 2;  // extra pixels to allow for border
	//	matrix.Translate(PMReal(nCenterX),PMReal(nCenterY));
	//	gPort->image(fCurAGMImageRecordPtr, matrix,0);
	//	//CA("ok");
	//}
	//else
	//{
	//	//InterfacePtr<IBooleanControlData> buttonData(this, IID_IBOOLEANCONTROLDATA);
	//	//bool16 isSelected = buttonData->IsSelected();
	//	//
	//	//// Get the data for this particular widget
	//	//InterfacePtr<IXLibraryButtonData> data(this, IID_IXLIBRARYBUTTONDATA);
	//	//if ( data ) {
	//	//
	//	//	PMRect iconRect( frame.Left()+3, frame.Top()+3, frame.Right()-3, frame.Bottom()-fTextFrameHeight-3 );
	//	//	
	//	//	// just draw an X
	//	//	gPort->gsave();
	//	//	{
	//	//		if ( isSelected )
	//	//		{
	//	//			gPort->setlinewidth(3.0);
	//	//			gPort->setrgbcolor( 1.0,0,0 ); // red
	//	//		}
	//	//		else
	//	//		{
	//	//			gPort->setlinewidth(2.0);
	//	//			gPort->setrgbcolor( 0,0,1.0 ); // blue
	//	//		}
	//	//	
	//	//		gPort->moveto(iconRect.Left(),iconRect.Top() );
	//	//		gPort->lineto(iconRect.Right(),iconRect.Bottom() );
	//	//		
	//	//		gPort->moveto(iconRect.Left(),iconRect.Bottom() );
	//	//		gPort->lineto(iconRect.Right(),iconRect.Top() );
	//	//		
	//	//		gPort->stroke();
	//	//	}

	//		//gPort->setrgbcolor(PMReal(1), PMReal(0), PMReal(0));
	//		//gPort->setlinewidth(PMReal(3));
	//		//gPort->setmiterlimit(PMReal(2));  // SPAM: what units does this use?
	//		//gPort->moveto(frame.Left()+2, frame.Bottom()-2-20);
	//		//gPort->lineto(frame.Right()-2, frame.Top()+2);
	//		//gPort->stroke();

	//		//gPort->grestore();
	//	//}
	//}	
	//
}

//========================================================================================
// XLibraryItemButton::DrawThumbnailBorder
//========================================================================================
void XLibraryItemButton::DrawThumbnailBorder(IViewPort* vp, SysRgn updateRgn)
{
	//CA(__FUNCTION__);
	AGMGraphicsContext gc(vp, this, updateRgn);
	InterfacePtr<IGraphicsPort> gPort(gc.GetViewPort(), IID_IGRAPHICSPORT);
	if(gPort == NULL)
	{
		//CA("gPort is null");
	}

	PMRect frame = GetFrame();
	frame.MoveTo(0, 0);
	
	InterfacePtr<IBooleanControlData> buttonData(this, IID_IBOOLEANCONTROLDATA);
	bool16 isSelected = buttonData->IsSelected();

	// Check if the thumbnail is currently selected
	gPort->gsave();
	{
		PMRect completeRect = frame;					//The complete thunmbnail rectangle
		completeRect.Inset( 1, 1 );

		//Rectangle for the image/icon in the thumbnail
		PMRect iconRect( completeRect.Left(), completeRect.Top(), completeRect.Right(), completeRect.Bottom() );//- fTextFrameHeight );
		//Rectangle for the name of the thumbnail
		//PMRect textRect( completeRect.Left(), completeRect.Bottom()-fTextFrameHeight, completeRect.Right(), completeRect.Bottom() );

		InterfacePtr<IInterfaceColors> colors( /*gSession*/GetExecutionContextSession(), IID_IINTERFACECOLORS );//Cs4
		if(colors == nil)
		{
			//CA("colors is nil");
		}
		RealAGMColor bevelShadowColor, bevelHighlightColor;

		//Get the interface colors
		colors->GetRealAGMColor( kInterfaceBevelShadow, bevelShadowColor );
		colors->GetRealAGMColor( kInterfaceBevelHighlight, bevelHighlightColor );


		completeRect.Inset( 1, 1 );
		gPort->setlinewidth(1.0);
		gPort->setrgbcolor( bevelHighlightColor.red, bevelHighlightColor.green, bevelHighlightColor.blue );

		//Draw the bevel highlight lines around icon rectangle
		gPort->newpath();
		gPort->moveto( completeRect.Left(), completeRect.Bottom() );
		gPort->lineto( completeRect.Right(), completeRect.Bottom() );
		gPort->lineto( completeRect.Right(), completeRect.Top() );
		gPort->stroke();

		gPort->moveto( iconRect.Left()+1, iconRect.Bottom() );
		gPort->lineto( iconRect.Right()-1, iconRect.Bottom() );
		gPort->stroke();

		iconRect.Inset( 1, 1 );
		//textRect.Inset( 1, 1 );

		gPort->setrgbcolor( bevelShadowColor.red, bevelShadowColor.green, bevelShadowColor.blue );
		
		//bevel shadow lines for icon rectangle
		gPort->moveto( iconRect.Left(), iconRect.Bottom() );
		gPort->lineto( iconRect.Left(), iconRect.Top() );
		gPort->lineto( iconRect.Right()-1, iconRect.Top() );
		gPort->stroke();

		//and for the text rectangle
	/*	gPort->moveto( textRect.Left(), textRect.Bottom()-1 );
		gPort->lineto( textRect.Left(), textRect.Top() );
		gPort->lineto( textRect.Right()-1, textRect.Top() );
		gPort->stroke();*/
	}

	if ( isSelected ) {

		// Inset the thumbnail's frame by 2 pixels to draw a frame around it
		PMRect thumbnailRect = frame;
		thumbnailRect.Inset( 2, 2 );
		
		// Set the line width to 2 pixels
		gPort->setlinewidth( 2.0 );
		
		// Get the highlight color
		InterfacePtr<IInterfaceColors> colors(/*gSession*/GetExecutionContextSession(), IID_IINTERFACECOLORS);//Cs4
		RealAGMColor highlight;
		colors->GetRealAGMColor( kInterfaceHighLight, highlight );
		gPort->setrgbcolor(highlight.red, highlight.green, highlight.blue);

		thumbnailRect.Right( thumbnailRect.Right() +1 );
		thumbnailRect.Bottom( thumbnailRect.Bottom() +1 );		
		// Draw the highlight rectangle around the thumbnail
		gPort->rectstroke(thumbnailRect);
	}

	gPort->grestore();
	
}


/* HandlePreview
*/
ErrorCode XLibraryItemButton::HandlePreview_NEW(
	const IDFile & previewFile, 
	uint32 nWidthWidget, 
	uint32 nHeightWidget)
{

  // CA(__FUNCTION__);

	ErrorCode result = kFailure;

    // Find the preview bitmap, get its bounds and device context.
	do{
		int32 width = nWidthWidget;
		int32 height = nHeightWidget;
		
		//Creating the file stream
		IPMStream *fileStream = StreamUtil::CreateFileStreamRead(previewFile,kOpenIn);
        
		if (fileStream == nil)
		{	//CA("fileStream == nil");
			break;
		}
		InterfacePtr<IK2ServiceRegistry>    serviceR(/*gSession*/GetExecutionContextSession(), IID_IK2SERVICEREGISTRY);//Cs4
		if ( serviceR == nil)
		{ //CA(" serviceR == nil ");
			break;
		}
		int    numHandlers = serviceR->GetServiceProviderCount(kImportProviderService);
		for (int i = 0; i < numHandlers; i++)
		{
			fileStream->Seek(0, kSeekFromStart);
			InterfacePtr<IK2ServiceProvider> provider(serviceR->QueryNthServiceProvider(kImportProviderService, i));
			if ( provider == nil)
			{  //CA(" provider == nil ");
				break;
			}
			InterfacePtr<IImportProvider> importProvider(provider, IID_IIMPORTPROVIDER);
			if(importProvider == nil)
			{
				//CA("importProvider is nil");
			}
			
			InterfacePtr<IImportPreview> preview(importProvider, IID_IIMPORTPREVIEW);
			

			if (importProvider && preview && importProvider->CanImportThisStream(fileStream) == IImportProvider::kFullImport)
			{
				// Now that we have a provider, try to generate a preview
					
				////////////// for getting freame size of image
				// setup a transformation from source image to preview image (destination)
				/*InterfacePtr<IImageAttributes>
					iSrcAttr((IImageAttributes*)::CreateObject(kImageItem,IID_IIMAGEATTRIBUTES));
				//ASSERT(iSrcAttr);
				if(iSrcAttr == nil)
				{	//CA("Attrbute nil");
					break;
				}
				uint32 nWidthSrc, nHeightSrc;
				iSrcAttr->GetTag(IImageAttributes::kPMTagImageWidth, &nWidthSrc);
				iSrcAttr->GetTag(IImageAttributes::kPMTagImageHeight, &nHeightSrc);
				if ((nWidthSrc <= 0) || (nHeightSrc <= 0))
				{	///CA("image hieght not supported");
					break;
				}
				PREMediatorClass::widthI=nWidthSrc;
				PREMediatorClass::heightI=nHeightSrc;
				////////// end ////*/

				//create the preview
				/*InterfacePtr<IEPSAttributes>
			iSrcAttr((IEPSAttributes*)::CreateObject(kDisplayListPageItemBoss ,IID_IEPSATTRIBUTES));
				if(iSrcAttr == nil)
				{	//CA("Attrbute nil");
					break;
				}
				PMReal ScrRect=iSrcAttr->GetTargetRes();
				PMString ss;
				ss.AppendNumber(ScrRect);
				ss.AppendNumber(ScrRect.top);
				ss.Append(" : ");
				ss.AppendNumber(ScrRect.bottom);
				ss.Append(" : ");
				ss.AppendNumber(ScrRect.right);
				ss.Append(" : ");
				ss.AppendNumber(ScrRect.left);*/

				//CA(ss);
				

				uint8 *buffer =new uint8[width*height*400];
				if ( buffer == nil)
				{	//CA("buffer == nil");
					break;
				}				
				//set the background to white // FF  // BA for gray
				//CA("Before memset function Vaibhav");
				::memset(buffer, 0xCE, width*height*400);

				if ( preview->Create24bitRGBPreview( (uint8*)buffer, width, height, fileStream, kFalse )  != kSuccess ) 
				{
					delete [] buffer;
					//CA("buffer == delete");
					break;
				}

			//	PREMediatorClass::widthI=width;
			//	PREMediatorClass::heightI=height;		
				fpCurImageSysFile = new IDFile(previewFile);
				fCurAGMImageRecordPtr = new AGMImageRecord;
				memset (fCurAGMImageRecordPtr, 0, sizeof(AGMImageRecord));
				fCurAGMImageRecordPtr->bounds.yMax = nHeightWidget;
				fCurAGMImageRecordPtr->bounds.xMax = nWidthWidget;
				fCurAGMImageRecordPtr->baseAddr = buffer;
				fCurAGMImageRecordPtr->byteWidth = nWidthWidget * 3;
				fCurAGMImageRecordPtr->colorSpace = kAGMCsRGB;
				fCurAGMImageRecordPtr->bitsPerPixel = (int16)24;	
				result = kSuccess;	
			}
		}
			fileStream->Close();
            fileStream->Release();  
			
	} while(false);
	
    return result;
}
////////////// not show eps file ///
/* HandlePreview
*/
ErrorCode XLibraryItemButton::HandlePreview(
	const IDFile& previewFile, 
	uint32 nWidthWidget, 
	uint32 nHeightWidget)
{
	ErrorCode result = kFailure;
	//PMString ASD = previewFile.GetString();
	//CA(ASD);
	do
	{	
	
		// get source stream (image file to preview)
		InterfacePtr<IPMStream>
			iFileStream(StreamUtil::CreateFileStreamRead(previewFile));
		// State of the bool data may be true, but we might just have 
		// a duff stream, so don't assert but break out anyway
		if(iFileStream == nil)
		{	//CA("File Stream nil");
			break;
		}
	//	CA("7.2");	
		InterfacePtr<IImageStreamManager>
			iStreamMgr((IImageStreamManager*)::CreateObject(
			kImageStreamManagerBoss, IID_IIMAGESTREAMMANAGER));
		//ASSERT(iStreamMgr);
		if(iStreamMgr == nil)
		{	//CA("file manager nil");
			break;
		}
	
		InterfacePtr<IImageStream>
			iSrcStream((IImageStream *)::CreateObject(
			kReadFormatImageStreamBoss, IID_IIMAGESTREAM));
		//ASSERT(iSrcStream);
		if (iSrcStream == nil)
		{	//CA("Image stream nil");
			break;
		}
	
		InterfacePtr<IFormatImageStreamData>
			iSrcData(iSrcStream, IID_IFORMATIMAGESTREAMDATA);
		//ASSERT(iSrcData);
		if(iSrcData == nil)
		{	//CA("Foramt of image not supported");
			break;
		}

		// check if there is an image format that recognizes this image file
		InterfacePtr<IImageFormatManager>
			iImageFormatManager((IImageFormatManager*)::CreateObject(
			kImageFormatManager, IID_IIMAGEFORMATMANAGER));
		//ASSERT(iImageFormatManager);
		if (iImageFormatManager == nil)
		{	//CA("image format manager nil");
			break;
		}

		InterfacePtr<IImageReadFormat> iImageReadFormat(
			iImageFormatManager->QueryImageReadFormat(iFileStream));
		// Don't assert here: if we can't get this interface, it just
		// means it's a format that we can't preview. Fair enough, fail quietly
		if (iImageReadFormat == nil)
		{	//CA("Cant read image Due to low Quality");
			break;
		}
	
		// setup a transformation from source image to preview image (destination)
		InterfacePtr<IImageAttributes>
			iSrcAttr((IImageAttributes*)::CreateObject(kImageObject,IID_IIMAGEATTRIBUTES));
		//ASSERT(iSrcAttr);
		if(iSrcAttr == nil)
		{	//CA("Attrbute nil");
			break;
		}

		InterfacePtr<IImageAttributes>
			iDstAttr((IImageAttributes*)::CreateObject(kImageObject, IID_IIMAGEATTRIBUTES));
		//ASSERT(iDstAttr);
		if (iDstAttr == nil)
			break;

		// open source image and get its attributes
		if (iImageReadFormat->CanRead(iFileStream) == kFalse)
		{	//CA("image format nil");
			break;
		}

		iImageReadFormat->Open(iFileStream);
		iImageReadFormat->GetImageAttributes(iSrcAttr);

		// now setup the preview image (destination) attributes
		uint32 nColorSpace = kPMRGBColorSpace;
		uint32 nBitsPerChannel = 8;
		uint32 nChannelsPerPixel = 3;
		uint32 nPlanarConfig = kPMChunky;
		uint32 nXRes = 72;
		uint32 nYRes = 72;

		uint32 nWidthSrc, nHeightSrc, nWidthDst, nHeightDst;
		uint32 nColorSpaceSrc;

		iSrcAttr->GetTag(IImageAttributes::kPMTagImageWidth, &nWidthSrc);
		iSrcAttr->GetTag(IImageAttributes::kPMTagImageHeight, &nHeightSrc);
		if ((nWidthSrc <= 0) || (nHeightSrc <= 0))
		{	///CA("image hieght not supported");
			break;
		}

//		PREMediatorClass::widthI=nWidthSrc;
	//	PREMediatorClass::heightI=nHeightSrc;

		iSrcAttr->GetTag(IImageAttributes::kPMTagColorSpace, &nColorSpaceSrc);
		if ((nColorSpaceSrc < 1) || (nColorSpaceSrc > 0x1000))
		{	//CA("color space nil");
			break;
		}
		// don't upsample the source - center it in the preview box
		if (nWidthSrc < nWidthWidget)
		{
			nWidthDst = nWidthSrc;
			nHeightDst = nHeightSrc;
			if (nHeightDst > nHeightWidget)
			{
				nHeightDst = nHeightWidget;
				nWidthDst = ToInt32(Round(PMReal(nHeightDst) * PMReal(nWidthSrc) / PMReal(nHeightSrc)));
			}
		}
		else
		{
			if (nHeightSrc < nHeightWidget)
			{
				nHeightDst = nHeightSrc;
				nWidthDst = nWidthSrc;
				if (nWidthDst > nWidthWidget)
				{
					nWidthDst = nWidthWidget;
					nHeightDst = ToInt32(Round(PMReal(nWidthDst) * PMReal(nHeightSrc) / PMReal(nWidthSrc)));
				}
			}
			else // downsample to fit in the preview dimensions
			{
				nWidthDst = nWidthWidget;
				nHeightDst = ToInt32(Round(PMReal(nWidthDst) * PMReal(nHeightSrc) / PMReal(nWidthSrc)));
				if (nHeightDst > nHeightWidget)
				{
					nHeightDst = nHeightWidget;
					nWidthDst = ToInt32(Round(PMReal(nHeightDst) * PMReal(nWidthSrc) / PMReal(nHeightSrc)));
				}
			}
		}
		iDstAttr->CopyTags(iSrcAttr);
		iDstAttr->SetTag(IImageAttributes::kPMTagColorSpace, 4, &nColorSpace);
		iDstAttr->SetTag(IImageAttributes::kPMTagBitsPerChannel, 4, &nBitsPerChannel);
		iDstAttr->SetTag(IImageAttributes::kPMTagChannelsPerPixel, 4, &nChannelsPerPixel);
		iDstAttr->SetTag(IImageAttributes::kPMTagPlanarConfig, 4, &nPlanarConfig);
		iDstAttr->SetTag(IImageAttributes::kPMTagImageWidth, 4, &nWidthDst);
		iDstAttr->SetTag(IImageAttributes::kPMTagImageHeight, 4, &nHeightDst);
		iDstAttr->SetTag(IImageAttributes::kPMTagXResolution, 4, &nXRes);
		iDstAttr->SetTag(IImageAttributes::kPMTagYResolution, 4, &nYRes);
		if (iDstAttr->TagExists(IImageAttributes::kPMTagColorMap))
		{
			iDstAttr->DeleteTag(IImageAttributes::kPMTagColorMap);
		}
		iSrcData->SetImageReadFormat(iImageReadFormat);
		iStreamMgr->SetSourceAttributes(iSrcAttr);
		iStreamMgr->SetDestinationAttributes(iDstAttr);
		iStreamMgr->SetFirstImageStreamSegment(iSrcStream);

		// do the actual conversion
		InterfacePtr<IImageStream> iDstStream(iStreamMgr->QueryImageStream());
		//ASSERT(iDstStream);
		if (iDstStream == nil)
		{	//CA("Stream nil");
			break; // conversion failed
		}
		ImageStreamRecord imageStreamRec;
		memset (&imageStreamRec, 0, sizeof(ImageStreamRecord));
		imageStreamRec.fHeight = nHeightDst;
		imageStreamRec.fWidth = nWidthDst;
		imageStreamRec.fRowBytes = imageStreamRec.fWidth * 3;
		int32 bufSize = imageStreamRec.fHeight * imageStreamRec.fRowBytes;
		imageStreamRec.fBuffer = new uchar[bufSize];
		if (imageStreamRec.fBuffer)
		{	
			iDstStream->ReadData(imageStreamRec);
		}
		iImageReadFormat->Close();
		if (!imageStreamRec.fBuffer)
		{
			//CA("Buffer nil");
			break;
		}
		fpCurImageSysFile = new IDFile(previewFile);
		fCurAGMImageRecordPtr = new AGMImageRecord;
		memset (fCurAGMImageRecordPtr, 0, sizeof(AGMImageRecord));
		fCurAGMImageRecordPtr->bounds.yMax = (int16)imageStreamRec.fHeight;
		fCurAGMImageRecordPtr->bounds.xMax = (int16)imageStreamRec.fWidth;
		fCurAGMImageRecordPtr->baseAddr = imageStreamRec.fBuffer;
		fCurAGMImageRecordPtr->byteWidth = imageStreamRec.fWidth * 3;
		fCurAGMImageRecordPtr->colorSpace = kAGMCsRGB;
		fCurAGMImageRecordPtr->bitsPerPixel = (int16)24;

		result = kSuccess;
	} while(false);
	
	return result;
}


#ifdef WINDOWS
/* areEqual
*/
bool16 XLibraryItemButton::areEqual(
	const IDFile& file1, const IDFile& file2)
{
	bool16 retval=kFalse;
	do
	{
		//CA(__FUNCTION__);

		InterfacePtr<ICoreFilename>
			cfn1((ICoreFilename*)::CreateObject(kCoreFilenameBoss, IID_ICOREFILENAME));
		//ASSERT(cfn1);
		if(!cfn1)
		{
			break;
		}
		cfn1->Initialize(&file1);		
		InterfacePtr<ICoreFilename>
			cfn2((ICoreFilename *)::CreateObject(kCoreFilenameBoss, IID_ICOREFILENAME));
		//ASSERT(cfn2);
		if(!cfn2)
		{
			break;
		}
		cfn2->Initialize(&file2);
		bool32 couldConvert1 = cfn1->ConvertToUNC();
		bool32 couldConvert2 = cfn2->ConvertToUNC();
		if(couldConvert1 && couldConvert2) {
			PMString* name1Ptr = cfn1->GetFullName();
			PMString* name2Ptr = cfn2->GetFullName();
			//ASSERT(name1Ptr && name2Ptr);
			if (!(name1Ptr && name2Ptr))
			{
				break;
			}
			// recall Compare's like stricmp
			retval = (0 == name1Ptr->Compare(kFalse /* case insensitive*/, *name2Ptr));
		}
	} while(kFalse);
	
	return retval;
}

#endif



ErrorCode XLibraryItemButton::createPreview(
	const IDFile& previewFile, 
	uint32 width, 
	uint32 height, 
	uint8 backGrey
	)
{
	do
	{
		// get source stream (image file to preview)
		InterfacePtr<IPMStream>
			fileStream(StreamUtil::CreateFileStreamRead(previewFile));

		if(fileStream == nil) {
			break;
		}
		InterfacePtr<IK2ServiceRegistry>	serviceRegistry(/*gSession*/GetExecutionContextSession(), UseDefaultIID());//Cs4
		ASSERT(serviceRegistry);
		if(!serviceRegistry) {
			break;
		}
		int	numHandlers = serviceRegistry->GetServiceProviderCount(kImportProviderService);
		for (int i = 0; i < numHandlers; i++) {

			InterfacePtr<IK2ServiceProvider> provider(
				serviceRegistry->QueryNthServiceProvider(kImportProviderService, i));
			InterfacePtr<IImportProvider> importProvider(provider, IID_IIMPORTPROVIDER);
			
			if (importProvider && 
				importProvider->CanImportThisStream(fileStream) == IImportProvider::kFullImport) {
			
				InterfacePtr<IImportPreview> preview(importProvider, IID_IIMPORTPREVIEW);
				if(preview) {
									
					bool16 reallocateNeeded = kTrue;
					// If the dimensions we're trying to write to have 
					// changed, do a realloc. This might happen for instance
					// if we decide to let the widget be resizable in some way,
					// either programmatically by us or by the end-user. But at
					// present (Mar 2005), the image widget is fixed dimension
					if( (this->fCachedImHeight == height) &&
						(this->fCachedImWidth == width)) {
							reallocateNeeded = kFalse;
						}

					if(reallocateNeeded) {
						// Trash any existing storage
						this->deleteBuffers();
					
						fpCurAGMImage = new AGMImageRecord;
						memset (fpCurAGMImage, 0, sizeof(AGMImageRecord));
						fpCurAGMImage->bounds.xMin 			= 0;
						fpCurAGMImage->bounds.yMin 			= 0;
						fpCurAGMImage->bounds.xMax 			= width;
						fpCurAGMImage->bounds.yMax 			= height;
						fpCurAGMImage->byteWidth 			= 3*width; //BYTES2ROWBYTES(3*width);
						fpCurAGMImage->colorSpace 			= kAGMCsRGB;
						fpCurAGMImage->bitsPerPixel 		= 24;
						fpCurAGMImage->decodeArray 			= 0;
						fpCurAGMImage->colorTab.numColors 	= 0;
						fpCurAGMImage->colorTab.theColors 	= nil;

						this->fDataBuffer =	new uint8[((fpCurAGMImage->byteWidth) * height)];
						ASSERT(this->fDataBuffer);
						fpCurAGMImage->baseAddr = static_cast<void *>(this->fDataBuffer);

						this->fCachedImHeight = height;
						this->fCachedImWidth = width;
					}
					ASSERT(fpCurAGMImage);
					//set the background to be grey
					::memset(fpCurAGMImage->baseAddr, backGrey, (fpCurAGMImage->byteWidth) * height);
			
					if (fpCurAGMImage->baseAddr) {
						//AcquireWaitCursor busyCursor;
						preview->Create24bitRGBPreview( (uint8*)fpCurAGMImage->baseAddr,
							width, height, fileStream, kTrue );
						fCurImageSysFile = previewFile;
						// Exit, we don't need another handler
						
						return kSuccess;
					}
				}
			}
			// Be sure to reset the stream if we're trying out another handler
			fileStream->Seek(0,kSeekFromStart);
		} // i loop over handlers

		

	} while(false);
	
	return kFailure;
}


void XLibraryItemButton::deleteBuffers()
{
	if(fpCurAGMImage) {
		//delete (uint8 *)fpCurAGMImage->baseAddr;
		delete [] fDataBuffer;
		delete fpCurAGMImage;
		fpCurAGMImage = nil;
	}	
}
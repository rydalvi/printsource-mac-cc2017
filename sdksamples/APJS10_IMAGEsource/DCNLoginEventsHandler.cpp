#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "DCNID.h"
#include "SPID.h"
#include "ILoginEvent.h"
#include "CAlert.h"
#include "DCNLoginEventsHandler.h"
//#include "DCNSelectionObserver.h"
//#include "IPaletteMgr.h"
#include "PaletteRefUtils.h"
#include "IPanelMgr.h"
#include "LocaleSetting.h"
#include "IWindow.h"
#include "IApplication.h"
//#include "DCNActionComponent.h"
#include "IWidgetParent.h"
//#include "ILoggerFile.h"
#include "PRImageHelper.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#define CA(z) CAlert::InformationAlert(z)

extern double objId;			
extern double parId;
extern double parTypeId;
extern double sectId;

CREATE_PMINTERFACE(DCNLoginEventsHandler,kDCNLoginEventsHandlerImpl)

DCNLoginEventsHandler::DCNLoginEventsHandler(IPMUnknown* boss):CPMUnknown<ILoginEvent>(boss){}

DCNLoginEventsHandler::~DCNLoginEventsHandler(){}

bool16 DCNLoginEventsHandler::userLoggedIn(void)
{
//CA("DCNLoginEventsHandler::userLoggedIn");
	do{
		InterfacePtr<IApplication> 	iApplication(/*gSession*/GetExecutionContextSession()->QueryApplication());//Cs4
		if(iApplication == nil)
			break;
		//Commented By sachin Sharma On 23/06/07
		//InterfacePtr<IPaletteMgr> iPaletteMgr(iApplication->QueryPanelManager());//QueryPaletteManager()); Modified By sachin Sharma on23/06/07
		//if(iPaletteMgr == nil) 
		//	break;

		//InterfacePtr<IPanelMgr> iPanelMgr(iPaletteMgr, UseDefaultIID()); 
		InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager()); 
		if(iPanelMgr == nil)
			break;
		
		//if(iPanelMgr->IsPanelWithWidgetIDShown(kSPPanelWidgetID))    //IsPanelWithWidgetIDVisible(kSPPanelWidgetID))
		//	iPanelMgr->ShowPanelByWidgetID(kDCNPanelWidgetID);

	}while(kFalse);	
	return kTrue;	
}

bool16 DCNLoginEventsHandler::userLoggedOut(void)
{
	//CA("DCNLoginEventsHandler::userLoggedOut");
	do{
		InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
		if(ptrImageHelper == nil)
		{
			//CA("ProductImages plugin not found ");
			return kFalse;
		}
		if(ptrImageHelper->isImagePanelOpen()){
			ptrImageHelper->EmptyImageGrid();
			ptrImageHelper->closeImagePanel();
			
			objId=-1;			
			parId=-1;
			parTypeId=-1;
			sectId=-1;
		}
	}while(kFalse);	
	return kTrue;	
}

bool16 DCNLoginEventsHandler::resetPlugIn(void)
{
	//CA("DCNLoginEventsHandler::resetPlugIn");
	return kTrue;
}

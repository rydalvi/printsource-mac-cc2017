#ifndef __ASSETINFO_H__
#define __ASSETINFO_H__

#include "IPMUnknown.h"
#include "DCNID.h"
#include "K2Vector.h" 
using namespace std;

class AssetInfo
{
public:	
	K2Vector<int32> itemIDS ;
	K2Vector<int32> imageMPVID;
	K2Vector<int32> imageTypeID;
	K2Vector<int32> PVImageIndex;
	K2Vector<PMString> typeName;
	K2Vector<int32> attributeID;
	K2Vector<PMString> imageType;  
	K2Vector<PMString> imageVector1 ;
	K2Vector<PMString> imagefileName;
	K2Vector<PMString> vectorImageTypeTagName;

	inline void clearAll()
	{
		imageType.clear();
		vectorImageTypeTagName.clear();
		imageTypeID.clear();
		imageMPVID.clear();
		imageVector1.clear();
		imagefileName.clear();
		typeName.clear();
		itemIDS.clear();
		attributeID.clear();
	}
};
#endif
#include "VCPlugInHeaders.h"

//interfaces
#include "ITip.h"
//#include "ITool.h"
#include "IControlView.h"
#include "IWidgetUtils.h"
#include "IPanelControlData.h"
#include "Utils.h"
#include "TipsID.h"

//includes
#include "PMString.h"
#include "Trace.h"
#include "HelperInterface.h"
#include "StringUtils.h"

#include "DCNID.h"

class MyGridTip : public ITip
{ 
	public: 
		MyGridTip(IPMUnknown *boss); 
		virtual ~MyGridTip(); 
		virtual PMString GetTipText(const PMPoint& mouseLocation/*, ITip::TipWindowPlacementAdvice *outWindowPlacement*/); 
		virtual bool16  UpdateToolTipOnMouseMove(); 
		DECLARE_HELPER_METHODS()
}; 

CREATE_PMINTERFACE( MyGridTip,kMyGridTipImpl )
DEFINE_HELPER_METHODS( MyGridTip )

MyGridTip::MyGridTip(IPMUnknown *boss)
	:HELPER_METHODS_INIT(boss)
{
}

MyGridTip::~MyGridTip()
{
}

PMString MyGridTip::GetTipText(const PMPoint& mouseLocation/*, ITip::TipWindowPlacementAdvice *outWindowPlacement*/) 
{ 

	do {
        InterfacePtr<IPanelControlData> data(this, IID_IPANELCONTROLDATA);
		ASSERT(data); if (!data) break;

		if ( data && data->Length() > 0)
		{ 
			for ( int32 i = data->Length() - 1; i >= 0; i-- ) // check children in reverse order of DVPanelView::Draw
			{ 
				if(data->IsWidgetInstantiated(i))
				{
					IControlView *  childView = data->GetWidget( i );
					SysPoint localWindowPoint = Utils<IWidgetUtils>()->ConvertPointToLocalWindow(mouseLocation, childView);
					if ( childView && childView->IsVisible() && childView->HitTest( localWindowPoint ) ) 
					{
						InterfacePtr<ITip> childTip(childView,IID_ITIP);
						if(childTip)
							return childTip->GetTipText(localWindowPoint/*, outWindowPlacement*/);
					}
				}
			}
		}
		} while(false);
	return PMString();
}
 
bool16  MyGridTip::UpdateToolTipOnMouseMove()
{

	return kFalse;
}

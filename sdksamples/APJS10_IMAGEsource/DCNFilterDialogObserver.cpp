//========================================================================================
//  
//  $File: $
//  
//  Owner: Sachin Sharma
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
// General includes:
#include "CDialogObserver.h"
// Project includes:
#include "DCNID.h"
#include "CAlert.h"
#include "IAppFramework.h"

#include "DCNActionComponent.h"
#include "IClientOptions.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"

#define CA(x)	CAlert::InformationAlert(x)
#define YP(x) CAlert::InformationAlert(x)
#define CA_NUM(i,Message,NUM) PMString K##i(Message);K##i.AppendNumber(NUM);CA(K##i)

extern bool16 isShowPVImages;
extern bool16 isShowPartnerImages;
extern int32 isProd;
extern double objId;
extern double langaugeId;
extern double parId;
extern  double parTypeId;
extern double sectId;

// Chetan --
/** Implements IObserver based on the partial implementation CDialogObserver.
	@ingroup content sprayer*/

class DCNFilterDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		DCNFilterDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~DCNFilterDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);

		void populateRoleDropDownList();
		void populateUserDropDownList(double roleId);
		void populateProductList(double classId);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(DCNFilterDialogObserver, kDCNFilterDialogObserverImpl)

/* AutoAttach
*/
void DCNFilterDialogObserver::AutoAttach()
{
	static bool8 IsAutoAttachCalledFirstTime = kTrue;
	
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	return;

	do
	{
		//CA("Inside AutoAttach");
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if(!panelControlData)
		{
			//CA("panelcontroldata nil");
			break;
		}
		
		// Attach to other widgets you want to handle dynamically here.
		 AttachToWidget(kDCNSaveButtonWidgetID,      IID_ITRISTATECONTROLDATA,	panelControlData);
		 AttachToWidget(kDCNCancelButtonWidgetID,      IID_ITRISTATECONTROLDATA,	panelControlData);
		 
		 AttachToWidget(kDisplayPartnerImagesCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		 AttachToWidget(kDisplayPickListImagesCheckBoxWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);


	} while (kFalse);
}

/* AutoDetach
*/
void DCNFilterDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();
	do
	{
	//	CA("Inside AutoDetach");
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) {
			break;
		}
		// Detach from other widgets you handle dynamically here.
	    DetachFromWidget(kDCNSaveButtonWidgetID, IID_ITRISTATECONTROLDATA,	panelControlData);
		DetachFromWidget(kDCNCancelButtonWidgetID, IID_ITRISTATECONTROLDATA,	panelControlData);
		
		DetachFromWidget(kDisplayPartnerImagesCheckBoxWidgetID, IID_ITRISTATECONTROLDATA,	panelControlData);
		DetachFromWidget(kDisplayPickListImagesCheckBoxWidgetID, IID_ITRISTATECONTROLDATA,	panelControlData);

	 } while (kFalse);
}

/* Update
*/
void DCNFilterDialogObserver::Update
(
	const ClassID& theChange,
	ISubject* theSubject,
	const PMIID& protocol,
	void* changedBy
)
{
	//CA("Inside dialog update ");
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	
	//CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA(" ptrIAppFramework nil ");
		return;
	}
	bool16 result=ptrIAppFramework->getLoginStatus();
	if(!result)
		return;

	do
	{		
		//CA(" -- 1 $$ ");
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		ASSERT(panelControlData);
		if(!panelControlData) 
		{
			break;
		}
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		ASSERT(controlView);
		if(!controlView) 
		{
			break;
		}

		
		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions == nil)
		{
			//CAlert::ErrorAlert("Interface for IClientOptions not found.");
			break ;
		}
		
		InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
	    if(ptrImageHelper == nil)
	    {
			//CA("ProductImages plugin not found ");
			return;
	    }

		InterfacePtr<IDialogController> dialogController(this,UseDefaultIID());
		if(dialogController == nil)
		{
			CA("dialogController == nil");
			break;
		}

		WidgetID theSelectedWidget = controlView->GetWidgetID();
			
		if((theSelectedWidget == kDCNSaveButtonWidgetID || theSelectedWidget == kOKButtonWidgetID)&& theChange==kTrueStateMessage)
		{
			AcquireWaitCursor awc;
			awc.Animate(); 
			
			ITriStateControlData :: TriState partnerImagesState = dialogController->GetTriStateControlData(kDisplayPartnerImagesCheckBoxWidgetID);
			ITriStateControlData :: TriState pickListState = dialogController->GetTriStateControlData(kDisplayPickListImagesCheckBoxWidgetID);
			
			if(partnerImagesState == ITriStateControlData::kSelected)
				isShowPartnerImages = kTrue;
			else
				isShowPartnerImages = kFalse;

			if(pickListState == ITriStateControlData::kSelected)
				isShowPVImages = kTrue;
			else
				isShowPVImages = kFalse;

			if(isShowPartnerImages == kTrue)			
			{
				ptrIClientOptions->set_DisplayPartnerImages(1);

				if(isProd == 1)
					ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
				else
					ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);

			}
			else
			{
				ptrIClientOptions->set_DisplayPartnerImages(0);

				if(isProd == 1)
					ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
				else
					ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);

			}

			if(isShowPVImages == kTrue)			
			{
				ptrIClientOptions->set_DisplayPickListImages(1);

				if(isProd == 1)
					ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
				else
					ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);

			}
			else
			{
				ptrIClientOptions->set_DisplayPickListImages(0);

				if(isProd == 1)
					ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
				else
					ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);

			}
			DCNActionComponent Ac(this);
			Ac.CloseFilterDialog();	
			break;
		}

		if((theSelectedWidget == kDCNCancelButtonWidgetID  || theSelectedWidget == kCancelButton_WidgetID )&& theChange==kTrueStateMessage)
		{			
			DCNActionComponent Ac(this);
			Ac.CloseFilterDialog();			
			break;
		}

		
		//if(theSelectedWidget == kDisplayPickListImagesCheckBoxWidgetID )
		//{
		//	
		//	//CA("kPickListImagesWidgetID 22 ");
		//	
		//	if(theChange == kTrueStateMessage)
		//		isShowPVImages = kTrue;
		//	else
		//		isShowPVImages = kFalse;
		//
		//	
		///*	AcquireWaitCursor awc;
		//	awc.Animate(); 

		//	if(isProd == 1)
		//		ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
		//	else
		//		ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);*/

		//	break;
		//}

		//if(theSelectedWidget == kDisplayPartnerImagesCheckBoxWidgetID )
		//{
		//	//CA("status1 == kTrue for partner images 11");
		//	
		//	//isShowPartnerImages =!isShowPartnerImages;
		//	if(theChange == kTrueStateMessage)
		//		isShowPartnerImages = kTrue;
		//	else
		//		isShowPartnerImages = kFalse;
		//	
		//	/*AcquireWaitCursor awc;
		//	awc.Animate(); 

		//	if(isProd == 1)
		//		ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
		//	else
		//		ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);*/

		//	break;
		//}

		
	} while (kFalse);
}








#include "VCPluginHeaders.h"


// ----- Interface Includes -----

#include "IPanelControlData.h"
#include "IControlView.h"
#include "IGridAttributes.h"
#include "IGraphicsPort.h"
#include "IViewPort.h"
#include "IWidgetParent.h"
#include "IWorkspace.h"
#include "ISession.h"
#include "IInterfaceColors.h"
#include "IWindow.h"
#include "IXLibraryViewController.h"
#include "CAlert.h"

// ----- Implementation Includes -----

#include "InterfacePtr.h"
#include "DVPanelView.h"
#include "AGMGraphicsContext.h"
#include "CreateObject.h"
#include "IPanelMgr.h"
#include "IApplication.h"
#include "PaletteRef.h"

#include "DCNID.h"
#define CA(x) CAlert::InformationAlert(x)

class XLibraryItemViewPanel : public DVPanelView
{
public:
	// ----- Initialization -----
	
	XLibraryItemViewPanel(IPMUnknown *boss);
	virtual ~XLibraryItemViewPanel();
	
	virtual void			ReadWrite(IPMStream *s, ImplementationID prop);

	virtual void			Show(bool16 doShow = kTrue);
	virtual void 			Resize(const PMPoint& dimensions, bool16 invalidate); 
	virtual void			WindowChanged(); 

	//virtual PMPoint			ConstrainDimensions(const PMPoint& dimensions) const; 

//	virtual void			WindowClosed();

};

CREATE_PERSIST_PMINTERFACE(XLibraryItemViewPanel, kXLibraryItemViewPanelImpl)

//========================================================================================
// CLASS XLibraryItemViewPanel
//========================================================================================

XLibraryItemViewPanel::XLibraryItemViewPanel(IPMUnknown *boss) :
	DVPanelView(boss)
{
}

XLibraryItemViewPanel::~XLibraryItemViewPanel()
{
}

void XLibraryItemViewPanel::Show(bool16 doShow)
{
	DVPanelView::Show(doShow);
}


void XLibraryItemViewPanel::ReadWrite(IPMStream *s, ImplementationID prop)
{
	DVPanelView::ReadWrite(s, prop);
}


//========================================================================================
// Resize
//========================================================================================
void XLibraryItemViewPanel::Resize(const PMPoint& dimensions, bool16 invalidate)
{
	//CA("Resize");
	PMRect frame = this->GetFrame();
	const PMReal nDeltaX = dimensions.X() - frame.Width();
	const PMReal nDeltaY = dimensions.Y() - frame.Height();
	
	frame.Right( frame.Right() + nDeltaX );
	frame.Bottom( frame.Bottom() + nDeltaY );
	
	
	// ----- DVControlView::Resize does smart invalidation, by only invalidating the
	//		 newly exposed part of the view. But if your data is layed out differently
	//		 because of a resize that doesn't work. So here we invalidate the whole view
	//		 before calling resize. [amb] 
	// 		 [tjg -- copied this comment and call from SwatchesPanel::RenderListView.cpp]
	if (invalidate)
		Invalidate();
	
	/*if(frame.Right == 0 && frame.Bottom == 0 )
	{
		CA("Its zero zero");
		 PMPoint newDimension(PMReal(207),PMReal(291));
		 DVControlView::Resize(newDimension, invalidate) ;
	}
	else*/
		//DVControlView::Resize( frame.Dimensions(), invalidate );

	InterfacePtr<IPanelControlData>	panelData( this, IID_IPANELCONTROLDATA );
	if(panelData == NULL)
	{
		//CA("panelData is NULL");
		return;
	}
	IControlView* childView;
	
	// Move the item panel scroll bar
	childView = panelData->FindWidget( kXLibraryItemScrollBarWidgetId );
	if(childView == NULL)
	{
		//CA("childView is NULL");
	}
	frame = childView->GetFrame();
	frame.Left( frame.Left() + nDeltaX );
	frame.Right( frame.Right() + nDeltaX );
	frame.Bottom( frame.Bottom() + nDeltaY );
	childView->SetFrame( frame );

	// Resize the item grid panel
	childView = panelData->FindWidget( kXLibraryItemGridWidgetId );
	if(childView == NULL)
	{
		//CA("childView1 is NULL");
	}
	frame = childView->GetFrame();
	frame.Right( frame.Right() + nDeltaX );
	frame.Bottom( frame.Bottom() + nDeltaY );
	childView->Resize( frame.Dimensions(), invalidate );
	
	// Notify the view controller that the panel was resized
	childView = panelData->FindWidget( kXLibraryItemGridWidgetId );
	if(childView == NULL)
	{
		//CA("childView2 is NULL");
		return;
	}
	InterfacePtr<IXLibraryViewController> viewController( childView, IID_IXLIBRARYVIEWCONTROLLER );
	if(viewController == NULL)
	{
		//CA("viewController is NULL");
		return;
	}
	viewController->SizeChanged();

	//Dirty();  //Depricated Method for Cs3..
	PreDirty();//Updated By Sachin sharma 22/06/2007

}


//========================================================================================
// WindowChanged
//========================================================================================
void XLibraryItemViewPanel::WindowChanged()
{
	DVPanelView::WindowChanged();

	InterfacePtr<IPanelControlData>	panelData( this, IID_IPANELCONTROLDATA );
	if(panelData == NULL)
	{
		//CA("panelData1 is NULL");
		return;
	}
	IControlView*					childView;
	
	InterfacePtr<IWidgetParent> widgetPtr(this, IID_IWIDGETPARENT);
	if(widgetPtr == NULL)
	{
		//CA("widgetPtr is NULL");
		return;
	}
	InterfacePtr<IWindow> window((IWindow*) widgetPtr->QueryParentFor(IID_IWINDOW));

	//InterfacePtr<IApplication> 	iApplication(gSession->QueryApplication());
	//if(iApplication==nil)
	//{
	//	CA("iApplication==nil");
	//	//ptrIAppFramework->LogDebug("AP46_CategoryBrowser::CTBActionComponent::DoPallete::iApplication == nil");
	//	return;
	//}
	//
	//InterfacePtr<IPanelMgr> iPanelMgr(iApplication->QueryPanelManager(), UseDefaultIID()); 
	//if(iPanelMgr == nil)
	//{
	//	CA("iPanelMgr==nil");
	//	//ptrIAppFramework->LogDebug("AP46_CategoryBrowser::CTBActionComponent::DoPallete::iPanelMgr == nil");				
	//	return;
	//}
	//IControlView* iControlView = NULL;
	//iControlView = iPanelMgr->GetPanelFromActionID(kDCNPanelWidgetActionID);
	//if(iControlView==NULL)
	//{
	//	CA("::iControlView == nil");		
	//	return;
	//}
	//
	//PaletteRef palRef = iPanelMgr->GetPaletteRefContainingPanel(iControlView );
	//if(!palRef.IsValid())
	//{
	//	CA("palRef is invalid");
	//	return;
	//}
	//
	if (window)
	//if(palRef.IsValid())
	{
		//CA("window");
		// Tell the view controller to setup the scroll bars
		childView = panelData->FindWidget( kXLibraryItemGridWidgetId );
		if(childView == NULL)
		{
			//CA("childView ");
		}
		InterfacePtr<IXLibraryViewController> viewController( childView, IID_IXLIBRARYVIEWCONTROLLER );
		if(viewController == NULL)
		{
			//CA("viewController");
		}
		viewController->SetupScrollBarConnections();
	}
	else
	{
		//CA("window bypass");
		//Hope this might be a place from where we can release the scroll bar
		//taking the hint that the parent window has vanished
		childView = panelData->FindWidget( kXLibraryItemGridWidgetId );
		if(childView == NULL)
		{
			//CA("childView9");
		}
		InterfacePtr<IXLibraryViewController> viewController( childView, IID_IXLIBRARYVIEWCONTROLLER );
		if(viewController == NULL)
		{
			//CA("viewController9");
		}
		viewController->ReleaseScrollBar();
	}
}


//========================================================================================
// ConstrainDimensions
//========================================================================================
//PMPoint XLibraryItemViewPanel::ConstrainDimensions(const PMPoint& dimensions) const
//{
//	PMPoint constrainedDim(dimensions);
///*
//	// Width can vary if not above maximum or below minimum
//	if(constrainedDim.X() > kMaxAssetLibWidth  )
//		constrainedDim.X( kMaxAssetLibWidth );
//	else if(constrainedDim.X() < kMinAssetLibWidth  )
//		constrainedDim.X( kMinAssetLibWidth );
//	
//	// Height can vary if not above maximum or below minimum
//	if(constrainedDim.Y() > kMaxAssetLibHeight  )
//		constrainedDim.Y( kMaxAssetLibHeight );
//	else if(constrainedDim.Y() < kMinAssetLibHeight  )
//		constrainedDim.Y( kMinAssetLibHeight );
//*/
//	InterfacePtr<const IPanelControlData>	panel( this, IID_IPANELCONTROLDATA );
//	if(panel == NULL)
//	{
//		//CA("panel is null");
//		
//	}
//	IControlView* view = panel->FindWidget( kXLibraryItemGridWidgetId);
//	if(view == NULL)
//	{
//		//CA("view is null");
//		
//	}
//	
//	PMReal scrollBarWidth = panel->FindWidget(kXLibraryItemScrollBarWidgetId)->GetFrame().Dimensions().X();
//	
//	constrainedDim.X(constrainedDim.X() - scrollBarWidth);
//  	constrainedDim = view->ConstrainDimensions(constrainedDim);
//  	constrainedDim.X(constrainedDim.X() + scrollBarWidth);
//
//	return constrainedDim;
//}


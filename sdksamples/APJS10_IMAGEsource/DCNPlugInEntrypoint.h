#ifndef __DCNPlugInEntrypoint_h__
#define __DCNPlugInEntrypoint_h__

#include "PlugIn.h"
#include "GetPlugin.h"
#include "ISession.h"

class DCNPlugInEntrypoint : public PlugIn
{
public:
	virtual bool16 Load(ISession* theSession);

#ifdef WINDOWS
	static ITypeLib* fDCNTypeLib;
#endif                    
};

#endif
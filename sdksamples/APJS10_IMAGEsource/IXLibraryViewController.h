
#pragma once
#ifndef __IXLibraryViewController__
#define __IXLibraryViewController__


class IControlView;
typedef uint16 LocalMenuID;


class IXLibraryViewController : public IPMUnknown
{
public:
	typedef enum {
		kThumbnailView,
		kListView
	} ViewType;
	
	typedef enum {
		kSortByName
	} SortType;
		
	virtual IControlView*	CreateViewItemWidget(PMString& itemName,int32& size) = 0;
								// Create a new view item widget
	
	virtual void			InsertViewItemWidget(IControlView* viewWidget) = 0;
								// Inserts the specified widget into the view
	
	virtual void			InsertViewItemWidgetAndUpdate(IControlView* viewWidget,
									bool16 autoSelect, bool16 autoScroll,const PMString &path) = 0;
								// Inserts the specified widget into the view and
								// updates the display
	
	virtual void			DeleteViewItemWidget(IControlView* viewWidget) = 0;
	virtual void			DeleteViewItemWidget(int32 position) = 0;
								// Deletes the specified widget from the view
	
	virtual void			DeleteViewItemWidgetAndUpdate(IControlView* viewWidget) = 0;
	virtual void			DeleteViewItemWidgetAndUpdate(int32 position) = 0;
								// Deletes the specified widget from the view and
								// updates the display
	
	virtual void			ViewItemWidgetChanged(IControlView* viewWidget,
									bool16 autoScroll) = 0;
	virtual void			ViewItemWidgetChanged(int32 position,
									bool16 autoScroll) = 0;
								// Resorts the grid based on the changed widget
	
	virtual void			SelectViewItemWidget(IControlView* viewWidget) = 0;
	virtual void			DeselectViewItemWidget(IControlView* viewWidget) = 0;
								// Select/Deselect the specified widget in the view
										
	virtual void			ScrollIntoView(IControlView* viewWidget) = 0;
								// Scroll the view to show the specified widget
	
	virtual int32			GetScrollValue() = 0;
	virtual void			SetScrollValue(int32 value) = 0;
								// Get and set grid view Y scroll bar values
								
	virtual void			UpdateView() = 0;
								// Forces an update of the view (redraw and scroll bar adjustment)
	
	virtual void			SizeChanged() = 0;
								// Notify the Controller that the view size changed
											
	virtual void			SetupScrollBarConnections() = 0;
								// Tell the controller to setup the scroll bars

	virtual void			ReleaseScrollBar() = 0;
										
	virtual void			SetViewType( ViewType viewMethod ) = 0;
								// Change the view method tothe new type
	
	virtual ViewType		GetViewType() const = 0;
								// Return the current view method
																									
	virtual void			SetSortOrder( SortType sortMethod ) = 0;
								// Change the sort method tothe new type
																
	virtual SortType		GetSortOrder() const = 0;
								// Return the current sort method

	virtual void			Select(int32 index, bool16 invalidate = kTrue, bool16 notifyOnChange = kTrue) = 0;
	virtual void			SelectAll(bool16 invalidate = kTrue, bool16 notifyOnChange = kTrue) = 0;
	virtual void 			Deselect(int32 index, bool16 invalidate = kTrue, bool16 notifyOnChange = kTrue ) = 0;
	virtual void			DeselectAll(bool16 invalidate = kTrue, bool16 notifyOnChange = kTrue) = 0;
	
	virtual bool16			IsSelected(int32 index) const = 0;
	virtual int32			GetSelected() const = 0;
	virtual void			GetSelected(K2Vector<int32>& multipleSelection) const = 0;
};

#endif

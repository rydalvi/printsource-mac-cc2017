//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/paneltreeview/WinFileSystemIterator.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: ipaterso $
//  
//  $DateTime: 2005/02/25 04:54:14 $
//  
//  $Revision: #8 $
//  
//  $Change: 320645 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

#ifdef WINDOWS 
#include "WinFileSystemIterator.h"

// General includes:
#include "K2Vector.tpp"
#include "CAlert.h"
#include "FileUtils.h"
#include "SDKUtilities.h"

/* SetStartingPath
*/
void WinFileSystemIterator::SetStartingPath(const IDFile& fileSpec)
{
	fSpec = fileSpec;
}


/* FindFirstFile
*/
bool16 WinFileSystemIterator::FindFirstFile(IDFile & resultFile, PMString filter)
{
	fFilter = filter;

	fFilesFound=0;
	if (fhFindFile != INVALID_HANDLE_VALUE)
	{
		::FindClose(fhFindFile);
		fhFindFile = INVALID_HANDLE_VALUE;
	}

	WIN32_FIND_DATA findData;
    PMString fldrFilter = fSpec.GetString();
    fldrFilter.Append(filter);
	fhFindFile = ::FindFirstFile(fldrFilter.GrabTString(), &findData);
	
	if (fhFindFile != INVALID_HANDLE_VALUE)
	{
		PMString fileName;
		fileName.SetTString((LPTSTR)&findData.cFileName);
		PMString fSpecPmString = fSpec.GetString();
		SDKUtilities::RemoveLastElement(fSpecPmString);
		//CAlert::InformationAlert(fSpecPmString);
        fSpec.SetString(fSpecPmString);
		//CAlert::InformationAlert(fSpec.GetString());
		resultFile = fSpec ;
	
		FileUtils::AppendPath(&resultFile, fileName);
		fFilesFound++;
	}

	return fhFindFile != INVALID_HANDLE_VALUE;
}


	
/* FindNextFile
*/
bool16 WinFileSystemIterator::FindNextFile(IDFile& resultFile)
{
	if (fFilesFound > eMaxFolderItemsPerSingleIteration)
	{
		return kFalse;
	}

	WIN32_FIND_DATA findData;
	bool16 foundNext = ::FindNextFile(fhFindFile, &findData);		
	if (foundNext)
	{
		PMString fileName;
		fileName.SetTString(findData.cFileName);
	
		resultFile = fSpec ;
		FileUtils::AppendPath(&resultFile, fileName);

		fFilesFound++;
	}

	if(!foundNext)
	{
		::FindClose (fhFindFile);
		fhFindFile = INVALID_HANDLE_VALUE;
	}
	return foundNext;
}


/* IsDirectory
*/
bool16 WinFileSystemIterator::IsDirectory(const IDFile& fileSpec)
{
	return fileSpec.GetAttribute(IDFile::kDirectory);
}
 

/* GetImmediateChildren
*/
void WinFileSystemIterator::GetImmediateChildren(
	const IDFile& parentSysFile, 
	K2Vector<IDFile>& outFileSpecs,
	const int32 maxFolderItemsPerBulkCall){
	const PMString dot(".");
	const PMString doubleDot("..");
	
	outFileSpecs.clear();

	do
	{

		WIN32_FIND_DATA findData;
		PMString filter("\\*.*");
		PMString fldrFilter = parentSysFile.GetString();
		fldrFilter.Append(filter);
		HANDLE hSearch = ::FindFirstFile(fldrFilter.GrabTString(), &findData); 
		if (hSearch == INVALID_HANDLE_VALUE)
		{ 
			break;
		} 
 
		bool16 hasNext =kTrue;
		int32 filesFound=0;
		while (hasNext && filesFound < maxFolderItemsPerBulkCall) 
		{ 
			
			IDFile resultFile;
			PMString fileName;
			fileName.SetTString(findData.cFileName);
			if(fileName != dot && fileName != doubleDot)
			{				
				resultFile = parentSysFile ;
				FileUtils::AppendPath(&resultFile, fileName);
				outFileSpecs.push_back(resultFile);
				filesFound++;
			}
 
			hasNext = ::FindNextFile(hSearch, &findData);
		} 
 
		// Close the search handle. 
		::FindClose(hSearch);

	} while(kFalse);
}

/////////kept for compatibility//////////////////
	/* FindFirstFile
*/


#endif

//	end, File: WinFileSystemIterator.cpp



////========================================================================================
////  
////  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/WinFileSystemIterator.cpp $
////  
////  Owner: Adobe Developer Technologies
////  
////  $Author: pmbuilder $
////  
////  $DateTime: 2003/09/30 15:41:37 $
////  
////  $Revision: #1 $
////  
////  $Change: 223184 $
////  
////  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
////  
////  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
////  with the terms of the Adobe license agreement accompanying it.  If you have received
////  this file from a source other than Adobe, then your use, modification, or 
////  distribution of it requires the prior written permission of Adobe.
////  
////========================================================================================
//
//#include "VCPluginHeaders.h"
//
//#ifdef WINDOWS 
//#include "WinFileSystemIterator.h"
//
//// General includes:
//#include "K2Vector.tpp"
//#include "CAlert.h"
//
//#include "FileUtils.h"
//#define SysFile IDFile
//
///* SetStartingPath
//*/
//void WinFileSystemIterator::SetStartingPath(const /*SysFile*/IDFile & fileSpec)
//{
//	fSpec = fileSpec;
//	PMString temp = FileUtils::SysFileToPMString(fSpec);
//	CharCounter len = temp.CharCount();
//	for(CharCounter i = len-1; i>=0; i--)
//	{
//		if(temp.GetChar(i) == '\\')
//		{
//			temp.Remove(i, len-i);	
//			break;
//		}
//	}
//	
//	fPath.SetTString(temp.GrabTString());
//}
//
//
///* FindFirstFile
//*/
//bool16 WinFileSystemIterator::FindFirstFile(/*SysFile*/IDFile & resultFile)
//{
//	fFilesFound=0;
//	if (fhFindFile != INVALID_HANDLE_VALUE)
//	{
//		::FindClose(fhFindFile);
//		fhFindFile = INVALID_HANDLE_VALUE;
//	}
//
//	WIN32_FIND_DATA findData;
//	fhFindFile = ::FindFirstFile(fSpec.GrabTString(), &findData);
//	
//	if (fhFindFile != INVALID_HANDLE_VALUE)
//	{
//		/* Code before Conversion
//		PMString fileName;
//		fileName.SetTString((LPTSTR)&findData.cFileName);
//		resultFile.SetString( fPath );
//		resultFile.Append(TEXT('\\'));
//		resultFile += fileName ;
//		fFilesFound++;
//		*/
//		PMString fileName;
//		fileName.SetTString((LPTSTR)&findData.cFileName);
//		//for conversion from CS to CS2
//		PMString temp = FileUtils::SysFileToPMString(fPath );
//		temp.Append(TEXT('\\'));
//		//end conversion
//		resultFile.SetString(temp /*(FileUtils::SysFileToPMString(fPath ))*/);
//		//resultFile.Append(TEXT('\\'));
//		FileUtils::AppendPath(&resultFile,fileName);
//		//resultFile += fileName ;
//		fFilesFound++;
//	}
//
//	return fhFindFile != INVALID_HANDLE_VALUE;
//}
//
//	
///* FindNextFile
//*/
//bool16 WinFileSystemIterator::FindNextFile(/*SysFile*/IDFile & resultFile)
//{
//	if (fFilesFound > eMaxFolderItemsPerSingleIteration)
//	{
//		return kFalse;
//	}
//
//	WIN32_FIND_DATA findData;
//	bool16 foundNext = ::FindNextFile(fhFindFile, &findData);		
//	if (foundNext)
//	{
//		/* Code before conversion
//		PMString fileName;
//		fileName.SetTString(findData.cFileName);
//		resultFile.SetString(fPath);
//		resultFile.Append(TEXT('\\'));
//		resultFile += fileName;
//		fFilesFound++;
//		*/
//		PMString fileName;
//		fileName.SetTString(findData.cFileName);
//
//		PMString temp = FileUtils::SysFileToPMString(fPath );
//		temp.Append(TEXT('\\'));
//		resultFile.SetString(temp);
//		FileUtils::AppendPath(&resultFile,fileName);
//		//resultFile += fileName;
//		fFilesFound++;
//
//	}
//
//	if(!foundNext)
//	{
//		::FindClose (fhFindFile);
//		fhFindFile = INVALID_HANDLE_VALUE;
//	}
//	return foundNext;
//}
//
//
///* IsDirectory
//*/
//bool16 WinFileSystemIterator::IsDirectory(const /*SysFile*/IDFile & fileSpec)
//{
//	if (::GetFileAttributes(fileSpec.GrabTString()) & FILE_ATTRIBUTE_DIRECTORY)
//	{
//		return kTrue;
//	}
//
// 	return kFalse;
//}
// 
//
///* GetImmediateChildren
//*/
//void WinFileSystemIterator::GetImmediateChildren(
//	const /*SysFile*/ IDFile & parentSysFile, 
//	K2Vector<SysFile>& outFileSpecs,
//	const int32 maxFolderItemsPerBulkCall){
//	const PMString dot(".");
//	const PMString doubleDot("..");
//	
//	outFileSpecs.clear();
//
//	do
//	{
//
//		WIN32_FIND_DATA findData;
//		PMString filter(parentSysFile);
//		filter += "\\*.*";
//		HANDLE hSearch = ::FindFirstFile(filter.GrabTString(), &findData); 
//		if (hSearch == INVALID_HANDLE_VALUE)
//		{ 
//			break;
//		} 
// 
//		bool16 hasNext =kTrue;
//		int32 filesFound=0;
//		while (hasNext && filesFound < maxFolderItemsPerBulkCall) 
//		{ 
//			
//			/*SysFile*/IDFile resultFile;
//			PMString fileName;
//			fileName.SetTString(findData.cFileName);
//			if(fileName != dot && fileName != doubleDot)
//			{
//				
//				resultFile.SetString(parentSysFile);
//				resultFile.Append(TEXT('\\'));
//				resultFile += fileName ;
//				outFileSpecs.push_back(resultFile);
//				filesFound++;
//			}
// 
//			hasNext = ::FindNextFile(hSearch, &findData);
//		} 
// 
//		// Close the search handle. 
//		::FindClose(hSearch);
//
//	} while(kFalse);
//}
//
//#endif
//
////	end, File: WinFileSystemIterator.cpp

#ifndef __DCNLoginEventsHandler_h__
#define __DCNLoginEventsHandler_h__

#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "DCNID.h"
#include "ILoginEvent.h"

class DCNLoginEventsHandler : public CPMUnknown<ILoginEvent>
{
public:
	DCNLoginEventsHandler(IPMUnknown* );
	~DCNLoginEventsHandler();
	bool16 userLoggedIn();
	bool16 userLoggedOut();
	bool16 resetPlugIn();
};

#endif
//========================================================================================
//  
//  $File: $
//  
//  Owner: Sachin Sharma
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
// General includes:
#include "CDialogController.h"
// Project includes:
#include "DCNID.h"

#include "IPanelControlData.h"
#include "CAlert.h"

#include "IAppFramework.h"
#include "ILoginHelper.h"
#include "PRImageHelper.h"


//#define CA(x)	CAlert::InformationAlert(x)
//-----------------------------------------------------------------------------------------------//
#define CA(x)	CAlert::InformationAlert(x)
#define YP(x) CAlert::InformationAlert(x)
#define CA_NUM(i,Message,NUM) PMString K##i(Message);K##i.AppendNumber(NUM);CA(K##i)
//------------------------------------------------------------------------------------------------//

extern bool16 isShowPVImages;
extern bool16 isShowPartnerImages;
extern int32 isProd;
extern double objId;
extern double langaugeId;
extern double parId;
extern double parTypeId;
extern double sectId;


/** DCNFilterDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.	
	@ingroup content sprayer
*/
class DCNFilterDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		DCNFilterDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~DCNFilterDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);
};

CREATE_PMINTERFACE(DCNFilterDialogController, kDCNFilterDialogControllerImpl)

/* ApplyFields
*/
void DCNFilterDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	//CA(" in DCNFilterDialogController InitializeDialogFields ");
	CDialogController::InitializeDialogFields(dlgContext);

	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
    		return;

	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil)
	{
		ptrIAppFramework->LogDebug("AP7_IMAGEsource::DCNFilterDialogController::InitializeDialogFields::No panelControlData");
		return;
	}	

	InterfacePtr<IDialogController> dlgController(this,IID_IDIALOGCONTROLLER);
	if(dlgController==nil)
		return;

	//---------
	InterfacePtr<IPRImageHelper> ptrImageHelper((static_cast<IPRImageHelper*> (CreateObject(kDCNProductImageIFaceBoss ,IPRImageHelper::kDefaultIID))));
    if(ptrImageHelper == nil)
    {
		//CA("ProductImages plugin not found ");
		return;
    }
	InterfacePtr<ILoginHelper> ptrLogInHelper((static_cast<ILoginHelper*> (CreateObject(kLNGLoginHelperBoss , IID_ILOGINHELPER /*ILoginHelper::kDefaultIID*/))));
	if(ptrLogInHelper == nil)
	{
		//CAlert::InformationAlert("Pointer to ptrLogInHelper is nil.");
		return ;
	}
	
	LoginInfoValue cserverInfoValue ;
	bool16 result = ptrLogInHelper->getCurrentServerInfo(cserverInfoValue) ;

	ClientInfoValue clientInfoObj = ptrLogInHelper->getCurrentClientInfoValue();
	int32 disPartnerImgVal = clientInfoObj.getDisplayPartnerImages();
	int32 disPickListImgVal = clientInfoObj.getDisplayPickListImages();


	if(/*disPartnerImgVal  == 1*/isShowPartnerImages == kTrue)
	{
		//isShowPartnerImages = kTrue;
		dlgController->SetTriStateControlData(kDisplayPartnerImagesCheckBoxWidgetID,kTrue, nil, kTrue, kFalse);
		if(isProd == 1)
			ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
		else
			ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);
	}
	else
	{
		//isShowPartnerImages = kFalse;
		dlgController->SetTriStateControlData(kDisplayPartnerImagesCheckBoxWidgetID,kFalse, nil, kTrue, kFalse);
	}

	if(/*disPickListImgVal  == 1*/ isShowPVImages == kTrue)
	{
		//isShowPVImages = kTrue;
		dlgController->SetTriStateControlData(kDisplayPickListImagesCheckBoxWidgetID,kTrue, nil, kTrue, kFalse);
		if(isProd == 1)
			ptrImageHelper->showProductImages(objId , langaugeId , parId , parTypeId , sectId) ;
		else
			ptrImageHelper->showItemImages(objId , langaugeId , parId , parTypeId , sectId, kFalse);
	}
	else
	{
		//isShowPVImages = kFalse;
		dlgController->SetTriStateControlData(kDisplayPickListImagesCheckBoxWidgetID,kFalse, nil, kTrue, kFalse);
	}
	//----------

	/*if(isShowPartnerImages == kTrue )
	{
		dlgController->SetTriStateControlData(kDisplayPartnerImagesCheckBoxWidgetID,kTrue, nil, kTrue, kFalse);
	}
	else
		dlgController->SetTriStateControlData(kDisplayPartnerImagesCheckBoxWidgetID,kFalse, nil, kTrue, kFalse);


	if(isShowPVImages  == kTrue)
	{
		dlgController->SetTriStateControlData(kDisplayPickListImagesCheckBoxWidgetID,kTrue, nil, kTrue, kFalse);
	}
	else
		dlgController->SetTriStateControlData(kDisplayPickListImagesCheckBoxWidgetID,kFalse, nil, kTrue, kFalse);*/

}

/* ValidateFields
*/
WidgetID DCNFilterDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.   
	return result;
}

/* ApplyFields
*/
void DCNFilterDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	// TODO add code that gathers widget values and applies them.
}

//  Code generated by DollyXs code generator

#ifndef _ITextMiscellanySuite_
#define _ITextMiscellanySuite_

// Interface includes:
#include "IPMUnknown.h"
#include "DCNID.h"
//#include "ISpecifier.h"


class ITextMiscellanySuite : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_DCNITEXTMISCELLANYSUITEE };
public:
	//virtual bool16 GetCurrentSpecifier(ISpecifier * & )=0;
	virtual bool16 GetUidList(UIDList &)=0;
	virtual bool16 setFrameUser(int32)=0;
};

#endif // _ITextMiscellanySuite_

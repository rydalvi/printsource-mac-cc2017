
#include "VCPlugInHeaders.h"

// Interface includes:
#include "IXLibraryButtonData.h"

// General includes:
#include "CPMUnknown.h"

// Project includes:
#include "DCNID.h"


class XLibraryButtonData : public CPMUnknown <IXLibraryButtonData>
{
public:

	XLibraryButtonData(IPMUnknown* boss) : CPMUnknown<IXLibraryButtonData>(boss) {}
	
	void			SetName(PMString& theName);
	PMString&		GetName();

	void		SetFileSize(int32& size);
	int32&		GetFileSize();
};

CREATE_PMINTERFACE(XLibraryButtonData, kXLibraryButtonDataImpl)

void	XLibraryButtonData::SetName(PMString& theName)
{
	fName.SetString( theName );
}

PMString&	 XLibraryButtonData::GetName()
{
	return fName;
}

void	XLibraryButtonData::SetFileSize(int32& size)
{
	fSize = size;
}

int32&	 XLibraryButtonData::GetFileSize()
{
	return fSize;
}




#pragma once
#ifndef	__IXLibraryButtonData__
#define	__IXLibraryButtonData__

class IXLibraryButtonData : public IPMUnknown
{
public:
	virtual void		SetName(PMString& theName) = 0;
	virtual PMString&	GetName() = 0;
	
	virtual void		SetFileSize(int32& size) = 0;
	virtual int32&		GetFileSize() = 0;
protected:

	PMString			fName;
	int32				fSize;
};

#endif
//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
// General includes:
#include "CDialogController.h"
#include "IAppFramework.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"
#include "Vector"
// Project includes:
#include "PUPID.h"
#include "AttrData.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextControlData.h"
#include "GlobalData.h"

#include "IClientOptions.h"
#include "SDKUtilities.h"
#include "PUPDialogController.h"
#include "FileUtils.h"
#include "CAlert.h"
#include "PUPDialogController.h"
extern PMString gActiveBookName;
//#define CA(X) CAlert::InformationAlert(X)
inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("PUPDialogObserver.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)

//#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

extern bool16 ISFirstDialog ;
extern bool16 IsChangePriceModeSelected;
IControlView* Fist1ScreenView= nil;
IControlView* Second2ScreenView = nil;

IControlView* PriceComboControlView = nil;
IControlView* EventListControlView = nil;
IControlView* OldPriceComboView = nil;
IControlView* NewPriceComboView = nil;
/** PUPDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.
*/

	
CREATE_PMINTERFACE(PUPDialogController, kPUPDialogControllerImpl)

/* ApplyFields
*/
void PUPDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	//CA("PUPDialogController::InitializeDialogFields");
	CDialogController::InitializeDialogFields(dlgContext);
	// Put code to initialize widget values here.

	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil)
	{
		CA("panelControlData == nil");
		return;
	}
	IControlView* FirstScreenView = panelControlData->FindWidget(kFirstScreenWidgetID);
	if(!FirstScreenView)
	{		CA("FirstScreenView == nil");
		return;
	}

	Fist1ScreenView = FirstScreenView;

	IControlView* SecondScreenView = panelControlData->FindWidget(kSecondScreenWidgetID);
	if(!SecondScreenView)
	{		CA("SecondScreenView == nil");
		return;
	}

	Second2ScreenView = SecondScreenView;

	if(ISFirstDialog == kFalse)
	{
		//CA("ISFirstDialog == kFalse");
		FirstScreenView->Disable();
		FirstScreenView->HideView();
		SecondScreenView->ShowView();	
		SecondScreenView->Enable();
		IControlView* TextWidget = panelControlData->FindWidget(kTotalDocWidgetID);
		if(!TextWidget)
		{		CA("TextWidget == nil");
		}
		else
		{	
			InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
			if(!textcontrol)
			{
				CA("textcontrol == nil");
				return;
			}
			
			PMString text;
			text.AppendNumber(GlobalData::TotalDoc);
			text.SetTranslatable(kFalse);
            text.ParseForEmbeddedCharacters();
			textcontrol->SetString(text);
			//CA(text);
			
		}
// 
////////////////////////////
		IControlView* TextWidget1 = panelControlData->FindWidget(kItemsRefreshedWidgetID);
		if(!TextWidget1)
		{
			CA("TextWidget1 == nil");
		}
		else
		{
			InterfacePtr<ITextControlData> textcontrol(TextWidget1,UseDefaultIID());
			if(!textcontrol)
			{
				CA("textcontrol == nil");
				return;
			}
			
			PMString text;
			text.AppendNumber((int32)GlobalData::TotalItemList.size()); //Cs4
			text.SetTranslatable(kFalse);
            text.ParseForEmbeddedCharacters();
			textcontrol->SetString(text);
//CA("GlobalData::TotalItemList.size() = "+text);
		}

		IControlView* TextWidget2 = panelControlData->FindWidget(klogPathWidgetID);
		if(!TextWidget2)
		{
			CA("TextWidget1 == nil");
		}
		else
		{
			InterfacePtr<ITextControlData> textcontrol(TextWidget2,UseDefaultIID());
			if(!textcontrol)
			{
				CA("textcontrol == nil");
				return;
			}
			
			PMString ExpFilePath("");

			if(IsChangePriceModeSelected)
			{
				//SDKUtilities::GetApplicationFolder(ExpFilePath);
                FileUtils::GetAppFolder(&ExpFilePath);
                #ifdef MACINTOSH
                    // Result path = Macintosh HD:Applications:Adobe InDesign CC Debug:Adobe InDesign CC.app:Contents:MacOS
                    // so removing last 3 folder names as actual name is till Macintosh HD:Applications:Adobe InDesign CC Debug
                    SDKUtilities::RemoveLastElement(ExpFilePath);
                    SDKUtilities::RemoveLastElement(ExpFilePath);
                    SDKUtilities::RemoveLastElement(ExpFilePath);
                #endif

                
		
				PMString pluginFolderStr("");
				PMString tempString;
				// appending '\'
				#ifdef MACINTOSH
					pluginFolderStr+=":";
				#else
					pluginFolderStr+="\\";
				#endif
					// appending folder name
					pluginFolderStr+="Plug-ins";

					// appending '\'
				#ifdef MACINTOSH
					pluginFolderStr+=":";
				#else
					pluginFolderStr+="\\";
				#endif
					// appending file name //***Commented By Sachin Sharma
					pluginFolderStr+=gActiveBookName/*GlobalData::skuDataList[0].Date*/; //file name "Vinit.pp";

				ExpFilePath+=pluginFolderStr;
				ExpFilePath.Append(".SKU.txt");
				PMString text("Change log : ");
				text.SetTranslatable(kFalse);
				text.Append(ExpFilePath);
				text.SetTranslatable(kFalse);
                text.ParseForEmbeddedCharacters();
				textcontrol->SetString(text);
			}
			else
			{
				ExpFilePath.SetTranslatable(kFalse);
                ExpFilePath.ParseForEmbeddedCharacters();
				textcontrol->SetString(ExpFilePath);
			}
		}
		
		IControlView* TextWidget3 = panelControlData->FindWidget(kTotalitemsfoundWidgetID);
		if(!TextWidget3)
		{
			CA("TextWidget1 == nil");
		}
		else
		{
			InterfacePtr<ITextControlData> textcontrol(TextWidget3,UseDefaultIID());
			if(!textcontrol)
			{
				CA("textcontrol == nil");
				return;
			}
			
			PMString text;
			text.AppendNumber((int32)GlobalData::TotalItemPresentList.size()); //Cs4
			text.SetTranslatable(kFalse);
            text.ParseForEmbeddedCharacters();
			textcontrol->SetString(text);
			//CA(text);
		}
////////////////////////
	//	IControlView* TextWidget1 = panelControlData->FindWidget(kTotalProductWidgetID);
	//	if(!TextWidget1)
	//	{
	//		CA("TextWidget == nil");
	//	}
	//	else
	//	{	
	//		InterfacePtr<ITextControlData> textcontrol(TextWidget1,UseDefaultIID());
	//		if(!textcontrol)
	//		{
	//			CA("textcontrol == nil");
	//			return;
	//		}
	//		
	//		PMString text;
	//		text.AppendNumber(GlobalData::ProductList.size());
	//		textcontrol->SetString(text);
	//	
	//	}
		
	//	IControlView* TextWidget3 = panelControlData->FindWidget(kTotalItemWidgetID);
	//	if(!TextWidget3)
	//	{
	//		CA("TextWidget3 == nil");
	//	}
	//	else
	//	{	
	//		InterfacePtr<ITextControlData> textcontrol(TextWidget3,UseDefaultIID());
	//		if(!textcontrol)
	//		{
	//			CA("textcontrol == nil");
	//			return;
	//		}
	//		
	//		PMString text;
	//		text.AppendNumber(GlobalData::TotalItemList.size());
	//		textcontrol->SetString(text);
	//	
	//	}

	//	IControlView* TextWidget2 = panelControlData->FindWidget(kTotalPriceAttributeWidgetID);
	//	if(!TextWidget2)
	//	{
	//		CA("TextWidget == nil");
	//	}
	//	else
	//	{	
	//		InterfacePtr<ITextControlData> textcontrol(TextWidget2,UseDefaultIID());
	//		if(!textcontrol)
	//		{
	//			CA("textcontrol == nil");
	//			return;
	//		}			
	//		PMString text;
	//		text.AppendNumber(GlobalData::skuDataList.size());
	//		textcontrol->SetString(text);
	//		
	//	}

	//	IControlView* TextWidget4 = panelControlData->FindWidget(kTotalUnChangedItemPriceWidgetID);
	//	if(!TextWidget4)
	//	{
	//		CA("TextWidget4 == nil");
	//	}
	//	else
	//	{	
	//		InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
	//		if(!textcontrol)
	//		{
	//			CA("textcontrol == nil");
	//			return;
	//		}
	//		
	//		PMString text;
	//		int32 TotalUNChangeItem = GlobalData::TotalItemList.size() - GlobalData::skuDataList.size(); 
	//		text.AppendNumber(TotalUNChangeItem);
	//		textcontrol->SetString(text);
	//	
	//	}
	}
	else if(ISFirstDialog == kTrue)
	{
		SecondScreenView->Disable();
		SecondScreenView->HideView();
		FirstScreenView->ShowView();
		FirstScreenView->Enable();
		//this->SetTriStateControlData(kPUPAllDocCheckBoxWidgetID,kTrue);
		IControlView* RDbtnRefreshPriceView = panelControlData->FindWidget(kRediobtnRefreshPriceID);
		InterfacePtr<ITriStateControlData> RDbtnRefreshPriceControl(RDbtnRefreshPriceView,IID_ITRISTATECONTROLDATA);
		if(RDbtnRefreshPriceControl == nil)
			return;

		IControlView* RDbtnChangePriceView = panelControlData->FindWidget(kRediobtnChangePriceID);
		InterfacePtr<ITriStateControlData> RDbtnChangePriceControl(RDbtnChangePriceView,IID_ITRISTATECONTROLDATA);
		if(RDbtnChangePriceControl == nil)
			return;

		IControlView* DropdwnRefreshPriceView = panelControlData->FindWidget(kPUPAttributeComboBoxWidgetID);
		if(DropdwnRefreshPriceView == nil)
			return;
		PriceComboControlView = DropdwnRefreshPriceView;
        
        IControlView* DropdwnEventListView = panelControlData->FindWidget(kPUPEventListComboBoxWidgetID);
		if(DropdwnEventListView == nil)
			return;
		EventListControlView = DropdwnEventListView;

		IControlView* DropdwnOldPriceView = panelControlData->FindWidget(kPUPOldAttributeComboBoxWidgetID);
		if(DropdwnOldPriceView == nil)
			return;
		OldPriceComboView = DropdwnOldPriceView;

		IControlView* DropdwnNewPriceView = panelControlData->FindWidget(kPUPNewAttributeComboBoxWidgetID);
		if(DropdwnNewPriceView == nil)
			return;		
		NewPriceComboView = DropdwnNewPriceView;

		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;
        
        InterfacePtr<IStringListControlData> eventDropListData(this->QueryListControlDataInterface(kPUPEventListComboBoxWidgetID));
		if(eventDropListData==nil)
			return;
        
		InterfacePtr<IDropDownListController> eventDropListController(eventDropListData, UseDefaultIID());
		if(eventDropListController == nil)
			return;
		
		InterfacePtr<IStringListControlData> sectionDropListData(this->QueryListControlDataInterface(kPUPAttributeComboBoxWidgetID));
		if(sectionDropListData==nil)
			return;

		InterfacePtr<IDropDownListController> sectionDropListController(sectionDropListData, UseDefaultIID());
		if(sectionDropListController == nil)
			return;

		InterfacePtr<IStringListControlData> OldPriceDropListData(this->QueryListControlDataInterface(kPUPOldAttributeComboBoxWidgetID));
		if(OldPriceDropListData==nil)
			return;

		InterfacePtr<IDropDownListController> OldPriceDropListController(OldPriceDropListData, UseDefaultIID());
		if(OldPriceDropListController == nil)
			return;

		InterfacePtr<IStringListControlData> NewPriceDropListData(this->QueryListControlDataInterface(kPUPNewAttributeComboBoxWidgetID));
		if(NewPriceDropListData==nil)
			return;

		InterfacePtr<IDropDownListController> NewPriceDropListController(NewPriceDropListData, UseDefaultIID());
		if(NewPriceDropListController == nil)
			return;
		
        InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			CAlert::ErrorAlert("Interface for IClientOptions not found.");
			return;
		}
        
        PMString dummylangId("");
		double defaultLanguageID = ptrIClientOptions->getDefaultLocale(dummylangId);

        eventDropListData->Clear(kFalse, kFalse);
        PMString eventStr("--Select Event To Refresh--");
        eventStr.SetTranslatable(kFalse);
        eventDropListData->AddString(eventStr);
        
        //Populating Event List
        VectorPubModelPtr vectorEventListptr=  ptrIAppFramework->ProjectCache_getAllProjects(1); // 1 default for English
        if(vectorEventListptr==nil)
		{
			ptrIAppFramework->LogError("AP46_PriceUpdate::PUPDialogController::InitializeDialogFields::ClassificationTree_getRoot's vectorEventListptr==nil");
			return;
		}
        
        VectorPubModel::iterator it1;
        for(it1=vectorEventListptr->begin(); it1!=vectorEventListptr->end(); it1++)
        {
            if(it1->getPubTypeId() != -1)
            {
                PMString pubname=it1->getName();
                pubname.SetTranslatable(kFalse);
                pubname.ParseForEmbeddedCharacters();
                eventDropListData->AddString(pubname);
            }
        }
        
        
        AttributeData attrdata;
		attrdata.clearAttrVector();
        
		sectionDropListData->Clear(kFalse, kFalse);
		OldPriceDropListData->Clear(kFalse, kFalse);
		NewPriceDropListData->Clear(kFalse, kFalse);
		PMString str("--Select Event Specific Fields--");

		str.SetTranslatable(kFalse);
		sectionDropListData->AddString(str);
		OldPriceDropListData->AddString(str);
		NewPriceDropListData->AddString(str);
		
        attrdata.setAttrVectorInfo(-1,"", "" , 0);
		
		VectorClassInfoPtr vectClassInfoValuePtr = ptrIAppFramework->ClassificationTree_getRoot(1); // 1 default for English
		if(vectClassInfoValuePtr==nil)
		{		
			ptrIAppFramework->LogError("AP46_PriceUpdate::PUPDialogController::InitializeDialogFields::ClassificationTree_getRoot's vectClassInfoValuePtr==nil");
			return;
		}
		VectorClassInfoValue::iterator it;
		it=vectClassInfoValuePtr->begin();
		double CurrentClassID = it->getClass_id();

		VectorAttributeInfoPtr AttrInfoVectPtr = ptrIAppFramework->StructureCache_getItemAttributesForClassAndParents(CurrentClassID,defaultLanguageID);
		if(AttrInfoVectPtr)
		{
			VectorAttributeInfoValue::iterator it;
			for(it=AttrInfoVectPtr->begin(); it!=AttrInfoVectPtr->end(); it++)
			{
                if(it->getIsEventSpecific())
                {
                    double sectid=it->getAttributeId();
                    PMString pubname=it->getDisplayName();
                    PMString table_col("ATTRID_");
                    table_col.AppendNumber(PMReal(it->getAttributeId()));//temp_to_test =it->getTable_col();
                    attrdata.setAttrVectorInfo(sectid,pubname,table_col,it->getAttributeId());

                    pubname.SetTranslatable(kFalse);
                    pubname.ParseForEmbeddedCharacters();
                    sectionDropListData->AddString(pubname);
                    OldPriceDropListData->AddString(pubname);
                    NewPriceDropListData->AddString(pubname);
                }
			}
            
            PMString str("- - - - - - - - - - - - - - -");
            str.SetTranslatable(kFalse);
            sectionDropListData->AddString(str);
            OldPriceDropListData->AddString(str);
            NewPriceDropListData->AddString(str);
            attrdata.setAttrVectorInfo(-1,"", "" , 0);
            
            PMString str1("--Select Global Fields--");
            str1.SetTranslatable(kFalse);
            sectionDropListData->AddString(str1);
            OldPriceDropListData->AddString(str1);
            NewPriceDropListData->AddString(str1);
            attrdata.setAttrVectorInfo(-1,"", "" , 0);

            
            for(it=AttrInfoVectPtr->begin(); it!=AttrInfoVectPtr->end(); it++)
			{
                if(!(it->getIsEventSpecific()))
                {
                    double sectid=it->getAttributeId();
                    PMString pubname=it->getDisplayName();
                    PMString table_col("ATTRID_");
                    table_col.AppendNumber(PMReal(it->getAttributeId()));//temp_to_test =it->getTable_col();
                    attrdata.setAttrVectorInfo(sectid,pubname,table_col,it->getAttributeId());
                    
                    pubname.SetTranslatable(kFalse);
                    pubname.ParseForEmbeddedCharacters();
                    sectionDropListData->AddString(pubname);
                    OldPriceDropListData->AddString(pubname);
                    NewPriceDropListData->AddString(pubname);
                }
			}
            
			if(AttrInfoVectPtr)
				delete AttrInfoVectPtr;

		}
        
        eventDropListController->Select(0);
		sectionDropListController->Select(0);	
		OldPriceDropListController->Select(0);
		NewPriceDropListController->Select(0);

		//RDbtnRefreshPriceControl->Select();
		RDbtnRefreshPriceControl->SetState(ITriStateControlData::kSelected,kTrue,kFalse);
		RDbtnChangePriceControl->SetState(ITriStateControlData::kUnselected,kTrue,kFalse);
		DropdwnRefreshPriceView->Enable();
        DropdwnEventListView->Enable();

		////RDbtnChangePriceControl->Deselect();
		DropdwnOldPriceView->Disable();
		DropdwnNewPriceView->Disable();
		IsChangePriceModeSelected = kFalse;

	}
}

/* ValidateFields
*/
WidgetID PUPDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID result = CDialogController::ValidateDialogFields(myContext);
	// Put code to validate widget values here.
	return result;
}

/* ApplyFields
*/
void PUPDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	// TODO add code that gathers widget values and applies them.
}

void PUPDialogController::CallDialogInitaliser(bool16 ISFirstDialog)
{
	//CA("PUPDialogController::CallDialogInitaliser");
	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	if (panelControlData == nil)
	{
		CA("panelControlData == nil");
		return;
	}
	IControlView* FirstScreenView = panelControlData->FindWidget(kFirstScreenWidgetID);
	if(!FirstScreenView)
	{		CA("FirstScreenView == nil");
		return;
	}

	Fist1ScreenView = FirstScreenView;
	IControlView* SecondScreenView = panelControlData->FindWidget(kSecondScreenWidgetID);
	if(!SecondScreenView)
	{		CA("SecondScreenView == nil");
		return;
	}

	Second2ScreenView = SecondScreenView;
	if(ISFirstDialog == kFalse)
	{	
		//CA("ISFirstDialog == kFalse");
		//CA("inside CallDialogInitialiser For Second Screen");
		FirstScreenView->Disable();
		FirstScreenView->HideView();
		SecondScreenView->ShowView();	
		SecondScreenView->Enable();
		IControlView* TextWidget = panelControlData->FindWidget(kTotalDocWidgetID);
		if(!TextWidget)
		{		CA("TextWidget == nil");
		}
		else
		{	
			InterfacePtr<ITextControlData> textcontrol(TextWidget,UseDefaultIID());
			if(!textcontrol)
			{
				CA("textcontrol == nil");
				return;
			}
			
			PMString text;
			text.AppendNumber(GlobalData::TotalDoc);
			text.SetTranslatable(kFalse);
            text.ParseForEmbeddedCharacters();
			textcontrol->SetString(text);
			
		}
/////////////////////////////////////////	
		IControlView* TextWidget1 = panelControlData->FindWidget(kItemsRefreshedWidgetID);
		if(!TextWidget1)
		{
			CA("TextWidget1 == nil");
		}
		else
		{
			InterfacePtr<ITextControlData> textcontrol(TextWidget1,UseDefaultIID());
			if(!textcontrol)
			{
				CA("textcontrol == nil");
				return;
			}
			
			PMString text;
			text.AppendNumber((int32)GlobalData::TotalItemList.size()); //Cs4
			text.SetTranslatable(kFalse);
            text.ParseForEmbeddedCharacters();
			textcontrol->SetString(text);

		}


///////////////////////////////////
	//	IControlView* TextWidget1 = panelControlData->FindWidget(kTotalProductWidgetID);
	//	if(!TextWidget1)
	//	{
	//		CA("TextWidget == nil");
	//	}
	//	else
	//	{	
	//		InterfacePtr<ITextControlData> textcontrol(TextWidget1,UseDefaultIID());
	//		if(!textcontrol)
	//		{
	//			CA("textcontrol == nil");
	//			return;
	//		}
	//		
	//		PMString text;
	//		text.AppendNumber(GlobalData::ProductList.size());
	//		textcontrol->SetString(text);
	//	
	//	}
		
	//	IControlView* TextWidget3 = panelControlData->FindWidget(kTotalItemWidgetID);
	//	if(!TextWidget3)
	//	{
	//		CA("TextWidget3 == nil");
	//	}
	//	else
	//	{	
	//		InterfacePtr<ITextControlData> textcontrol(TextWidget3,UseDefaultIID());
	//		if(!textcontrol)
	//		{
	//			CA("textcontrol == nil");
	//			return;
	//		}
	//		
	//		PMString text;
	//		text.AppendNumber(GlobalData::TotalItemList.size());
	//		textcontrol->SetString(text);
	//	
	//	}

	//	IControlView* TextWidget2 = panelControlData->FindWidget(kTotalPriceAttributeWidgetID);
	//	if(!TextWidget2)
	//	{
	//		CA("TextWidget == nil");
	//	}
	//	else
	//	{	
	//		InterfacePtr<ITextControlData> textcontrol(TextWidget2,UseDefaultIID());
	//		if(!textcontrol)
	//		{
	//			CA("textcontrol == nil");
	//			return;
	//		}			
	//		PMString text;
	//		text.AppendNumber(GlobalData::skuDataList.size());
	//		textcontrol->SetString(text);
	//		
	//	}

	//	IControlView* TextWidget4 = panelControlData->FindWidget(kTotalUnChangedItemPriceWidgetID);
	//	if(!TextWidget4)
	//	{
	//		CA("TextWidget4 == nil");
	//	}
	//	else
	//	{	
	//		InterfacePtr<ITextControlData> textcontrol(TextWidget4,UseDefaultIID());
	//		if(!textcontrol)
	//		{
	//			CA("textcontrol == nil");
	//			return;
	//		}
	//		
	//		PMString text;
	//		int32 TotalUNChangeItem = GlobalData::TotalItemList.size() - GlobalData::skuDataList.size(); 
	//		text.AppendNumber(TotalUNChangeItem);
	//		textcontrol->SetString(text);
	//	
	//	}
	}
	else if(ISFirstDialog == kTrue)
	{	
		//CA("inside CallDialogInitialiser For First Screen");
		SecondScreenView->Disable();
		SecondScreenView->HideView();
		FirstScreenView->ShowView();
		FirstScreenView->Enable();
		//this->SetTriStateControlData(kPUPAllDocCheckBoxWidgetID,kTrue);
		IControlView* RDbtnRefreshPriceView = panelControlData->FindWidget(kRediobtnRefreshPriceID);
		InterfacePtr<ITriStateControlData> RDbtnRefreshPriceControl(RDbtnRefreshPriceView,IID_ITRISTATECONTROLDATA);
		if(RDbtnRefreshPriceControl == nil)
			return;

		IControlView* RDbtnChangePriceView = panelControlData->FindWidget(kRediobtnChangePriceID);
		InterfacePtr<ITriStateControlData> RDbtnChangePriceControl(RDbtnChangePriceView,IID_ITRISTATECONTROLDATA);
		if(RDbtnChangePriceControl == nil)
			return;

		IControlView* DropdwnRefreshPriceView = panelControlData->FindWidget(kPUPAttributeComboBoxWidgetID);
		if(DropdwnRefreshPriceView == nil)
			return;

		IControlView* DropdwnOldPriceView = panelControlData->FindWidget(kPUPOldAttributeComboBoxWidgetID);
		if(DropdwnOldPriceView == nil)
			return;

		IControlView* DropdwnNewPriceView = panelControlData->FindWidget(kPUPNewAttributeComboBoxWidgetID);
		if(DropdwnNewPriceView == nil)
			return;		

		InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
		if(ptrIAppFramework == nil)
			return;
		
		InterfacePtr<IStringListControlData> sectionDropListData(this->QueryListControlDataInterface(kPUPAttributeComboBoxWidgetID));
		if(sectionDropListData==nil)
			return;

		InterfacePtr<IDropDownListController> sectionDropListController(sectionDropListData, UseDefaultIID());
		if(sectionDropListController == nil)
			return;

		InterfacePtr<IStringListControlData> OldPriceDropListData(this->QueryListControlDataInterface(kPUPOldAttributeComboBoxWidgetID));
		if(OldPriceDropListData==nil)
			return;

		InterfacePtr<IDropDownListController> OldPriceDropListController(OldPriceDropListData, UseDefaultIID());
		if(OldPriceDropListController == nil)
			return;

		InterfacePtr<IStringListControlData> NewPriceDropListData(this->QueryListControlDataInterface(kPUPNewAttributeComboBoxWidgetID));
		if(NewPriceDropListData==nil)
			return;

		InterfacePtr<IDropDownListController> NewPriceDropListController(NewPriceDropListData, UseDefaultIID());
		if(NewPriceDropListController == nil)
			return;
			
		sectionDropListData->Clear(kFalse, kFalse);
		PMString str("--Select--");
		str.SetTranslatable(kFalse);
		sectionDropListData->AddString(str);
		OldPriceDropListData->AddString(str);
		NewPriceDropListData->AddString(str);		

		AttributeData attrdata;
		attrdata.clearAttrVector();
	
		// for default language ID

		InterfacePtr<IClientOptions> ptrIClientOptions((static_cast<IClientOptions*> (CreateObject(kClientOptionsReaderBoss,IClientOptions::kDefaultIID))));
		if(ptrIClientOptions==nil)
		{
			CAlert::ErrorAlert("Interface for IClientOptions not found.");
			return;
		}

        PMString dummyLangId("");
		double defaultLanguageID = ptrIClientOptions->getDefaultLocale(dummyLangId);


		
		VectorAttributeInfoPtr AttrInfoVectPtr = ptrIAppFramework->/*AttributeCache_getItemAttributes*/StructureCache_getAllItemAttributes(defaultLanguageID);
		//VectorAttributeInfoPtr AttrInfoVectPtr=nil ;//temp_to_test ptrIAppFramework->ATTRIBMngr_getCoreAttributes();		

		if(AttrInfoVectPtr)
		{
			VectorAttributeInfoValue::iterator it;
			for(it=AttrInfoVectPtr->begin(); it!=AttrInfoVectPtr->end(); it++)
			{
				double sectid=it->getAttributeId();
				PMString pubname=it->getDisplayName();
				PMString table_col("");//temp_to_test =it->getTable_col();
				attrdata.setAttrVectorInfo(sectid,pubname,table_col,it->getAttributeId());
				pubname.SetTranslatable(kFalse);
                pubname.ParseForEmbeddedCharacters();
				sectionDropListData->AddString(pubname);
				OldPriceDropListData->AddString(pubname);
				NewPriceDropListData->AddString(pubname);
				//PMString as;
				//as.AppendNumber(it->getAttributeId());
				//CA(as);
			}
//12-april
			if(AttrInfoVectPtr)
				delete AttrInfoVectPtr;
//12-april
		}
		sectionDropListController->Select(0);	
		OldPriceDropListController->Select(0);
		NewPriceDropListController->Select(0);

		RDbtnRefreshPriceControl->Select();
		DropdwnRefreshPriceView->Enable();

		//RDbtnChangePriceControl->Deselect();
		DropdwnOldPriceView->Disable();
		DropdwnNewPriceView->Disable();
		IsChangePriceModeSelected = kFalse;
	}

}
//  Code generated by DollyXS code generator

#include "VCPlugInHeaders.h"
#include "CommonFunctions.h"
#include "GlobalData.h"

#include "IAppFramework.h"
#include "ISpecialChar.h"

#include "MetaDataTypes.h"
#include "CTUnicodeTranslator.h"
#include "TextIterator.h"
#include "CAlert.h"
#include "ITextModelCmds.h"


inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}

#define CA(X) CAlert::InformationAlert \
	( \
		PMString("CommonFunctions.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)

#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

#include "IXMLAttributeCommands.h"
void collectAllXMLTagAttributeValue(IIDXMLElement *& tagXMLElementPtr,XMLTagAttributeValue & tagAttrVal)
{
	tagAttrVal.ID = tagXMLElementPtr->GetAttributeValue(WideString("ID"));
	tagAttrVal.typeId = tagXMLElementPtr->GetAttributeValue(WideString("typeId"));
	tagAttrVal.header = tagXMLElementPtr->GetAttributeValue(WideString("header"));
	tagAttrVal.isEventField = tagXMLElementPtr->GetAttributeValue(WideString("isEventField"));
	tagAttrVal.deleteIfEmpty = tagXMLElementPtr->GetAttributeValue(WideString("deleteIfEmpty"));
	tagAttrVal.dataType = tagXMLElementPtr->GetAttributeValue(WideString("dataType"));
	tagAttrVal.isAutoResize = tagXMLElementPtr->GetAttributeValue(WideString("isAutoResize"));
	tagAttrVal.LanguageID = tagXMLElementPtr->GetAttributeValue(WideString("LanguageID"));
	tagAttrVal.index = tagXMLElementPtr->GetAttributeValue(WideString("index"));
	tagAttrVal.pbObjectId = tagXMLElementPtr->GetAttributeValue(WideString("pbObjectId"));
	tagAttrVal.parentID = tagXMLElementPtr->GetAttributeValue(WideString("parentID"));
	tagAttrVal.childId = tagXMLElementPtr->GetAttributeValue(WideString("childId"));
	tagAttrVal.sectionID = tagXMLElementPtr->GetAttributeValue(WideString("sectionID"));
	tagAttrVal.parentTypeID = tagXMLElementPtr->GetAttributeValue(WideString("parentTypeID"));
	tagAttrVal.isSprayItemPerFrame = tagXMLElementPtr->GetAttributeValue(WideString("isSprayItemPerFrame"));
	tagAttrVal.catLevel = tagXMLElementPtr->GetAttributeValue(WideString("catLevel"));
	tagAttrVal.imgFlag = tagXMLElementPtr->GetAttributeValue(WideString("imgFlag"));
	tagAttrVal.imageIndex = tagXMLElementPtr->GetAttributeValue(WideString("imageIndex"));
	tagAttrVal.flowDir = tagXMLElementPtr->GetAttributeValue(WideString("flowDir"));
	tagAttrVal.childTag = tagXMLElementPtr->GetAttributeValue(WideString("childTag"));
	tagAttrVal.tableFlag = tagXMLElementPtr->GetAttributeValue(WideString("tableFlag"));
	tagAttrVal.tableType = tagXMLElementPtr->GetAttributeValue(WideString("tableType"));
	tagAttrVal.tableId = tagXMLElementPtr->GetAttributeValue(WideString("tableId"));
	
	
	
	tagAttrVal.rowno = tagXMLElementPtr->GetAttributeValue(WideString("rowno"));
	tagAttrVal.colno = tagXMLElementPtr->GetAttributeValue(WideString("colno"));
}

void setAllXMLTagAttributeValue(IIDXMLElement *& tagXMLElementPtr,XMLTagAttributeValue & tagAttrVal)
	{
		tagXMLElementPtr->SetAttributeValue(WideString("ID"),WideString(tagAttrVal.ID)); 
		tagXMLElementPtr->SetAttributeValue(WideString("typeId"),WideString(tagAttrVal.typeId));
		tagXMLElementPtr->SetAttributeValue(WideString("isEventField"),WideString(tagAttrVal.isEventField));
		tagXMLElementPtr->SetAttributeValue(WideString("deleteIfEmpty"),WideString(tagAttrVal.deleteIfEmpty));
		tagXMLElementPtr->SetAttributeValue(WideString("dataType"),WideString(tagAttrVal.dataType));
		tagXMLElementPtr->SetAttributeValue(WideString("isAutoResize"),WideString(tagAttrVal.isAutoResize));
		tagXMLElementPtr->SetAttributeValue(WideString("LanguageID"),WideString(tagAttrVal.LanguageID));
		tagXMLElementPtr->SetAttributeValue(WideString("index"),WideString(tagAttrVal.index));
		tagXMLElementPtr->SetAttributeValue(WideString("pbObjectId"),WideString(tagAttrVal.pbObjectId));
		tagXMLElementPtr->SetAttributeValue(WideString("parentID"),WideString(tagAttrVal.parentID));
		tagXMLElementPtr->SetAttributeValue(WideString("childId"),WideString(tagAttrVal.childId));
		tagXMLElementPtr->SetAttributeValue(WideString("sectionID"),WideString(tagAttrVal.sectionID));
		tagXMLElementPtr->SetAttributeValue(WideString("parentTypeID"),WideString(tagAttrVal.parentTypeID));
		tagXMLElementPtr->SetAttributeValue(WideString("isSprayItemPerFrame"),WideString(tagAttrVal.isSprayItemPerFrame));
		tagXMLElementPtr->SetAttributeValue(WideString("catLevel"),WideString(tagAttrVal.catLevel));
		tagXMLElementPtr->SetAttributeValue(WideString("imgFlag"),WideString(tagAttrVal.imgFlag));
		tagXMLElementPtr->SetAttributeValue(WideString("imageIndex"),WideString(tagAttrVal.imageIndex));
		tagXMLElementPtr->SetAttributeValue(WideString("flowDir"),WideString(tagAttrVal.flowDir));
		tagXMLElementPtr->SetAttributeValue(WideString("childTag"),WideString(tagAttrVal.childTag));
		tagXMLElementPtr->SetAttributeValue(WideString("tableFlag"),WideString(tagAttrVal.tableFlag));
		tagXMLElementPtr->SetAttributeValue(WideString("tableType"),WideString(tagAttrVal.tableType));
		tagXMLElementPtr->SetAttributeValue(WideString("tableId"),WideString(tagAttrVal.tableId));	
		
		tagXMLElementPtr->SetAttributeValue(WideString("rowno"),WideString(tagAttrVal.rowno));
		tagXMLElementPtr->SetAttributeValue(WideString("colno"),WideString(tagAttrVal.colno));
	}

void createPRINTsourceXMLTagAttributes(IIDXMLElement *& tagXMLElementPtr,XMLTagAttributeValue & tagAttrVal)
	{
		XMLReference xmlRef = tagXMLElementPtr->GetXMLReference();	 	
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("ID"),WideString(tagAttrVal.ID)); 
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("typeId"),WideString(tagAttrVal.typeId));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("isEventField"),WideString(tagAttrVal.isEventField));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("deleteIfEmpty"),WideString(tagAttrVal.deleteIfEmpty));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("dataType"),WideString(tagAttrVal.dataType));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("isAutoResize"),WideString(tagAttrVal.isAutoResize));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("LanguageID"),WideString(tagAttrVal.LanguageID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("index"),WideString(tagAttrVal.index));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("pbObjectId"),WideString(tagAttrVal.pbObjectId));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("parentID"),WideString(tagAttrVal.parentID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("childId"),WideString(tagAttrVal.childId));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("sectionID"),WideString(tagAttrVal.sectionID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("parentTypeID"),WideString(tagAttrVal.parentTypeID));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("isSprayItemPerFrame"),WideString(tagAttrVal.isSprayItemPerFrame));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("catLevel"),WideString(tagAttrVal.catLevel));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("imgFlag"),WideString(tagAttrVal.imgFlag));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("imageIndex"),WideString(tagAttrVal.imageIndex));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("flowDir"),WideString(tagAttrVal.flowDir));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("childTag"),WideString(tagAttrVal.childTag));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("tableFlag"),WideString(tagAttrVal.tableFlag));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("tableType"),WideString(tagAttrVal.tableType));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("tableId"),WideString(tagAttrVal.tableId));		
		
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("rowno"),WideString(tagAttrVal.rowno));
		Utils<IXMLAttributeCommands>()->CreateAttribute(xmlRef,WideString("colno"),WideString(tagAttrVal.colno));
	}

//Method Purpose: remove the Special character from the Name. 
PMString keepOnlyAlphaNumeric(PMString name)
{
	PMString tagName("");

	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		bool isAlphaNumeric = false ;

		PlatformChar ch = name.GetChar(i);

		if(ch.IsAlpha() || ch.IsNumber())
			isAlphaNumeric = true ;

		if(ch.IsSpace())
		{
			isAlphaNumeric = true ;
			ch.Set('_') ;
		}

		if(ch == '_')
			isAlphaNumeric = true ;
	
		if(isAlphaNumeric) 
			tagName.Append(ch);
	}

	return tagName ;
}

PMString prepareTagName(PMString name)
{
	PMString tagName("");
	for(int i=0;i<name.NumUTF16TextChars(); i++)
	{
		if(name.GetChar(i) == '[' || name.GetChar(i) == ']' || name.GetChar(i) == '\n')
			continue;

		if(name.GetChar(i) ==' ')
		{
			tagName.Append("_");
		}
		else tagName.Append(name.GetChar(i));
	}
	PMString FinalTagName("");
	FinalTagName = keepOnlyAlphaNumeric(tagName); // removes the Special characters from the Name.

	return FinalTagName;
}

void updateTheTagWithModifiedAttributeValues(IIDXMLElement * &XMLElementTagPtr)
{
	
					
	PMString ID;
	ID.AppendNumber(PMReal(GlobalData::SelectedNewAttrID));
	PMString tagName;
	tagName = prepareTagName(GlobalData::NewTable_col);
	//CA(tagName); 
	//XMLElementTagPtr->SetTag(tagName); //Cs3
	XMLElementTagPtr->SetTag(XMLElementTagPtr->GetTagUID());  //Cs4
	XMLElementTagPtr->SetAttributeValue(WideString("ID"),WideString(ID));//Cs4
}
bool16 GetTextstoryFromBox(InterfacePtr<ITextModel> iModel, int32 startIndex, int32 finishIndex, PMString& story)
{	
	TextIterator begin(iModel, startIndex);
	TextIterator end(iModel, finishIndex);
	
	for (TextIterator iter = begin; iter <= end; iter++)
	{	
		const textchar characterCode = (*iter).GetValue();
		char buf;
		::CTUnicodeTranslator::Instance()->TextCharToChar(&characterCode, 1, &buf, 1); 
		story.Append(buf);
	}	
	return kTrue;
}

void replaceOldTagWithNewTag(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo)
{
//CA("replaceOldTagWithNewTag");	
	bool16 typeIdFound = kFalse;
	if(tagInfo.childTag == 1){
		for(int32 i=0;i<GlobalData::TotalItemList.size();i++)
		{
			if(GlobalData::TotalItemList[i]==tagInfo.childId)
			{
				typeIdFound = kTrue;
					break;
		}
			}
		if(typeIdFound == kFalse)
			GlobalData::TotalItemList.push_back(tagInfo.childId);
	}
	else
	{
		for(int32 i=0;i<GlobalData::TotalItemList.size();i++)
		{
			if(GlobalData::TotalItemList[i]==tagInfo.typeId)
			{
				typeIdFound = kTrue;
				break;
			}
		}
		if(typeIdFound == kFalse)
			GlobalData::TotalItemList.push_back(tagInfo.typeId);
	}
	//GlobalData::TotalItemList.push_back(tagInfo.typeId);
						
	PMString entireStory("");
	bool16 result1 = kFalse;				
	result1=GetTextstoryFromBox(textModel, tagInfo.startIndex+1, tagInfo.endIndex-1, entireStory);
	//CA(entireStory);
	bool16 refreshItemFlag = kFalse;
	
	if(tagInfo.childTag == 1){
		if(currentTypeID == tagInfo.childId){
			refreshItemFlag = kFalse;
		}
		else
		{
			refreshItemFlag = kTrue;
			currentTypeID = tagInfo.childId;
		}
	}
	else{
		if(currentTypeID == tagInfo.typeId){
			refreshItemFlag = kFalse;
		}
		else
		{
			refreshItemFlag = kTrue;
			currentTypeID = tagInfo.typeId;
		}
	}
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return;	
//CA_NUM("tagInfo.languageID",tagInfo.languageID);
//CA_NUM("typeID",tagInfo.typeId);
	PMString DBStory("");
	if(tagInfo.childTag == 1){
		//CA("tagInfo.childTag == 1");
		if(tagInfo.isEventField == 1)
		{
			//CA("tagInfo.isEventField");
			/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(tagInfo.childId,GlobalData::SelectedNewAttrID,tagInfo.sectionID);
			if(vecPtr != NULL)
				if(vecPtr->size()> 0)
					DBStory = vecPtr->at(0).getObjectValue();	*/

			
		}
		else if(GlobalData::SelectedNewAttrID == -401 || GlobalData::SelectedNewAttrID == -402 || GlobalData::SelectedNewAttrID == -403){
			//DBStory = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.childId,GlobalData::SelectedNewAttrID,tagInfo.languageID);
		}
		else{
			DBStory = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.childId,GlobalData::SelectedNewAttrID,tagInfo.languageID, tagInfo.sectionID, refreshItemFlag);
		}
	}
	else{
		//CA("tagInfo.childTag != 1");
		if(tagInfo.isEventField == 1)
		{
			//CA("tagInfo.isEventField");
			/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(tagInfo.typeId,GlobalData::SelectedNewAttrID,tagInfo.sectionID);
			if(vecPtr != NULL)
				if(vecPtr->size()> 0)
					DBStory = vecPtr->at(0).getObjectValue();*/	

			//CA("itemAttributeValue = " + itemAttributeValue);
		}
		else if(GlobalData::SelectedNewAttrID == -401 || GlobalData::SelectedNewAttrID == -402 || GlobalData::SelectedNewAttrID == -403){
			//DBStory = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,GlobalData::SelectedNewAttrID,tagInfo.languageID);
		}
		else{
			DBStory = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,GlobalData::SelectedNewAttrID,tagInfo.languageID, tagInfo.sectionID, refreshItemFlag);
		}
	}
	//CA("DBStory = " + DBStory);
	//**Aded By sachin Sharma
	Attribute cAttributeModel = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(1,tagInfo.languageID);
	PMString baseNumber("");
	if(tagInfo.childTag == 1){
		baseNumber = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.childId,cAttributeModel.getAttributeId(),tagInfo.languageID, tagInfo.sectionID,refreshItemFlag);		
	}
	else{
		baseNumber = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,cAttributeModel.getAttributeId(),tagInfo.languageID, tagInfo.sectionID,refreshItemFlag);
	}

	if(DBStory.IsEqual(entireStory)==kFalse)
	{
		SKUData tempData;
		metadata::Clock a;
		/*metadata::*/XMP_DateTime dt;  //---CS5 change--
		a.timestamp(dt);

		/*PMString Date;
		Date.AppendNumber(dt.mday);
		Date.Append("-");
		Date.AppendNumber(dt.month);
		Date.Append("-");
		Date.AppendNumber(dt.year);*/
		
		PMString Date;
		Date.AppendNumber(dt.month);
		Date.Append("-");
		Date.AppendNumber(dt.day);  //--CS5--Changes--
		Date.Append("-");
		Date.AppendNumber(dt.year);

		tempData.OriginalData.Append(entireStory);
		tempData.ChangedData.Append(DBStory);
		tempData.DocPath.Append(FileName);
		tempData.Date.Append(Date);
		tempData.PageNo=pageNo;
		tempData.ColumName.Append(GlobalData::OldTable_col);
		tempData.ColumName.Append(" >> ");
		tempData.ColumName.Append(GlobalData::NewTable_col);
		tempData.ItemID = tagInfo.typeId;
		
		//**** Now Getting the AttriBute Name from AttributeID		
		PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tagInfo.elementId,tagInfo.languageID);
		tempData.attributeName = dispname;		
		tempData.baseNumber =baseNumber;
		//*******Up To Here	

		GlobalData::skuDataList.push_back(tempData);
		
		
		int32 NoofChar1 = DBStory.NumUTF16TextChars();
		//WideString* myText=new WideString(DBStory./*GetPlatformString().c_str()*/GrabCString()); //Cs4
        DBStory.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> myText(new WideString(DBStory));
		int32 tStart=tagInfo.startIndex+1;
		int32 tEnd=tagInfo.endIndex-tStart;
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{	//CA("5");	
			iConverter->ChangeQutationMarkONOFFState(kFalse);
			//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
			//ErrorCode Err = textModel->Replace(tStart, tEnd, myText);				
			//ErrorCode Err = ReplaceText(textModel, tStart, tEnd, myText);
			ErrorCode Err = ReplaceText(textModel, tStart, tEnd, myText);
			iConverter->ChangeQutationMarkONOFFState(kTrue);
		}	
		else
		{	
			//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
			//ErrorCode Err = textModel->Replace(tStart, tEnd, myText);
			//ErrorCode Err = ReplaceText(textModel, tStart, tEnd, myText);
			ErrorCode Err = ReplaceText(textModel, tStart, tEnd, myText);
		}
		updateTheTagWithModifiedAttributeValues(tagInfo.tagPtr);
		//if(myText)	//------
		//	delete myText;
	}
}

void refreshTheItemAttribute(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo)
{
//CA("CommonFunction::refreshTheItemAttribute");
	bool16 typeIdFound = kFalse;
	if(tagInfo.childTag == 1){
		for(int32 i=0;i<GlobalData::TotalItemList.size();i++)
		{
			if(GlobalData::TotalItemList[i]==tagInfo.childId)
			{
				typeIdFound = kTrue;
				break;
			}
		}
		if(typeIdFound == kFalse)
			GlobalData::TotalItemList.push_back(tagInfo.childId);
	}
	else{
		for(int32 i=0;i<GlobalData::TotalItemList.size();i++)
		{
			if(GlobalData::TotalItemList[i]==tagInfo.typeId)
			{
				typeIdFound = kTrue;
				break;
			}
		}
		if(typeIdFound == kFalse)
			GlobalData::TotalItemList.push_back(tagInfo.typeId);
	}

	//GlobalData::TotalItemList.push_back(tagInfo.typeId);

	PMString entireStory("");
	bool16 result1 = kFalse;				
	result1=GetTextstoryFromBox(textModel, tagInfo.startIndex+1, tagInfo.endIndex-1, entireStory);
	//CA(entireStory);
	//temp_to_test  = ptrIAppFramework->findProductCore(tList[i].typeId,tList[i].COLName.GrabCString());
	bool16 refreshItemFlag = kFalse;
	if(tagInfo.childTag == 1){
		if(currentTypeID == tagInfo.childId)
			refreshItemFlag = kFalse;
		else
		{
			refreshItemFlag = kTrue;
			currentTypeID = tagInfo.childId;
		}
	}
	else{
		if(currentTypeID == tagInfo.typeId)
			refreshItemFlag = kFalse;
		else
		{
			refreshItemFlag = kTrue;
			currentTypeID = tagInfo.typeId;
		}
	}
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return;	
	
	PMString DBStory("");
	if(tagInfo.childTag == 1){
		//CA("tagInfo.childTag == 1______1");
		if(tagInfo.isEventField == 1)
		{
			//CA("tagInfo.isEventField");
			//VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(tagInfo.childId,GlobalData::attributeID,tagInfo.sectionID);
			//if(vecPtr != NULL){
			//	//CA("vecPtr != NULL");
			//	if(vecPtr->size()> 0){
			//		//CA("vecPtr->size() > 0");
			//		DBStory = vecPtr->at(0).getObjectValue();	
			//	}
			//}

			//CA("itemAttributeValue = " + itemAttributeValue);
		}
		else if(GlobalData::attributeID == -401 || GlobalData::attributeID == -402 || GlobalData::attributeID == -403){
			//DBStory = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.childId,GlobalData::attributeID,tagInfo.languageID);
		}
		else{
			DBStory = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.childId,GlobalData::attributeID,tagInfo.languageID,  tagInfo.sectionID, refreshItemFlag);
		}
	}
	else{
		//CA("tagInfo.childTag != 1______1");
		if(tagInfo.isEventField == 1)
		{
			//CA("tagInfo.isEventField");
			/*VectorCPubObjectValuePtr vecPtr = ptrIAppFramework->GetEventObject_getObjectValueByEventAttributeAndObjectId(tagInfo.typeId,GlobalData::attributeID, tagInfo.sectionID);
			if(vecPtr != NULL)
				if(vecPtr->size()> 0)
					DBStory = vecPtr->at(0).getObjectValue();	*/

			//CA("itemAttributeValue = " + itemAttributeValue);
		}
		else if(GlobalData::attributeID == -401 || GlobalData::attributeID == -402 || GlobalData::attributeID == -403){
			//DBStory = ptrIAppFramework->GETItem_GetItemMMYDetailsByLanguageId(tagInfo.typeId,GlobalData::attributeID,tagInfo.languageID);
		}
		else{
			DBStory = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,GlobalData::attributeID,tagInfo.languageID, tagInfo.sectionID, refreshItemFlag);
		}
	}
	//CA("DBStory = " + DBStory);
	PMString textToInsert;
	InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
	if(!iConverter)
		textToInsert=DBStory;
	else	
		textToInsert=iConverter->translateString(DBStory);
	DBStory = textToInsert;

	//****Added By Sachin
	Attribute cAttributeModel = ptrIAppFramework->ATTRIBUTECache_getItemAttributeForIndex(1,tagInfo.languageID);
	PMString baseNumber("");
	if(tagInfo.childTag == 1)
		baseNumber = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.childId,cAttributeModel.getAttributeId(),tagInfo.languageID, tagInfo.sectionID, refreshItemFlag);
	else
		baseNumber = ptrIAppFramework->GETItem_GetItemAttributeDetailsByLanguageId(tagInfo.typeId,cAttributeModel.getAttributeId(),tagInfo.languageID, tagInfo.sectionID, refreshItemFlag);
	//*****
	if(DBStory.IsEqual(entireStory)==kFalse)
	{
		SKUData tempData;

		metadata::Clock a;
		/*metadata::*/XMP_DateTime dt;  //--- CS5 Changes---
		a.timestamp(dt);

		PMString Date;
		Date.AppendNumber(dt.month);
		Date.Append("-");
		Date.AppendNumber(dt.day);    //---CS5 Changes---
		Date.Append("-");
		Date.AppendNumber(dt.year);

		tempData.OriginalData.Append(entireStory);
		tempData.ChangedData.Append(DBStory);
		tempData.DocPath.Append(FileName);
		tempData.Date.Append(Date);
		tempData.PageNo=pageNo;
		//tempData.ColumName.Append(tList[i].id);  // commented @Vaibhav
		tempData.ColumName.AppendNumber(PMReal(tagInfo.elementId));

		//**** Now Getting the AttriBute Name from AttributeID		
		PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tagInfo.elementId,tagInfo.languageID);
		tempData.attributeName = dispname;		
		tempData.baseNumber =baseNumber;		
		//*******Up To Here
		if(tagInfo.childTag == 1)
			tempData.ItemID = tagInfo.childId;
		else
			tempData.ItemID = tagInfo.typeId;

		GlobalData::skuDataList.push_back(tempData);
		

		//WideString* myText=new WideString(DBStory./*GetPlatformString().c_str()*/GrabCString()); //Cs4
        DBStory.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> myText(new WideString(DBStory));
		int32 tStart=tagInfo.startIndex+1;
		int32 tEnd=tagInfo.endIndex-tStart;
		
		if(iConverter)
		{	//CA("5");	
			iConverter->ChangeQutationMarkONOFFState(kFalse);
			//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
			//ErrorCode Err = textModel->Replace(tStart, tEnd, myText);				
			ErrorCode Err = ReplaceText(textModel, tStart, tEnd, myText);
			iConverter->ChangeQutationMarkONOFFState(kTrue);
		}	
		else
		{	//CA("6");
			//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
			//ErrorCode Err = textModel->Replace(tStart, tEnd, myText);
			ErrorCode Err = ReplaceText(textModel, tStart, tEnd, myText);
		}
		//if(myText)	//------
		//	delete myText;

	}
}

void replaceTheOldItemHeaderWithNewItemHeader(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo)
{
	PMString entireStory("");
	bool16 result1 = kFalse;				
	result1=GetTextstoryFromBox(textModel, tagInfo.startIndex+1, tagInfo.endIndex-1, entireStory);
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return;										
	PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(GlobalData::SelectedNewAttrID,tagInfo.languageID);
	if(dispname.IsEqual(entireStory)==kFalse)
	{
		int32 NoofChar1 = dispname.NumUTF16TextChars();
		//WideString* myText=new WideString(dispname./*GetPlatformString().c_str()*/GrabCString()); //Cs4
        dispname.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> myText(new WideString(dispname));
		int32 tStart=tagInfo.startIndex+1;
		int32 tEnd=tagInfo.endIndex-tStart;
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{	
			iConverter->ChangeQutationMarkONOFFState(kFalse);
			//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
			//ErrorCode Err = textModel->Replace(tStart, tEnd, myText);				
			ErrorCode Err = ReplaceText(textModel, tStart, tEnd, myText);
			iConverter->ChangeQutationMarkONOFFState(kTrue);
		}	
		else
		{
			//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
			//ErrorCode Err = textModel->Replace(tStart, tEnd, myText);
			ErrorCode Err = ReplaceText(textModel, tStart, tEnd, myText);
		}												
		updateTheTagWithModifiedAttributeValues(tagInfo.tagPtr);
		//if(myText)	//------
		//	delete myText;
	}
}
void refreshTheItemHeader(InterfacePtr<ITextModel> & textModel,double & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo)
{
	PMString entireStory("");
	bool16 result1 = kFalse;				
	result1=GetTextstoryFromBox(textModel, tagInfo.startIndex+1, tagInfo.endIndex-1, entireStory);
	
	InterfacePtr<IAppFramework> ptrIAppFramework((static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID))));
	if(ptrIAppFramework == nil)
		return;	
	PMString dispname = ptrIAppFramework->ATTRIBUTECache_getItemAttributeName(tagInfo.elementId,tagInfo.languageID);
//	CA(dispname);
	if(dispname.IsEqual(entireStory)==kFalse)
	{
		//WideString* myText=new WideString(dispname./*GetPlatformString().c_str()*/GrabCString()); //Cs4
        dispname.ParseForEmbeddedCharacters();
		boost::shared_ptr<WideString> myText(new WideString(dispname));
		int32 tStart=tagInfo.startIndex+1;
		int32 tEnd=tagInfo.endIndex-tStart;
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{	
			iConverter->ChangeQutationMarkONOFFState(kFalse);
			//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
			//ErrorCode Err = textModel->Replace(tStart, tEnd, myText);				
			ErrorCode Err = ReplaceText(textModel, tStart, tEnd, myText);
			iConverter->ChangeQutationMarkONOFFState(kTrue);
		}	
		else
		{
			//ErrorCode Err = textModel->Replace(kTrue,tStart, tEnd, myText);	//CS3 Change
			//ErrorCode Err = textModel->Replace(tStart, tEnd, myText);
			ErrorCode Err = ReplaceText(textModel, tStart, tEnd, myText);
		}
		//if(myText)	//------
		//	delete myText;
	}
}
/*
void mainPriceUpdate(InterfacePtr<ITextModel> & textModel,int32 & currentTypeID,PMString & FileName,int32 & pageNo,TagStruct & tagInfo)
	{
		if(tagInfo.typeId == -2)
			{
				if(IsChangePriceModeSelected == kFalse)
				{
					if(tagInfo.elementId != GlobalData::attributeID)
						continue;
					if(!itagReader->GetUpdatedTag(tagInfo))
					return;
					refreshTheItemHeader(textModel,currentTypeID,FileName,pageNo,tagInfo);
					
				}
				else if(IsChangePriceModeSelected == kTrue)
				{					
					if(tagInfo.elementId != GlobalData::SelectedOldAttrID)
						continue;
					if(!itagReader->GetUpdatedTag(tagInfo))
					return;
					replaceTheOldItemHeaderWithNewItemHeader(textModel,currentTypeID,FileName,pageNo,tagInfo);
					
				}
			}
			else
			{

				if(IsChangePriceModeSelected == kFalse)
				{
					UpdateParentList(BoxUIDRef);
					//if(tList[i].id != GlobalData::Table_col) //commented bcoz attribute id funda comes into picture @vaibhav
					if(tagInfo.elementId != GlobalData::attributeID)
						continue;					
					if(!itagReader->GetUpdatedTag(tagInfo))
					return;
					refreshTheItemAttribute(textModel,currentTypeID,FileName,pageNo,tagInfo);
					
				}
				else if(IsChangePriceModeSelected == kTrue)
				{					
					UpdateParentList(BoxUIDRef);
					if(tagInfo.elementId != GlobalData::SelectedOldAttrID)
						continue;
					if(!itagReader->GetUpdatedTag(tagInfo))
						return;
					replaceOldTagWithNewTag(textModel,currentTypeID,FileName,pageNo,tagInfo);                 
				}
			}
	}
*/

ErrorCode ReplaceText(ITextModel* textModel, const TextIndex position, const int32 length, const /*K2*/boost::shared_ptr<WideString>& text)
{ErrorCode status = kFailure;
	do {
		InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(iConverter)
		{
			//CA("iConverter");
			iConverter->ChangeQutationMarkONOFFState(kFalse);			
		}

		ASSERT(textModel);
		if (!textModel) {
			//CA("!textModel");
			break;
		}
		if (position < 0 || position >= textModel->TotalLength()) {
			//CA("position invalid");
			break;
		}
		if (length < 0 || length >= textModel->TotalLength()) {
			//CA("length invalid");
			break;
		}
    	InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
    	ASSERT(textModelCmds);
    	if (!textModelCmds) {
			//CA("!textModelCmds");
			break;
		}

		if(length == 0)
		{
			InterfacePtr<ICommand> insertTextCmd(textModelCmds->InsertCmd(position, text));
			ASSERT(insertTextCmd);		
			if (!insertTextCmd) {
				//CA("!replaceCmd");
				break;
			}
			//CA("before Replace ");
			status = CmdUtils::ProcessCommand(insertTextCmd);
			if(status != kSuccess)
			{
				//CA("status != kSuccess");
				break;
			}

		}
		else
		{
			InterfacePtr<ICommand> replaceCmd(textModelCmds->ReplaceCmd(position, length, text));
			ASSERT(replaceCmd);		
			if (!replaceCmd) {
				//CA("!replaceCmd");
				break;
			}
			//CA("before Replace ");
			status = CmdUtils::ProcessCommand(replaceCmd);
			if(status != kSuccess)
			{
				//CA("status != kSuccess");
				break;
			}
		}
		//CA("After Replace");
		if(iConverter)
		{
			//CA("true iConverter");
			iConverter->ChangeQutationMarkONOFFState(kTrue);			
		}
		
	} while(false);
	CmdUtils::ProcessScheduledCmds(ICommand::kLowestPriority);
	return status;
}
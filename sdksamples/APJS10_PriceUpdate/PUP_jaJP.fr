//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__


// Japanese string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_jaJP)
{
        k_jaJP,	// Locale Id
        0,		// Character encoding converter

        {
        	// ----- Menu strings
                kPUPCompanyKey,					kPUPCompanyValue,
                kPUPAboutMenuKey,					kPUPPluginName "[JP]...",
                kPUPPluginsMenuKey,				kPUPPluginName "[JP]",
				kPUPDialogMenuItemKey,			"Show dialog[JP]",

                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_jaJP,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings
					kPUPDialogTitleKey,     kPUPPluginName "[JP]",

              // ----- Error strings

                // ----- Misc strings
                kPUPAboutBoxStringKey,			kPUPPluginName " [JP], version " kPUPVersion " by " kPUPAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_jaJP,
                
                kPUPCatlogPriceRefresgStringKey,				"Catalog Price Refresh",
				kPUPRefreshPriceStringKey,						"Refresh Price",
				kPUPCurrentPriceStringKey,						"Current Price:",		
				kPUPReplaceFieldStringKey,						"Replace field",
				kPUPCurrentFieldStringKey,						"Current Field:",	
				kPUPNewFieldStringKey,							"New Field:",		
				kPUPRefreshPriceResultStringKey,				"Refresh Price Results",
				kPUPNoOfDocRefreshedStringKey,					"No. of document refreshed",
				kPUPColenStringKey,								" : ",
				kPUPTotalItemFoundStringKey,					"Total items found",
				kPUPItemsRefreshedStringKey,					"Items refreshed",
				kPUPDashSeparatorStringKey,						"-----------------------------------------------------------------------------",
				kPUPChangeLogStringKey,							"Change log : ",
                kPUPEventToRefreshStringKey,                    "Event to Refresh:",

        }

};

#endif // __ODFRC__

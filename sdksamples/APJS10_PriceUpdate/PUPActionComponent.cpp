//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

#include "IBookManager.h"
#include "IBook.h"
#include "IBookContentMgr.h"

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
inline PMString numToPMString(int32 num)
{
	PMString x;
	x.AppendNumber(num);
	return x;
}
#define CA(X) CAlert::InformationAlert \
	( \
		PMString("PUPDialogObserver.cpp") + PMString("\n") + \
		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
		PMString("\n Message : ")+ X \
	)

#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


// Project includes:
#include "PUPID.h"
#include "IActionStateList.h"
#include "PUPDialogController.h"
#include "PUPActionComponent.h"
#include "IWindow.h"

//IDialog* PUPActionComponent::MainDlg = nil;
//IDialog* PUPActionComponent::SelDocDlg;
//IDialog* PUPActionComponent::ProcessDlg;
//IDialog* PUPActionComponent::SummaryDlg = nil;
 IDialog *MainDlg = 0;
 //IDialog *SummaryDlg = nil;

extern IControlView* Fist1ScreenView;
extern IControlView* Second2ScreenView ;
bool16 ISFirstDialog = kTrue;

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.
*/

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(PUPActionComponent, kPUPActionComponentImpl)
	
//***************************************************************************
bool16 IsTheBookBlank()
{

	bool16 status = kFalse;
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
			return status;
		}

		InterfacePtr<IBookManager> bookManager(/*gSession*/GetExecutionContextSession(), UseDefaultIID()); //Cs4
		if (bookManager == nil) 
		{ 
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::IsTheBookBlank::bookManager == nil");
			break; 
		}
		IBook * currentActiveBook = bookManager->GetCurrentActiveBook();
		if(currentActiveBook == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::IsTheBookBlank::currentActiveBook == nil");
			break;			
		}
		InterfacePtr<IBookContentMgr> iBookContentMgr(currentActiveBook,UseDefaultIID());
		if(iBookContentMgr == nil)
		{
			ptrIAppFramework->LogDebug("AP46CreateMedia::CMMDialogObserver::IsTheBookBlank::iBookContentMgr == nil");
			break;
		}

		int32 contentCount = iBookContentMgr->GetContentCount();
		if(contentCount <= 0)
		{
			status = kTrue;
		}
	}while(kFalse);

	return status;
}
//******************************************************************************
/* PUPActionComponent Constructor
*/
void PUPActionComponent::CloseProcessDialog()
{
	//CA("Close Process");
	/*if(ProcessDlg){
		if(ProcessDlg->IsOpen())
		{
			ProcessDlg->Close();
			ProcessDlg->Release();
		}
	}*/
}
		
void PUPActionComponent::ShowMainDialog()
{
	ISFirstDialog = kTrue;
	PUPDialogController DlgController(this);
	//DlgController.CallDialogInitaliser(kTrue);

}
void PUPActionComponent::OpenSummaryDialog()
{
	ISFirstDialog = kFalse;	
	this->DoDialog();
	PUPDialogController DlgController(this);
	//DlgController.CallDialogInitaliser(kFalse);
		
}
void PUPActionComponent::CloseSummaryDialog()
{
	//if(SummaryDlg){
	//	if(SummaryDlg->IsOpen()){
	//		CA("333");
	//		SummaryDlg->Close();
	//		//SummaryDlg->Release();
	//		
	//	}
	//}
	if(MainDlg){
		if(MainDlg->IsOpen())
		{
			MainDlg->Close();
		}
	}
}
void PUPActionComponent::CloseSelDocDialog()
{
	/*if(SelDocDlg){
		if(SelDocDlg->IsOpen())
			SelDocDlg->Close();
	}*/
}
void PUPActionComponent::OpenProcessDialog()
{



}
void PUPActionComponent::CloseMainDialog()
{
	if(MainDlg){
		if(MainDlg->IsOpen())
		{
			MainDlg->Close();			
		}
	}
}
void PUPActionComponent::OpenSelectFileDialog()
{
	
}


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
//CREATE_PMINTERFACE(PUPActionComponent, kPUPActionComponentImpl)

/* PUPActionComponent Constructor
*/
PUPActionComponent::PUPActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void PUPActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		/*case kPUPAboutActionID:
		{
			this->DoAbout();
			break;
		}					*/

		case kPUPDialogActionID:
		{
			ISFirstDialog = kTrue;	
			this->DoDialog();
			break;
		}


		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void PUPActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kPUPAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}

/* DoMenuItem1
*/
void PUPActionComponent::DoMenuItem1(IActiveContext* ac)
{
	CAlert::InformationAlert(kPUPMenuItem1StringKey);
}


/* DoDialog
*/
void PUPActionComponent::DoDialog()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(/*gSession*/GetExecutionContextSession()->QueryApplication()); //Cs4
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kPUPPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kModeless);
		//MainDlg=dialogMgr->CreateNewDialog(dialogSpec, IDialog::kModeless);
		//ASSERT(dialog);
		if (dialog == nil) {
			break;
		}

		// Set appropriate window title based on mode
		InterfacePtr<IWindow> window(dialog, UseDefaultIID());
		ASSERT(window != nil);
		if (window == nil) {
			break;
		}

		PMString windowTitle;
		if(ISFirstDialog)
		{	
			PMString FIRST("Refresh Price/ Replace Field");//PMString FIRST("Refresh Price");//PMString FIRST("Catalog Price Refresh");
			FIRST.SetTranslatable(kFalse);
			windowTitle = FIRST;
		}
		else
		{
			PMString SECOND(kPUPRefreshPriceResultStringKey/*"Refresh Price Results"*/);//PMString SECOND("Catalog Price Refresh Results");
			SECOND.SetTranslatable(kFalse);
			windowTitle = SECOND;
		}
		
		window->SetTitle(windowTitle);

		// Open the dialog.
		//dialog->Open(); 
		//MainDlg->AddRef();
		MainDlg = dialog;
		dialog->Open(); 
	
	} while (false);			
}
void PUPActionComponent::UpdateActionStates (IActionStateList* iListPtr)
{	
	//**** Not Needed More
	/*InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;

	bool16 result=ptrIAppFramework->LOGINMngr_getLoginStatus();
	if(result)
	{
		iListPtr->SetNthActionState(0,kEnabledAction);		
	}
	else
	{
		iListPtr->SetNthActionState(0,kDisabled_Unselected);		
	}	*/
}


//*****Added For Cs4
void  PUPActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList *listToUpdate, GSysPoint mousePoint, IPMUnknown* widget)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
		return;
	bool16 result=ptrIAppFramework->getLoginStatus();
	bool16 isbookblank = IsTheBookBlank();
	for(int32 iter = 0; iter < listToUpdate->Length(); iter++) 
	{
		ActionID actionID = listToUpdate->GetNthAction(iter);
		if(actionID == kPUPDialogActionID)
		{ 
			if(result && !isbookblank)
			{
				listToUpdate->SetNthActionState(0,kEnabledAction);		
			}
			else
			{
				listToUpdate->SetNthActionState(0,kDisabled_Unselected);		
			}
		}
	}
	
}

//  Code generated by DollyXs code generator

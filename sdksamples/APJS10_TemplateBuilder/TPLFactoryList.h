//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================
REGISTER_PMINTERFACE(TPLActionComponent, kTPLActionComponentImpl)
REGISTER_PMINTERFACE(TPLSelectionObserver,		kTPLSelectionObserverImpl)
REGISTER_PMINTERFACE(TPLWidgetObserver,			kTPLWidgetObserverImpl)
//REGISTER_PMINTERFACE( ActSel, kMYframeObserverImpl)


//REGISTER_PMINTERFACE(TPLPFListBoxObserver,		kTPLPFListBoxObserverImpl)
//REGISTER_PMINTERFACE(TPLPGListBoxObserver,	kTPLPGListBoxObserverImpl)
//REGISTER_PMINTERFACE(TPLPRListBoxObserver,		kTPLPRListBoxObserverImpl)
//REGISTER_PMINTERFACE(TPLItemListBoxObserver,	kTPLItemListBoxObserverImpl)
REGISTER_PMINTERFACE(TPLCustFlavHlpr,			kTPLCustFlavHlprImpl)
REGISTER_PMINTERFACE(TPLDragSource,				kTPLDragSourceImpl)
REGISTER_PMINTERFACE(TPLLstboxTextObserver,		kTPLLstboxTextObserverImpl)
REGISTER_PMINTERFACE(TPLPageItemScrapHandler,	kTPLPageItemScrapHandlerImpl)

//REGISTER_PMINTERFACE(TPLDocObserver,			kTPLDocObserverImpl)
REGISTER_PMINTERFACE(TPLDocServiceProvider,		kTPLDocServiceProviderImpl)
REGISTER_PMINTERFACE(TPLDocResponder,			kTPLDocResponderImpl)
REGISTER_PMINTERFACE(TblBscSuiteASB,			kTPLTblBscSuiteASBImpl)
REGISTER_PMINTERFACE(TblBscSuiteTextCSB,		kTPLTblBscSuiteTextCSBImpl)
REGISTER_PMINTERFACE(TPLRadioBtnsObserver,		kTPLRadioBtnsObserverImpl)
REGISTER_PMINTERFACE(CTemplatesDialogCloser,	kTemplatesDialogCloserImpl)
REGISTER_PMINTERFACE(TPLLoginEventsHandler,		kTPLLoginEventsHandlerImpl)
REGISTER_PMINTERFACE(TPLTreeViewHierarchyAdapter, kTPLTreeViewHierarchyAdapterImpl)
REGISTER_PMINTERFACE(TPLTreeViewWidgetMgr,		kTPLTreeViewWidgetMgrImpl)
REGISTER_PMINTERFACE( PnlTrvNodeEH,				kPnlTrvNodeEHImpl)
REGISTER_PMINTERFACE(TextMiscellanySuiteLayoutCSB, kTPLTextMiscellanySuiteLayoutCSBImpl)
REGISTER_PMINTERFACE(TextMiscellanySuiteASB,	kTPLTextMiscellanySuiteASBImpl)
REGISTER_PMINTERFACE(DynMnuDynamicMenu,			kDynMnuDynamicMenuImpl )

REGISTER_PMINTERFACE(TPLActionFilter,			kTPLActionFilterImpl)
//REGISTER_PMINTERFACE(SnipRunControlView_TMP ,	kTPLPanalControlViewImpl )
REGISTER_PMINTERFACE(OverlapProperties ,		kIoverlapPropertiesImpl )
#include "VCPluginHeaders.h"
#include "IListBoxController.h"
#include "SDKListBoxHelper.h"
#include "TPLListboxData.h"
// Interface includes
#include "ISelectionManager.h"
#include "ITableSuite.h"
#include "ITableAttrRealNumber.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ITextValue.h"
#include "CAlert.h"
#include "ControlStripID.h"
#include "CEventHandler.h"
//#include "Utills.h"
#include"IDocumentList.h"
#include"IDocument.h"
#include"IDocumentUtils.h"
#include"IApplication.h"
#include "ISelectionUtils.h"
#include "ITextMiscellanySuite.h"
#include "ITextEditSuite.h"
#include "TPLMediatorClass.h"
#include "SelectionObserver.h"
#include "IGraphicFrameData.h"
#include "IEvent.h"
///////////////////
#include "ICommand.h"
#include "CmdUtils.h"
#include "IImportFileCmdData.h"
#include "IPlaceGun.h"
#include "IReplaceCmdData.h"
#include "FileUtils.h"
#include "IHierarchy.h"
#include "IGeometry.h"
#include "TransformUtils.h"
#include "SDKLayoutHelper.h"
#include "ISpread.h"
#include "IPasteboardUtils.h"
#include "LayoutUtils.h"
#include "SDKUtilities.h"
//#include "GlobalData.h"
#include "KeyboardDefs.h"	
#include "TPLID.h"
#define CA(X) CAlert::InformationAlert(X)

//extern GlobalDataList gList;


/**
	Class to observe changes in the active context, and change UI when attributes change.
	@ingroup tableattributes
 */
 class ActSel : public CEventHandler
{
	public:
		/** 
			Constructor.
		*/
		ActSel(IPMUnknown *boss);
		/**
			Destructor.
		*/
		virtual ~ActSel(){};
//virtual bool16 ButtonDblClk(IEvent* e);
virtual bool16 KeyDown(IEvent* e);
//virtual bool16 KeyUp(IEvent* e);


	//protected:
	//	virtual void HandleSelectionChanged(const ISelectionMessage*   );

	
		
};

CREATE_PMINTERFACE(ActSel, kMYframeObserverImpl);


//ActSel::ActSel(IPMUnknown *boss) :
//	ActiveSelectionObserver(boss, IID_IFRAMEOPTIONSSELECTIONOBSERVER)
//{CA("Cons");
//}
//
//
//ActSel::~ActSel()
//{//CA("De Cons");
//}
//void ActSel::HandleSelectionChanged(const ISelectionMessage *  Selmessage){
//CA("hye we got it");
//}
ActSel::ActSel(IPMUnknown *boss) :
	CEventHandler(boss)
{//CA("Cons");
}


bool16 ActSel::KeyDown(IEvent* e)
{    bool16 result=kFalse;
	if(e == nil) return kFalse;

	const SysChar	systemkey = e->GetChar();
	/*VirtualKey key= e->GetVirtualKey();  
	SysKeyCode systemkey= key.GetKeyCode(); */
	PMString f;
	if(TPLMediatorClass::curSelLstbox==1)
		InterfacePtr<IListBoxController> listCntl(TPLMediatorClass::PFLstboxCntrlView,IID_ILISTBOXCONTROLLER);    

	if(TPLMediatorClass::curSelLstbox==2)
		InterfacePtr<IListBoxController> listCnt2(TPLMediatorClass::PGLstboxCntrlView,IID_ILISTBOXCONTROLLER);

	if(TPLMediatorClass::curSelLstbox==3)
		InterfacePtr<IListBoxController> listCnt3(TPLMediatorClass::PRLstboxCntrlView,IID_ILISTBOXCONTROLLER);
	
	if(TPLMediatorClass::curSelLstbox==4)
		InterfacePtr<IListBoxController> listCnt4(TPLMediatorClass::ItemLstboxCntrlView,IID_ILISTBOXCONTROLLER);
    if(TPLMediatorClass::curSelLstbox==5)
		InterfacePtr<IListBoxController> listCnt5(TPLMediatorClass::ProjectLstboxCntrlView,IID_ILISTBOXCONTROLLER);

	//if(TPLMediatorClass::curSelLstbox==6)
	//	InterfacePtr<IListBoxController> listCnt1(TPLMediatorClass::PFLstboxCntrlView,IID_ILISTBOXCONTROLLER);    
	
	switch(systemkey){
	case kUpArrowKey://CA("UP");
	
	switch(TPLMediatorClass::curSelLstbox)
	{
	case 1:
		if(TPLMediatorClass::curSelLstbox==1)
		InterfacePtr<IListBoxController> listCnt1(TPLMediatorClass::PFLstboxCntrlView,IID_ILISTBOXCONTROLLER);
		{
		          
			   int32 PFRow = -1;
			     // CA("Break 8");     
			   if(TPLMediatorClass::curSelRowIndex==0){
				   return kTrue;
			       break;
			   }
						if(TPLMediatorClass::count1!=0){
			            InterfacePtr<IListBoxController> listCnt1(TPLMediatorClass::PFLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			           if(listCnt1 == nil) 
				       break;
					  int32 NewINdex = TPLMediatorClass::curSelRowIndex-1;
					  //	 CA("Break 9");	
					  listCnt1->Select(NewINdex,kTrue,kTrue  ) ;
					   //CA("Break 10");
					        TPLMediatorClass::curSelRowIndex++;
							//TPLMediatorClass::curSelRowIndex++;
                          //  CA("break 10.5 ");
							TPLMediatorClass::curSelRowIndex= NewINdex;
                    //added by shail

				//const int kSelectionLength =  curSelection.Length() ;

			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			
				//curSelRowIndex=curSelection[0];
				TPLListboxData PFLstboxData;
				int32 vectSize=PFLstboxData.returnListVectorSize(2);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==TPLMediatorClass::curSelRowIndex)
					{
						TPLListboxInfo PFListboxInfo=PFLstboxData.getData(2,y);
						curSelRowStr=PFListboxInfo.name;
						//CA(PGListboxInfo.name);
						TPLMediatorClass::imageFlag=PFListboxInfo.isImageFlag;
						break;
					}
				}
			
			TPLMediatorClass::curSelLstbox=1;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			PFRow =TPLMediatorClass::curSelRowIndex ;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			TPLMediatorClass::tableFlag=0;

							//end shail


							//CA("break 10.6 ");
					  
						    
							f.Clear();
							f.AppendNumber(TPLMediatorClass::curSelRowIndex);
                            //if(PREMediatorClass::whichlistbox==1)
							//CA("End");
							
						}

		}
		      
		result= kTrue;
			
         break;
	

	case 2:
		if(TPLMediatorClass::curSelLstbox==2)
		InterfacePtr<IListBoxController> listCnt2(TPLMediatorClass::PGLstboxCntrlView,IID_ILISTBOXCONTROLLER);
		{
		          
			   int32 PGRow = -1;
			     // CA("Break 8");     
			   if(TPLMediatorClass::curSelRowIndex==0){
				   return kTrue;
			       break;
			   }
						if(TPLMediatorClass::count2!=0){
			            InterfacePtr<IListBoxController> listCnt2(TPLMediatorClass::PGLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			           if(listCnt2 == nil) 
				       break;
					  int32 NewINdex = TPLMediatorClass::curSelRowIndex-1;
					  //	 CA("Break 9");	
					  listCnt2->Select(NewINdex,kTrue,kTrue  ) ;
					   //CA("Break 10");
					        TPLMediatorClass::curSelRowIndex++;
							//TPLMediatorClass::curSelRowIndex++;
                          //  CA("break 10.5 ");
							TPLMediatorClass::curSelRowIndex= NewINdex;
                    //added by shail

				//const int kSelectionLength =  curSelection.Length() ;

			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			
				//curSelRowIndex=curSelection[0];
				TPLListboxData PGLstboxData;
				int32 vectSize=PGLstboxData.returnListVectorSize(2);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==TPLMediatorClass::curSelRowIndex)
					{
						TPLListboxInfo PGListboxInfo=PGLstboxData.getData(2,y);
						curSelRowStr=PGListboxInfo.name;
						//CA(PGListboxInfo.name);
						TPLMediatorClass::imageFlag=PGListboxInfo.isImageFlag;
						break;
					}
				}
			
			TPLMediatorClass::curSelLstbox=2;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			PGRow =TPLMediatorClass::curSelRowIndex ;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			TPLMediatorClass::tableFlag=0;

							//end shail


							//CA("break 10.6 ");
					  
						    
							f.Clear();
							f.AppendNumber(TPLMediatorClass::curSelRowIndex);
                            //if(PREMediatorClass::whichlistbox==1)
							//CA("End");
							
						}

		}
		      
		result= kTrue;
			
         break;
	
	case 3:
		if(TPLMediatorClass::curSelLstbox==3)
		InterfacePtr<IListBoxController> listCnt3(TPLMediatorClass::PRLstboxCntrlView,IID_ILISTBOXCONTROLLER);
		{
		    int32 PRRow=-1;       
			
			//CA("Break 11");     
			if(TPLMediatorClass::curSelRowIndex==0){
						return kTrue;
			            break;
			}
						if(TPLMediatorClass::count3!=0){
			            InterfacePtr<IListBoxController> listCnt3(TPLMediatorClass::PRLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			           if(listCnt3 == nil) 
				       break;
					  int32 NewINdex = TPLMediatorClass::curSelRowIndex-1;
					  	// CA("Break 12");	
					  listCnt3->Select(NewINdex,kTrue,kTrue  ) ;
					  // CA("Break 10");
					        TPLMediatorClass::curSelRowIndex++;
							//TPLMediatorClass::curSelRowIndex++;
                           // CA("break 13 ");
							TPLMediatorClass::curSelRowIndex= NewINdex;
                 //added by shail
			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			
				//curSelRowIndex=curSelection[0];
				TPLListboxData PRLstboxData;
				int32 vectSize=PRLstboxData.returnListVectorSize(3);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==TPLMediatorClass::curSelRowIndex)
					{
						TPLListboxInfo PRListboxInfo=PRLstboxData.getData(3,y);
						curSelRowStr=PRListboxInfo.name;
						//CA(PRListboxInfo.name);
						TPLMediatorClass::tableFlag=PRListboxInfo.tableFlag;
						TPLMediatorClass::imageFlag=PRListboxInfo.isImageFlag;
						break;
					}
				}
			
			TPLMediatorClass::curSelLstbox=3;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			PRRow = TPLMediatorClass::curSelRowIndex;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::PRLstboxCntrlView=TPLMediatorClass::PRLstboxCntrlView;


							//end shail

							//CA("break 14 ");
					  
						    
							f.Clear();
							f.AppendNumber(TPLMediatorClass::curSelRowIndex);
                            //if(PREMediatorClass::whichlistbox==1)
							//CA("End");
							
						}

		}
		      
		result= kTrue;
			
         break;
	

	case 4:
		if(TPLMediatorClass::curSelLstbox==4)
		InterfacePtr<IListBoxController> listCnt4(TPLMediatorClass::ItemLstboxCntrlView,IID_ILISTBOXCONTROLLER);
		{
		           
			int32 ItemRow =-1;
			//CA("Break 11");     
			if(TPLMediatorClass::curSelRowIndex==0){
				return kTrue;
			      break;
			        }
						if(TPLMediatorClass::count4!=0){
			            InterfacePtr<IListBoxController> listCnt4(TPLMediatorClass::ItemLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			           if(listCnt4 == nil) 
				       break;
					  int32 NewINdex = TPLMediatorClass::curSelRowIndex-1;
					  	// CA("Break 12");	
					  listCnt4->Select(NewINdex,kTrue,kTrue  ) ;
					  // CA("Break 10");
					        TPLMediatorClass::curSelRowIndex++;
							//TPLMediatorClass::curSelRowIndex++;
                            //CA("break 13 ");
							TPLMediatorClass::curSelRowIndex= NewINdex;
							//CA("break 14 ");
					//Added by shail
					int curSelRowIndex=-1;
		          PMString curSelRowStr("");

			/* getting current selection here */
			
				//curSelRowIndex=curSelection[0];

				TPLListboxData ItemLstboxData;
				
				int32 vectSize=ItemLstboxData.returnListVectorSize(4);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==TPLMediatorClass::curSelRowIndex)
					{
						TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(4,y);
						curSelRowStr=ItemListboxInfo.name;
						//CA(ItemListboxInfo.name);
						TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
						break;
					}
				}
			
			TPLMediatorClass::curSelLstbox=4;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
		    ItemRow = TPLMediatorClass::curSelRowIndex;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::ItemLstboxCntrlView=TPLMediatorClass::ItemLstboxCntrlView;
			TPLMediatorClass::tableFlag=0;

							//end shail

					  
						    
							f.Clear();
							f.AppendNumber(TPLMediatorClass::curSelRowIndex);
                            //if(PREMediatorClass::whichlistbox==1)
							//CA("End");
							
						}

		}		      
		result= kTrue;
			
         break;

	case 5:
		if(TPLMediatorClass::curSelLstbox==5)
		InterfacePtr<IListBoxController> listCnt5(TPLMediatorClass::ProjectLstboxCntrlView,IID_ILISTBOXCONTROLLER);
		{
		         
			int32 ProjectRow= -1;
			     // CA("Break 11");     
			if(TPLMediatorClass::curSelRowIndex==0){
				return kTrue;
			    break;
			}
						if(TPLMediatorClass::count5!=0){
			            InterfacePtr<IListBoxController> listCnt5(TPLMediatorClass::ProjectLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			           if(listCnt5 == nil) 
				       break;
					  int32 NewINdex = TPLMediatorClass::curSelRowIndex-1;
					  	 //CA("Break 12");	
					  listCnt5->Select(NewINdex,kTrue,kTrue  ) ;
					   //CA("Break 10");
					        TPLMediatorClass::curSelRowIndex++;
							//TPLMediatorClass::curSelRowIndex++;
                            //CA("break 13 ");
							TPLMediatorClass::curSelRowIndex= NewINdex;
			//Added by shail
              int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			
				//curSelRowIndex=curSelection[0];

				TPLListboxData ProjectLstboxData;
				
				int32 vectSize=ProjectLstboxData.returnListVectorSize(5);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==TPLMediatorClass::curSelRowIndex)
					{
						TPLListboxInfo ProjectListboxInfo=ProjectLstboxData.getData(5,y);
						curSelRowStr=ProjectListboxInfo.name;
						//CA(ProjectListboxInfo.name);
						TPLMediatorClass::imageFlag=ProjectListboxInfo.isImageFlag;
						break;
					}
				}
			
			TPLMediatorClass::curSelLstbox=5;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			ProjectRow = TPLMediatorClass::curSelRowIndex;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::ProjectLstboxCntrlView=TPLMediatorClass::ProjectLstboxCntrlView;
			TPLMediatorClass::tableFlag=0;




							//End shail


							//CA("break 14 ");
					  
						    
							f.Clear();
							f.AppendNumber(TPLMediatorClass::curSelRowIndex);
                            //if(PREMediatorClass::whichlistbox==1)
							//CA("End");
							
						}

		}		      
		result= kTrue;
			
         break;

		}
	   

      result= kFalse;
			
         break;

		 

		 
			
case kDownArrowKey: 
		//CA("Down");
	switch(TPLMediatorClass::curSelLstbox)
	{
	case 1:
		if(TPLMediatorClass::curSelLstbox==1)
			
		InterfacePtr<IListBoxController> listCnt1(TPLMediatorClass::PFLstboxCntrlView,IID_ILISTBOXCONTROLLER);
		{
		    //CA("I m  in checking");
			int32 PFRow=-1;      
			//CA("Break 8");     
			if(TPLMediatorClass::curSelRowIndex>=TPLMediatorClass::count1){CA("i m more than max index");
					//	return kFalse;
						break;
			}
						if(TPLMediatorClass::count1!=0)
						{
					//		CA("break 1  in checking");
					    InterfacePtr<IListBoxController> listCnt1(TPLMediatorClass::PFLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			           if(listCnt1 == nil) 
				       break;
					//  CA("break 2  in checking");
					   int32 NewINdex = TPLMediatorClass::curSelRowIndex+1;
					//  	 CA("Break 9");	
					  listCnt1->Select(NewINdex,kTrue,kTrue  ) ;
					 //  CA("Break 10");
					      //  TPLMediatorClass::curSelRowIndex--;
							//TPLMediatorClass::curSelRowIndex++;
                         //   CA("break 10.5 ");
							TPLMediatorClass::curSelRowIndex= NewINdex;
						//	CA("break 10.6 ");
				
							
				//added by shail

				//const int kSelectionLength =  curSelection.Length() ;

			int curSelRowIndex=-1;
			PMString curSelRowStr("");
            CA("break 11  in checking");
			/* getting current selection here */
			
				//curSelRowIndex=curSelection[0];
				TPLListboxData PFLstboxData;
				int32 vectSize=PFLstboxData.returnListVectorSize(1);
               
				/* For getting data of the selected row of listbox */
				
				for(int y=0;y<vectSize;y++)
				{
					
					
					if(y==TPLMediatorClass::curSelRowIndex)
					{
						TPLListboxInfo PFListboxInfo=PFLstboxData.getData(1,y);
						curSelRowStr=PFListboxInfo.name;
						//CA(PGListboxInfo.name);
						TPLMediatorClass::imageFlag=PFListboxInfo.isImageFlag;
						break;
						
					}
					
				}
				
			
			TPLMediatorClass::curSelLstbox=1;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			PFRow =TPLMediatorClass::curSelRowIndex ;
			TPLMediatorClass::curSelRowString=curSelRowStr;
		//	TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			TPLMediatorClass::tableFlag=0;
           // CA("break 16  in checking");
							//end shail

					  
						    
							f.Clear();
							f.AppendNumber(TPLMediatorClass::curSelRowIndex);
                            //if(PREMediatorClass::whichlistbox==1)
							////CA("End");
						
							
						}

		}
		      
		result= kTrue;
			
         break;

	case 2:
		if(TPLMediatorClass::curSelLstbox==2)
		InterfacePtr<IListBoxController> listCnt2(TPLMediatorClass::PGLstboxCntrlView,IID_ILISTBOXCONTROLLER);
		{
		    int32 PGRow=-1;      
			// //CA("Break 8");     
			if(TPLMediatorClass::curSelRowIndex>=TPLMediatorClass::count2){
						return kTrue;
						break;
			}
						if(TPLMediatorClass::count2!=0){
			            InterfacePtr<IListBoxController> listCnt2(TPLMediatorClass::PGLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			           if(listCnt2 == nil) 
				       break;
					  int32 NewINdex = TPLMediatorClass::curSelRowIndex+1;
					  //	 //CA("Break 9");	
					  listCnt2->Select(NewINdex,kTrue,kTrue  ) ;
					   ////CA("Break 10");
					       // TPLMediatorClass::curSelRowIndex--;
							//TPLMediatorClass::curSelRowIndex++;
                          //  //CA("break 10.5 ");
							TPLMediatorClass::curSelRowIndex= NewINdex;
							////CA("break 10.6 ");
				
							
				//added by shail

				//const int kSelectionLength =  curSelection.Length() ;

			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			
				//curSelRowIndex=curSelection[0];
				TPLListboxData PGLstboxData;
				int32 vectSize=PGLstboxData.returnListVectorSize(2);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==TPLMediatorClass::curSelRowIndex)
					{
						TPLListboxInfo PGListboxInfo=PGLstboxData.getData(2,y);
						curSelRowStr=PGListboxInfo.name;
						//CA(PGListboxInfo.name);
						TPLMediatorClass::imageFlag=PGListboxInfo.isImageFlag;
						break;
					}
				}
			
			TPLMediatorClass::curSelLstbox=2;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			PGRow =TPLMediatorClass::curSelRowIndex ;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			//TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			TPLMediatorClass::tableFlag=0;

							//end shail

					  
						    
							f.Clear();
							f.AppendNumber(TPLMediatorClass::curSelRowIndex);
                            //if(PREMediatorClass::whichlistbox==1)
							////CA("End");
							
						}

		}
		      
		result= kTrue;
			
         break;
	
		 case 3:
		if(TPLMediatorClass::curSelLstbox==3)
		InterfacePtr<IListBoxController> listCnt3(TPLMediatorClass::PRLstboxCntrlView,IID_ILISTBOXCONTROLLER);
		{
		    int32 PRRow=-1;       
			////CA("Break 11");     
			if(TPLMediatorClass::curSelRowIndex>=TPLMediatorClass::count3){
				return kTrue;
			    break;
			}
						
			if(TPLMediatorClass::count3!=0){
			InterfacePtr<IListBoxController> listCnt3(TPLMediatorClass::PRLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			if(listCnt3 == nil) 
			break;
			int32 NewINdex = TPLMediatorClass::curSelRowIndex+1;
			listCnt3->Select(NewINdex,kTrue,kTrue  ) ;
					
			TPLMediatorClass::curSelRowIndex= NewINdex;

			//added by shail
			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			
				//curSelRowIndex=curSelection[0];
			TPLListboxData PRLstboxData;
			int32 vectSize=PRLstboxData.returnListVectorSize(3);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==TPLMediatorClass::curSelRowIndex)
					{
						TPLListboxInfo PRListboxInfo=PRLstboxData.getData(3,y);
						curSelRowStr=PRListboxInfo.name;
						////CA(PRListboxInfo.name);
						TPLMediatorClass::tableFlag=PRListboxInfo.tableFlag;
						TPLMediatorClass::imageFlag=PRListboxInfo.isImageFlag;
						break;
					}
				}
			
			TPLMediatorClass::curSelLstbox=3;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			PRRow = TPLMediatorClass::curSelRowIndex;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::PRLstboxCntrlView=TPLMediatorClass::PRLstboxCntrlView;
				
		}

		}
		      
		result= kTrue;
			
         break;

	

		 case 4:
		if(TPLMediatorClass::curSelLstbox==4)
		InterfacePtr<IListBoxController> listCnt4(TPLMediatorClass::ItemLstboxCntrlView,IID_ILISTBOXCONTROLLER);
		{
		           
			int32 ItemRow=-1;
			       ////CA("Break 11");     
			if(TPLMediatorClass::curSelRowIndex>=TPLMediatorClass::count4){
				return kTrue;
			    break;
			}
						if(TPLMediatorClass::count4!=0){
			            InterfacePtr<IListBoxController> listCnt4(TPLMediatorClass::ItemLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			           if(listCnt4 == nil) 
				       break;
					  int32 NewINdex = TPLMediatorClass::curSelRowIndex+1;
					  	// //CA("Break 12");	
					  listCnt4->Select(NewINdex,kTrue,kTrue  ) ;
					  // //CA("Break 10");
					        //TPLMediatorClass::curSelRowIndex--;
							//TPLMediatorClass::curSelRowIndex++;
                            ////CA("break 13 ");
							TPLMediatorClass::curSelRowIndex= NewINdex;
                           //Added by shail
					int curSelRowIndex=-1;
		          PMString curSelRowStr("");

			/* getting current selection here */
			
				//curSelRowIndex=curSelection[0];

				TPLListboxData ItemLstboxData;
				
				int32 vectSize=ItemLstboxData.returnListVectorSize(4);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==TPLMediatorClass::curSelRowIndex)
					{
						TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(4,y);
						curSelRowStr=ItemListboxInfo.name;
						//CA(ItemListboxInfo.name);
						TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
						break;
					}
				}
			
			TPLMediatorClass::curSelLstbox=4;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
		    ItemRow = TPLMediatorClass::curSelRowIndex;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::ItemLstboxCntrlView=TPLMediatorClass::ItemLstboxCntrlView;
			TPLMediatorClass::tableFlag=0;

							//end shail


							////CA("break 14 ");
					  
						    
							f.Clear();
							f.AppendNumber(TPLMediatorClass::curSelRowIndex);
                            //if(PREMediatorClass::whichlistbox==1)
							////CA("End");
							
						}

		}		      
		result= kTrue;
			
         break;

		  case 5:
		if(TPLMediatorClass::curSelLstbox==5)
		InterfacePtr<IListBoxController> listCnt5(TPLMediatorClass::ProjectLstboxCntrlView,IID_ILISTBOXCONTROLLER);
		{
		    int32 ProjectRow=-1;      
			
			
			if(TPLMediatorClass::curSelRowIndex==TPLMediatorClass::count5){ 
				return kTrue;
			    break;
			}
				
			
						if(TPLMediatorClass::count5!=0){
			            InterfacePtr<IListBoxController> listCnt5(TPLMediatorClass::ProjectLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			           if(listCnt5 == nil) 
				       break;
					  int32 NewINdex = TPLMediatorClass::curSelRowIndex+1;
					  	 ////CA("Break 12");	
					  listCnt5->Select(NewINdex,kTrue,kTrue  ) ;
					   
							TPLMediatorClass::curSelRowIndex= NewINdex;

							//Added by shail
              int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			
				//curSelRowIndex=curSelection[0];

				TPLListboxData ProjectLstboxData;
				
				int32 vectSize=ProjectLstboxData.returnListVectorSize(5);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==TPLMediatorClass::curSelRowIndex)
					{
						TPLListboxInfo ProjectListboxInfo=ProjectLstboxData.getData(5,y);
						curSelRowStr=ProjectListboxInfo.name;
						//CA(ProjectListboxInfo.name);
						TPLMediatorClass::imageFlag=ProjectListboxInfo.isImageFlag;
						break;
					}
				}
			
			TPLMediatorClass::curSelLstbox=5;
			TPLMediatorClass::curSelRowIndex=TPLMediatorClass::curSelRowIndex;
			ProjectRow = TPLMediatorClass::curSelRowIndex;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::ProjectLstboxCntrlView=TPLMediatorClass::ProjectLstboxCntrlView;
			TPLMediatorClass::tableFlag=0;




							//End shail

							////CA("break 14 ");
					  
						    
							f.Clear();
							f.AppendNumber(TPLMediatorClass::curSelRowIndex);
                            //if(PREMediatorClass::whichlistbox==1)
							////CA("End");
							
						}

			

		}		      
		result= kTrue;
			
         break;

		}
	   

      result= kTrue;
			
         break;


default :		result=kFalse;	
					break;
	
	
	}
	
	////CA(f);
return result;								
//return CEventHandler::KeyDown(e);
}

/*bool16 ActSel::KeyUp(IEvent* e)
{
	bool16 result=kFalse;
	if(TPLMediatorClass::TotalRows!=0)
		//CA("up");
         return CEventHandler::KeyUp(e);
}*/

#include "VCPlugInHeaders.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "TPLID.h"
#include "SDKListBoxHelper.h"
#include "IPanelControlData.h"
#include "TPLMediatorClass.h"
#include "TPLListboxData.h"
#include "IListControlData.h"
#include "IAppFramework.h"
#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TPLItemListBoxObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

int32 ItemRow = -1;
class TPLItemListBoxObserver : public CObserver
{
public:
	TPLItemListBoxObserver(IPMUnknown *boss);
	~TPLItemListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(TPLItemListBoxObserver, kTPLItemListBoxObserverImpl)

TPLItemListBoxObserver::TPLItemListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

TPLItemListBoxObserver::~TPLItemListBoxObserver()
{
}

void TPLItemListBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void TPLItemListBoxObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_ILISTCONTROLDATA);    //----------Change In CS3 
	}
}

void TPLItemListBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{//CAlert::InformationAlert(__FUNCTION__);
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}

	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) 
	{
		do 
		{
			ItemRow = -1;
			InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if(panel==nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLItemListBoxObserver::panel == nil");			
				break;
			}

			SDKListBoxHelper listHelper(this, kTPLPluginID);

			IControlView* ItemLstboxCntrlView= listHelper.FindCurrentListBox(panel, 4);
			if(ItemLstboxCntrlView== nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLItemListBoxObserver::ItemLstboxCntrlView == nil");			
				break;
			}

			InterfacePtr<IListBoxController> listCntl(ItemLstboxCntrlView,IID_ILISTBOXCONTROLLER);
			if(listCntl == nil) 
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLItemListBoxObserver::listCntl == nil");			
				break;
			}

			K2Vector<int32> curSelection ;
			listCntl->GetSelected( curSelection ) ;

			const int kSelectionLength =  curSelection.Length() ;

			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			if (kSelectionLength> 0 )
			{
				curSelRowIndex=curSelection[0];

				TPLListboxData ItemLstboxData;
				
				int32 vectSize=ItemLstboxData.returnListVectorSize(4);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==curSelRowIndex)
					{
						TPLListboxInfo ItemListboxInfo=ItemLstboxData.getData(4,y);
						curSelRowStr=ItemListboxInfo.name;
						TPLMediatorClass::imageFlag=ItemListboxInfo.isImageFlag;
						break;
					}
				}
			}
			TPLMediatorClass::curSelLstbox=4;
			TPLMediatorClass::curSelRowIndex=curSelRowIndex;
			ItemRow = curSelRowIndex;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::ItemLstboxCntrlView=ItemLstboxCntrlView;
			TPLMediatorClass::tableFlag=0;
		} while(0);
	}
}


#ifndef __TPLLoginEventsHandler_h__
#define __TPLLoginEventsHandler_h__

#include "VCPlugInHeaders.h"
#include "IRegisterEvent.h"
#include "TPLID.h"
#include "ILoginEvent.h"

class TPLLoginEventsHandler : public CPMUnknown<ILoginEvent>
{
public:
	TPLLoginEventsHandler(IPMUnknown* );
	~TPLLoginEventsHandler();
	bool16 userLoggedIn();
	bool16 userLoggedOut();
	bool16 resetPlugIn();
};

#endif
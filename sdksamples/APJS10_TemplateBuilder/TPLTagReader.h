#ifndef __TPLTAGREADER_H__
#define __TPLTAGREADER_H__

#include "VCPluginHeaders.h"
#include "TPLTagStruct.h"

class TPLTagReader
{
public:
	TPLTagList getTagsFromBox(UIDRef);
	TPLTagList getFrameTags(UIDRef);
	bool16 GetUpdatedTag(TPLTagStruct&);
private:
	void getTextFrameTags(void);
	void getGraphicFrameTags(void);
	bool16 getCorrespondingTagAttributes
		(const PMString&, const PMString&, TPLTagStruct&);
	TPLTagList tList;
	UIDRef boxUIDRef;
	UID textFrameUID;
};
#endif
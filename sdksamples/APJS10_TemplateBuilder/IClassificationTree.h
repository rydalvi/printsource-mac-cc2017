#ifndef __IClassificationTree_h__
#define __IClassificationTree_h__

#include "VCPlugInHeaders.h"
#include "CTBID.h"
#include "IPMUnknown.h"

class IClassificationTree : public IPMUnknown
{
public:
	enum	{kDefaultIID = IID_ICLASSTREE};
	virtual bool16 ShowClassificationTree
		(int32& clsId, PMString& clasName, PMString& hierarchy)=0; 
};

#endif
//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__


// Japanese string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_jaJP)
{
        k_jaJP,	// Locale Id
        0,		// Character encoding converter

        {
        	// ----- Menu strings
                kTPLCompanyKey,					kTPLCompanyValue,
                kTPLAboutMenuKey,					kTPLPluginName "[JP]...",
                kTPLPluginsMenuKey,				kTPLPluginName "[JP]",

                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_jaJP,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings
                kTPLPanelTitleKey,				kTPLPluginName "[JP]",
                kTPLStaticTextKey,				kTPLPluginName "[JP]",

              // ----- Error strings

                // ----- Misc strings
                kTPLAboutBoxStringKey,				kTPLPluginName " [JP], version " kTPLVersion " by " kTPLAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_jaJP,
				kTPLItemOneStringKey,				"TemplatePalette::ItemOne[US]",
				kTPLShowCategoriesStringKey,		"Show Categories",
				kTPLAddAsAccessoryAttrStringKey,	"Add as Accessory Attribute",
				kTPLAddAsComponentAttrStringKey,	"Add as Component Attribute",
				kTPLAddAsCrossRefAttrStringKey,		"Add as Cross Reference Attribute ",
				kTPLAddAsMMYAttrStringKey,			"Add as MMY Attribute ",
				kTPLAddAsMMYSortAttrStringKey,		"Add as MMY Sort Attribute ",
				kTPLTagInfoStringKey,				"Tag Info ",
				kTPLAttributesStringKey,			"Attributes",  
				
				kSelectStringKey,					"Select:", 
				kAssignCurrentFrameStringKey,		"Assign to the Current Frame",
				kCreateNewFrameStringKey,			"Create New Frame",
				kFrameTagStringKey,					"Frame Tag",
				kAutoResizeStringKey,				"Auto Resize",
				kOverflowStringKey,					"Overflow",
				kOutputasStringKey,					"Output as:",
				kSpreadBasedStringKey,				"Spread Based",
				kIncludeHeaderStringKey,			"Include Header",
				kDisplayNameStringKey,				"Display Name",
				kDeleteIfEmptyStringKey,			"Delete If Empty",
				kSprayItemPerFrameStringKey,		"SprayItemPerFrame",
				kHorizontalFlowStringKey,			"HorizontalFlow",
				kItemListStringKey,					"Item List",
				kTabbedTextStringKey,				"Tabbed Text",
				kTableStringKey,					"Table",
				kTableNameStringKey,				"Table Name",
				kImageDescriptionStringKey,			"ImageDescription",
				kHorizontalFlowStringKey1,			"Horizontal Flow",
				kVerticalFlowStringKey,				"Vertical Flow",
				kReloadListStringKey,				"Reload List",
				kPrintsourcetStringKey,				"Main:&PRINTsource9",
				kTPLFrameOptionsStringKey,			"Frame Options:",
				kMainStringKey,						"Main",
                kIncludeListNameStringKey,          "Incl. ListName",
                kOutputAsSwatchStringKey,           "Output As Swatch",
        }

};

#endif // __ODFRC__

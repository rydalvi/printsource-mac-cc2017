#include "VCPlugInHeaders.h"
#include "IClassIDData.h"
#include "IControlView.h"
#include "ISignalMgr.h"
#include "IDocumentSignalData.h"
#include "IUIDData.h"
#include "IObserver.h"
#include "CreateObject.h"
#include "CResponder.h"
#include "TPLID.h"
#include "TPLCommonFunctions.h"
#include "CAlert.h"

#include "CAlert.h"

#define FILENAME			PMString("TPLDocResponder.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
class TPLDocResponder : public CResponder
{
	public:
		TPLDocResponder(IPMUnknown* boss);
		virtual void Respond(ISignalMgr* signalMgr);
};

CREATE_PMINTERFACE(TPLDocResponder, kTPLDocResponderImpl)

TPLDocResponder::TPLDocResponder(IPMUnknown* boss) :
	CResponder(boss)
{
}

void TPLDocResponder::Respond(ISignalMgr* signalMgr)
{
	// Get the service ID from the signal manager
	ServiceID serviceTrigger = signalMgr->GetServiceID();

	// Get a UIDRef for the document.  It will be an invalid UIDRef
	// for BeforeNewDoc, BeforeOpenDoc, AfterSaveACopy, and AfterCloseDoc because the
	// document doesn't exist at that point.
	InterfacePtr<IDocumentSignalData> docData(signalMgr, UseDefaultIID());
	if (docData == nil)
	{
		ASSERT_FAIL("Invalid IDocumentSignalData* - TPLDocResponder::Respond");
		return;
	}
	UIDRef docRef = docData->GetDocument();

	// Take action based on the service ID
	switch (serviceTrigger.Get())
	{
		case kAfterNewDocSignalResponderService:
		case kDuringOpenDocSignalResponderService:
		{
			InterfacePtr<IObserver> docObserver(docRef, IID_IPSTLSTOBSERVER);
			if (docObserver != nil)
			{
				docObserver->AutoAttach();
			}
			break;
		}
		case kBeforeCloseDocSignalResponderService:
		{
			InterfacePtr<IObserver> docObserver(docRef, IID_IPSTLSTOBSERVER);
			if (docObserver != nil)
			{
				docObserver->AutoDetach();
			}
			break;
		}
		default:
			break;
	}

}


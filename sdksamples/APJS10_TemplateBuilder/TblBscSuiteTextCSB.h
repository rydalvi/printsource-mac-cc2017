#include "VCPlugInHeaders.h"

#ifndef __TBLBSCSUITETEXTCSB_H__
#define __TBLBSCSUITETEXTCSB_H__


// Project includes:
#include "TPLID.h"
#include "ITblBscSuite.h"
#include "ITableModel.h"



class TblBscSuiteTextCSB : public CPMUnknown<ITblBscSuite>
{
public:
	/** Constructor.
		@param boss refers to boss class on which this is aggregated.
	*/
	TblBscSuiteTextCSB (IPMUnknown *boss);
	
	/** Destructor.
	*/
	virtual ~TblBscSuiteTextCSB (void);

public:
	/**	See ITblBscSuite::CanInsertTable.
	*/
	virtual bool16 CanInsertTable() const;

	/**	See ITblBscSuite::InsertTable.
	*/
	virtual ErrorCode InsertTable(int32 numRows, int32 numCols);

	/**	See ITblBscSuite::CanSetCellText.
	*/
	virtual bool16 CanSetCellText(int32 row, int32 col) const;

	/**	See ITblBscSuite::SetCellText.
	*/
	virtual ErrorCode SetCellText(int32 row, int32 col, const WideString& text);

	/**	See ITblBscSuite::CanGetCellText.
	*/
	virtual bool16 CanGetCellText(int32 row, int32 col) const;

	/**	See ITblBscSuite::GetCellText.
	*/
	virtual void GetCellText(int32 row, int32 col, WideString& text) const;

	virtual bool16 GetCellRange(GridArea& gridArea) ;

	//added on 3July
	virtual bool16 queryTableModel(InterfacePtr<ITableModel> & iTableModel);

	/**	Figure out the table associated with the text selection.
		@return reference to table model, nil otherwise.
	*/
	ITableModel* queryTableModel() const;
 private:
	/**	Figure out the table associated with the text selection and
		check the row and column are valid.
		@param row coord of row to validate
		@param col coord of column to validate
		@return reference to table model if row and column valid, nil otherwise.
	*/
	ITableModel* queryTableModelAndValidateRowCol(int32 row, int32 col) const;

	/**	Set the text of the given table cell.
		@return kSuccess if operation succeeded, kFailure otherwise.
		@param tableModel reference to the table model
		@param row 
		@param col
		@param text
	*/
	ErrorCode setCellText(InterfacePtr<ITableModel>& tableModel, 
							int32 row, int32 col, 
							const WideString& text);

	/**	Extract a string containing the text in the given text story thread.
		@param textStoryThread
		@param text 
	*/
	void getText(const ITextStoryThread* textStoryThread, WideString& text) const;

};
#endif
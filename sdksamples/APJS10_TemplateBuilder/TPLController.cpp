#include "VCPlugInHeaders.h"
#include "CDialogController.h"
#include "SystemUtils.h"
#include "TPLID.h"
#include "CAlert.h"

#define CA(x)	CAlert::InformationAlert(x)

class TPLController : public CDialogController
{
	public:
		TPLController(IPMUnknown* boss) : CDialogController(boss) {}
		virtual ~TPLController() {}
		virtual void InitializeFields();
		virtual WidgetID ValidateFields();
		virtual void ApplyFields(const WidgetID& widgetId);
};

CREATE_PMINTERFACE(TPLController, kTPLControllerImpl)

void TPLController::InitializeFields() 
{
	CDialogController::InitializeFields();
	
}

WidgetID TPLController::ValidateFields() 
{
	WidgetID result = CDialogController::ValidateFields();
	return result;
}

void TPLController::ApplyFields(const WidgetID& widgetId) 
{
	PMString resultString;
	resultString = this->GetTextControlData( kTPLDropDownWidgetID);
	resultString.Translate();
//	CA(resultString);
}

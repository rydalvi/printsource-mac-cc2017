#ifndef _ITextMiscellanySuite_
#define _ITextMiscellanySuite_

// Interface includes:
#include "IPMUnknown.h"
#include "TPLID.h"
// #include "ISpecifier.h"        //Removed from Indesign CS3 API


class ITextMiscellanySuite : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_ITPLTEXTMISCELLANYSUITEE };
public:
//	virtual bool16 GetCurrentSpecifier(ISpecifier * & )=0;         //CS3 change
	virtual bool16 GetUidList(UIDList &)=0;
	virtual bool16 GetFrameUIDRef(UIDRef &)=0;
	virtual bool16 GetCaretPosition(TextIndex &pos)=0;
	virtual bool16 GetTextSelectionRange(TextIndex &start, TextIndex &end)=0;
};
#endif // _ITextMiscellanySuite_

#include "VCPlugInHeaders.h"
#include "ITemplatesDialogCloser.h"
#include "CPMUnknown.h"
#include "TPLActionComponent.h"
#include "TPLID.h"
#include "TPLMediatorClass.h"
#include "TPLSelectionObserver.h"
#include "IApplication.h"
#include "IPanelMgr.h"
#include "CAlert.h"

#define CA(X)		CAlert::InformationAlert(X)

extern double CurrentClassID;
extern bool16 IsRefresh ;

class CTemplatesDialogCloser:public CPMUnknown<ITemplatesDialogCloser>
{	
public:
	CTemplatesDialogCloser(IPMUnknown* );
	~CTemplatesDialogCloser();	
	void closeTemplDialog(void);
	void refreshItemTreeForClassID(double ClassId);
};

CREATE_PMINTERFACE(CTemplatesDialogCloser,kTemplatesDialogCloserImpl)

CTemplatesDialogCloser::CTemplatesDialogCloser(IPMUnknown* boss):CPMUnknown<ITemplatesDialogCloser>(boss)
{}

CTemplatesDialogCloser::~CTemplatesDialogCloser()
{
	
}

void CTemplatesDialogCloser::closeTemplDialog(void)
{
	//CAlert::InformationAlert(__FUNCTION__);
	/*	TPLActionComponent actionComponetObj(this);
	actionComponetObj.CloseTemplatePalette();    */
}

void CTemplatesDialogCloser::refreshItemTreeForClassID(double ClassId)
{
	//CAlert::InformationAlert("Inside CTemplatesDialogCloser::refreshItemTreeForClassID ");
	TPLMediatorClass::currTempletSelectedClassID = ClassId;
	InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
	if(app == NULL) 
	{ 
		//CA("No Application");
		return;
	}
	
	InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
	if(panelMgr == NULL) 
	{	
		//CA("No IPanelMgr");	
		return;
	}
	
	IControlView* myPanel = panelMgr->GetVisiblePanel(kTPLPanelWidgetID);
	if(!myPanel) 
	{	
		//CA("No TPLPanelWidgetControlView");
		return;
	}
	if(!panelMgr->IsPanelWithWidgetIDShown(kTPLPanelWidgetID))
	return ;

	CurrentClassID = ClassId;
	TPLMediatorClass::loadData=kTrue;
	TPLSelectionObserver selObserver(this);
	IsRefresh = kTrue;
	selObserver.loadPaletteData();
	IsRefresh = kFalse;
}

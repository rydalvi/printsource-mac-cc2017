#include "VCPlugInHeaders.h"
#include "IControlView.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IntNodeID.h"
#include "CTreeViewWidgetMgr.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SysControlIds.h"
#include "TPLID.h"
#include "TPLTreeModel.h"
#include "TPLDataNode.h"
#include "TPLMediatorClass.h"
#include "CAlert.h"
#include "TPLTreeDataCache.h"
//#include "ISpecialChar.h"
//#include "ISpecialChar.h"
#include "CAlert.h"
#include "IAppFramework.h"
//#include "IMessageServer.h"
//#define FILENAME			PMString("TPLTreeViewWidgetMgr.cpp")
//#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
//#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
#define CA(X) CAlert::InformationAlert(X); 

extern TPLDataNodeList ProjectDataNodeList;
extern int32 RowCountForTree;
extern int32 SelectedRowNo;
extern TPLDataNodeList PFDataNodeList;
extern TPLDataNodeList PGDataNodeList;
extern TPLDataNodeList PRDataNodeList;
extern TPLDataNodeList ITEMDataNodeList;
extern TPLDataNodeList CatagoryDataNodeList;


class TPLTreeViewWidgetMgr: public CTreeViewWidgetMgr
{
public:
	TPLTreeViewWidgetMgr(IPMUnknown* boss);
	virtual ~TPLTreeViewWidgetMgr() {}
	virtual	IControlView*	CreateWidgetForNode(const NodeID& node) const;
	virtual	WidgetID		GetWidgetTypeForNode(const NodeID& node) const;
	virtual	bool16 ApplyNodeIDToWidget
		( const NodeID& node, IControlView* widget, int32 message = 0 ) const;
	virtual PMReal GetIndentForNode(const NodeID& node) const;
private:
	PMString getNodeText(const int32& uid, int32 *RowNo) const;
	void indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const;
	enum {ePFTreeIndentForNode=3};
};	

CREATE_PMINTERFACE(TPLTreeViewWidgetMgr, kTPLTreeViewWidgetMgrImpl)

TPLTreeViewWidgetMgr::TPLTreeViewWidgetMgr(IPMUnknown* boss) :
	CTreeViewWidgetMgr(boss)
{	//CA("Inside WidgetManager constructor");
}

IControlView* TPLTreeViewWidgetMgr::CreateWidgetForNode(const NodeID& node) const
{	//CA("CreateWidgetForNode");
	IControlView* retval =
		(IControlView*) ::CreateObject(::GetDataBase(this),
							RsrcSpec(LocaleSetting::GetLocale(), 
							kTPLPluginID, 
							kViewRsrcType, 
							kTPLTreePanelNodeRsrcID),IID_ICONTROLVIEW);
	ASSERT(retval);
	return retval;
}

WidgetID TPLTreeViewWidgetMgr::GetWidgetTypeForNode(const NodeID& node) const
{	//CA("GetWidgetTypeForNode");
	return kTPLTreePanelNodeWidgetID;
}

bool16 TPLTreeViewWidgetMgr::ApplyNodeIDToWidget
(const NodeID& node, IControlView* widget, int32 message) const
{	//CA("ApplyNodeToWidget");
	CTreeViewWidgetMgr::ApplyNodeIDToWidget( node, widget );
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return kFalse;
	}
	do
	{
	
		InterfacePtr<IPanelControlData> panelControlData(widget, UseDefaultIID());
		ASSERT(panelControlData);
		if(panelControlData==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::panelControlData is nil");		
			break;
		}

		IControlView*   expanderWidget = panelControlData->FindWidget(kTPLTreeNodeExpanderWidgetID);
		ASSERT(expanderWidget);
		if(expanderWidget == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::expanderWidget is nil");		
			break;
		}

		IControlView*   TextIconWidget = panelControlData->FindWidget(kTPLTreeCopyIconSuiteWidgetID);
		ASSERT(TextIconWidget);
		if(TextIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::TextIconWidget is nil");
			break;
		}

		IControlView*   TextParaIconWidget = panelControlData->FindWidget(kTPLTreeCopyParaIconSuiteWidgetID);
		ASSERT(TextParaIconWidget);
		if(TextParaIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::TextParaIconWidget is nil");		
			break;
		}

		IControlView*   ImageIconWidget = panelControlData->FindWidget(kTPLTreeImageIconSuiteWidgetID);
		ASSERT(ImageIconWidget);
		if(ImageIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::ImageIconWidget is nil");
			break;
		}

		IControlView*   ParaImageIconWidget = panelControlData->FindWidget(kTPLTreeParaImageIconSuiteWidgetID);
		ASSERT(ParaImageIconWidget);
		if(ParaImageIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::ParaImageIconWidget is nil");
			break;
		}

		IControlView*   PVBoxIconWidget = panelControlData->FindWidget(kTPLTreePVBoxIconSuiteWidgetID);
		ASSERT(PVBoxIconWidget);
		if(PVBoxIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::PVBoxIconWidget is nil");
			break;
		}

		IControlView*   TableIconWidget = panelControlData->FindWidget(kTPLTreeTableIconSuiteWidgetID);
		ASSERT(TableIconWidget);
		if(TableIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::ApplyNodeIDToWidget::TableIconWidget is nil");
			break;
		}
		IControlView*   ListIconWidget = panelControlData->FindWidget(kTPLTreeListIconSuiteWidgetID);
		if(ListIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::ApplyNodeIDToWidget::ListIconWidget is nil");
			break;
		}
	//following controlView added on 13/12/06 by Tushar 
		IControlView*   DollerIconWidget = panelControlData->FindWidget(kTPLTreeDollorIconSuiteWidgetID);
		ASSERT(DollerIconWidget);
		if(DollerIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::DollerIconWidget is nil");		
			break;
		}
	//added on 10/01/06 by Dattatray
		IControlView*   ProductGroupNameIconWidget = panelControlData->FindWidget(kTPLProductGroupNameIconSuiteWidgetID);
		ASSERT(ProductGroupNameIconWidget);
		if(ProductGroupNameIconWidget == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::ProductGroupNameIconWidget is nil");		
			break;
		}
		

		InterfacePtr<const ITreeViewHierarchyAdapter>   adapter(this, UseDefaultIID());
		ASSERT(adapter);
		if(adapter==nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::adapter is nil");		
			break;
		}

		TPLTreeModel model;
		TreeNodePtr<IntNodeID>  uidNodeIDTemp(node);
		int32 uid= uidNodeIDTemp->Get();

		TPLDataNode pNode;
		TPLTreeDataCache dc;

		dc.isExist(uid, pNode);

		TreeNodePtr<IntNodeID>  uidNodeID(node);
		ASSERT(uidNodeID);
		if(uidNodeID == nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::uidNodeID is nil");		
			break;
		}

		int32 *RowNo= NULL;
		int32 LocalRowNO;
		RowNo = &LocalRowNO;

		PMString stringToDisplay( this->getNodeText(uidNodeID->Get(), RowNo)/*"Amit Awasthi"*/);
		stringToDisplay.SetTranslatable( kFalse );
		int result = -1; 

		/*PMString QWE("SelectRowNIO : ");
		QWE.AppendNumber(SelectedRowNo);*/
		//CA(QWE);
		switch(SelectedRowNo)
		{
			case 0:
				if(TPLMediatorClass::IsOneSourceMode)
				{
					result = CatagoryDataNodeList[*RowNo].getHitCount();
				}
				else
				{
					result = ProjectDataNodeList[*RowNo].getHitCount();
				}
				break;
			case 1:
				result = PFDataNodeList[*RowNo].getHitCount();
				break;
			case 2:
				result = PGDataNodeList[*RowNo].getHitCount();
				break;
			case 3:
				result = PRDataNodeList[*RowNo].getHitCount();
				break;
			case 4:
				result = ITEMDataNodeList[*RowNo].getHitCount();
				break;

		}
		/*PMString ASD("result : ");
				ASD.AppendNumber(result);
				ASD.Append("Row no : ");
				ASD.AppendNumber(*RowNo);
				CA(ASD);*/
				
			switch(result)
			{
			case 0:
				
				ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				TextIconWidget->ShowView();
				ListIconWidget->HideView();
				break;
			case 1:
				TextIconWidget->HideView();
				TableIconWidget->HideView();
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				ImageIconWidget->ShowView();
				ListIconWidget->HideView();
				break;
			case 2:
				TextIconWidget->HideView();
				ImageIconWidget->HideView();
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				TableIconWidget->ShowView();
				ListIconWidget->HideView();
				break;
			
			case 3:
				ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				TextParaIconWidget->ShowView();
				ListIconWidget->HideView();
				break;

			case 4:				
				ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextIconWidget->HideView();				
				TextParaIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				ParaImageIconWidget->ShowView();
				ListIconWidget->HideView();
				break;

			case 5:						
				ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextIconWidget->HideView();				
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				PVBoxIconWidget->ShowView();
				ListIconWidget->HideView();
				break;

		//case 6: added by Tushar on 13/12/06
			case 6:						
				ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextIconWidget->HideView();				
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				DollerIconWidget->ShowView();
				ListIconWidget->HideView();
				break;
		//case 7: added by Dattatray on 10-01-07
			case 7:						
				ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextIconWidget->HideView();				
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->ShowView();
				ListIconWidget->HideView();
				break;
			case 8:						
				ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextIconWidget->HideView();				
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				ListIconWidget->ShowView();
				break;
			default:
				TextIconWidget->HideView();
				ImageIconWidget->HideView();
				TableIconWidget->HideView();
				TextParaIconWidget->HideView();
				ParaImageIconWidget->HideView();
				PVBoxIconWidget->HideView();
				DollerIconWidget->HideView();
				ProductGroupNameIconWidget->HideView();
				ListIconWidget->HideView();
				break;
			}

			/*if(adapter->GetNumChildren(node)<=0)*/
				expanderWidget->HideView();	
			/*else
				expanderWidget->ShowView();
		}
*/
		IControlView* displayStringView = panelControlData->FindWidget( kTPLTreeNodeNameWidgetID );
		ASSERT(displayStringView);
		if(displayStringView == nil) 
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::displayStringView is nil");
			break;
		}
		InterfacePtr<ITextControlData>  textControlData( displayStringView, UseDefaultIID() );
		ASSERT(textControlData);
		if(textControlData== nil)
		{
			ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLTreeViewWidgetMgr::ApplyNodeIDToWidget::textControlData is nil");		
			break;		
		}
		
		/*InterfacePtr<ISpecialChar> iConverter(static_cast<ISpecialChar*> (CreateObject(kSpecialCharBoss,ISpecialChar::kDefaultIID)));
		if(!iConverter)
		break;
		PMString stringToInsert("");
		stringToInsert.Append(iConverter->handleAmpersandCase(stringToDisplay));
		*/
        stringToDisplay.ParseForEmbeddedCharacters();
		textControlData->SetString(stringToDisplay);
		
		this->indent( node, widget, displayStringView );
	} while(kFalse);
	return kTrue;
}

PMReal TPLTreeViewWidgetMgr::GetIndentForNode(const NodeID& node) const
{	//CA("Inside GetIndentForNode");
	do
	{
		TreeNodePtr<IntNodeID>  uidNodeID(node);
		ASSERT(uidNodeID);
		if(uidNodeID == nil) 
			break;
		
		TPLTreeModel model;
		int nodePathLengthFromRoot = model.GetNodePathLengthFromRoot(uidNodeID->Get());

		if( nodePathLengthFromRoot <= 0 ) 
			return 0.0;
		
		return  PMReal((nodePathLengthFromRoot * ePFTreeIndentForNode)+0.5);
	} while(kFalse);
	return 0.0;
}

PMString TPLTreeViewWidgetMgr::getNodeText(const int32& uid, int32 *RowNo) const
{	//CA("getNodeText");
	TPLTreeModel model;
	return model.ToString(uid, RowNo);
}

void TPLTreeViewWidgetMgr::indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const
{	//CA("Inside indent");
	const PMReal indent = this->GetIndent(node);	
	PMRect widgetFrame = widget->GetFrame();
	widgetFrame.Left() = indent;
	widget->SetFrame( widgetFrame );
	staticTextWidget->WindowChanged();
	PMRect staticTextFrame = staticTextWidget->GetFrame();
	staticTextFrame.Right( widgetFrame.Right()+1500 );
	
	widgetFrame.Right(widgetFrame.Right()+1500);
	widget->SetFrame(widgetFrame);
	
	staticTextWidget->SetFrame( staticTextFrame );
}
	

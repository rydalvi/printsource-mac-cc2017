#include "VCPlugInHeaders.h"

#include <cstdint>
#include "CIDErasablePanelView.h"
//#include "ImageReorgniser.h"
//#include "GlobalFunctions.h"1


// Plug-in includes

#include "TPLID.h"
#include "CAlert.h"

//Alert Macros.////////////
//#include "IPalettePanelUtils.h"
//#include "IPanelControlData.h"
//inline PMString numToPMString(int32 num)
//{
//	PMString x;
//	x.AppendNumber(num);
//	return x;
//}
//#define CA(X) CAlert::InformationAlert \
//	( \
//		PMString("MyControlView.cpp") + PMString("\n") + \
//		PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + \
//		PMString("\n Message : ")+ X \
//	)
//#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//end Alert Macros./////////////

int32 num_of_images = 1 ;
using namespace std;

/** Overrides the ConstrainDimensions to control the maximum and minimum width 
	and height of panel when it is resized.

	@ingroup snippetrunner
	@author Seoras Ashby
*/
class  SnipRunControlView_TMP : public CIDErasablePanelView
{
	public:
		/** 
			Constructor.
		*/
		SnipRunControlView_TMP(IPMUnknown* boss) 
			: CIDErasablePanelView(boss) {/*fowner = boss*/ ;}

		/** 
			Destructor.
		*/
		virtual ~SnipRunControlView_TMP() {;}

		/** Allows the panel size to be constrained.  
			@param dimensions OUT specifies the maximum and minimum width and height of the panel
				when it is resized.
		*/
		virtual PMPoint	ConstrainDimensions(const PMPoint& dimensions) const;

		/**	Clear the SnippetRunner framework log when resizing. 
			The multi line widget log gives some odd behaviour if we don't. 
			Means you lose the log contents when you resize.
			@param newDimensions
			@param invalidate
		*/
		virtual  void Resize(const PMPoint& newDimensions, bool16 invalidate);

	/*private:

		static const int kMinimumPanelWidth;
		static const int kMaximumPanelWidth;
		static const int kMinimumPanelHeight;
		static const int kMaximumPanelHeight;
		IPMUnknown *fowner ; */
};

// define the max/min width and height for the panel
const int	/*SnipRunControlView_TMP::*/kMinimumPanelWidth	=	207 ;
const int 	/*SnipRunControlView_TMP::*/kMaximumPanelWidth	=	800 ;
const int 	/*SnipRunControlView_TMP::*/kMinimumPanelHeight	=	291 ;
const int 	/*SnipRunControlView_TMP::*/kMaximumPanelHeight	=	1800 ;

#pragma mark SnipRunControlView_TMP implementation
/* Make the implementation available to the application.
*/
CREATE_PERSIST_PMINTERFACE(SnipRunControlView_TMP , kTPLPanalControlViewImpl )


/* Allows the panel size to be constrained.  
*/
PMPoint SnipRunControlView_TMP::ConstrainDimensions(const PMPoint& dimensions) const
{
	PMPoint constrainedDim(dimensions);

	//// Width can vary if not above maximum or below minimum
	//if(constrainedDim.X() > kMaximumPanelWidth)
	//{
	//	constrainedDim.X(kMaximumPanelWidth);
	//}
	//else if(constrainedDim.X() < kMinimumPanelWidth)
	//{
	//	constrainedDim.X(kMinimumPanelWidth);
	//}

	//// Height can vary if not above maximum or below minimum
	//if(constrainedDim.Y() > kMaximumPanelHeight)
	//{
	//	constrainedDim.Y(kMaximumPanelHeight);
	//}
	//else if(constrainedDim.Y() < kMinimumPanelHeight)
	//{
	//	constrainedDim.Y(kMinimumPanelHeight);
	//}

	constrainedDim.ConstrainTo( PMRect(kMinimumPanelWidth, kMinimumPanelHeight, kMaximumPanelWidth, kMaximumPanelHeight) );


	return constrainedDim;
}

/*
*/
void SnipRunControlView_TMP::Resize(const PMPoint& newDimensions, bool16 invalidate)
{
	//Getting problem while porting Indesign CS3 resizable bcoz of one of the plugin having 0,0 coordinates so handle like this.
	//no other way after i tired
	//PMReal x=newDimensions.X();
	//PMReal y=newDimensions.Y();
	//this->fEraseBeforeDraw = kTrue;
	//if(x == 0 && y == 0 )
	//{
	//	//CA("Its zero zero");
	//	PMPoint newDimension(PMReal(207),PMReal(291));
	//	 CIDErasablePanelView::Resize(newDimension, invalidate) ;
	//}
	//else
	//{
		CIDErasablePanelView::Resize(newDimensions, invalidate) ;
	/*}
	this->Invalidate();*/
	//CA("SnipRunControlView_TMP::Resize");
	
}






#include "VCPlugInHeaders.h"
#include "WidgetID.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "TPLID.h"

#define CA(x)	CAlert::InformationAlert(x)

class TPLDropDownListObserver : public CObserver
{
	public:
		TPLDropDownListObserver(IPMUnknown *boss);
		~TPLDropDownListObserver();
		virtual void AutoAttach();
		virtual void AutoDetach();
		virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

//CREATE_PMINTERFACE(TPLDropDownListObserver, kTPLDropDownListObserverImpl)

TPLDropDownListObserver::TPLDropDownListObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

TPLDropDownListObserver::~TPLDropDownListObserver()
{
}

void TPLDropDownListObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void TPLDropDownListObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this);
	}
}

void TPLDropDownListObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) 
	{
	/*	do 
		{
			InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if(panel==nil)
				break;

			SDKListBoxHelper listHelper(this, kTmplDlgPluginID);
			IControlView * listBoxCntrlView = listHelper.FindCurrentListBox(panel, 3);
			if(listBoxCntrlView == nil) 
				break;

			InterfacePtr<IListBoxController> listCntl(listBoxCntrlView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
			if(listCntl == nil) 
				break;

			K2Vector<int32> multipleSelection ;
			listCntl->GetSelected( multipleSelection ) ;

			const int kSelectionLength =  multipleSelection.Length() ;
			PMString currentSelectionRowString("");

			/* getting current selection here 
			if (kSelectionLength> 0 )
			{
				selectedRowIndex=multipleSelection[0];
				
				listData taskPanelListData;
				int32 vectSize = taskPanelListData.returnListVectorSize(3);

				/* For getting data of the selected row of listbox 
				for(int y=0; y<vectSize; y++)
				{
					if(y==selectedRowIndex)
					{
						listInfo list = taskPanelListData.getData(3,y);
						currentSelectionRowString = list.name;
						TmplDragEventFinder::isImgflag=list.isImageFlag;
						break;
					}
				}
			}
			TmplDragEventFinder::listboxType = 3;
			TmplDragEventFinder::selectedRowIndex=selectedRowIndex;
			TmplDragEventFinder::currentSelectionRowString=currentSelectionRowString;
			TmplDragEventFinder::cntrlView= listBoxCntrlView;
		} while(0);
	*/
	}
}



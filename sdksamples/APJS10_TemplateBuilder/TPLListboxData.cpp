#include "VCPluginHeaders.h"
#include "string.h"
#include "TPLListboxData.h"
#include "CAlert.h"
#include "TPLMediatorClass.h"


//#include "IMessageServer.h"
inline PMString  numToPMString(int32 num)
	{
		PMString numStr;
		numStr.AppendNumber(num);
		return numStr;
	}

#define CA(X) CAlert::InformationAlert \
	( \
	PMString("BoxReader.cpp") + PMString("\n") + \
    PMString(__FUNCTION__) + PMString("\n") + numToPMString(__LINE__) + X)
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


vector<TPLListboxInfo> TPLListboxData::PFListboxData;
vector<TPLListboxInfo> TPLListboxData::PGListboxData;
vector<TPLListboxInfo> TPLListboxData::PRListboxData;
vector<TPLListboxInfo> TPLListboxData::ItemListboxData;
vector<TPLListboxInfo> TPLListboxData::ProjectListboxData;
vector<TPLListboxInfo> TPLListboxData::CatagoryListboxData;

typedef vector<TPLListboxInfo> TPLListboxInfoVect;

void TPLListboxData::setData
(int16 listboxType, int16 index, double id, double typeId, double LanguageID, 
 PMString name, bool16 state, PMString code, bool16 flag,int32 tableFlag,
 bool16 isEventField,
int32 header,
int32 childTag,
int32 tableType,
int32 flowDir,
int32 isSprayItemPerFrame,
int32 catLevel,
int32 imageIndex,
int32 deleteIfEmpty,
int32 dataType ,
int32 field4,
int32 field5) 
{
	TPLListboxInfo listinfo;
	listinfo.id = id;
	listinfo.index = index;
	listinfo.typeId = typeId;
	listinfo.isSelected = state;
	listinfo.name = name;
	listinfo.LanguageID= LanguageID;
	listinfo.isDragged = kFalse;
	listinfo.code = code;
	listinfo.isImageFlag=flag;
	listinfo.tableFlag=tableFlag;

	listinfo.isEventField = isEventField;
	listinfo.header = header;
	listinfo.childTag = childTag;
	listinfo.tableType = tableType;
	listinfo.flowDir = flowDir;
	listinfo.isSprayItemPerFrame = isSprayItemPerFrame;
	listinfo.catLevel = catLevel;
	listinfo.imageIndex = imageIndex;
	listinfo.dataType = dataType;


	//added on 11July...serial blast day
	listinfo.listBoxType = listboxType;
	listinfo.field4 = field4;
	listinfo.field5 = field5;

	switch(listboxType)
	{
		case 1:
			PFListboxData.push_back(listinfo);
			break;
		case 2:
			PGListboxData.push_back(listinfo);
			break;
		case 3:
			PRListboxData.push_back(listinfo);
			break;
		case 4:
			ItemListboxData.push_back(listinfo);
		case 5:
			if(TPLMediatorClass::IsOneSourceMode)
			{
				CatagoryListboxData.push_back(listinfo);
			}
			else
			{
				ProjectListboxData.push_back(listinfo);
			}			
			break;
	}
}

TPLListboxInfo TPLListboxData::getData(int16 listboxType, int16 index)
{//CA(__FUNCTION__);
//PMString data;
//data.Append("listboxType : ");
//data.AppendNumber(listboxType);
//data.Append("\n");
//data.Append("index : ");
//data.AppendNumber(index);
//CA(data);
	TPLListboxInfo listinfo;
	switch(listboxType)
	{
		case 1:
			listinfo = this->PFListboxData[index];
			break;
		case 2:
			listinfo = this->PGListboxData[index];
			break;
		case 3:
			listinfo = this->PRListboxData[index];
			break;
		case 4:
			listinfo = this->ItemListboxData[index];
			break;
		case 5:
			if(TPLMediatorClass::IsOneSourceMode)
			{
				listinfo = this->CatagoryListboxData[index];
			}
			else
			{
				listinfo = this->ProjectListboxData[index];
			}
			break;
	}
	return listinfo;
}

int32 TPLListboxData::returnListVectorSize(int16 listboxType)
{//CA(__FUNCTION__);
	switch(listboxType)
	{
		case 1:
			return static_cast<int32>(PFListboxData.size());
			break;
		case 2:
			return static_cast<int32>(PGListboxData.size());
			break;
		case 3:
			return static_cast<int32>(PRListboxData.size());
			break;
		case 4:
			return static_cast<int32>(ItemListboxData.size());
		case 5:
			if(TPLMediatorClass::IsOneSourceMode)
			{
				return static_cast<int32>(CatagoryListboxData.size());
			}
			else
				return static_cast<int32>(ProjectListboxData.size());
			break;
	}
	return 0;
}
	
void TPLListboxData::clearVector(int16 listboxType)
{
	//CA(__FUNCTION__);
	switch(listboxType)
	{
		case 1:
			/*PFListboxData.erase
				(PFListboxData.begin(), PFListboxData.end());*/
			PFListboxData.clear();
			break;
		case 2:
			/*PGListboxData.erase
				(PGListboxData.begin(), PGListboxData.end());*/
			PGListboxData.clear();
			break;
		case 3:
			/*PRListboxData.erase
				(PRListboxData.begin(), PRListboxData.end());*/
			PRListboxData.clear();
			break;
		case 4:
			/*ItemListboxData.erase
				(ItemListboxData.begin(), ItemListboxData.end());*/
			ItemListboxData.clear();
			break;
		case 5:
			/*ProjectListboxData.erase
				(ProjectListboxData.begin(), ProjectListboxData.end());*/
			if(TPLMediatorClass::IsOneSourceMode)
			{
				CatagoryListboxData.clear();
			}
			else
				ProjectListboxData.clear();
			break;
	}
}

void TPLListboxData::setIsBoxDragged(int16 listboxType, int16 index, bool16 flag)
{//CA(__FUNCTION__);
	TPLListboxInfo listinfo;
	TPLListboxInfoVect::iterator myIterator;

	switch(listboxType)
	{
		case 1:
			for (myIterator = PFListboxData.begin(); myIterator != PFListboxData.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					(*myIterator).isDragged = flag;
				}
			}
			break;
		case 2:
			for (myIterator = PGListboxData.begin(); myIterator != PGListboxData.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					(*myIterator).isDragged = flag;
				}
			}
			break;
		case 3:
			for (myIterator = PRListboxData.begin(); myIterator != PRListboxData.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					(*myIterator).isDragged = flag;
				}
			}
			break;
		case 4:
			for (myIterator = ItemListboxData.begin(); myIterator != ItemListboxData.end(); myIterator++)
			{
				if((*myIterator).index == index)
				{
					(*myIterator).isDragged = flag;
				}
			}
			break;
		case 5:
			if(TPLMediatorClass::IsOneSourceMode)
			{
				for (myIterator = CatagoryListboxData.begin(); myIterator != CatagoryListboxData.end(); myIterator++)
				{
					if((*myIterator).index == index)
					{
						(*myIterator).isDragged = flag;
					}
				}
			}
			else
			{
				for (myIterator = ProjectListboxData.begin(); myIterator != ProjectListboxData.end(); myIterator++)
				{
					if((*myIterator).index == index)
					{
						(*myIterator).isDragged = flag;
					}
				}
			}
			break;
	}
}

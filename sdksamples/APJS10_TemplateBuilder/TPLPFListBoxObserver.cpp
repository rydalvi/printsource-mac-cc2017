#include "VCPlugInHeaders.h"
#include "ISubject.h"
#include "IControlView.h"
#include "IListBoxController.h"
#include "CAlert.h"
#include "CObserver.h"
#include "TPLID.h"
#include "SDKListBoxHelper.h"
#include "IPanelControlData.h"
#include "TPLMediatorClass.h"
#include "TPLListboxData.h"
#include "IListControlData.h"
#include "IAppFramework.h"
#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("TPLPFListBoxObserver.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}


int32 PFRow = -1;
class TPLPFListBoxObserver : public CObserver
{
public:
	TPLPFListBoxObserver(IPMUnknown *boss);
	~TPLPFListBoxObserver();
	virtual void AutoAttach();
	virtual void AutoDetach();
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
};

CREATE_PMINTERFACE(TPLPFListBoxObserver, kTPLPFListBoxObserverImpl)

TPLPFListBoxObserver::TPLPFListBoxObserver(IPMUnknown* boss)
: CObserver(boss)
{
}

TPLPFListBoxObserver::~TPLPFListBoxObserver()
{
}

void TPLPFListBoxObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
}

void TPLPFListBoxObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_ILISTCONTROLDATA);   //CS3 Change
	}
}

void TPLPFListBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
	if(ptrIAppFramework == nil)
	{
		//CA("ptrIAppFramework == nil");
		return;
	}
	
	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) 
	{
		do 
		{
			PFRow = -1;
			InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if(panel==nil)
			{
				//ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLPFListBoxObserver::Update::panel == nil");			
				break;
			}

			SDKListBoxHelper listHelper(this, kTPLPluginID);

			IControlView* PFLstboxCntrlView = listHelper.FindCurrentListBox(panel, 1);
			if(PFLstboxCntrlView == nil) 
			{
			//	ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLPFListBoxObserver::PFLstboxCntrlView is nil");			
				break;
			}

			InterfacePtr<IListBoxController> listCntl(PFLstboxCntrlView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
			if(listCntl == nil)
			{
				ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLPFListBoxObserver::Update::listCntl == nil");			
				break;
			}

			K2Vector<int32> curSelection ;
			listCntl->GetSelected( curSelection ) ;

			const int kSelectionLength =  curSelection.Length() ;

			int curSelRowIndex=-1;
			PMString curSelRowStr("");

			/* getting current selection here */
			if (kSelectionLength> 0 )
			{
				curSelRowIndex=curSelection[0];

				TPLListboxData PFLstboxData;
				
				int32 vectSize=PFLstboxData.returnListVectorSize(1);

				/* For getting data of the selected row of listbox */
				for(int y=0;y<vectSize;y++)
				{
					if(y==curSelRowIndex)
					{
						TPLListboxInfo PFListboxInfo=PFLstboxData.getData(1,y);
						curSelRowStr=PFListboxInfo.name;
						TPLMediatorClass::imageFlag=PFListboxInfo.isImageFlag;
						break;
					}
				}
			}
			TPLMediatorClass::curSelLstbox=1;
			TPLMediatorClass::curSelRowIndex=curSelRowIndex;
			PFRow = curSelRowIndex;
			TPLMediatorClass::curSelRowString=curSelRowStr;
			TPLMediatorClass::PFLstboxCntrlView=PFLstboxCntrlView;
			TPLMediatorClass::tableFlag=0;
		} while(0);
	}
}


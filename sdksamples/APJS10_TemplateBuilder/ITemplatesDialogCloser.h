
#ifndef __ITemplatesDialogCloser__
#define __ITemplatesDialogCloser__

#include "IPMUnknown.h"
#include "TPLID.h"

class ITemplatesDialogCloser : public IPMUnknown
{
	public:	
		enum { kDefaultIID = IID_ITEMPLATEDIALOGCLOSER };
		virtual void closeTemplDialog(void)=0;
		virtual void refreshItemTreeForClassID(double ClassId)=0;
		/*virtual PMString getEventName(int32 elementID,int32 typeID ,int32 level=-1)=0;
		virtual PMString getCatagoryName(int32 elementID ,int32 typeID ,int32 level=-1 )=0;*/
};

#endif

#include "VCPlugInHeaders.h"
#include "IHierarchy.h"
#include "ISpreadlist.h"
#include "IDocument.h"
#include "ISession.h"
#ifdef	 DEBUG
#include "IPlugInList.h"
#include "IObjectModel.h"
#endif
#include "ILayoutUtils.h" //Cs4
#include "LNGID.h"
#include "AFSTreeModel.h"
#include "CAlert.h"
//#include "LNGMediatorClass.h"
//#include "GlobalData.h"

#include "PublicationNode.h"
//#include "IClientOptions.h"
#include "IAppFrameWork.h"
#include "AFSTreeDataCache.h"
//#include "ISpecialChar.h"

#include "CAlert.h"
//#include "IMessageServer.h"
#define FILENAME			PMString("AFSTreeModel.cpp")
#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}

#define CA(X) CAlert::InformationAlert(X);

AFSTreeDataCache dc;

int32 AFSTreeModel::root=0;
PMString AFSTreeModel::publicationName;
int32 AFSTreeModel::classId=0;

int32 RowCountForTree = -1;

//extern int32 SelectedRowNo;
static int32 NodeCount =0;

extern PublicationNodeList pNodeDataList;

//extern VectorClientModelPtr vectorClientModelPtr;


AFSTreeModel::AFSTreeModel()
{
    //root=-1;
    //classId=11002;
}

AFSTreeModel::~AFSTreeModel()
{
}


int32 AFSTreeModel::GetRootUID() const
{
//    if(dc.isExist(root, pNode))
//    {
//        return root;
//    }
    
    PublicationNode pNode;
    dc.clearMap();

    PublicationNodeList CurrentNodeList;
    CurrentNodeList.clear();

    NodeCount =0;
    CurrentNodeList = pNodeDataList;
    
    RowCountForTree = 0;
    PMString name("Root");
    pNode.setPubId(root);
    pNode.setChildCount(static_cast<int32>(CurrentNodeList.size()));
    /*pNode.setLevel(0);*/
    pNode.setParentId(-2);
    pNode.setPublicationName(name);
    pNode.setSequence(0);
    pNode.setTreeNodeId(root);
    pNode.setTreeParentNodeId(-2);
    
    dc.add(pNode);
    NodeCount++;

    int count=0;
    for(int i =0 ; i<CurrentNodeList.size(); i++)
    { //CAlert::InformationAlert(" 123");
        /*pNode.setLevel(1);*/
        pNode = CurrentNodeList[i];
        
        pNode.setTreeNodeId(NodeCount);
        pNode.setTreeParentNodeId(root);
        //CA_NUM("NodeCount:", NodeCount);
        pNode.setParentId(root);
        pNode.setSequence(i);
        count++;
//        pNode.setPubId(CurrentNodeList[i].getPubId());
        pNode.setPubId(i+1);
        PMString ASD(CurrentNodeList[i].getName());
        ASD.AppendNumber(PMReal(pNode.getPubId()));
        pNode.setPublicationName(ASD);
        pNode.setChildCount(0);

        NodeCount++;
        
        dc.add(pNode);
    }
    
    return root;
}


int32 AFSTreeModel::GetRootCount() const
{
    //CA("LNGTreeModel::GetRootCount");
    int32 retval=0;
    PublicationNode pNode;
    if(dc.isExist(root, pNode))
    {
        //CAI(pNode.getChildCount());
        return pNode.getChildCount();
    }
    return 0;
}

int32 AFSTreeModel::GetChildCount(const UID& uid) const
{
    //CA("LNGTreeModel::GetChildCount");
    PublicationNode pNode;
    
    if(dc.isExist(uid.Get(), pNode))
    {
        //CAI(pNode.getChildCount());
        return pNode.getChildCount();
    }
    return -1;
}

int32 AFSTreeModel::GetParentUID(const UID& uid) const
{
    
    PublicationNode pNode;
    
    if(dc.isExist(uid.Get(), pNode))
    {
        if(pNode.getParentId()==-2)
            return 0; //kInvalidUID;
        return pNode.getParentId();
    }
    return 0; //kInvalidUID;
}


int32 AFSTreeModel::GetChildIndexFor(const UID& parentUID, const UID& childUID) const
{
    PublicationNode pNode;
    int32 retval=-1;
    
    if(dc.isExist(childUID.Get(), pNode))
        return pNode.getSequence();
    return -1;
}


int32 AFSTreeModel::GetNthChildUID(const UID& parentUID, const int32& index) const
{
    PublicationNode pNode;
    int32 retval=-1;
    
    if(dc.isExist(parentUID.Get(), index, pNode))
    {
        
        return pNode.getTreeNodeId();
    }
    return 0; //kInvalidUID;
}


int32 AFSTreeModel::GetNthRootChild(const int32& index) const
{
    //CA("LNGTreeModel::GetNthRootChild");
    PublicationNode pNode;
    if(dc.isExist(root, index, pNode))
    {
        //CAI(pNode.getClientId());
        return pNode.getTreeNodeId();
    }
    return 0; //kInvalidUID;
}

int32 AFSTreeModel::GetIndexForRootChild(const UID& uid) const
{
    PublicationNode pNode;
    if(dc.isExist(uid.Get(), pNode))
        return pNode.getSequence();
    return -1;
}

PMString AFSTreeModel::ToString(const UID& uid, int32 *Rowno) const
{
    PublicationNode pNode;
    PMString name("");
    
    if(dc.isExist(uid.Get(), pNode))
    {	
        *Rowno= pNode.getSequence();
        //*Rowno = pNode.getHitCount();
        /*PMString ASD("getHitCount in ToolString : ");
         ASD.AppendNumber(pNode.getHitCount());
         CA(ASD);*/
        return pNode.getName();
    }
    return name;
}


int32 AFSTreeModel::GetNodePathLengthFromRoot(const UID& uid)
{
    PublicationNode pNode;
    if(uid.Get()==root)
        return 0;
    if(dc.isExist(uid.Get(), pNode))
        return (1);
    return -1;
}

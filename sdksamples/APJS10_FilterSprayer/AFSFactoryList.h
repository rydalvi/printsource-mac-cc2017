//========================================================================================
//  
//  $File: $
//  
//  Owner: Yuan
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================
REGISTER_PMINTERFACE(AFSActionComponent, kAFSActionComponentImpl)
REGISTER_PMINTERFACE(AFSDialogController, kAFSDialogControllerImpl)
REGISTER_PMINTERFACE(AFSDialogObserver, kAFSDialogObserverImpl)

REGISTER_PMINTERFACE(AFSPanelObserver, kAFSPanelObserverImpl)

REGISTER_PMINTERFACE(AFSTreeViewHierarchyAdapter, kAFSTreeViewHierarchyAdapterImpl)
REGISTER_PMINTERFACE(AFSTreeViewWidgetMgr,		kAFSTreeViewWidgetMgrImpl)

REGISTER_PMINTERFACE(PnlTrvNodeEH, kAFSTrvNodeEHImpl)

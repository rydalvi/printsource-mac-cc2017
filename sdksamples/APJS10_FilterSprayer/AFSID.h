//========================================================================================
//  
//  $File: $
//  
//  Owner: Yuan
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __AFSID_h__
#define __AFSID_h__

#include "SDKDef.h"

// Company:
#define kAFSCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kAFSCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kAFSPluginName	"Filter Sprayer"			// Name of this plug-in.
#define kAFSPrefixNumber	0xe9400 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kAFSVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kAFSAuthor		"Yuan"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kAFSPrefixNumber above to modify the prefix.)
#define kAFSPrefix		RezLong(kAFSPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kAFSStringPrefix	SDK_DEF_STRINGIZE(kAFSPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kAFSMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kAFSMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kAFSPluginID, kAFSPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kAFSActionComponentBoss,    kAFSPrefix + 0)
DECLARE_PMID(kClassIDSpace, kAFSPanelWidgetBoss,        kAFSPrefix + 1)
DECLARE_PMID(kClassIDSpace, kAFSDialogBoss,             kAFSPrefix + 2)
DECLARE_PMID(kClassIDSpace, kAFSDropDownListWidgetBoss,	kAFSPrefix + 3)
DECLARE_PMID(kClassIDSpace, kAFSTreeViewWidgetBoss,		kAFSPrefix + 4)
DECLARE_PMID(kClassIDSpace, kAFSTreeNodeWidgetBoss,		kAFSPrefix + 5)
DECLARE_PMID(kClassIDSpace, kAFSTreeTextBoxWidgetBoss,	kAFSPrefix + 6)

//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 3)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 4)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kAFSBoss, kAFSPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IAFSTRVSHADOWEVENTHANDLER ,	        kAFSPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IAFSINTERFACE, kAFSPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kAFSActionComponentImpl, kAFSPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kAFSDialogControllerImpl, kAFSPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kAFSDialogObserverImpl, kAFSPrefix + 2 )

DECLARE_PMID(kImplementationIDSpace, kAFSPanelObserverImpl, kAFSPrefix + 3 )
DECLARE_PMID(kImplementationIDSpace, kAFSTreeViewHierarchyAdapterImpl  ,		kAFSPrefix + 4)

DECLARE_PMID(kImplementationIDSpace, kAFSTreeViewWidgetMgrImpl  ,	kAFSPrefix+ 5)

DECLARE_PMID(kImplementationIDSpace, kAFSTrvNodeEHImpl,		    kAFSPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 3)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 4)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kAFSImpl, kAFSPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kAFSAboutActionID, kAFSPrefix + 0)
DECLARE_PMID(kActionIDSpace, kAFSPanelWidgetActionID, kAFSPrefix + 1)
DECLARE_PMID(kActionIDSpace, kAFSSeparator1ActionID, kAFSPrefix + 2)
DECLARE_PMID(kActionIDSpace, kAFSPopupAboutThisActionID, kAFSPrefix + 3)
DECLARE_PMID(kActionIDSpace, kAFSDialogActionID, kAFSPrefix + 4)
DECLARE_PMID(kActionIDSpace, kAFSPanelMenuActionID, kAFSPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kAFSActionID, kAFSPrefix + 25)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kAFSPanelWidgetID,             kAFSPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kAFSDialogWidgetID,            kAFSPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kAFSFilterTextWidgetID,        kAFSPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kAFSPublicationGPWidgetID,     kAFSPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kAFSFilterDropDownWidgetID,    kAFSPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kAFSTreeViewWidgetID ,         kAFSPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kAFSTreeNodeNameWidgetID ,     kAFSPrefix + 6)

DECLARE_PMID(kWidgetIDSpace, kAPSListParentWidgetId,        kAFSPrefix + 7)

DECLARE_PMID(kWidgetIDSpace, kAFSPaginationWidgetID,        kAFSPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kAFSFilterResultTextWidgetID,  kAFSPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kAFSPrePageButtonWidgetID,     kAFSPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kAFSNextPageButtonWidgetID,    kAFSPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kAFSSprayButtonWidgetID,		kAFSPrefix + 12)


//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 2)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 3)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 4)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kAFSWidgetID, kAFSPrefix + 25)


// "About Plug-ins" sub-menu:
#define kAFSAboutMenuKey			kAFSStringPrefix "kAFSAboutMenuKey"
#define kAFSAboutMenuPath		kSDKDefStandardAboutMenuPath kAFSCompanyKey

// "Plug-ins" sub-menu:
#define kAFSPluginsMenuKey 		kAFSStringPrefix "kAFSPluginsMenuKey"
#define kAFSPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kAFSCompanyKey kSDKDefDelimitMenuPath kAFSPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kAFSAboutBoxStringKey	kAFSStringPrefix "kAFSAboutBoxStringKey"
#define kAFSPanelTitleKey					kAFSStringPrefix	"kAFSPanelTitleKey"
#define kAFSStaticTextKey kAFSStringPrefix	"kAFSStaticTextKey"
#define kAFSInternalPopupMenuNameKey kAFSStringPrefix	"kAFSInternalPopupMenuNameKey"
#define	kAFSFilterStringKey	kAFSStringPrefix		"kAFSFilterStringKey"
#define kAFSTargetMenuPath kAFSInternalPopupMenuNameKey
#define kBlankStringTextKey	kAFSStringPrefix	"kBlankStringTextKey"

// Menu item positions:

#define	kAFSSeparator1MenuItemPosition		10.0
#define kAFSAboutThisMenuItemPosition		11.0

#define kAFSDialogTitleKey kAFSStringPrefix "kAFSDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kAFSPanelMenuItemKey kAFSStringPrefix "kAFSPanelMenuItemKey"
// "Plug-ins" sub-menu item position for dialog:
#define kAFSDialogMenuItemPosition	12.0


#define kAFSTreePanelNodeRsrcID 3200
// PNG specific ID
#define kAFSPNGSprayRsrcID		30001

// Initial data format version numbers
#define kAFSFirstMajorFormatNumber  RezLong(1)
#define kAFSFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kAFSCurrentMajorFormatNumber kAFSFirstMajorFormatNumber
#define kAFSCurrentMinorFormatNumber kAFSFirstMinorFormatNumber

#endif // __AFSID_h__

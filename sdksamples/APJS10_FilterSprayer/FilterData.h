#ifndef __FilterData_h__
#define __FilterData_h__

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "vector"

using namespace std;

class PubFilter{
private:
    PMString filterId;
    PMString filterName;
    int32 filterResultCount;
    int32 totalPage;
    int32 currentPage;
    int32 limit;
    int32 offset;
public:
    PubFilter(){
        this->filterId.Clear();
        this->filterName.Clear();
        this->filterResultCount = 0;
        this->totalPage = 1;
        this->currentPage = 1;
        this->limit = 10;
        this->offset = 0;
    }
    PMString getFilterId(void){
        return this->filterId;
    }
    void setFilterId(PMString id){
        this->filterId=id;
    }
    PMString getFilterName(void){
        return this->filterName;
    }
    void setFilterName(PMString name){
        this->filterName=name;
    }
    int32 getFilterResultCount(void){
        return this->filterResultCount;
    }
    void setFilterResultCount(int32 count){
        this->filterResultCount=count;
    }
    int32 getTotalPage(void){
        return this->totalPage;
    }
    void setTotalPage(int32 totalPage){
        this->totalPage=totalPage;
    }
    int32 getCurrentPage(void){
        return this->currentPage;
    }
    void setCurrentPage(int32 page){
        this->currentPage=page;
    }
    int32 getLimit(void){
        return this->limit;
    }
    void setLimit(int32 limit){
        this->limit=limit;
    }
    int32 getOffset(void){
        return this->offset;
    }
    void setOffset(int32 offset){
        this->offset=offset;
    }
    
};

class FilterData{
private:
    static vector<PubFilter> filterVector;
public:
    void setFilterVector(PMString,PMString);
    PubFilter getFilterVector(int32);
    void clearFilterVector();
    void updateFilterVector(PubFilter, int32);
};
#endif
//========================================================================================
//  
//  $File: $
//  
//  Owner: Yuan
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IPanelMgr.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IPanelControlData.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
#include "IActionStateList.h"

// Project includes:
#include "AFSID.h"
#include "IAppFramework.h"
#include "FilterValue.h"
#include "FilterData.h"

#define CA(X)		CAlert::InformationAlert(X)

IAppFramework* ptrIAppFramework = NULL;

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup apjs10_filtersprayer

*/
class AFSActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		AFSActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
    
        virtual void UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint, IPMUnknown* widget);

	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();
    
        /** Opens Dynamic Panel */
        void DoPanel();
		
		/** Opens this plug-in's dialog. */
		void DoDialog();
		


};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(AFSActionComponent, kAFSActionComponentImpl)

/* AFSActionComponent Constructor
*/
AFSActionComponent::AFSActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void AFSActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		case kAFSPopupAboutThisActionID:
		case kAFSAboutActionID:
		{
			this->DoAbout();
			break;
		}
					

		case kAFSDialogActionID:
		{
			this->DoDialog();
			break;
		}
            
        case kAFSPanelMenuActionID:
        {
            this->DoPanel();
            break;
        }


		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void AFSActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kAFSAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}



/* DoDialog
*/
void AFSActionComponent::DoDialog()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kAFSPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) {
			break;
		}

		// Open the dialog.
		dialog->Open(); 
	
	} while (false);			
}

/* DoDialog
 */
void AFSActionComponent::DoPanel()
{
    do
    {
        // Get the application interface and the DialogMgr.
        InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
        ASSERT(application);
        if (application == nil) {
            return;
        }
        
        InterfacePtr<IPanelMgr> iPanelMgr(application->QueryPanelManager());
        if(iPanelMgr == nil){
            return;
        }
        // Show Panel by widget ID
        iPanelMgr->ShowPanelByWidgetID(kAFSPanelWidgetID);
        
        // Temperay connet to api call, get data and then show in the dropdown
        // Will be moved to Observer file latter
        // Start
        if(ptrIAppFramework == NULL)
        {
            InterfacePtr<IAppFramework> ptrIAppFrameworkOne(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
            if(ptrIAppFrameworkOne == NULL)
                return;
            ptrIAppFramework = ptrIAppFrameworkOne;
        }
        
        if(ptrIAppFramework == NULL)
            return;
        
        // Get dropdown widget
        IControlView* myPanel = iPanelMgr->GetVisiblePanel(kAFSPanelWidgetID);
        
        InterfacePtr<IPanelControlData> panelControlData(myPanel, UseDefaultIID());
        if(!panelControlData)
        {
            //CA("No SPPanelWidgetControlData");
            return;
        }

        IControlView* FilterDropListCntrlView = panelControlData->FindWidget(kAFSFilterDropDownWidgetID);
        if(FilterDropListCntrlView == nil)
        {
            //CA("SectionDropListCntrlView == nil");
            return;
        }
        
        InterfacePtr<IDropDownListController> FilterDropListCntrler(FilterDropListCntrlView, UseDefaultIID());
        if(FilterDropListCntrler==nil)
        {
            //CA("SectionDropListCntrler is nil");
            return;
        }
        
        InterfacePtr<IStringListControlData> FilterDropListCntrlData(FilterDropListCntrler, UseDefaultIID());
        if(FilterDropListCntrlData==nil)
        {
            //CA("SectionDropListCntrlData is nil");
            return;
        }


        // Check login status
        bool16 loginStatus = ptrIAppFramework->getLoginStatus();
        
        if (loginStatus == kTrue)
        {
            vector<FilterValue> result;
            ptrIAppFramework->callFilterService(result);
            
            // Init filters
            FilterData filters;
            filters.clearFilterVector();
            FilterDropListCntrlData->Clear();
//            SectionDropListCntrlView->Enable();
            
            PMString as;
            as.Append("--Select--");
            FilterDropListCntrlData->AddString(as);
            if (result.size() > 0)
            {
                vector<FilterValue>::iterator iter;
                for(iter = result.begin(); iter != result.end(); ++iter)
                {
                    PMString id = iter->getFilterId();
                    PMString filterName = iter->getFilterName();
                    FilterDropListCntrlData->AddString(filterName);
                    filters.setFilterVector(id, filterName);
                }
                FilterDropListCntrler->Select(1);
            }
            else
            {
                FilterDropListCntrler->Select(0);
            }

        }
        // End
        
    } while (false);			
}


/* UpdateActionStates
 */
void AFSActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList* iListPtr, GSysPoint mousePoint, IPMUnknown* widget)
{
    do
    {
        if(ptrIAppFramework == NULL)
        {
            InterfacePtr<IAppFramework> ptrIAppFrameworkOne(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
            if(ptrIAppFrameworkOne == NULL)
                return;
            ptrIAppFramework = ptrIAppFrameworkOne;
        }
        
        if(ptrIAppFramework == NULL)
            return;
        
        bool16 result=ptrIAppFramework->getLoginStatus();
        
        for(int32 iter = 0; iter < iListPtr->Length(); iter++)
        {
            ActionID actionID = iListPtr->GetNthAction(iter);
            switch(actionID.Get())
            {
                case kAFSPanelMenuActionID:
                {
                    if (result == kTrue)
                    {
                        iListPtr->SetNthActionState(iter,kEnabledAction);
                    }
                    else
                    {
                        iListPtr->SetNthActionState(iter,kDisabled_Unselected);
                    }
                    break;
                }
                default:
                {
                    break;
                }
            }
        }

        
    } while(kFalse);
    
}




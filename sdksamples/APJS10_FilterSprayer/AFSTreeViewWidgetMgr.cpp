#include "VCPlugInHeaders.h"
#include "IControlView.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "IntNodeID.h"
#include "UIDNodeID.h"
#include "CTreeViewWidgetMgr.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SysControlIds.h"
#include "AFSID.h"
#include "AFSTreeModel.h"
#include "PublicationNode.h"
//#include "MediatorClass.h"
#include "CAlert.h"
#include "AFSTreeDataCache.h"
//#include "ISpecialChar.h"
//#include "ISpecialChar.h"
#include "CAlert.h"
#include "IAppFramework.h"
#include "IBooleanControlData.h"
#include "IUIFontSpec.h"
//#include "IMessageServer.h"
//#define FILENAME			PMString("SPTreeViewWidgetMgr.cpp")
//#define FUNCTIONNAME		PMString(__FUNCTION__)
//#define CA(X) CAMessage(FILENAME,FUNCTIONNAME,X,__LINE__);
//#define CA_NUM(a,b) {PMString str;str.Append(a);str.AppendNumber(b);CA(str);}
//#define CAI(x)	{PMString str;str.AppendNumber(x);CA(str);}
#define CA(X) CAlert::InformationAlert(X);



class AFSTreeViewWidgetMgr: public CTreeViewWidgetMgr
{
public:
    AFSTreeViewWidgetMgr(IPMUnknown* boss);
    virtual ~AFSTreeViewWidgetMgr() {}
    virtual	IControlView*	CreateWidgetForNode(const NodeID& node) const;
    virtual	WidgetID		GetWidgetTypeForNode(const NodeID& node) const;
    virtual	bool16 ApplyNodeIDToWidget
    ( const NodeID& node, IControlView* widget, int32 message = 0 ) const;
    virtual PMReal GetIndentForNode(const NodeID& node) const;
private:
    PMString getNodeText(const int32& uid, int32 *RowNo) const;
    void indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const;
    enum {ePFTreeIndentForNode=3};
};

CREATE_PMINTERFACE(AFSTreeViewWidgetMgr, kAFSTreeViewWidgetMgrImpl)

AFSTreeViewWidgetMgr::AFSTreeViewWidgetMgr(IPMUnknown* boss) :
CTreeViewWidgetMgr(boss)
{	//CA("Inside WidgetManager constructor");
}

IControlView* AFSTreeViewWidgetMgr::CreateWidgetForNode(const NodeID& node) const
{	//CA("CreateWidgetForNode");
    IControlView* retval =
    (IControlView*) ::CreateObject(::GetDataBase(this),
                                   RsrcSpec(LocaleSetting::GetLocale(),
                                            kAFSPluginID,
                                            kViewRsrcType,
                                            kAFSTreePanelNodeRsrcID),IID_ICONTROLVIEW);
    ASSERT(retval);
    return retval;
}

WidgetID AFSTreeViewWidgetMgr::GetWidgetTypeForNode(const NodeID& node) const
{	//CA("GetWidgetTypeForNode");
    return kAPSListParentWidgetId;
}

bool16 AFSTreeViewWidgetMgr::ApplyNodeIDToWidget
(const NodeID& node, IControlView* widget, int32 message) const
{
    CTreeViewWidgetMgr::ApplyNodeIDToWidget( node, widget );
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == nil)
    {
        //CA("ptrIAppFramework == nil");
        return kFalse;
    }
    do
    {
        InterfacePtr<IPanelControlData> panelControlData(widget, UseDefaultIID());
        //ASSERT(panelControlData);
        if(panelControlData==nil)
        {
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::panelControlData is nil");
            break;
        }
        
        InterfacePtr<const ITreeViewHierarchyAdapter>   adapter(this, UseDefaultIID());
        //ASSERT(adapter);
        if(adapter==nil)
        {
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::adapter is nil");
            break;
        }
        
        AFSTreeModel model;
        TreeNodePtr<IntNodeID>  uidNodeIDTemp(node);
        int32 uid= uidNodeIDTemp->Get();
        
        PublicationNode pNode;
        AFSTreeDataCache dc;
        
        dc.isExist(uid, pNode);
        
        TreeNodePtr<IntNodeID>  uidNodeID(node);
        //ASSERT(uidNodeID);
        if(uidNodeID == nil)
        {
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::uidNodeID is nil");
            break;
        }
        
        int32 *RowNo= NULL;
        int32 LocalRowNO;
        RowNo = &LocalRowNO;
        
//        CA("pNode.getName()  = " + pNode.getName());
        PMString stringToDisplay(this->getNodeText(uidNodeID->Get(), RowNo));
        
//        PMString stringToDisplay(pNode.getName());
        stringToDisplay.SetTranslatable( kFalse );
        
        IControlView* displayStringView = panelControlData->FindWidget( kAFSTreeNodeNameWidgetID );
        //ASSERT(displayStringView);
        if(displayStringView == nil)
        {
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::displayStringView is nil");
            break;
        }
        
        displayStringView->ShowView();
        
        InterfacePtr<ITextControlData>  textControlData( displayStringView, UseDefaultIID() );
        //ASSERT(textControlData);
        if(textControlData== nil)
        {
            ptrIAppFramework->LogDebug("AP7_TemplateBuilder::LNGTreeViewWidgetMgr::ApplyNodeIDToWidget::textControlData is nil");
            break;
        }

        stringToDisplay.SetTranslatable(kFalse);
        stringToDisplay.ParseForEmbeddedCharacters();
        textControlData->SetString(stringToDisplay);
        
        this->indent( node, widget, displayStringView );

        
    } while(kFalse);
    return kTrue;
}

PMReal AFSTreeViewWidgetMgr::GetIndentForNode(const NodeID& node) const
{	//CA("Inside GetIndentForNode");
    do
    {
        TreeNodePtr<IntNodeID>  uidNodeID(node);
        ASSERT(uidNodeID);
        if(uidNodeID == nil) 
            break;
        
        AFSTreeModel model;
        int nodePathLengthFromRoot = model.GetNodePathLengthFromRoot(uidNodeID->Get());
        
        if( nodePathLengthFromRoot <= 0 ) 
            return 0.0;
        
        return  PMReal((nodePathLengthFromRoot * ePFTreeIndentForNode)+0.5);
    } while(kFalse);
    return 0.0;
}

PMString AFSTreeViewWidgetMgr::getNodeText(const int32& uid, int32 *RowNo) const
{	//CA("getNodeText");
    AFSTreeModel model;
    return model.ToString(uid, RowNo);
}

void AFSTreeViewWidgetMgr::indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget ) const
{	//CA("Inside indent");
    const PMReal indent = this->GetIndent(node);	
    PMRect widgetFrame = widget->GetFrame();
    widgetFrame.Left() = indent;
    widget->SetFrame( widgetFrame );
    staticTextWidget->WindowChanged();
    PMRect staticTextFrame = staticTextWidget->GetFrame();
    staticTextFrame.Right( widgetFrame.Right()+1500 );
    
    widgetFrame.Right(widgetFrame.Right()+1500);
    widget->SetFrame(widgetFrame);
    
    staticTextWidget->SetFrame( staticTextFrame );
}


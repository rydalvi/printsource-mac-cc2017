//========================================================================================
//
//  $File: $
//
//  Owner: Yuan
//
//  $Author: $
//
//  $DateTime: $
//
//  $Revision: $
//
//  $Change: $
//
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or
//  distribution of it requires the prior written permission of Adobe.
//
//========================================================================================
#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "IWidgetParent.h"
#include "CAlert.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ITreeViewController.h"
#include "ITextControlData.h"
#include "K2Vector.h"
#include "K2Vector.tpp"
#include "UIDNodeID.h"
// General includes:
#include "SelectionObserver.h"
// Project includes:
#include "AFSID.h"
#include "IAppFramework.h"
#include "FilterData.h"
#include "ItemModel.h"
#include "AFSTreeModel.h"
#include "ITreeViewMgr.h"
#include "PublicationNode.h"
#include "map"
#include "AFSTreeDataCache.h"
#include "IDataSprayer.h"
//#include "GlobalData.h"

#define CA(X)		CAlert::InformationAlert(X)
int32 CurrentSelectedProductRow = -100;
int32 firstpNodeListCount = 0;
bool16 singleSelectionSpray = kFalse;
PublicationNodeList orignalpNodeDataList;
extern IPanelControlData*	mainPanelCntrlData;
extern PublicationNodeList pNodeDataList;
extern double itemDescriptionAttribute;
extern AFSTreeDataCache dc;
extern bool16 isCancelButtonClick;

// Used as extern in subsectionsprayer
double CurrentSelectedSection = -1;
double CurrentSelectedPublicationID= -1;
double CurrentSelectedSubSection = -1;
double global_lang_idn = -1;
int32 ListFlag= 0;
int32 global_project_level = 0;
char AlphabetArray[] = "abcdefghijklmnopqrstuvwxyz";
bool16 searchResult = kFalse;

/** Implements IObserver based on the partial implementation CDialogObserver.
 
	
	@ingroup apjs10_filtersprayer
 */
class AFSPanelObserver : public ActiveSelectionObserver
{
public:
    /**
     Constructor.
     @param boss interface ptr from boss object on which this interface is aggregated.
     */
    AFSPanelObserver(IPMUnknown* boss) : ActiveSelectionObserver(boss) {}
    
    /** Destructor. */
    virtual ~AFSPanelObserver() {}
    
    /**
     Called by the application to allow the observer to attach to the subjects
     to be observed, in this case the dialog's info button widget. If you want
     to observe other widgets on the dialog you could add them here.
     */
    virtual void AutoAttach();
    
    /** Called by the application to allow the observer to detach from the subjects being observed. */
    virtual void AutoDetach();
    
    /**
     Called by the host when the observed object changes, in this case when
     the dialog's info button is clicked.
     @param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
     @param theSubject points to the ISubject interface for the subject that has changed.
     @param protocol specifies the ID of the changed interface on the subject boss.
     @param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
     */
    virtual void Update
    (
     const ClassID& theChange,
     ISubject* theSubject,
     const PMIID& protocol,
     void* changedBy
     );
    
    void getFilterResults(PubFilter pubfilter, int32 limit, int32 offset);
    
protected:
    IPanelControlData* QueryPanelControlData();
    void AttachWidget(IPanelControlData* , const WidgetID& , const PMIID&);
    void DetachWidget(IPanelControlData* , const WidgetID& , const PMIID&);
    
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
 */
CREATE_PMINTERFACE(AFSPanelObserver, kAFSPanelObserverImpl)

/* AutoAttach
 */
void AFSPanelObserver::AutoAttach()
{
    // Call the base class AutoAttach() function so that default behavior
    // will still occur (OK and Cancel buttons, etc.).
//    AFSPanelObserver::AutoAttach();
    do
    {
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == nil)
            break;
        
        InterfacePtr<IPanelControlData> panelControlData(QueryPanelControlData());
        ASSERT(panelControlData);
        if(panelControlData == nil) {
            break;
        }
        // Attach to other widgets you want to handle dynamically here.
        else
        {
            mainPanelCntrlData = panelControlData;
            AttachWidget(panelControlData, kAFSFilterDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
            AttachWidget(panelControlData, kAFSPrePageButtonWidgetID, IID_IBOOLEANCONTROLDATA);
            AttachWidget(panelControlData, kAFSNextPageButtonWidgetID, IID_IBOOLEANCONTROLDATA);
            AttachWidget(panelControlData, kAFSSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
        }
        
    } while (kFalse);
}

/* AutoDetach
 */
void AFSPanelObserver::AutoDetach()
{
    // Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
//    AFSPanelObserver::AutoDetach();
    do
    {
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == nil)
            break;
        
        InterfacePtr<IPanelControlData> panelControlData(QueryPanelControlData());
        ASSERT(panelControlData);
        if(panelControlData == nil) {
            break;
        }
        // Detach from other widgets you handle dynamically here.
        else
        {
            if(!mainPanelCntrlData) {
                break;
            }
            DetachWidget(panelControlData, kAFSFilterDropDownWidgetID, IID_ISTRINGLISTCONTROLDATA);
            DetachWidget(panelControlData, kAFSPrePageButtonWidgetID, IID_IBOOLEANCONTROLDATA);
            DetachWidget(panelControlData, kAFSNextPageButtonWidgetID, IID_IBOOLEANCONTROLDATA);
            DetachWidget(panelControlData, kAFSSprayButtonWidgetID, IID_ITRISTATECONTROLDATA);
        }
        
    } while (kFalse);
}

/* Update
 */
void AFSPanelObserver::Update
(
	const ClassID& theChange,
	ISubject* theSubject,
	const PMIID& protocol,
	void* changedBy
 )
{
    // Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
//    AFSPanelObserver::Update(theChange, theSubject, protocol, changedBy);
    PubFilter pubfilter;
    do
    {
        InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
        ASSERT(controlView);
        if(!controlView) {
            return;
        }
        // Get the button ID from the view.
        WidgetID theSelectedWidget = controlView->GetWidgetID();
        
        InterfacePtr<IPanelControlData> panelControlData(QueryPanelControlData());
        
        if(!panelControlData)
        {
            CA("No SPPanelWidgetControlData");
            return;
        }
        
        InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
        if(ptrIAppFramework == NULL)
        {
            return;
        }
        
        // TODO: process this
        if(theSelectedWidget == kAFSFilterDropDownWidgetID && theChange == kPopupChangeStateMessage)
        {
            InterfacePtr<IStringListControlData> filterDropListData(controlView,UseDefaultIID());
            if (filterDropListData==nil)
            {
                return;
            }
            
            InterfacePtr<IDropDownListController> filterDropListController(filterDropListData, UseDefaultIID());
            if (filterDropListController==nil)
            {
                return;
            }
            
            int32 selectedRow = filterDropListController->GetSelected();
            
            if(selectedRow <= 0)
            {
                return;
            }
            
            FilterData filters;
            pubfilter = filters.getFilterVector(selectedRow-1);
            // initial currentpage
            pubfilter.setCurrentPage(1);
            filters.updateFilterVector(pubfilter, selectedRow-1);
            int32 limit = pubfilter.getLimit();
            int32 offset = (pubfilter.getCurrentPage() - 1)*limit;
            getFilterResults(pubfilter, limit, offset);
            
        }
        // Click Pre Page Button
        else if(theSelectedWidget == kAFSPrePageButtonWidgetID && theChange == kTrueStateMessage)
        {
            IControlView* filterDropdownWidgetView = panelControlData->FindWidget(kAFSFilterDropDownWidgetID);
            
            InterfacePtr<IStringListControlData> filterDropListData(filterDropdownWidgetView,UseDefaultIID());
            if (filterDropListData==nil)
            {
                return;
            }
            
            InterfacePtr<IDropDownListController> filterDropListController(filterDropListData, UseDefaultIID());
            if (filterDropListController==nil)
            {
                return;
            }
            
            int32 selectedRow = filterDropListController->GetSelected();
            
            if(selectedRow <= 0)
            {
                return;
            }
            
            FilterData filters;
            pubfilter = filters.getFilterVector(selectedRow-1);
            pubfilter.setCurrentPage(pubfilter.getCurrentPage() - 1);
            filters.updateFilterVector(pubfilter, selectedRow-1);
            int32 currentPage = pubfilter.getCurrentPage() < 1 ? 1 : pubfilter.getCurrentPage();
            int32 limit = pubfilter.getLimit();
            int32 offset = (currentPage - 1)*limit;
            getFilterResults(pubfilter, limit, offset);
        }
        // Click Next Page Button
        else if(theSelectedWidget == kAFSNextPageButtonWidgetID && theChange == kTrueStateMessage)
        {
            IControlView* filterDropdownWidgetView = panelControlData->FindWidget(kAFSFilterDropDownWidgetID);
            
            InterfacePtr<IStringListControlData> filterDropListData(filterDropdownWidgetView,UseDefaultIID());
            if (filterDropListData==nil)
            {
                return;
            }
            
            InterfacePtr<IDropDownListController> filterDropListController(filterDropListData, UseDefaultIID());
            if (filterDropListController==nil)
            {
                return;
            }
            
            int32 selectedRow = filterDropListController->GetSelected();
            
            if(selectedRow <= 0)
            {
                return;
            }

            FilterData filters;
            pubfilter = filters.getFilterVector(selectedRow-1);
            pubfilter.setCurrentPage(pubfilter.getCurrentPage() + 1);
            filters.updateFilterVector(pubfilter, selectedRow-1);
            int32 currentPage = pubfilter.getCurrentPage();
            int32 limit = pubfilter.getLimit();
            int32 offset = (currentPage - 1)*limit;
            getFilterResults(pubfilter, limit, offset);
            
        }
        // Click Spray Button
        else if(theSelectedWidget == kAFSSprayButtonWidgetID && theChange==kTrueStateMessage)
        {
            bool16 isUserLoggedIn=ptrIAppFramework->getLoginStatus();
            if(!isUserLoggedIn)
                return;
            
            InterfacePtr<IDataSprayer> DataSprayerPtr((IDataSprayer*)::CreateObject(kDataSprayerBoss, IID_IDataSprayer));
            if(!DataSprayerPtr)
            {
                CA("Create DataSprayer Failed");
                return;
            }
            //CA("theSelectedWidget==kSPSprayButtonWidgetID && theChange==kTrueStateMessage");
            if(CurrentSelectedProductRow == -100)//-100 is taken purposely.-100 does not exists.So something has to be selected from the list box.
            {
                CA("Click out of item list");
                break;
            }
            
//            InterfacePtr<ISubSectionSprayer> iSSSprayer((static_cast<ISubSectionSprayer*> (CreateObject(kSubSectionSprayerBoss,IID_ISUBSECTIONSPRAYER))));
//            if(iSSSprayer==nil)
//            {
//                CA("Create iSSSprayer Failed");
//                return;
//            }
            
            //
            K2Vector<double> multipleSelection;
            PublicationNodeList temppNodeDataList = pNodeDataList;
            if(firstpNodeListCount == 0)
                orignalpNodeDataList = pNodeDataList;
            else
                temppNodeDataList = orignalpNodeDataList;
            
            IControlView* TreePanelCtrlView = panelControlData->FindWidget(kAFSTreeViewWidgetID);
            if(TreePanelCtrlView != nil)
            {
                
                
                InterfacePtr<ITreeViewController> treeViewMgr(TreePanelCtrlView, UseDefaultIID());
                if(treeViewMgr==nil)
                {
                    //CA("treeViewCntrl==nil");
                    ptrIAppFramework->LogError("AP7_ProductFinder::SPSelectionObserver::Update::treeViewCntrl == nil");
                    break ;
                }
                
                NodeIDList selectedItem;
                
                treeViewMgr->GetSelectedItemsDisplayOrder(selectedItem);
                if(selectedItem.size()<=0)
                {
                    //CA("selectedItem.size()<=0");
                    ptrIAppFramework->LogInfo("AP7_ProductFinder::SPSelectionObserver::Update::selectedItem.size()<=0");
                    break ;
                }
                
                NodeID nid=selectedItem[0];
                TreeNodePtr<UIDNodeID>  uidNodeIDTemp(nid);
                int32 uid= uidNodeIDTemp->GetUID().Get();
                
                const int32 kSelectionLength = selectedItem.size();
                if(kSelectionLength > 1)
                {
                    singleSelectionSpray = kFalse;
//                    Mediator::setIsMultipleSelection(kTrue);
//                    CA("kSelectionLength > 1");
                    pNodeDataList.clear();
                    for(int32 count = 0;count < kSelectionLength ; count++)
                    {
                        NodeID nid=selectedItem[count];
                        TreeNodePtr<UIDNodeID>  uidNodeIDTemp(nid);
                        int32 uid= uidNodeIDTemp->GetUID().Get();
                        PublicationNode pNode;
                        bool16 isIdExist = dc.isExist(uid, pNode );
                        
                        if(isIdExist)
                        {
                            pNodeDataList.push_back(pNode);
                        }
                    }
                    
//                    FilterSprayer SSsp;
//                    SSsp.selectedSubSection.Append("Selected Item Group/Item List");
//                    SSsp.selectedSubSection.SetTranslatable(kFalse);
//                    iSSSprayer->setSprayCustomProductOrItemListFlag(kTrue);
//                    SSsp.startSprayingSubSection();
                    
                    firstpNodeListCount++;
                    
//                    if(isCancelButtonClick == kFalse)
//                    {
//                        pNodeDataList.clear();
//                        CurrentSelectedProductRow =0; //multipleSelection[0];//-100 ; //**********
//                        pNodeDataList = temppNodeDataList;
//                        
//                        //listCntl->DeselectAll();
//                        isCancelButtonClick = kFalse;
//                        break;
//                    }
//                    else
//                    {
//                        isCancelButtonClick = kTrue;
//                        break;
//                    }
                }
                else if(kSelectionLength == 1)
                {
                    singleSelectionSpray = kTrue;
//                    Mediator::setIsMultipleSelection(kTrue);
//                    CA("kSelectionLength == 1");
                    
                    pNodeDataList.clear();
                    for(int32 count = 0;count < kSelectionLength ; count++)
                    {						
                        NodeID nid=selectedItem[count];
                        TreeNodePtr<UIDNodeID>  uidNodeIDTemp(nid);
                        int32 uid= uidNodeIDTemp->GetUID().Get();
                        PublicationNode pNode;
                        bool16 isIdExist = dc.isExist(uid, pNode );
                        
                        if(isIdExist)
                        {
                            pNodeDataList.push_back(pNode);
                        }
                    }

//                    SubSectionSprayer SSsp;
                    
//                    SSsp.selectedSubSection.Append("Selected Item Group/Item");	
//                    SSsp.selectedSubSection.SetTranslatable(kFalse);
//                    iSSSprayer->setSprayCustomProductOrItemListFlag(kTrue);
                    
//                    SSsp.startSprayingSubSection();
                    
//                    firstpNodeListCount++;
                    
//                    if(isCancelButtonClick == kFalse)
//                    {
//                        pNodeDataList.clear();
//                        CurrentSelectedProductRow =0;// multipleSelection[0];//-100 ; //**********
//                        pNodeDataList = temppNodeDataList;
//                        iSSSprayer->setSprayCustomProductOrItemListFlag(kFalse);
//                        
//                        //listCntl->DeselectAll();
//                        isCancelButtonClick = kFalse;
//                        break;
//                    }
//                    else 
//                    {
//                        isCancelButtonClick = kTrue;
//                        break;
//                    }
                }
            }
            pNodeDataList = temppNodeDataList;
            
            
        }
        
    } while (kFalse);
}

void AFSPanelObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
    do
    {
        IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
        if (iControlView == nil)
            break;
        InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
        if (iSubject == nil)
            break;
        iSubject->AttachObserver(this, interfaceID);
    }
    while (false);
}

void AFSPanelObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
    do
    {
        IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
        if (iControlView == nil)
            break;
        InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
        if (iSubject == nil)
            break;
        iSubject->DetachObserver(this, interfaceID);
    }
    while (false); 
}

IPanelControlData* AFSPanelObserver::QueryPanelControlData()
{
    //CA("QueryPanelControlData");
    IPanelControlData* iPanel = nil;
    do
    {
        InterfacePtr<IPanelControlData> iPanelControlData(this, UseDefaultIID());
        if (iPanelControlData == nil)
            break;
        iPanelControlData->AddRef();
        iPanel = iPanelControlData;
    }
    while (false); 
    //CA("end of  QueryPanelControlData");
    return iPanel;
}

void AFSPanelObserver::getFilterResults(PubFilter pubfilter, int32 limit, int32 offset)
{
    InterfacePtr<IPanelControlData> panelControlData(QueryPanelControlData());
    
    if(!panelControlData)
    {
        CA("No SPPanelWidgetControlData");
        return;
    }
    
    InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
    if(ptrIAppFramework == NULL)
    {
        return;
    }
    
    // Check login status
    bool16 loginStatus = ptrIAppFramework->getLoginStatus();
    
    if (loginStatus == kFalse)
    {
        return;
    }
    
    PMString filterId = pubfilter.getFilterId();
    
    // Get filter results
    // Get config item description field id
    if (itemDescriptionAttribute <= 0)
    {
        itemDescriptionAttribute = ptrIAppFramework->CONFIGCACHE_getItemDescriptionAttribute();
    }
    
    // Get filter results count
    if(pubfilter.getFilterResultCount() <= 0)
    {
        int32 filterResultCount;
        ptrIAppFramework->callFilterResultCountService(filterId, filterResultCount);
        pubfilter.setFilterResultCount(filterResultCount);
    }
    if (pubfilter.getFilterResultCount() > 0)
    {
        int32 totalPage = pubfilter.getFilterResultCount() / pubfilter.getLimit() + 1;
        pubfilter.setTotalPage(totalPage);
    }
    
    // Set value of filterResultTextWidgetId
    PMString filterResultText("Results: ");
    filterResultText.AppendNumber(pubfilter.getFilterResultCount());
    IControlView* filterResultCountWidgetView = panelControlData->FindWidget(kAFSFilterResultTextWidgetID);
    
    if (filterResultCountWidgetView == nil)
    {
        CA("Cannot find result text widget.");
    }
    else
    {
        InterfacePtr<ITextControlData>  textControlData(filterResultCountWidgetView, UseDefaultIID());
        filterResultText.SetTranslatable(kFalse);
        textControlData->SetString(filterResultText);
    }
    
    // Initial show/hide pagination button
    IControlView* prePageButton = nil;
    IControlView* nextPageButton = nil;
    prePageButton = panelControlData->FindWidget(kAFSPrePageButtonWidgetID);
    nextPageButton = panelControlData->FindWidget(kAFSNextPageButtonWidgetID);
    
    if (prePageButton != nil)
    {
        if (pubfilter.getCurrentPage() > 1)
        {
            prePageButton->ShowView();
        }
        else
        {
            prePageButton->HideView();
        }
    }
    
    if (nextPageButton != nil)
    {
        if (pubfilter.getTotalPage() > pubfilter.getCurrentPage())
        {
            nextPageButton->ShowView();
        }
        else
        {
            nextPageButton->HideView();
        }
    }
    
    
    // Get filter results
    vector<CItemModel> filterResult;
    ptrIAppFramework->callFilterResultService(filterId, limit, offset, filterResult);
    
    // Cache filter results
    pNodeDataList.clear();
    
    if (filterResult.size() > 0)
    {
        //Test
        PMString size("");
        size.AppendNumber(filterResult.size());
//        CA(size);
        
        
        vector<CItemModel>::iterator iter;
        
        for(iter = filterResult.begin(); iter != filterResult.end(); ++iter)
        {
            double id = iter->getItemID();
            double sectionId = iter->getSectionResultId();
            vector<ItemValue> values = iter->getValues();
            PMString name("Item        ");
            name.AppendNumber(PMReal(id));
            
            PMString description("");
            vector<ItemValue>::iterator iterValue;
            for(iterValue = values.begin(); iterValue != values.end(); ++iterValue)
            {
                if(PMReal(iterValue->getFieldId()) == PMReal(itemDescriptionAttribute))
                {
                    description.Append(iterValue->getFieldValue());
                }
            }
            
            name.Append(" :");
            name.Append(description);
            
            PublicationNode node;
            node.setPubId(id);
            node.setSectionID(sectionId);
            node.setPublicationName(name);
            pNodeDataList.push_back(node);
            
        }
        
    }
    // Run GetRootUID function
    IControlView* spTreeListBoxWidgetView = panelControlData->FindWidget(kAFSTreeViewWidgetID);
    InterfacePtr<ITreeViewMgr> treeViewMgr(spTreeListBoxWidgetView, UseDefaultIID());
    if(!treeViewMgr)
    {
        CA("No tree view");
        ptrIAppFramework->LogDebug("AP7_TemplateBuilder::TPLSelectionObserver::loadPaletteData::!treeViewMgr");
        return;
    }
    
    // Entrance of the tree
    AFSTreeModel pModel;
    PMString pfName("Root");
    pModel.setRoot(-1, pfName, 1);
    treeViewMgr->ClearTree(kTrue);
    pModel.GetRootUID();
    treeViewMgr->ChangeRoot();
}


#include "VCPlugInHeaders.h"
#include "CPMUnknown.h"
#include "ITreeViewHierarchyAdapter.h"
//#include "IUIDData.h"
#include "AFSID.h"
#include "IntNodeID.h"
#include "AFSTreeModel.h"


class AFSTreeViewHierarchyAdapter : public CPMUnknown<ITreeViewHierarchyAdapter>
{
public:
    AFSTreeViewHierarchyAdapter(IPMUnknown* boss);
    virtual ~AFSTreeViewHierarchyAdapter();
    virtual NodeID_rv	GetRootNode() const;
    virtual NodeID_rv	GetParentNode( const NodeID& node ) const;
    virtual int32		GetNumChildren( const NodeID& node ) const;
    virtual NodeID_rv	GetNthChild( const NodeID& node, const int32& nth ) const;
    virtual int32		GetChildIndex( const NodeID& parent, const NodeID& child ) const;
    virtual NodeID_rv	GetGenericNodeID() const;
    virtual bool16  ShouldAddNthChild( const NodeID& node, const int32& nth ) const { return kTrue; }
    
private:
    AFSTreeModel	fBscTreeModel;
};

CREATE_PMINTERFACE(AFSTreeViewHierarchyAdapter, kAFSTreeViewHierarchyAdapterImpl)

AFSTreeViewHierarchyAdapter::AFSTreeViewHierarchyAdapter(IPMUnknown* boss) :
CPMUnknown<ITreeViewHierarchyAdapter>(boss)
{
}

AFSTreeViewHierarchyAdapter::~AFSTreeViewHierarchyAdapter()
{
}

NodeID_rv	AFSTreeViewHierarchyAdapter::GetRootNode() const
{
    int32 rootUID = fBscTreeModel.GetRootUID();
    return IntNodeID::Create(rootUID);
}

NodeID_rv	AFSTreeViewHierarchyAdapter::GetParentNode( const NodeID& node ) const
{
    do
    {
        TreeNodePtr<IntNodeID> uidNodeID(node);
        if (uidNodeID == nil)
            break;
        
        int32 uid = uidNodeID->Get();
        if(uid == fBscTreeModel.GetRootUID())
            break;
        
        ASSERT(uid != kInvalidUID);
        if(uid == kInvalidUID)
            break;
        
        int32 uidParent = fBscTreeModel.GetParentUID(uid);
        if(uidParent != kInvalidUID)
            return IntNodeID::Create(uidParent);
        
    }while(kFalse);
    return kInvalidNodeID;
}

int32 AFSTreeViewHierarchyAdapter::GetNumChildren( const NodeID& node ) const
{
    int32 retval=0;
    do
    {
        TreeNodePtr<IntNodeID> uidNodeID(node);
        if (uidNodeID == nil)
            break;
        
        int32 uid = uidNodeID->Get();
        if(uid == kInvalidUID)
            break;
        
        if(uid == fBscTreeModel.GetRootUID())
            retval = fBscTreeModel.GetRootCount();
        else
            retval = fBscTreeModel.GetChildCount(uid);
        
    } while(kFalse);
    return retval;
}

NodeID_rv	AFSTreeViewHierarchyAdapter::GetNthChild( const NodeID& node, const int32& nth ) const
{
    TreeNodePtr<IntNodeID>	uidNodeID(node);
    if( uidNodeID != nil)
    {
        int32 uidChild = 0 ; //kInvalidUID;
        if(uidNodeID->Get() == fBscTreeModel.GetRootUID())
        {
            uidChild = fBscTreeModel.GetNthRootChild(nth);
        }
        else
        {
            uidChild = fBscTreeModel.GetNthChildUID(uidNodeID->Get(), nth);
        }
        
        if(uidChild != kInvalidUID)
            return IntNodeID::Create(uidChild);
    }
    return kInvalidNodeID;
}

int32 AFSTreeViewHierarchyAdapter::GetChildIndex
(const NodeID& parent, const NodeID& child ) const
{
    do
    {
        TreeNodePtr<IntNodeID>	parentUIDNodeID(parent);
        ASSERT(parentUIDNodeID);
        if(parentUIDNodeID==nil)
            break;
        
        TreeNodePtr<IntNodeID>	childUIDNodeID(child);
        ASSERT(childUIDNodeID);
        if(childUIDNodeID==nil)
            break;
        
        if(parentUIDNodeID->Get() == kInvalidUID)
            break;
        
        if(childUIDNodeID->Get() == kInvalidUID) 
            break;
        
        if(parentUIDNodeID->Get() == fBscTreeModel.GetRootUID()) 
            return fBscTreeModel.GetIndexForRootChild(childUIDNodeID->Get());
        else 
            return fBscTreeModel.GetChildIndexFor(parentUIDNodeID->Get(), childUIDNodeID->Get());			
    } while(kFalse);
    return (-1);
}

NodeID_rv	AFSTreeViewHierarchyAdapter::GetGenericNodeID() const
{
    return IntNodeID::Create(0/*kInvalidUID*/);
}

#include "VCPluginHeaders.h"
#include "PMString.h"
#include "FilterData.h"
#include "vector"

using namespace std;

vector<PubFilter> FilterData::filterVector;

void FilterData::setFilterVector(PMString id,PMString name){
    PubFilter filter;
    filter.setFilterId(id);
    filter.setFilterName(name);
    filterVector.push_back(filter);
}

PubFilter FilterData::getFilterVector(int32 index){
    return filterVector[index];
}

void FilterData::clearFilterVector(void){
    filterVector.erase(filterVector.begin(),filterVector.end());
}

void FilterData::updateFilterVector(PubFilter pubfilter, int32 index){
    filterVector[index] = pubfilter;
}

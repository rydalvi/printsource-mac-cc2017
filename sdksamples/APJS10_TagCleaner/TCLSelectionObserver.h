#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ISubject.h"
#include "IAppFramework.h"
//#include "ListBoxHelper.h"
#include "IListBoxController.h"
#include "ITextControlData.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
#include "TCLActionComponent.h"
#include "IDocFileHandler.h"
//#include "CObserver.h"
#include "PMTypes.h"
#include "InterfaceColorDefines.h"
#include "IPMStream.h"
#include "WorkspaceID.h"

#include "TCLID.h"


class TCLSelectionObserver : public ActiveSelectionObserver
{
	public:
		TCLSelectionObserver(IPMUnknown *boss);
		virtual ~TCLSelectionObserver();
		void AutoAttach();
		void AutoDetach();
		void loadPaletteData();	
		void Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy);
		IPanelControlData*	QueryPanelControlData();

		ErrorCode CloseDocument(const UIDRef& documentUIDRef, 
			const IDFile* idFile,
			bool16 saveOnClose = kFalse,
			UIFlags uiFlags = K2::kFullUI, 
			bool8 allowCancel = kTrue, 
			IDocFileHandler::CloseCmdMode cmdMode = IDocFileHandler::kSchedule);
		//IDFile DocFile;

protected :
	    void AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		void DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
		

};



//========================================================================================
//  
//  $File: $
//  
//  Owner: Apsiva Inc.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// General includes:
#include "MenuDef.fh"
#include "ActionDef.fh"
#include "ActionDefs.h"
#include "AdobeMenuPositions.h"
#include "LocaleIndex.h"
#include "PMLocaleIds.h"
#include "StringTable.fh"
#include "ObjectModelTypes.fh"
#include "ShuksanID.h"
#include "ActionID.h"
#include "CommandID.h"
#include "WorkspaceID.h"
#include "WidgetID.h"
#include "BuildNumber.h"
#include "PlugInModel_UIAttributes.h"
#include "PanelList.fh"
//#include "LNGID.h"

#include "InterfaceColorDefines.h"
#include "IControlViewDefs.h"
#include "SysControlIDs.h"
#include "Widgets.fh"	// for PalettePanelWidget or DialogBoss


// Project includes:
#include "TCLID.h"
#include "GenericID.h"
#include "ShuksanID.h"
#include "TextID.h"
#include "AFWJSID.h"

#ifdef __ODFRC__

/*  Plugin version definition.
*/
resource PluginVersion (kSDKDefPluginVersionResourceID)
{
	kTargetVersion,
	kTCLPluginID,
	kSDKDefPlugInMajorVersionNumber, kSDKDefPlugInMinorVersionNumber,
	kSDKDefHostMajorVersionNumber, kSDKDefHostMinorVersionNumber,
	kTCLCurrentMajorFormatNumber, kTCLCurrentMinorFormatNumber,
	{ kInDesignProduct, kInCopyProduct},
	{ kWildFS },
	kUIPlugIn,
	kTCLVersion
};

/*  The ExtraPluginInfo resource adds extra information to the Missing Plug-in dialog
	that is popped when a document containing this plug-in's data is opened when
	this plug-in is not present. These strings are not translatable strings
	since they must be available when the plug-in isn't around. They get stored
	in any document that this plug-in contributes data to.
*/
resource ExtraPluginInfo(1)
{
	kTCLCompanyValue,			// Company name
	kTCLMissingPluginURLValue,	// URL 
	kTCLMissingPluginAlertValue,	// Missing plug-in alert text
};

/* Boss class definition.
*/
resource ClassDescriptionTable(kSDKDefClassDescriptionTableResourceID)
{{{
	
	/**
	 This boss class supports two interfaces:
	 IActionComponent and IPMPersist.

	 
	 @ingroup ap7_tagcleaner
	*/
	Class
	{
		kTCLActionComponentBoss,
		kInvalidClass,
		{
			/** Handle the actions from the menu. */
			IID_IACTIONCOMPONENT, kTCLActionComponentImpl,
			/** Persist the state of the menu across application instantiation.
			Implementation provided by the API.*/
			IID_IPMPERSIST, kPMPersistImpl
		}
	},

	/**
		 This boss class inherits from an API panel boss class, and
		 adds an interface to control a pop-up menu on the panel.
		 The implementation for this interface is provided by the API.

		 
		 @ingroup ap7_tagcleaner
	*/

	Class
	{
		kTCLPanelWidgetBoss,
		kPalettePanelWidgetBoss,
		{
			/** The plug-in's implementation of ITextControlData with an exotic IID of IID_IPANELMENUDATA.
			Implementation provided by the API.
			*/
			IID_IPANELMENUDATA, kCPanelMenuDataImpl,
			IID_IOBSERVER , kTCLContentSelectionObserverImpl,
		}
	},
	
	


}}};

/*  Implementation definition.
*/
resource FactoryList (kSDKDefFactoryListResourceID)
{
	kImplementationIDSpace,
	{
		#include "TCLFactoryList.h"
	}
};


/*  Menu definition.
*/
resource MenuDef (kSDKDefMenuResourceID)
{
	{
		/*
		// The About Plug-ins sub-menu item for this plug-in.
		kTCLAboutActionID,			// ActionID (kInvalidActionID for positional entries)
		kTCLAboutMenuPath,			// Menu Path.
		kSDKDefAlphabeticPosition,			// Menu Position.
		kSDKDefIsNotDynamicMenuFlag,		// kSDKDefIsNotDynamicMenuFlag or kSDKDefIsDynamicMenuFlag
		*/

        /*
		// Separator for the popup menu on the panel
		kTCLSeparator1ActionID,
		kTCLInternalPopupMenuNameKey kSDKDefDelimiterAndSeparatorPath,	// :- to mark an item as a separator.
		kTCLSeparator1MenuItemPosition,
		kSDKDefIsNotDynamicMenuFlag,
		*/
        /*
		// About this plugin submenu for the popup menu on the panel
		kTCLPopupAboutThisActionID,
		kTCLTargetMenuPath
		kTCLAboutThisMenuItemPosition,
		kSDKDefIsNotDynamicMenuFlag,
		*/
		
		kTCLPanelPSMenuActionID,
		"Main:"kAFWJSPRINTsourceKey, //kTCLPrintsourceKey,//kPSPPluginsMenuPath,
		5, 
		kSDKDefIsNotDynamicMenuFlag, 


	}
};

/* Action definition.
*/
resource ActionDef (kSDKDefActionResourceID)
{
	{
	/*
		kTCLActionComponentBoss, 		// ClassID of boss class that implements the ActionID.
		kTCLAboutActionID,	// ActionID.
		kTCLAboutMenuKey,	// Sub-menu string.
		kOtherActionArea,				// Area name (see ActionDefs.h).
		kNormalAction,					// Type of action (see ActionDefs.h).
		kDisableIfLowMem,				// Enabling type (see ActionDefs.h).
		kInvalidInterfaceID,			// Selection InterfaceID this action cares about or kInvalidInterfaceID.
		kSDKDefInvisibleInKBSCEditorFlag, // kSDKDefVisibleInKBSCEditorFlag or kSDKDefInvisibleInKBSCEditorFlag.
		*/
		
		kTCLActionComponentBoss,
		kTCLPanelPSMenuActionID,
		kTCLPluginsMenuKey,	
		kOtherActionArea,
		kNormalAction,
		kCustomEnabling, //kCustomEnabling,	
		kInvalidInterfaceID,
		kSDKDefVisibleInKBSCEditorFlag,
		
		

        /*
		kTCLActionComponentBoss,
		kTCLPopupAboutThisActionID,
		kSDKDefAboutThisPlugInMenuKey,	// Key to the name of this action
		kOtherActionArea,
		kNormalAction,
		kDisableIfLowMem,
		kInvalidInterfaceID,
		kSDKDefInvisibleInKBSCEditorFlag,
        */


	}
};


/*  LocaleIndex
	The LocaleIndex should have indicies that point at your
	localizations for each language system that you are localized for.
*/
/*  String LocaleIndex.
*/
resource LocaleIndex ( kSDKDefStringsResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_enUS, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_enGB, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_deDE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_frFR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_esES, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_ptBR, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_svSE, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_daDK, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nlNL, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_itIT, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_nbNO, kSDKDefStringsResourceID + index_enUS
		kWildFS, k_fiFI, kSDKDefStringsResourceID + index_enUS
		kInDesignJapaneseFS, k_jaJP, kSDKDefStringsResourceID + index_jaJP
	}
};

resource LocaleIndex (kSDKDefStringsNoTransResourceID)
{
	kStringTableRsrcType,
	{
		kWildFS, k_Wild, kSDKDefStringsNoTransResourceID + index_enUS
	}
};

resource StringTable (kSDKDefStringsNoTransResourceID + index_enUS)
{
	k_enUS,									// Locale Id
	kEuropeanMacToWinEncodingConverter,		// Character encoding converter
	{
		// No-Translate strings go here:

		kTCLInternalPopupMenuNameKey,	kTCLInternalPopupMenuNameKey,		// No need to translate, internal menu name.

	}
};




/*  Panel LocaleIndex.
*/
resource LocaleIndex (kSDKDefPanelResourceID)
{
	kViewRsrcType,
	{
		kWildFS, k_Wild, 	kSDKDefPanelResourceID + index_enUS
	}
};
/*
type TCLPanelWidget(kViewRsrcType) : ErasablePrimaryResourcePanelWidget(ClassID = kTCLPanelWidgetBoss)
{

};
*/


/*  Type definition.
*/
type TCLPanelWidget(kViewRsrcType) : PalettePanelWidget(ClassID = kTCLPanelWidgetBoss)
{
	CPanelMenuData;
};

/*  PanelList definition.
*/
resource PanelList (kSDKDefPanelResourceID)
{
	{
		// 1st panel in the list
		kSDKDefPanelResourceID,		// Resource ID for this panel (use SDK default rsrc ID)
		kTCLPluginID,			// ID of plug-in that owns this panel
		kNotResizable//kIsResizable,
		kTCLPanelWidgetActionID,	// Action ID to show/hide the panel
		kTCLPanelTitleKey,	// Shows up in the Window list.
		"",							// Alternate menu path of the form "Main:Foo" if you want your palette menu item in a second place
		0.0,						// Alternate Menu position Alternate Menu position for determining menu order
		0,0,						// Rsrc ID, Plugin ID for a PNG icon resource to use for this palette
		c_Panel
	}
};

/*  PanelView definition.
	The view is not currently localised: therefore, it can reside here.
	However, if you wish to localise it, it is recommended to locate it in one of
	the localised framework resource files (i.e. TCL_enUS.fr etc.).
*/

resource PNGA(kTCLPNGAnalyze_DocumentIconRsrcID) "Analyze_Document_116x18.png"
resource PNGR(kTCLPNGAnalyze_DocumentIconRollRsrcID)   "Analyze_Document_hover_116x18.png"

resource PNGA(kPNGCleanUp_DocumentIconRsrcID)  "Clean_Up_Document-116x18.png"
resource PNGR(kPNGCleanUp_DocumentIconRollRsrcID)  "Clean_Up_Document_hover-116x18.png"

resource PNGA(kTCLPNGCloseIconRsrcID) "Close_53x18.png"
resource PNGR(kTCLPNGCloseIconRollRsrcID)   "Close_hover_53x18.png"




resource TCLPanelWidget(kSDKDefPanelResourceID + index_enUS)
{
	__FILE__, __LINE__,					// Localization macro
	kTCLPanelWidgetID,			// WidgetID
	kPMRsrcID_None,						// RsrcID
	kBindNone,							// Binding (0=none)
	//kBindLeft | kBindRight| kBindTop | kBindBottom ,
	0, 0, 360, 250,						// Frame: left, top, right, bottom.
	kTrue, kTrue,						// Visible, Enabled
	kFalse,								// Erase background
	kInterfacePaletteFill,				// Erase to color
	kFalse,								// Draw dropshadow
	kTCLPanelTitleKey,			// Panel name
	{
	/*
		StaticTextWidget
		(
			0,								// WidgetId (default=0)
			 kSysStaticTextPMRsrcId,		// RsrcId
			kBindNone,						// Frame binding
			5, 10, 202, 27,					// Frame: left, top, right, bottom.
			kTrue, kTrue,					// Visible, Enabled
			kAlignLeft, kEllipsizeEnd, kTrue		// Alignment, ellipsize style, convert ampersands
			kTCLStaticTextKey,		// Initial text.
			0								// No associated widget
		),
		*/
		GroupPanelWidget
		(
			kTLCSecondGroupPanelWidgetID, // widget ID
			kPMRsrcID_None, // PMRsrc ID
			kBindLeft | kBindRight | kBindTop //kBindNone,// frame binding
			Frame(10 , 10 , 355 , 245) // left, top, right, bottom
			kTrue, // visible
			kTrue, // enabled
			0, // header widget ID
			{ // 
			RollOverIconButtonWidget
				(
					kAnalyzeDocumentWidgetID, 
					kTCLPNGAnalyze_DocumentIconRsrcID
					kTCLPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(12,90,132,110)//	Frame(258.0 ,20.0-6,338.0 ,40.0-4)                  //Frame(171+2 , 85 , 200 , 108 )
					kTrue, 
					kTrue,
					kADBEIconSuiteButtonType,
				),
				RollOverIconButtonWidget
				(
					kStartCleanUpButtonWidgetID, 
					kPNGCleanUp_DocumentIconRsrcID
					kTCLPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(135,90,255,110)//	Frame(258.0 ,20.0-6,338.0 ,40.0-4)                  //Frame(171+2 , 85 , 200 , 108 )
					kTrue, 
					kTrue,
					kADBEIconSuiteButtonType,
				),
				//removed close button 
				/*
				RollOverIconButtonWidget
				(
					kClose1ButtonWidgetID, 
					kTCLPNGCloseIconRsrcID
					kTCLPluginID,									// WidgetId, RsrcId
					kBindNone,										// Frame binding
					Frame(258,110,314,130)//	Frame(258.0 ,20.0-6,338.0 ,40.0-4)                  //Frame(171+2 , 85 , 200 , 108 )
					kTrue, 
					kTrue,
					kADBEIconSuiteButtonType,
				),
				*/
				
				StaticTextWidget
				(
					// CControlView properties
					kLevel2TextWidgetID,	// widget ID
					kSysStaticTextPMRsrcId,			// PMRsrc ID
					kBindNone,						// frame binding
					Frame(15,20,300,30),			// left, top, right, bottom	//Frame(10,40,110,60),
					kTrue,							// visible
					kTrue, 
													// StaticTextAttributes properties
					kAlignLeft,						// Alignment
					kDontEllipsize,kTrue,					// Ellipsize style
					kTCLClickAnalyzeButtonKey                                  
													// AssociatedWidgetAttributes properties
					kInvalidWidgetID,				// associated widget ID
				),
				
				StaticTextWidget
				(
					// CControlView properties
					kLevel2TextWidgetID,	// widget ID
					kSysStaticTextPMRsrcId,			// PMRsrc ID
					kBindNone,						// frame binding
					Frame(15,33,250,43),			// left, top, right, bottom	//Frame(10,40,110,60),
					kTrue,							// visible
					kTrue, 
													// StaticTextAttributes properties
					kAlignLeft,						// Alignment
					kDontEllipsize,kTrue,					// Ellipsize style
					kTCLHowManyUnusedFramesKey                                    //to obtain a status",			// control label
													// AssociatedWidgetAttributes properties
					kInvalidWidgetID,				// associated widget ID
				),
				
				
				
				
					StaticTextWidget
				(
					// CControlView properties
					kLevel3TextWidgetID,	// widget ID
					kSysStaticTextPMRsrcId,			// PMRsrc ID
					kBindNone,						// frame binding
					Frame(15,46,260,56),			// left, top, right, bottom	//Frame(10,40,110,60),
					kTrue,							// visible
					kTrue, 
													// StaticTextAttributes properties
					kAlignLeft,						// Alignment
					kDontEllipsize,kTrue,					// Ellipsize style
					kTCLAnyUnusedFramesKey		// control label
													// AssociatedWidgetAttributes properties
					kInvalidWidgetID,				// associated widget ID
				),
				StaticTextWidget
				(
					// CControlView properties
					kLevel3TextWidgetID,	// widget ID
					kSysStaticTextPMRsrcId,			// PMRsrc ID
					kBindNone,						// frame binding
					Frame(15,59,270,69),			// left, top, right, bottom	//Frame(10,40,110,60),
					kTrue,							// visible
					kTrue, 
													// StaticTextAttributes properties
					kAlignLeft,						// Alignment
					kDontEllipsize,kTrue,					// Ellipsize style
					kTCLCleanUpDocumentKey		// control label
													// AssociatedWidgetAttributes properties
					kInvalidWidgetID,				// associated widget ID
				),
				
				StaticTextWidget
				(
					// CControlView properties
					kLevel4TextWidgetID,	// widget ID
					kSysStaticTextPMRsrcId,			// PMRsrc ID
					kBindNone,						// frame binding
					Frame(15,120,165,140),			// left, top, right, bottom	//Frame(10,40,110,60),
					kTrue,							// visible
					kTrue, 
													// StaticTextAttributes properties
					kAlignLeft,						// Alignment
					kDontEllipsize,kTrue,					// Ellipsize style
					kTCLTotalFramesInDocumentKey,			// control label
													// AssociatedWidgetAttributes properties
					kInvalidWidgetID,				// associated widget ID
				),
				StaticTextWidget
				(
					// CControlView properties
					kLevel8TextWidgetID,	// widget ID
					kSysStaticTextPMRsrcId,			// PMRsrc ID
					kBindNone,						// frame binding
					Frame(170,120,250,140),			// left, top, right, bottom	//Frame(10,40,110,60),
					kTrue,							// visible
					kTrue, 
													// StaticTextAttributes properties
					kAlignLeft,						// Alignment
					kDontEllipsize,kTrue,					// Ellipsize style
					kTCLNotAnalyzedKey,			// control label
													// AssociatedWidgetAttributes properties
					kInvalidWidgetID,				// associated widget ID
				),
				StaticTextWidget
				(
					// CControlView properties
					kLevel5TextWidgetID,	// widget ID
					kSysStaticTextPMRsrcId,			// PMRsrc ID
					kBindNone,						// frame binding
					Frame(15,145,165,165),			// left, top, right, bottom	//Frame(10,40,110,60),
					kTrue,							// visible
					kTrue, 
													// StaticTextAttributes properties
					kAlignLeft,						// Alignment
					kDontEllipsize,kTrue,					// Ellipsize style
					kTCLUnusedFramesInDocumentKey,			// control label
													// AssociatedWidgetAttributes properties
					kInvalidWidgetID,				// associated widget ID
				),
				StaticTextWidget
				(
					// CControlView properties
					kLevel9TextWidgetID,	// widget ID
					kSysStaticTextPMRsrcId,			// PMRsrc ID
					kBindNone,						// frame binding
					Frame(170,145,250,165),			// left, top, right, bottom	//Frame(10,40,110,60),
					kTrue,							// visible
					kTrue, 
													// StaticTextAttributes properties
					kAlignLeft,						// Alignment
					kDontEllipsize,kTrue,					// Ellipsize style
					kTCLNotAnalyzedKey,			// control label
													// AssociatedWidgetAttributes properties
					kInvalidWidgetID,				// associated widget ID
				),
				
				InfoStaticTextWidget
				 (
					kLevel7TextWidgetID, 
					kPMRsrcID_None,							// WidgetId, RsrcId
					kBindLeft | kBindRight,					// Frame binding
					Frame(35,195,340,215), //Frame(122+20-10,3,158+16,23) // Frame(15,3,50,23)//Frame(20,3,55,23)// Frame
					kTrue, kTrue, kAlignLeft, 
					kEllipsizeEnd,kTrue,							// Visible, Enabled
					"",//"unused frames have been removed.",	//File Name							// Text
					0,
					kPaletteWindowSystemScriptHiliteFontId, 
					kPaletteWindowSystemScriptHiliteFontId,
				 ),
				 InfoStaticTextWidget
				 (
					kLevel20TextWidgetID, 
					kPMRsrcID_None,							// WidgetId, RsrcId
					kBindLeft | kBindRight,					// Frame binding
					Frame(15,195,340,215), //Frame(122+20-10,3,158+16,23) // Frame(15,3,50,23)//Frame(20,3,55,23)// Frame
					kTrue, kTrue, kAlignLeft, 
					kEllipsizeEnd,kTrue,							// Visible, Enabled
					"",	//File Name							// Text
					0,
					kPaletteWindowSystemScriptHiliteFontId, 
					kPaletteWindowSystemScriptHiliteFontId,
				 ),
				 
				InfoStaticTextWidget
				 (
					kfilenameTextWidgetID, 
					kPMRsrcID_None,							// WidgetId, RsrcId
					kBindLeft | kBindRight,					// Frame binding
					Frame(15,170,160,190), //Frame(122+20-10,3,158+16,23) // Frame(15,3,50,23)//Frame(20,3,55,23)// Frame
					kTrue, kTrue, kAlignLeft, 
					kEllipsizeEnd,kTrue,							// Visible, Enabled
					"",	//File Name							// Text
					0,
					kPaletteWindowSystemScriptHiliteFontId, 
					kPaletteWindowSystemScriptHiliteFontId,
				 ),
				 
				 
				InfoStaticTextWidget
				 (
					kfilehasbeencleanedTextWidgetID, 
					kPMRsrcID_None,							// WidgetId, RsrcId
					kBindLeft | kBindRight,					// Frame binding
					Frame(15,170,340,190), //Frame(122+20-10,3,158+16,23) // Frame(15,3,50,23)//Frame(20,3,55,23)// Frame
					kTrue, kTrue, kAlignLeft, 
					kEllipsizeEnd,kTrue,							// Visible, Enabled
					"",		//has been cleaned.						// Text
					0,
					kPaletteWindowSystemScriptHiliteFontId, 
					kPaletteWindowSystemScriptHiliteFontId,
				 ),
				
				/*
				StaticTextWidget
				(
					// CControlView properties
					kconcatfilenameTextWidgetID,	// widget ID
					kSysStaticTextPMRsrcId,			// PMRsrc ID
					kBindNone,						// frame binding
					Frame(15,170,340,190),			// left, top, right, bottom	//Frame(10,40,110,60),
					kTrue,							// visible
					kTrue, 
													// StaticTextAttributes properties
					kAlignLeft,						// Alignment
					kDontEllipsize,kTrue,					// Ellipsize style
					"",			// control label
													// AssociatedWidgetAttributes properties
					kInvalidWidgetID,				// associated widget ID
				),	
				*/
				
				
				
			}
		),
	}

	kTCLInternalPopupMenuNameKey		// Popup menu name (internal)
};



#endif // __ODFRC__

#include "TCL_enUS.fr"
#include "TCL_jaJP.fr"

//  Code generated by DollyXs code generator

#include "VCPluginHeaders.h"
#include "ISelectionManager.h"
#include "SelectionObserver.h"
#include "Trace.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "CAlert.h"
#include "IWidgetParent.h"
#include "ILayoutUtils.h"
#include "ISubject.h"
#include "ISelectableDialogSwitcher.h"
#include "IAppFramework.h"
#include "IListBoxController.h"
#include "IListControlData.h"
#include "ITextControlData.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
#include "TCLSelectionObserver.h"
#include "IListBoxController.h"
#include "ILayoutUtils.h"
#include "ISpread.h"
#include "IGeometry.h"
#include "ISpreadList.h"
#include "SystemUtils.h"
#include "ITextAttrUtils.h"
#include "IPanelControlData.h"
#include "ITextValue.h"
#include "AcquireModalCursor.h" 
#include "ILayoutUtils.h"		//Cs4
#include "ILayoutUIUtils.h"		//Cs4
#include "ITextFrameColumn.h"
#include "IHierarchy.h"
#include "IMultiColumnTextFrame.h"
#include "IGraphicFrameData.h"
#include "IDialogMgr.h"
#include "IWindow.h"
#include "ILayoutControlData.h"
#include "IXMLUtils.h"
#include  "IIDXMLElement.h"
#include "XMLReference.h"
#include "IXMLElementCommands.h"
#include "IDatabase.h"
#include "TCLActionComponent.h"
#include "AcquireModalCursor.h"
#include "ProgressBar.h"
#include "TCLActionComponent.h"
#include "IPanelMgr.h"
#include "IApplication.h"
#include "FileUtils.h"
#include "IDFile.h"
#include "SDKFileHelper.h"
#include "IDocFileHandler.h"
#include "IDocumentUtils.h"
#include "IGalleySettings.h"
#include <IInterfaceColors.h>
#include "IGalleySettings.h"
#include "IWorkspace.h"
#include "IDocumentList.h"
//#include "IIndexTopicListList.h"
//#include "IIndexTopicList.h"
//#include "IndexTopicEntry.h"
//#include "IndexTopicEntryNode.h"
//#include "IActiveTopicListContext.h"
//#include "ICreateTopicListCmdData.h"
//#include "ICreateTopicEntryCmdData.h"
//#include "ICreateIndexPageEntryCmdData.h"
//#include "IIndexPageEntryCmdData.h"
//#include "IRangeData.h"
//#include "IInsertIndexMarkCmdData.h"
//#include "ISelectionUtils.h"
//#include "ITextSelectionSuite.h"
//#include "IDeleteIndexPageEntryCmdData.h"
//#include "IDeleteTopicEntryCmdData.h"
//#include "IndexReference.h"
//#include "IPageItemTypeUtils.h"
//#include "IXMLReferenceData.h"
//#include "IStyleInfo.h"
//#include "ITextAttributes.h"
//#include "IStyleGroupManager.h"
//#include "IBookManager.h"
//#include "IBookContentMgr.h"
//#include "IBookUtils.h"
//#include "IOpenLayoutCmdData.h"
//#include "IDocumentCommands.h"
//

class RangeProgressBar;
RealAGMColor red;

#define CA(x) CAlert::InformationAlert(x);

InterfacePtr<IPanelControlData> PanelControlDataforList;

CREATE_PMINTERFACE(TCLSelectionObserver, kTCLContentSelectionObserverImpl)

TCLSelectionObserver::TCLSelectionObserver(IPMUnknown *boss) :
ActiveSelectionObserver(boss/*, IID_ITCLContentSELECTIONOBSERVER*/) {}//("RfhSelectionObserver::RfhSelectionObserver");}

TCLSelectionObserver::~TCLSelectionObserver() {}

void TCLSelectionObserver::AutoAttach()
{	
	ActiveSelectionObserver::AutoAttach();
	do
	{
		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData==nil)
		{
			return;
		}

		loadPaletteData();
		AttachWidget(iPanelControlData, kStartCleanUpButtonWidgetID, IID_ITRISTATECONTROLDATA);//IID_ITRISTATECONTROLDATA
		AttachWidget(iPanelControlData, kClose1ButtonWidgetID, IID_ITRISTATECONTROLDATA);
		AttachWidget(iPanelControlData, kAnalyzeDocumentWidgetID, IID_ITRISTATECONTROLDATA);
	}while(kFalse);
}

void TCLSelectionObserver::AutoDetach()
{
	ActiveSelectionObserver::AutoDetach();
	do
	{
		InterfacePtr<IPanelControlData> iPanelControlData(QueryPanelControlData());
		if (iPanelControlData==nil)
		{
			CA("iPanelControlData is nil");
			return;
		}

		DetachWidget(iPanelControlData, kStartCleanUpButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(iPanelControlData, kClose1ButtonWidgetID, IID_ITRISTATECONTROLDATA);
		DetachWidget(iPanelControlData, kAnalyzeDocumentWidgetID, IID_ITRISTATECONTROLDATA);
	}while(kFalse);
}

void TCLSelectionObserver::Update
(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{
	ActiveSelectionObserver::Update(theChange, theSubject, protocol, changedBy);
	do
	{	//JAMIE *** Why do we listen to frame selections?  Meaning, if I select a frame and the Tag Cleaner palette is opened, this Observer is called!  And why the observer get his twice on a frame selection?
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
            break;
		}

		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if(!panelControlData) 
		{
			break;
		}

		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			return;	
		}

		WidgetID theSelectedWidget = controlView->GetWidgetID();
		if(theSelectedWidget==kClose1ButtonWidgetID && theChange==kTrueStateMessage)
		{
			TCLActionComponent actionComponetObj(this);
			actionComponetObj.ClosePallete(); 
		}

		if(theSelectedWidget==kStartCleanUpButtonWidgetID && theChange==kTrueStateMessage)
		{
			//CA("Clean Up Document button clicked");
			AcquireWaitCursor awc ;
			awc.Animate(); 

			InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			IDocument* doc = layoutData->GetDocument();
			IDataBase* docDatabase = ::GetDataBase((IPMUnknown*) doc);
			const IDFile* idFile = docDatabase->GetSysFile();

			IIDXMLElement* rootElem = Utils<IXMLUtils>()->QueryRootElement(docDatabase);

			int32 count1 = rootElem->GetChildCount();
			int32 badTagsCnt = 0;

			do
			{
				PMString progressBarTest("Document Cleaner");
				progressBarTest.SetTranslatable(kFalse);
				PMString taskText("Removing invalid tags from document");
				taskText.SetTranslatable(kFalse);

				RangeProgressBar progressBar(progressBarTest, 0, count1, kTrue);
				progressBar.SetTaskText(taskText);
				
				for(int32 i = count1 - 1; i >= 0; --i)
				{
					XMLReference xmlRef = rootElem->GetNthChild(i);
					IIDXMLElement* childElem = xmlRef.Instantiate();

					const XMLContentReference& contentRef = childElem->GetContentReference();
					const UIDRef & uidref = contentRef.GetUIDRef();
					if(uidref  == UIDRef::gNull)
					{
						ErrorCode status = Utils<IXMLElementCommands>()->DeleteElement(xmlRef,kTrue);
						badTagsCnt++;
					}

					childElem->Release();

					progressBar.SetPosition(count1 - i);
				}
			} while (kFalse);

			rootElem->Release();


			//---------------------------------------------------------------------------------------------
			//code for show() hidden text
			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			if(!panelControlData) 
			{
				break;
			}

			InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
			if (controlView == nil)
			{
				return;	
			}

			IControlView * filenamecontrolview = panelControlData->FindWidget(kfilenameTextWidgetID);
			if(filenamecontrolview == nil)
			{
				break;
			}
			filenamecontrolview->ShowView();

			IControlView * fileiscleanedcontrolview = panelControlData->FindWidget(kfilehasbeencleanedTextWidgetID);
			if(fileiscleanedcontrolview == nil)
			{
				break;
			}
			fileiscleanedcontrolview->ShowView();

			IControlView * level20controlview = panelControlData->FindWidget(kLevel20TextWidgetID);
			if(level20controlview == nil)
			{
				break;
			}
			level20controlview->ShowView();

			IControlView * level7controlview = panelControlData->FindWidget(kLevel7TextWidgetID);
			if(level7controlview == nil)
			{
				break;
			}
			level7controlview->ShowView();
			//end for hidden code
			//------------------------------------------------------------------------------------------------

			IControlView * Level7Textcontrolview = panelControlData->FindWidget(kLevel7TextWidgetID);
			if(Level7Textcontrolview == nil)
			{
				break;
			}

			InterfacePtr<ITextControlData>Level7Textcontroldata(Level7Textcontrolview,UseDefaultIID());
			if(Level7Textcontroldata == nil)
			{
				break;
			}

			IControlView * Level20Textcontrolview = panelControlData->FindWidget(kLevel20TextWidgetID);
			if(Level20Textcontrolview == nil)
			{
				break;
			}

			InterfacePtr<ITextControlData>Level20Textcontroldata(Level20Textcontrolview,UseDefaultIID());
			if(Level20Textcontroldata == nil)
			{
				break;
			}

			PMString insertText1("");
			insertText1.AppendNumber(badTagsCnt);
			insertText1.SetTranslatable(kFalse);
			PMString unusedFramesHaveBeenRemoved(" unused frames have been removed.");
			unusedFramesHaveBeenRemoved.SetTranslatable(kFalse);

			insertText1.Append(unusedFramesHaveBeenRemoved);
			Level20Textcontroldata->SetString(insertText1);

			IControlView * Level9Textcontrolview = panelControlData->FindWidget(kLevel9TextWidgetID);
			if(Level9Textcontrolview == nil)
			{
				break;
			}

			InterfacePtr<ITextControlData>Level9Textcontroldata(Level9Textcontrolview,UseDefaultIID());
			if(Level9Textcontroldata == nil)
			{
				break;
			}

			PMString insertText2("");
			insertText2.AppendNumber(badTagsCnt);
			insertText2.SetTranslatable(kFalse);
			Level9Textcontroldata->SetString(insertText2);

			IControlView * Level8Textcontrolview = panelControlData->FindWidget(kLevel8TextWidgetID);
			if(Level8Textcontrolview == nil)
			{
				break;
			}
			InterfacePtr<ITextControlData>Level8Textcontroldata(Level8Textcontrolview,UseDefaultIID());
			if(Level8Textcontroldata == nil)
			{
				break;
			}

			PMString insertText("");
			insertText.AppendNumber(count1);
			insertText.SetTranslatable(kFalse);
			Level8Textcontroldata->SetString(insertText);

			//CloseDocument(::GetUIDRef(doc), idFile, kTrue);
			//We still want the save as prompt, instead of saving as automatically
			CloseDocument(::GetUIDRef(doc), nil,kTrue);
			PMString fileName("");
			doc->GetName(fileName);
           
			IControlView * FilenameTextcontrolview = panelControlData->FindWidget(kfilenameTextWidgetID);
			if(FilenameTextcontrolview == nil)
			{
				break;
			}

			InterfacePtr<ITextControlData>FilenameTextcontroldata(FilenameTextcontrolview,UseDefaultIID());
			if(FilenameTextcontroldata == nil)
			{
				break;
			}

			IControlView * fileiscleanednewcontrolview = panelControlData->FindWidget(kfilehasbeencleanedTextWidgetID);
			if(fileiscleanednewcontrolview == nil)
			{
				break;
			}

			InterfacePtr<ITextControlData>Textcontroldata(fileiscleanednewcontrolview,UseDefaultIID());
			if(Textcontroldata == nil)
			{
				break;
			}
			
			PMString insertText29("");
			insertText29.SetTranslatable(kFalse);
			insertText29.Append(fileName);

			PMString hasBeenCleaned(" has been cleaned.");
			hasBeenCleaned.SetTranslatable(kFalse);
			insertText29.Append(hasBeenCleaned);
			Textcontroldata->SetString(insertText29);
		}	

		if(theSelectedWidget==kAnalyzeDocumentWidgetID && theChange==kTrueStateMessage)
		{
			//CA("Analyze Document button clicked");
            AcquireWaitCursor awc ;
			awc.Animate(); 

			InterfacePtr<ILayoutControlData> layoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layoutData == nil)
			{
				PMString var=kTCLNotAnalyzedKey;
				
				InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
				if(!panelControlData) 
				{
					break;
				}

				IControlView * Level8Textcontrolview = panelControlData->FindWidget(kLevel8TextWidgetID);
				if(Level8Textcontrolview == nil)
				{
					break;
				}

				InterfacePtr<ITextControlData>Level8Textcontroldata(Level8Textcontrolview,UseDefaultIID());
				if(Level8Textcontroldata == nil)
				{
					break;
				}

				PMString insertText7("");
				insertText7.Append(var);
				insertText7.SetTranslatable(kFalse);
				Level8Textcontroldata->SetString(insertText7);

				IControlView * Level9Textcontrolview = panelControlData->FindWidget(kLevel9TextWidgetID);
				if(Level9Textcontrolview == nil)
				{
					break;
				}

				InterfacePtr<ITextControlData>Level9Textcontroldata(Level9Textcontrolview,UseDefaultIID());
				if(Level9Textcontroldata == nil)
				{
					break;
				}

				PMString insertText8("");
				insertText8.Append(var);
				insertText8.SetTranslatable(kFalse);
				Level9Textcontroldata->SetString(insertText8);

				IControlView * filenamecontrolview = panelControlData->FindWidget(kfilenameTextWidgetID);
				if(filenamecontrolview == nil)
				{
					break;
				}
				filenamecontrolview->HideView();

				IControlView * fileiscleanedcontrolview = panelControlData->FindWidget(kfilehasbeencleanedTextWidgetID);
				if(fileiscleanedcontrolview == nil)
				{
					break;
				}
				fileiscleanedcontrolview->HideView();

				IControlView * level20controlview = panelControlData->FindWidget(kLevel20TextWidgetID);
				if(level20controlview == nil)
				{
					break;
				}
				level20controlview->HideView();

				IControlView * level7controlview = panelControlData->FindWidget(kLevel7TextWidgetID);
				if(level7controlview == nil)
				{
					break;
				}
				level7controlview->HideView();
				break;
			}

			IDocument* doc = layoutData->GetDocument();
			if (doc == nil)
			{
				CA("doc == nil");
				break;
			}

			IDataBase* database = ::GetDataBase((IPMUnknown*)doc);//GetDataBase(doc);
			if(!database)
			{	
				break;
			}

			IIDXMLElement*  rootElem = Utils<IXMLUtils>()->QueryRootElement(database);
			if(rootElem == NULL)
			{	
				break;
			}


			int32 count1 = rootElem->GetChildCount();
			int32 badTagsCnt = 0;

			do
			{
				PMString progressBarTest("Document Cleaner");
				progressBarTest.SetTranslatable(kFalse);
				PMString taskText("Inspecting document for invalid tags");
				taskText.SetTranslatable(kFalse);

				RangeProgressBar progressBar(progressBarTest, 0, count1, kTrue);
				progressBar.SetTaskText(taskText);
				
				for(int32 i = count1 - 1; i >= 0; --i)
				{
					XMLReference xmlRef = rootElem->GetNthChild(i);
					IIDXMLElement* childElem = xmlRef.Instantiate();

					const XMLContentReference& contentRef = childElem->GetContentReference();
					const UIDRef & uidref = contentRef.GetUIDRef();
					if(uidref  == UIDRef::gNull)
					{
						badTagsCnt++;
					}

					childElem->Release();

					progressBar.SetPosition(i + 1);
				}
			} while (kFalse);

			rootElem->Release();


			//JAMIE - what does this do?
			InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
			if(ptrIAppFramework == nil)
			{
				return;
			}

			InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
			if(!panelControlData) 
			{
				break;
			}

			IControlView * Level8Textcontrolview = panelControlData->FindWidget(kLevel8TextWidgetID);
			if(Level8Textcontrolview == nil)
			{
				break;
			}
			InterfacePtr<ITextControlData>Level8Textcontroldata(Level8Textcontrolview,UseDefaultIID());
			if(Level8Textcontroldata == nil)
			{
				break;
			}
		
			PMString insertText("");
			insertText.AppendNumber(count1);
			insertText.SetTranslatable(kFalse);
			Level8Textcontroldata->SetString(insertText);
	
			IControlView * Level20Textcontrolview = panelControlData->FindWidget(kLevel20TextWidgetID);
			if(Level20Textcontrolview == nil)
			{
				break;
			}

			InterfacePtr<ITextControlData>Level20Textcontroldata(Level20Textcontrolview,UseDefaultIID());
			if(Level20Textcontroldata == nil)
			{
				break;
			}
	
			PMString insertText1("");
			insertText1.AppendNumber(badTagsCnt);
			insertText1.SetTranslatable(kFalse);
			Level20Textcontroldata->SetString(insertText1);
        
			IControlView * Level9Textcontrolview = panelControlData->FindWidget(kLevel9TextWidgetID);
			if(Level9Textcontrolview == nil)
			{
				break;
			}

			InterfacePtr<ITextControlData>Level9Textcontroldata(Level9Textcontrolview,UseDefaultIID());
			if(Level9Textcontroldata == nil)
			{
				break;
			}
		
			PMString insertText2("");
			insertText2.AppendNumber(badTagsCnt);
			insertText2.SetTranslatable(kFalse);
			Level9Textcontroldata->SetString(insertText2);

			IControlView * filenamecontrolview = panelControlData->FindWidget(kfilenameTextWidgetID);
			if(filenamecontrolview == nil)
			{
				break;
			}
			filenamecontrolview->HideView();

			IControlView * fileiscleanedcontrolview = panelControlData->FindWidget(kfilehasbeencleanedTextWidgetID);
			if(fileiscleanedcontrolview == nil)
			{
				break;
			}
			fileiscleanedcontrolview->HideView();

			IControlView * level20controlview = panelControlData->FindWidget(kLevel20TextWidgetID);
			if(level20controlview == nil)
			{
				break;
			}
			level20controlview->HideView();

			IControlView * level7controlview = panelControlData->FindWidget(kLevel7TextWidgetID);
			if(level7controlview == nil)
			{
				break;
			}
			level7controlview->HideView();
		}

	}while(kFalse);
}

IPanelControlData* TCLSelectionObserver::QueryPanelControlData()
{
	IPanelControlData* iPanel = nil;
	do
	{
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
		{
            break;
		}

		InterfacePtr<IWidgetParent> iWidgetParent(this, UseDefaultIID());
		if (iWidgetParent == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::QueryPanelControlData::iWidgetParent == nil");										
			break;
		}

		InterfacePtr<IPanelControlData> iPanelControlData(iWidgetParent->GetParent(), UseDefaultIID());
		if (iPanelControlData == nil)
		{
			//ptrIAppFramework->LogDebug("AP7_RefreshContent::RfhSelectionObserver::QueryPanelControlData::iPanelControlData == nil");												
			break;
		}

		iPanelControlData->AddRef();
		iPanel = iPanelControlData;

		// JAMIE : is this OK or will it mess up iPanel?
		//iPanelControlData->Release();
	}

	while (false); 
	return iPanel;
}

void TCLSelectionObserver::AttachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;

		iSubject->AttachObserver(this, interfaceID);
	}
	while (false); 
}

void TCLSelectionObserver::DetachWidget(IPanelControlData* iPanelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	do
	{
		IControlView* iControlView = iPanelControlData->FindWidget(widgetID);
		if (iControlView == nil)
			break;

		InterfacePtr<ISubject> iSubject(iControlView, UseDefaultIID());
		if (iSubject == nil)
			break;

		iSubject->DetachObserver(this, interfaceID);
	}
	while (false); 
}

void TCLSelectionObserver::loadPaletteData()
{
	do
	{
		PMString var=kTCLNotAnalyzedKey;
		InterfacePtr<IAppFramework> ptrIAppFramework(static_cast<IAppFramework*> (CreateObject(kAppFrameworkBoss,IAppFramework::kDefaultIID)));
		if(ptrIAppFramework == nil)
			return;

        InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if(!panelControlData) 
		{
			break;
		}

        IControlView * Level8Textcontrolview = panelControlData->FindWidget(kLevel8TextWidgetID);
		if(Level8Textcontrolview == nil)
		{
			break;
		}

		InterfacePtr<ITextControlData>Level8Textcontroldata(Level8Textcontrolview,UseDefaultIID());
		if(Level8Textcontroldata == nil)
		{
			break;
		}

		PMString insertText7("");
		insertText7.Append(var);
		Level8Textcontroldata->SetString(insertText7);

		IControlView * Level9Textcontrolview = panelControlData->FindWidget(kLevel9TextWidgetID);
		if(Level9Textcontrolview == nil)
		{
			break;
		}

		InterfacePtr<ITextControlData>Level9Textcontroldata(Level9Textcontrolview,UseDefaultIID());
		if(Level9Textcontroldata == nil)
		{
			break;
		}

		PMString insertText8("");
		insertText8.Append(var);
		Level9Textcontroldata->SetString(insertText8);

		IControlView * filenamecontrolview = panelControlData->FindWidget(kfilenameTextWidgetID);
		if(filenamecontrolview == nil)
		{
			break;
		}
        filenamecontrolview->HideView();

		IControlView * fileiscleanedcontrolview = panelControlData->FindWidget(kfilehasbeencleanedTextWidgetID);
		if(fileiscleanedcontrolview == nil)
		{
			break;
		}
		fileiscleanedcontrolview->HideView();

		IControlView * level20controlview = panelControlData->FindWidget(kLevel20TextWidgetID);
		if(level20controlview == nil)
		{
			break;
		}
		level20controlview->HideView();

		IControlView * level7controlview = panelControlData->FindWidget(kLevel7TextWidgetID);
		if(level7controlview == nil)
		{
			break;
		}
		level7controlview->HideView();

		//ptrIAppFramework->LogError("AP7_CategoryBrowser::CTBSelectionObserver::loadPaletteData");

		TCLActionComponent actionObsever(this);
		actionObsever.DoPalette();
	}while(kFalse);
}

ErrorCode TCLSelectionObserver::CloseDocument(const UIDRef& documentUIDRef,
			const IDFile* idFile,
			bool16 saveOnClose,
			UIFlags uiFlags, 
			bool8 allowCancel, 
			IDocFileHandler::CloseCmdMode cmdMode)
{
	ErrorCode result = kFailure;
	do {
		InterfacePtr<IDocFileHandler> docFileHandler(Utils<IDocumentUtils>()->QueryDocFileHandler(documentUIDRef));
		if (!docFileHandler) {
			break;
		}

		if(saveOnClose && docFileHandler->CanSaveAs(documentUIDRef) ) {
			docFileHandler->SaveAs(documentUIDRef, idFile, uiFlags);
			result = ErrorUtils::PMGetGlobalErrorCode();
			ASSERT_MSG(result == kSuccess, "IDocFileHandler::Save failed");
			if (result != kSuccess) {
				break;
			}

			// *** This was included for Index saving & closing a document
			//if (docFileHandler->CanClose(documentUIDRef)) {
			//	docFileHandler->Close(documentUIDRef);
			//	result = ErrorUtils::PMGetGlobalErrorCode();
			//	ASSERT_MSG(result == kSuccess, "IDocFileHandler::Close failed");
			//	if (result != kSuccess) {
			//		break;
			//	}
			//}
		}
	} while(false);

	return result;
}
